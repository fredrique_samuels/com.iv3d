package com.iv3d.app.launcher;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/10/06.
 */
public class AppLauncherMain {

    static Process process = null;
    static String[] commands = null;

    public static void main(String[] args) {
        if(args.length == 0) {
            return;
        }

        try {
            commands = getCommands(args[0]);
            startProcess();

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    process.destroy();
                }
            });

            run();

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    static String[] getCommands(String arg) {
        String fileAsString = getFileContents(arg);
        String[] split = fileAsString.split("\n");
        return Arrays.asList(split)
                .stream()
                .map(s -> {
                    String r = s.trim();
                    if(r.contains(" ")) {
                        r = "\"" + r + "\"";
                    }
                    return r;
                })
                .collect(Collectors.toList())
                .toArray(new String[]{});
    }

    static String getFileContents(String file) {
        StringBuilder sb = new StringBuilder();
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));

            String line = buf.readLine();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeStream(is);
        }

        return sb.toString();
    }

    private static void closeStream(InputStream is) {
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void run() throws InterruptedException, IOException {
        int i = 5;
        while (i != 0){
            if(process.isAlive()) {
                i=5;
                Thread.sleep(3000);
            } else {
                i-=1;
                System.out.println("Starting Process - retries left " + i);
                startProcess();
            }
        }
    }

    static boolean startProcess() throws IOException, InterruptedException {
        System.out.println("Starting Process");
        ProcessBuilder pb = new ProcessBuilder(commands);
        process=pb.start();
        process.waitFor(10, TimeUnit.SECONDS);
        return process.isAlive();
    }
}
