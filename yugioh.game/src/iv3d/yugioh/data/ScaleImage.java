package iv3d.yugioh.data;

import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by fred on 2017/04/07.
 */
public class ScaleImage {

    public static void main(String[] args) {

        String[] files = new String[]{
                "dark.png",
                "divine.png",
                "earth.png",
                "effect-bg.png",
                "fire.png",
                "fusion-bg.png",
                "level-1.png",
                "level-2.png",
                "level-3.png",
                "level-4.png",
                "level-5.png",
                "level-6.png",
                "level-7.png",
                "level-8.png",
                "level-9.png",
                "level-10.png",
                "level-11.png",
                "level-12.png",
                "light.png",
                "monster-data-box.png",
                "monster-pendulum-data-box.png",
                "normal-bg.png",
                "rank-1.png",
                "rank-2.png",
                "rank-3.png",
                "rank-4.png",
                "rank-5.png",
                "rank-6.png",
                "rank-7.png",
                "rank-8.png",
                "rank-9.png",
                "rank-10.png",
                "rank-11.png",
                "rank-12.png",
                "spell-box.png",
                "ritual-bg.png",
                "synchro-bg.png",
                "trap-box.png",
                "water.png",
                "wind.png",
                "xyz-bg.png",
                "link-bg.png",
                "ritual-box.png",
                "synchro-box.png",
                "xyz-box.png",
                "link-box.png",
                "effect-box.png",
                "normal-box.png",
                "fusion-box.png",
                "link-bottom-left-off.png",
                "link-bottom-left-on.png",
                "link-bottom-off.png",
                "link-bottom-on.png",
                "link-bottom-right-off.png",
                "link-bottom-right-on.png",
                "link-left-off.png",
                "link-left-on.png",
                "link-right-off.png",
                "link-right-on.png",
                "link-top-left-off.png",
                "link-top-left-on.png",
                "link-top-off.png",
                "link-top-on.png",
                "link-top-right-off.png",
                "link-top-right-on.png",
                "spell-bg.png",
                "trap-bg.png",
                "card-back-1.png"
        };

        String src = "/Users/fred/Documents/lab_work/github/dashboard-ui/yugioh-res/cardbuilder/layers/";
        String dst = "/Users/fred/Documents/lab_work/github/dashboard-ui/yugioh-res/cardbuilder/layers-lr/";

        for (String f : files) {
            String inputFile = src + f;
            String outFile = dst + f;
            BufferedImage im = loadImage(inputFile);
            BufferedImage scaled = scale(im, .5);
            saveImage(scaled, outFile);
        }
    }

    private static void saveImage(BufferedImage im, String outFileName) {
        try {
            File outputfile = new File(outFileName);
            ImageIO.write(im, "png", outputfile);
        } catch (IOException e) {
        }
    }

    private static BufferedImage scale(BufferedImage before, double scale) {
        int w = new Float(before.getWidth() * scale).intValue();
        int h = new Float(before.getHeight() * scale).intValue();
        BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        AffineTransform at = new AffineTransform();
        at.scale(scale, scale);
        AffineTransformOp scaleOp =
                new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        return scaleOp.filter(before, after);
    }

    private static BufferedImage loadImage(String s) {
        try {
            return ImageIO.read(new File(s));
        } catch (IOException e) {
        }
        return null;
    }
}
