package iv3d.yugioh.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iv3d.common.core.ResourceLoader;
import iv3d.games.ygo.cards.download.wiki.WikiCardDataDownloader;
import iv3d.games.ygo.cards.download.wiki.WikiCardsContainer;
import iv3d.games.ygo.cards.download.wiki.WikiCardData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by fred on 2017/04/10.
 */
public class DownloadData {
    interface PropertySetter {
        void apply(WikiCardData c, String name, String data);
    }

    public static void main(String[] args) {

        new WikiCardDataDownloader().run();

        List<String> demoCardNames = Arrays.asList(new String[]{
                "Call of the Haunted",
                "Armor Break",
                "Twin Twisters",
                "Black Pendant",
                "Summon Cloud",
                "Union Hangar",
                "Dark Hole",
                "Hymn of Light",
                "Mirror Force",
                "Alexandrite Dragon",
                "Qliphort Scout",
                "Black Luster Soldier",
                "Odd-Eyes Saber Dragon",
                "Odd-Eyes Pendulum Dragon",
                "Junk Warrior",
                "Gaia Knight, the Force of Earth",
                "Stardust Dragon",
                "Formula Synchron",
                "Clearwing Fast Dragon",
                "Blue-Eyes Ultimate Dragon",
                "Five-Headed Dragon",
                "Sea Monster of Theseus",
                "Odd-Eyes Venom Dragon",
                "Odd-Eyes Raging Dragon",
                "Gem-Knight Pearl",
                "Gem-Merchant",
                "Skelesaurus",
                "Tune Warrior",
                "D/D Orthros",
                "Legacy of Yata-Garasu",
                "Decode Talker",
                "Firewall Dragon",
                "Gaiasaber, the Video Knight",
                "Gouki the Great Ogre",
                "Honeybot",
                "Link Spider",
                "Missus Radiant",
                "Proxy Dragon",
                "Star Grail Dragon Imduk",
                "Star Grail Shrine Maiden Eve",
                "Star Grail Swordsman Aurum",
                "Star Grail Warrior Ningirsu",
                "Topologic Bomber Dragon",
                "Trickster Holy Angel"
        });
        File[] files = new File("../cardhtml").listFiles();

        List<WikiCardData> cards = new ArrayList<>();
        List<WikiCardData> demoCards = new ArrayList<>();
        int i=0;
        for (File file:files) {
            ++i;
//            if(!file.getPath().contains("../cardhtml/Abyss_Actor_-_Extras.html"))
//                continue;

            try {
                String s = ResourceLoader.readAsString(file);
                WikiCardData c = parseHtml(s);
                cards.add(c);
                System.out.println(i);

                if(demoCardNames.contains(c.nameEnglish) || c.nameEnglish.contains("Gear") || c.nameEnglish.contains("Gadget")) {
                    demoCards.add(c);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(file.getPath());
                System.out.println("--------------");
            }
//            break;
        }

        saveMasterFile(cards, "../master-list.json");
        saveMasterFile(demoCards, "../demo-master-list.json");

    }

    private static void saveMasterFile(List<WikiCardData> cards, String pathname) {
        WikiCardsContainer cardsContainer = new WikiCardsContainer();
        cardsContainer.cards = cards;
        File resultFile = new File(pathname);
        writeJsonFile(cardsContainer, resultFile);
    }

    private static void writeJsonFile(Object value, File resultFile) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(resultFile, value);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static WikiCardData parseHtml(String stringResult) {
        Map<String, PropertySetter> propertySetterMap = new HashMap<>();
        propertySetterMap.put("English", (c,n,d) -> c.nameEnglish=d);
        propertySetterMap.put("French", (c,n,d) -> c.nameFrench=d);
        propertySetterMap.put("German", (c,n,d) -> c.nameGerman=d);
        propertySetterMap.put("Italian", (c,n,d) -> c.nameItalian=d);
        propertySetterMap.put("Korean", (c,n,d) -> c.nameKorean=d);
        propertySetterMap.put("Portuguese", (c,n,d) -> c.namePortuguese=d);
        propertySetterMap.put("Spanish", (c,n,d) -> c.nameSpanish=d);
        propertySetterMap.put("Japanese (kana)", (c,n,d) -> c.nameJapaneseKana=d);
        propertySetterMap.put("Japanese (base)", (c,n,d) -> c.nameJapaneseBase=d);
        propertySetterMap.put("Japanese (rōmaji)", (c,n,d) -> c.nameJapaneseRomaji=d);
        propertySetterMap.put("Card type", (c,n,d) -> c.cardType=d);
        propertySetterMap.put("Property", (c,n,d) -> c.property=d);
        propertySetterMap.put("Passcode", (c,n,d) -> c.passcode=d);
        propertySetterMap.put("loreEnglish", (c,n,d) -> c.loreEnglish=d);
        propertySetterMap.put("loreFrench", (c,n,d) -> c.loreFrench=d);
        propertySetterMap.put("loreGerman", (c,n,d) -> c.loreGerman=d);
        propertySetterMap.put("loreItalian", (c,n,d) -> c.loreItalian=d);
        propertySetterMap.put("lorePortuguese", (c,n,d) -> c.lorePortuguese=d);
        propertySetterMap.put("loreSpanish", (c,n,d) -> c.loreSpanish=d);
        propertySetterMap.put("loreJapanese", (c,n,d) -> c.loreJapanese=d);
        propertySetterMap.put("loreKorean", (c,n,d) -> c.loreKorean=d);
        propertySetterMap.put("Attribute", (c,n,d) -> c.attribute=d);
        propertySetterMap.put("Types", (c,n,d) -> c.types=d);
        propertySetterMap.put("Type", (c,n,d) -> c.types=d);
        propertySetterMap.put("Rank", (c,n,d) -> c.rank=d);
        propertySetterMap.put("Level", (c,n,d) -> c.level=d);
        propertySetterMap.put("ATK / DEF", (c,n,d) -> c.atkdef=d);
        propertySetterMap.put("Materials", (c,n,d) -> c.materials=d);
        propertySetterMap.put("Material", (c,n,d) -> c.materials=d);
        propertySetterMap.put("Link Markers", (c,n,d) -> c.linkMarkers=d);
        propertySetterMap.put("ATK / LINK", (c,n,d) -> c.atklink=d);
        propertySetterMap.put("Pendulum Scale", (c,n,d) -> c.scale=d);



        Document parse = Jsoup.parse(stringResult);
        Elements select = parse.select(".cardtablerow");
//        System.out.println(select);
        WikiCardData card = new WikiCardData();

        select.forEach( e-> {
            Elements header = e.select(".cardtablerowheader");
            if(header.size()>0) {
                String heading = header.first().text();
                String value=null;

                Element firstRowData= e.select(".cardtablerowdata").first();
                Element span = firstRowData.select("span").first();
                if(span!=null) {
                    value = span.text();
                } else {
                    value = firstRowData.text();
                }

                PropertySetter propertySetter = propertySetterMap.get(heading);
                if(propertySetter!=null) {
                    propertySetter.apply(card, heading, value);
                } else {
//                    System.out.println(e.select(".cardtablerowdata").firstRowData());
//                    System.out.println(heading);
//                    System.out.println(value);
                }
            } else {
//                System.out.println(e);
                Element first = e.select(".cardtablespanrow > b").first();
                if(first!=null) {
                    String heading = first.text();
//                System.out.println(heading);
                    Elements sections = e.select(".collapsible");

                    sections.forEach(s -> {
                        if (heading.equals("Card descriptions")) {
                            String section = s.select(".navbox-title").first().text();
                            String value = s.select(".navbox-list").first().text();

                            String key = "lore" + section.trim().replace("\u00A0", "");
                            PropertySetter propertySetter = propertySetterMap.get(key);
                            if (propertySetter != null) {
                                propertySetter.apply(card, heading, value);
                            }
//                        System.out.println("-----------");
//                        System.out.println(section);
//                        System.out.println(value);
                        }
                    });
//                System.out.println(sections);
                }
            }
        });

        try {
            Elements cardtable = parse.select(".cardtable-cardimage");
            String url = cardtable.select("a").first().attr("href");
            String key = cardtable.select("img").first().attr("data-image-key");
            card.imageUrl = url;
            card.imageKey = key;
        } catch (Exception e) {

        }
        return card;
    }

}
