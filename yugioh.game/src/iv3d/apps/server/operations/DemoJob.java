package iv3d.apps.server.operations;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.api.OperationState;
import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.AbstractOperationRunner;
import com.app.server.base.server.operations.OperationLogger;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;
import com.iv3d.common.utils.ExceptionUtils;

/**
 * Created by fred on 2017/05/22.
 */
public class DemoJob extends AbstractOperationRunner<AppContext> {

    protected AppContext createContext(AppContext appContext){return appContext;}

    @Override
    protected void run(AppContext context, JobProcessInfo processInfo, OperationLogger operationLogger) {
        operationLogger.info("Running Operation for app " + context.getPackageId());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            operationLogger.error(ExceptionUtils.getStackTrace(e));
            setErrorState(e);
        }
    }

}
