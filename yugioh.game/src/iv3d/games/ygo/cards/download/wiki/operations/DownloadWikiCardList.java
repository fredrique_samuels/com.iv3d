package iv3d.games.ygo.cards.download.wiki.operations;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.AppResourceWriter;
import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.AbstractOperationRunner;
import com.app.server.base.server.operations.OperationLogger;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.ResourceWriteStatus;
import com.app.server.base.server.operations.results.WritePolicy;
import com.iv3d.common.date.UtcSystemTimer;
import com.iv3d.common.rest.HttpRestClient;
import iv3d.games.ygo.cards.download.wiki.WikiCardListUrl;
import iv3d.games.ygo.cards.download.wiki.WikiConstants;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created by fred on 2017/05/15.
 */
public class DownloadWikiCardList extends AbstractOperationRunner<WikiOperationsContext> {

    static final Logger logger = Logger.getLogger(DownloadWikiCardList.class);

    public DownloadWikiCardList() {
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getSynchroJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getXyzJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getFusionJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getRitualJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getPendulumJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getGeminiJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getTunerJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getSpiritJsonUrl(0)));

        addChildOperationRunner(new DownloadList(WikiCardListUrl.getNormalJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getNormalJsonUrl(1)));

        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(500)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(1000)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(1500)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(2000)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(2500)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getEffectJsonUrl(3000)));

        addChildOperationRunner(new DownloadList(WikiCardListUrl.getTrapJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getTrapJsonUrl(500)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getTrapJsonUrl(1000)));

        addChildOperationRunner(new DownloadList(WikiCardListUrl.getSpellJsonUrl(0)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getSpellJsonUrl(500)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getSpellJsonUrl(1000)));
        addChildOperationRunner(new DownloadList(WikiCardListUrl.getSpellJsonUrl(1500)));
    }

    @Override
    protected WikiOperationsContext createContext(AppContext appContext) {
        return new WikiOperationsContext(new AppResourceWriter(appContext));
    }

    @Override
    protected void run(WikiOperationsContext context, JobProcessInfo appJobProcessInfo, OperationLogger operationLogger) {
        setOperationName("Card List Download");
    }

    private class DownloadList extends AbstractOperationRunner<WikiOperationsContext> {

        private WikiCardListUrl cardListUrl;

        public DownloadList(WikiCardListUrl cardListUrl) {
            this.cardListUrl = cardListUrl;
        }

        @Override
        protected void run(WikiOperationsContext context, JobProcessInfo appJobProcessInfo, OperationLogger operationLogger) {
            operationLogger.info("Starting download for file " + cardListUrl);
            ResourceWriteStatus resourceWriteStatus = downloadCardListFile(context.getResourceWriter(), cardListUrl);
            setOperationStatus(resourceWriteStatus);
        }

        private ResourceWriteStatus downloadCardListFile(AppResourceWriter resourceWriter, WikiCardListUrl wikiCardListUrl) {
            return downloadCardListFile(resourceWriter, wikiCardListUrl.getOffset(), wikiCardListUrl.getUrl(), wikiCardListUrl.getPrefix());
        }

        private ResourceWriteStatus downloadCardListFile(AppResourceWriter resourceWriter, int offset, String url, String prefix) {
            String file = prefix + "-list-" + offset + ".json";
            String resource = WikiConstants.CARD_LIST_DIR + "/" + file;
            String content = null;
            try {
                content = new HttpRestClient().get(url);
            } catch (Exception e) {
                return ResourceWriteStatus.createBuilder(new File(resource)).setName(file).setError(e).build();
            }
            return resourceWriter.writePrivateResource(content, resource, WritePolicy.REPLACE, url);
        }
    }
}
