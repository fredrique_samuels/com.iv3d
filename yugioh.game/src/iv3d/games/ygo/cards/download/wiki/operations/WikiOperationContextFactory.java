package iv3d.games.ygo.cards.download.wiki.operations;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.AppResourceWriter;
import com.app.server.base.server.operations.OperationContext;
import com.app.server.base.server.operations.OperationContextFactory;

/**
 * Created by fred on 2017/05/22.
 */
public class WikiOperationContextFactory implements OperationContextFactory {
    @Override
    public OperationContext create(AppContext appContext) {
        return  new WikiOperationsContext(new AppResourceWriter(appContext));
    }
}
