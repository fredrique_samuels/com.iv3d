package iv3d.games.ygo.cards.download.wiki;

public class WikiCardData {
        public String nameEnglish;
        public String nameFrench;
        public  String nameItalian;
        public  String nameGerman;
        public String nameKorean;
        public String namePortuguese;
        public String nameSpanish;
        public String nameJapaneseKana;
        public String nameJapaneseBase;
        public String nameJapaneseRomaji;
        public String cardType;
        public String property;
        public String passcode;
        public String loreEnglish;
        public String loreFrench;
        public String loreGerman;
        public String loreItalian;
        public String lorePortuguese;
        public String loreSpanish;
        public String loreJapanese;
        public String loreKorean;
        public String attribute;
        public String types;
        public String rank;
        public String level;
        public String atkdef;
        public String materials;
        public String imageUrl;
        public String imageKey;
        public String atklink;
        public String linkMarkers;
        public String scale;
    }