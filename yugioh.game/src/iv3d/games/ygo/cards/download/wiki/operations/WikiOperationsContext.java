package iv3d.games.ygo.cards.download.wiki.operations;

import com.app.server.base.server.AppResourceWriter;
import com.app.server.base.server.operations.OperationContext;
import com.google.inject.Inject;

/**
 * Created by fred on 2017/05/15.
 */
public class WikiOperationsContext implements OperationContext {
    private AppResourceWriter resourceWriter;

    @Inject
    public WikiOperationsContext(AppResourceWriter resourceWriter) {
        this.resourceWriter = resourceWriter;
    }

    public AppResourceWriter getResourceWriter() {
        return resourceWriter;
    }
}
