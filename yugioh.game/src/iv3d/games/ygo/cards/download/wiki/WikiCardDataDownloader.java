package iv3d.games.ygo.cards.download.wiki;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.rest.HttpRestClient;
import com.iv3d.common.utils.FileUtils;
import com.iv3d.common.utils.UriUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by fred on 2017/04/23.
 */
public class WikiCardDataDownloader {
    public void run() {
        String[] extraFiles  = new String[]{
                "Decode_Talker",
                "Firewall_Dragon",
                "Gaiasaber,_the_Video_Knight",
                "Gouki_the_Great_Ogre",
                "Honeybot",
                "Link_Spider",
                "Missus_Radiant",
                "Proxy_Dragon",
                "Star_Grail_Dragon_Imduk",
                "Star_Grail_Shrine_Maiden_Eve",
                "Star_Grail_Swordsman_Aurum",
                "Star_Grail_Warrior_Ningirsu",
                "Topologic_Bomber_Dragon",
                "Trickster_Holy_Angel",
        };
        ArrayList<String> cardNames = getCardNames();
        int i = 0;

        for (String s : cardNames) {
            i++;
            downloadHtmlFile(s);
        }

        for (String s : extraFiles) {
            i++;
            downloadHtmlFile(s);
        }
    }

    private void downloadHtmlFile(String cardName) {
        String replace = UriUtils.encode(cardName.replace(" ", "_"));
        String url = "http://yugioh.wikia.com/wiki/" + replace;
        File out = new File("../cardhtml/" + replace + ".html");
        if(out.exists()){
            return;
        }
        System.out.println("Downloading... "+cardName);
        String html = downloadHtml(url);
        FileUtils.writeStringToFile(out, html);
    }

    private String downloadHtml(String url) {
        HttpRestClient httpRestClient = new HttpRestClient();
        HttpRestClient.RequestRunner requestRunner = httpRestClient.buildGet(url);
        return requestRunner.getStringResult();
    }

    private static ArrayList<String> getCardNames() {
        String s = ResourceLoader.readAsString("./master-card-list.json");
        ArrayList<String> cardNames = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode actualObj = mapper.readTree(s);
            JsonNode cards = actualObj.get("cards");
            int size = cards.size();
            cards.forEach( item -> cardNames.add(item.get("name").textValue()));
        } catch (IOException e) {
        }
        return cardNames;
    }
}
