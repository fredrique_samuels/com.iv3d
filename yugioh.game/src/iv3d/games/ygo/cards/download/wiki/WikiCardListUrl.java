package iv3d.games.ygo.cards.download.wiki;

/**
 * Created by fred on 2017/05/11.
 */
public final class WikiCardListUrl {
    private static String SYNCHRO_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3ASynchro-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String XYZ_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3AXyz-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String FUSION_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3AFusion-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String RITUAL_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3ARitual-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String EFFECT_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3AEffect-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String NORMAL_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3ANormal-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String PENDULUM_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3APendulum-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String TUNER_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BMonster-20type%3A%3ATuner-20monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String GEMINI_URL = "http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BMonster-20type%3A%3AGemini-20monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String SPIRIT_URL ="http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BMonster-20type%3A%3ASpirit-20monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=";
    private static String TRAP_URL_1 = "http://yugioh.wikia.com/wiki/Special:Ask/-5B-5BClass-201::Official-5D-5D-20-5B-5BConcept:Non-2Dmonster-20cards-5D-5D-20-5B-5BCard-20type::Trap-20Card-5D-5D/-3FEnglish-20name/-3FJapanese-20name/-3FCard-20image/-3FProperty/-3FLore/mainlabel%3D-2D/offset%3D";
    private static String TRAP_URL_2 = "/limit%3D500/prettyprint%3Dtrue/format%3Djson";
    private static String SPELL_URL_1 = "http://yugioh.wikia.com/wiki/Special:Ask/-5B-5BClass-201::Official-5D-5D-20-5B-5BConcept:Non-2Dmonster-20cards-5D-5D-20-5B-5BCard-20type::Spell-20Card-5D-5D/-3FEnglish-20name/-3FJapanese-20name/-3FCard-20image/-3FProperty/-3FLore/mainlabel%3D-2D/offset%3D";
    private static String SPELL_URL_2 = "/limit%3D500/prettyprint%3Dtrue/format%3Djson";

    private String url;
    private String prefix;
    private int offset;

    public WikiCardListUrl(String url, String prefix, int offset) {
        this.url = url;
        this.prefix = prefix;
        this.offset = offset;
    }

    public String getUrl() {
        return url;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getOffset() {
        return offset;
    }

    public static WikiCardListUrl getSynchroJsonUrl(int offset) {
        return new WikiCardListUrl(SYNCHRO_URL + offset, "synchro", offset);
    }
    public static WikiCardListUrl getXyzJsonUrl(int offset) {
        return new WikiCardListUrl(XYZ_URL + offset, "xyz", offset);
    }
    public static WikiCardListUrl getFusionJsonUrl(int offset) {
        return new WikiCardListUrl(FUSION_URL + offset, "fusion", offset);
    }
    public static WikiCardListUrl getRitualJsonUrl(int offset) {
        return new WikiCardListUrl(RITUAL_URL + offset, "ritual", offset);
    }
    public static WikiCardListUrl getEffectJsonUrl(int offset) {
        return new WikiCardListUrl(EFFECT_URL + offset, "effect", offset);
    }
    public static WikiCardListUrl getNormalJsonUrl(int offset) {
        return new WikiCardListUrl(NORMAL_URL + offset, "normal", offset);
    }
    public static WikiCardListUrl getPendulumJsonUrl(int offset) {
        return new WikiCardListUrl(PENDULUM_URL + offset, "pendulum", offset);
    }
    public static WikiCardListUrl getTunerJsonUrl(int offset) {
        return new WikiCardListUrl(TUNER_URL + offset, "tuner", offset);
    }
    public static WikiCardListUrl getGeminiJsonUrl(int offset) {
        return new WikiCardListUrl(GEMINI_URL + offset, "gemini", offset);
    }
    public static WikiCardListUrl getSpiritJsonUrl(int offset) {
        return new WikiCardListUrl(SPIRIT_URL + offset, "spirit", offset);
    }
    public static WikiCardListUrl getTrapJsonUrl(int offset) {
        return new WikiCardListUrl(TRAP_URL_1 + offset + TRAP_URL_2, "trap", offset);
    }
    public static WikiCardListUrl getSpellJsonUrl(int offset) {
        return new WikiCardListUrl(SPELL_URL_1 + offset + SPELL_URL_2, "spell", offset);
    }
}
