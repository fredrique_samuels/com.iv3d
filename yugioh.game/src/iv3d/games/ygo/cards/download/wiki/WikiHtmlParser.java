package iv3d.games.ygo.cards.download.wiki;

import iv3d.yugioh.data.DownloadData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fred on 2017/05/15.
 */
public class WikiHtmlParser {
    interface PropertySetter {
        void apply(WikiCardData c, String name, String data);
    }

    public WikiCardData parser(String html) {
        Map<String, PropertySetter> propertySetterMap = new HashMap<>();
        propertySetterMap.put("English", (c,n,d) -> c.nameEnglish=d);
        propertySetterMap.put("French", (c,n,d) -> c.nameFrench=d);
        propertySetterMap.put("German", (c,n,d) -> c.nameGerman=d);
        propertySetterMap.put("Italian", (c,n,d) -> c.nameItalian=d);
        propertySetterMap.put("Korean", (c,n,d) -> c.nameKorean=d);
        propertySetterMap.put("Portuguese", (c,n,d) -> c.namePortuguese=d);
        propertySetterMap.put("Spanish", (c,n,d) -> c.nameSpanish=d);
        propertySetterMap.put("Japanese (kana)", (c,n,d) -> c.nameJapaneseKana=d);
        propertySetterMap.put("Japanese (base)", (c,n,d) -> c.nameJapaneseBase=d);
        propertySetterMap.put("Japanese (rōmaji)", (c,n,d) -> c.nameJapaneseRomaji=d);
        propertySetterMap.put("Card type", (c,n,d) -> c.cardType=d);
        propertySetterMap.put("Property", (c,n,d) -> c.property=d);
        propertySetterMap.put("Passcode", (c,n,d) -> c.passcode=d);
        propertySetterMap.put("loreEnglish", (c,n,d) -> c.loreEnglish=d);
        propertySetterMap.put("loreFrench", (c,n,d) -> c.loreFrench=d);
        propertySetterMap.put("loreGerman", (c,n,d) -> c.loreGerman=d);
        propertySetterMap.put("loreItalian", (c,n,d) -> c.loreItalian=d);
        propertySetterMap.put("lorePortuguese", (c,n,d) -> c.lorePortuguese=d);
        propertySetterMap.put("loreSpanish", (c,n,d) -> c.loreSpanish=d);
        propertySetterMap.put("loreJapanese", (c,n,d) -> c.loreJapanese=d);
        propertySetterMap.put("loreKorean", (c,n,d) -> c.loreKorean=d);
        propertySetterMap.put("Attribute", (c,n,d) -> c.attribute=d);
        propertySetterMap.put("Types", (c,n,d) -> c.types=d);
        propertySetterMap.put("Type", (c,n,d) -> c.types=d);
        propertySetterMap.put("Rank", (c,n,d) -> c.rank=d);
        propertySetterMap.put("Level", (c,n,d) -> c.level=d);
        propertySetterMap.put("ATK / DEF", (c,n,d) -> c.atkdef=d);
        propertySetterMap.put("Materials", (c,n,d) -> c.materials=d);
        propertySetterMap.put("Material", (c,n,d) -> c.materials=d);
        propertySetterMap.put("Link Markers", (c,n,d) -> c.linkMarkers=d);
        propertySetterMap.put("ATK / LINK", (c,n,d) -> c.atklink=d);
        propertySetterMap.put("Pendulum Scale", (c,n,d) -> c.scale=d);



        Document parse = Jsoup.parse(html);
        Elements select = parse.select(".cardtablerow");
//        System.out.println(select);
        WikiCardData card = new WikiCardData();

        select.forEach( e-> {
            Elements header = e.select(".cardtablerowheader");
            if(header.size()>0) {
                String heading = header.first().text();
                String value=null;

                Element firstRowData= e.select(".cardtablerowdata").first();
                Element span = firstRowData.select("span").first();
                if(span!=null) {
                    value = span.text();
                } else {
                    value = firstRowData.text();
                }

                PropertySetter propertySetter = propertySetterMap.get(heading);
                if(propertySetter!=null) {
                    propertySetter.apply(card, heading, value);
                } else {
//                    System.out.println(e.select(".cardtablerowdata").firstRowData());
//                    System.out.println(heading);
//                    System.out.println(value);
                }
            } else {
//                System.out.println(e);
                Element first = e.select(".cardtablespanrow > b").first();
                if(first!=null) {
                    String heading = first.text();
//                System.out.println(heading);
                    Elements sections = e.select(".collapsible");

                    sections.forEach(s -> {
                        if (heading.equals("Card descriptions")) {
                            String section = s.select(".navbox-title").first().text();
                            String value = s.select(".navbox-list").first().text();

                            String key = "lore" + section.trim().replace("\u00A0", "");
                            PropertySetter propertySetter = propertySetterMap.get(key);
                            if (propertySetter != null) {
                                propertySetter.apply(card, heading, value);
                            }
//                        System.out.println("-----------");
//                        System.out.println(section);
//                        System.out.println(value);
                        }
                    });
//                System.out.println(sections);
                }
            }
        });

        Elements cardtable = parse.select(".cardtable-cardimage");
        String url = cardtable.select("a").first().attr("href");
        String key = cardtable.select("img").first().attr("data-image-key");
        card.imageUrl = url;
        card.imageKey = key;
        
        return card;
    }
}
