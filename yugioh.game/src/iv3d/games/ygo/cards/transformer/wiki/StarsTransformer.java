package iv3d.games.ygo.cards.transformer.wiki;

import iv3d.games.ygo.cards.download.wiki.WikiCardData;

/**
 * Created by fred on 2017/05/10.
 */
public class StarsTransformer {
    public Integer get(WikiCardData data) {
        if(data.level!=null && !data.level.isEmpty()) {
            return Integer.valueOf(data.level);
        }
        if(data.rank!=null && !data.rank.isEmpty()) {
            return Integer.valueOf(data.rank);
        }
        return null;
    }
}
