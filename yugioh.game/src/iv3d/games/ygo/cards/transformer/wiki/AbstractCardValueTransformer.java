package iv3d.games.ygo.cards.transformer.wiki;

/**
 * Created by fred on 2017/05/10.
 */
public class AbstractCardValueTransformer {

    protected String getValueOrNull(String value) {
        if(hasValue(value)) {
            return value;
        }
        return null;
    }

    protected boolean hasValue(String value) {
        return value!=null && !value.isEmpty();
    }

    protected Integer getIntegerOrNull(String value) {
        if(hasValue(value)) {
            return Integer.valueOf(value);
        }
        return null;
    }
}
