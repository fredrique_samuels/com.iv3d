package iv3d.games.ygo.cards.transformer.wiki;

import iv3d.games.ygo.cards.download.wiki.WikiCardData;

/**
 * Created by fred on 2017/05/10.
 */
public class DefTransformer extends AbstractCardValueTransformer {
    public Integer get(WikiCardData cardData) {
        if(hasValue(cardData.atkdef)) {
            return Integer.valueOf(cardData.atkdef.split("/")[1].trim());
        }
        return null;
    }
}
