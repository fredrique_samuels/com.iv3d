package iv3d.games.ygo.cards.transformer.wiki;

import iv3d.games.ygo.cards.download.wiki.WikiCardData;

/**
 * Created by fred on 2017/05/10.
 */
public class AtkTransformer extends AbstractCardValueTransformer {
    public Integer get(WikiCardData cardData) {
        if(hasValue(cardData.atkdef)) {
            return Integer.valueOf(cardData.atkdef.split("/")[0].trim());
        }
        if(hasValue(cardData.atklink)) {
            return Integer.valueOf(cardData.atklink.split("/")[0].trim());
        }
        return null;
    }
}
