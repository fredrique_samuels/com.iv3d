package iv3d.games.ygo.cards.transformer.wiki;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/11.
 */
public class CardValueTransformer extends AbstractCardValueTransformer {
    private String value;

    public CardValueTransformer(String value) {
        this.value = value;
    }

    public final String getString(){ return getValueOrNull(value);}

    public final Integer getInteger() {
        if(hasValue(value)) {
            return Integer.valueOf(value);
        }
        return null;
    }

    public final String[] getStringArray(String separator) {
        if(hasValue(value)) {
            if(value.contains(separator)) {
                return Arrays.asList(value.split(separator)).stream()
                        .map(s -> s.trim())
                        .collect(Collectors.toList())
                        .toArray(new String[]{});
            }
            return new String[]{value};
        }
        return null;
    }
}
