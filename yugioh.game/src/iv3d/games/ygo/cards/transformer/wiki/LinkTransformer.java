package iv3d.games.ygo.cards.transformer.wiki;

import iv3d.games.ygo.cards.download.wiki.WikiCardData;

/**
 * Created by fred on 2017/05/10.
 */
public class LinkTransformer extends AbstractCardValueTransformer {
    public Integer get(WikiCardData cardData) {
        if(hasValue(cardData.atklink)) {
            return Integer.valueOf(cardData.atklink.split("/")[1].trim());
        }
        return null;
    }
}
