package iv3d.games.ygo.cards;

import iv3d.games.ygo.cards.download.wiki.WikiCardData;
import iv3d.games.ygo.cards.transformer.wiki.*;

/**
 * Created by fred on 2017/05/10.
 */
public class Card {
    public static final String ATTRIBUTE_WIND = "WIND";
    public static final String ATTRIBUTE_WATER = "WATER";
    public static final String ATTRIBUTE_FIRE = "FIRE";
    public static final String ATTRIBUTE_LIGHT = "LIGHT";
    public static final String ATTRIBUTE_DARK = "DARK";
    public static final String ATTRIBUTE_EARTH = "EARTH";
    public static final String ATTRIBUTE_DIVINE = "DIVINE";

    public static final String TYPE_SPELLCASTER = "Spellcaster";
    public static final String TYPE_DRAGON = "Dragon";
    public static final String TYPE_ZOMBIE = "Zombie";
    public static final String TYPE_WARRIOR = "Warrior";
    public static final String TYPE_BEAST_WARRIOR = "Beast-Warrior";
    public static final String TYPE_BEAST = "Beast";
    public static final String TYPE_WINGED_BEAST = "Winged Beast";
    public static final String TYPE_FIEND = "Fiend";
    public static final String TYPE_FAIRY = "Fairy";
    public static final String TYPE_INSECT = "Insect";
    public static final String TYPE_DINOSAUR = "Dinosaur";
    public static final String TYPE_REPTILE = "Reptile";
    public static final String TYPE_FISH = "Fish";
    public static final String TYPE_SEA_SERPENT = "Sea Serpent";
    public static final String TYPE_AQUA = "Aqua";
    public static final String TYPE_PYRO = "Pyro";
    public static final String TYPE_THUNDER = "Thunder";
    public static final String TYPE_ROCK = "Rock";
    public static final String TYPE_PLANT = "Plant";
    public static final String TYPE_MACHINE = "Machine";
    public static final String TYPE_PSYCHIC = "Psychic";
    public static final String TYPE_DIVINE_BEAST = "Divine-Beast";
    public static final String TYPE_WYRM = "Wyrm";
    public static final String TYPE_CYBERSE = "Cyberse";

    public static final String TYPE_NORMAL = "Normal";
    public static final String TYPE_EFFECT = "Effect";
    public static final String TYPE_RITUAL = "Ritual";
    public static final String TYPE_FUSION = "Fusion";
    public static final String TYPE_SYNCHRO = "Synchro";
    public static final String TYPE_XYZ = "Xyz";
    public static final String TYPE_PENDULUM = "Pendulum";
    public static final String TYPE_LINK = "Link";
    public static final String TYPE_TOON = "Toon";
    public static final String TYPE_SPIRIT = "Spirit";
    public static final String TYPE_UNION = "Union";
    public static final String TYPE_GEMINI = "Gemini";
    public static final String TYPE_TUNER = "Tuner";
    public static final String TYPE_FLIP = "Flip";

    public static final String LINK_MARKER_LEFT = "Left";
    public static final String LINK_MARKER_TOP = "Top";
    public static final String LINK_MARKER_BOTTOM = "Bottom";
    public static final String LINK_MARKER_RIGHT = "Right";
    public static final String LINK_MARKER_BOTTOM_RIGHT = "Bottom-Right";
    public static final String LINK_MARKER_BOTTOM_LEFT = "Bottom-Left";
    public static final String LINK_MARKER_TOP_LEFT = "Top-Left";
    public static final String LINK_MARKER_TOP_RIGHT = "Top-Right";

    private final String imageKey;
    private final String id;
    private final Integer stars;
    private final String name;
    private final String nameFrench;
    private final  String nameItalian;
    private final  String nameGerman;
    private final String nameKorean;
    private final String namePortuguese;
    private final String nameSpanish;
    private final String nameJapaneseKana;
    private final String nameJapaneseBase;
    private final String nameJapaneseRomaji;
    private final String attribute;
    private final String lore;
    private final String loreFrench;
    private final String loreGerman;
    private final String loreItalian;
    private final String lorePortuguese;
    private final String loreSpanish;
    private final String loreJapanese;
    private final String loreKorean;
    private final Integer atk;
    private final Integer def;
    private final Integer link;
    private final Integer scale;
    private final String[] types;
    private final String property;
    private final String[] linkMarkers;
    private final String passcode;
    private final String imageUrl;

    public Card(WikiCardData cardData) {
        this.id =  new CardValueTransformer(cardData.nameEnglish).getString();
        this.stars = new StarsTransformer().get(cardData);
        this.name = new CardValueTransformer(cardData.nameEnglish).getString();
        this.nameFrench = new CardValueTransformer(cardData.nameFrench).getString();
        this.nameItalian = new CardValueTransformer(cardData.nameItalian).getString();
        this.nameGerman = new CardValueTransformer(cardData.nameGerman).getString();
        this.nameKorean = new CardValueTransformer(cardData.nameKorean).getString();
        this.namePortuguese = new CardValueTransformer(cardData.namePortuguese).getString();
        this.nameSpanish = new CardValueTransformer(cardData.nameSpanish).getString();
        this.nameJapaneseKana = new CardValueTransformer(cardData.nameJapaneseKana).getString();
        this.nameJapaneseBase = new CardValueTransformer(cardData.nameJapaneseBase).getString();
        this.nameJapaneseRomaji = new CardValueTransformer(cardData.nameJapaneseRomaji).getString();
        this.attribute = new CardValueTransformer(cardData.attribute).getString();
        this.lore = new CardValueTransformer(cardData.loreEnglish).getString();
        this.loreFrench = new CardValueTransformer(cardData.loreFrench).getString();
        this.loreGerman = new CardValueTransformer(cardData.loreGerman).getString();
        this.loreItalian = new CardValueTransformer(cardData.loreItalian).getString();
        this.lorePortuguese = new CardValueTransformer(cardData.lorePortuguese).getString();
        this.loreSpanish = new CardValueTransformer(cardData.loreSpanish).getString();
        this.loreJapanese = new CardValueTransformer(cardData.loreJapanese).getString();
        this.loreKorean = new CardValueTransformer(cardData.loreKorean).getString();
        this.atk = new AtkTransformer().get(cardData);
        this.def = new DefTransformer().get(cardData);
        this.link = new LinkTransformer().get(cardData);
        this.scale = new CardValueTransformer(cardData.scale).getInteger();
        this.types = new CardValueTransformer(cardData.types).getStringArray("/");
        this.property = new CardValueTransformer(cardData.property).getString();
        this.linkMarkers = new CardValueTransformer(cardData.linkMarkers).getStringArray(",");
        this.passcode = new CardValueTransformer(cardData.passcode).getString();
        this.imageUrl = new CardValueTransformer(cardData.imageUrl).getString();
        this.imageKey = new CardValueTransformer(cardData.imageKey).getString();
    }

    public String getImageKey() {
        return imageKey;
    }

    public String getId() {
        return id;
    }

    public Integer getStars() {
        return stars;
    }

    public String getName() {
        return name;
    }

    public String getNameFrench() {
        return nameFrench;
    }

    public String getNameItalian() {
        return nameItalian;
    }

    public String getNameGerman() {
        return nameGerman;
    }

    public String getNameKorean() {
        return nameKorean;
    }

    public String getNamePortuguese() {
        return namePortuguese;
    }

    public String getNameSpanish() {
        return nameSpanish;
    }

    public String getNameJapaneseKana() {
        return nameJapaneseKana;
    }

    public String getNameJapaneseBase() {
        return nameJapaneseBase;
    }

    public String getNameJapaneseRomaji() {
        return nameJapaneseRomaji;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getLore() {
        return lore;
    }

    public String getLoreFrench() {
        return loreFrench;
    }

    public String getLoreGerman() {
        return loreGerman;
    }

    public String getLoreItalian() {
        return loreItalian;
    }

    public String getLorePortuguese() {
        return lorePortuguese;
    }

    public String getLoreSpanish() {
        return loreSpanish;
    }

    public String getLoreJapanese() {
        return loreJapanese;
    }

    public String getLoreKorean() {
        return loreKorean;
    }

    public Integer getAtk() {
        return atk;
    }

    public Integer getDef() {
        return def;
    }

    public Integer getLink() {
        return link;
    }

    public Integer getScale() {
        return scale;
    }

    public String[] getTypes() {
        return types;
    }

    public String getProperty() {
        return property;
    }

    public String[] getLinkMarkers() {
        return linkMarkers;
    }

    public String getPasscode() {
        return passcode;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    //    hasMarker(m){return this.getLinkMarkers().indexOf(m)>-1}
//    hasLeftLinkMarker(){return this.hasMarker(LINK_MARKER_LEFT)}
//    hasRightLinkMarker(){return this.hasMarker(LINK_MARKER_RIGHT)}
//    hasTopLinkMarker(){return this.hasMarker(LINK_MARKER_TOP)}
//    hasTopLeftLinkMarker(){return this.hasMarker(LINK_MARKER_TOP_LEFT)}
//    hasTopRightLinkMarker(){return this.hasMarker(LINK_MARKER_TOP_RIGHT)}
//    hasBottomLinkMarker(){return this.hasMarker(LINK_MARKER_BOTTOM)}
//    hasBottomLeftLinkMarker(){return this.hasMarker(LINK_MARKER_BOTTOM_LEFT)}
//    hasBottomRightLinkMarker(){return this.hasMarker(LINK_MARKER_BOTTOM_RIGHT)}
//
//    getCardType(){return new CardTypeTransformer().get(this.cardData)}
//    isTrap(){return this.getCardType()==CARD_TYPE_TRAP}
//    isSpell(){return this.getCardType()==CARD_TYPE_SPELL}
//    isMonster(){return this.getCardType()==CARD_TYPE_MONSTER}
//
//    isSynchroMonster(){return this.isType(TYPE_SYNCHRO)}
//    isXyzMonster(){return this.isType(TYPE_XYZ)}
//    isLinkMonster(){return this.isType(TYPE_LINK)}
//    isFusionMonster(){return this.isType(TYPE_FUSION)}
//    isRitualMonster(){return this.isType(TYPE_RITUAL)}
//    isEffectMonster(){return this.isType(TYPE_EFFECT)}
//    isNormalMonster(){return this.isMonster() && !(this.isEffectMonster() || this.isRitualMonster() || this.isFusionMonster() || this.isXyzMonster() || this.isSynchroMonster() || this.isLinkMonster() )}
//    isPendulumMonster(){return this.isType(TYPE_PENDULUM)}
//    isExtraDeckCard() {return this.isLinkMonster() || this.isFusionMonster() || this.isXyzMonster() || this.isSynchroMonster() }
//
//    isType(t){return this.getTypes().indexOf(t)>-1}
//
//    isDarkAttribute(){return this.getAttribute()==ATTRIBUTE_DARK}
//    isLightAttribute(){return this.getAttribute()==ATTRIBUTE_LIGHT}
//    isWindAttribute(){return this.getAttribute()==ATTRIBUTE_WIND}
//    isWaterAttribute(){return this.getAttribute()==ATTRIBUTE_WATER}
//    isFireAttribute(){return this.getAttribute()==ATTRIBUTE_FIRE}
//    isEarthAttribute(){return this.getAttribute()==ATTRIBUTE_EARTH}
//    isDivineAttribute(){return this.getAttribute()==ATTRIBUTE_DIVINE}
//
//
//    isDragonType(){return this.isType(TYPE_DRAGON)}
//    isSpellcasterType(){return this.isType(TYPE_SPELLCASTER)}
//    isZombieType(){return this.isType(TYPE_ZOMBIE)}
//    isWarriorType(){return this.isType(TYPE_WARRIOR)}
//    isBeastWarriorType(){return this.isType(TYPE_BEAST_WARRIOR)}
//    isBeastType(){return this.isType(TYPE_BEAST)}
//    isWingedBeastType(){return this.isType(TYPE_WINGED_BEAST)}
//    isFiendType(){return this.isType(TYPE_FIEND)}
//    isFairyType(){return this.isType(TYPE_FAIRY)}
//    isInsectType(){return this.isType(TYPE_INSECT)}
//    isDinosaurType(){return this.isType(TYPE_DINOSAUR)}
//    isReptileType(){return this.isType(TYPE_REPTILE)}
//    isFishType(){return this.isType(TYPE_FISH)}
//    isSeaSerpentType(){return this.isType(TYPE_SEA_SERPENT)}
//    isAquaType(){return this.isType(TYPE_AQUA)}
//    isPyroType(){return this.isType(TYPE_PYRO)}
//    isThunderType(){return this.isType(TYPE_THUNDER)}
//    isPlantType(){return this.isType(TYPE_PLANT)}
//    isRockType(){return this.isType(TYPE_ROCK)}
//    isMachineType(){return this.isType(TYPE_MACHINE)}
//    isPsychicType(){return this.isType(TYPE_PSYCHIC)}
//    isDivineBeastType(){return this.isType(TYPE_DIVINE_BEAST)}
//    isWyrmType(){return this.isType(TYPE_WYRM)}
//    isCyberseType(){return this.isType(TYPE_CYBERSE)}
//
//    isEquipCard(){return this.getProperty()==PROPERTY_EQUIP}
//    isContinuousCard(){return this.getProperty()==PROPERTY_CONTINUOUS}
//    isFieldCard(){return this.getProperty()==PROPERTY_FIELD}
//    isRitualCard(){return this.getProperty()==PROPERTY_RITUAL}
//    isQuickPlayCard(){return this.getProperty()==PROPERTY_QUICK_PLAY}
//    isCounterCard(){return this.getProperty()==PROPERTY_COUNTER}
//    isNormalCard(){return this.getProperty()==PROPERTY_NORMAL}
//
//    isToonMonster(){return this.isType(TYPE_TOON)}
//    isSpiritMonster(){return this.isType(TYPE_SPIRIT)}
//    isUnionMonster(){return this.isType(TYPE_UNION)}
//    isGeminiMonster(){return this.isType(TYPE_GEMINI)}
//    isTunerMonster(){return this.isType(TYPE_TUNER)}
//    isFlipMonster(){return this.isType(TYPE_FLIP)}
//
//    getImageUrl(){return new ImageUrlTransformer().get(this.cardData)}
//    getImageKey(){return new ImageKeyTransformer().get(this.cardData)}
//
//    //summary getters
//    getTypeSummaryString() {
//        if(this.isSpell() || this.isTrap()) {
//            return this.getCardType() + "/" + this.getProperty()
//        }
//
//        const attr = this.getAttribute()
//        const types = this.getTypes()
//        let typesString = attr
//        for (let tIndex in types) {
//            typesString+="/"+types[tIndex]
//        }
//        return typesString
//    }
//
//    getAtkDefSummaryString() {
//        const atk = this.getAtk()
//        if(atk) return atk + "/" + (this.isLinkMonster()?this.getLink():this.getDef())
//        return null
//    }
}
