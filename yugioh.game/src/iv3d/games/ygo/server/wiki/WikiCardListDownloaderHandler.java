package iv3d.games.ygo.server.wiki;

import com.app.server.base.server.AppResourceWriter;
import com.app.server.base.server.AppContext;
import com.app.server.base.server.operations.OperationRunner;
import com.app.server.base.server.operations.results.*;
import com.app.server.base.server.web.RequestHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.date.UtcSystemTimer;
import com.iv3d.common.rest.HttpRestClient;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.common.utils.UriUtils;
import iv3d.games.ygo.cards.download.wiki.WikiCardData;
import iv3d.games.ygo.cards.download.wiki.WikiCardListUrl;
import iv3d.games.ygo.cards.download.wiki.WikiCardsContainer;
import iv3d.games.ygo.cards.download.wiki.WikiHtmlParser;
import iv3d.games.ygo.cards.download.wiki.operations.DownloadWikiCardList;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.*;

/**
 * Created by fred on 2017/05/11.
 */
public class WikiCardListDownloaderHandler extends RequestHandler {

    private static Logger logger = Logger.getLogger(WikiCardListDownloaderHandler.class);

    private static String CARD_LIST_DIR = "cardlists";
    private static String HTML_FILES_DIR = "cardhtml";

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        UtcSystemClock utcSystemClock = new UtcSystemClock();
        UtcDate now = utcSystemClock.now();
        logger.info("Starting Card Data Update Process" + now.toIso8601String());

        getAppContext().getTasks();

        OperationRunner downloadWikiCardList = new DownloadWikiCardList();
        downloadWikiCardList.start(getAppContext(), null);
//        OperationStatus result = downloadWikiCardList.getResult();
//        OperationStats stats = result.getStats();

//
//        logger.info("====================");
//        logger.info("WIKI CARD LIST DATA");
//        for (Map.Entry<String, String> entry:stats.getAll().entrySet()) {
//            logger.info(entry.getKey() + " : " + entry.getValue());
//        }

//        List<OperationSummary> allWikiIndexFiles = writeAllWikiIndexFiles();
//        logWikiIndexOperations(allWikiIndexFiles);

//        List<OperationStatus> writeAllHtmlFiles = writeAllHtmlFiles();
//        logWriteAllHtmlFiles(writeAllHtmlFiles);
//
//        OperationStatus writeWikiMasterFile = writeWikiCardMasterFile();
//        OperationStats stats = writeWikiMasterFile.getStats();
//        Map<String, String> all = stats.getAll();
//        logger.info("====================");
//        logger.info("MASTER WIKI CARD DATA");
//        for (Map.Entry<String, String> entry:all.entrySet()) {
//            logger.info(entry.getKey() + " : " + entry.getValue());
//        }




//        String pathname = "SpeedroidTerrortop-SP17-EN-C-1E.png";
//        String imageUrl = "https://vignette4.wikia.nocookie.net/yugioh/images/b/b9/SpeedroidTerrortop-SP17-EN-C-1E.png/revision/latest/scale-to-width-down/300?cb=20170310171721";
//        AppResourceWriter resourceWriter = getResourceWriter();
//        ImageDownloadSummary downloadSummary = resourceWriter.writePrivateImageResource(WritePolicy.SKIP_IF_EXISTS, imageUrl, pathname);



//        writeLowResImages();

        returnOkResponse(response);
    }

    private OperationStatus writeWikiCardMasterFile() {
        WikiHtmlParser wikiHtmlParser = new WikiHtmlParser();
        List<File> wikiFiles = getWikiFiles();
        List<OperationStatus> operationSummaries = new ArrayList<>();
        List<WikiCardData> cardData = new ArrayList<>();
        UtcSystemTimer timer = new UtcSystemTimer();

        String id = "YU-GI-OH CARD WIKI PAGE";
        long fileCount = 0;;
        for (File file : wikiFiles) {
            UtcSystemTimer parseTimer = new UtcSystemTimer();
            try {
                WikiCardData data = wikiHtmlParser.parser(ResourceLoader.readAsString(file));
                cardData.add(data);
                operationSummaries.add(new ParseStatus(parseTimer.stop(), id, file.getPath(), null));
            } catch (Exception error) {
                operationSummaries.add(new ParseStatus(parseTimer.stop(), id, file.getPath(), error));
            }
            fileCount++;
            if (fileCount%100==0) {
                logger.info("Parsed " + fileCount + " of " + wikiFiles.size());
            }
        }

        AppResourceWriter resourceWriter = getResourceWriter();
        File masterJson = resourceWriter.getPrivateResource("master-wiki-data.json");

        boolean createdOnWrite = !masterJson.exists();
        try {
            JsonUtils.writeToFile(masterJson, new WikiCardsContainer(cardData));
        } catch (Exception error) {
            return ResourceWriteStatus.createBuilder(masterJson, timer.stop())
                    .setError(error)
                    .setChildren(operationSummaries)
                    .build();
        }

        return ResourceWriteStatus.createBuilder(masterJson, timer.stop())
                .setSuccess(WritePolicy.REPLACE, createdOnWrite)
                .setChildren(operationSummaries)
                .build();
    }

    private List<File> getWikiFiles() {
        ArrayList<String> cardNames = new ArrayList<>();
        AppContext appContext = this.getAppContext();
        File privateResource = appContext.getPrivateResourceFile(HTML_FILES_DIR);
        File[] files = privateResource.listFiles();
        return Arrays.asList(files);
    }

    private void logWriteAllHtmlFiles(List<OperationStatus> operationSummaries) {
        logger.info("====================");
        logger.info("WRITE HTML FILES SUMMARY");
        logger.info("Operations : " + operationSummaries.size() );
        logger.info("Errors : " + OperationStatus.errorCount(operationSummaries) );
        logger.info("Success : " + OperationStatus.successCount(operationSummaries)  );
        logger.info("New Files : " + ResourceWriteStatus.newFilesCount(operationSummaries));
        logger.info("Http Errors : " + OperationStatus.errorCount(operationSummaries, HttpRequestStatus.OPERATION_TYPE));
    }

    private void logWikiIndexOperations(List<OperationStatus> operationSummaries) {
        logger.info("====================");
        logger.info("WIKI WRITE SUMMARY");
        logger.info("Operations : " + operationSummaries.size() );
        logger.info("Errors : " + OperationStatus.errorCount(operationSummaries) );
        logger.info("Success : " + OperationStatus.successCount(operationSummaries)  );
        logger.info("New Files : " + ResourceWriteStatus.newFilesCount(operationSummaries));
        logger.info("Http Errors : " + OperationStatus.errorCount(operationSummaries, HttpRequestStatus.OPERATION_TYPE));
    }

    private void writeLowResImages() {

    }

    private ArrayList<OperationStatus> writeAllHtmlFiles() {
        ArrayList<String> cardNames = loadCardNames();
        logger.info("Loaded card count : "+cardNames.size());

        ArrayList<OperationStatus> summaries = new ArrayList<>();
        for (String cn: cardNames) {
            summaries.add(downloadHtmlFile(cn));
        }
        return summaries;
    }

    private ArrayList<String> loadCardNames() {
        ArrayList<String> cardNames = new ArrayList<>();
        AppContext appContext = this.getAppContext();
        File privateResource = appContext.getPrivateResourceFile(CARD_LIST_DIR);
        File[] files = privateResource.listFiles();
        for (File f : files) {
            JsonNode rootNode = JsonUtils.parseToTreeModel(f);
            JsonNode carNode = rootNode.path("results");
            Iterator<Map.Entry<String, JsonNode>> fields = carNode.fields();
            while(fields.hasNext()){
                cardNames.add(fields.next().getKey());
            }
        }

        String[] extraFiles  = new String[]{
                "Decode_Talker",
                "Firewall_Dragon",
                "Gaiasaber,_the_Video_Knight",
                "Gouki_the_Great_Ogre",
                "Honeybot",
                "Link_Spider",
                "Missus_Radiant",
                "Proxy_Dragon",
                "Star_Grail_Dragon_Imduk",
                "Star_Grail_Shrine_Maiden_Eve",
                "Star_Grail_Swordsman_Aurum",
                "Star_Grail_Warrior_Ningirsu",
                "Topologic_Bomber_Dragon",
                "Trickster_Holy_Angel",
        };

        cardNames.addAll(Arrays.asList(extraFiles));

        return cardNames;
    }

    private OperationStatus downloadHtmlFile(String cardName) {
        String formattedName = UriUtils.encode(cardName.replace(" ", "_"));
        String url = "http://yugioh.wikia.com/wiki/" + formattedName;
        String pathname = HTML_FILES_DIR + "/" + formattedName + ".html";

        AppResourceWriter resourceWriter = getResourceWriter();
        File file = resourceWriter.getPrivateResource(pathname);

        if(file.exists()){
            return ResourceWriteStatus.createBuilder(file).setSkipped(WritePolicy.SKIP_IF_EXISTS).build();
        }

        logger.info("Downloading Html for : " + cardName);

        String html= null;
        HttpRestClient httpRestClient = new HttpRestClient();
        HttpRestClient.RequestRunner requestRunner = httpRestClient.buildGet(url);
        try {
            html = requestRunner.getStringResult();
        } catch (Exception error) {
            return HttpRequestStatus.createBuilder(requestRunner.getRestRequest()).setError(error).build();
        }

        return resourceWriter.writePrivateResource(html, pathname, WritePolicy.SKIP_IF_EXISTS);
    }

    private List<OperationStatus> writeAllWikiIndexFiles() {
        List<OperationStatus> downloadSummary = new ArrayList<>();

        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getSynchroJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getXyzJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getFusionJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getRitualJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getPendulumJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getGeminiJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getTunerJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getSpiritJsonUrl(0)));

        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getNormalJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getNormalJsonUrl(1)));

        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(500)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(1000)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(1500)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(2000)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(2500)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getEffectJsonUrl(3000)));

        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getTrapJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getTrapJsonUrl(500)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getTrapJsonUrl(1000)));

        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getSpellJsonUrl(0)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getSpellJsonUrl(500)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getSpellJsonUrl(1000)));
        downloadSummary.add(downloadCardListFile(WikiCardListUrl.getSpellJsonUrl(1500)));
        return downloadSummary;
    }

    private ResourceWriteStatus downloadCardListFile(WikiCardListUrl wikiCardListUrl) {
        return downloadCardListFile(wikiCardListUrl.getOffset(), wikiCardListUrl.getUrl(), wikiCardListUrl.getPrefix());
    }

    private ResourceWriteStatus downloadCardListFile(int offset, String url, String prefix) {
        String content = new HttpRestClient().get(url);
        String resource = CARD_LIST_DIR + "/" + prefix + "-list-" + offset + ".json";
        return getResourceWriter().writePrivateResource(content, resource, WritePolicy.REPLACE);
    }
}
