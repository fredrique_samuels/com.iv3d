package iv3d.games.ygo.server.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.iv3d.common.core.ResourceLoader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by fred on 2016/11/03.
 */
public class CardImageHandler extends RequestHandler {

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String image = getRequiredParameter("image");
        downloadFile(request, response, ResourceLoader.getFile("../ygodata/card_images_lr/"+image));
    }
}
