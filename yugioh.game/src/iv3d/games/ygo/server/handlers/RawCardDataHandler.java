package iv3d.games.ygo.server.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.utils.JsonUtils;
import iv3d.games.ygo.cards.download.wiki.WikiCardsContainer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * Created by fred on 2017/05/10.
 */
public class RawCardDataHandler extends RequestHandler {
    WikiCardsContainer cardsContainer;

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if(cardsContainer==null) {
            cardsContainer = JsonUtils.readFromFile("../master-list.json", WikiCardsContainer.class);
        }

        String ids = getRequiredParameter("cardIds");
        String[] strings = JsonUtils.readFromString(ids, String[].class);
        String json = JsonUtils.writeToString(cardsContainer.getCardsByName(strings));
        File privateResource = getAppContext().getPrivateResourceFile("test.txt");
        String s = ResourceLoader.readAsString(privateResource);
        ResourceLoader.writeString("This is data", getAppContext().getPrivateResourceFile("test2.txt"));
        returnJson(json);
    }
}
