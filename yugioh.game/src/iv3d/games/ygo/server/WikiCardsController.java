package iv3d.games.ygo.server;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.google.inject.Inject;
import iv3d.games.ygo.server.wiki.WikiCardListDownloaderHandler;

/**
 * Created by fred on 2017/05/11.
 */
public class WikiCardsController extends AppWebController {

    @Inject
    private WikiCardListDownloaderHandler wikiCardListDownloaderHandler;

    @Inject
    public WikiCardsController(LocalAppSettingsManager appSettingsManager) {
        super(appSettingsManager.get("cardserver"));
    }

    @Override
    public void configure() {
        bindPathHandler("/carddata/wiki/download/cardlist", wikiCardListDownloaderHandler);
    }
}
