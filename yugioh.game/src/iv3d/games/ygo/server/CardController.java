package iv3d.games.ygo.server;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.google.inject.Inject;
import iv3d.games.ygo.server.handlers.CardImageHandler;
import iv3d.games.ygo.server.handlers.RawCardDataHandler;

/**
 * Created by fred on 2016/11/03.
 */
public class CardController extends AppWebController {

    @Inject
    private CardImageHandler cardPageHandler;

    @Inject
    private RawCardDataHandler rawCardDataHandler;

    @Inject
    public CardController(LocalAppSettingsManager appSettingsManager) {
        super(appSettingsManager.get("cardserver"));

    }

    @Override
    public void configure() {
        bindPathHandler("/", cardPageHandler);
        bindPathHandler("/carddata/raw", rawCardDataHandler);
    }
}
