

const globalDispatcher = {d:null}

/**
 * react-connect dispatcher management
 */
export default class Dispatcher {

    static setGlobal(dispatcher) {
        globalDispatcher.d = dispatcher
    }

    static getGlobal() {
        return globalDispatcher.d
    }
}