import React from 'react'
import IdGenerator from '../../../IdGenerator'
import {StackController} from "../model/StackController";

export class ComponentStackView extends React.Component {

    componentWillMount() {
        this.state = {
            compId : IdGenerator.generateGlobalId(),
            layers : [],
            indexSeed:0
        }
    }

    componentDidMount() {
        const {mountedHandler} = this.props
        if(mountedHandler) mountedHandler(this.createUiContext())
    }

    render() {
        const {uicontext} = this.props
        const {layers, compId} = this.state
        const layerViews = []
        for (let i in layers) {
            const layer = layers[i]
            const layerView = this.createLayerView(layer, this.createUiContext(), i==0);
            layerViews.push(layerView)
        }
        
        return (
            <div key={compId} class="fill-parent center-content-vh">
                {layerViews}
            </div>
        )
    }

    createUiContext() {
        const {uicontext} = this.props
        return uicontext.setComponentStackController(new StackController(this))
    }

    createLayerView(layer, uicontext, isFirst) {
        const {layerBackgroundColor} = this.props
        const newVar = {
            zIndex:layer.index,
            position:"absolute",
            top:"0px",
            left:"0px",
            backgroundColor: isFirst ? "rgba(0,0,0,0)" : layerBackgroundColor
        };
        return (
            <div key={layer.id} class="fade-in-element fill-parent-absolute center-content-vh" style={newVar}>
                {layer.compFactory(layer.id, uicontext)}
            </div>
        )
    }

}