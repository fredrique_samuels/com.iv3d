import {ComponentStackModel} from "./ComponentStackModel";
export class StackController {


    constructor(csv) {
        this.componentStackModel = new ComponentStackModel(csv)
    }

    /**
     *
     * @param compFactory a factory
     *      Where factory is a function(layerId, controller) => {return <SomeComp />}
     */
    addStackedComponent(compFactory) {
        if(this.componentStackModel) {
            return this.componentStackModel.addComponent(compFactory)
        }
    }

    removeStackedComponent(layerId) {
        if(this.componentStackModel) {
            this.componentStackModel.removeComponent(layerId)
        }
    }
}