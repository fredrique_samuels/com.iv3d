import Immutable from 'immutable'
import ViewModel from "../../shared/model/ViewModel";

export class ComponentStackModel extends ViewModel {

    constructor(parent) {
        super(parent);
    }

    addComponent(compFactory) {
        const newIndexSeed = this.state().indexSeed + 1;
        const layers = this.state().layers
        const layerId = this.state().compId + "_" +new String(newIndexSeed);
        layers.push( {
            id : layerId,
            index : newIndexSeed,
            compFactory :  compFactory
        })

        this.setState({
            indexSeed:newIndexSeed,
            layers:layers
        })
        return layerId
    }

    removeComponent(layerId) {
        const layers = this.state().layers
        if(layers.length==0) return

        if(layerId) {
            let orderedMap = new Immutable.OrderedMap();
            layers.forEach(l => orderedMap = orderedMap.set(l.id, l))
            orderedMap = orderedMap.delete(layerId)

            const values = orderedMap.toArray();
            this.setState({layers: values})
        }
    }

    removeTopComponent() {
        const layers = this.state().layers
        if(layers.length==0) return
        const list = Immutable.List(layers);
        this.setState({layers: list.remove(layers.length-1).toArray()})
    }
}
