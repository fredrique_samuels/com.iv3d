import React from 'react'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {ComponentStackView} from '../view/ComponentStackView'

export default class ComponentStackViewBuilder extends AbstractParamsBuilder {

    constructor(cb) {
        super({mountedHandler:null, layerBackgroundColor:"rgba(0,0,0,.7)" }, cb)
    }

    setMountedHandler(mh){this.params.mountedHandler = mh;return this}
    setLayerBackgroundColor(c){this.params.layerBackgroundColor = c;return this}

    buildView(uiContext) {
        const params = this.build()
        return <ComponentStackView uicontext={uiContext} layerBackgroundColor={params.layerBackgroundColor} mountedHandler={params.mountedHandler} />
    }
}