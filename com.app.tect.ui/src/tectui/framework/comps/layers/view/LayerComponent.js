import React from 'react'
import Immutable from 'immutable'
import Transition from 'react-inline-transition-group'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import {connect} from 'react-redux'
import {layerIdGenerator} from '../LayerIdGenerator'

import * as ComponentModels from "../../../ComponentModels"
import * as LayerManagerActions from '../LayerManagerActions'


class Layer extends React.Component {

    constructor() {
        super()
        this.id = layerIdGenerator.generate()
    }

    render() {
        const {userLayerParam, contentParams} = this.props;

        const defaultLayerParams = {
            opacity: 1,
            zIndex:3
        }

        const layerParams = Object.assign({}, defaultLayerParams, userLayerParam)

        let content = null
        if(contentParams.contentHandlerFactory) {
            const args = {contentParams, layerId: layerParams.layerId}
            content = contentParams.contentHandlerFactory.createContent(args)
        }

        return (<div class=" fade-in-element center-content-vh fill-parent-absolute layer-root" style={layerParams} key={layerParams.layerId}>
            <div style={this.getLayoutParams()}>{content}</div>
        </div>)
    }
    getLayoutParams() {
      const {contentParams} = this.props;
      if(contentParams.layoutParams)
        return contentParams.layoutParams
      return {
          position:"relative",
          top:"0%",
          left:"0%",
          width:"100%",
          height:"100%"
      }
    }

    // Create a new layer div.
    // The layering is controlled by the zIndex param property.
    // Properties not specified defaults to predetermined values.
    //
    static createForItems(userLayerParam, contentParams) {
        return <Layer userLayerParam={userLayerParam} contentParams={contentParams}/>
    }
}


class ModelFactory {
    build() {
        return {layers : Immutable.OrderedMap()}
    }
}

@connect((store) => {
    return {
        environment : store.environment,
        componentModels : store.componentModels,
    }
})
export default class LayerComponent extends React.Component {
    constructor(){
        super()
        this.cmg = new ComponentModels.ComponentModelGateway({
            modelFactory:new ModelFactory()
        })
    }

    componentWillMount() {
        this.cmg.createModel(this)
    }

    componentDidMount() {
        const {mountedCallback} = this.props
        if(mountedCallback) mountedCallback(this.cmg.getModelId())
    }

    componentWillUnmount(){
        this.cmg.deleteModel(this)
    }

    render() {
        const model = this.cmg.getModel(this)
        if(!model) return null
        let contentComp = this.createContentComponent(model)
        return (
            <div class="center-content-vh fill-parent-absolute applicationContentComponent layer-root">
                {contentComp}
            </div>
        )
    }

    createContentComponent(model) {
        const {lastLayer, layers} = this.extractLayersFromModel(model)
        const showClearButton = lastLayer?lastLayer.layerParams.canBeCleared:false
        const utilComp = showClearButton?(<div style={{height:"50px", width:"100%", top:"0px"}} >
            {this.createBackButton()}
        </div>):null
        // const layersComps = FadeInAndOutTransition.render(layers)

        const containerStyle = {
          width:"100%",
          position:"relative",
          height:showClearButton?"calc(100% - 50px)":"100%"
        }
        return ( <div class="fill-parent-absolute">
                <div style={containerStyle}>
                    <div style={{position:'relative',width:'100%',height:"100%"}}>
                        {layers}
                    </div>
                </div>
                {utilComp}
            </div>
        )
    }

    createBackButton() {
        return (<div >
            <button id="back-button" onClick={this.clearLastLayer.bind(this)} class="btn btn-default btn-lg" style={{marginLeft:"calc(50% - 40px)", width:"80px",height:"50px", positon:"relative",display:"inline",marginRight:"50px"}}><i class="fa fa-reply"></i></button>
        </div>)
    }

    clearLastLayer() {
        const model = this.cmg.getModel(this)
        const modelId = this.cmg.getModelId()

        let lastLayer = null
        model.layers.valueSeq().toList().map( arg => lastLayer=arg.layerId )
        if(lastLayer) {
          ComponentModels.updateModel(this.props.dispatch, modelId, LayerManagerActions.removeAppLayerBiz, {layerId:lastLayer})
        }
    }

    extractLayersFromModel(model) {
        var layerIndex = 4
        var i=0
        var layoutCount = model.layers.size
        var opacityForHiddenLayer = .8//layoutCount>1?.1/layoutCount:.1;
        let lastLayer = null
        const layers = model.layers.valueSeq().toList().map( layerBizParams =>
            {
                lastLayer = layerBizParams
                i+=1
                const isTopLayer = i==model.layers.size
                const v = {
                    zIndex:(layerIndex++),
                    opacity: isTopLayer?1:opacityForHiddenLayer,
                    // backgroundColor: isTopLayer?"rgba(0,0,0,.8)":"rgba(0,0,0,0)",
                    key: layerBizParams.layerId,
                    layerId: layerBizParams.layerId,
                }
                const layerComp = <Layer userLayerParam={v} contentParams={Object.assign({}, layerBizParams.contentParams, {uicontext:layerBizParams.uicontext})}/>


                let lp = layerBizParams.contentParams.layoutParams;

                const base = {
                    opacity:0.0
                }

                const appear = {
                    opacity:1,
                    transition: 'opacity 5000ms',
                }

                const leave = {
                    opacity:0,
                    transition: 'opacity 5000ms'
                }

                const layerTransition = {
                    base,
                    appear,
                    leave,
                }

                return (
                    <ReactCSSTransitionGroup transitionName="example" transitionEnterTimeout={1500} transitionLeaveTimeout={1300}>
                        {layerComp}
                    </ReactCSSTransitionGroup>
                )

                // return (<Transition
                //     childrenBaseStyle={layerTransition.base}
                //     childrenAppearStyle={layerTransition.appear}
                //     childrenEnterStyle={layerTransition.appear}
                //     childrenLeaveStyle={layerTransition.leave}
                // >
                //     {layerComp}
                // </Transition>)

                // return layerComp
            }
        )

        return {layers, lastLayer}
    }
}

const DEFAULT_LAYER_PARAMS = {canBeCleared:true}


/**
 *
 * DOCUMENTATION
 *
 * @ContentParams
 * An object used by the LayerContentFactory to constuct the view objects.
 * {
 *
 *      contentParams: Contains the data to be displayed
 *      {
 *          contentHandlerFactory[optional]: An class instance with a obj.createContentHandler(@LayerModel)
 *                                           method that return a React.Component.
 *          dataNodes: {},
 *          formData:{},
 *          // or any custom user data
 *          layoutParams:{} // css styling params
 *      }
 * }
 *
 *
 * @LayerModel
 * A object contain all the data for the layer.
 *
 * {
 *  layerComponentModelId[optional]: see @layerComponentModelId,
 *  layerId: The current layer Id. Used to remove the layer if needed,
 *  contentParams: see @ContentParams,
 *  layerParams: see @LayerParams,
 * }
 *
 */
export class LayerComponentManager {
    constructor(uicontext, layerComponentModelId) {
        this.layerComponentModelId = layerComponentModelId
        this.uicontext = uicontext
    }

    createLayerComponent(modelId, onMountedCallback) {
      return <LayerComponent uicontext={this.uicontext} modelId={modelId} mountedCallback={onMountedCallback}/>
    }

    addLayer(contentParams, layerParams={canBeCleared:true}) {
        const layerId = layerIdGenerator.generate()
        const bizParams = {
            uicontext:this.uicontext,
            layerId: layerId,
            contentParams:contentParams,
            layerParams:Object.assign({}, DEFAULT_LAYER_PARAMS, layerParams),
        }

        ComponentModels.updateModel(this.uicontext.getDispatcher(), this.layerComponentModelId, LayerManagerActions.setAppLayerBiz, bizParams)
        return layerId
    }

    removeLayer(layerId) {
        const bizParams = {
            layerId: layerId,
        }
        ComponentModels.updateModel(this.uicontext.getDispatcher(), this.layerComponentModelId, LayerManagerActions.removeAppLayerBiz, bizParams)
        return layerId
    }

    clearAllLayers() {
        ComponentModels.updateModel(this.uicontext.getDispatcher(), this.layerComponentModelId, LayerManagerActions.removeAllAppLayerBiz, {})
    }
}
