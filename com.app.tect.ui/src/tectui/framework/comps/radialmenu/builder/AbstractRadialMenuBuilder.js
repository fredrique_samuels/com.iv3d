import React from 'react'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {AbstractActionConfigBuilder} from "../../action/builder/AbstractActionConfigBuilder";
import {RadialMenuView} from './../view/RadialMenuView'

export class AbstractRadialMenuBuilder extends AbstractParamsBuilder {
    constructor(cb) {
        super({
                items:[]
            },
            cb)
    }

    createItem() {
        return new AbstractActionConfigBuilder(this.__addItem.bind(this))
    }
    __addItem(i){
        this.params.items.push(i)
        return this
    }
    buildView(uicontext) {
        console.error("AbstractRadialMenuBuilder.buildView is not supported!!")
        return null
    }
    buildViewToLayer(uicontext) {
        var contentParams = {
            contentHandlerFactory: {
                createContent:(layerModel) => {
                    const css = {
                        position:"relative",
                        left:"calc(50% - 150px)",
                        top:"calc(50% - 150px)"
                    }
                    return <div style={css}><RadialMenuView menuItems={layerModel.contentParams.menuItems} layerModel={layerModel} /></div>
                }
            },
            layoutParams: {
                position:"relative",
                top:"0%",
                left:"0%",
                width:"100%",
                height:"100%"
            },
            menuItems:this.build()
        };
        uicontext.layerComponentManager().addLayer(contentParams)
    }
}