import React from 'react'

class RemainingMenusCountComp extends React.Component {
    render() {
        const {count, colorClass} = this.props
        if(count==0) return null

        const menuIndexStyle = {
            position:"absolute",
            color:"white",
            borderRadius:"10px",
            width:"20px",
            left:"-0px",
            top: "-0px",
            paddingLeft:"3px",
        }

        const text = count>9?'9+':count
        return <div class={colorClass} style={menuIndexStyle}>{text}</div>
    }
}

class RadialAction {
    constructor(layerModel, action) {
        this.action = action
        this.layerModel = layerModel
    }
    run(){
        this.close()
        if(this.action){
            this.action()
        }
    }
    close() {
        const { layerId } = this.layerModel
        const { uicontext } = this.layerModel.contentParams
        uicontext.layerComponentManager().removeLayer(layerId)
    }
}

export class RadialMenuView extends React.Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.state=this.createState()

    }

    render() {

        const menuIndexStyle = {
            position:"absolute",
            color:"white",
            borderRadius:"10px",
            width:"20px",
            left:"-0px",
            top: "-0px",
            paddingLeft:"3px",
        }

        const {menuIndex, menus} = this.state
        const itemsDescr = menus[menuIndex]
        const menusRemainCount = this.getMenusRemainingCount()

        let itemComps = []
        let transformIndex = 0

        const transforms = this.getItemTransforms(menusRemainCount>0?itemsDescr.length+1:itemsDescr.length)



        if(menusRemainCount>0) {
            itemComps.push(<span style={{transform: transforms[0].transform}} class="radmenu-item">
              <a style={{transform: transforms[0].itransform}} href="#" onClick={this.nextMenu.bind(this)}>
                <i class={"fa fa-ellipsis-h fa-2x"}></i>
              </a>
              <RemainingMenusCountComp colorClass={"color-primary"} count={menusRemainCount}/>
              <div style={this.buttonTextTopCss()}>{this.i8n('radialmenu.more', 'More')}</div>
            </span>)
            transformIndex+=1
        }


        for(var i in itemsDescr) {
            const icon = itemsDescr[i].getIcon()
            const action = itemsDescr[i].getAction()
            const style = itemsDescr[i].getStyle()
            const label = itemsDescr[i].getLabel()
            const ra = new RadialAction(this.props.layerModel, action)

            itemComps.push(<span style={Object.assign({},style?style:{},{transform: transforms[transformIndex].transform})} class="radmenu-item">
            <a style={{transform: transforms[transformIndex].itransform}} href="#" onClick={ra.run.bind(ra)}>
                {icon.isFontAwesome()?<i class={"fa " + icon.getFaKey() + " fa-2x"}></i>:null}
                <div style={ Object.assign({},transforms[transformIndex].textCss)}>
                {this.i8n(label)}
            </div>
            </a>
          </span>)
            transformIndex+=1
        }


        const menusOpenedCount = this.getMenusOpenedCount()
        return (<div class="radmenu" >
            <div class="radmenu-root">
                {itemComps}
                <span class="radmenu-item color-error">
                      <a href="#" onClick={this.close.bind(this)}>
                        <i class="fa fa-times fa-2x"></i>
                      </a>
                      <RemainingMenusCountComp colorClass={"color-error"} count={menusOpenedCount}/>
                    </span>
            </div>
        </div>)


    }

    close() {
        if(this.state.menuIndex>0) {
            this.prevMenu()
            return false
        }

        if(this.props.layerModel) {
            const { layerId } = this.props.layerModel
            const { uicontext } = this.props.layerModel.contentParams
            uicontext.layerComponentManager().removeLayer(layerId)
        }
        return false
    }

    i8n(key, nullValue=null) {
        if(this.props.layerModel) {
            const { uicontext } = this.props.layerModel.contentParams
            return uicontext.i8n().getMessage(key)
        }
        return nullValue?nullValue:key
    }


    getItemTransforms(len) {
        const transforms1 = [this.createTransform("0", this.buttonTextTopCss())]

        const transforms2 = [this.createTransform("0", this.buttonTextTopCss()),
            this.createTransform("90", this.buttonTextRightCss())]

        const transforms3 = [this.createTransform("0", this.buttonTextTopCss()),
            this.createTransform("120", this.buttonTextRightCss()),
            this.createTransform("240", this.buttonTextLeftCss())]

        const transforms4 = [this.createTransform("0", this.buttonTextTopCss()),
            this.createTransform("90", this.buttonTextRightCss()),
            this.createTransform("180",this.buttonTextBottomCss()),
            this.createTransform("270", this.buttonTextLeftCss())]


        const transforms5 = [this.createTransform("0", this.buttonTextTopCss()),
            this.createTransform("72", this.buttonTextRightCss()),
            this.createTransform("144", this.buttonTextRightCss()),
            this.createTransform("216", this.buttonTextLeftCss()),
            this.createTransform("288", this.buttonTextLeftCss())]

        const transforms6 = [this.createTransform("0", this.buttonTextTopCss()),
            this.createTransform("60", this.buttonTextRightCss()),
            this.createTransform("120", this.buttonTextRightCss()),
            this.createTransform("180",this.buttonTextBottomCss()),
            this.createTransform("240", this.buttonTextLeftCss()),
            this.createTransform("300", this.buttonTextLeftCss())]


        if(len==1)return transforms1
        if(len==2)return transforms2
        if(len==3)return transforms3
        if(len==4)return transforms4
        if(len==5)return transforms5
        if(len==6)return transforms6

        return []
    }

    buttonTextTopCss() {
        return {
            position:"absolute",
            bottom:"65px",
            minWidth:"100%",
            display:"flex",
            justifyContent:"center",
            fontSize:"15px",
            fontWeight:"bold",
            left:"0px",
            color:"#000000",
            textShadow: "0 0 20px #cccccc"
        }
    }

    buttonTextBottomCss() {
        return {
            position: "absolute",
            top: "65px",
            minWidth: "100%",
            display: "flex",
            justifyContent: "center",
            fontSize: "15px",
            fontWeight: "bold",
            left: "0px",
            color: "#000000",
            textShadow: "0 0 20px #cccccc"
        }
    }

    buttonTextLeftCss() {
        return {
            position: "absolute",
            right: "65px",
            top: "15px",
            width: "500%",
            height: "30px",
            display: "flex",
            justifyContent: "flex-end",
            alignContent: "center",
            fontSize: "15px",
            fontWeight: "bold",
            color: "#000000",
            textShadow: "0 0 20px #cccccc"
        }
    }

    buttonTextRightCss() {
        return  {
            position:"absolute",
            left:"65px",
            top:"15px",
            width:"500%",
            height:"30px",
            display:"flex",
            justifyContent:"flex-start",
            alignContent:"center",
            fontSize:"15px",
            fontWeight:"bold",
            color:"#000000",
            textShadow: "0 0 20px #cccccc"
        }
    }

    createTransform(rot, textCss) {
        return {transform:"translateX(0px) translateY(0px) rotate("+rot+"deg) translateY(-90px)",
            itransform:"rotate(-"+rot+"deg)",
            textCss:textCss}
    }

    nextMenu() {
        const {menuIndex, menus} = this.state
        if(menuIndex==menus.length-1)return

        const newMenuIndex = menuIndex+1
        const newMenu = menus[newMenuIndex]
        const lastMenu  = newMenuIndex==menus.length-1
        this.setState({menuIndex:newMenuIndex, visibleItems:newMenu, hasNextMenu:lastMenu})
    }

    prevMenu() {
        const {menuIndex, menus} = this.state
        if(menuIndex==0)return

        const newMenuIndex = menuIndex-1
        const newMenu = menus[newMenuIndex]
        this.setState({menuIndex:newMenuIndex, visibleItems:newMenu, hasNextMenu:true})
    }

    getMenusRemainingCount() {
        const {menuIndex, menus} = this.state
        if(menus.length==1)return 0
        return menus.length-(menuIndex+1)
    }

    getMenusOpenedCount() {
        const {menuIndex, menus} = this.state
        if(menus.length==1)return 0
        return menuIndex
    }

    createState() {
        const {items} = this.props.menuItems
        let visibleItems = items
        let menuIndex = 0
        let menus = [items]
        const multiMenus = items.length>6


        if(multiMenus) {
            menus = [[]]
            let ci = 0
            for (var i in items) {
                if(i!=0 && i%5==0) {
                    menus.push([])
                    ci+=1
                }
                menus[ci].push(items[i])
            }
        }
        return {menuIndex, menus}
    }
}
