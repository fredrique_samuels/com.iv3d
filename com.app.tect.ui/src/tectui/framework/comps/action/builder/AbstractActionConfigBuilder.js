import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {ActionConfig} from "./ActionConfig";
import {AbstractIconBuilder} from "../../icon/builder/AbstractIconBuilder";



export class AbstractActionConfigBuilder extends AbstractParamsBuilder {
    constructor(cb) {
        super({
            icon:null,
            label:"",
            action:null,
            style:{}
        }, cb)
    }

    setLabel(l){this.params.label = l;return this}
    setIcon(icon){this.params.icon = icon;return this}
    createIcon() {
        return new AbstractIconBuilder(this.setIcon.bind(this))
    }
    setAction(a){this.params.action = a;return this}
    setStyle(s){this.params.style = s;return this}
    set(ac){this.params = Object.assign({}, ac.params);return this}
    build(){ return new ActionConfig(this.params)}
}