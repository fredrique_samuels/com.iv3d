export class ActionConfig {
    constructor(params){
        this.params = params
    }

    getIcon(){return this.params.icon}
    getLabel(){return this.params.label}
    getAction(){return this.params.action}
    getStyle(){return this.params.style}
}