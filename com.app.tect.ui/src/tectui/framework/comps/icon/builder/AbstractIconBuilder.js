import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import * as icon from "./Icon"

export class AbstractIconBuilder extends AbstractParamsBuilder {

    constructor(cb) {
        super({
            type:null,
            faOptions:[]
        }, cb)
    }

    setImage(img){
        this.params.type=icon.ICON_TYPE_IMAGE
        this.params.url=img
        return this
    }

    setFaIcon(content){
        this.params.type=icon.ICON_TYPE_FONTAWESOME
        this.params.faKey=content
        return this
    }

    setSvg(url){
        this.params.type=icon.ICON_TYPE_SVG
        this.params.url=url
        return this
    }

    build(){
        return new icon.Icon(Object.assign({}, this.params))
    }
}