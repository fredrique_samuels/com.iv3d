

export const ICON_TYPE_FONTAWESOME = 'icon.type.fontawesome'
export const ICON_TYPE_IMAGE = 'icon.type.image'
export const ICON_TYPE_SVG = 'icon.type.svg'


export class Icon {

    constructor(params) {
        this.params = params;
    }

    isFontAwesome(){return this.params.type==ICON_TYPE_FONTAWESOME}
    getFaKey(){return this.params.faKey}
    faKey(){return this.params.faKey}

    isImage(){return this.params.type==ICON_TYPE_IMAGE}
    isSvg(){return this.params.type==ICON_TYPE_SVG}
    url(){return this.params.url}

    get(params = {width:null, height:null}) {
        if(this.isFontAwesome) {
            return <i class={"fa " + this.params.faKey + " " + this.params.faOptions.join(" ")}></i>
        } 
    }

    static createForImage(url){return new Icon({url:url,type:ICON_TYPE_IMAGE})}
    static createForSvg(url){return new Icon({url:url,type:ICON_TYPE_SVG})}
    static createForFontAwesome(faKey, faOptions=[]){
        return new Icon({faKey:faKey, faOptions:faOptions, type:ICON_TYPE_FONTAWESOME})
    }

}