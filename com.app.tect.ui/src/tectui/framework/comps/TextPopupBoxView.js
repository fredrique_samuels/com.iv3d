import React from 'react'
import {BorderPanel} from './shared/view/BorderPanel'

export class TextPopupBoxView extends React.Component {
    render() {
        const {closeHandler, text, type} = this.props
        const handler = () => {if(closeHandler)closeHandler()}

        let classes = "fa-action fa fa-check-circle-o color-success-text fa-2x"
        if(type=='error') {
            classes = "fa-action fa fa-times-circle-o color-error-text fa-2x"
        }

        return (
            <BorderPanel>
                <div style={{textShadow:"0px 0px 20px black", color:"white", position:"relative", width:"100%"}} >
                    <div style={{display:"flex",justifyContent:"center",  width:"100%", marginLeft:"5px", marginRight:"5px"}} >{text}</div>
                    <div style={{width: "100%", textAlign: "center"}}>
                        <i class={classes} onClick={handler}></i>
                    </div>
                </div>
            </BorderPanel>
        )
    }
}