import React from 'react'
import {LogItem} from './LogItem'
import {LogViewModel} from "../model/LogViewModel";

const ALL = 'ALL'
const INFO = 'INFO'
const WARNING = 'WARNING'
const ERRORS = 'ERRORS'

class FilterButton extends React.Component {
    render() {
        const {text, color, action}= this.props
        return (
            <button onClick={action} style={{float: "left", marginTop: "5px", marginRight: "5px", marginLeft: "5px"}} class={"btn btn-xs hover-shadow-light " + color} >
                {text}
            </button>
        )
    }
}

export class LogsContainerView extends React.Component {
    componentWillMount() {
        new LogViewModel(this).setup()
    }
    render() {
        const model = new LogViewModel(this);

        return (
            <div class="fill-parent" style={{maxHeight:"100%", position:"relative"}}>
                <div style={{width:"100%", height:"30px", position:"absolute", top:"0px"}}>
                    <FilterButton action={model.setAll.bind(model)} text={model.allLabel()} color={model.isAll()?"color-primary":"color-default"}/>
                    <FilterButton action={model.setInfo.bind(model)} text={model.infoLabel()} color={model.isInfo()?"":"color-default"}/>
                    <FilterButton action={model.setWarning.bind(model)} text={model.warningLabel()} color={model.isWarning()?"color-warning":"color-default"}/>
                    <FilterButton action={model.setErrors.bind(model)} text={model.errorLabel()} color={model.isError()?"color-error":"color-default"}/>
                </div>
                <div class="panel-text" style={{width:"100%", maxHeight:"calc( 100% - 30px )", paddingTop:"30px", overflowY:"auto"}}>
                    {model.logs().map( l => <LogItem log={l} />)}
                </div>
            </div>
        )
    }

}