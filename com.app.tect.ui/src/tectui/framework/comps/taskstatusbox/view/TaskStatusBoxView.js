import React from 'react'

import AbstractStatusBoxViewBuilder from '../../statusbox/builder/AbstractStatusBoxViewBuilder'
import TaskStatusViewModel from "../model/TaskStatusViewModel";
import LeftScrollButton from './LeftScrollButton'
import RightScrollButton from './RightScrollButton'
import HistoryItemButton from './HistoryItemButton'
import * as tectui from "../../../../tectui";


class StatusUpdateTimer {

    constructor() {
        this.timer = null
    }

    start() {
        this.reset()
    }

    reset() {
        this.timer = new tectui.Timeout();
    }

    updateState() {

    }
}

export default class TaskStatusBoxView extends React.Component {


    constructor() {
        super()
    }

    componentWillMount() {
        this.state = new TaskStatusViewModel(this).newState()
    }

    render() {
        const model = new TaskStatusViewModel(this)
        const scrollModel = model.historyScrollModel()

        const headingComp = (
            <div style={{width:"100%",marginBottom:"2px"}}>
                {model.name()}
                <button onClick={this.refreshStatus.bind(this)}
                        style={{float: "right", marginTop: "5px", marginLeft: "5px", marginRight: "5px"}}
                        class="btn btn-xs color-primary hover-shadow-light">
                    <i class="fa fa-refresh"></i>
                </button>
                {this.historyItemView(model, scrollModel)}
            </div>
        )
        return new AbstractStatusBoxViewBuilder()
            .setHeading(headingComp)
            .setStatus(model.status())
            .setStatusText(model.statusText())
            .setContent(model.contentComps())
            .buildView(this.props.uicontext)
    }
    refreshStatus() {
        const model = new TaskStatusViewModel(this)
        model.refreshState()
    }
    historyItemView(model, scrollModel) {
        if(!model.hasHistoryItems())return null
        const historyItemComps = model.historyItems().map( (item, index) => {
            if(scrollModel.isVisible(index)) {
                return <HistoryItemButton item={item} action={model.historyItemAction()} />
            }
            return null
        } )

        return (
            <div style={{float: "right"}}>
                <LeftScrollButton enabled={scrollModel.hasPrev()} action={model.scrollHistoryItemPrev.bind(model)}/>
                {historyItemComps}
                <RightScrollButton enabled={scrollModel.hasNext()}
                                   action={model.scrollHistoryItemNext.bind(model)}/>
            </div>
        )
    }
}