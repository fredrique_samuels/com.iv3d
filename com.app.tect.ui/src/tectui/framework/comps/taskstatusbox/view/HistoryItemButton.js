import React from 'react'
import StatusIcon from '../../shared/view/StatusIcon'

export default class HistoryItemButton extends React.Component {
    render() {
        const { item } = this.props
        return (
            <button onClick={this.clicked.bind(this)} class="btn color-default btn-sm hover-shadow" style={{borderRadius: "0px"}}>
                <StatusIcon status={item.status.toLowerCase()}/> {'#' + item.runId}
            </button>
        )
    }

    clicked() {
        const { item, action } = this.props
        if(action) {
            action(item)
        }
    }
}
