import React from 'react'
import AbstractCardOptionOverlaysBuilder from "../../cardselect/builder/AbstractCardOptionOverlaysBuilder";
import TaskStatus from "../model/TaskStatus";

export default class TaskCardRenderer {

    constructor(uicontext) {
        this.uicontext = uicontext
    }

    render(data) {
        const taskStatus = new TaskStatus(data);
        let builder = new AbstractCardOptionOverlaysBuilder()
            .addView(<div class="fill-parent center-content-vh"><i class="fa fa-file fa-4x"></i></div>);

        builder = builder.createStatusBannerWithIcon(taskStatus.status())
            .commit()

        return builder.build()
    }
}

