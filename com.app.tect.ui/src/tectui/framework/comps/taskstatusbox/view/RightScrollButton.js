import React from 'react'

export default class RightScrollButton extends React.Component {
    render() {
        const {enabled, action} = this.props
        const css = {borderRadius:"0px 4px 4px 0px"}
        return (
            <button onClick={action} class={"btn btn-sm hover-shadow " + (enabled?"color-primary":"color-default")} style={css}>
                <i class="fa fa-arrow-right"></i>
            </button>
        )
    }
}