
import React from 'react'
import {BorderPanel} from '../../shared/view/BorderPanel'
import StatusIcon from '../../shared/view/StatusIcon'

export class LogItem extends React.Component {
    render() {
        const { log } = this.props

        return (
            <div style={{width:"100%"}}>
                <BorderPanel borderColorClass={log.level.toLowerCase()}>
                    <StatusIcon status={log.level} /> <span>{new Date(log.date.iso).toUTCString()}</span>
                    <p>{log.message}</p>
                </BorderPanel>
            </div>
        )
    }
}