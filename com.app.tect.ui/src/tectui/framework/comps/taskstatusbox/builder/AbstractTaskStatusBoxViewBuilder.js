import React from 'react'
import TaskStatusBoxView from '../view/TaskStatusBoxView'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {AbstractTitledPanelViewBuilder} from "../../titledpanel/builder/AbstractTitledPanelViewBuilder";

export default class AbstractTaskStatusBoxViewBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({
            taskData:{},
            historyData:[],
            historyItemAction:null,
            refreshStateHandler:null
        }, bc)
    }

    buildView(uicontext) {
        return <TaskStatusBoxView params={this.buildData()} uicontext={uicontext} />
    }

    setTaskData(d){
        this.params.taskData = d
        return this
    }

    setHistoryData(h){
        this.params.historyData = h
        return this
    }

    setHistoryItemClickedCallback( cb ) {
        this.params.historyItemAction = cb
        return this
    }

    setRefreshStateHandler( cb ) {
        this.params.refreshStateHandler = cb
        return this
    }

    buildData() {
        const build = this.build()
        return Object.assign({}, build.taskData, {historyData:build.historyData, historyItemAction:build.historyItemAction, refreshStateHandler:build.refreshStateHandler})
    }

    buildView(uicontext) {
        console.warn("AbstractTaskStatusBoxViewBuilder.buildView is not supported!!")
        return null
    }

    buildViewToLayer(uicontext) {
        new AbstractTitledPanelViewBuilder()
            .setContent(<div class="fill-parent"><TaskStatusBoxView params={this.buildData()} uicontext={uicontext} /></div>)
            .setStyleProperty({width:"90%", height:"90%"})
            .setFixedHeight("100%")
            .disableScroll()
            .buildViewToLayer(uicontext)

    }

}