import Immutable from 'immutable'
import React from 'react'
import ViewModel from "../../shared/model/ViewModel";
import TaskStatus from "./TaskStatus";
import TaskCardRenderer from "../view/TaskCardRenderer";
import ItemScrollIndexModel from "../../shared/model/ItemScrollIndexModel";
import AbstractTabsViewBuilder from "../../tabs/builder/AbstractTabsViewBuilder";
import AbstractCardSelectViewBuilder from "../../cardselect/builder/AbstractCardSelectViewBuilder";
import AbstractFormViewBuilder from "../../form/builder/AbstractFormViewBuilder";
import {LogsContainerView} from '../view/LogsContainerView'

export default class TaskStatusViewModel extends ViewModel {
    constructor(parent) {
        super(parent)
    }
    newState() {
        const taskStatus = new TaskStatus(this.parent.props.params)
        const newState = {
            params : this.parent.props.params,
            historyScrollModel : new ItemScrollIndexModel(taskStatus.historyItems().length, 5).scrollToEnd()
        }
        return Object.assign({}, newState)
    }
    updateJobStatus(s){this.setState({params:s})}
    taskStatus(){ return new TaskStatus(this.state().params) }
    name(){ return this.taskStatus().name()}
    status() { return this.taskStatus().status()}
    hasRunId() {return this.taskStatus().runId() != null }
    statusText() {
        const runId = this.taskStatus().runId();
        if(runId>0) {
            return "#" + runId + " " + this.taskStatus().status().toUpperCase()
        }
        return null
    }
    historyItemAction(){return this.taskStatus().historyItemAction()}
    refreshState() {
        const refreshStateHandler = this.taskStatus().refreshStateHandler();
        console.log(refreshStateHandler)
        if(refreshStateHandler) {
            refreshStateHandler(this.updateJobStatus.bind(this))
        }
    }
    updateJobStatus(js){
        const state = this.state();
        this.setState({params:Object.assign({}, state.params, js)})
    }
    contentComps() {
        let tabsBuilder = new AbstractTabsViewBuilder()

        const stats = this.__getStatComp()
        if(stats){
            tabsBuilder = tabsBuilder.addTab(this.i8nMessage('task.status.tab.label.stats'), stats)
        }

        const children = this.__getChildrenComp()
        if(children) {
            tabsBuilder = tabsBuilder.addTab(this.i8nMessage('task.status.tab.label.children') + "(" +this.taskStatus().childrenParams().length+ ")", children)
        }

        const logComp = this.__getLogComp()
        if(logComp) {
            tabsBuilder = tabsBuilder.addTab(this.i8nMessage('task.status.tab.label.logs'), logComp)
        }

        const errorComp = this.__getErrorComp()
        if(errorComp) {
            tabsBuilder = tabsBuilder.addTab(this.i8nMessage('task.status.tab.label.error'), errorComp)
        }

        return tabsBuilder.buildView(this.uicontext())
    }
    historyItems(){return this.taskStatus().historyItems()}
    hasHistoryItems(){return this.taskStatus().historyItems().length>0 }
    historyScrollModel(){return this.state().historyScrollModel}
    scrollHistoryItemPrev(){
        this.setState({historyScrollModel:this.state().historyScrollModel.prev()})
    }
    scrollHistoryItemNext(){
        this.setState({historyScrollModel:this.state().historyScrollModel.next()})
    }
    __getErrorComp() {
        let formViewBuilder = this.uicontext().formViewBuilder();
        if(this.taskStatus().hasError()) {
            const errorMessage = this.taskStatus().errorMessage();
            if(errorMessage) {
                formViewBuilder = formViewBuilder.createText()
                    .setText(errorMessage)
                    .setLabel(this.i8nMessage('task.status.label.error.message'))
                    .commit()
            }

            const errorTrace = this.taskStatus().errorTrace();
            if(errorTrace) {
                formViewBuilder = formViewBuilder.createText()
                    .setText(errorTrace)
                    .setLabel(this.i8nMessage('task.status.label.error.trace'))
                    .commit()
            }

            return formViewBuilder.buildView(this.uicontext())
        }
        return null
    }
    __getChildrenComp() {
        if(this.taskStatus().hasChildren()) {
            const childrenParams = this.taskStatus().childrenParams();

            let cardSelectViewBuilder = new AbstractCardSelectViewBuilder()
                .setDataViewRenderer(new TaskCardRenderer(this.uicontext()));

            childrenParams.forEach( (c => {
                const ts = new TaskStatus(c);
                cardSelectViewBuilder = cardSelectViewBuilder
                    .createOption()
                    .setLabel(ts.name())
                    .setAction( ((data) => {
                        this.uicontext().taskStatusBoxViewBuilder()
                            .setTaskData(data)
                            .buildViewToLayer(this.uicontext())
                    }).bind(this) )
                    .setData(c)
                    .commit()
            }).bind(this))
            return cardSelectViewBuilder.buildView(this.uicontext())
        }

        return null
    }
    __getStatComp(){
        const attributesExcluded = ["operation.status.attribute.logs"]
        let builder = new AbstractFormViewBuilder();
        const attributes = this.taskStatus().attributes()
        if(attributes.length>0) {
            attributes.filter( attr => !attributesExcluded.includes(attr.name) ).forEach( (attr => {
                builder = builder.createText()
                    .setLabel(this.__getAttributeLabel(attr))
                    .setText(this.__getAttributeValue(attr))
                    .commit()
            }).bind(this))
           return builder.buildView(this.uicontext())
        }
        return null
    }
    __getAttributeValue(attr){
        const transformer = this.__valueTransformers().get(attr.name);
        return transformer ? transformer(attr.value) : String(attr.value)
    }
    __getAttributeLabel(attr){
        return this.i8n().getMessage(attr.name)
    }
    __valueTransformers(){
        let valueTransformers =  new Immutable.Map()
        return valueTransformers.set('taskId', (v) => "#" + JSON.parse(v).runId)
    }
    __getLogComp() {
        return <LogsContainerView uicontext={this.uicontext()} logs={this.taskStatus().logs()} />
    }
}