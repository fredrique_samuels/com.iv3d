import React from 'react'
import * as Constants from '../../shared/Constants'

export default class TaskStatus {

    constructor(params) {
        this.params = params
    }

    id(){return this.params.id}
    attributes(){ return this.params.attributes ? this.params.attributes : [] }
    hasError(){ return this.params.error }
    errorMessage(){ return this.params.errorMessage }
    errorTrace(){ return this.params.errorTrace }
    status(){ return this.__getStatus() }
    hasParent(){ return this.params.parent > 0 }
    name(){ return this.params.name ? this.params.name :(<span>{this.params.id>0?<i>{this.params.id}</i>:""} {this.params.type}</span>)}
    runId() { return this._getRunId() }
    jobId(){return this.__getAttributeValue("operation.status.attribute.job.id")}
    logs(){
        let logs = this.__getAttributeValue("operation.status.attribute.logs") || "[]";
        logs = JSON.parse(logs)
        const cp = this.childrenParams();
        for (var i=0;i<cp.length;i++) {
            logs = logs.concat(new TaskStatus(cp[i]).logs());
        }
        return logs
    }
    childrenParams() { return this.__getChildren() }
    hasChildren(){return this.childrenParams().length>0}
    historyItems(){return this.params.historyData?this.params.historyData:[]}
    historyItemAction(){return this.params.historyItemAction}
    refreshStateHandler(){return this.params.refreshStateHandler}

    _getRunId() {
        const attrRunId = this.__getRunIdFromAttribute()
        if(attrRunId) return attrRunId

        return -1
    }
    __getStatus() {
        if(this.params.status) {
            return this.params.status.toLowerCase()
        }

        if(this.hasError()) {
            return Constants.STATUS_ERROR
        }

        return Constants.STATUS_SUCCESS
    }
    __getRunIdFromAttribute() {
        const runId = this.__getAttributeValue('operation.status.attribute.run.id')
        if(runId) {
            return runId
        }
        return null
    }
    __getAttributeValue(attrName) {
        const filtered = this.attributes().filter( attr => attr.name == attrName )
        if(filtered.length==0) return null
        return filtered[0].value
    }
    __getChildren() {
        const { children } = this.params
        if(children) {
            return children
        }
        return []
    }
}