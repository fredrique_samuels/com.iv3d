import ViewModel from "../../shared/model/ViewModel";

const ALL = 'ALL'
const INFO = 'INFO'
const WARNING = 'WARNING'
const ERRORS = 'ERRORS'


export class LogViewModel extends ViewModel {

    constructor(parent) {
        super(parent);
    }

    setup() {
        this.init({mode:ALL})
    }

    logs() {
        if(this.isInfo()) {
            return this.infoLogs()
        }
        if(this.isWarning()) {
            return this.warningLogs()
        }
        if(this.isError()) {
            return this.errorLogs()
        }
        return this.allLogs()
    }

    allLabel(){return this.uicontext().i8nMessage("tectui.framework.logview.filter.all") + " ("+this.allLogs().length+")"}
    infoLabel(){return this.uicontext().i8nMessage("tectui.framework.logview.filter.info") + " ("+this.infoLogs().length+")"}
    warningLabel(){return this.uicontext().i8nMessage("tectui.framework.logview.filter.warning") + " ("+this.warningLogs().length+")"}
    errorLabel(){return this.uicontext().i8nMessage("tectui.framework.logview.filter.error") + " ("+this.errorLogs().length+")"}

    errorLogs(){return this.allLogs().filter(l => l.level==ERRORS)}
    warningLogs(){return this.allLogs().filter(l => l.level==WARNING)}
    infoLogs(){return this.allLogs().filter(l => l.level==INFO)}
    allLogs(){return this.props().logs.sort(
        (a, b) => {
            if(a.date.millisecondsSinceEpoc < b.date.millisecondsSinceEpoc) {
                return 1;
            }
            if(a.date.millisecondsSinceEpoc > b.date.millisecondsSinceEpoc) {
                return -1;
            }
            return 0
        }
    )}

    setAll(){this.setState({mode:ALL})}
    setErrors(){this.setState({mode:ERRORS})}
    setInfo(){this.setState({mode:INFO})}
    setWarning(){this.setState({mode:WARNING})}

    isAll(){return this.state().mode==ALL}
    isInfo(){return this.state().mode==INFO}
    isError(){return this.state().mode==ERRORS}
    isWarning(){return this.state().mode==WARNING}

}