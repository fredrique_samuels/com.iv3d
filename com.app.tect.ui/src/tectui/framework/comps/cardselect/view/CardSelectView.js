import React from 'react'
import CardSelectViewModel from "../model/CardSelectViewModel";


export default class CardSelectView extends React.Component {

    render() {

        const model = new CardSelectViewModel(this);
        const heading = model.heading();

        const itemCntr = {
            borderRadius:"3px 0px 0px 3px",
            overflowY:"auto",
            paddingTop:heading?"30px":"0px",
            position:"relative",
            maxHeight:"100%"
        }

        const panelHeading = {
            position:"absolute",
            top:"0",
            left:"0",
            width:"100%",
            height:"20px",
            textAlign:"center",
            backgroundColor:"rgba(0,0,0,0.7)",
            color:"#eeeeee",
            width:"100%"
        }


        return (<div class="fill-parent item-cntr" style={itemCntr}>
            {heading?<div class="heading-pane" style={panelHeading}>{heading}</div>:null}
            <div style={{width:"100%", overflowY:"auto", maxHeight:heading?"calc( 100% - 30px )":"100%"}}>
                {model.cardOptionComps()}
            </div>
        </div>)
    }
}