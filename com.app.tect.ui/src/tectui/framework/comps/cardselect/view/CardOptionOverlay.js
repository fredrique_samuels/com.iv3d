import React from 'react'

export default class CardOptionOverlay extends React.Component {
    render() {
        const zIndex = this.props

        const contentLayer = {
            width:"100%",
            height:"100%",
            position:"absolute",
            color:"slategrey",
            top:"0px",
            overflow:"hidden",
            zIndex:new String(zIndex)
        }

        return (
            <div style={contentLayer} >
                {this.props.children}
            </div>
        )
    }
}
