import React from 'react'

export default class CardOptionStatusOverlay extends  React.Component {
    render() {

        const { status, faIcon } = this.props

        return this.renderTopRightIcon(status, faIcon)
    }

    renderBanner(status, faIcon) {
        const statusColorBar = {
            height: "30%",
            top: "65%",
            position: "absolute",
            width: "100%",
            opacity: ".9"
        }

        return (
            <div class={"color-" + status + " center-content-vh"} style={statusColorBar}>
                {faIcon ? <i class={"fa " + faIcon + " fa-1x"}></i> : null}
            </div>
        )
    }

    renderTopRightIcon(status, faIcon) {
        const statusColorBar = {
            height: "30%",
            top: "5px",
            right: "5px",
            position: "absolute",
            width: "20px",
            height: "20px",
            opacity: ".9",
            borderRadius:"5px"
        }

        return (
            <div class={"color-" + status + " center-content-vh"} style={statusColorBar}>
                {faIcon ? <i class={"fa " + faIcon + " fa-1x"}></i> : null}
            </div>
        )
    }
}