import React from 'react'

import CardOptionOverlay from './CardOptionOverlay'

export default class CardOptionView extends React.Component {
    render() {
        const { overlays, label } = this.props

        const itemPane = {
            width:"100px",
            minHeight:"100px",
            margin:"2px",
            display:"inline-block",
            verticalAlign:"top",
            overflow:"hidden",
            cursor:"pointer"
        }
        const itemContent = {
            width:"100px",
            height:"100px",
            overflow:"hidden",
            border:"1px solid slategrey",
            backgroundColor:"rgba(0,0,0,.4)",
            position:"relative",
            borderRadius:"4px 4px 0px 0px"
        }
        const itemLabel = {
            width:"100%",
            color:"white",
            fontSize:"11px",
            textAlign:"center",
            backgroundColor:"rgba(0,0,0,.7)",
            paddingTop:"2px",
            paddingBottom:"3px",
            borderRadius:"0px 0px 4px 4px"
        }

        const overLayComps = overlays ? overlays.map( (v, k) => <CardOptionOverlay zIndex={k}>{v}</CardOptionOverlay> ) : []

        return (
            <div class="item-pane hover-shadow-light" style={itemPane} onClick={this.onclick.bind(this)}>
                <div class="item-content" style={itemContent}>
                    {overLayComps}
                </div>
                {label?<div class="item-label" style={itemLabel}>{label}</div>:null}
            </div>
        )
    }

    onclick() {
        const { data, action } = this.props
        if(action) {
            action(data)
        }
    }
}