import React from 'react'

import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import CardSelectView from '../view/CardSelectView'
import CardOptionBuilder from "./CardOptionBuilder";
import {AbstractTitledPanelViewBuilder} from "../../titledpanel/builder/AbstractTitledPanelViewBuilder";

export default class AbstractCardSelectViewBuilder extends AbstractParamsBuilder {

    constructor(buildCallback=null) {
        super({
                heading: null,
                options: [],
                dataViewRenderer:{render: (data) => null},
            },
            buildCallback)
    }

    /**
     * Set the heading value
     * @param h
     * @returns {AbstractCardSelectViewBuilder}
     */
    setHeading(h){this.params.heading = h;return this}

    /**
     * Set the data renderer
     * @param dvr @see {AbstractDataViewRenderer}
     * @returns {AbstractCardSelectViewBuilder}
     */
    setDataViewRenderer(dvr){this.params.dataViewRenderer = dvr;return this}

    /**
     * Create a new Card Option
     *
     * @returns {CardOptionBuilder}
     */
    createOption(){return new CardOptionBuilder(this.__addOption.bind(this))}
    __addOption(o){this.params.options.push(o);return this}

    buildView(uicontext){return <CardSelectView params={this.build()} uicontext={uicontext}/>}
    buildViewToLayer(uicontext) {
        new AbstractTitledPanelViewBuilder()
            .setContent(<CardSelectView params={this.build()} uicontext={uicontext}/>)
            .setFixedHeight("100%")
            .setStyleProperty({width:"90%", height:"90%"})
            .buildViewToLayer(uicontext)
    }
}