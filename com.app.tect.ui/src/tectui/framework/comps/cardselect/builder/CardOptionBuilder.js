import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export default class CardOptionBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({
                label:null,
                action:null,
                data: null
            },
            bc)
    }
    setLabel(l){this.params.label=l;return this}
    setAction(l){this.params.action=l;return this}
    setData(d){this.params.data=d;return this}
}