import React from 'react'
import AbstractParamsBuilder from "../../../../../../platform/builder/AbstractParamsBuilder";
import CardOptionStatusOverlay from './../../view/overlays/CardOptionStatusOverlay'

export default class StatusOverlayBuilder extends AbstractParamsBuilder {

    constructor(status, bc) {
        super({status:status, faIcon:null}, bc)
    }
    setFaIcon(faIcon){this.params.faIcon = faIcon;return this}
    build(){return <CardOptionStatusOverlay status={this.params.status} faIcon={this.params.faIcon} />}

}