import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import StatusOverlayBuilder from "./overlays/StatusOverlayBuilder";
import * as Constants from '../../shared/Constants'

export default class AbstractCardOptionOverlaysBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({overlays:[]}, bc)
    }

    addView(view){this.params.overlays.push(view);return this}
    createWarningBanner(){return new StatusOverlayBuilder('warning', this.addView.bind(this))}
    createWarningBannerWithIcon(){ return this.createWarningBanner().setFaIcon("fa-warning") }
    createSuccessBanner(){return new StatusOverlayBuilder('success', this.addView.bind(this))}
    createSuccessBannerWithIcon(){return this.createSuccessBanner().setFaIcon("fa-check-circle")}
    createErrorBanner(){return new StatusOverlayBuilder('error', this.addView.bind(this))}
    createErrorBannerWithIcon(){return this.createErrorBanner().setFaIcon("fa-exclamation-circle")}
    createWaitingBanner(){
        return new StatusOverlayBuilder('primary', this.addView.bind(this))
            .setFaIcon("fa-ellipsis-h")
    }
    createRunningBanner(){
        return new StatusOverlayBuilder('primary', this.addView.bind(this))
            .setFaIcon("fa-refresh fa-spin fa-fw")
    }
    createStoppedBanner(){
        return new StatusOverlayBuilder('default', this.addView.bind(this))
            .setFaIcon("fa-ban color-default-text")
    }
    createStatusBanner(status) {
        switch (status) {
            case Constants.STATUS_STOPPED:
                return this.createStoppedBanner()
            case Constants.STATUS_WAITING:
                return this.createWaitingBanner()
            case Constants.STATUS_RUNNING:
                return this.createRunningBanner()
            case Constants.STATUS_WARNING:
                return this.createWarningBanner()
            case Constants.STATUS_ERROR:
                return this.createErrorBanner()
        }
        return this.createSuccessBanner()
    }
    createStatusBannerWithIcon(status) {
        switch (status) {
            case Constants.STATUS_STOPPED:
                return this.createStoppedBanner()
            case Constants.STATUS_WAITING:
                return this.createWaitingBanner()
            case Constants.STATUS_RUNNING:
                return this.createRunningBanner()
            case Constants.STATUS_WARNING:
                return this.createWarningBannerWithIcon()
            case Constants.STATUS_ERROR:
                return this.createErrorBannerWithIcon()
        }
        return this.createSuccessBannerWithIcon()
    }
    build(){
        return this.params.overlays
    }
}