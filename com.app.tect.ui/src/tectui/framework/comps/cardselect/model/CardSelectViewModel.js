import React from 'react'
import ViewModel from "../../shared/model/ViewModel";
import CardOptionView from '../view/CardOptionView'

export default class CardSelectViewModel extends ViewModel {

    constructor(parent) {
        super(parent);
    }
    state(){return this.parent.props.params}
    heading() {return this.state().heading}
    cardOptionComps(){return this.state().options.map( option => this.__createOptionComp(option) )}

    __createOptionComp(option) {
        const { label, action, data } = option
        const dataViewRenderer = this.state().dataViewRenderer
        const overlays = dataViewRenderer ? dataViewRenderer.render(data) : null
        return <CardOptionView data={data} label={label} action={action} overlays={overlays} />
    }
}