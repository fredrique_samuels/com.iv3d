import LoadingModelUpdater from "./LoadingModelUpdater";

export default class LoadingModel {
    constructor(parent, settings={}) {
        this.parent = parent
        this.settings =  settings
    }
    newState(){return {text:"Loading...", progress:0.0, done:false, error:false}}
    state(){return this.parent.state}
    setState(s){return this.parent.setState(s)}
    uicontext(){return (this.parent.props.layerModel?this.parent.props.layerModel.contentParams.uicontext : null)|| this.parent.props.uicontext }
    autoClose(){return this.settings.autoClose}
    done(){return this.state().done}
    error(){return this.state().error}
    text(){return this.state().text}
    progress(){return this.state().progress}
    progressStrPerc(deci=2){return this.state().progress.toFixed(deci)+"%"}
    busy(){return !this.done() && !this.error()}
    success(){return this.done() && !this.error()}
    close(){
        this.__closeLayerOld()
        if(this.settings.closeHandler) {
            this.settings.closeHandler()
        }
    }
    update() {
        return new LoadingModelUpdater(this)
    }

    __closeLayerOld(){
        const {layerModel} = this.parent.props
        if(!layerModel)return

        const { layerId } = this.parent.props.layerModel
        const { uicontext } = this.parent.props.layerModel.contentParams
        this.uicontext().layerComponentManager().removeLayer(layerId)
    }
}


