

export default class LoadingModelUpdater {
    constructor(model) {
        this.params = Object.assign({}, model.state())
        this.model = model
    }
    setText(v){this.params.text = v;return this}
    setProgress(v){this.params.progress = v;return this}
    setDone(){this.params.done = true;this.params.progress=100;return this}
    setError(error){this.params.error = true;return this}
    run() {
        if (this.model.autoClose() && ( this.params.done || this.params.error )) {
            this.model.close()
        } else {
            this.model.setState(this.params)
        }
    }
}