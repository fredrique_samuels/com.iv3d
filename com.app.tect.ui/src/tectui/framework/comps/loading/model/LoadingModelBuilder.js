import LoadingModel from "./LoadingModel";

export default class LoadingModelBuilder {
    constructor(parent) {
        this.autoClose = false;
        this.parent = parent
        this.closeHandler = null
    }
    setAutoClose(){this.autoClose=true;return this}
    setCloseHandler(h){this.closeHandler=h;return this}
    build(){return new LoadingModel(this.parent, {autoClose:this.autoClose, closeHandler:this.closeHandler})}
}
