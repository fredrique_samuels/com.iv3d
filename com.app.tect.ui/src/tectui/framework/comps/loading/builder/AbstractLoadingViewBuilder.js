import React from 'react'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import Timeout from "../../../Timeout";
import SpinLoadingView from '../view/SpinLoadingView'
import ProgressLoadingView from '../view/ProgressLoadingView'


export default class AbstractLoadingViewBuilder extends AbstractParamsBuilder {
    constructor(cb) {
        super({
            mode:'spin',
            controller: {
                start: (model) => new Timeout(1000, () => model.update().setDone().run())
            },
            text:null,
            autoClose:false,
            closeHandler:null
        }, cb)
    }

    setController(c){
        this.params.controller = c
        return this
    }

    setAutoClose() {
        this.params.autoClose = true
        return this
    }

    setProgressMode() {
        this.params.mode='progress'
        return this
    }

    buildView(uicontext) {
        const params = this.build()
        if(params.mode=='spin')
            return <SpinLoadingView text={params.text} closeHandler={params.closeHandler} controller={params.controller} autoClose={params.autoClose} />
        return <ProgressLoadingView controller={params.controller} closeHandler={params.closeHandler} autoClose={params.autoClose} />
    }

    buildViewToLayer(uicontext) {

        var contentParams = {
            contentHandlerFactory: {
                createContent:(layerModel) => {
                        const params = layerModel.contentParams.loadingParams
                        if(params.mode=='spin')
                            return <SpinLoadingView layerModel={layerModel} controller={params.controller} autoClose={params.autoClose} />
                        return <ProgressLoadingView  layerModel={layerModel} controller={params.controller} autoClose={params.autoClose} />
                    }
            },
            layoutParams: {
                position:"relative",
                minWidth:"40%",
                maxHeight:"80%",
                backgroundColor:"rgba(0,0,0,0)"
            },
            loadingParams : this.build()
        };
        return uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:false})
    }
}