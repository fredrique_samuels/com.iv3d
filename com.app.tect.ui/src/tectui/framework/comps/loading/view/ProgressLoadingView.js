import React from 'react'
import LoadingModelBuilder from "../model/LoadingModelBuilder";
import {BorderPanel} from './../../shared/view/BorderPanel'

class SuccessCheck extends React.Component {
    render() {
        const {model} = this.props
        if(!model.success())return null;
        return (
            <div style={{width: "100%", textAlign: "center"}}>
                <i class="fa-action fa fa-check-circle-o color-success-text fa-2x" onClick={model.close.bind(model)}></i>
            </div>
        )
    }
}

class FailCross extends React.Component {
    render() {
        const {model} = this.props
        if(!model.error())return null;
        return (
            <div style={{width: "100%", textAlign: "center"}}>
                <i class="fa-action fa fa-times-circle-o color-error-text fa-2x" onClick={model.close.bind(model)}></i>
            </div>
        )
    }
}

export default class ProgressLoadingView extends React.Component {

    render() {
        const model = this.createModel();
        const meterClasses = ["progress-meter"]
        if (model.busy()) {
            meterClasses.push("progress-meter-orange")
        } else {
            meterClasses.push("progress-meter-complete")
        }

        if (model.error()) {
            meterClasses.push("progress-meter-red")
        }

        return (
            <BorderPanel>
                <div style={{textShadow:"0px 0px 20px black", color:"white", position:"relative", width:"100%"}} >
                    <div class={meterClasses.join(" ")} style={{marginRight:"10%", marginLeft:"10%", width:"80%", height:"30px"}}>
                        <span style={{width:model.progressStrPerc()}} ></span>
                    </div>
                    <div style={{marginLeft:"10%"}} >{model.text()}</div>
                    <SuccessCheck model={model} />
                    <FailCross model={model} />
                </div>
            </BorderPanel>
        )
    }

    componentWillMount() {
        const {controller} = this.props
        const model = this.createModel()
        this.state = model.newState()
        if(controller) {
            controller.start(model)
        }
    }

    componentWillUnmount() {
        const {controller} = this.props
        const model = this.createModel()
        if(controller && model.busy() && controller.stop)controller.stop()
    }

    createModel() {
        let loadingModelBuilder = new LoadingModelBuilder(this);
        if(this.props.autoClose) {
            loadingModelBuilder = loadingModelBuilder.setAutoClose()
        }
        if(this.props.closeHandler) {
            loadingModelBuilder = loadingModelBuilder.setCloseHandler(this.props.closeHandler)
        }
        return loadingModelBuilder
            .build()
    }
}