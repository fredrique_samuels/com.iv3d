import React from 'react'
import LoadingModelBuilder from "../model/LoadingModelBuilder";
import {BorderPanel} from './../../shared/view/BorderPanel'

export default  class SpinLoadingView extends React.Component {

    componentWillMount() {
        const {controller} = this.props
        const model = this.model()
        this.state = model.newState()
        if(controller) {
            controller.start(model)
        }
    }

    componentWillUnmount() {
        const {controller} = this.props
        const model = this.model()
        if(controller && model.busy() && controller.stop)controller.stop()
    }

    render() {
        const {controller, text} = this.props
        const model = this.model();
        return (
            <BorderPanel>
                <div style={{textShadow:"0px 0px 20px black", color:"white", position:"relative"}} >
                    <div class="center-content-h" style={{width:"100%"}}>
                        <i class="tt-status-icon fa fa-refresh fa-spin fa-fw fa-2x color-primary-text"></i>
                    </div>
                    <div style={{width:"100%", textAlign:"center"}}>
                        {controller?model.text():text}
                    </div>
                </div>
            </BorderPanel>
        )
    }
    model() {
        return new LoadingModelBuilder(this)
            .setAutoClose()
            .build()
    }
}