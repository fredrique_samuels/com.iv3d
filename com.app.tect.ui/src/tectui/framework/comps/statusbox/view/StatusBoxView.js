import React from 'react'
import TsbStyleCache from "../styles/TsbStyleCache";
import StatusBoxViewModel from "../model/StatusBoxViewModel";

export default class StatusBoxView extends React.Component {

    render() {
        const styleCache = new TsbStyleCache();
        const boxModel = new StatusBoxViewModel(this);

        return (
            <div class="fill-parent" style={styleCache.get('tsb-ctnr-panel')}>
                {this.__headingComp(boxModel, styleCache)}
                {this.__statusComp(boxModel, styleCache)}
                <div style={{flex:"1", width:"100%", overflowY:"scroll"}}>
                {boxModel.content()}
                </div>
            </div>
        )
    }

    __headingComp(boxModel, styleCache) {
        const heading = boxModel.heading();
        if(heading) {
            return <div class="text-heading-large" style={styleCache.get('tsb-heading-cntr')}>{heading}</div>
        }
        return null
    }

    __statusComp(boxModel, styleCache) {
        const status = boxModel.status();
        const statusText = boxModel.statusText();

        if(status) {
            return <div class={"color-" + status + " panel-font"} style={styleCache.get('tsb-status')}>{statusText}</div>
        }
        return null
    }
}