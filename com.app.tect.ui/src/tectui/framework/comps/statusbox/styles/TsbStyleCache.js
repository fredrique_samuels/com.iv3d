import AbstractStyleCache from "../../shared/styles/AbstractStyleCache";

export default class TsbStyleCache extends AbstractStyleCache {
    constructor(uicontext)  {
        super(uicontext)

        this.set('tsb-ctnr-panel', {
            minWidth:"300px",
            minHeight:"300px",
            flexFlow: "column",
            display:"flex"
        })
        this.set('tsb-heading-cntr', {
            width:"100%",
            padding:"5px 5px 5px 5px"
        })
        this.set('tsb-status', {
            width:"100%",
            minHeight:"46px",
            borderBottomWidth:"6px",
            borderTopWidth:"6px",
            borderLeftWidth:"0px",
            borderRightWidth:"0px",
            borderStyle:"solid",
            fontWeight:"bold",
            fontSize:"16px",
            paddingLeft:"5px",
            display:"flex",
            justifyContent:"flexStart",
            alignItems:"center",
            boxSizing:"border-box"
        })

    }
}