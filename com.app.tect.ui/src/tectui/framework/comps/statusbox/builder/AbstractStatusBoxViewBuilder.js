import React from 'react'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import StatusBoxView from '../view/StatusBoxView'
import {AbstractTitledPanelViewBuilder} from "../../titledpanel/builder/AbstractTitledPanelViewBuilder";


export default class AbstractStatusBoxViewBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({heading:null, statusText:null, status:null, content:null}, bc)
    }

    setHeading(t){ this.params.heading = t;return this}
    setStatus(status) {this.params.status = status; return this}
    setStatusText(status) {this.params.statusText = status; return this}
    setContent(c) {this.params.content = c; return this}
    buildView(uicontext){ return <StatusBoxView params={this.build()} uicontext={uicontext} /> }
    buildViewToLayer(uicontext) {
        new AbstractTitledPanelViewBuilder()
            .setContent(<StatusBoxView params={this.build()} uicontext={uicontext} />)
            .setStyleProperty({width:"90%", height:"90%"})
            .buildViewToLayer(uicontext)
    }

}
