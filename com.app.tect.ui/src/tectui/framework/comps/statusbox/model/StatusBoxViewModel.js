import ViewModel from "../../shared/model/ViewModel";

export default class StatusBoxViewModel extends ViewModel {

    constructor(parent, statusBoxParams) {
        super(parent);
        this.params = statusBoxParams ? statusBoxParams : parent.props.params
    }
    status(){return this.params.status}
    heading(){return this.params.heading}
    statusText(){return this.params.statusText}
    content() {return this.params.content}
}
