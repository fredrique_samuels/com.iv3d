import React from 'react'

export default class TabContent extends React.Component {
    render() {
        const { tabsModel, tabIndex, view } = this.props
        const active = tabsModel.activeTabIndex()==tabIndex
        return (
            <div style={{width:"100%", display:active?"block":"none", maxHeight:"100%", overflowY:"auto"}}>
                {view}
            </div>
        )
    }
}