import React from 'react'
import TabsViewModel from "../model/TabsViewModel";

export default class TabsView extends React.Component {

    componentWillMount() {
        this.state = new TabsViewModel(this).newState()
    }

    render() {
        const model = new TabsViewModel(this)
        return (
            <div style={{width:"100%", padding:"5px", height:"100%"}}>
                <div style={{width:"100%"}}>
                    {model.buttonComps()}
                </div>
                <div style={{position:"relative",width:"100%", height:"calc(100% - 35px)"}}>
                    {model.viewComps()}
                </div>
            </div>
        )
    }
}
