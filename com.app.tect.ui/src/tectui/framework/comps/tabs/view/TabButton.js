import React from 'react'

export default class TabButton extends React.Component {
    render() {
        const { tabsModel, tabIndex, title} = this.props
        const active = tabsModel.activeTabIndex()==tabIndex
        const colorClass = active ? "color-primary" : "color-disabled"

        const cssClasses = "btn btn-sm " + colorClass
        const event = this.clicked.bind(this)
        const text = title ? title : "Tab " + (tabIndex + 1)
        const css = {
            marginRight:"1px",
            borderBottomRightRadius:"0px",
            borderBottomLeftRadius:"0px"
        }
        return (
            <div class="inline-element">
                <button onClick={event} style={css} class={cssClasses} >{text}</button>
            </div>
        )
    }
    clicked() {
        const { tabsModel, tabIndex} = this.props
        tabsModel.setTabActive(tabIndex)
    }
}