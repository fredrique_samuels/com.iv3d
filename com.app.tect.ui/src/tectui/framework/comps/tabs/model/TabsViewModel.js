import React from 'react'
import ViewModel from "../../shared/model/ViewModel";
import TabContent from './../view/TabContent'
import TabButton from './../view/TabButton'

export default class TabsViewModel extends ViewModel {
    constructor(parent) {
        super(parent)
    }

    newState(){
        const { params } = this.parent.props
        const { displays } = params
        return {
            tabIndex: (displays && displays.length) > 0 ? 0 : -1,
            tabCount: (displays && displays.length) > 0 ? displays.length : 0
        }
    }
    viewComps() {
        return this.displays().map( (d, index) => <TabContent view={d.content} tabIndex={index} tabsModel={this}/> )
    }
    buttonComps() {
        if(this.count()<1) return []
        return this.displays().map( (d, index) => <TabButton title={d.title} tabIndex={index} tabsModel={this}/> )
    }
    displays(){
        const { params } = this.parent.props
        if(!params)return []

        const { displays } = params
        if(!displays)return []

        return displays
    }
    count(){return this.displays().length}
    activeTabIndex(){ return this.state().tabIndex}
    setTabActive(index){
        this.setState({tabIndex:index})
    }
}