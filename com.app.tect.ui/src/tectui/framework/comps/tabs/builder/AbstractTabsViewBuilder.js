import React from 'react'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import TabsView from '../view/TabsView'
import {AbstractTitledPanelViewBuilder} from "../../titledpanel/builder/AbstractTitledPanelViewBuilder";

export default class AbstractTabsViewBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({displays:[]}, bc)
    }

    addTab(title, content){this.params.displays.push({title:title, content:content});return this}
    buildView(uicontext){return <TabsView params={this.build()} uicontext={uicontext} />}
    buildViewToLayer(uicontext) {
        new AbstractTitledPanelViewBuilder()
            .setContent(<div class="fill-parent"><TabsView params={this.build()} uicontext={uicontext} /></div>)
            .setStyleProperty({width:"90%", height:"90%"})
            .buildViewToLayer(uicontext)

    }
}