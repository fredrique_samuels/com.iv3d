import React from 'react'
import * as FormConstants from './FormConstants'
import {FormView} from './view/FormView'
import {FormInputFactory} from "./view/FormInputFactory";


class FormContentFactory {
    createContent(layerModel) {
        return <FormView form={layerModel.contentParams.form} layerModel={layerModel} />
    }
}

export class FormComponentManager  {
  constructor(uicontext) {
    this.uicontext = uicontext
  }

  createForm(form){
    return <FormView form={form} />
  }

  __createInput(viewParams, inputType){return new FormInputFactory().build(Object.assign({}, viewParams, {type:inputType}))}
  createTextInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_TEXT)}
  createTextAreaInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_TEXTAREA)}
  createMultiSelect(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT)}
  createSingleSelect(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT)}
  createDropdownSelect(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT)}
  createColorInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_COLOR)}
  createUrlInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_URL)}
  createEmailInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_EMAIL)}
  createFileInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_FILE)}
  createPasswordInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_PASSWORD)}
  createNumberInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_NUMBER)}

  createBlockTextDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT)}
  createTextDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_TEXT)}
  createImageDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_IMAGE)}
  createInfoBlockDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK)}

  createLayeredForm(form, layoutParams={}) {

    const defaultLayoutParams = {
        position:"relative",
        top:"25%",
        left:"20%",
        width:"60%",
        maxHeight:"50%",
        overflow:"auto",
        backgroundColor:"rgba(0,0,0,.7)",
        boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
    }


    var contentParams = {
        contentHandlerFactory: new FormContentFactory(),
        layoutParams: Object.assign({},defaultLayoutParams, layoutParams),
        form : form
    };
    return this.uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:false})
  }
  containsCancelInput(form) {
      const { actions } = form
      if(actions) {
          for( let aIndex in actions) {
              if (actions[aIndex].type==FormConstants.FORM_ACTION_TYPE_CANCEL) {
                  return true
              }
          }
      }
      return false
  }

}
