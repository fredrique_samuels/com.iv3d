export class FormInputIterator {
    iterate(inputs, callback)  {
        if(inputs) {
            return inputs.map(p => {
                callback(p)
                this.iterate(p.inputs, callback)
                return null
            })
        }
        return []
    }
}