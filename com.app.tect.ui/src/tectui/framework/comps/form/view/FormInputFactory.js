import React from 'react'

import TextInput from './input/TextInput'
import TextArea from './input/TextArea'
import NumberInput from './input/NumberInput'
import MultiSelect from './input/MultiSelect'
import SingleSelect from './input/SingleSelect'
import DropdownSelect from './input/DropdownSelect'
import FileInput from './input/FileInput'
import PasswordInput from './input/PasswordInput'
import ColorInput from './input/ColorInput'
import UrlInput from './input/UrlInput'
import EmailInput from './input/EmailInput'
import FormSection from './input/FormSection'

import BlockTextDisplay from './display/BlockTextDisplay'
import TextDisplay from './display/TextDisplay'
import ImageDisplay from './display/ImageDisplay'
import InfoBlock from './display/InfoBlock'

import * as FormConstants from "../FormConstants";

export class FormInputFactory {
    buildFromInputs(inputs) {
        if(inputs) {
            return inputs.map(p => this.build(p))
        }
        return []
    }
    build(viewParams) {
        if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXT) {
            return <TextInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXTAREA) {
            return <TextArea viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT) {
            return <MultiSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT) {
            return <SingleSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT) {
            return <DropdownSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FILE) {
            return <FileInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_COLOR) {
            return <ColorInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_URL) {
            return <UrlInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_EMAIL) {
            return <EmailInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_PASSWORD) {
            return <PasswordInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_NUMBER) {
            return <NumberInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FORMSECTION) {
            return ( <FormSection viewParams={viewParams} >
                    {this.buildFromInputs(viewParams.inputs)}
                </FormSection>
            )
        }

        if(viewParams.type==FormConstants.FORM_USER_TYPE_CUSTOM) {
            return <div style={{width:"100%"}}>{viewParams.data}</div>
        }

        if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT) {
            return <BlockTextDisplay viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_IMAGE) {
            return <ImageDisplay viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK) {
            return <InfoBlock viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_TEXT) {
            return <TextDisplay textParams={viewParams} />
        }

        return null
    }

    buildInput(viewParams) {
        if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXT) {
            return <TextInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXTAREA) {
            return <TextArea viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT) {
            return <MultiSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT) {
            return <SingleSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT) {
            return <DropdownSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FILE) {
            return <FileInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_COLOR) {
            return <ColorInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_URL) {
            return <UrlInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_EMAIL) {
            return <EmailInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_PASSWORD) {
            return <PasswordInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_NUMBER) {
            return <NumberInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FORMSECTION) {
            return ( <FormSection viewParams={viewParams} >
                    {this.buildFromInputs(viewParams.inputs)}
                </FormSection>
            )
        }
        return null
    }
}