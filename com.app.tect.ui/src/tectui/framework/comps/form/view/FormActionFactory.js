import React from 'react'
import * as FormConstants from "../FormConstants";

export class FormActionFactory {
    buildFromActions(parent, actions) {
        if(actions) {
            return actions.map(p => this.build(parent, p))
        }
        return []
    }
    build(form, viewParams) {
        if(viewParams.type==FormConstants.FORM_ACTION_TYPE_CANCEL) {
            return  <button type="button" class="form-button btn btn-error btn-sm" onClick={form.close.bind(form)} >Cancel</button>
        } else if(viewParams.type==FormConstants.FORM_ACTION_TYPE_ACTION) {
            return  <button type="button" class="form-button btn btn-error btn-sm" onClick={(event) => {viewParams.action(event, form)}} >{viewParams.title}</button>
        } else if(viewParams.type==FormConstants.FORM_ACTION_TYPE_SUBMIT) {
            return <input class="form-button btn btn-primary btn-sm" id={form.submitButtonId} type="submit" defaultValue={viewParams.title}/>
        }
        return null
    }
}