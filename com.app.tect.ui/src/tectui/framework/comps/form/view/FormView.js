import React from 'react'
import {FormInputIdGenerator} from "../FormConstants";
import {FormInputFactory} from "./FormInputFactory";
import {FormActionFactory} from "./FormActionFactory";
import {FormInputIterator} from "./FormInputIterator";
import ErrorText from "./input/ErrorText";

class FormValidationResult {

    constructor(cb) {
        this.cb = cb
        this.params = {
            fieldErrors : [],
            error: null
        }
    }
    addFieldError(field, message) {
        this.params.fieldErrors.push({field:field, message:message})
        return this
    }
    addAllFieldErrors(errors) {
        for( var e in errors)
            this.addFieldError(errors[e].field, errors[e].message)
        return this
    }
    setError(e) {
        this.params.error = e
        return this
    }
    commit(){this.cb(this.params)}

}

export class FormView extends React.Component {

    constructor() {
        super()
        this.formId = FormInputIdGenerator.generate()
    }

    render() {
        const style={
            backgroundColor:"grey"
        }

        const containerStyle = {
            position:"relative",
            width:"100%",
            padding:"12px"
        }

        const items = new FormInputFactory().buildFromInputs(this.state.form.inputs)
        const actions = new FormActionFactory().buildFromActions(this, this.state.form.actions)

        const formContent = (<div style={containerStyle}>
            <div class="panel-text" style={{width:"100%"}}>
                <form id={this.state.form.id?this.state.form.id:this.formId}>
                    {this.state.form.error?<ErrorText text={this.state.form.error} />:null}
                    {items}
                    {actions}
                </form>
            </div>
        </div>)
        return formContent
    }

    componentWillMount() {
        this.state = {form : this.props.form}
    }

    componentDidMount() {
        const submitCallback =  this.submit.bind(this)
        const callback = function(event)
        {
            console.log('FormComponent', event)
            var theForm = $(this);
            submitCallback(theForm.serialize(), theForm.serializeArray())
            return false;
        }
        $('#' + this.formId).submit(callback);
    }

    submit(data, dataArray) {
        const dataMap = {}
        dataArray.map(p => dataMap[p.name] = p.value)
        const validation = this.props.form.validation
        if(validation) {
            const { type, url, enctype, callback } = validation
            if(type=='callback') {
                try {
                    const formResult = new FormValidationResult((result => {
                        if(result.fieldErrors.length==0 && result.error==null) {
                            this.submitForm(data, dataArray, dataMap)
                        }
                        else {
                            this.processValidationErrors(result)
                        }
                    }));
                    callback({data, dataArray, dataMap}, formResult)
                } catch (e){
                    console.error("An error occured", e);
                }
            }
        } else {
            try {
                this.submitForm(data, dataArray, dataMap)
            } catch (e){
                console.error("An error occured", e);
            }
        }
    }

    submitForm(data, dataArray, dataMap) {
        const submit = this.props.form.submit
        if(submit) {
            const { type, url, enctype, callback } = submit
            if(type=='callback') {

                const formResult = new FormValidationResult((result => {
                    if(result.fieldErrors.length==0 && (result.error==null || result.error=="")) {
                        this.close()
                    }
                    else {
                        this.processValidationErrors(result)
                    }
                }));

                try {
                    callback({data, dataArray, dataMap}, formResult)
                } catch (e){
                    console.error("An error occured", e);
                }
            }
        } else {
            this.close()
        }
    }

    processValidationErrors(result) {
        const errors  = result.fieldErrors
        const newState = Object.assign({}, this.state.form, {error:result.error})
        new FormInputIterator().iterate(newState.inputs, function(input) {
            input.errorText=null
            for(let v in errors) {
                if(errors[v].field==input.name && input.name) {
                    input.errorText = errors[v].message
                }
            }
        })
        this.setState({form:newState})
    }

    close()  {
        if(this.props.layerModel) {
            const { layerId } = this.props.layerModel
            const { uicontext } = this.props.layerModel.contentParams
            uicontext.layerComponentManager().removeLayer(layerId)
        }

        const closeHandler =  this.state.form.closeHandler
        if(closeHandler) {
            closeHandler()
        }
    }

}
