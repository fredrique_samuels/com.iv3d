import React from 'react'

import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'

export default class ColorInput extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { name, value } = this.props.viewParams
        const containerViewParams = Object.assign({}, this.props.viewParams, {ignoreError:true})
        return (
            <FormInputContainer viewParams={containerViewParams}>
                <input type="color" defaultValue={value} name={name} class="form-control"/>
            </FormInputContainer>
        )
    }
}