import React from 'react'

export default class HelpIcon extends React.Component {
    render() {
        return <i style={{fontSize:"18px"}} onClick={this.onclick.bind(this)} class="form-help fa fa-question-circle "></i>
    }

    onclick() {
        const {actionHandler, text} = this.props
        if(actionHandler) {
            actionHandler(text)
        }
    }
}