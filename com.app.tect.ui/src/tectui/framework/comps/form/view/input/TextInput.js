
import React from 'react'
import SingleLineInput from './SingleLineInput'


export default class TextInput extends React.Component {
    render() {
        const lineParams = Object.assign({}, this.props.viewParams, {inputType:"text"})
        return <SingleLineInput viewParams={lineParams} />
    }
}
