import React from 'react'
import RequiredText from './RequiredText'
import HelpIcon from './HelpIcon'

export default class FormGroup extends React.Component {
    render() {
        const {title, inputComp, helpActionHandler, noTitle} = this.props.viewParams
        return (<div class="form-group">
            {noTitle ? null :
                <div class="form-label-container">
                    <RequiredText viewParams={this.props.viewParams}/>
                    <label class="form-label">{title}</label>
                    {this.props.viewParams.helpText?<HelpIcon actionHandler={helpActionHandler} text={this.props.viewParams.helpText}/>:null}
                </div>
            }
            {inputComp}
        </div>)
    }
}