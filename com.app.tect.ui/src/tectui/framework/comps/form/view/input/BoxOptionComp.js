import React from 'react'

export default class BoxOptionComp extends React.Component {
    render(){
        const {name, option, optionsType, value} = this.props.viewParams
        const checked = this.isChecked(value, option)
        return <div><input name={name} defaultValue={option.value} defaultChecked={checked} type={optionsType} /><label class="form-list-select-label">{option.display?option.display:""}</label></div>
    }
    isChecked(value, option) {
        if(Array.isArray(value)) {
            for (var v in value) {
                if(value[v]==option.value)
                    return true
            }
        }
        return value==option.value
    }
}