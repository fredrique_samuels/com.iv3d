import React from 'react'

import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'
import SingleLineInputAddon from './SingleLineInputAddon'
import ExampleText from './ExampleText'

import { FormInputIdGenerator } from '../../FormConstants'

export default class SingleLineInput extends React.Component {
    constructor() {
        super()
        this.id = FormInputIdGenerator.generate()
    }
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { name, value, min, max, placeholder, exampleText, addonLeft, addonRight, inputType, dataList, onChangeAction } = this.props.viewParams
        return (
            <FormInputContainer viewParams={this.getContainerViewParams()}>
                <SingleLineInputAddon dir="-left" value={addonLeft} />
                <input onChange={onChangeAction} list={this.id} name={name} defaultValue={value} type={inputType} placeholder={placeholder} class={this.getInputClasses()} min={min} max={max} />
                <SingleLineInputAddon dir="-right" value={addonRight} />
                <ExampleText text={exampleText} />
                {this.getDataList(dataList)}
            </FormInputContainer>
        )
    }
    getDataList(list) {
        if(list) {
            const optionComps = list.map( x => <option value={x}/> )
            return (<datalist id={this.id}>
                {optionComps}
            </datalist>)
        }
        return null
    }

    getInputClasses() {
        const { addonLeft, addonRight } = this.props.viewParams
        let c = "form-control"
        if(addonLeft) {
            c+= " form-control-has-left-addon"
        }
        if(addonRight) {
            c+= " form-control-has-right-addon"
        }
        return c
    }
    getContainerViewParams() {
        const formInputContainParams = {inputContainerCssClasses:this.getContainerCssClasses()}
        return Object.assign({}, this.props.viewParams, formInputContainParams)
    }
    getContainerCssClasses() {
        const { addonLeft, addonRight } = this.props.viewParams
        const inputContainerCssClasses = []
        if(addonLeft || addonRight) {
            inputContainerCssClasses.push("input-group")
        }
        return inputContainerCssClasses
    }
}