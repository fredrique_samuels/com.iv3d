import React from 'react'

export default class SingleLineInputAddon extends React.Component {
    render() {
        if(this.props.value) {
            const { faIcon, text} = this.props.value
            const c = "input-group-addon input-group-addon" + this.props.dir
            if(faIcon)
                return <span class={c}><i class={"fa " + faIcon }></i></span>
            else if(text)
                return <span class={c}>{text}</span>
        }
        return null
    }
}
