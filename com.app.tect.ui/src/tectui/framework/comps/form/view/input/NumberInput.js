import React from 'react'
import SingleLineInput from './SingleLineInput'

export default class NumberInput extends React.Component {
    render() {
        const lineParams = Object.assign({}, this.props.viewParams, {inputType:"number"})
        return <SingleLineInput viewParams={lineParams} />
    }
}