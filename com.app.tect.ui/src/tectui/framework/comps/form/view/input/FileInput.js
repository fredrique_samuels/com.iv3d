import React from 'react'

import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'
import * as tectui from "../../../../../tectui";


export default class FileInput extends React.Component {

    constructor() {
        super()
        this.widgetId = tectui.generateUniqueUiId();
    }

    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { name } = this.props.viewParams
        return (
            <FormInputContainer viewParams={this.props.viewParams}>
                <div class="form-control-file-input-container">
                    <input name={name} id={this.widgetId} onChange={this.onChange.bind(this)} type="file" class="form-control"/>
                </div>
                <i class="file-input-left-corner"></i>
            </FormInputContainer>
        )
    }
    onChange() {
        const { onChangeAction } = this.props.viewParams

        const node = document.getElementById(this.widgetId);
        if(onChangeAction) {
            onChangeAction(node)
        }
    }
}