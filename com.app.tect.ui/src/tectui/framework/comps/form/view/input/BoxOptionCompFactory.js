import React from 'react'
import BoxOptionComp from './BoxOptionComp'

export default class BoxOptionCompFactory {
    build(viewParams) {
        const {options, name, optionsType, value} = viewParams
        if(options) {
            return options.map( p => <BoxOptionComp viewParams={{value:value, optionsType:optionsType, option:p, name:name}} ></BoxOptionComp> )
        }
        return null
    }
}