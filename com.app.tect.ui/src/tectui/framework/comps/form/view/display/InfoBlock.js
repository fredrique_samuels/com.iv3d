import React from 'react'
import FormGroup from '../input/FormGroup'
import FormInputContainer from '../input/FormInputContainer'

export default class InfoBlock extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { heading, graphics, text  } = this.props.viewParams
        const containerViewParams = Object.assign({}, this.props.viewParams, {ignoreError:true})

        const headingComp = heading?<h1 class="text-heading">{heading}</h1>:null
        const graphicComp = graphics?(<div style={this.infoCtnrCss()}>{graphics}</div>):null
        const textComp = text?<p class="panel-text">{text}</p>:null

        return (
            <FormInputContainer viewParams={containerViewParams} >
                {graphicComp}
                {headingComp}
                {textComp}
            </FormInputContainer>
        )
    }


    infoCtnrCss() {
        return {
            padding:"15px",
            float:"left",
            maxWidth:"45%",
            border:"2px solid slategrey",
            marginRight:"10px"
        }
    }
}