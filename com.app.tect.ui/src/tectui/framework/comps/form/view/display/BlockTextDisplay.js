import React from 'react'
import FormGroup from '../input/FormGroup'
import FormInputContainer from '../input/FormInputContainer'
import ExampleText from '../input/ExampleText'

export default class BlockTextDisplay extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { text } = this.props.viewParams
        const containerViewParams = Object.assign({}, this.props.viewParams, {ignoreError:true})
        return (
            <FormInputContainer viewParams={containerViewParams}>
                <ExampleText text={text}/>
            </FormInputContainer>
        )
    }
}