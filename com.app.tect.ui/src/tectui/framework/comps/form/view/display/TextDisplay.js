import React from 'react'
import FormGroup from '../input/FormGroup'
import FormInputContainer from '../input/FormInputContainer'

export default class TextDisplay extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.textParams, {inputComp})} />
    }
    createView() {
        const { text } = this.props.textParams
        const containerViewParams = Object.assign({}, this.props.textParams, {ignoreError:true})
        return (
            <FormInputContainer viewParams={containerViewParams}>
                <div class="form-text-value-cntr">
                    <label class="form-label">{text}</label>
                </div>
            </FormInputContainer>
        )
    }
}