import React from 'react'
import FormGroup from '../input/FormGroup'
import FormInputContainer from '../input/FormInputContainer'

export default class ImageDisplay extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { url } = this.props.viewParams
        const containerViewParams = Object.assign({}, this.props.viewParams, {ignoreError:true})

        return (
            <FormInputContainer viewParams={containerViewParams} style={this.getFicCss()}>
                <div style={this.imgCtnrCss()}>
                    <img style={this.imgCss()} src={url} />
                </div>
            </FormInputContainer>
        )
    }
    getFicCss(){
        const { height } = this.props.viewParams
        const ficStyle={
            display:'flex',
            justifyContent:'center',
            alignContent:'center'
        }
        if(height) {
            ficStyle.height=height;
        }
        return ficStyle
    }

    imgCtnrCss() {
        let justifyContent = "center"
        const { justify } = this.props.viewParams
        if(justify) {
            if(justify=='LEFT') {
                justifyContent = 'flex-start'
            } else if(justify=='RIGHT') {
                justifyContent = 'flex-end'
            }
        }
        return {display:'flex',justifyContent:justifyContent,width:'100%',height:'100%'}
    }

    imgCss() {return {maxWidth:'100%',maxHeight:'100%'}}
}