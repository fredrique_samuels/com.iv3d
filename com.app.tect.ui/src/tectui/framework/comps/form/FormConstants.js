import IdGenerator from '../../IdGenerator'

export const FORM_INPUT_TYPE_TEXT = 'form.input.type.text'
export const FORM_INPUT_TYPE_TEXTAREA = 'form.input.type.textarea'
export const FORM_INPUT_TYPE_MULTIOPTION_SELECT = 'form.input.type.multiselect'
export const FORM_INPUT_TYPE_SINGLEOPTION_SELECT = 'form.input.type.singleselect'
export const FORM_INPUT_TYPE_DROPDOWN_SELECT = 'form.input.type.dropdownselect'
export const FORM_INPUT_TYPE_COLOR = 'form.input.type.color'
export const FORM_INPUT_TYPE_NUMBER = 'form.input.type.number'
export const FORM_INPUT_TYPE_URL = 'form.input.type.url'
export const FORM_INPUT_TYPE_EMAIL = 'form.input.type.email'
export const FORM_INPUT_TYPE_FILE = 'form.input.type.file'
export const FORM_INPUT_TYPE_PASSWORD = 'form.input.type.password'
export const FORM_INPUT_TYPE_FORMSECTION = 'form.input.type.formsection'

export const FORM_USER_TYPE_CUSTOM = 'form.user.type.custom'

export const FORM_DISPLAY_TYPE_BLOCK_TEXT = 'form.display.type.block.text'
export const FORM_DISPLAY_TYPE_TEXT = 'form.display.type.text'
export const FORM_DISPLAY_TYPE_IMAGE = 'form.display.type.image'
export const FORM_DISPLAY_TYPE_INFO_BLOCK = 'form.display.type.infoLogs.block'

export const FORM_ACTION_TYPE_CANCEL = 'form.action.type.cancel'
export const FORM_ACTION_TYPE_ACTION = 'form.action.type.action'
export const FORM_ACTION_TYPE_SUBMIT = 'form.action.type.submit'


export class FormInputIdGenerator {
    static generate() {
        return IdGenerator.generateGlobalId('ui_form_')
    }
}
