import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export default class SubmitButtonBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({
            title : "Submit",
            type : "form.action.type.submit"
        }, bc)
    }

    setLabel(c){
        this.params.title = c;
        return this
    }
}