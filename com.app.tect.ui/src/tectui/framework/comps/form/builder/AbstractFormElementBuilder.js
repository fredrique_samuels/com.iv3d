import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import AddonBuilder from "./AddonBuilder";
import OptionBuilder from "./OptionBuilder";
import {FormInputFactory} from "../view/FormInputFactory";
import * as FormConstants from "../FormConstants";

export default class AbstractFormElementBuilder extends AbstractParamsBuilder {

    constructor(type, bc) {
        super({
            type : type,
            title : null,
            placeholder : null,
            exampleText : null,
            helpText : null,
            errorText : null,
            name: null,
            value: null,
            addonLeft:null,
            addonRight:null,
            dataList:null,
            required:false,
            options:[],
            noTitle:false,
            min:'0.0',
            max:'0.0',
            onChangeAction:() => {},

            text : null,
            heading : null,
            graphics : null,
            url : null,
            justify : 'CENTER',
            noTitle:false,
            actionHandler:null,

            data:null

        },  bc)
    }

    setLabel(v){this.params.title = v; return this}
    setPlaceholder(v){this.params.placeholder = v; return this}
    setExampleText(v){this.params.exampleText = v; return this}
    setHelpText(v){this.params.helpText = v; return this}
    setErrorText(v){this.params.errorText = v; return this}
    setName(v){this.params.name = v; return this}
    setValue(v){this.params.value = v; return this}
    setDataList(v){this.params.dataList = v; return this}
    setRequired(){this.params.required = true; return this}
    setNoLabel(){this.params.noTitle = true; return this}
    setMin(v){this.params.min = new String(v); return this}
    setMax(v){this.params.max = new String(v); return this}
    setData(v){this.params.data = v; return this}
    setOnChangeCallback(v){this.params.onChangeAction = v; return this}
    setChangeHandler(v){return this.setOnChangeCallback(v)}
    setActionHandler(v){this.params.actionHandler = v; return this}
    createAddonLeft(){return new AddonBuilder(this.__setAddonLeft.bind(this))}
    createAddonRight(){return new AddonBuilder(this.__setAddonRight.bind(this))}
    createOption(){return new OptionBuilder(this.__addOption.bind(this))}

    setText(v){this.params.text = v; return this}
    setUrl(v){this.params.url = v; return this}
    setJustifyLeft(v){this.params.justify = 'LEFT'; return this}
    setJustifyRight(v){this.params.justify = 'RIGHT'; return this}
    setGraphics(v){this.params.graphics = v; return this}
    setTitle(v) {this.params.heading = v; return this}

    setTextInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_TEXT;return this}
    setTextAreaInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_TEXTAREA;return this}
    setMultiSelectType() {this.params.type=FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT;return this}
    setSingleSelectType() {this.params.type=FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT;return this}
    setDropdownSelectType() {this.params.type=FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT;return this}
    setColorInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_COLOR;return this}
    setUrlInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_URL;return this}
    setEmailInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_EMAIL;return this}
    setFileInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_FILE;return this}
    setPasswordInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_PASSWORD;return this}
    setNumberInputType() {this.params.type=FormConstants.FORM_INPUT_TYPE_NUMBER;return this}

    setCustomComponentType() {this.params.type=FormConstants.FORM_USER_TYPE_CUSTOM;return this}

    setBlockTextDisplayType() {this.params.type=FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT;return this}
    setTextDisplayType() {this.params.type=FormConstants.FORM_DISPLAY_TYPE_TEXT;return this}
    setImageDisplayType() {this.params.type=FormConstants.FORM_DISPLAY_TYPE_IMAGE;return this}
    setInfoBlockDisplayType() {this.params.type=FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK;return this}


    buildView() {
        return new FormInputFactory().build(this.build())
    }

    __addOption(o){this.params.options.push(o);return this}
    __setAddonLeft(v){this.params.addonLeft = v; return this}
    __setAddonRight(v){this.params.addonRight = v; return this}

}