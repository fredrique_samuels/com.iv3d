import FormContentBuilder from "./FormContentBuilder";
import * as FormConstants from "../FormConstants";

export default class FormSectionBuilder extends FormContentBuilder {

    constructor(bc=null) {
        super({
            type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
            title : null,
            heading : null,
            inputs:[]},
            bc);
    }

    setTitle(t){this.params.title = t;return this}
}