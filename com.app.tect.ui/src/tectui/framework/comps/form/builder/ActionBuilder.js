import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export default class ActionBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({
            type:null,
            callback:null
        }, bc)
    }

    setCallback(c){
        this.params.type = "callback";
        this.params.callback = c;
        return this
    }
}