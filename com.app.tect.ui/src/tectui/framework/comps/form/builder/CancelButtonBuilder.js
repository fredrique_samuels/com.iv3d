import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export class CancelButtonBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({
            title : "Cancel",
            type : "form.action.type.cancel"
        }, bc)
    }

    setLabel(c){
        this.params.title = c;
        return this
    }
}