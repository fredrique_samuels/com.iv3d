import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import * as FormConstants from "../FormConstants";
import AbstractFormElementBuilder from "./AbstractFormElementBuilder";


export default class FormContentBuilder extends AbstractParamsBuilder {
    constructor(p, bc=null) {
        super(p, bc)
    }

    createTextInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_TEXT, this.__addInput.bind(this)) }
    createPasswordInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_PASSWORD, this.__addInput.bind(this)) }
    createTextAreaInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_TEXTAREA, this.__addInput.bind(this)) }
    createMultiSelectInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT, this.__addInput.bind(this)) }
    createSingleSelectInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT, this.__addInput.bind(this)) }
    createListSelectInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT, this.__addInput.bind(this)) }
    createFileInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_FILE, this.__addInput.bind(this)) }
    createColorInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_COLOR, this.__addInput.bind(this)) }
    createEmailInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_EMAIL, this.__addInput.bind(this)) }
    createUrlInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_URL, this.__addInput.bind(this)) }
    createNumberInput(){ return new AbstractFormElementBuilder(FormConstants.FORM_INPUT_TYPE_NUMBER, this.__addInput.bind(this)) }

    addComponent(comp){return new AbstractFormElementBuilder(FormConstants.FORM_USER_TYPE_CUSTOM, this.__addInput.bind(this)).setData(comp).commit()}

    createBlockText(){ return new AbstractFormElementBuilder(FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT, this.__addInput.bind(this)) }
    createImage(){ return new AbstractFormElementBuilder(FormConstants.FORM_DISPLAY_TYPE_IMAGE, this.__addInput.bind(this)) }
    createInfoBlock(){ return new AbstractFormElementBuilder(FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK, this.__addInput.bind(this)) }
    createText(){ return new AbstractFormElementBuilder(FormConstants.FORM_DISPLAY_TYPE_TEXT, this.__addInput.bind(this)) }

    __addInput(i){this.params.inputs.push(i);return this}
}
