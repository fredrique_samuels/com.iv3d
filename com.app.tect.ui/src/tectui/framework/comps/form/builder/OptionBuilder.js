import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export default class OptionBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({display:"", value:""}, bc)
    }

    setLabel(l){this.params.display=l;return this}
    setValue(l){this.params.value=l;return this}
}