import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export class FormButtonBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({
            title : "Action",
            type : "form.action.type.action",
            action: null
        }, bc)
    }

    setLabel(c){
        this.params.title = c;
        return this
    }

    setCallback(cb) {
        this.params.action = cb
        return this
    }
}