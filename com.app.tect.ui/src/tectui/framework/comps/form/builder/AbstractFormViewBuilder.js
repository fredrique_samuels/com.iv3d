import React from 'react'
import $ from 'jquery'

import {FormView} from '../view/FormView'
import FormSectionBuilder from "./FormSectionBuilder";
import FormContentBuilder from "./FormContentBuilder";
import ActionBuilder from "./ActionBuilder";
import SubmitButtonBuilder from "./SubmitButtonBuilder";
import {CancelButtonBuilder} from "./CancelButtonBuilder";
import {FormButtonBuilder} from "./FormButtonBuilder";
import {AbstractTitledPanelViewBuilder} from "../../titledpanel/builder/AbstractTitledPanelViewBuilder";


export default class AbstractFormViewBuilder extends FormContentBuilder {

    constructor(bc) {
        super({
            validation:null,
            submit:null,
            actions:[],
            inputs:[],
            closeHandler:null,
            id:null
        }, bc)
    }

    setId(h){this.params.id = h;return this}
    setCloseHandler(h){this.params.closeHandler = h;return this}

    createFormSection(){return new FormSectionBuilder(this.__addInput.bind(this))}
    createSubmitButton() {return new SubmitButtonBuilder(this.__addAction.bind(this))}
    createSubmitAction(){return new ActionBuilder(this.__setSubmitAction.bind(this))}
    createFormButton(){return new FormButtonBuilder(this.__addAction.bind(this))}
    createCancelButton(){return new CancelButtonBuilder(this.__addAction.bind(this))}
    createValidationAction(){return new ActionBuilder(this.__setValidationAction.bind(this))}

    __addAction(a){this.params.actions.push(a);return this}
    __setSubmitAction(a) {this.params.submit = a;return this}
    __setValidationAction(a) {this.params.validation = a;return this}

    buildView(uicontext, layerModel) { return <FormView form={this.build()} layerModel={layerModel}/> }
    buildViewToLayer(uicontext) {

        var contentParams = {
            contentHandlerFactory: {createContent:(layerModel) => {
                const form = layerModel.contentParams.form
                return new AbstractTitledPanelViewBuilder()
                    .setContent(
                        <FormView form={form} layerModel={layerModel}/>
                        )
                    .setStyleProperty({width: "100%", height: "90%"})
                    .buildView(uicontext, form.submit == null ? layerModel : null)
                }
            },
            layoutParams: {
                position:"relative",
                width:"60%",
                height:"80%",
                display:"flex",
                alignItems:"center",
            },
            form : this.build()
        };
        return uicontext.layerComponentManager().addLayer(contentParams)
    }

}