import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";

export default class AddonBuilder extends AbstractParamsBuilder {

    constructor(bc) {
        super({}, bc)
    }

    setText(t){this.__reset(); this.params.text = t; return this}
    setFaIcon(t){this.__reset(); this.params.faIcon = t; return this}

    __reset(){this.setParams({})}
}