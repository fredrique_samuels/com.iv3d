

export const STATUS_WARNING = 'warning'
export const STATUS_ERROR = 'error'
export const STATUS_SUCCESS = 'success'
export const STATUS_RUNNING = 'running'
export const STATUS_WAITING = 'waiting'
export const STATUS_STOPPED = 'stopped'