import React from 'react'
import * as Constants from '../Constants'

export default class StatusButton extends React.Component {

    render() {
        const {status, classes} = this.props.status.toLowerCase()
        const formattedStatus = status.toLowerCase()
        if(formattedStatus==Constants.STATUS_SUCCESS)
            return this.createItem("color-success-text fa fa-check-circle", classes);
        if(formattedStatus==Constants.STATUS_WARNING)
            return this.createItem("color-warning-text fa fa-warning", classes);
        if(formattedStatus==Constants.STATUS_ERROR)
            return this.createItem("color-error-text fa fa-exclamation-circle", classes);
        if(formattedStatus==Constants.STATUS_WAITING)
            return this.createItem(" color-waiting-text fa fa-ellipsis-h", classes);
        if(formattedStatus==Constants.STATUS_RUNNING)
            return this.createItem("color-running-text fa fa-refresh fa-spin fa-fw", classes);
        if(formattedStatus==Constants.STATUS_STOPPED)
            return this.createItem("fa fa-ban color-default-text", classes);
        return null
    }

    createItem(s, u) {
        return <i class={s+" "+(u?u.join():"")}></i>
    }
}