import React from 'react'
import * as Constants from '../Constants'

export default class StatusIcon extends React.Component {

    render() {
        const status = this.props.status.toLowerCase()
        if(status==Constants.STATUS_SUCCESS)
            return <i class=" color-success-text fa fa-check-circle"></i>
        if(status==Constants.STATUS_WARNING)
            return <i class=" color-warning-text fa fa-warning"></i>
        if(status==Constants.STATUS_ERROR)
            return <i class=" color-error-text fa fa-exclamation-circle"></i>
        if(status==Constants.STATUS_WAITING)
            return <i class=" color-waiting-text fa fa-ellipsis-h"></i>
        if(status==Constants.STATUS_RUNNING)
            return <i class="color-running-text fa fa-refresh fa-spin fa-fw"></i>
        if(status==Constants.STATUS_STOPPED)
            return <i class="fa fa-ban color-default-text"></i>
        return null
    }
}