
import React from 'react'

export class BorderPanel extends React.Component {
    render() {
        const {borderColorClass, backgroundColorClass} = this.props
        const css = {
            marginBottom:"2px",
            borderRadius:"4px",
            borderStyle:"solid",
            borderWidth:"2px ",
            color:"white",
            width:"100%",
            padding:"4px",
            overflowY:"auto"
        }
        return (
            <div class={"fill-parent" + (borderColorClass?" border-color-"+borderColorClass:" border-color-white") + (backgroundColorClass?" background-color-"+backgroundColorClass:" background-color-light-grey")} style={css}>
                {this.props.children}
            </div>
        )
    }
}