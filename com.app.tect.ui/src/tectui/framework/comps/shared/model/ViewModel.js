export default class ViewModel {
  constructor(parent) {
      this.parent = parent
  }
  init(state){if(state){this.parent.state = state}}
  state(){return this.parent.state}
  setState(s) {
      this.parent.setState(s)
  }
  props(){return this.parent.props}
  i8n(){return this.uicontext().i8n()}
  i8nMessage(k){return this.uicontext().i8n().getMessage(k)}
  uicontext(){return this.parent.props.uicontext}
}
