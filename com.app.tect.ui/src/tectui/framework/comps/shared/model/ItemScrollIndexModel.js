

export default class ItemScrollIndexModel {

    constructor(itemCount, preferedGroupSize) {
        this.preferedGroupSize = preferedGroupSize
        this.itemCount = itemCount
        this.currentIndex=this.itemCount>0?0:-1
    }
    isVisible(index) {
        if(this.notValid())return false

        const lowerCurrentIndex = this.currentIndex
        const upperCurrentIndex = this.currentIndex + this.groupSize() - 1

        return index >= lowerCurrentIndex && index <= upperCurrentIndex
    }
    hasNext() {
        if(this.notValid())return false
        return this.currentIndex < this.maxIndex()
    }
    hasPrev() {
        if(this.notValid())return false
        return this.currentIndex > 0
    }
    next() {
        if(this.hasNext()) {
            this.currentIndex += 1
        }
        return this
    }
    prev() {
        if(this.hasPrev()) {
            this.currentIndex -= 1
        }
        return this
    }
    groupSize() {
        if(this.notValid())return 0
        if(this.preferedGroupSize <= 0)return 1
        if(this.preferedGroupSize > this.itemCount) {
            return this.itemCount
        }
        return this.preferedGroupSize
    }
    scrollToEnd() {
        this.currentIndex = this.maxIndex()
        return this
    }
    maxIndex() {
        if(this.groupSize() >= this.itemCount) {
            return 0
        }
        return this.itemCount - this.groupSize()
    }
    valid() { return this.itemCount > 0 }
    notValid() { return !this.valid() }
}