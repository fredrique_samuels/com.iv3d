import React from 'react'
import IdGenerator from '../../../IdGenerator'
import * as ElementUtils from '../../../ElementUtils'

class FontAwesomeIcon extends React.Component {
    render() {
        const {graphic} = this.props
        return (<h3 class="panel-title">
            <i class={"fa "+graphic}></i>
        </h3>)
    }
}

class ImageIcon extends React.Component {
    render() {
        const {graphic} = this.props
        return  (<div class="titledbox-image-icon inline-element">
            <svg viewBox="0 0 300 300" width="100%" height="100%">
                <image width="300" height="300" xlinkHref={graphic}/>
            </svg>
        </div>)
    }
}

class TitledBoxIcon  extends React.Component {
    render() {
        const {icon} = this.props
        if (icon) {
            if(icon.isFontAwesome()) {
                return <FontAwesomeIcon graphic={icon.faKey()}/>
            }
            if(icon.isImage() || icon.isSvg()) {
                return <ImageIcon graphic={icon.url()}/>
            }
        }
        return null
    }
}

class TitlePanelHeader extends React.Component {
    render() {
        const {content, headerHeight, borderColorClass, contentFactory, title, icon, noCloseButton, style} = this.props.params
        const {roundedBorders} = this.props
        const css = {
            width:"100%",
            position:"absolute",
            height:headerHeight,
            top:"0px",
            borderRadius:roundedBorders?"4px 4px 0px 0px":"0px"
        }

        const backgroundColorCss = (borderColorClass ? " background-color-" + borderColorClass : " background-color-white")
        const textColorCss = (borderColorClass ? " text-color-" + borderColorClass : " text-color-black")
        return (
            <div class={"center-content-v text-heading" +  backgroundColorCss} style={css}>
                <div>
                    <div class="inline-element" style={{height: "100%", margin: "auto"}}>
                        <TitledBoxIcon icon={icon}/>
                    </div>
                    <div class="inline-element" style={{height: "100%", margin: "auto"}}>
                        <span>{title}</span>
                    </div>
                </div>
                {this.createCloseButton()}
            </div>
        )
    }

    createCloseButton() {
        const {noCloseButton} = this.props.params
        if(noCloseButton)return null

        const {layerModel} = this.props
        if(layerModel)
            {

                const css = {
                    cursor:"pointer",
                    float:"right",
                    height:"16px",
                    width:"16px",
                    position:"absolute",
                    right:"10px"
                };
                return (
                    <div class="hover-shadow" style={css} onClick={this.close.bind(this)}>
                        <ImageIcon graphic="/res/images/icons/ball-cute-close-stop-no-cancel-icon_32.png"/>
                    </div>
                )
            }

        return null
    }

    close() {
        const { layerId } = this.props.layerModel
        const { uicontext } = this.props.layerModel.contentParams
        uicontext.layerComponentManager().removeLayer(layerId)
    }
}

export class TitledPanelView extends React.Component {

    constructor() {
        super()
        this.scrollContainerId = IdGenerator.generateGlobalId()
    }

    componentWillMount() {
        this.state = {hasScrollContent:false}
    }

    componentDidMount() {
        this.updateScrollStatus();
    }

    componentDidUpdate() {
        this.updateScrollStatus();
    }

    updateScrollStatus() {
        const dom = document.getElementById(this.scrollContainerId)
        const elementHasScrollContent = ElementUtils.elementHasScrollContent(dom);
        if (this.state.hasScrollContent != elementHasScrollContent) {
            this.setState({hasScrollContent: elementHasScrollContent});
        }
    }

    render() {
        const {content,headerHeight,  fixedHeight, disableScroll, borderColorClass, contentFactory, title, icon, noCloseButton, style} = this.props.params

        const {hasScrollContent} = this.state
        const containerCss = {
            minWidth:"200px",
            position:"relative",
            display:"flex",
            alignItems:"center"
        };
        return (
            <div class={(fixedHeight?"back-panel-shadow panel-text ":"") } style={Object.assign({}, style, containerCss)} >

                <div class={(fixedHeight?"":"back-panel-shadow panel-text ") + (borderColorClass ? "border-color-" + borderColorClass : "border-color-white")} id={this.scrollContainerId} style={{
                    width:"100%",
                    height:fixedHeight?fixedHeight:"auto",
                    maxHeight:"100%",
                    backgroundColor:"rgba(0,0,0,.7)",
                    overflow:disableScroll?"hidden":"auto",
                    position:"relative",
                    borderWidth:"2px",
                    borderStyle:"solid",
                    borderRadius:"4px"
                }}>
                    <div style={{width:"100%", height:fixedHeight?"100%":"auto", paddingTop:headerHeight}}>
                        {content}
                    </div>
                    {hasScrollContent?null:<TitlePanelHeader layerModel={this.props.layerModel} params={this.props.params} roundedBorders={false} />}
                </div>
                {hasScrollContent? <TitlePanelHeader layerModel={this.props.layerModel} params={this.props.params} roundedBorders={true}/> :null}
            </div>
        )
    }

}