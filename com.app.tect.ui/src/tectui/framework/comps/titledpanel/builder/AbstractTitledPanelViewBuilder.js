import React from 'react'
import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {TitledPanelView} from '../view/TitledPanelView'
import {AbstractIconBuilder} from "../../icon/builder/AbstractIconBuilder";


export class AbstractTitledPanelViewBuilder extends AbstractParamsBuilder {

    constructor(cb) {
        super({
            title:null,
            content:null,
            icon:null,
            bgColor:null,
            headingColor:null,
            borderColorClass:null,
            noCloseButton:false,
            fixedHeight:null,
            disableScroll:false,
            headerHeight:"40px",
            style:{}
        }, cb)
    }

    setFixedHeight(h) {
        this.params.fixedHeight = new String(h)
        return this
    }

    setMaxHeaderHeight(h) {
        this.params.headerHeight = new String(h)
        return this
    }

    disableScroll(h) {
        this.params.disableScroll = true
        return this
    }

    setHeading(h) {
        this.params.title = h
        return this
    }

    setContent(c) {
        this.params.content = c
        return this
    }

    setBorderColorClass(className) {
        this.params.borderColorClass=className
        return this
    }

    setStyleProperty(prop) {
        this.params.style = Object.assign({}, this.params.style, prop)
        return this
    }

    createIcon() {
        return new AbstractIconBuilder(this.__setIcon.bind(this))
    }
    __setIcon(c) {this.params.icon = c;return this}

    buildView(uicontext, layerModel) {
        return <TitledPanelView params={this.build()} layerModel={layerModel} />
    }

    buildViewToLayer(uicontext) {
        const defaultLayoutParams = {
            position:"relative",
            top:"0%",
            left:"0%",
            width:"100%",
            height:"100%",
            display:"flex",
            alignItems: "center",
            justifyContent: "center"
        }

        var contentParams = {
            contentHandlerFactory: {createContent: (layerModel) => {
                return <TitledPanelView params={layerModel.contentParams.titledBoxParams} layerModel={layerModel} />
            }},
            layoutParams: Object.assign({},defaultLayoutParams, defaultLayoutParams),
            titledBoxParams : this.build(),
            uicontext:this.uicontext
        };

        return uicontext.layerComponentManager().addLayer(contentParams,{canBeCleared:false})
    }
}