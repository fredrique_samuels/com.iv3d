

export default class TimeUnit {
    static milliSeconds(units){
        return {
            milliSeconds : function(){return units}
        }
    }

    static seconds(units){
        return {
            milliSeconds : function(){return 1000*units}
        }
    }

    static minutes(units){
        return {
            milliSeconds : function(){return 1000*60*units}
        }
    }

    static hours(units){
        return {
            milliSeconds : function(){return 1000*60*60*units}
        }
    }

    static days(units){
        return {
            milliSeconds : function(){return 1000*60*60*24*units}
        }
    }
}