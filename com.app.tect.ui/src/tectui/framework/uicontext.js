

import {LayerComponentManager} from "./comps/layers/view/LayerComponent";

import CookieManager from "./server/Cookies";
import AccountManager from "./server/AccountManager";

import I8nManager from "./i8n/I8nManager";
import I8nMessageSource from "./i8n/I8nMessageSource";

import AbstractCardSelectViewBuilder from "./comps/cardselect/builder/AbstractCardSelectViewBuilder";
import AbstractTaskStatusBoxViewBuilder from "./comps/taskstatusbox/builder/AbstractTaskStatusBoxViewBuilder";
import AbstractStatusBoxViewBuilder from "./comps/statusbox/builder/AbstractStatusBoxViewBuilder";
import AbstractFormViewBuilder from "./comps/form/builder/AbstractFormViewBuilder";
import AbstractFormElementBuilder from "./comps/form/builder/AbstractFormElementBuilder";
import AbstractTabsViewBuilder from "./comps/tabs/builder/AbstractTabsViewBuilder";
import AbstractLoadingViewBuilder from "./comps/loading/builder/AbstractLoadingViewBuilder";
import {AbstractTitledPanelViewBuilder} from "./comps/titledpanel/builder/AbstractTitledPanelViewBuilder";

import messages_en from './i8n/messages_en.json'

//TODO
/**
 Change API for accessing {TVisionManager} to
 const platformManager = new tvision.PlatformManager(uicontext)

 */

import Dispatcher from "./Dispatcher";

export default class UiContext {
    constructor(params={}) {
        this.params = params;
    }

    // mutation methods
    setProperties(userparams={}) {return new UiContext(Object.assign({}, this.params, userparams))}
    getProperty(p){return this.params[p]}
    setLayerComponentModelId(modelId) {return this.setProperties({layer_component_id:modelId})}

    // getters
    getDispatcher(){return Dispatcher.getGlobal()}

    //builders
    cardSelectViewBuilder(){return new AbstractCardSelectViewBuilder()}
    taskStatusBoxViewBuilder(){return new AbstractTaskStatusBoxViewBuilder()}
    statusBoxViewBuilder(){return new AbstractStatusBoxViewBuilder()}
    formViewBuilder(){return new AbstractFormViewBuilder()}
    formElementViewBuilder(){return new AbstractFormElementBuilder()}
    tabsViewBuilder(){return new AbstractTabsViewBuilder()}
    loadingViewBuilder(){return new AbstractLoadingViewBuilder()}
    titledPanelViewBuilder(){return new AbstractTitledPanelViewBuilder()}

    // @Deprecated Use stack interface
    // ui managers
    layerComponentManager(){return new LayerComponentManager(this, this.params.layer_component_id)}
    removeLayer(layerComp){
        const { layerId } = layerComp.props.layerModel
        const { uicontext } = layerComp.props.layerModel.contentParams
        uicontext.layerComponentManager().removeLayer(layerId)
    }

    cookieManager(){return new CookieManager()}
    accountManager(){return new AccountManager(this)}


    //component stack to replace layer component
    componentStackController() {return this.params.componentStackController}
    setComponentStackController(csc) { return this.setProperties({componentStackController:csc})}
    addStackedComponent(componentFactory) {
        const {componentStackController} = this.params
        if(componentStackController) {
            return componentStackController.addStackedComponent(componentFactory, this)
        }
    }
    removeStackedComponent(layerId) {
        const {componentStackController} = this.params
        if(componentStackController) {
            componentStackController.removeStackedComponent(layerId)
        }
    }

    locale(){return "en"}
    i8n(){return new I8nManager(this, new I8nMessageSource(messages_en))}
    i8nMessage(m){return this.i8n().getMessage(m)}


}
