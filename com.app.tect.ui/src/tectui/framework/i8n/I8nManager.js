

export default class I8nManager {
    constructor(uicontext, messagesSource) {
       this.uicontext = uicontext
       this.messageSource = messagesSource
    }

    getMessageFromKey(key, locale = 'en'){
        return this.messageSource.getMessageFromKey(key, locale)
    }

    getMessage(key) {
        const msg = this.getMessageFromKey(key, this.uicontext.locale())
        if(msg) {
            return msg
        }
        console.error("I8n message not found for key:", key)
        return key
    }
}