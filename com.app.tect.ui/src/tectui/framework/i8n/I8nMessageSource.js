export default class I8nMessageSource {

    constructor(en_locale) {
        this.locales = {
            'en':en_locale
        }
    }

    setLocaleMessages(locale, messages) {
        this.locales[locale] = messages
    }

    getMessageFromKey(key, locale){
        return this.locales[locale].messages[key]
    }
}