
const globalSeed = {s:1}
export default class IdGenerator {
    constructor(prefix) {
        this.seed = 1
        this.prefix = prefix
    }

    generate() {
        return this.prefix + (this.seed++)
    }

    static generateGlobalId(prefix="ui_comp_") {
        globalSeed.s = globalSeed.s+1
        return prefix+globalSeed.s
    }
}