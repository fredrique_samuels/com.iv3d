


export function isChildHidden(element, child) {
    return child.offsetTop + child.offsetHeight >
        element.offsetTop + element.offsetHeight ||
        child.offsetLeft + child.offsetWidth >
        element.offsetLeft + element.offsetWidth
}

export function elementHasScrollContent(element) {
    return element.offsetHeight < element.scrollHeight ||
        element.offsetWidth < element.scrollWidth
}

export function getHiddenElementCount(element) {
    if(this.elementHasScrollContent(element)) {
        let hiddenChildCount = 0
        for(var i=0; i<element.childElementCount; i++){
            if (this.isChildHidden(element, element.children[i])) {
                hiddenChildCount+=1
            }
        }
        return hiddenChildCount
    }
    return 0
}

export function getVisibleElementCount(element) {
    const totalChildren = element.childElementCount
    if(this.elementHasScrollContent(element)) {
        let hiddenChildCount = this.getHiddenElementCount(element)
        return totalChildren-hiddenChildCount
    }
    return totalChildren
}