export default class Timeout {
    constructor(interval, doneCallback){
        this.timer = setTimeout(doneCallback, interval)
    }

    stop() {
        clearTimeout(this.timer)
    }
}