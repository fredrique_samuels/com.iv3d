import React from 'react'

import AbstractCardSelectViewBuilder from "./framework/comps/cardselect/builder/AbstractCardSelectViewBuilder";
import AbstractCardOptionOverlaysBuilder from "./framework/comps/cardselect/builder/AbstractCardOptionOverlaysBuilder";
import AbstractTabsViewBuilder from "./framework/comps/tabs/builder/AbstractTabsViewBuilder";
import AbstractStatusBoxViewBuilder from "./framework/comps/statusbox/builder/AbstractStatusBoxViewBuilder";
import AbstractFromViewBuilder from './framework/comps/form/builder/AbstractFormViewBuilder'
import AbstractFormElementBuilder from './framework/comps/form/builder/AbstractFormElementBuilder'
import AbstractTaskStatusBoxViewBuilder from "./framework/comps/taskstatusbox/builder/AbstractTaskStatusBoxViewBuilder";
import AbstractLoadingViewBuilder from "./framework/comps/loading/builder/AbstractLoadingViewBuilder";
import AbstractComponentStackViewBuilder from "./framework/comps/stack/builder/ComponentStackViewBuilder";
import BaseTaskStatus from "./framework/comps/taskstatusbox/model/TaskStatus"

import BaseStatusIcon from './framework/comps/shared/view/StatusIcon'
import * as FrameworkConstants from './framework/comps/shared/Constants.js'

import BaseTimeout from "./framework/Timeout"
import BaseTimeUnit from "./framework/TimeUnit"
import BaseUiContext from "./framework/uicontext"
import IdGenerator from "./framework/IdGenerator"
import Dispatcher from "./framework/Dispatcher"

import {TextPopupBoxView} from './framework/comps/TextPopupBoxView'


import {AbstractActionConfigBuilder} from "./framework/comps/action/builder/AbstractActionConfigBuilder";
import {AbstractRadialMenuBuilder} from "./framework/comps/radialmenu/builder/AbstractRadialMenuBuilder";
import {AbstractIconBuilder} from "./framework/comps/icon/builder/AbstractIconBuilder";
import {AbstractTitledPanelViewBuilder} from "./framework/comps/titledpanel/builder/AbstractTitledPanelViewBuilder";
import {BorderPanel} from "./framework/comps/shared/view/BorderPanel";
import AbstractI8nManager from './framework/i8n/I8nManager'
import AbstractI8nMessageSource from './framework/i8n/I8nMessageSource'

export const STATUS_WARNING = FrameworkConstants.STATUS_WARNING
export const STATUS_ERROR = FrameworkConstants.STATUS_ERROR
export const STATUS_SUCCESS = FrameworkConstants.STATUS_SUCCESS
export const STATUS_RUNNING = FrameworkConstants.STATUS_RUNNING
export const STATUS_WAITING = FrameworkConstants.STATUS_WAITING
export const STATUS_STOPPED = FrameworkConstants.STATUS_STOPPED

export function  setDispatcher(s) {
    Dispatcher.setGlobal(s)
}

export function dispatcher() {
    return Dispatcher.getGlobal()
}

export function generateUniqueUiId() {
    return IdGenerator.generateGlobalId()
}

export function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}


export class I8nMessageSource extends AbstractI8nMessageSource {

    constructor(en_locale) {
        super(en_locale)
    }
}

export class I8nManager extends AbstractI8nManager {

    constructor(uicontext, messageSource) {
        super(uicontext, messageSource)
    }
}


export class ViewUtils {
    static createStackedLoadingLayer(uicontext, text) {
        const loadingFactory = (layerId, uicontext) => {
            const css = {
                position: "relative",
                minWidth: "40%",
                maxHeight: "80%",
                backgroundColor: "rgba(0,0,0,0)"
            };

            return (
                <div style={css}>
                    {
                        new LoadingViewBuilder()
                            .setController({start:(model) => model.update().setText(text).run()})
                            .buildView(uicontext)
                    }
                </div>
            )
        }
        return uicontext.addStackedComponent(loadingFactory);
    }

    static createStackedTextPopupSuccess(uicontext, text, closeHandler) {
        const loadingFactory = (layerId, uicontext) => {
            const css = {
                position: "relative",
                minWidth: "40%",
                maxHeight: "80%",
                backgroundColor: "rgba(0,0,0,0)"
            };
            return (
                <div style={css}>
                    <TextPopupBoxView text={text} closeHandler={() => {
                        uicontext.removeStackedComponent(layerId)
                        if(closeHandler)closeHandler()
                    }} />
                </div>
            )
        }
        return uicontext.addStackedComponent(loadingFactory);
    }

    static createStackedTextPopupError(uicontext, text) {
        const loadingFactory = (layerId, uicontext) => {
            const css = {
                position: "relative",
                minWidth: "40%",
                maxHeight: "80%",
                backgroundColor: "rgba(0,0,0,0)"
            };
            return (
                <div style={css}>
                    <TextPopupBoxView type={"error"} text={text} closeHandler={() => uicontext.removeStackedComponent(layerId)} />
                </div>
            )
        }
        return uicontext.addStackedComponent(loadingFactory);
    }
}

/**

DataViewRenderer is an object that has a render(data) method excepting any
object and returning an React.Component.

Examples would be:
    class DVR {
        render(data) {
            return <div></div>
        }
    }

    dvr = new DVR()

or using a lambda:

    dvr = { render: (data) => <div></div> }

 **/
export class DataViewRenderer {}


export class BorderPanelView extends BorderPanel {

    constructor() {
        super()
    }
}

/**
 * Build a selection view with the options rendered as a grid of cards.
 *
 * @see {AbstractCardSelectViewBuilder}
 */
export class CardSelectViewBuilder extends AbstractCardSelectViewBuilder {
    constructor(bc=null){super(bc)}
}

/**
 * Build a list of views to be used on a Card Option Panel.
 *
 * @see {AbstractCardOptionOverlaysBuilder}
 */
export class CardOptionOverlaysBuilder extends AbstractCardOptionOverlaysBuilder {
    constructor(bc=null){super(bc)}
}

/**
 * Create a tabbed display of views
 *
 * @see {AbstractTabsViewBuilder}
 */
export class TabsViewBuilder extends AbstractTabsViewBuilder {
    constructor(bc) {
        super(bc)
    }
}

/**
 * Create a Status box view
 *
 * @see {AbstractStatusBoxViewBuilder}
 */
export class StatusBoxViewBuilder extends AbstractStatusBoxViewBuilder {
    constructor(bc) {
        super(bc)
    }
}

/**
 * Create a status box containing task related stats
 *
 * @see {AbstractTaskStatusBoxViewBuilder}
 */
export class TaskStatusBoxViewBuilder extends AbstractTaskStatusBoxViewBuilder {
    constructor(bc) {
        super(bc)
    }
}


export class TaskStatus extends BaseTaskStatus {

    constructor(params) {
        super(params)
    }
}

/**
 * Create a Form
 *
 * @see {AbstractFromViewBuilder}
 */
export class FormViewBuilder extends AbstractFromViewBuilder {

    constructor(bc) {
        super(bc)
    }
}

export class FormElementBuilder extends AbstractFormElementBuilder {
    constructor(bc) {
        super(bc)
    }
}

/**
 * Create a loading view
 * @see {AbstractLoadingViewBuilder}
 */
export class LoadingViewBuilder extends AbstractLoadingViewBuilder {
    constructor(cb){
        super(cb)
    }
}

/**
 * Create a timeout objects
 */
export class Timeout extends BaseTimeout {
    constructor(interval, doneCallback){
        super(interval, doneCallback)
    }
}

export class TimeUnit extends BaseTimeUnit {
}


export class RadialMenuViewBuilder extends AbstractRadialMenuBuilder {

    constructor(cb) {
        super(cb);
    }
}

export class TitledPanelViewBuilder extends AbstractTitledPanelViewBuilder {

    constructor(cb) {
        super(cb);
    }
}

export class ActionConfigBuilder extends AbstractActionConfigBuilder {
    constructor(cb) {
        super(cb)
    }
}

export class ComponentStackViewBuilder extends AbstractComponentStackViewBuilder {
    constructor(cb) {
        super(cb)
    }
}

/**
 * Context object for creating view and accessing the Ui model
 */
export class UiContext extends BaseUiContext {
    constructor(params={}) {
        super(params)
    }
}


/**
 * Icon definition object.
 */
export class IconBuilder extends AbstractIconBuilder {

    constructor() {
        super()
    }
}

export class StatusIcon extends BaseStatusIcon {

    constructor() {
        super()
    }
}