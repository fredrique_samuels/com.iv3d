import {TestRadialMenuComponent} from "./TestRadialMenu";
import {TestRadialMenuComponentCount} from "./TestRadialMenuItemCount";

export class RadialMenuTests {
    get(menu) {
        return [
            menu.createHeading("Radial Menu"),
            menu.createTestButton("Radial Menu More",function(){ new TestRadialMenuComponent().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Radial Menu 1",function(){ new TestRadialMenuComponentCount(1).run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Radial Menu 2",function(){ new TestRadialMenuComponentCount(2).run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Radial Menu 3",function(){ new TestRadialMenuComponentCount(3).run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Radial Menu 4",function(){ new TestRadialMenuComponentCount(4).run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Radial Menu 5",function(){ new TestRadialMenuComponentCount(5).run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Radial Menu 6",function(){ new TestRadialMenuComponentCount(6).run(menu.getUiContext())}.bind(menu))
        ]
    }
}