import React from 'react'
import { connect } from 'react-redux'
import * as tectui from "../../tectui";



export class TestRadialMenuComponentCount {

   constructor(count = 1) {
       this.count = count
   }

  run(uicontext) {
      let builder = new tectui.RadialMenuViewBuilder();
      builder = this.generateItems(builder)
      builder.buildViewToLayer(uicontext)
  }

  generateItems(b) {
      let builder = b

      builder = this.createItem(builder, "fa-share", null, 'Share')
      if(this.count>1) builder = this.createItem(builder, "fa-facebook", null, "Facebook")
      if(this.count>2) builder = this.createItem(builder, "fa-lock", null, "Security")
      if(this.count>3) builder = this.createItem(builder, "fa-user", null, "User")
      if(this.count>4) builder = this.createItem(builder, "fa-skype", null, "Skype")
      if(this.count>5) builder = this.createItem(builder, "fa-twitter", null, "Twitter" )

      return builder
  }

    createItem(builder, faIcon, action, label) {
        return builder.createItem()
            .setLabel(label)

            .createIcon()
            .setFaIcon(faIcon)
            .commit()

            .commit()
    }
}