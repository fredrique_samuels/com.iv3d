import React from 'react'
import { connect } from 'react-redux'

import * as tectui from '../../tectui'


export class TestRadialMenuComponent {

  run(uicontext) {
      let builder = new tectui.RadialMenuViewBuilder();
      builder = this.createItem(builder, "fa-share", null, "Share")
      builder = this.createItem(builder, "fa-facebook", null, "Facebook")
      builder = this.createItem(builder, "fa-lock", null, "Security")
      builder = this.createItem(builder, "fa-user", null, "User")
      builder = this.createItem(builder, "fa-skype", null, "Skype")

      builder = this.createItem(builder, "fa-twitter", null, "Twitter")
      builder = this.createItem(builder, "fa-linkedin", null, "Linkedin")
      builder = this.createItem(builder, "fa-envelope-open-o", null, "Mail")
      builder = this.createItem(builder, "fa-clock-o", null, "Clock")
      builder = this.createItem(builder, "fa-share", null, "Share")

      builder = this.createItem(builder, "fa-facebook", null, "Facebook")
      builder = this.createItem(builder, "fa-lock", null, "Security")
      builder = this.createItem(builder, "fa-user", null, "User")
      builder = this.createItem(builder, "fa-skype", null, "Skype")
      builder = this.createItem(builder, "fa-twitter", null, "Twitter")

      builder = this.createItem(builder, "fa-linkedin", null, "Linkedin")
      builder = this.createItem(builder, "fa-envelope-open-o", null, "Mail")
      builder = this.createItem(builder, "fa-clock-o", null, "Clock")
      builder.buildViewToLayer(uicontext)

  }

    createItem(builder, faIcon, action, label) {
        return builder.createItem()
            .setLabel(label)

            .createIcon()
            .setFaIcon(faIcon)
            .commit()

            .commit()
    }
}
