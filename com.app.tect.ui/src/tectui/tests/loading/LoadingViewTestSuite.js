import React from 'react'
import TestSpinLoaderWithNoController from "./tests/TestSpinLoaderWithNoController";
import TestSpinLoaderWithController from "./tests/TestSpinLoaderWithController";
import TestProgressLoaderWithNoController from "./tests/TestProgressLoaderWithNoController";
import TestProgressLoaderWithController from "./tests/TestProgressLoaderWithController";

export default class LoadingViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("Loading View"),
            menu.createTestButton("Spin with no controller",function(){ new TestSpinLoaderWithNoController().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Spin with controller",function(){ new TestSpinLoaderWithController().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Progress load with no controller",function(){ new TestProgressLoaderWithNoController().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Progress load with controller",function(){ new TestProgressLoaderWithController().run(menu.getUiContext())}.bind(menu))
        ]
    }
}