import React from 'react'
import { LoadingViewBuilder} from "../../../tectui";
import DemoLoadingController from "./shared/DemoLoadingController";

export default class TestSpinLoaderWithNoController {
    run(uicontext) {
        new LoadingViewBuilder()
            .setController(new DemoLoadingController())
            .buildViewToLayer(uicontext)
    }
}