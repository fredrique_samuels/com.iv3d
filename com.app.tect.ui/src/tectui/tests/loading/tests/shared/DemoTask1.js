import Timeout from "../../../../framework/Timeout";
import DemoTask2 from "./DemoTask2";


export default class DemoTask1 {
    constructor(model, controller) {
        this.model = model
        this.controller = controller
    }

    run() {
        this.model.update()
            .setText("Running Task 1 ...")
            .setProgress(30)
            .run()
        new Timeout(2000, this.onDone.bind(this))
    }

    onDone() {
        if(this.controller.canceled())
            return
        new DemoTask2(this.model, this.controller).run()
    }
}
