import Timeout from "../../../../framework/Timeout";


export default class DemoTask2 {
    constructor(model, controller) {
        this.model = model
        this.controller = controller
    }

    run() {
        this.model.update()
            .setText("Running Task 2 ...")
            .setProgress(60)
            .run()
        new Timeout(2000, this.onDone.bind(this))
    }

    onDone() {
        if(this.controller.canceled())
            return
        this.model.update()
            .setDone()
            .run()
    }
}