import DemoTask1 from "./DemoTask1";

export default class DemoLoadingController {

    constructor(){
        this.counter = 1
        this.isCanceled = false
    }

    start(model) {
        new DemoTask1(model, this).run()
    }

    canceled(){return this.isCanceled}

    stop() {
        this.isCanceled = true
    }
}
