import React from 'react'
import { LoadingViewBuilder} from "../../../tectui";


export default class TestProgressLoaderWithNoController {
    run(uicontext) {
        new LoadingViewBuilder()
            .setProgressMode()
            .buildViewToLayer(uicontext)
    }
}