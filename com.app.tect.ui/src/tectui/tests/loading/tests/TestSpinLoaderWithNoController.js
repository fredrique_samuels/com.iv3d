import React from 'react'
import { LoadingViewBuilder} from "../../../tectui";


export default class TestSpinLoaderWithNoController {
    run(uicontext) {
        new LoadingViewBuilder()
            .buildViewToLayer(uicontext)
    }
}