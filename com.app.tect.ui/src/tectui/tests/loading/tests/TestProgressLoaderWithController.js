import React from 'react'
import { LoadingViewBuilder} from "../../../tectui";
import DemoLoadingController from "./shared/DemoLoadingController";

export default class TestProgressLoaderWithController {
    run(uicontext) {
        new LoadingViewBuilder()
            .setController(new DemoLoadingController())
            .setProgressMode()
            .buildViewToLayer(uicontext)
    }
}