import React from 'react'
import * as tectui from '../../../tectui'

export default class StatusBoxBasic {
    run(uicontext) {
        new tectui.StatusBoxViewBuilder()
            .setHeading("Status Box Test")
            .setStatus(tectui.STATUS_SUCCESS)
            .setContent(<div class="fill-parent" style={{backgroundColor:"yellow"}}>Content 2</div>)
            .buildViewToLayer(uicontext)
    }
}