import React from 'react'
import StatusBoxBasic from './tests/StatusBoxBasic'

export default class StatusBoxViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("Status Box"),
            menu.createTestButton("Basic",function(){ new StatusBoxBasic().run(menu.getUiContext())}.bind(menu))
        ]
    }
}