import React from 'react'
import { connect } from 'react-redux'
import * as tectui from "../../tectui";

export class TestTitledBoxNoTitle {
  run(uicontext) {

      new tectui.TitledPanelViewBuilder()
          .setContent(<div style={{overflow:"hidden"}}><p>This is a value</p></div>)
          .buildViewToLayer(uicontext)

  }
}
