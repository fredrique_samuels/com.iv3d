import React from 'react'
import * as tectui from "../../tectui";

export class TestTitledBoxFaIcon {
  run(uicontext) {
      new tectui.TitledPanelViewBuilder()
          .setHeading("Font Awesome Icon")
          .setContent(<div class="has-text"><p>This is a value</p></div>)

          .createIcon()
          .setFaIcon('fa-clock-o')
          .commit()

          .buildViewToLayer(uicontext)
  }
}
