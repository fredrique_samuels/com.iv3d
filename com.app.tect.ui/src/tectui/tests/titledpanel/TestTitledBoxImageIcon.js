import React from 'react'
import * as tectui from "../../tectui";

export class TestTitledBoxImageIcon {
  run(uicontext) {


      new tectui.TitledPanelViewBuilder()
          .setHeading("Image Icon")
          .setContent(<div class="has-text"><p>This is a value</p></div>)

          .createIcon()
          .setImage("http://vignette3.wikia.nocookie.net/yugioh/images/3/3b/MysticalSpaceTyphoon-LDK2-EN-C-1E.png/revision/latest?cb=20161007084018")
          .commit()

          .buildViewToLayer(uicontext)

  }
}
