import React from 'react'
import { connect } from 'react-redux'
import * as tectui from "../../tectui";

export class TestTitledBox {
  run(uicontext) {

      new tectui.TitledPanelViewBuilder()
          .setHeading("Basic Demo")
          .setContent(<div style={{overflow:"hidden"}}><p>This is a value</p></div>)
          .buildViewToLayer(uicontext)

  }
}
