import React from 'react'
import {TestTitledBox} from "./TestTitledBox";
import {TestTitledBoxFaIcon} from "./TestTitledBoxFaIcon";
import {TestTitledBoxImageIcon} from "./TestTitledBoxImageIcon";
import {TestTitledBoxNoTitle} from "./TestTitledBoxNoTitle";

export class TitlePanelViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("Title Panel"),
            menu.createTestButton("Basic",function(){ new TestTitledBox().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("No Title",function(){ new TestTitledBoxNoTitle().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("FA Icon",function(){ new TestTitledBoxFaIcon().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Image Icon",function(){ new TestTitledBoxImageIcon().run(menu.getUiContext())}.bind(menu))
        ]
    }
}