import React from 'react'
import TestCardSelectBasic from './tests/TestCardSelectBasic'
import TestCardSelectScroll from "./tests/TestCardSelectScroll";

export default class CardSelectViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("Card Select"),
            menu.createTestButton("Basic",function(){ new TestCardSelectBasic().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Scroll",function(){ new TestCardSelectScroll().run(menu.getUiContext())}.bind(menu))
        ]
    }
}