import React from 'react'
import { CardSelectViewBuilder,  CardOptionOverlaysBuilder} from "../../../tectui";


export default class TestCardSelectBasic {
    run(uicontext) {

        const dvr = {
            render: (data) => {

                const builder = new CardOptionOverlaysBuilder()
                    .addView(<div class="fill-parent center-content-vh"><i class="fa fa-tasks fa-4x"></i></div>);

                if(data=='warning') {
                    builder.createWarningBannerWithIcon()
                        .commit()
                }

                if(data=='success') {
                    builder.createSuccessBannerWithIcon()
                        .commit()
                }

                if(data=='error') {
                    builder.createErrorBannerWithIcon()
                        .commit()
                }

                if(data=='waiting') {
                    builder.createWaitingBanner()
                        .commit()
                }

                if(data=='running') {
                    builder.createRunningBanner()
                        .commit()
                }

                if(data=='stopped') {
                    builder.createStoppedBanner()
                        .commit()
                }

                return builder.build()
            }
        }

        let builder = new CardSelectViewBuilder()
            .setHeading("This is a Demo")
            .setDataViewRenderer(dvr);

        for (var i=0;i<50;i++) {
            builder = builder.createOption()
                .setLabel("Warning")
                .setAction((data) => console.error(data))
                .setData('warning')
                .commit()
        }

        builder.buildViewToLayer(uicontext)
    }
}