import React from 'react'
import { CardSelectViewBuilder,  CardOptionOverlaysBuilder} from "../../../tectui";


export default class TestCardSelectBasic {
    run(uicontext) {

        const dvr = {
            render: (data) => {

                const builder = new CardOptionOverlaysBuilder()
                    .addView(<div class="fill-parent center-content-vh"><i class="fa fa-tasks fa-4x"></i></div>);

                if(data=='warning') {
                    builder.createWarningBannerWithIcon()
                        .commit()
                }

                if(data=='success') {
                    builder.createSuccessBannerWithIcon()
                        .commit()
                }

                if(data=='error') {
                    builder.createErrorBannerWithIcon()
                        .commit()
                }

                if(data=='waiting') {
                    builder.createWaitingBanner()
                        .commit()
                }

                if(data=='running') {
                    builder.createRunningBanner()
                        .commit()
                }

                if(data=='stopped') {
                    builder.createStoppedBanner()
                        .commit()
                }

                return builder.build()
            }
        }

        new CardSelectViewBuilder()
            .setHeading("This is a Demo")
            .setDataViewRenderer(dvr)

            .createOption()
            .setLabel("Warning")
            .setAction( (data) => console.error(data) )
            .setData('warning')
            .commit()

            .createOption()
            .setLabel("Success")
            .setAction( (data) => console.error(data) )
            .setData("success")
            .commit()

            .createOption()
            .setLabel("Error")
            .setAction( (data) => console.error(data) )
            .setData("error")
            .commit()

            .createOption()
            .setLabel("Waiting")
            .setAction( (data) => console.error(data) )
            .setData("waiting")
            .commit()

            .createOption()
            .setLabel("Running")
            .setAction( (data) => console.error(data) )
            .setData("running")
            .commit()

            .createOption()
            .setLabel("Stopped")
            .setAction( (data) => console.error(data) )
            .setData("stopped")
            .commit()

            .buildViewToLayer(uicontext)
    }
}