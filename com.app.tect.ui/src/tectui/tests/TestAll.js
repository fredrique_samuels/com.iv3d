import React from 'react'
import { connect } from 'react-redux'

import LayerComponent from '../framework/comps/layers/view/LayerComponent'

// import Transition from 'react-inline-transition-group'
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Transition from 'react-transition-group/Transition';
import TransitionGroup from 'react-transition-group/TransitionGroup';

// NEW TESTS
import CardSelectViewTestSuite from "./cardselect/CardSelectViewTestSuite";
import TabsViewTestSuite from "./tabs/TabsViewTestSuite";
import uicontext from "../framework/uicontext";
import StatusBoxViewTestSuite from "./statusbox/StatusBoxViewTestSuite";
import {TaskStatusBoxViewTestSuite} from "./taskstatusbox/TaskStatusBoxViewTestSuite";
import FormViewTestSuite from "./form/FormViewTestSuite";
import LoadingViewTestSuite from "./loading/LoadingViewTestSuite";
import {setDispatcher} from "../tectui";
import {RadialMenuTests} from "./radialmenu/RadialMenuTests";
import {TitlePanelViewTestSuite} from "./titledpanel/TitlePanelViewTestSuite";


const LAYER_MANAGER_ID = "test.layer.manager"



class DemosMenu extends React.Component {
    render() {
        const style={
            position:"relative",
            overflow:"auto",
            padding:"10px"
        }

        return (<div class="back-panel fill-parent" style={style}>

            {new CardSelectViewTestSuite().get(this)}
            {new TabsViewTestSuite().get(this)}
            {new StatusBoxViewTestSuite().get(this)}
            {new TaskStatusBoxViewTestSuite().get(this)}
            {new FormViewTestSuite().get(this)}
            {new LoadingViewTestSuite().get(this)}
            {new RadialMenuTests().get(this)}
            {new TitlePanelViewTestSuite().get(this)}

        </div>)
    }

    getUiContext(){
      const {layerModel} = this.props
      const { uicontext } = layerModel.contentParams
      return uicontext
    }

    createTestButton(text, onClick) {
      return <div style={{margin:"3px"}} class="inline-element"><button  class="btn btn-primary" onClick={onClick}>{text}</button></div>
    }
    createHeading(text) {
      return <h2 style={{color:"grey"}}>{text}</h2>
    }
}

class DemoContentHandler {
    createContent(layerModel) {
        return <DemosMenu layerModel={layerModel} />
    }
}

@connect((store) => {
    return {
        environment : store.environment,
        componentModels : store.componentModels,
    }
})
export default class TestAllWidgets extends React.Component {

    constructor() {
        super()
        this.uicontext = new uicontext();
    }

    componentWillMount() {
        setDispatcher(this.props.dispatch)
    }

    render() {
        return (
          <div class="back-panel fill-parent">
            <LayerComponent modelId={LAYER_MANAGER_ID} mountedCallback={this.onLayerMounted.bind(this)}/>
          </div>)
    }

    onLayerMounted(id) {
        console.log(id+" mounted successfully")
        this.uicontext = this.uicontext.setLayerComponentModelId(id)

        {
            var contentParams = {
                contentHandlerFactory: new DemoContentHandler()
            };
            this.uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:false})
        }
    }
}
