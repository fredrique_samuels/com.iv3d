import React from 'react'
import * as tectui from "../../../tectui";

export default class TabsBasic {
    run(uicontext) {
           new tectui.TabsViewBuilder()
            .addTab("Tab 1", <div class="fill-parent" style={{backgroundColor:"red"}}>Content 1</div>)
            .addTab("Tab 2", <div class="fill-parent" style={{backgroundColor:"yellow"}}>Content 2</div>)
            .buildViewToLayer(uicontext)
    }
}