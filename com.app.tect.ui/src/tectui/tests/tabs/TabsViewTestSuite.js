import React from 'react'
import TabsBasic from './tests/TabsBasic'

export default class TabsViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("Tabs"),
            menu.createTestButton("Basic",function(){ new TabsBasic().run(menu.getUiContext())}.bind(menu))
        ]
    }
}