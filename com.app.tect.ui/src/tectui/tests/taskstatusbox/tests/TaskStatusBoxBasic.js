import React from 'react'
import * as tectui from '../../../tectui'

export default class TaskStatusBoxBasic {
    run(uicontext) {
        new tectui.TaskStatusBoxViewBuilder()
            .setTaskData(this.data())
            .setHistoryItemClickedCallback( (item) => console.error(item))
            .setHistoryData(this.history())
            .buildViewToLayer(uicontext)
    }
    history() {
        return [
            {
                "jobId": 1,
                "runId": 1,
                "status": "WARNING"
            },
            {
                "jobId": 1,
                "runId": 2,
                "status": "WAITING"
            },
            {
                "jobId": 1,
                "runId": 3,
                "status": "ERROR"
            },
            {
                "jobId": 1,
                "runId": 4,
                "status": "SUCCESS"
            },
            {
                "jobId": 1,
                "runId": 5,
                "status": "ERROR"
            },
            {
                "jobId": 1,
                "runId": 6,
                "status": "WARNING"
            },
            {
                "jobId": 1,
                "runId": 7,
                "status": "SUCCESS"
            },
            {
                "jobId": 1,
                "runId": 8,
                "status": "SUCCESS"
            },
            {
                "jobId": 1,
                "runId": 9,
                "status": "ERROR"
            },
            {
                "jobId": 1,
                "runId": 10,
                "status": "WARNING"
            },
            {
                "jobId": 1,
                "runId": 11,
                "status": "STOPPED"
            },
            {
                "jobId": 1,
                "runId": 12,
                "status": "SUCCESS"
            }
        ]
    }
    data() {
        return {
            "id": -1,
            "doe": {
                "millisecondsSinceEpoc": 1498569583710,
                "year": 2017,
                "month": 6,
                "hour": 13,
                "minute": 19,
                "second": 43,
                "day": 27
            },
            "parentId": -1,
            "startDate": {
                "millisecondsSinceEpoc": 1498569583710,
                "year": 2017,
                "month": 6,
                "hour": 13,
                "minute": 19,
                "second": 43,
                "day": 27
            },
            "durationInMs": 0,
            "errorMessage": "",
            "errorTrace": "",
            "error": false,
            "type": "TASK",
            "children": [
                {
                    "id": -1,
                    "doe": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "parentId": -1,
                    "startDate": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "durationInMs": 0,
                    "errorMessage": "",
                    "errorTrace": "",
                    "error": false,
                    "type": "waiting",
                    "children": [],
                    "attributes": [
                        {
                            "name": "operation.status.attribute.run.id",
                            "value": 12
                        }],
                    "status": "WAITING"
                },
                {
                    "id": -1,
                    "doe": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "parentId": -1,
                    "startDate": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "durationInMs": 0,
                    "errorMessage": "",
                    "errorTrace": "",
                    "error": false,
                    "type": "running",
                    "children": [],
                    "attributes": [
                        {
                            "name": "operation.status.attribute.run.id",
                            "value": 12
                        }],
                    "status": "RUNNING"
                },
                {
                    "id": -1,
                    "doe": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "parentId": -1,
                    "startDate": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "durationInMs": 0,
                    "errorMessage": "",
                    "errorTrace": "",
                    "error": false,
                    "type": "stopped",
                    "children": [],
                    "attributes": [
                        {
                            "name": "operation.status.attribute.run.id",
                            "value": 13
                        }],
                    "status": "STOPPED"
                },
                {
                    "id": -1,
                    "doe": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "parentId": -1,
                    "startDate": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "durationInMs": 0,
                    "errorMessage": "Some thing went very wrong",
                    "errorTrace": "Exception in thread \"main\" java.lang.NullPointerException\nat com.example.myproject.Book.getTitle(Book.java:16)\nat com.example.myproject.Author.getBookTitles(Author.java:25)\nat com.example.myproject.Bootstrap.main(Bootstrap.java:14)",
                    "error": true,
                    "type": "error",
                    "children": [],
                    "attributes": [
                        {
                            "name": "operation.status.attribute.run.id",
                            "value": 14
                        }],
                    "status": "ERROR"
                },
                {
                    "id": -1,
                    "doe": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "parentId": -1,
                    "startDate": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "durationInMs": 0,
                    "errorMessage": "",
                    "errorTrace": "",
                    "error": false,
                    "type": "warning",
                    "children": [],
                    "attributes": [
                        {
                            "name": "operation.status.attribute.run.id",
                            "value": 15
                        }],
                    "status": "WARNING"
                },
                {
                    "id": -1,
                    "doe": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "parentId": -1,
                    "startDate": {
                        "millisecondsSinceEpoc": 1498569583710,
                        "year": 2017,
                        "month": 6,
                        "hour": 13,
                        "minute": 19,
                        "second": 43,
                        "day": 27
                    },
                    "durationInMs": 0,
                    "errorMessage": "",
                    "errorTrace": "",
                    "error": false,
                    "type": "success",
                    "children": [],
                    "attributes": [
                        {
                            "name": "operation.status.attribute.run.id",
                            "value": 16
                        }],
                    "status": "SUCCESS"
                }
            ],
            "attributes": [
                {
                    "name": "operation.status.attribute.run.id",
                    "value": 11
                },
                {
                    "name": "appPackage",
                    "value": "server.admin"
                },
                {
                    "name": "jobBean",
                    "value": "iv3d.apps.server.operations.demojob"
                },
                {
                    "name": "jobId",
                    "value": 1
                }
            ],
            "status": "WAITING"
        }

    }
}