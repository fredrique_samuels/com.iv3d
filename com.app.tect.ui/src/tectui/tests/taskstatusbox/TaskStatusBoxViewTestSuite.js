import React from 'react'
import TaskStatusBoxBasic from './tests/TaskStatusBoxBasic'

export class TaskStatusBoxViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("Task StatusBox Basic"),
            menu.createTestButton("Basic",function(){ new TaskStatusBoxBasic().run(menu.getUiContext())}.bind(menu))
        ]
    }
}