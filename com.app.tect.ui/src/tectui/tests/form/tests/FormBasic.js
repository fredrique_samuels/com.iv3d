import React from 'react'
import * as tectui from "../../../tectui";

export default class FormBasic {
    run(uicontext) {
        let builder = new tectui.FormViewBuilder()

        builder = this.textInput(builder)
        builder = this.addonLeft(builder)
        builder = this.addonRight(builder)
        builder = this.addonLeftAndRight(builder)
        builder = this.textInputDataList(builder)
        builder = this.inputTypesFormSection(builder)
        builder = this.displayTypesFormSection(builder)


        builder.buildViewToLayer(uicontext)
    }

    displayTypesFormSection(builder) {
        builder = builder.createFormSection()
            .setTitle("Block Text")
            .createBlockText()
            .setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
            .commit()
            .commit()

        builder = builder.createFormSection()
            .setTitle("Image")
            .createImage()
            .setUrl("https://www.w3schools.com/html/pic_mountain.jpg")
            .setJustifyLeft()
            .setNoLabel()
            .commit()
            .commit()


        builder = builder.createFormSection()
            .setTitle("Info Block")
            .createInfoBlock()
            .setTitle("Lorem Ipsum?")
            .setGraphics(<i class="fa fa-home fa-2x color-primary-text"></i>)
            .setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
            .commit()
            .commit()

        builder = builder.createFormSection()
            .setTitle("Info Block With Image")
            .createInfoBlock()
            .setTitle("Lorem Ipsum?")
            .setGraphics(<img style={{maxWidth:"100%", maxHeight:"100%"}} src="/res/images/demo-info-block.png"></img>)
            .setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
            .commit()
            .commit()

        builder = builder.createFormSection()
            .setTitle("Text")
            .createText()
            .setLabel('Text Display')
            .setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
            .commit()
            .commit()

        return builder
    }

    inputTypesFormSection(builder) {
        builder = builder.createFormSection()
            .setTitle("Form Section Inputs Types")

        builder = this.passwordInput(builder);
        builder = this.textAreaInput(builder)

        //TODO HELP AND EXAMPLE TEXT NOT SHOWING
        // FOR MULTI, SINGLE AND LIST SELECT
        builder = this.multiSelect(builder)
        builder = this.singleSelect(builder)
        builder = this.listSelect(builder)

        builder = this.fileInput(builder)
        builder = this.colorInput(builder)
        builder = this.emailInput(builder)
        builder = this.urlInput(builder)
        builder = this.numberInput(builder)

        builder = builder.commit()

        return builder;
    }
    numberInput(builder) {
        return builder.createNumberInput()
            .setLabel('Number Input')
            .setMin(0)
            .setMax(10.0)
            .setName('number.input')
            .setRequired()
            .commit();
    }
    urlInput(builder) {
        return builder.createUrlInput()
            .setLabel('Url')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('url.input')
            .setRequired()
            .commit();
    }
    emailInput(builder) {
        return builder.createEmailInput()
            .setLabel('Email')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('email.input')
            .setRequired()
            .commit();
    }
    colorInput(builder) {
        return builder.createColorInput()
            .setLabel('Color')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('color.input')
            .setRequired()
            .setValue("#bd001f")
            .commit();
    }
    fileInput(builder) {
        return builder.createFileInput()
            .setLabel('File')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('file.input')
            .setRequired()
            .commit();
    }

    singleSelect(builder) {
        return builder.createSingleSelectInput()
            .setLabel('Single Select')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('singleselect.input')
            .setValue("TRAIN")
            .setRequired()

            .createOption()
            .setLabel("Car")
            .setValue("CAR")
            .commit()

            .createOption()
            .setLabel("Train")
            .setValue("TRAIN")
            .commit()

            .createOption()
            .setLabel("Bus")
            .setValue("bus")
            .commit()

            .commit();
    }

    listSelect(builder) {
        return builder.createListSelectInput()
            .setLabel('List Select')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('list.input')
            .setValue("TRAIN")
            .setRequired()

            .createOption()
            .setLabel("Car")
            .setValue("CAR")
            .commit()

            .createOption()
            .setLabel("Train")
            .setValue("TRAIN")
            .commit()

            .createOption()
            .setLabel("Bus")
            .setValue("bus")
            .commit()

            .commit();
    }

    multiSelect(builder) {
        return builder.createMultiSelectInput()
            .setLabel('Multi Select')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('multiselect.input')
            .setValue(["ORANGE"])
            .setRequired()

            .createOption()
            .setLabel("Apple")
            .setValue("APPlE")
            .commit()

            .createOption()
            .setLabel("Orange")
            .setValue("ORANGE")
            .commit()

            .commit();
    }

    textAreaInput(builder) {
        return builder.createTextAreaInput()
            .setLabel('Text Area')
            .setHelpText('Help Text')
            .setExampleText('Example Text')
            .setName('textarea.input')
            .setValue('textarea.input')
            .setRequired()
            .commit();
    }

    passwordInput(builder) {
        return builder.createPasswordInput()
            .setLabel('Password')
            .setPlaceholder("Enter password")
            .setHelpText('Help')
            .setExampleText('Please enter a valid value')
            .setName('text.password')
            .setValue('text.password')
            .setRequired()

            .createAddonLeft()
            .setFaIcon("fa-lock")
            .commit()

            .commit()
    }

    textInputDataList(builder) {
        return builder.createTextInput()
            .setLabel('Data List')
            .setPlaceholder("Enter browser name")
            .setDataList(["Internet Explorer", "Firefox", "Chrome", "Opera", "Safari"])
            .commit();
    }

    addonLeftAndRight(builder) {
        return builder.createTextInput()
            .setLabel('Text Addon Left and Right')

            .createAddonLeft()
            .setFaIcon("fa-user")
            .commit()

            .createAddonRight()
            .setText(".00")
            .commit()

            .commit();
    }

    addonRight(builder) {
        return builder.createTextInput()
            .setLabel('Text Addon Right')

            .createAddonRight()
            .setText(".00")
            .commit()

            .commit();
    }

    addonLeft(builder) {
        return builder.createTextInput()
            .setLabel('Text Addon Left')

            .createAddonLeft()
            .setFaIcon("fa-user")
            .commit()

            .commit();
    }

    textInput(builder) {
        return builder.createTextInput()
            .setLabel('Text Input')
            .setPlaceholder('Placeholder')
            .setHelpText('Help')
            .setExampleText('Please enter a valid value')
            .setName('text.input1')
            .setValue('text.input1')
            .commit();
    }



    /**


     inputs : [

     {
         type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
         title : "Text Label",
         inputs : [
             {
                 type: FormConstants.FORM_DISPLAY_TYPE_TEXT,
                 title:"Label",
                 text: "Value",
             }
         ]
     }

     ]
     }


     */
}