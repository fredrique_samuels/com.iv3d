import React from 'react'
import * as tectui from "../../../tectui";

export default class FormValidation {
    run(uicontext) {
        let builder = new tectui.FormViewBuilder()

        builder = builder.createTextInput()
            .setLabel("User Name")
            .setPlaceholder("User Name")
            .setExampleText("Joe Parmer")
            .setHelpText("Correct value is Joe Parmer")
            .setName("username")
            
            .createAddonLeft()
            .setFaIcon('fa-user')
            .commit()
            
            .commit()

        builder = builder.createPasswordInput()
            .setLabel("Password")
            .setPlaceholder("Password")
            .setHelpText("Correct value is p@ssword")
            .setName("password")

            .createAddonLeft()
            .setFaIcon('fa-lock')
            .commit()

            .commit()

        builder = builder.createSubmitButton()
            .setLabel("Login")
            .commit()

        builder.createSubmitAction()
            .setCallback(this.submitForm.bind(this))
            .commit()

        builder.createValidationAction()
            .setCallback(this.validateForm.bind(this))
            .commit()

        builder.buildViewToLayer(uicontext)
    }

    submitForm(form, result) {
        console.log("Submitting form!!");
    }

    validateForm(form, result) {
        const { dataArray, data , dataMap} = form
        const errors = []

        const username = dataMap["username"]
        if(username) {
            if(username!="Joe Parmer") {
                errors.push({field:"username", message:"User name must be 'Joe Parmer'"})
            }
        }
        else {
            errors.push({field:"username", message:"No User name provided"})
        }

        const password = dataMap["password"]
        if(password) {
            if(password!="p@ssword") {
                errors.push({field:"password", message:"Password must be 'p@ssword'"})
            }
        }
        else {
            errors.push({field:"password", message:"No Password provided"})
        }

        result.addAllFieldErrors(errors)
            .commit();

    }

}