import React from 'react'
import FormBasic from './tests/FormBasic'
import FormValidation from "./tests/FormValidation";

export default class FormViewTestSuite {
    get(menu) {
        return [
            menu.createHeading("TectUi Form"),
            menu.createTestButton("Types",function(){ new FormBasic().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Validation",function(){ new FormValidation().run(menu.getUiContext())}.bind(menu))
        ]
    }
}