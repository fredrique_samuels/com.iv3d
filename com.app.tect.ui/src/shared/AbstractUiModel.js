

export class AbstractUiModel {

    constructor(parent) {
        this.parentComp = parent
    }

    updateState(stateChanges) {this.parentComp.setState(stateChanges)}
    state() {return this.parentComp.state}
    i8nMessage(m) {return this.uicontext().getI8nMessage(m)}
    uicontext() {return this.parentComp.props.uicontext}
}