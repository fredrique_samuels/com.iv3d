import {TVisionApp} from '../tvision/model/shared/TVisionApp'

export default class LocationNewsApp extends TVisionApp {
  constructor(uicontext) {
    super(uicontext, {
      title : "News",
      icon : {
        type : "icon.type.image",
        graphic : "/res/images/icons/location_news2.png"
      },
      packageId : "com.tvision.app.news.location"
    })
  }
}
