import React from 'react'
import {generateUniqueUiId} from "../../../../tectui/tectui";
import {Timeout} from "../../../../tectui/tectui";

class DemoLoadingController {
    constructor() {

    }

    start(loadingModel) {
        new Timeout(2500, () => loadingModel.close())
    }
}


export default class AdminServerOperationsAppView extends React.Component {
    constructor() {
        super()
        this.widgetId = generateUniqueUiId()
    }

    render() {
        return <div class="fill-parent">
            {this.props.uicontext.layerComponentManager().createLayerComponent(this.widgetId, this.onLayerManagerReady.bind(this))}
        </div>
    }

    onLayerManagerReady() {
        // display loading box
    }
}