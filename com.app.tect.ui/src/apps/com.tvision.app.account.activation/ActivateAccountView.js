import React from 'react'

import {ActivateAccountForm} from './ActivateAccountForm'
import * as tectui from "../../tectui/tectui";


export class ActivateAccountView extends React.Component {

    render() {
        const {uicontext, platform} = this.props

        const formFactory  = (layerId, uicontext) => <ActivateAccountForm uicontext={uicontext} platform={platform} />
        const mountedHandler  = (uicontext) => uicontext.addStackedComponent(formFactory)

        const stackedComps = new tectui.ComponentStackViewBuilder()
            .setMountedHandler(mountedHandler)
            .buildView(uicontext);

        return (
            <div class="fill-parent">
                {stackedComps}
            </div>
        )
    }

}