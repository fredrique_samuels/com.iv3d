import React from 'react'
import * as tectui from "../../tectui/tectui";


export class ResendEmailButton extends React.Component {
    render() {
        const {uicontext, platform} = this.props

        return (
            <div class="center-content-vh" style={{widht:"100%"}}>
                <button class="btn btn-sm color-primary" onClick={this.resendEmail.bind(this)}>{platform.i8nMessage("com.tvision.app.account.activation.form.button.resendmail.label")}</button>
            </div>
        )
    }

    resendEmail() {
        const {uicontext, platform} = this.props

        const loadingText = new String(platform.i8nMessage("com.tvision.app.account.activation.form.resendmail.loading")) + " ..."
        const loadingLayerId = tectui.ViewUtils.createStackedLoadingLayer(uicontext, loadingText)
        platform.userAccountHandler().resendValidationEmail(() => {uicontext.removeStackedComponent(loadingLayerId)});
    }
}