import React from 'react'
import {TVisionApp} from '../../tvision/model/shared/TVisionApp'
import {ActivateAccountView} from './ActivateAccountView'

export class ActivateAccountApp extends TVisionApp {
    constructor(uicontext, platform) {
        super(uicontext, {
            title : platform.i8nMessage("com.tvision.app.account.activation.packageinfo.appname"),
            icon : {
                    type : "icon.type.image",
                    graphic : "/res/images/icons/config.png"
                },
            packageId : "com.tvision.app.account.activation",
            visibleInMenu:false,
            requiresAccountActivation:false,
            viewFactory : {
                createView:(uicontext, app, platform) => <ActivateAccountView uicontext={uicontext} app={app} platform={platform}/>
            }
        },
        platform)
    }
}
