import React from 'react'
import * as tectui from '../../tectui/tectui'

import {ResendEmailButton} from './ResendEmailButton'

export class ActivateAccountForm extends React.Component {

    render() {
        const {uicontext, platform} = this.props
        const closeAppHandler = () => platform.applicationHandler().destroyProcessByPackageId("com.tvision.app.account.activation");

        let builder = new tectui.FormViewBuilder()

        builder = builder.createFormSection()
            .commit()

        builder = builder.createFormSection()
            .setTitle(platform.i8nMessage("com.tvision.app.account.activation.form.help.header"))
            .createInfoBlock()
            .setNoLabel()
            .setGraphics(<img style={{maxWidth:"100%", maxHeight:"100%"}} src="/res/images/signup/activation-code.png"></img>)
            .setText(platform.i8nMessage("com.tvision.app.account.activation.form.help.resendifnoreceived"))
            .commit()

        builder = builder.addComponent(<ResendEmailButton uicontext={uicontext} platform={platform} />)
            .commit()
        const resendEmailForm = builder.buildView(this.props.uicontext);


        builder = new tectui.FormViewBuilder()
        builder = builder.createFormSection()
            .setTitle(platform.i8nMessage("com.tvision.app.account.activation.form.header"))
        builder = builder
            .createTextInput()
            .setLabel(platform.i8nMessage("com.tvision.app.account.activation.form.input.activationtoken.label"))
            .setPlaceholder(platform.i8nMessage("com.tvision.app.account.activation.form.input.activationtoken.placeholder"))
            .setHelpText(platform.i8nMessage("com.tvision.app.account.activation.form.input.activationtoken.help"))
            .setName("token")

            .createAddonLeft()
            .setFaIcon('fa-lock')
            .commit()

            .commit()

        builder = builder.commit()
            .createFormButton()
            .setLabel(platform.i8nMessage("com.tvision.app.account.activation.form.button.activatelater.label"))
            .setCallback(closeAppHandler)
            .commit()

        builder = builder.createSubmitButton()
            .setLabel(platform.i8nMessage("com.tvision.app.account.activation.form.button.activate.label"))
            .commit()

        const submitHandler = (form, formResult) => {

            const loadingText = platform.i8nMessage("com.tvision.app.account.activation.form.activate.loading");
            const loadingLayerId = tectui.ViewUtils.createStackedLoadingLayer(uicontext, loadingText)

            const {dataMap} = form
            const token = dataMap["token"]

            const successHandler = () => {
                formResult.commit()
                uicontext.removeStackedComponent(loadingLayerId)
                const text = platform.i8nMessage("com.tvision.app.account.activation.form.activate.success");
                tectui.ViewUtils.createStackedTextPopupSuccess(uicontext, text, () => closeAppHandler())
            }

            const errorHandler = () => {
                formResult.setError("").commit()
                const text = platform.i8nMessage("com.tvision.app.account.activation.form.activate.invalidtoken");
                tectui.ViewUtils.createStackedTextPopupError(uicontext, text, () => closeAppHandler())
                uicontext.removeStackedComponent(loadingLayerId)
            }

            const cbo = {
                success: successHandler,
                error: errorHandler
            }

            platform.userAccountHandler().activateAccount(token, cbo)
        }

        builder.createSubmitAction()
            .setCallback(submitHandler)
            .commit()

        const form = builder.buildView(this.props.uicontext);

        return (
            <div class="center-content-vh background-color-black-transparent" style={{height:"100%"}}>
                <div class="" style={{width:"600px", padding:"10px", overflowY:"auto"}}>
                    {resendEmailForm}
                    {form}
                </div>
            </div>
        )
    }
}