import React from 'react'
import {TVisionApp} from '../../tvision/model/shared/TVisionApp'
import TestAllWidgets from '../../tectui/tests/TestAll'

export default class AdminWidgetTestApp extends TVisionApp {
    constructor(uicontext, platform) {
        super(uicontext, {
            title : "Widgets Test",
            icon : {
                    type : "icon.type.image",
                    graphic : "/res/images/icons/config.png"
                },
            packageId : "com.tvision.app.admin.widgets.test",
            viewFactory : {createView:(uicontext, app, platform) => <TestAllWidgets />}
        },
        platform)
    }
}
