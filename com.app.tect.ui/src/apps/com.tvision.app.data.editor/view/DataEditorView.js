import React from 'react'
import * as tectui from "../../../tectui/tectui";

export class WhiteBorderContainer extends React.Component {
    render() {
        return (
            <div class="fill-parent" style={{overflowY:"auto", border:"1px solid white", backgroundColor: "rgba(0,0,0,.7)"}}>
                {this.props.children}
            </div>
        )
    }
}

export class PropertiesSection extends React.Component {
    render(){
        return (
            <div class="inline-element" style={{width:"25%", height:"100%"}}>
                <WhiteBorderContainer>
                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                </WhiteBorderContainer>
            </div>
        )
    }
}


class NoSourceDataView extends React.Component {

    constructor() {
        super()
    }

    render() {
        const setFileSource = this.setSource.bind(this)
        const onFileUploaded = function(input) {
            console.error("Uploaded file", input);
            if (input.files && input.files[0]) {
                console.error("Uploaded file", input.files[0]);

                var file = input.files[0]
                var reader = new FileReader();

                reader.onload = function (e) {
                    setFileSource(e.target.result, file)
                }

                reader.readAsDataURL(file);
            }
        };

        const form = new tectui.FormViewBuilder()
            .setId("dataSourceForm")

            .createFileInput()
            .setName("sourceData")
            .setLabel("Select Image")
            .setOnChangeCallback(onFileUploaded)
            .commit()

            .createTextInput()
            .setName("name")
            .setLabel("Name")
            .commit()

            .buildView()

        // const imgInput = new tectui.FormElementBuilder()
        //     .setFileInputType()
        //     .setLabel("Select Image")
        //     .setOnChangeCallback(onFileUploaded)
        //     .buildView();


        const {fileSrc, file} = this.state
        let fileDisplay = null
        if(fileSrc) {
            fileDisplay = new tectui.FormElementBuilder()
                .setImageDisplayType()
                .setUrl(fileSrc)
                .setNoLabel()
                .buildView();
        }


        const submitAction = () => {

            $.post("http://localhost:59180/process", $("#dataSourceForm").serialize(), function(data) {
                alert(data);
            });

            // var str = $( "form" ).serialize();
            // $post()

            var fd = new FormData();
            if(file) {
                fd.append("afile", file);
            }


            fd.append("name", "Groucho");
            var xhr = new XMLHttpRequest();
            xhr.open('post', 'http://localhost:59180/process', true);

            xhr.upload.onprogress = function(e) {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                    console.error(percentComplete + '% uploaded');
                }
            };
            xhr.onload = function() {
                if (this.status == 200) {
                    var resp = JSON.parse(this.response);
                    console.log('Server got:', resp);
                    // var image = document.createElement('img');
                    // image.src = resp.dataUrl;
                    // document.body.appendChild(image);
                };
            };
            xhr.send(fd);

        }
        const submitButton = <button class="btn btn-sm" onClick={submitAction} >submit</button>

        return (
            <div>
                {form}
                {submitButton}
                {fileDisplay}
            </div>
        )
    }

    componentWillMount() {
        this.state = {
            fileSrc : null
        }
    }

    setSource(src, file) {
        console.error(src)
        this.setState({fileSrc:src, file: file})
    }
}

export class ContentSection extends React.Component {
    render(){
        return (
            <div class="inline-element" style={{width:"50%", height:"100%"}}>
                <WhiteBorderContainer>
                    <NoSourceDataView />
                </WhiteBorderContainer>
            </div>
        )
    }
}

export class EditOptionTypeSection extends React.Component {
    render() {
        return (
            <div class="inline-element" style={{width: "25%", height: "100%"}}>
                <WhiteBorderContainer>
                </WhiteBorderContainer>
            </div>
        )
    }
}

export class EditOptionsSection extends React.Component {
    render(){
        return (
            <div class="inline-element" style={{width:"25%", height:"100%"}}>
                <WhiteBorderContainer>

                </WhiteBorderContainer>
            </div>
        )
    }
}

export class DataEditorView extends React.Component {
    render(){
        return (
            <div class="fill-parent back-panel-shadow panel-text">
                <EditOptionsSection />
                <ContentSection />
                <PropertiesSection />
            </div>
        )
    }
}