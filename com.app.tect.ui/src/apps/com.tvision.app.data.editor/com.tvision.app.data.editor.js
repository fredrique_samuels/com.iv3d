import React from 'react'
import {TVisionApp} from '../../tvision/model/shared/TVisionApp'
import {DataEditorView} from './view/DataEditorView'


export const PACKAGE_ID = "com.tvision.app.data.editor"

export class DataEditorApp extends TVisionApp {
  constructor(uicontext, platform) {
    super(uicontext, {
      title : "Data Editor",
      icon : {
        type : "icon.type.image",
        graphic : "/res/images/icons/config.png"
      },
      packageId : "com.tvision.app.data.editor",
      requiresAccountActivation:false,
      viewFactory : {createView:(uicontext, app, platform) => <DataEditorView appArgs={app.getArgs()} uicontext={uicontext} platform={platform} />}
    },
    platform)
  }
}
