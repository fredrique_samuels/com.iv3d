import React from 'react'
import {TVisionApp} from '../../tvision/model/shared/TVisionApp'


export const PACKAGE_ID = "com.tvision.app.tanome"

export class TanomeApp extends TVisionApp {
  constructor(uicontext, platform) {
    super(uicontext, {
      title : "Tanome",
      icon : {
        type : "icon.type.image",
        graphic : "/res/images/icons/config.png"
      },
      packageId : "com.tvision.app.tanome",
      requiresAccountActivation:false,
      viewFactory : {createView:(uicontext, app, platform) => null}
    },
    platform)
  }
}
