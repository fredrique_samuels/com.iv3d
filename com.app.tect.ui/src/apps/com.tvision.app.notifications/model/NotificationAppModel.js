import React from 'react'
import Immutable from 'immutable'

import {AbstractUiModel} from "../../../shared/AbstractUiModel";
import {NotificationItemView} from '../view/NotificationItemView'
import {NotificationTypeFilterButtonView} from '../view/NotificationTypeFilterButtonView'

export class NotificationAppModel extends AbstractUiModel {

    static newState() {
        return {activeFilters: new Immutable.Set()}
    }

    constructor(parent) {
        super(parent)
    }

    platform(){return this.parentComp.props.platform}

    notifications() {
        return this.platform().notificationHandler()
            .notifications()
            .filter(n => this.__isActive(n.getType()))
            .map((n) => <NotificationItemView notification={n} platform={this.platform()} />)
    }

    filterButtons() {
        const notificationTypes = this.__notificationTypes();

        const activeButtons = notificationTypes.filter( t => this.__isActive(t) )
            .map( t => <NotificationTypeFilterButtonView platform={this.platform()} model={this} type={t} isActive={true}/> )
        const nonActiveButtons = notificationTypes.filter( t => !this.__isActive(t))
            .map( t => <NotificationTypeFilterButtonView platform={this.platform()} model={this} type={t} isActive={false}/> )

        return activeButtons.concat(nonActiveButtons)
    }

    toggleFilter(t) {
        if(this.__isActive(t)) {
            this.updateState({activeFilters:this.state().activeFilters.delete(t)})
        } else {
            this.updateState({activeFilters:this.state().activeFilters.add(t)})
        }

    }

    __isActive(id) {
        return this.state().activeFilters.includes(id)
    }

    __notificationTypes() {
        const notifications = this.platform().notificationHandler().notifications()
        let typeSet = new Immutable.Set()
        for (var ni in notifications) {
            typeSet = typeSet.add(notifications[ni].getType())
        }
        return typeSet.toArray()
    }

}