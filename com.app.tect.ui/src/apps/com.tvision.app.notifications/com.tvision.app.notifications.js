import React from 'react'
import {TVisionApp} from '../../tvision/model/shared/TVisionApp'
import {NotificationsAppView} from './view/NotificationsAppView'


export const PACKAGE_ID = "com.tvision.app.notifications"

export class NotificationsApp extends TVisionApp {
  constructor(uicontext, platform) {
    super(uicontext, {
      title : "Notifications",
      icon : {
        type : "icon.type.image",
        graphic : "/res/images/icons/alert.png"
      },
      packageId : "com.tvision.app.notifications",
      requiresAccountActivation:false,
      viewFactory : {createView:(uicontext, app, platform) => <NotificationsAppView appArgs={app.getArgs()} uicontext={uicontext} platform={platform} />}
    },
    platform)
  }
}
