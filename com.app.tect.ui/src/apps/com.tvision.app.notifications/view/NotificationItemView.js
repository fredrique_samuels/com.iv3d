
import React from 'react'
import {BorderPanelView} from '../../../tectui/tectui'

export class NotificationItemView extends React.Component {
    render() {
        const { notification, platform } = this.props
        const clearNotification = () => platform.notificationHandler().deleteNotification(notification.getId())

        return (
            <div style={{width:"100%"}}>
                <BorderPanelView>
                    <div class="inline-element" style={{width:"calc(100% - 100px)",  height:"100%"}}>
                        <div class="center-content-v" style={{width:"100%"}}>
                            <div class="inline-element" >
                                <span class={"notification-icon notification-icon-"+notification.getType()+" fa-2x"} ></span>
                            </div>
                            <div class="inline-element">
                                <span >{notification.getDate().toUTCString()}</span>
                            </div>
                        </div>
                        <p>{notification.getText()}</p>
                    </div>
                    <div class="inline-element" style={{width:"100px"}}>
                        <div class="center-content-vh fill-parent" style={{paddingBottom:"20px"}}>
                            <button class="btn btn-primary btn-sm hover-shadow" onClick={clearNotification}>{platform.i8nMessage("com.tvision.app.notifications.item.button.clear")}</button>
                        </div>
                    </div>
                </BorderPanelView>
            </div>
        )
    }
}