
import React from 'react'

export class NotificationTypeFilterButtonView extends React.Component {
    render() {
        const { type, isActive, model, platform } = this.props

        const newVar = {
            margin:"2px",
            cursor:"pointer",
            fontSize:"13px"
        };
        return (
            <button onClick={ () => model.toggleFilter(type) } class={"btn btn-xs "+(isActive?"color-primary":"color-default") } style={newVar}>
                <span class={"notification-icon-"+type} style={{marginRight:"2px"}}></span>
                {platform.i8nMessage("com.tvision.app.notifications.filter."+type)}
            </button>
        )
    }

    toTitleCase(str)
    {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
}