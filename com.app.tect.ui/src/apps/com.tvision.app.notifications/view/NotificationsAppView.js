import React from 'react'
import {NotificationAppHeaderView} from './NotificationAppHeaderView'
import {NotificationAppContentView} from './NotificationAppContentView'
import {NotificationAppModel} from "../model/NotificationAppModel";

export class NotificationsAppView extends React.Component {
    constructor() {
        super()
    }
    
    componentWillMount() {
        this.state = NotificationAppModel.newState()
    }

    render() {

        const {uicontext, platform, appArgs} = this.props
        const model = new NotificationAppModel(this)

        const titledPanel = uicontext.titledPanelViewBuilder()
            .setHeading(<NotificationAppHeaderView platform={platform} /> )
            .setContent(<NotificationAppContentView uicontext={uicontext} model={model} platform={platform} />)
            .setFixedHeight("100%")
            .setStyleProperty({width:"80%", height:"100%"})
            .disableScroll()
            .buildView(uicontext);

        return (
            <div class="center-content-vh fill-parent">
                {titledPanel}
            </div>
        )
    }
}