import React from 'react'

export class NotificationAppContentView extends React.Component {

    render() {

        const {platform, model, uicontext} = this.props

        const filterPanel = uicontext.titledPanelViewBuilder()
            .setHeading(<div style={{color:"black"}}>{platform.i8nMessage("com.tvision.app.notifications.filters.label")}</div>)
            .setStyleProperty({width:"100%", padding:"5px", backgroundColor:"black"})
            .setFixedHeight("120px")
            .setMaxHeaderHeight("25px")
            .setContent(model.filterButtons())
            .buildView();

        return (
            <div class="fill-parent" style={{maxHeight:"100%", position:"relative"}}>
                <div style={{width:"100%", height:"130px", position:"absolute", top:"0px"}}>
                    {filterPanel}
                </div>
                <div class="panel-text" style={{width:"100%", maxHeight:"100%", padding:"130px 5px 5px 5px", overflowY:"auto"}}>
                    {model.notifications()}
                </div>
            </div>
        )
    }
}