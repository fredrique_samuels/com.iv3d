import React from 'react'

export class NotificationAppHeaderView extends React.Component {
    render() {
        const {platform} = this.props

        const ctrCss = {
            color:"black",
            fontSize:"16px"
        }

        const imgCtrCss = {
            height:"25px",
            width:"25px"
        }

        const imgCss = {
            height:"25px",
            width:"25px"
        }

        return (
            <div>
                <img style={imgCss} src="/res/images/icons/alert.png" />
                <div class="inline-element" style={ctrCss}>
                    {platform.i8nMessage("com.tvision.app.notifications.heading")}
                </div>
            </div>
        )
    }
}