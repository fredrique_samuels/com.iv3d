import {TVisionApp} from '../tvision/model/shared/TVisionApp'

export default class UserSettingsApp extends TVisionApp {
  constructor(uicontext) {
    super(uicontext, {
      title : "User Settings",
      icon : {
        type : "icon.type.image",
        graphic : "/res/images/icons/user.png"
      },
      packageId : "com.tvision.app.user.settings"
    })
  }
}
