import * as DndConstants from './DndConstants'

export default class CardDragData {
    constructor(source, card, ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
        this.cardId = (card && ygoEditorModel)?ygoEditorModel.getCardId(card):null
        this.source = source
    }
    addToMainDeck() {
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SEARCH_RESULTS) {
            this.ygoEditorModel.addToMainDeck(this.cardId)
        }
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SIDE_DECK) {
            this.ygoEditorModel.moveToMainDeck(this.cardId)
        }
    }
    addToSideDeck() {
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SEARCH_RESULTS) {
            this.ygoEditorModel.addToSideDeck(this.cardId)
        }
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_MAIN_DECK) {
            this.ygoEditorModel.moveToSideDeck(this.cardId)
        }
    }
    canAddToMainDeck() {
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SEARCH_RESULTS) {
            return this.ygoEditorModel.canAddToMainDeck(this.cardId)
        }
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SIDE_DECK) {
            return this.ygoEditorModel.canMoveToMainDeck(this.cardId)
        }
        return false
    }
    canAddToSideDeck() {
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SEARCH_RESULTS) {
            return this.ygoEditorModel.canAddToSideDeck(this.cardId)
        }
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_MAIN_DECK) {
            return this.ygoEditorModel.canMoveToSideDeck(this.cardId)
        }
        return false
    }
    canDeleteCard() {
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_MAIN_DECK) {
            return true
        }
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SIDE_DECK) {
            return true
        }
        return false
    }
    deleteCard() {
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_MAIN_DECK) {
            return this.ygoEditorModel.deleteFromMainDeck(this.cardId)
        }
        if(this.source==DndConstants.YGO_CARD_DRAG_SOURCE_SIDE_DECK) {
            return this.ygoEditorModel.deleteFromSideDeck(this.cardId)
        }
    }
    toJson(){
        return JSON.stringify({cardId:this.cardId, source:this.source})
    }
    fromJson(json){
        const obj = JSON.parse(json)
        this.cardId = obj.cardId
        this.source = obj.source
        return this
    }
    populateEvent(event) {
        event.dataTransfer.setData(DndConstants.YGO_CARD_DRAG_DATA,this.toJson())
    }
    fromEvent(event, ygoEditorModel) {
        const data = event.dataTransfer.getData(DndConstants.YGO_CARD_DRAG_DATA);
        if(data) {
            this.ygoEditorModel = ygoEditorModel
            return this.fromJson(data)
        }
        return null
    }

}