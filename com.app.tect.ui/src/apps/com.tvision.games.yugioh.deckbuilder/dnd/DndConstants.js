

export const YGO_CARD_DRAG_DATA = "ygo.card.drag.data"
export const YGO_CARD_DRAG_SOURCE_SEARCH_RESULTS = "ygo.card.drag.source.search"
export const YGO_CARD_DRAG_SOURCE_MAIN_DECK = "ygo.card.drag.source.maindeck"
export const YGO_CARD_DRAG_SOURCE_SIDE_DECK = "ygo.card.drag.source.sidedeck"