import AbstractParamsBuilder from "../../../platform/builder/AbstractParamsBuilder";
import {ResourceLoader} from "./ResourceLoader";

export class ResourceLoadingConfigBuilder  extends AbstractParamsBuilder {
    constructor(cb) {
        super({
            message: "",
            load : (loadContextParams, cbo) => {cbo.success(null)},
            success : (loadContextParams, data) => {}
        }, cb)
    }

    setSuccessHandler(h){
        if(h) {
            this.params.success = h
        }
        return this
    }

    setMessage(m){
        if(m) {
            this.params.message = m
        }
        return this
    }

    setLoadHandler(h){
        if(h)
            this.params.load=h
        return this
    }
}
/**

 new ResourceLoadingBuilder()
 .setParams({})

 .createResource()
 .setMessage("Loading resource 1"
 .setSuccessAction((data) => {resource1:data})
 .commit()

 .createResource()
 .setMessage("Loading resource 2)
 .setSuccessHandler((data) => {resource2:data})
 .commit()

 .setErrorHandler()
 .build()

 */
export class AbstractResourceLoaderBuilder extends AbstractParamsBuilder {

    constructor(cb) {
        super({
            resources:[],
            loadContextParams:{},
            errorHandler:(e) => {
                console.error(e)
            }
        }, cb)
    }

    addContextParams(p) {this.params.loadContextParams = Object.assign({}, this.params.loadContextParams, p);return this}
    setErrorHandler(h){this.params.errorHandler = h;return this}
    createResource(){return new ResourceLoadingConfigBuilder(this.__addResource.bind(this))}
    __addResource(r){this.params.resources.push(r);return this}
    build(){
        const p = super.build()
        return new ResourceLoader(p.resources, p.loadContextParams, p.errorHandler)
    }
}