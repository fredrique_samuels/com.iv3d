
export class AbstractLoadController {

    constructor() {
        this.__loadModel = null
    }

    start(model) {
        this.__loadModel = model
    }
    running(){return this.__loadModel!=null}
    model(){return this.__loadModel}
    success(data) {
    }

    error(e) {
        console.error(e)
        this.__loadModel.update()
            .setDone()
            .setError(e)
            .run()
    }
    done(){
        this.__loadModel.update()
            .setDone()
            .run()
    }
    canceled() {
        return this.isCanceled
    }

    stop() {
        this.isCanceled = true
    }
}