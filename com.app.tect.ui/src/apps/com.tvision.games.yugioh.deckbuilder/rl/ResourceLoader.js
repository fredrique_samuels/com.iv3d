import {AbstractLoadController} from "./AbstractLoadController";
import * as tectui from "../../../tectui/tectui";


export class ResourceLoader extends AbstractLoadController {
    constructor(resources, loadContextParams, errorHandler){
        super()
        this.resources = resources
        this.loadContextParams = loadContextParams
        this.errorHandler = errorHandler
        this.percPerResource = 90.0 / this.resources.length
        this.progress = 10
        this.currentResource = -1

    }
    start(model) {
        if(this.running())
            return
        super.start(model)
        this.loadNextResource()
    }

    loadNextResource() {
        const r = this.nextResource()
        if(r) {
            this.model().update()
                .setText(r.message)
                .setProgress(this.progress)
                .run()
            r.load(this.loadContextParams,
                {
                    success:((data) => {
                        r.success(this.loadContextParams, data)
                        this.progress += this.percPerResource
                        this.loadNextResource()
                    }).bind(this),
                    error:this.error.bind(this)
                })
        } else {
            this.done()
        }
    }
    nextResource() {
        if(this.resources.length == 0 || this.currentResource==this.resources.length-1) {
            return null
        }
        this.currentResource += 1
        return this.resources[this.currentResource]
    }
}