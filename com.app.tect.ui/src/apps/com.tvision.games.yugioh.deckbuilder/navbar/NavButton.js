import React from 'react'

export default class NavButton extends React.Component {
    render() {
        const { icon, disabled, isActive, action } = this.props
        const buttonClassValues = "tvision-nav-menu-button shadow"
            + (isActive?" color-primary":" tvision-nav-menu-button-default")
            + (disabled?" tvision-nav-menu-button-disabled":"")
        const iconClassValues = "fa " + icon + " fa-2x"
        return <button onClick={action} class={buttonClassValues} disabled={disabled}><i class={iconClassValues}></i></button>
    }
}