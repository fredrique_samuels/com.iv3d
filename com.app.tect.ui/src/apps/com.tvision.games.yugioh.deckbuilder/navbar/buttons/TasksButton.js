import React from 'react'
import NavButton from '../NavButton'
import * as tectui from "../../../../tectui/tectui";
import {LoadCardTaskLists} from "./LoadCardTaskLists";

export class TasksButton extends React.Component {
    render() {
        const action = this.clicked.bind(this)
        return <NavButton icon="fa-tasks" disabled={this.disabled()} action={action}/>
    }
    disabled() {
        const { ygoEditorModel } = this.props.params
        return ygoEditorModel.inTasksMode()
    }
    clicked() {
        const { ygoEditorModel, uicontext } = this.props.params
        ygoEditorModel.setTasksMode()
        new tectui.LoadingViewBuilder()
            .setController(new LoadCardTaskLists(ygoEditorModel))
            .buildViewToLayer(uicontext)
    }
}