import React from 'react'
import NavButton from '../NavButton'
import FormFactory from "../forms/FormFactory";

export default class ClearDeckButton extends React.Component {
    render() {
        const { ygoEditorModel } = this.props.params
        const disabled = !ygoEditorModel.inDeckEditMode() || ygoEditorModel.isDeckEmpty()
        const action = this.clicked.bind(this)
        return <NavButton icon="fa-close" disabled={disabled} action={action}/>
    }
    clicked() {
        const { ygoEditorModel } = this.props.params
        new FormFactory(ygoEditorModel).clearDeck()
    }
}