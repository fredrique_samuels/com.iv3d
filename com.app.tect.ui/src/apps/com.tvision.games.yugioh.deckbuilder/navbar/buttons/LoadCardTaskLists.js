import {TTServerGateway} from "../../../../rest/ttserver";


export class LoadCardTaskLists {
    constructor(ygoEditorModel, gw){
        this.ygoEditorModel = ygoEditorModel;
        this.gw = gw
        this.isCanceled = false
    }
    start(model) {
        this.model = model
        this.model.update()
            .setText("Loading Task List...")
            .setProgress(60)
            .run()
        new TTServerGateway(this.ygoEditorModel.getUiContext()).admin_GetAppByPackageId(this, "cardserver")
    }
    success(data){
        if(this.canceled())
            return
        this.model.update()
            .setDone()
            .run()
        this.ygoEditorModel.setJobsData(data)
    }
    error(req, e){}
    canceled(){return this.isCanceled}
    stop() {this.isCanceled = true}
}