import React from 'react'
import NavButton from '../NavButton'

export default class SampleHandsModeButton extends React.Component {
    render() {
        const { ygoEditorModel } = this.props.params
        const isActive = ygoEditorModel.inSampleHandsMode()
        const action = isActive?null:ygoEditorModel.setSampleHandsMode.bind(ygoEditorModel)
        return <NavButton icon="fa-hand-paper-o" isActive={isActive} disabled={this.disabled()} action={action}/>
    }
    disabled() {
        const { ygoEditorModel } = this.props.params
        return  ygoEditorModel.getMainDeckCards().length<6
    }
}