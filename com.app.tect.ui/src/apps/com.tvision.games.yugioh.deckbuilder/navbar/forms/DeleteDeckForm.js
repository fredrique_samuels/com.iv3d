

export default class DeleteDeckForm {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    render() {
        const uicontext = this.ygoEditorModel.getUiContext();
        uicontext.formViewBuilder()

            .createSubmitAction()
            .setCallback(this.clearDeck.bind(this))
            .commit()

            .createCancelButton()
            .setLabel(this.ygoEditorModel.getI8nMessage('ygo.edit.cleardeck.cancel'))
            .commit()

            .createSubmitButton()
            .setLabel(this.ygoEditorModel.getI8nMessage('ygo.edit.deletedeck.submit'))
            .commit()

            .createBlockText()
            .setText(this.ygoEditorModel.getI8nMessage('ygo.edit.deletedeck.notice'))
            .commit()

            .buildViewToLayer(uicontext)
    }
    clearDeck(form) {
        this.ygoEditorModel.deleteDeck(this.ygoEditorModel.getDeckName())
        return true
    }

}