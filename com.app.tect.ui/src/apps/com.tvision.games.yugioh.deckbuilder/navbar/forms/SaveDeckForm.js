
const DECK_NAME_FIELD = "deckname";

export class SaveDeckForm {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    render() {
        const uicontext = this.ygoEditorModel.getUiContext();
        uicontext.formViewBuilder()

            .createSubmitAction()
            .setCallback(this.submitForm.bind(this))
            .commit()

            .createValidationAction()
            .setCallback(this.validateForm.bind(this))
            .commit()

            .createCancelButton()
            .setLabel(this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.cancel"))
            .commit()

            .createSubmitButton()
            .setLabel(this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.submit"))
            .commit()

            .createTextInput()
            .setLabel(this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.text"))
            .setPlaceholder(this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.placeholder"))
            .setExampleText(this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.exampletext"))
            .setHelpText(this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.helptext"))
            .setName(DECK_NAME_FIELD)
            .setValue(this.ygoEditorModel.getDeckName())
            .commit()

            .buildViewToLayer(uicontext)
    }
    submitForm(form) {
        const { dataMap } = form
        this.ygoEditorModel.saveDeck(dataMap[DECK_NAME_FIELD])
        return true
    }
    validateForm(form) {
        const { dataArray, data , dataMap} = form
        const errors = []

        const deckname = dataMap[DECK_NAME_FIELD]
        if(deckname) {
            if(deckname=="") {
                errors.push({
                    field:DECK_NAME_FIELD,
                    message:this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.validate.deckname.empty")
                })
            }
        }
        else {
            errors.push({
                field:DECK_NAME_FIELD,
                message:this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.validate.deckname.notprovided")
            })
        }

        return {
            passed:errors.length==0,
            errors: errors
        }
    }
}

