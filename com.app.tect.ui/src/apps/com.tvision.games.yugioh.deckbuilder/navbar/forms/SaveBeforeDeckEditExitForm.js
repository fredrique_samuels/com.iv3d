
export default class SaveBeforeDeckEditExitForm {

    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    render() {
        const uicontext = this.ygoEditorModel.getUiContext();
        uicontext.formViewBuilder()
            .createCancelButton()
            .setLabel(this.ygoEditorModel.getI8nMessage('ygo.edit.savebeforeexit.cancel'))
            .commit()

            .createFormButton()
            .setLabel(this.ygoEditorModel.getI8nMessage('ygo.edit.savebeforeexit.discard'))
            .setCallback(this.discardAndContinue.bind(this))
            .commit()

            .createBlockText()
            .setText(this.ygoEditorModel.getI8nMessage('ygo.edit.savebeforeexit.notice'))
            .commit()

            .buildViewToLayer(uicontext)
    }
    discardAndContinue(event, form) {
        form.close()
        this.ygoEditorModel.setDeckSelectMode()
    }
}
