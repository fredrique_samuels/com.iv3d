
import ClearDeckForm from "./ClearDeckForm";
import SaveBeforeDeckEditExitForm from "./SaveBeforeDeckEditExitForm";
import DeleteDeckForm from "./DeleteDeckForm";
import {SaveDeckForm} from "./SaveDeckForm";

export default class FormFactory {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }

    saveDeck(){
        new SaveDeckForm(this.ygoEditorModel).render()
    }

    clearDeck() {
        new ClearDeckForm(this.ygoEditorModel).render()
    }

    deleteDeck() {
        new DeleteDeckForm(this.ygoEditorModel).render()
    }

    saveBeforeDeckEditExit() {
        new SaveBeforeDeckEditExitForm(this.ygoEditorModel).render()
    }
}