import React from 'react'
import Fetch from 'react-fetch'
import StyleCache from '../styles/StyleCache'

import EditDeckModeButton from './buttons/EditDeckModeButton'
import DeckSelectModeButton from './buttons/DeckSelectModeButton'
import SampleHandsModeButton from './buttons/SampleHandsModeButton'
import SaveDeckButton from './buttons/SaveDeckButton'
import ClearDeckButton from './buttons/ClearDeckButton'
import DeleteDeckButton from './buttons/DeleteDeckButton'
import {TasksButton} from './buttons/TasksButton'

export default class NavBar extends React.Component {
  render() {
      const { uicontext } = this.props.params
      return <div class="inline-element back-panel" style={new StyleCache().get('nav-bar')}>
          <div style={navButtongroup}>
              <SaveDeckButton params={this.props.params} />
              <ClearDeckButton params={this.props.params} />
          </div>

          <div style={navButtongroup}>
              <DeleteDeckButton params={this.props.params} />
          </div>

          <div style={navButtongroup}>
              <EditDeckModeButton params={this.props.params} />
              <DeckSelectModeButton  params={this.props.params} />
              <SampleHandsModeButton params={this.props.params} />
          </div>

          <div style={navButtongroup}>
                  <TasksButton params={this.props.params} uicontext={uicontext}/>
          </div>
      </div>

      const navButtongroup = {
        marginBottom:"40px"
      }
  }
}
