import React from 'react'

import AbstractViewContainer from '../AbstractViewContainer'
import YgoCardView from '../YgoCardView'
import StyleCache from "../styles/StyleCache";

export default class SampleHandsView extends React.Component {
    render() {
        const styleCache = new StyleCache();
        const { ygoEditorModel } = this.props.params
        const cards = ygoEditorModel.getSampleOpeningHand();
        return (<AbstractViewContainer hidden={!ygoEditorModel.inSampleHandsMode()} >
            <div class="fill-parent"  style={{color:"lightgrey"}}>
                <div >
                    <div class="back-panel center-content-vh" style={styleCache.get("card-count-label")} >{ygoEditorModel.getI8nMessage("ygo.edit.samplehands.heading")}</div>
                    <div class="center-content-vh" style={{width:"100%", height:"50%"}}>
                        {
                            cards.length < 6 ? null :
                        [<div class="inline-element" style={{width:"16%", height:"100%"}}>
                            <YgoCardView card={cards[0]} ygoEditorModel={ygoEditorModel}></YgoCardView>
                        </div>,
                        <div class="inline-element" style={{width:"16%", height:"100%"}}>
                            <YgoCardView card={cards[1]} ygoEditorModel={ygoEditorModel}></YgoCardView>
                        </div>,
                        <div class="inline-element" style={{width:"16%", height:"100%"}}>
                            <YgoCardView card={cards[2]} ygoEditorModel={ygoEditorModel}></YgoCardView>
                        </div>,
                        <div class="inline-element" style={{width:"16%", height:"100%"}}>
                            <YgoCardView card={cards[3]} ygoEditorModel={ygoEditorModel}></YgoCardView>
                        </div>,
                        <div class="inline-element" style={{width:"16%", height:"100%"}}>
                            <YgoCardView card={cards[4]} ygoEditorModel={ygoEditorModel}></YgoCardView>
                        </div>,
                        <div class="inline-element" style={{width:"16%", height:"100%", border:"1px solid red"}}>
                            <YgoCardView card={cards[5]} ygoEditorModel={ygoEditorModel}></YgoCardView>
                        </div>]
                        }
                    </div>
                    <div class="center-content-vh" style={{width:"100%"}}>
                        <button class="btn  btn-sm color-primary" onClick={ygoEditorModel.refreshSampleOpeningHand.bind(ygoEditorModel)}><i class="fa fa-refresh fa-2x"></i></button>
                    </div>
                </div>
            </div>
        </AbstractViewContainer>)
    }
}
