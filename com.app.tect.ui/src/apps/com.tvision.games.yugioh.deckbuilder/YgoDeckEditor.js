import React from 'react'
import StyleCache from './styles/StyleCache'

import NavBar from './navbar/NavBar'
import DeckEditView from './deckedit/DeckEditView'
import DeckSelectView from './deckselect/DeckSelectView'
import SampleHandsView from './samplehands/SampleHandsView'
import {AppTasksView} from './tasks/AppTasksView'


class EditorContent extends React.Component {

  render() {
    const { params } = this.props

    return (
      <div class="fill-parent" style={{padding:"10px"}}>
        <NavBar params={params} />
        <DeckEditView params={params} />
        <DeckSelectView params={params} />
        <SampleHandsView params={params} />
        <AppTasksView params={params} />
    </div>
    )
  }
}

class EditorContentFactory {
    createContent(layerModel) {
        const { contentParams } = layerModel
        const { uicontext, ygoEditorModel } = contentParams
        const params = { uicontext, ygoEditorModel }
        return <EditorContent params={params}/>
    }
}

const YGO_EDITOR_LAYER_MANAGER_ID = 'com.tvision.games.yugioh.deckbuilder.layerid'
export default class YgoDeckEditor extends React.Component {

  render() {
    return <div class="fill-parent" style={{}}>
        {this.props.uicontext.layerComponentManager().createLayerComponent(YGO_EDITOR_LAYER_MANAGER_ID, this.onLayerManagerReady.bind(this))}
      </div>
  }

  onLayerManagerReady() {
      const uicontext = this.props.uicontext.setLayerComponentModelId(YGO_EDITOR_LAYER_MANAGER_ID)
      const ygoEditorModel = this.props.ygoEditorModel.setUiContext(uicontext)
      var contentParams = {
          contentHandlerFactory: new EditorContentFactory(),
          ygoEditorModel:ygoEditorModel
      };
      uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:false})
  }
}
