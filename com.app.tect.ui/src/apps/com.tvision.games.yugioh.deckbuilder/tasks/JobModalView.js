import React from 'react'
import {StatusIcon} from "../../../tectui/tectui";
import {TaskStatusRunner} from "./TaskStatusRunner";
import * as tectui from "../../../tectui/tectui";

export class JobModalView extends React.Component {

    componentWillMount() {
        const {job, jobStatus, runHistory, uicontext} = this.props
        this.state = {
            job:job,
            jobStatus:jobStatus,
            runHistory:runHistory,
            uicontext:uicontext
        }
    }

    render() {
        const {job, uicontext} = this.state
        const jobInfoView = uicontext.formViewBuilder()

            .createBlockText()
            .setText(job.description ? job.description : "No description available.")
            .setNoLabel()
            .commit()

            .createText()
            .setLabel("Last Updated")
            .setText(new Date(job.doe.millisecondsSinceEpoc).toDateString())
            .commit()

            .buildView()


        return (
                <div class="fill-parent" style={{minWidth: "500px"}}>
                    <div style={{width: "100%", overflow:"auto"}}>
                        <div style={{width: "100%", height:"30px"}}>
                            <button onClick={this.scheduleJob.bind(this)} style={{float: "right", marginTop: "5px", marginRight: "5px"}} class="btn btn-xs color-primary hover-shadow-light">
                                <i class="fa fa-play"></i>
                            </button>
                            <button onClick={this.refreshStatus.bind(this)} style={{float: "right", marginTop: "5px", marginRight: "5px"}} class="btn btn-xs color-primary hover-shadow-light">
                                <i class="fa fa-refresh"></i>
                            </button>
                            {this.createStatusButton()}
                        </div>
                        <div style={{width: "100%"}}>
                            {jobInfoView}
                        </div>
                    </div>
                </div>
            )
    }

    createStatusButton() {
        const {jobStatus} = this.state
        const ts = new tectui.TaskStatus(jobStatus);
        const runId = ts.runId();
        if(runId>0) {
            return (
                <button onClick={ this.showStatusBox.bind(this) }
                        style={{float: "right", marginTop: "5px", marginRight: "5px"}}
                        class="btn btn-xs color-default hover-shadow-light">
                    <StatusIcon status={ts.status()}/> {'#'+runId}
                </button>
            )
        }
        return null
    }

    scheduleJob() {
        const {job, uicontext} = this.state
        TaskStatusRunner.scheduleJob(job, uicontext, this.updateJobStatus.bind(this))
    }
    refreshStatus() {
        const {job, jobStatus, uicontext} = this.state
        const ts = new tectui.TaskStatus(jobStatus);
        TaskStatusRunner.refreshRunStatus(job.id, ts.runId(), uicontext, this.updateJobStatus.bind(this))
    }

    showStatusBox() {
        const {job, jobStatus, uicontext} = this.state
        const taskStatus = new tectui.TaskStatus(jobStatus);
        TaskStatusRunner.showJobRunStatusBox(job, taskStatus.runId(), uicontext)
    }

    updateJobStatus(jobStatus) {
        this.setState({jobStatus:jobStatus})
    }

}