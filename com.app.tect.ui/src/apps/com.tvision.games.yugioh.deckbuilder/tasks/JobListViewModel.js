import React from 'react'

import * as tectui from "../../../tectui/tectui";
import {AbstractResourceLoaderBuilder} from "../rl/AbstractResourceLoaderBuilder";
import {TTServerGateway} from "../../../rest/ttserver";
import {TaskStatusRunner} from "./TaskStatusRunner";

export class JobListViewModel {

    constructor(model, uicontext) {
        this.model = model
        this.uicontext = uicontext


        const dvr = {
            render: (data) => {

                const builder = new tectui.CardOptionOverlaysBuilder()
                    .addView(
                        <div class="fill-parent center-content-vh">
                            <i class="fa fa-tasks fa-4x"></i>
                        </div>
                    );

                return builder.build()
            }
        }

        this.cardsBuilder = uicontext.cardSelectViewBuilder()
            .setHeading("Job List")
            .setDataViewRenderer(dvr)

        const jobs = this.model.getJobs()
        jobs.forEach(job => this.createJobOption(job))
    }

    createJobOption(job) {
        this.cardsBuilder = this.cardsBuilder.createOption()
            .setLabel(job.name)
            .setAction((job) => this.createJobModal(job))
            .setData(job)
            .commit()
    }
    cardsView(){
        return this.cardsBuilder.buildView(this.uicontext)
    }

    createJobModal(job) {

        TaskStatusRunner.showJobModel(job, this.uicontext)

        // const controller = new AbstractResourceLoaderBuilder()
        //     .addContextParams({
        //         job:job,
        //         uicontext:this.uicontext
        //     })
        //
        //     .createResource()
        //     .setMessage("Loading job status")
        //     .setLoadHandler((loadContextParams, cbo) => new TTServerGateway(loadContextParams.uicontext).admin_GetJobStatus(cbo, loadContextParams.job.id))
        //     .setSuccessHandler((loadContextParams, data) => loadContextParams.jobStatus = data)
        //     .commit()
        //
        //     .createResource()
        //     .setMessage("Loading job history")
        //     .setLoadHandler((loadContextParams, cbo) =>
        //                 {
        //                     const taskStatus = new tectui.TaskStatus(loadContextParams.jobStatus);
        //                     new TTServerGateway(loadContextParams.uicontext).admin_GetJobHistory(cbo, taskStatus.jobId(), taskStatus.runId())
        //                 })
        //     .setSuccessHandler((loadContextParams, runHistory) => {new TaskStatusRunner(loadContextParams.uicontext, loadContextParams.job, loadContextParams.jobStatus, runHistory).run();})
        //     .commit()
        //
        //     .build()
        //
        // new tectui.LoadingViewBuilder()
        //     .setController(controller)
        //     .buildViewToLayer(this.uicontext)
    }


}