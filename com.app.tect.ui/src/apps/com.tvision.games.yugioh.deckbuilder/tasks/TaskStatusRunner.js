import React from 'react'
import * as tectui from "../../../tectui/tectui";
import {StatusIcon} from "../../../tectui/tectui";
import {TTServerGateway} from "../../../rest/ttserver";
import {AbstractResourceLoaderBuilder} from "../rl/AbstractResourceLoaderBuilder";
import {JobModalView} from './JobModalView'

export class TaskStatusRunner {

    constructor(uicontext, job, jobStatus, runHistory) {
        this.uicontext = uicontext;
        this.job=job
        this.jobStatus = jobStatus
        this.runHistory = runHistory
        console.error("Model Status", this.jobStatus, this.runHistory)
    }
    run() {
        this.createModal(this.job, this.jobStatus, this.runHistory)
    }
    createModal(job, jobStatus, runHistory) {
        this.uicontext.titledPanelViewBuilder()
            .setHeading(job.name)
            .setContent(
                <JobModalView job={job} jobStatus={jobStatus} runHistory={runHistory} uicontext={this.uicontext} />
            )
            .setStyleProperty({backgroundColor: "rgba(0,0,0,.8)"})
            .createIcon()
            .setFaIcon('fa-tasks')
            .commit()

            .buildViewToLayer(this.uicontext)
    }

    statusButton(job, jobStatus, runHistory, uicontext) {
        return (
            <button onClick={ (() => {this.showStatusBox(job, jobStatus, runHistory, uicontext)}).bind(this) } style={{float: "right", marginTop: "5px", marginRight: "5px"}} class="btn btn-xs color-default hover-shadow-light">
                <StatusIcon status={jobStatus.status}/>
            </button>
        )
    }

    showStatusBox(job, jobStatus, runHistory, uicontext) {
        new tectui.TaskStatusBoxViewBuilder()
            .setTaskData(jobStatus)
            .setHistoryItemClickedCallback( (item) => {
                const taskStatus = new tectui.TaskStatus(jobStatus);
                if(item.runId!=taskStatus.runId()) {
                    console.error(item)
                    TaskStatusRunner.showJobRunStatusBox(job, item.runId, uicontext)
                }
            })
            .setRefreshStateHandler((cb) => {
                const taskStatus = new tectui.TaskStatus(jobStatus);
                TaskStatusRunner.refreshRunStatus(job.id, taskStatus.runId(), uicontext, cb)
            })
            .setHistoryData(runHistory)
            .buildViewToLayer(this.uicontext)
    }

    static showJobModel(job, uicontext) {
        const controller = new AbstractResourceLoaderBuilder()
            .addContextParams({
                job:job,
                uicontext:uicontext
            })

            .createResource()
            .setMessage("Loading job status")
            .setLoadHandler((loadContextParams, cbo) => new TTServerGateway(loadContextParams.uicontext).admin_GetJobStatus(cbo, loadContextParams.job.id))
            .setSuccessHandler((loadContextParams, data) => loadContextParams.jobStatus = data)
            .commit()

            .createResource()
            .setMessage("Loading job history")
            .setLoadHandler((loadContextParams, cbo) =>
            {
                const taskStatus = new tectui.TaskStatus(loadContextParams.jobStatus);
                new TTServerGateway(loadContextParams.uicontext).admin_GetJobHistory(cbo, taskStatus.jobId(), taskStatus.runId())
            })
            .setSuccessHandler((loadContextParams, runHistory) => {new TaskStatusRunner(loadContextParams.uicontext, loadContextParams.job, loadContextParams.jobStatus, runHistory).run();})
            .commit()

            .build()

        new tectui.LoadingViewBuilder()
            .setController(controller)
            .buildViewToLayer(uicontext)
    }

    static scheduleJob(job, uicontext, statusCb) {
        const controller = new AbstractResourceLoaderBuilder()
            .addContextParams({
                job:job,
                uicontext:uicontext
            })

            .createResource()
            .setMessage("Scheduling job")
            .setLoadHandler((loadContextParams, cbo) => new TTServerGateway(loadContextParams.uicontext).admin_ScheduleJobRunForAppId(cbo, loadContextParams.job.appId, loadContextParams.job.beanId))
            .setSuccessHandler((loadContextParams, jobStatus) => {
                if(statusCb)statusCb(jobStatus)
                const ts = new tectui.TaskStatus(jobStatus);
                TaskStatusRunner.showJobRunStatusBox(loadContextParams.job, ts.runId(), loadContextParams.uicontext)
            })
            .commit()

            .build()

        new tectui.LoadingViewBuilder()
            .setController(controller)
            .buildViewToLayer(uicontext)
    }

    static showJobRunStatusBox(job, runId, uicontext) {
        const controller = new AbstractResourceLoaderBuilder()
            .addContextParams({
                job:job,
                runId:runId,
                uicontext:uicontext
            })

            .createResource()
            .setMessage("Loading job status")
            .setLoadHandler((loadContextParams, cbo) => {
                new TTServerGateway(loadContextParams.uicontext).admin_GetJobRunStatus(cbo, loadContextParams.job.id, loadContextParams.runId)
            })
            .setSuccessHandler((loadContextParams, data) => loadContextParams.jobStatus = data)
            .commit()

            .createResource()
            .setMessage("Loading job history")
            .setLoadHandler((loadContextParams, cbo) =>
            {
                const taskStatus = new tectui.TaskStatus(loadContextParams.jobStatus);
                new TTServerGateway(loadContextParams.uicontext).admin_GetJobHistory(cbo, taskStatus.jobId(), taskStatus.runId())
            })
            .setSuccessHandler((loadContextParams, runHistory) => {
                new tectui.TaskStatusBoxViewBuilder()
                    .setTaskData(loadContextParams.jobStatus)
                    .setHistoryItemClickedCallback( (item) => {
                        const taskStatus = new tectui.TaskStatus(loadContextParams.jobStatus);
                        if(item.runId!=taskStatus.runId()) {
                            TaskStatusRunner.showJobRunStatusBox(loadContextParams.job, item.runId, uicontext)
                        }
                    })
                    .setRefreshStateHandler((cb) => {
                        console.log("Running Refresh")
                        TaskStatusRunner.refreshRunStatus(loadContextParams.job.id, loadContextParams.runId, loadContextParams.uicontext, cb)
                    })
                    .setHistoryData(runHistory)
                    .buildViewToLayer(loadContextParams.uicontext)
            })
            .commit()

            .build()

        new tectui.LoadingViewBuilder()
            .setController(controller)
            .buildViewToLayer(uicontext)
    }

    static refreshRunStatus(jobId, runId, uicontext, cb) {
        const controller = new AbstractResourceLoaderBuilder()
            .addContextParams({
                jobId:jobId,
                runId:runId,
                uicontext:uicontext,
                cb:cb
            })

            .createResource()
            .setMessage("Loading job status")
            .setLoadHandler((loadContextParams, cbo) => {
                new TTServerGateway(loadContextParams.uicontext).admin_GetJobRunStatus(cbo, loadContextParams.jobId, loadContextParams.runId)
            })
            .setSuccessHandler((loadContextParams, data) => loadContextParams.cb(data))
            .commit()

            .build()

        new tectui.LoadingViewBuilder()
            .setController(controller)
            .buildViewToLayer(uicontext)
    }
}