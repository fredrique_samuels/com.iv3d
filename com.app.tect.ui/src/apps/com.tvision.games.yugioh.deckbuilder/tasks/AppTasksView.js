import React from 'react'
import AbstractViewContainer from '../AbstractViewContainer'
import * as tectui from "../../../tectui/tectui";
import {JobListViewModel} from "./JobListViewModel";

export class AppTasksView extends React.Component {
    render() {
        const {ygoEditorModel, uicontext} = this.props.params
        const jobListViewModel = new JobListViewModel(ygoEditorModel, uicontext);

        return (
            <AbstractViewContainer hidden={!ygoEditorModel.inTasksMode()} >
                {jobListViewModel.cardsView()}
            </AbstractViewContainer>
        )
    }
}