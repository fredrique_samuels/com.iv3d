import React from 'react'
import * as YgoCardConstants from './model/YgoCardParam'


class Svg extends React.Component {
    render() {
        return (<svg draggable="true" onDragStart={this.dragStarted.bind(this)} width="100%" height="100%" viewBox="0 0 343 500" version="1.1"
                     xmlns="http://www.w3.org/2000/svg" xmlnsXlink= "http://www.w3.org/1999/xlink">
            <defs>
                <clipPath id="myClip">
                    <rect x="165" y="65" width="175" height="175"/>
                </clipPath>
                <clipPath id="pendulumClip">
                    <rect x="165" y="65" width="175" height="145"/>
                </clipPath>
            </defs>
            {this.props.children}
        </svg>)
    }
    dragStarted(event){
        const { card, dragStartedCallback } = this.props
        if(dragStartedCallback) {
            dragStartedCallback(card, event)
        }
    }
}

class Image extends React.Component {
    render() {
        const { image, baseUrl} = this.props
        return <image xlinkHref={baseUrl+image} x="0" y="0" height="500px" width="343px" />
    }
}


class CardBackSvg extends React.Component {
    render() {
        const { baseUrl } = this.props
        return (
            <Svg>
                <Image baseUrl={baseUrl} image={"card-back-1.png"} />
            </Svg>
        )
    }
}

class CardBgColorImg extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null

        if(card.isLinkMonster()) {
            return <Image baseUrl={baseUrl} image={"link-box.png"} />
        }
        if(card.isXyzMonster()) {
            return <Image baseUrl={baseUrl} image={"xyz-box.png"} />
        }
        if(card.isSynchroMonster()) {
            return <Image baseUrl={baseUrl} image={"synchro-box.png"} />
        }
        if(card.isRitualMonster()) {
            return <Image baseUrl={baseUrl} image={"ritual-box.png"} />
        }
        if(card.isFusionMonster()) {
            return <Image baseUrl={baseUrl} image={"fusion-box.png"} />
        }
        if(card.isEffectMonster()) {
            return <Image baseUrl={baseUrl} image={"effect-box.png"} />
        }

        return <Image baseUrl={baseUrl} image={"normal-box.png"} />
    }
}

class CardImage extends React.Component {
    render() {
        const { card } = this.props.params
        const imageUrl = card.getImageUrl()
        const imageKey = "http://localhost:58080/cardserver/?image=" + card.getImageKey()
        if(card.isPendulumMonster())
            return <image clipPath="url(#pendulumClip)" xlinkHref={imageKey} x="0" y="0" height="343px" width="500px" transform="scale(2 2.5) translate(-165 -63)" />
        return <image clipPath="url(#myClip)" xlinkHref={imageKey} x="0" y="0" height="343px" width="500px" transform="scale(2 2.05) translate(-165 -63)" />
    }
}

class CardDataBox extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(card.isSpell()) {
            return <Image baseUrl={baseUrl} image={"spell-box.png"} />
        }
        if(card.isTrap()) {
            return <Image baseUrl={baseUrl} image={"trap-box.png"} />
        }
        if(card.isType(YgoCardConstants.TYPE_PENDULUM))
            return <Image baseUrl={baseUrl} image={"monster-pendulum-data-box.png"} />
        return <Image baseUrl={baseUrl} image={"monster-data-box.png"} />
    }
}

class CardLevelRank extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null
        if(card.isLinkMonster())return null
        if(card.isXyzMonster())
            return <Image baseUrl={baseUrl} image={"rank-"+card.getStars()+".png"} />
        return <Image baseUrl={baseUrl} image={"level-"+card.getStars()+".png"} />
    }
}

class CardAttribute extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null

        let image = ""
        if(card.isDarkAttribute()) image="dark.png"
        if(card.isLightAttribute()) image="light.png"
        if(card.isWindAttribute()) image="wind.png"
        if(card.isWaterAttribute()) image="water.png"
        if(card.isFireAttribute()) image="fire.png"
        if(card.isEarthAttribute()) image="earth.png"
        if(card.isDivineAttribute()) image="divine.png"
        return <Image baseUrl={baseUrl} image={image} />
    }
}

class CardAtk extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null

        const atk = card.getAtk()
        if(!atk)return null
        let xPos = ""
        if(atk.length==1)
            xPos = "83"
        if(atk.length==2)
            xPos = "74"
        if(atk.length==3)
            xPos = "64"
        if(atk.length==4)
            xPos = "55"
        return <text x={xPos} y="463" fontSize="37" fontWeight="bold" fontFamily="Palatino Linotype">{atk}</text>
    }
}

class CardLink extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null
        if(!card.isLinkMonster()) return null
        const value = card.getLink()
        if(!value)return null
        return <text x="199" y="463" fill="black" fontSize="30" fontWeight="bold" fontFamily="Palatino Linotype ">{"LINK-"+card.getLink()}</text>
    }
}

class CardDef extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null
        if(card.isLinkMonster()) return null
        const value = card.getDef()
        if(!value)return null
        let xPos = ""
        if(value.length==1)
            xPos = "242"
        if(value.length==2)
            xPos = "231"
        if(value.length==3)
            xPos = "222"
        if(value.length==4)
            xPos = "213"
        return <text x={xPos} y="463" fontSize="37" fontWeight="bold" fontFamily="Palatino Linotype">{value}</text>
    }
}

class CardScaleLeft extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null
        if(!card.isPendulumMonster())return null
        const value = card.getScale()

        let xPos = "19"
        if(value.length==2)
            xPos = "14"
        return <text x={xPos} y="350" fill="black" fontSize="22" fontWeight="bold" fontFamily="Palatino Linotype">{value}</text>
    }
}

class CardScaleRight extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isMonster())return null
        if(!card.isPendulumMonster())return null
        const value = card.getScale()

        let xPos = "313"
        if(value.length==2)
            xPos = "308"
        return <text x={xPos} y="350" fill="black" fontSize="22" fontWeight="bold" fontFamily="Palatino Linotype">{value}</text>
    }
}

class CardLinkMarker extends React.Component {
    render() {
        const { card, baseUrl } = this.props.params
        if(!card.isLinkMonster())return null
        const { on, dir } = this.props
        const onoff = on?"on":"off"
        return <Image baseUrl={baseUrl} image={"link-"+dir+"-"+onoff+".png"} />
    }
}

class CardSvg extends React.Component {
    render() {
        const { card, ygoEditorModel, baseUrl, dragStartedCallback } = this.props
        const params = {
            card,
            ygoEditorModel,
            baseUrl
        }

        return (
            <Svg card={card} dragStartedCallback={dragStartedCallback} >
                <CardImage params={params} />
                <CardBgColorImg params={params} />
                <CardDataBox params={params} />
                <CardLevelRank params={params} />
                <CardAttribute params={params} />

                <CardAtk params={params} />
                <CardDef params={params} />
                <CardLink params={params} />
                <CardScaleLeft params={params} />
                <CardScaleRight params={params} />

                <CardLinkMarker on={card.hasBottomLinkMarker()} dir="bottom" params={params} />
                <CardLinkMarker on={card.hasTopLinkMarker()} dir="top" params={params} />
                <CardLinkMarker on={card.hasLeftLinkMarker()} dir="left" params={params} />
                <CardLinkMarker on={card.hasRightLinkMarker()} dir="right" params={params} />
                <CardLinkMarker on={card.hasBottomRightLinkMarker()} dir="bottom-right" params={params} />
                <CardLinkMarker on={card.hasBottomLeftLinkMarker()} dir="bottom-left" params={params} />
                <CardLinkMarker on={card.hasTopLeftLinkMarker()} dir="top-left" params={params} />
                <CardLinkMarker on={card.hasTopRightLinkMarker()} dir="top-right" params={params} />
            </Svg>
        )
    }
}

export default class YgoCardView extends React.Component {
    render() {
        const { card, ygoEditorModel, dragStartedCallback,  } = this.props
        const baseUrl = "https://raw.githubusercontent.com/fredriquesamuels/dashboard-ui/gh-pages/yugioh-res/cardbuilder/layers-lr/"
        if(card) {
            return (
                <div class="fill-parent" onClick={this.clicked.bind(this)}>
                    <CardSvg card={card} baseUrl={baseUrl} ygoEditorModel={ygoEditorModel} dragStartedCallback={dragStartedCallback} />
                </div>
            )
        }

        return (
            <div draggable="true" onDragStart={this.dragStarted.bind(this)} class="fill-parent" onClick={this.clicked.bind(this)}>
                <CardBackSvg  baseUrl={baseUrl} />
            </div>
        )
    }
    clicked(event) {
      const { card, ygoEditorModel, onClickCallback } = this.props
      if(ygoEditorModel) {
          ygoEditorModel.setPreviewCard(card)
      }
      if(onClickCallback) {
          onClickCallback(event)
      }
    }
    dragStarted(event){
        const { card } = this.props
        event.dataTransfer.setData(card, event.target.id);
    }

}
