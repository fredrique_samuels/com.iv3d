import React from 'react'

import * as DeckEditConstants from '../dnd/DndConstants'
import CardDragData from '../dnd/CardDragData'
import YgoCardView from '../YgoCardView'

export default class SideDeckCard  extends React.Component {
    render() {
        const { card, ygoEditorModel}  = this.props
        return <YgoCardView card={card} ygoEditorModel={ygoEditorModel} dragStartedCallback={this.cardDragStarted.bind(this)}/>
    }
    cardDragStarted(card, event) {
        const { ygoEditorModel } = this.props
        var data = new CardDragData(DeckEditConstants.YGO_CARD_DRAG_SOURCE_SIDE_DECK, card, ygoEditorModel).toJson();
        event.dataTransfer.setData(DeckEditConstants.YGO_CARD_DRAG_DATA,data)
    }
}
