import React from 'react'

import CardDragData from '../dnd/CardDragData'
import MainDeckCard from './MainDeckCard'
import SideDeckCard from './SideDeckCard'
import StyleCache from '../styles/StyleCache'

export default class DeckListView extends React.Component {
    render() {
        const styleCache = new StyleCache();
        const { ygoEditorModel } = this.props.params

        const mainDeckCards = ygoEditorModel.getMainDeckCards()
        const mainDeckCardsComp = mainDeckCards.map( c => {
            return (
                <div class="inline-element" style={styleCache.get('card-space')}>
                    <MainDeckCard card={c} ygoEditorModel={ygoEditorModel} />
                </div>)
        })

        const extraDeckCards = ygoEditorModel.getExtraDeckCards()
        const extraDeckCardsComp = extraDeckCards.map( c => {
            return (
                <div class="inline-element" style={styleCache.get('card-space')}>
                    <MainDeckCard card={c} ygoEditorModel={ygoEditorModel}/>
                </div>)
            } )

        const sideDeckCards = ygoEditorModel.getSideDeckCards()
        const sideDeckCardsComp = sideDeckCards.map( c => {
            return (
                <div class="inline-element" style={styleCache.get('card-space')}>
                    <SideDeckCard card={c} ygoEditorModel={ygoEditorModel}/>
                </div>)
            })

        return (
            <div class="inline-element" style={styleCache.get("deck-view-container", (ygoEditorModel.inAdvancedSearchMode()?{display:"none"}:{}))} >
                <div class="back-panel" style={styleCache.get("card-count-label")} >{ygoEditorModel.getDeckName()}</div>
                <div style={styleCache.get("main-deck-container") } onDrop={this.mainDeckDropped.bind(this)} onDragOver={this.allowMainDeckDrop.bind(this)} >
                    <div class="back-panel" style={styleCache.get("card-count-label")} >{ygoEditorModel.getI8nMessage("ygo.edit.deckview.maindeck")+" : " + mainDeckCards.length}</div>
                    <div style={styleCache.get("card-list-container")} >
                        {mainDeckCardsComp}
                    </div>
                </div>

                <div style={styleCache.get("extra-deck-container")} onDrop={this.mainDeckDropped.bind(this)} onDragOver={this.allowMainDeckDrop.bind(this)} >
                    <div class="back-panel" style={styleCache.get("card-count-label")} >{"Extra Deck : " + extraDeckCards.length}</div>
                    <div style={styleCache.get("card-list-container")} >
                        {extraDeckCardsComp}
                    </div>
                </div>

                <div style={styleCache.get("side-deck-container")} onDrop={this.sideDeckDropped.bind(this)} onDragOver={this.allowSideDeckDrop.bind(this)} >
                    <div class="back-panel" style={styleCache.get("card-count-label")} >{"Side Deck : " + sideDeckCards.length}</div>
                    <div style={styleCache.get("card-list-container")} >
                        {sideDeckCardsComp}
                    </div>
                </div>
            </div>
        )
    }
    allowMainDeckDrop(event) {
        const { ygoEditorModel } = this.props.params
        const transferData = new CardDragData().fromEvent(event, ygoEditorModel)
        if(transferData) {
            if(transferData.canAddToMainDeck()) {
                event.preventDefault();
            }
        }
    }
    mainDeckDropped(event) {
        const { ygoEditorModel } = this.props.params
        const transferData = new CardDragData().fromEvent(event, ygoEditorModel)
        if(transferData) {
            event.preventDefault();
            transferData.addToMainDeck()
        }
    }
    allowSideDeckDrop(event) {
        const { ygoEditorModel } = this.props.params
        const transferData = new CardDragData().fromEvent(event, ygoEditorModel)
        if(transferData) {
            if(transferData.canAddToSideDeck()) {
                event.preventDefault();
            }
        }
    }
    sideDeckDropped(event) {
        const { ygoEditorModel } = this.props.params
        const transferData = new CardDragData().fromEvent(event, ygoEditorModel)
        if(transferData) {
            event.preventDefault();
            transferData.addToSideDeck()
        }
    }
}