import React from 'react'

import StyleCache from '../styles/StyleCache'
import DeckSelectProfile from './DeckSelectProfile'


export default class DeckLastEditedPanel extends React.Component {
    render() {
        const styleCache = new StyleCache();
        const { uicontext, ygoEditorModel } = this.props.params

        const searchResults = ygoEditorModel.getDeckNamesOrderedByLastUpdated()
        const searchResultsComp = searchResults.map( deckName => <DeckSelectProfile deck={ygoEditorModel.getDeckByName(deckName)} ygoEditorModel={ygoEditorModel} /> )

        return (
            <div class="inline-element" style={styleCache.get("card-search-container")}>
                <div style={styleCache.get('recent-deck-edited-panel')}>
                    <div class="back-panel" style={styleCache.get("card-count-label")} >Recently Updated</div>
                    <div style={styleCache.get("card-list-container")} >
                        {searchResultsComp}
                    </div>
                </div>
            </div>)
    }
}