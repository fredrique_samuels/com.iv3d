import React from 'react'

export default class DeckSelectProfile extends React.Component {
    render() {
        const {deck, ygoEditorModel} = this.props
        const active = ygoEditorModel.getDeckName()==deck.getName()
        const style = {
            width:"100%",
            height:"80px",
            marginTop:"2px",
            padding:"3px",
            overflow:"hidden",
            boxSizing: "border-box",
            border:active?"3px solid #6e2caf":"none",
            color:"slategrey"
        }
        return <div class="back-panel" style={style} onClick={this.clicked.bind(this)} >
            <div>{deck.getName()}</div>
            <div>{deck.getDateLastUpdated()}</div>
        </div>
    }
    clicked() {
        const {deck, ygoEditorModel} = this.props
        ygoEditorModel.setDeckActive(deck.getName())
    }
}