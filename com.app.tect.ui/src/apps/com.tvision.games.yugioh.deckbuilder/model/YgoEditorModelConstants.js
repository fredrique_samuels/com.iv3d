

export const DECK_EDIT_MODE = 'ygoeditor.mode.deckedit'
export const DECK_SAMPLE_HANDS = 'ygoeditor.mode.samplehands'
export const DECK_DECK_SELECT = 'ygoeditor.mode.deckselect'
export const ADVANCED_SEARCH_MODE = 'ygoeditor.mode.advancedsearch'
export const TASKS_MODE = 'ygoeditor.mode.tasks'

export const MIN_ATK_FILTER = "ygoeditor.min.atk.filter"
export const MAX_ATK_FILTER = "ygoeditor.max.atk.filter"
export const MIN_DEF_FILTER = "ygoeditor.min.def.filter"
export const MAX_DEF_FILTER = "ygoeditor.max.def.filter"
export const MIN_STARS_FILTER = "ygoeditor.min.stars.filter"
export const MAX_STARS_FILTER = "ygoeditor.max.stars.filter"
export const MIN_SCALE_FILTER = "ygoeditor.min.scale.filter"
export const MAX_SCALE_FILTER = "ygoeditor.max.scale.filter"
export const MIN_LINK_FILTER = "ygoeditor.min.link.filter"
export const MAX_LINK_FILTER = "ygoeditor.max.link.filter"