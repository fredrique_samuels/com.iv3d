import {YgoDeckTransformer} from "../domain/YgoDeck";
import YgoDeck from "../domain/YgoDeck";

const DECKLIST_COOKIE = "com.tvision.games.yugioh.deckbuilder.decklist";

class CookieDeckDao {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }

    saveDeck(accountId, deck) {
        let decks = this.readDeckList();
        decks = this.createAccountListIfNull(decks, accountId);
        this.updateAccountDeck(decks, accountId, deck);
        this.saveDeckList(decks);
    }

    loadDecksForAccount(accountId, transformer) {
        const deckList = this.readDeckList()
        const accountRawDecks = deckList[String(accountId)]
        const decks = []
        if(accountRawDecks) {
            for (var deckName in accountRawDecks) {
                if (accountRawDecks.hasOwnProperty(deckName)) {
                    var deckData = accountRawDecks[deckName];
                    var deck = new YgoDeck();
                    deck.updateFromData(deckData)
                    const deckParams = deck.getParams(transformer)
                    decks.push(deckParams)
                }
            }
        }
        return decks
    }

    readDeckList() {
        const uicontext = this.ygoEditorModel.getUiContext();
        var cookie = uicontext.cookieManager().getCookie(DECKLIST_COOKIE);
        return this.parseCookie(cookie);
    }

    saveDeckList(decks) {
        const uicontext = this.ygoEditorModel.getUiContext();
        uicontext.cookieManager().setCookie(DECKLIST_COOKIE, JSON.stringify(decks))
    }

    parseCookie(cookie) {
        try {
            return cookie ? JSON.parse(cookie) : {}
        } catch (e) {
            return {}
        }
        return {};
    }

    updateAccountDeck(decks, accountId, deck) {
        decks[String(accountId)][deck.getName()] = deck.getForPersistence();
    }

    createAccountListIfNull(decks, accountId) {
        if (!decks[String(accountId)]) {
            decks[String(accountId)] = {}
        }
        return decks
    }
}

export default class DeckDao {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }

    cookieDao(){return new CookieDeckDao(this.ygoEditorModel)}

    save(accountId, deckParam){
        var deck = new YgoDeck();
        deck.update(deckParam);
        this.cookieDao().saveDeck(accountId,  deck)
    }

    getDecksForAccount(accountId, transformer) {
        return this.cookieDao().loadDecksForAccount(accountId, transformer);
    }
}