import YgoDeckParam from "../YgoDeckParam";
import Immutable from 'immutable'

export default class YgoDeck {
    constructor() {
        this.values = this.defaultData();
    }
    defaultData() {
        return {
            name: "",
            mainDeck: [],
            sideDeck: [],
            dlu: new Date().toUTCString()
        }
    }
    getForPersistence() {
        return this.values
    }
    getName() {
        return this.values.name
    }
    updateFromData(data) {
        this.values = Object.assign({},this.defaultData(),data)
    }
    update(deckParam) {
        const dlu = deckParam.getDateLastUpdatedUTC()
        const name = deckParam.getName()
        const mainDeck = deckParam.getMainDeckCards().concat(deckParam.getExtraDeckCards()).map( card => card.getId())
        const sideDeck = deckParam.getSideDeckCards().map( card => card.getId())

        this.values = Object.assign({},this.values,{
            name:name,
            dlu:dlu,
            mainDeck:mainDeck,
            sideDeck:sideDeck
        })
    }
    getParams(cardIdToCardTransformer) {
        const paramData = {
            name:this.values.name,
            mainDeck : Immutable.List(this.values.mainDeck.map( cardId => cardIdToCardTransformer.transform(cardId))),
            sideDeck : Immutable.List(this.values.sideDeck.map( cardId => cardIdToCardTransformer.transform(cardId))),
            saved:true,
            dlu:new Date(Date.parse(this.values.dlu))
        }
        return new YgoDeckParam(paramData)
    }
}

export class YgoDeckTransformer {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }

    fromParam(deckParam) {
        const dlu = deckParam.getDateLastUpdatedUTC()
        const name = deckParam.getName()
        const mainDeck = deckParam.getMainDeckCards().concat(deckParam.getExtraDeckCards()).map( card => card.getId())
        const sideDeck = deckParam.getSideDeckCards().map( card => card.getId())

        return new YgoDeck({
            name:name,
            dlu:dlu,
            mainDeck:mainDeck,
            sideDeck:sideDeck
        })
    }

    toParam(deck) {

        // var utcSeconds = 1234567890;
        // var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        // d.setUTCSeconds(utcSeconds);
    }
}