import Immutable from 'immutable'

export default class YgoDeckParam {
    constructor(v={}) {
        this.value = Object.assign({}, {
                name:"",
                mainDeck : new Immutable.List(),
                sideDeck : new Immutable.List(),
                saved:false,
                dlu:new Date()
            },v)
    }

    duplicate(v) {
        return new YgoDeckParam({
            name : v.name?v.name:this.value.name,
            mainDeck : new Immutable.List(this.value.mainDeck.toArray()),
            sideDeck : new Immutable.List(this.value.sideDeck.toArray()),
            saved:v.saved?v.saved:false
        })
    }
    getName(){return this.value.name}
    isSaved(){return this.value.saved}
    isEmpty(){return this.value.mainDeck.size==0 && this.value.sideDeck.size==0}
    getDateLastUpdatedUTC(){return this.value.dlu.toUTCString()}
    getDateLastUpdated(){return this.value.dlu.toLocaleString()}

    getMainDeckCards(){return this.value.mainDeck.filter(c => !c.isExtraDeckCard()).toArray()}
    getExtraDeckCards(){return this.value.mainDeck.filter(c => c.isExtraDeckCard()).toArray()}
    getSideDeckCards(){return this.value.sideDeck.toArray()}

    addToMainDeck(card) {return this.immutable({mainDeck: this.value.mainDeck.push(card),dlu:new Date()})}
    addToSideDeck(card) {return this.immutable({sideDeck: this.value.sideDeck.push(card),dlu:new Date()})}
    clear(){return this.immutable({sideDeck: this.value.sideDeck.clear(), mainDeck: this.value.mainDeck.clear(),dlu:new Date()})}
    moveToSideDeck(card) {
        return this.deleteFromMainDeck(card).addToSideDeck(card)
    }
    moveToMainDeck(card) {
        return this.deleteFromSideDeck(card).addToMainDeck(card)
    }
    deleteFromMainDeck(card){
        const entry = this.value.mainDeck.findEntry( c=> c.getId(c)==card.getId())
        const entryIndex = entry[0]
        const newDeck = this.value.mainDeck.delete(entryIndex)
        return this.immutable({mainDeck:newDeck,dlu:new Date()})
    }
    deleteFromSideDeck(card){
        const entry = this.value.sideDeck.findEntry( c=> c.getId(c)==card.getId())
        const entryIndex = entry[0]
        const newDeck = this.value.sideDeck.delete(entryIndex)
        return this.immutable({sideDeck:newDeck,dlu:new Date()})
    }
    getCopiesCount(card) {
        const mdc = this.value.mainDeck.filter((value, key) => value.getId()==card.getId()).count()
        const sdc = this.value.sideDeck.filter((value, key) => value.getId()==card.getId()).count()
        return mdc + sdc
    }
    hasMaxMainDeckCards(card) {
        const mainDeckSize = this.getMainDeckCards().length
        const extraDeckSize = this.getExtraDeckCards().length
        if(card.isExtraDeckCard() && extraDeckSize>=15) {
            return true;
        }

        if(!card.isExtraDeckCard() && mainDeckSize>=60) {
            return true;
        }
        return false
    }
    hasMaxSideDeckCards() {
        const sideDeckSize = this.getSideDeckCards().length
        if(sideDeckSize>=15) return true;
        return false
    }
    immutable(v={}) {
        return new YgoDeckParam(Object.assign({}, this.value, Object.assign({},v, {saved:false})))
    }
    compareDlu(other) {
        return other.value.dlu - this.value.dlu
    }

}