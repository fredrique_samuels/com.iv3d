import StateHandler from "./StateHandler";

export default class SampleOpeningHandsHandler extends StateHandler {
    constructor(ygoEditorModel) {
        super(ygoEditorModel)
    }

    getSampleOpeningHand() {
        if(this.getState().sampleOpeningHand.length==0)
            return this.createNewSampleHand()
        return this.getState().sampleOpeningHand
    }

    refreshSampleOpeningHand() {
        this.updateState({sampleOpeningHand: this.createNewSampleHand()})
    }

    createNewSampleHand() {
        var sampleOpeningHand = []
        const cards = this.getModel().getMainDeckCards()
        if (cards.length > 6) {
            this.shuffle(cards)
            sampleOpeningHand = cards.slice(0, 6);
        }
        return sampleOpeningHand;
    }

    shuffle(a) {
        for (let i = a.length; i; i--) {
            let j = Math.floor(Math.random() * i);
            [a[i - 1], a[j]] = [a[j], a[i - 1]];
        }
    }


    getDefaults() {return {sampleOpeningHand:[]}}
}