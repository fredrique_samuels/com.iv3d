import StateHandler from "./StateHandler";
import YgoDeckParam from "../YgoDeckParam";

export default class DeckEditHandler extends StateHandler {
    constructor(model){super(model)}

    getDefaults() {return {editDeck:new YgoDeckParam()}}

    getMainDeckCards(){return this.sortCards(this.getState().editDeck.getMainDeckCards())}
    getExtraDeckCards(){return this.sortCards(this.getState().editDeck.getExtraDeckCards())}
    getSideDeckCards(){return this.sortCards(this.getState().editDeck.getSideDeckCards())}
    getDeckName(){return this.getState().editDeck.getName()}
    isDeckEmpty(){return this.getState().editDeck.isEmpty()}
    isDeckSaved(){return this.getState().editDeck.isSaved()}
    clearDeck(){
        const newDeck = this.getState().editDeck.clear()
        this.updateState({editDeck:newDeck})
    }
    addToMainDeck(cardId) {
        if(this.canAddToMainDeck(cardId)) {
            const card = this.getCardById(cardId)
            const newDeck = this.getState().editDeck.addToMainDeck(card)
            this.updateState({editDeck:newDeck})
        }
    }
    canAddToMainDeck(cardId){
        if(this.hasMaxDuplicates(cardId)) return false
        if(this.hasMaxMainDeckCards(cardId))return false
        return true
    }
    addToSideDeck(cardId) {
        if(this.canAddToSideDeck(cardId)) {
            const card = this.getCardById(cardId)
            const newDeck = this.getState().editDeck.addToSideDeck(card)
            this.updateState({editDeck:newDeck})
        }
    }
    canAddToSideDeck(cardId){
        if(this.hasMaxDuplicates(cardId))return false
        if(this.hasMaxSideDeckCards()) return false
        return true
    }
    moveToSideDeck(cardId) {
        if(this.canMoveToSideDeck(cardId)) {
            const card = this.getCardById(cardId)
            const newDeck = this.getState().editDeck.moveToSideDeck(card)
            this.updateState({editDeck:newDeck})
        }
    }
    canMoveToSideDeck(cardId) {
        if(this.hasMaxSideDeckCards()) return false
        return true
    }
    moveToMainDeck(cardId) {
        if(this.canMoveToMainDeck(cardId)) {
            const card = this.getCardById(cardId)
            const newDeck = this.getState().editDeck.moveToMainDeck(card)
            this.updateState({editDeck:newDeck})
        }
    }
    deleteFromMainDeck(cardId) {
        const card = this.getCardById(cardId)
        const newDeck = this.getState().editDeck.deleteFromMainDeck(card)
        this.updateState({editDeck:newDeck})
    }
    deleteFromSideDeck(cardId) {
        const card = this.getCardById(cardId)
        const newDeck = this.getState().editDeck.deleteFromSideDeck(card)
        this.updateState({editDeck:newDeck})
    }
    canMoveToMainDeck(cardId) {return !this.hasMaxMainDeckCards(cardId)}
    totalCardDuplicateCount(cardId) {return this.getState().editDeck.getCopiesCount(this.getCardById(cardId))}
    hasMaxMainDeckCards(cardId) {return this.getState().editDeck.hasMaxMainDeckCards(this.getCardById(cardId))}
    hasMaxSideDeckCards() {return this.getState().editDeck.hasMaxSideDeckCards()}
    hasMaxDuplicates(cardId) {return this.totalCardDuplicateCount(cardId)>=3}
}