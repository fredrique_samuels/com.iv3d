import StateHandler from "./StateHandler";

export class JobsHandler extends StateHandler {
    constructor(model) {
        super(model)
    }

    setJobsData(td) {
        this.updateState({jobsData:td})
    }

    getJobs() {
        if(this.getState().jobsData) {
            return this.getState().jobsData.jobs
        }
        return []
    }

}