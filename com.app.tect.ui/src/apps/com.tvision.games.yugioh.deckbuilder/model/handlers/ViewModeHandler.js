import StateHandler from "./StateHandler";
import * as YgoEditorModelConstants from '../YgoEditorModelConstants'

export default class ViewModeHandler extends StateHandler {
    constructor(model) {
        super(model)
    }

    inDeckEditMode(){return YgoEditorModelConstants.DECK_EDIT_MODE==this.getState().viewmode || YgoEditorModelConstants.ADVANCED_SEARCH_MODE==this.getState().viewmode }
    inDeckSelectMode(){return YgoEditorModelConstants.DECK_DECK_SELECT==this.getState().viewmode}
    inSampleHandsMode(){return YgoEditorModelConstants.DECK_SAMPLE_HANDS==this.getState().viewmode}
    inAdvancedSearchMode(){return YgoEditorModelConstants.ADVANCED_SEARCH_MODE==this.getState().viewmode}
    inTasksMode(){return YgoEditorModelConstants.TASKS_MODE==this.getState().viewmode}

    setDeckEditMode(){this.updateState({viewmode:YgoEditorModelConstants.DECK_EDIT_MODE})}
    setDeckSelectMode(){this.updateState({viewmode:YgoEditorModelConstants.DECK_DECK_SELECT})}
    setSampleHandsMode(){this.updateState({viewmode:YgoEditorModelConstants.DECK_SAMPLE_HANDS})}
    setAdvancedSearchMode(){this.updateState({viewmode:YgoEditorModelConstants.ADVANCED_SEARCH_MODE})}
    setTasksMode(){this.updateState({viewmode:YgoEditorModelConstants.TASKS_MODE})}

    getDefaults() {return {viewmode:YgoEditorModelConstants.DECK_EDIT_MODE}}
}