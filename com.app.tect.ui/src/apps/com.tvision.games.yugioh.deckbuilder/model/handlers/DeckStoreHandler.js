import Immutable from 'immutable'
import StateHandler from "./StateHandler";


class CardIdToCardTransformer {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    transform(cardId) {
        return this.ygoEditorModel.getCardById(cardId)
    }
}

export default class DeckStoreHandler extends StateHandler {
    constructor(model){super(model)}

    getDeckNames(){return this.getState()
        .decks
        .toList()
        .toArray()
        .filter(d => this.getState().deckSearchTerm?d.getName().toLowerCase().includes(this.getState().deckSearchTerm.toLowerCase()):true)
        .map(d=>d.getName())
        .sort((a,b)=> a.localeCompare(b))
    }
    getDeckByName(name){return this.getState().decks.toList().toArray().filter(d=>d.getName()==name)[0]}
    getDeckNamesOrderedByLastUpdated(){return this.getState()
        .decks
        .toList()
        .toArray()
        .sort((a,b)=>a.compareDlu(b))
        .map(d=>d.getName())
        .filter((d,index) => index<10 )
    }
    setDeckActive(deckName){this.updateState({editDeck:this.getDeckByName(deckName)})}
    saveDeck(deckName){
        const state = this.getState();
        const editDeck = state.editDeck
        const newDeck = editDeck.duplicate({name:deckName, saved:true})

        const uicontext = this.getModel().getUiContext();
        const accountId = uicontext.accountManager().getAccountId()
        this.getModel().deckDao().save(accountId, newDeck)

        this.updateState({decks:state.decks.set(deckName, newDeck), editDeck:newDeck})
    }
    deleteDeck(deckName){
        var state = this.getState();
        const entry = state.decks.findEntry( d=> d.getName()==deckName)
        if(entry) {
            const entryIndex = entry[0]
            const newList = state.decks.delete(entryIndex)
            this.updateState({decks:newList})
            this.getUiContext().deckEditHandler().clearDeck()
        }
    }
    updateDeckSearchTerm(t){this.updateState({deckSearchTerm:t})}
    getDefaults() {
        return {
            decks:new Immutable.Map(),
            deckSearchTerm:""
        }
    }
    loadStartupData() {
        const transformer = new CardIdToCardTransformer(this.getModel())
        const uicontext = this.getUiContext();
        const accountId = uicontext.accountManager().getAccountId()
        const deckParams = this.getModel().deckDao().getDecksForAccount(accountId, transformer)

        let deckMap = new Immutable.Map()
        for(var dpIndex in deckParams) {
            var dp = deckParams[dpIndex];
            deckMap = deckMap.set(dp.getName(), dp)
        }
        this.updateState({decks:deckMap})
    }
}

