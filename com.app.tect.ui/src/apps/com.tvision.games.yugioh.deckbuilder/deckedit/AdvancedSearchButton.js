import React from 'react'


export default class AdvancedSearchButton extends React.Component {
    render() {
        const { ygoEditorModel } = this.props.params

        if(ygoEditorModel.inAdvancedSearchMode()) {
            return (
                <button class="btn btn-xs color-error" onClick={this.hideAdvancedSearch.bind(this)}
                        style={{width: "30px", height: "30px"}}>
                    <i class="fa fa-forward"></i>
                </button>
            )
        }

        return (
            <button class="btn btn-xs color-primary" onClick={this.showAdvancedSearch.bind(this)}
                    style={{width: "30px", height: "30px"}}><i class="fa fa-backward"></i>
            </button>
        )
    }
    hideAdvancedSearch() {
        const { ygoEditorModel } = this.props.params
        ygoEditorModel.setDeckEditMode()
    }
    showAdvancedSearch() {
        const { ygoEditorModel } = this.props.params
        ygoEditorModel.setAdvancedSearchMode()
    }
}

