import React from 'react'

import AbstractViewContainer from '../AbstractViewContainer'
import AdvancedCardFilterView from '../cardfilter/AdvancedCardFilterView'
import DeckListView from '../decklist/DeckListView'
import StyleCache from '../styles/StyleCache'
import YgoCardView from '../YgoCardView'

import CardFilterPanel from './CardFilterPanel'

export default class DeckEditView extends React.Component {
  render() {
    const styleCache = new StyleCache();
    const { ygoEditorModel } = this.props.params

    return (
      <AbstractViewContainer hidden={!ygoEditorModel.inDeckEditMode()} >
        <div class="inline-element"  style={styleCache.get('card-details-container')} >
          <div class="inline-element" style={styleCache.get('card-info-space')}><YgoCardView card={ygoEditorModel.getPreviewCard()} /></div>
          <div style={{fontWeight:"bold"}}>{ygoEditorModel.getPreviewCardName()}</div>
          <div class="inline-element" style={styleCache.get('card-lore-block')}>{ygoEditorModel.getPreviewCardLore()}</div>
        </div>
        <DeckListView params={this.props.params} />
        <AdvancedCardFilterView params={this.props.params} />
        <CardFilterPanel params={this.props.params} />
      </AbstractViewContainer>
    )
  }
}
