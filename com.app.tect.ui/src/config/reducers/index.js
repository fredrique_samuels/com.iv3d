import { combineReducers } from 'redux'

import EnvironmentReducer from './EnvironmentReducer'
import ComponentModelsReducer from './../../tectui/framework/ComponentModels'

export default combineReducers({
  //domain
  environment: EnvironmentReducer,
  componentModels : ComponentModelsReducer,
})
