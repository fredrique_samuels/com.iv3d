import { applyMiddleware,  createStore } from 'redux'

import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import {createLogger} from 'redux-logger'

import reducers from './reducers'



const error = (store)=>(next)=>(action)=> {
    try {
        next(action)
    } catch(e) {
        console.log("Error on action ", e, action.type)
    }
}

const middelware = applyMiddleware(promise(),
    // thunk,
    logger,
    error);

export default createStore(reducers, {}, middelware)
