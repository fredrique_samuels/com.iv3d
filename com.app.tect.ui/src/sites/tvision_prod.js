'use strict'

import { Provider } from 'react-redux'

import React from 'react'
import ReactDom from 'react-dom'
import store from './../config/store'

import {TVisionView} from '../tvision/TVisionView'

const app = document.getElementById('app')
console.error(app)
ReactDom.render(
    <Provider store={store}>
        <TVisionView environmentMode={"PROD"} />
    </Provider>,
    app)
