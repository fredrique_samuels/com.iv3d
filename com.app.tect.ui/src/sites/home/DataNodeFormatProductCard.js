import React from 'react'
import ProductCard from './ProductCard'

export default class DataNodeFormatProductCard extends React.Component {
  render() {
    const productContent = (
      <div class="fill-parent">
        <div style={{position:"relative", width:"100%", height:"100px", backgroundSize: "100% 100%", backgroundImage: "url('/res/images/datanode-500x100.svg')", backgroundColor:"white"}}>
        </div>
        <div style={{backgroundSize: "100% 100%",backgroundImage:"url('/res/images/datanode-watermark-500x200.svg')",padding:"0px",textAlign:"left",position:"relative", width:"100%", height:"calc(100% - 100px)", fontFamily:"Droid San", overflowY:"hidden"}}>
          <div style={{position:"relative", margin:"5px"}}>
            <p>
                View your data like you have never done before. Our Node Graphs allows you to organize, share and view information like never before.
            </p>
          </div>
          <button  style={{position:"absolute", bottom:"10px", right:"10px"}} onClick={this.redirect.bind(this)} class="btn btn-primary btn-sm">try it<i class="fa  fa-angle-double-right"></i></button>
        </div>
      </div>
    )

    return <ProductCard contentComp={productContent}/>
  }

  redirect() {
    // window.location="/app_datanode_edit_demo.html";
  }
}
