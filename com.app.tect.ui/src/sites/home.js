import React from 'react'
import { connect } from 'react-redux'

import DataNodeFormatProductCard from './home/DataNodeFormatProductCard'

import {setDispatcher} from "../tectui/tectui";

const DATANODE_EDIT_DEMO_MODELID = "datanode.edit.demo.modelid"

@connect((store) => {
  return {
    environment : store.environment,
  }
})
export default class SiteHome extends React.Component {

  constructor() {
    super()
  }

  componentWillMount() {
    setDispatcher(this.props.dispatch)
  }

  render() {
    const contentArea = {
      position:"relative",
      width:"100%",
      height:"100%",
    }

    return ( <div class="fill-parent" style={contentArea}>
        {this.renderNavBar()}
        {this.renderProductView()}
    </div> )
  }

  renderNavBar() {
    const navBarStyle = {
      width:"100%",
      height:"120px",
      position:"relative",
      backgroundColor:"rgba(211,211,211, 1)"
    }
    return (<div class="fill-parent" style={navBarStyle}>
      <svg width="300px" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
          <a>
            <image x="0" y="0" height="100%" width="100%" xlinkHref="/res/images/tt_logo_entry_animated.svg" />
          </a>
      </svg>
      <button class="site-home-menu-button"><i class="fa fa-question fa-2x"></i></button>
      <button class="site-home-menu-button"><i class="fa fa-home fa-2x"></i></button>
    </div>)
  }

  renderProductView() {
    const productStyle = {
      overflow:"auto",
      backgroundSize: "100% 100%",
      backgroundImage: "url('/res/images/003.jpeg')",
      width:"100%",
      height:"calc(100% - 120px)",
      position:"relative"
    }
    return (<div class="fill-parent" style={productStyle}>
        <DataNodeFormatProductCard />
    </div>)
  }

}
