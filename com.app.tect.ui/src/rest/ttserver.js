
export const HOST = "http://localhost:58080"


export class TTServerGateway {

    constructor() {
    }

    admin_GetAppByPackageId(cbo, packageId) {
        const url = this.__createLink("/server.admin/app?packageId=" + packageId);
        this.__ajax(cbo, "GET", url);
    }

    admin_GetJobStatus(cbo, jobId) {
        const url = this.__createLink("/server.admin/jobs/status?jobId=" + jobId);
        this.__ajax(cbo, "GET", url);
    }

    admin_GetJobRunStatus(cbo, jobId, runId) {
        const url = this.__createLink("/server.admin/jobs/status?jobId=" + jobId + "&runId=" + runId);
        this.__ajax(cbo, "GET", url);
    }

    admin_ScheduleJobRunAppPackage(cbo, appPackageId, jobId) {
        const url = this.__createLink("/server.admin/jobs/run?appPackageId=" + appPackageId + "&jobBeanId=" + jobId);
        this.__ajax(cbo, "GET", url);
    }

    admin_ScheduleJobRunForAppId(cbo, appId, jobId) {
        const url = this.__createLink("/server.admin/jobs/run?appId=" + appId + "&jobBeanId=" + jobId);
        this.__ajax(cbo, "GET", url);
    }

    admin_GetJobHistory(cbo, jobId, maxRunId) {
        const path = "/server.admin/jobs/run/history?jobId=" + jobId + this.__optionalValue("runId", maxRunId)
        const url = this.__createLink(path);
        this.__ajax(cbo, "GET", url);
    }

    __optionalValue(name, maxRunId) {
        return ( maxRunId ? ("&"+name+"=" + maxRunId) : "" );
    }


    __createLink(path){
        return HOST  + path
    }

    __ajax(cbo, method, url, timeout=5000) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (cbo.success) cbo.success(JSON.parse(this.responseText).body)
            } else if (this.readyState == 4 && this.status != 200) {
                if (cbo.error) cbo.error(this, this.error)
            }
        };
        xhttp.timeout = timeout;
        xhttp.ontimeout = function () { if (cbo.error) cbo.error(this, "Request Timeout") }
        xhttp.open(method, url, true);
        xhttp.send();
    }
}