
import AbstractParamsBuilder from './builder/AbstractParamsBuilder'

/**
 * Abstract description of an interface to a platform on which an app runs.
 */
export class AbstractPlatform {
    sendNotification(notification){}
}

export class ParamsBuilder extends AbstractParamsBuilder {
    constructor(params={}, buildCallback=null) {
        super(params, buildCallback)
    }
}

export class NotificationBuilder extends ParamsBuilder {

    constructor() {
        super({
            type:'',
            message:'',
            data:null,
            applicationPackageId:null,
            date:new Date()
        })
    }

    build(){}
}