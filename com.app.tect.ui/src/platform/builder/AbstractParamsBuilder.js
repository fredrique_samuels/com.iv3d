export default class AbstractParamsBuilder {

    constructor(params={}, buildCallback=null) {
        this.buildCallback = buildCallback
        this.params = params
    }
    setParams(params){this.params = params;return this}
    commit() {
        if(this.buildCallback)
            return this.buildCallback(this.build())
        return this.build()
    }
    build(){return this.params}
}