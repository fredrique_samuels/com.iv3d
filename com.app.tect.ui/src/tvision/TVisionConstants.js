export const TVISION_MANAGER = 'tvision_manager_instance'

export const DISPLAY_MODE_APP='displaymode.app'
export const DISPLAY_MODE_HOME='displaymode.home'
export const DISPLAY_MODE_MENU='displaymode.menu'
export const DISPLAY_MODE_APP_LIST='displaymode.applist'

export const USER_SETTINGS_DISPLAY_NAME = "user.settings.displayname"
export const USER_SETTINGS_FIRST_NAME = "user.settings.firstname"
export const USER_SETTINGS_LAST_NAME = "user.settings.lastname"

export const USER_SETTINGS_CONTACT_EMAIL = "user.settings.contact.email"

export const ACCOUNT_SETTINGS_NAME = "account.settings.name"
export const ACCOUNT_SETTINGS_ACTIVATED = "account.settings.activated"