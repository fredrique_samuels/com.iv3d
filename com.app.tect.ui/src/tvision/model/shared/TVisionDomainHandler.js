import {AbstractHandler} from "./AbstractHandler";


export class TVisionDomainHandler extends AbstractHandler {

    constructor(model, defaultValue) {
        super(model, defaultValue);
    }
    getServer(){return this.model().getServer(this)}
    i8nMessage(m){return this.model().i8nMessage(m)}
    setExpireSession(){this.model().userAccountHandler().setSessionModeExpired()}
    getSessionValues(uiContext, session) {
        return this.getDefaults(uiContext)
    }

    saveSessionValues(uiContext, session) {

    }

    loadServerValues(doneHandler, uiContext, session) {
        if(doneHandler) doneHandler(this.getSessionValues(uiContext, session))
    }

    getCurrentDataVersion() {
        return {}
    }
}