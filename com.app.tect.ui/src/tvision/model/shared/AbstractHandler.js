export class AbstractHandler {

    constructor(model, defaultValue={}) {
        this.defaultValue = defaultValue
        this.__model = model
    }
    state(){return this.__model.state()}
    model(){return this.__model}
    updateState(s){return this.__model.updateState(s)}
    i8nMessage(m) {return this.__model.i8nMessage(m)}
    uicontext() {return this.__model.uicontext()}
    getDefaults(uicontext){return Object.assign({}, this.defaultValue)}
    onSessionOpened(){}
    onSessionClosing(){}

}