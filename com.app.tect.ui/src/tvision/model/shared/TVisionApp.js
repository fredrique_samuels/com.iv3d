import React from 'react'

export class TVisionApp {
  constructor(uicontext, params, platform) {
    this.uicontext = uicontext
    this.platform = platform
    this.appParams = this.applyDefaults(params)
    this.appArgs = {args:null}
  }

  createView() {
    const { viewFactory } = this.appParams
    if(viewFactory)
      {
          const view = viewFactory.createView(this.uicontext, this, this.platform);
          this.appArgs.args = null
          return view
      }

    const { title } = this.appParams
    return <div class="fill-parent" style={{border:"1px solid black", backgroundColor:"slategrey"}}>
        {title}
      </div>
  }

  getPackageId() {return this.appParams.packageId}
  getTitle() {return this.appParams.title}
  getIcon() {return this.appParams.icon}
  setArgs(args){this.appArgs.args=args;return this}
  getArgs(){return this.appArgs.args}
  isVisibleInMenu(){return this.appParams.visibleInMenu}
  requiresAccountActivation(){return this.appParams.requiresAccountActivation}

  getSettings() {
    const { title, icon } = this.appParams
    return {
      title : title,
      icon : icon
    }
  }

  applyDefaults(params) {
    const defaultValues = {
      title:'No Title',
      packageId:null,
      icon:null,
      visibleInMenu:true,
      requiresAccountActivation:true
    }
    return Object.assign({}, defaultValues, params)
  }

  open() {
      this.platform.applicationHandler().createAppInstance(this)
  }

}
