

export class Lock {

    constructor() {
        this.locked= false;
    }

    isLocked(){return this.locked}
    lock(){this.locked = true}
    unlock(){this.locked = false}
}