import AbstractParamsBuilder from "../../../platform/builder/AbstractParamsBuilder";


export class AsyncActionResponseBuilder extends AbstractParamsBuilder {

    constructor() {
        super({
            success:false,
            data:{},
            message:"",
            namedMessages:[],
            sessionExpired:false
        })
    }

    setSuccess() {this.params.success=true;return this}
    setMessage(s){this.params.message = s;return this}
    setData(d){this.params.data=d;return this;}
    setSessionExpired(){this.params.sessionExpired=true;return this}
    addNamedMessage(name, msg){this.params.namedMessages.push({name:name, message:msg});return this;}
}