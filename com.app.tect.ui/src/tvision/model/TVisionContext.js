
import React from 'react'

import {TVisionI8nManager} from "../i8n/TVisionI8nManager";
import {UserAccountHandler} from "./handlers/UserAccountHandler";
import {NotificationHandler} from "./handlers/NotificationHandler";
import {SettingsHandler} from "./handlers/SettingsHandler";
import {ApplicationHandler} from "./handlers/ApplicationHandler";
import {MessageHandler} from "./handlers/MessageHandler";
import {ServerPollHandler} from "./handlers/ServerPollHandler";
import {Server} from "./server/Server";


export default class TVisionContext {
    constructor(uicontext, parentComp) {
        this.__uicontext = uicontext
        this.parentComp = parentComp
    }
    isProduction() {return this.props.environmentMode=="PROD"}
    isDev() {return this.props.environmentMode=="DEV"}
    isStage() {return this.props.environmentMode=="STAGE"}
    updateState(stateChanges) {this.parentComp.setState(stateChanges);console.error("State Changes", stateChanges);}
    state() {return this.parentComp.state}
    i8nMessage(m) {return new TVisionI8nManager(this.__uicontext).getMessage(m)}
    getUiContext() {return this.__uicontext}
    getServer(handler){return new Server(handler, true) }

    uicontext() {return this.__uicontext}
    // domain handlers
    applicationHandler(){return new ApplicationHandler(this)}
    userAccountHandler(){return new UserAccountHandler(this)}
    settingsHandler(){return new SettingsHandler(this)}
    notificationHandler(){return new NotificationHandler(this)}
    messageHandler(){return new MessageHandler(this)}

    static getDomains(parent) {
        return [
            new UserAccountHandler(parent),
            new SettingsHandler(parent),
            new NotificationHandler(parent),
            new ApplicationHandler(parent),
            new MessageHandler(parent),
            new ServerPollHandler(parent)
        ]
    }


    static loadSessionState(uicontext, session) {
        let state = {}
        TVisionContext.getDomains().forEach(domain => state = Object.assign({}, state, domain.getSessionValues(uicontext, session)))
        return state;
    }

    static loadLastActiveSession(uicontext) {
        return UserAccountHandler.loadLastActiveSession(uicontext)
    }


    static loadServerState(parentComp, doneHandler, uicontext, session) {
        const onStateLoadedHandler = (serverState) => {
            parentComp.setState(serverState)
            if(doneHandler){doneHandler()}
        }

        let state = {}
        const domains = TVisionContext.getDomains(new TVisionContext(uicontext, parentComp))

        function onHandlerDone(s, nextIndex)  {
            state = Object.assign({}, state, s)
            if(nextIndex < domains.length) {
                domains[nextIndex].loadServerValues((ns) => onHandlerDone(ns, nextIndex+1), uicontext, session)
            } else {
                onStateLoadedHandler(state)
            }
        }

        domains[0].loadServerValues((ns) => onHandlerDone(ns, 1), uicontext, session)
    }

    switchSession(newSession, onDoneHandler) {
        const currentSession = this.userAccountHandler().getSession();
        if(currentSession.isSameAccount(newSession)) {
            this.userAccountHandler().setSessionModeUser()
            if(onDoneHandler){onDoneHandler()}
            return
        }
        this.saveState()
        this.userAccountHandler().saveSessionValues(this.getUiContext(), newSession)
        TVisionContext.loadServerState(this.parentComp, onDoneHandler, this.uicontext(), newSession)
    }

    saveState() {
        const session = this.userAccountHandler().getSession();
        TVisionContext.getDomains(this).forEach(domain => domain.saveSessionValues(this.getUiContext(), session))
    }

    getCurrentDataVersionParams() {
        let dataVersions = {}
        const appendVersion = domain => dataVersions = Object.assign({}, dataVersions, domain.getCurrentDataVersion())
        TVisionContext.getDomains(this).forEach(appendVersion)
        return dataVersions
    }

}
