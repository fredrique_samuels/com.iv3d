import {TVisionDomainHandler} from "../shared/TVisionDomainHandler";
import * as tectui from "../../../tectui/tectui";


class PollHandlerState {

    constructor(params={}) {
        this.params = Object.assign({}, {
            timeout:null
        }, params)

        new tectui.Timeout();
    }
    stop(){
        if(this.params.timeout) {
            this.params.timeout.stop()
        }
        return this.__modify({timeout:null})
    }

    __modify(params) {
        const newParams = Object.assign({}, this.params, params)
        return new PollHandlerState(newParams)
    }
}

export class ServerPollHandler extends TVisionDomainHandler {

    constructor(model) {
        super(model, {serverPollHandler:{pollThread:new PollHandlerState()}});
    }

    loadServerValues(doneHandler, uiContext, session) {
        this.__stopCurrentThread()
        if(session.isOffline()) {
            if (doneHandler) {
                doneHandler({serverPollHandler: {pollThread: new PollHandlerState()}})
            }
            return
        }

        const pollHandlerState = this.startDelayedPoll(tectui.TimeUnit.seconds(10).milliSeconds(), false);
        if(doneHandler) {
            doneHandler({serverPollHandler:{pollThread:pollHandlerState}})
        }
    }

    startDelayedPoll(delay, update) {
        const timeout = new tectui.Timeout(delay, this.checkForUpdates.bind(this));
        const pollHandlerState = new PollHandlerState({timout: timeout});
        if(update) {
            this.updateState({serverPollHandler: {pollThread: pollHandlerState}})
        }
        return pollHandlerState
    }

    checkForUpdates() {
        const currentDataVersionParams = this.model().getCurrentDataVersionParams();
        console.error("Checking for updates", currentDataVersionParams)
        this.startDelayedPoll(tectui.TimeUnit.seconds(10).milliSeconds(), true)
    }

    __stopCurrentThread() {
        this.state().serverPollHandler.pollThread.stop()
    }
}