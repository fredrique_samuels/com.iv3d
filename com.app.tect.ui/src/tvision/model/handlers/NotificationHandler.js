
import Immutable from 'immutable'
import {TVisionDomainHandler} from "../shared/TVisionDomainHandler";


export class NotificationHandler extends TVisionDomainHandler {

    constructor(model) {
        super(model, {
            notificationsHandler : {
                notifications: []
            }
        })
    }

    loadServerValues(doneHandler, uiContext, session) {
        if(session.isOffline()) {
            doneHandler(this.__defaultState())
            return
        }

        const serverDone = (response) => {
            doneHandler(this.__buildHandlerState(response.data))
        }
        this.getServer().loadNotifications(serverDone, session)
    }

    deleteNotification(id) {
        const list  = new Immutable.List(this.notifications())
        const index = list.findIndex(n => n.getId()==id)
        if(index >= 0) {
            const newNotications = list.delete(index).toArray()
            this.__updateNotification(newNotications)
        }
    }

    notifications() {
        return [].concat(this.__getHandlerState().notifications)
    }

    __updateNotification(nl) {this.__updateHandlerState({notifications:nl})}


    __getHandlerState(){return this.state().notificationsHandler}
    __updateHandlerState(updates) {
        const newState = Object.assign({}, this.state().notificationsHandler, updates)
        this.updateState({notificationsHandler:newState})
    }

    __buildHandlerState(v){return {notificationsHandler:v}}
    __defaultState() {
        const v = {
            notificationsDataVersion:0,
            notifications:[]
        }
        return this.__buildHandlerState(v)
    }

    getCurrentDataVersion() {
        return {notificationsDataVersion:this.state().notificationsHandler.notificationsDataVersion}
    }
}