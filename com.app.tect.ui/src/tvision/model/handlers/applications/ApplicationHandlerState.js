import TVisionModel from "./TVisionModel";

export class ApplicationHandlerState {
    static createNewState(applist=[]) {
        return {
            applicationHandler: {
                model:new TVisionModel(),
                applist:applist
            }
        }
    }

    static getHandlerState(handler) {
        return handler.state().applicationHandler
    }

    static getHandlerStateForModel(handler, model) {
        const applicationHandler = handler.state().applicationHandler;
        return {applicationHandler:Object.assign({}, applicationHandler, {model:model})}
    }

}