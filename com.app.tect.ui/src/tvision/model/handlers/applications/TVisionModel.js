import React from 'react'
import Immutable from 'immutable'

const DISPLAY_MODE_APP='displaymode.app'
const DISPLAY_MODE_HOME='displaymode.home'
const DISPLAY_MODE_MENU='displaymode.menu'
const DISPLAY_MODE_APP_LIST='displaymode.applist'

export default class TVisionModel {
  constructor(model) {
      if(model) {
        this.model = model
      } else {
        this.model = {
          displayMode : DISPLAY_MODE_MENU,
          processList : new Immutable.OrderedMap(),
          // processList : new Immutable.List(),
          processIdSeed : 0
        }
      }
  }

  returnNew(m) {
    return new TVisionModel(Object.assign({}, this.model, m))
  }
  
  inDisplayModeHome(){return this.model.displayMode==DISPLAY_MODE_HOME}
  inDisplayModeApp(){return this.model.displayMode==DISPLAY_MODE_APP}
  inDisplayModeMenu(){return this.model.displayMode==DISPLAY_MODE_MENU}
  inDisplayModeAppList(){return this.model.displayMode==DISPLAY_MODE_APP_LIST}

  getProcessList() {return this.model.processList.toArray() }
  copyProcessList() {return Immutable.List.of(this.model.processList.toArray())}

  static setDisplayModeHome(oldModel){return oldModel.returnNew({displayMode:DISPLAY_MODE_HOME})}
  static setDisplayModeApp(oldModel){return oldModel.returnNew({displayMode:DISPLAY_MODE_APP})}
  static setDisplayModeMenu(oldModel){return oldModel.returnNew({displayMode:DISPLAY_MODE_MENU})}
  static setDisplayModeAppList(oldModel){return oldModel.returnNew({displayMode:DISPLAY_MODE_APP_LIST})}

  static setProcessVisible(oldModel, params) {
    const process = oldModel.model.processList.get(params.process.processId)
    if(process) {
      oldModel.model.processList.toList().forEach(p => {
        p.visible=(p.processId==params.process.processId)
        return true
      })
      return oldModel.returnNew({ displayMode:DISPLAY_MODE_APP, processList:oldModel.model.processList} )
    }

    return oldModel
  }

  static destroyProcess(oldModel, params) {
      const { process } = params;
      return oldModel.returnNew({
          processList:oldModel.model.processList.delete(process.processId)
      } )
  }

  static runApp(oldModel, params) {
      const { app } = params
      const process = oldModel.model.processList.toList().find( p => {
          return app.getPackageId() && app.getPackageId()==p.app.getPackageId()
      } )
  }

  static createAppInstance(oldModel, params){
    const { app, appArgs } = params
    const process = oldModel.model.processList.toList().find( p => {
        return app.getPackageId() && app.getPackageId()==p.app.getPackageId()
      } )

    if( !process ) {
        //hide all visible processes
        oldModel.model.processList.toList().forEach(p => {
          p.visible=false
          return true
        })

        //create new process
        const newProcessIdSeed =  oldModel.model.processIdSeed + 1
        const newProcessList = oldModel.model.processList.set(newProcessIdSeed,
          {
            processId:newProcessIdSeed,
            visible:true,
            app:app.setArgs(appArgs)
          }
        )

        return oldModel.returnNew({
          displayMode:DISPLAY_MODE_APP,
          processList:newProcessList,
          processIdSeed:newProcessIdSeed
        } )
    } else {
        process.app.setArgs(appArgs)
      return TVisionModel.setProcessVisible(oldModel, {process:process})
    }
    return oldModel
  }
}
