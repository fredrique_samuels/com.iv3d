import {SESSION_TYPE_USER} from "./SessionHandlerConstants";
export class SessionHandlerState {
    static getDefault() {
        return {
                sessionId: null,
                accountId: 0,
                userId: 0,
                sessionType: SESSION_TYPE_USER
        }

    }

    static createNewState(params={}) {
        return {sessionHandler:Object.assign({}, SessionHandlerState.getDefault(), params)}
    }

    static getHandlerState(handler){
        return handler.state().sessionHandler
    }

    static updateHandlerState(handler, updates) {
        const newState = Object.assign({}, SessionHandlerState.getHandlerState(handler), updates)
        return handler.updateState({sessionHandler:newState})
    }

    static createStateForSession(session) {
        const params = {
            sessionId: session.getSessionId(),
            userId: session.getUserId(),
            accountId: session.getAccountId()
        };
        return SessionHandlerState.createNewState(params);
    }
}