import {Session} from "../../domain/Session";
import {TVISION_STORE_SESSIONS_OFFLINE} from "./SessionHandlerConstants";
import {TVISION_STORE_SESSIONS_LAST_SESSION_ID} from "./SessionHandlerConstants";
import {TVISION_STORE_SESSIONS} from "./SessionHandlerConstants";

/*
SAVED MODEL STRUCTURE

{
    offline: ...sessionData
    accounts: [
        {
            userId : 1234,
            accountId : 1234,

        }
        , ...
    ]
}


*/


/*

 tvision.model.domain.sessions.last.session = session12345
 tvision.model.domain.sessions.session.session12345 =
 {
 userId:user1234,
 accountId:account12345
 }

 tvision.model.domain.sessions.session.user1234.account12345 =
 {
 }

 */
export class SessionStore {

    constructor(uiContext) {
        this.uiContext = uiContext
    }

    saveAsLastSession(session) {
        const cookieManager = this.uiContext.cookieManager();

        if(session.isOffline()) {
            cookieManager.deleteCookie(TVISION_STORE_SESSIONS_LAST_SESSION_ID)
            return
        }

        const sessionId = new String(session.getSessionId());

        cookieManager.setCookie(TVISION_STORE_SESSIONS_LAST_SESSION_ID, sessionId)
        cookieManager.setCookie(TVISION_STORE_SESSIONS + "." + sessionId, session.toJson())
    }

    getLastSessionId() {
        const cookieManager = this.uiContext.cookieManager();
        const cookie = cookieManager.getCookie(TVISION_STORE_SESSIONS_LAST_SESSION_ID);
        return cookie;
    }

    loadSession(sessionId) {
        const cookieManager = this.uiContext.cookieManager();
        const cookie = cookieManager.getCookie(TVISION_STORE_SESSIONS + "." + sessionId);
        if(cookie) {
            return Session.fromJson(cookie)
        }
        return null;
    }
}