import {AsyncActionResponseBuilder} from "../../response/AsyncActionResponseBuilder";
import {DemoServer} from "../../server/DemoServer";
import {Server} from "../../server/Server";


export class SessionServer {


    constructor(handler) {
        this.handler = handler;
    }

    validateCredentials(u,p, responseHandler) {
        if(!u) {
            const response = new AsyncActionResponseBuilder()
                .setMessage(this.handler.i8nMessage("tvision.session.login.validation.username.empty"))
                .build()
            responseHandler(response)
            return
        }

        if(!p) {
            const response = new AsyncActionResponseBuilder()
                .setMessage(this.handler.i8nMessage("tvision.session.login.validation.password.empty"))
                .build()
            responseHandler(response)
            return
        }
        this.getServer().validateCredentials(u,p, responseHandler)
    }

}
