import {Session} from "../../../domain/Session";


export function Logout(handler) {
    const loggedIn = !handler.isSessionModeOffline();
    if(loggedIn) {
        const session = Session.createOffline();
        handler.model().switchSession(session)
    }
    handler.setSessionModeUser();
}