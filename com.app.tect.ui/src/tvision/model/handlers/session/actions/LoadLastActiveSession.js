import {SessionStore} from "../SessionStore";
import {Session} from "../../../domain/Session";

export function LoadLastActiveSession(uiContext) {
    const sessionStore = new SessionStore(uiContext);
    const lastSessionId = sessionStore.getLastSessionId();
    if(!lastSessionId) {
        return Session.createOffline()
    }

    const session = sessionStore.loadSession(lastSessionId);
    if(session) {
        return session
    }

    return Session.createOffline()
}