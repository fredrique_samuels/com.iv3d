import {Session} from "../../../domain/Session";

export function SetSessionModeOffline(handler) {
    if(!handler.isSessionModeOffline()) {
        const session = Session.createOffline();
        handler.model().switchSession(session)
    }
}