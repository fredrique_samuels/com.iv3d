export const SESSION_TYPE_EXPIRED  = "session.type.expired"
export const SESSION_TYPE_LOGIN  = "session.type.login"
export const SESSION_TYPE_USER  = "session.type.user"
export const SESSION_TYPE_LOGOUT  = "session.type.logout"
export const SESSION_TYPE_REGISTER  = "session.type.register"

export const TVISION_STORE_SESSIONS = "tvision.model.domain.sessions"
export const TVISION_STORE_SESSIONS_LAST_SESSION_ID = "tvision.model.domain.sessions.last.sessionid"