import {TVisionDomainHandler} from "../shared/TVisionDomainHandler";
import {ApplicationHandlerState} from "./applications/ApplicationHandlerState";
import YgoDeckBuilderApp from "../../../apps/com.tvision.games.yugioh.deckbuilder/com.tvision.games.yugioh.deckbuilder";
import AdminWidgetTestApp from "../../../apps/com.tvision.app.admin.widgets.test/com.tvision.app.admin.widgets.test";
import {NotificationsApp} from "../../../apps/com.tvision.app.notifications/com.tvision.app.notifications"
import TVisionModel from "./applications/TVisionModel";
import {ActivateAccountApp} from "../../../apps/com.tvision.app.account.activation/com.tvision.app.account.activation";
import {TanomeApp} from "../../../apps/com.tvision.apps.tanome/com.tvision.app.tanome";
import {DataEditorApp} from "../../../apps/com.tvision.app.data.editor/com.tvision.app.data.editor";


export class ApplicationHandler extends TVisionDomainHandler {

    constructor(model) {
        super(model, ApplicationHandlerState.createNewState())
    }

    getHandlerState() {
        return ApplicationHandlerState.getHandlerState(this);
    }

    updateStateModel(newModel) {
        if(newModel) {
            this.updateState(ApplicationHandlerState.getHandlerStateForModel(this, newModel))
        }
    }

    loadServerValues(doneHandler, uiContext, session) {

        const defaultApps = [
            new ActivateAccountApp(uiContext, this.model()),
            new NotificationsApp(uiContext, this.model()),
            new DataEditorApp(uiContext, this.model())
        ]

        if(session.isOffline()) {
            const serverState = ApplicationHandlerState.createNewState(defaultApps);
            doneHandler(serverState)
            return
        }

        const userApps =[
            new YgoDeckBuilderApp(uiContext, this.model()),
            new AdminWidgetTestApp(uiContext, this.model())

            // new UserSettingsApp(this.uicontext),
            // new LocationNewsApp(this.uicontext),
            // new AdminServerOperationsApp(this.uicontext)
        ]
        const serverState = ApplicationHandlerState.createNewState(defaultApps.concat(userApps))
        doneHandler(serverState)

        // const serverDone = (response) => {
        //     doneHandler(SettingsHandlerState.createNewState(response.data))
        // }
        // new Server(this).loadAccountSettings(serverDone, session)
    }

    getAppList() {
        const appList = this.getHandlerState().applist
        return appList.sort((a,b)=> a.getTitle().localeCompare(b.getTitle()))
    }

    getProcessList() { return this.getHandlerState().model.getProcessList() }

    getSortedAppList() {
        const appList = this.getHandlerState().applist;
        return appList.sort((a,b)=> a.getTitle().localeCompare(b.getTitle()))
    }

    createAppInstance(app, appArgs={}) {
        const userAccountHandler = this.model().userAccountHandler();
        const notActivated = !this.model().settingsHandler().getAccount().isActivated();

        if (app.requiresAccountActivation() && notActivated) {
            this.model().applicationHandler().runApp("com.tvision.app.account.activation")
        } else {
            const oldModel = this.getHandlerState().model
            const newModel = TVisionModel.createAppInstance(oldModel, {app: app, appArgs: appArgs})
            this.updateStateModel(newModel);
        }
    }

    setProcessVisible(process) {
        const oldModel = this.getHandlerState().model
        const newModel = TVisionModel.setProcessVisible(oldModel, {process:process})
        this.updateStateModel(newModel);
    }

    destroyProcess(process) {
        const oldModel = this.getHandlerState().model
        const newModel = TVisionModel.destroyProcess(oldModel, {process:process})
        this.updateStateModel(newModel);
    }

    destroyProcessByPackageId(appPackageId) {
        const appList = this.getProcessList();
        const process = appList.find( p => {
            return appPackageId && appPackageId==p.app.getPackageId()
        })

        this.destroyProcess(process)
    }

    runApp(appPackageId, appArgs={}) {
        const appList = this.getAppList();
        const app = appList.find( app => {
            return appPackageId && appPackageId==app.getPackageId()
        })

        if(app)
            this.createAppInstance(app, appArgs)
    }

    setDisplayModeHome(){
        const oldModel = this.getHandlerState().model
        const newModel = TVisionModel.setDisplayModeHome(oldModel)
        this.updateStateModel(newModel);
    }

    setDisplayModeApp(){
        const oldModel = this.getHandlerState().model
        const newModel = TVisionModel.setDisplayModeApp(oldModel)
        this.updateStateModel(newModel);
    }

    setDisplayModeMenu(){
        const oldModel = this.getHandlerState().model
        const newModel = TVisionModel.setDisplayModeMenu(oldModel)
        this.updateStateModel(newModel);
    }

    setDisplayModeAppList(){
        const oldModel = this.getHandlerState().model
        const newModel = TVisionModel.setDisplayModeAppList(oldModel)
        this.updateStateModel(newModel);
    }

    inDisplayModeHome(){return this.getHandlerState().model.inDisplayModeHome()}
    inDisplayModeApp(){return this.getHandlerState().model.inDisplayModeApp()}
    inDisplayModeMenu(){return this.getHandlerState().model.inDisplayModeMenu()}
    inDisplayModeAppList(){return this.getHandlerState().model.inDisplayModeAppList()}
}