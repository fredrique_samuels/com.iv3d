import {SettingsStore} from "./SettingsStore";
import * as TVisionConstants from "../../../TVisionConstants";
import {UserParams} from "../../domain/UserParams";
import {AccountParams} from "../../domain/AccountParams";

export class SettingsHandlerState {
    static getDefault() {
        return {settings:{}}
    }

    static createNewState(params={}) {
        return {settingsHandler:Object.assign({}, SettingsHandlerState.getDefault(), params)}
    }

    static getHandlerState(handler){
        return handler.state().settingsHandler
    }

    static updateHandlerState(handler, settings) {
        return handler.updateState({settingsHandler:{settings:settings}})
    }

    static saveHandlerState(session, uiContext,  handler) {
        const store = new SettingsStore(uiContext);
        store.saveSessionSettings(session, SettingsHandlerState.getHandlerState(handler))
    }

    static createOfflineState() {
        return { settings :
            {
                user: UserParams.createOffline(),
                account: AccountParams.createOffline()
            }
        }
    }

    static createStateForSession(uiContext, session) {
        if(session.isOffline()) {
            return SettingsHandlerState.createNewState(SettingsHandlerState.createOfflineState());
        }

        const settings = new SettingsStore(uiContext).getSessionSettings(session);
        if(settings)
            return SettingsHandlerState.createNewState(settings);

        return SettingsHandlerState.createNewState();
    }
}