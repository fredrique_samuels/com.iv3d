import * as TVisionConstants from "../../../TVisionConstants";

const ACCOUNT_SETTINGS_NS = "tvision.model.domain.settings"

export class SettingsStore {

    constructor(uiContext) {
        this.uiContext = uiContext
    }

    getSessionSettings(session) {
        if (session.isOffline()) {
            return [
                {name: TVisionConstants.USER_SETTINGS_DISPLAY_NAME, value: "offline user"},
                {name: TVisionConstants.USER_SETTINGS_FIRST_NAME, value: "Offline User"},
                {name: TVisionConstants.USER_SETTINGS_LAST_NAME, value: null},

                {name: TVisionConstants.USER_SETTINGS_CONTACT_EMAIL, value: null}
            ]
        }

        const ns = session.createAccountNameSpace(ACCOUNT_SETTINGS_NS);

        const cookieManager = this.uiContext.cookieManager();
        const cookie = cookieManager.getCookie(ns);
        if (cookie) {
            return JSON.parse(cookie)
        }

        return null
    }

    saveSessionSettings(session, settings) {
        if(session.isOffline()) return

        const ns = session.createAccountNameSpace(ACCOUNT_SETTINGS_NS);

        const cookieManager = this.uiContext.cookieManager()
        cookieManager.setCookie(ns, JSON.stringify(settings))
    }

}