import {TVisionDomainHandler} from "../shared/TVisionDomainHandler";
import {LoadLastActiveSession} from "./session/actions/LoadLastActiveSession";
import {Logout} from "./session/actions/Logout";
import {SetSessionModeOffline} from "./session/actions/SetSessionModeOffline";
import {SessionHandlerState} from "./session/SessionHandlerState";
import {SessionServer} from "./session/SessionGateway";
import {Session} from "../domain/Session";
import {SessionStore} from "./session/SessionStore";
import {SESSION_TYPE_USER} from "./session/SessionHandlerConstants";
import {SESSION_TYPE_EXPIRED} from "./session/SessionHandlerConstants";
import {SESSION_TYPE_LOGIN} from "./session/SessionHandlerConstants";
import {SESSION_TYPE_LOGOUT} from "./session/SessionHandlerConstants";
import {SESSION_TYPE_REGISTER} from "./session/SessionHandlerConstants";
import {AsyncActionResponseBuilder} from "../response/AsyncActionResponseBuilder";



export class UserAccountHandler extends TVisionDomainHandler {

    constructor(model) {
        super(model, SessionHandlerState.createNewState())
    }

    getSessionValues(uiContext, session) {
        return SessionHandlerState.createStateForSession(session)
    }

    getSessionServer(){return new SessionServer(this)}

    /**
     * Load the last session recorded when the page was last open. If the session has expired then an offline
     * session is returned.
     */
    static loadLastActiveSession(uiContext){return LoadLastActiveSession(uiContext)}

    getHandlerState() { return SessionHandlerState.getHandlerState(this) }
    updateHandlerState(updates) { SessionHandlerState.updateHandlerState(this, updates) }

    saveSessionValues(uiContext, session) {
        new SessionStore(uiContext).saveAsLastSession(session)
    }

    /**
     * Session Modes
     * USER: There is a user account associated with the current session. All data to be stored in cookies(if available)
     *      and server.
     * EXPIRED: Set active when the USER session expires. Forces user to re-authenticate or go into offline mode.
     * LOGIN: User is prompted to login.
     */
    setOfflineSessionMode() {SetSessionModeOffline(this);}
    setSessionModeUser() {this.updateHandlerState({sessionType : SESSION_TYPE_USER})}
    setSessionModeExpired() {if(this.getHandlerState().sessionType!=SESSION_TYPE_EXPIRED)this.updateHandlerState({sessionType: SESSION_TYPE_EXPIRED})}
    setSessionModeLogin() {if(!this.isSessionModeLogin()) this.updateHandlerState({sessionType: SESSION_TYPE_LOGIN})}
    setSessionModeLogout() {if(!this.isSessionModeLogout()) this.updateHandlerState({sessionType: SESSION_TYPE_LOGOUT})}
    setSessionModeRegister() {if(!this.isSessionModeRegister()) this.updateHandlerState({sessionType: SESSION_TYPE_REGISTER})}
    isSessionModeOffline(){return this.getSession().isOffline()}
    isSessionModeUser(){return this.getHandlerState().sessionType==SESSION_TYPE_USER}
    isSessionModeExpired(){return this.getHandlerState().sessionType==SESSION_TYPE_EXPIRED}
    isSessionModeLogin(){return this.getHandlerState().sessionType==SESSION_TYPE_LOGIN}
    isSessionModeLogout(){return this.getHandlerState().sessionType==SESSION_TYPE_LOGOUT}
    isSessionModeRegister(){return this.getHandlerState().sessionType==SESSION_TYPE_REGISTER}

    getSessionId(){return this.getHandlerState().getSessionId}
    getSession(){return new Session(this.getHandlerState())}

    validateCredentials(params, cbo){
        const responseHandler = (asyncResponse) => {
            if(asyncResponse.success) {
                const {sId, uId, aId} = asyncResponse.data
                const session = Session.createUserSession(sId, uId, aId)
                this.model().switchSession(session, () => cbo.success())
            } else {
                cbo.error(asyncResponse.message?asyncResponse.message:"Server Error")
            }
        }
        this.getServer().login(params, responseHandler)
    }

    createNewUserAndAccount(params, cbo) {
        const responseHandler = (asyncResponse) => {
            if(asyncResponse.success) {
                const {sId, uId, aId} = asyncResponse.data
                const session = Session.createUserSession(sId, uId, aId)
                this.model().switchSession(session, () => cbo.success())
            } else {
                cbo.error(asyncResponse.message, asyncResponse.fieldErrors)
            }
        }

        this.getServer().createNewUserAndAccount(params, responseHandler);
    }

    logout(cbo){
        const responseHandler = (asyncResponse) => {
            if(asyncResponse.success) {
                const {sId, uId, aId} = asyncResponse.data
                const session = Session.createUserSession(sId, uId, aId)
                this.model().switchSession(session, () => cbo.success())
            } else {
                cbo.error(asyncResponse.message, asyncResponse.fieldErrors)
            }
        }
        this.getServer().logout(responseHandler)
    }

    resendValidationEmail(onDoneHandler) {
        const responseHandler = (asyncResponse) => {
            if(asyncResponse.success) {
                onDoneHandler()
            } else {
                if(asyncResponse.sessionExpired) {
                    this.model().sessionHandler().setSessionModeExpired()
                }
            }
        }
        this.getServer().resendValidationEmail(this.getSession(), responseHandler)
    }

    activateAccount(token, cbo) {
        const setAccountActive = () => this.model().settingsHandler().updateAccount({activated:true, dlu:new Date()})
        const responseHandler = (asyncResponse) => {
            if(asyncResponse.success) {
                setAccountActive()
                cbo.success()
            } else {
                if(asyncResponse.sessionExpired) {
                    this.model().sessionHandler().setSessionModeExpired()
                }
                cbo.error()
            }
        }

        this.getServer().activateAccount(token, this.getSession(), responseHandler)
    }

}