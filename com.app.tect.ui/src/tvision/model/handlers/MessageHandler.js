import {TVisionDomainHandler} from "../shared/TVisionDomainHandler";

export class MessageHandler extends TVisionDomainHandler {

    constructor(model) {
        super(model, {messageHandler:{messages:[]}});
    }

    loadServerValues(doneHandler, uiContext, session) {
        if (session.isOffline()) {
            const serverState = {messageHandler:{messages:[]}}
            doneHandler(serverState)
        } else {
            const serverState = {messageHandler: {messages: []}}
            doneHandler(serverState)
        }
    }
}