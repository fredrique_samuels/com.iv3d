import {TVisionDomainHandler} from "../shared/TVisionDomainHandler";
import {SettingsHandlerState} from "./settings/SettingsHandlerState";
import {UserParams} from "../domain/UserParams";
import {AccountParams} from "../domain/AccountParams";

export class SettingsHandler extends TVisionDomainHandler {

    constructor(model) {
        super(model, SettingsHandlerState.createNewState())
    }

    getUser() {
        const user = SettingsHandlerState.getHandlerState(this).settings.user;
        if(user) return user
        return UserParams.createOffline();
    }

    getAccount() {
        const account = SettingsHandlerState.getHandlerState(this).settings.account;
        if(account) return account
        return AccountParams.createOffline();
    }

    updateAccount(params) {
        const state = this.state();
        state.settingsHandler.settings.account.update(params)
        this.updateState(state)
    }

    getSessionValues(uiContext, session) {
        return SettingsHandlerState.createStateForSession(uiContext, session)
    }

    saveSessionValues(uiContext, session) {
        // SettingsHandlerState.saveHandlerState(session, uiContext, this)
    }

    loadServerValues(doneHandler, uiContext, session) {
        if(session.isOffline()) {
            const newState = SettingsHandlerState.createNewState(SettingsHandlerState.createOfflineState());
            doneHandler(newState)
            return
        }

        const serverDone = (response) => {
            doneHandler(SettingsHandlerState.createNewState(response.data))
        }
        this.getServer().loadAccountSettings(serverDone, session)
    }

}