import {AsyncActionResponseBuilder} from "../response/AsyncActionResponseBuilder";
import {Session} from "../domain/Session";
import AccountDomainBuilder from "./demo/builders/AccountDomainBuilder";
import {UserDomainBuilder} from "./demo/builders/UserDomainBuilder";
import {DemoSetup} from "./demo/setup/DemoSetup";
import {AccountDomain} from "./demo/domain/AccountDomain";
import {UserDomain} from "./demo/domain/UserDomain";

function createDemoData() {
    const demoData = {
        userIdSeed:2,
        users : [],
        accounts : []
    }

    {
        const user = new UserDomainBuilder('demo.user.1', 'jon.snow@mail.com', 'snow')
            .setFirstName("Jon")
            .setLastName("Jon")
            .buildModel();

        const account = new AccountDomainBuilder('demo.account.1', user, "code1")
            .setActivated()
            .buildModel()
        demoData.users.push(user)
        demoData.accounts.push(account)
    }

    {
        const user = new UserDomainBuilder('demo.user.2', 'ned.stark@mail.com', 'stark')
            .setFirstName('Eddard')
            .setLastName('Stark')
            .buildModel();

        const account = new AccountDomainBuilder('demo.account.2', user, "code2")
            .setName("My Account")
            .buildModel()
        AccountDomainBuilder.addNotifications(account, 12)

        demoData.users.push(user)
        demoData.accounts.push(account)
    }

    return demoData
}

const demoData = DemoSetup.createDemoData()

export class DemoServer {


    constructor(handler) {
        this.handler = handler
    }

    validateCredentials(u,p, responseHandler) {
        const user = this.__findUserByCredentials(u, p);
        if (user != null) {
            const account = this.__getUserAccount(user);
            const session = this.__createAccountSession(user, account);

            const response = new AsyncActionResponseBuilder()
                .setData(session)
                .setSuccess()
                .build()
            responseHandler(response)
            return
        }

        const response = new AsyncActionResponseBuilder()
            .setMessage(this.handler.i8nMessage("tvision.session.login.validation.nomatch"))
            .build()
        responseHandler(response)
    }

    createNewUserAndAccount(email, password, confirm_password, responseHandler) {
        const errors = []

        if(!email) {
            errors.push({field:"email", errorCode:"EMPTY"})
        } else if(this.__findUser(email)) {
           errors.push({field:"email", errorCode:"IN_USE"})
        }

        if(!password) {
            errors.push({field:"password", errorCode:"EMPTY"})
        } else if(password.length < 4) {
            errors.push({field:"password", errorCode:"MIN_LENGTH"})
        } else if(password!=confirm_password){
            errors.push({field:"confirm_password", errorCode:"CANNOT_CONFIRM"})
        }

        if(errors.length) {
            const response = new AsyncActionResponseBuilder()
                .setData({
                    fieldErrors: errors
                })
                .build()
            responseHandler(response)
        } else {

            demoData.userIdSeed = demoData.userIdSeed + 1
            const user = new UserDomainBuilder('demo.user.' + demoData.userIdSeed, email, password)
                .buildModel();
            const account = new AccountDomainBuilder('demo.account.2', user, "code2")
                .buildModel()
            demoData.users.push(user)
            demoData.accounts.push(account)

            const response = new AsyncActionResponseBuilder()
                .setData(this.__createAccountSession(user, account))
                .setSuccess()
                .build()
            responseHandler(response)
        }
    }

    loadAccountSettings(serverDone, session) {
        const user = this.__findUserById(session.getUserId())
        const account = this.__findAccountById(session.getAccountId())

        const settings = {
            user:user.toParams(),
            account:account.toParams()
        }

        const response = new AsyncActionResponseBuilder()
            .setData({settings:settings})
            .setSuccess()
            .build()
        serverDone(response)
    }

    resendValidationEmail(session, responseHandler) {
        const response = new AsyncActionResponseBuilder()
            .setSuccess()
            .build()
        responseHandler(response)
    }

    activateAccount(token, session, responseHandler) {
        const accountId = session.getAccountId();
        const account = this.__findAccountById(accountId)
        const success = account.activate(token)

        if(success) {
            const response = new AsyncActionResponseBuilder()
                .setSuccess()
                .build()
            responseHandler(response)
        } else {
            const response = new AsyncActionResponseBuilder()
                .setMessage(this.handler.i8nMessage("com.tvision.app.account.activation.form.activate.invalidtoken"))
                .build()
            responseHandler(response)
        }
    }

    loadNotifications(responseHandler, session) {
        const accountId = session.getAccountId();
        const account = this.__findAccountById(accountId);
        const notifications = account.getNotificationParams()

        const response = new AsyncActionResponseBuilder()
            .setData(notifications)
            .setSuccess()
            .build()
        responseHandler(response)
    }

    __findUserById(id) {
        const ts = demoData.users.filter(user => {
            return user.getId() == id
        });
        if (ts.length != 0) {
            return ts[0]
        }
        return UserDomain.null()
    }

    __findUser(email) {
        const ts = demoData.users.filter(user => user.getEmail() == email);
        if (ts.length != 0) {
            return ts[0]
        }
        return UserDomain.null()
    }

    __findUserByCredentials(mail, password) {
        const user = this.__findUser(mail)
        if(user==null)return null;
        if(user.params.password==password) {
            return user
        }
        return UserDomain.null()
    }

    __getUserAccount(user) {
        const filter = demoData.accounts.filter(a => a.userHasAccessRights(user));
        if(filter.length)
            return filter[0]
        return AccountDomain.null()
    }

    __findAccountById(id) {
        const ts = demoData.accounts.filter(account => account.getId() == id);
        if (ts.length != 0) {
            return ts[0]
        }
        return AccountDomain.null()
    }

    __createAccountSession(user, account) {
        const uId = user.getId();
        const aId = account.getId();
        const sId = "demo.session.id." + uId + "." + aId
        const session = Session.createUserSession(sId, uId, aId);
        return session
    }
}