import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {UserDomain} from "../domain/UserDomain";

export class UserDomainBuilder extends AbstractParamsBuilder {

    constructor(id, email, password) {
        super({
            id:id,
            email:email,
            password:password,
            displayName:email,
            firstName:null,
            lastName:null,
            dlu:new Date()
        })
    }

    setFirstName(n) {
        this.params.firstName = n
        return this
    }

    setLastName(n) {
        this.params.lastName = n
        return this
    }

    buildModel() {
        return new UserDomain(this.build())
    }
}
