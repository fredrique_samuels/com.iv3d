import AbstractParamsBuilder from "../../../../../platform/builder/AbstractParamsBuilder";
import {AccountDomain} from "../domain/AccountDomain";
import {NotificationBuilder} from "../../../domain/NotificationParams";

export default class AccountDomainBuilder extends AbstractParamsBuilder {

    constructor(id, user, activationCode) {
        super({
            id:id,
            accessibleUsersIds:[user.getId()],
            activationCode: activationCode,
            activated:false,
            name:null,
            dlu:new Date(),
            accountDataVersion:0,
            notifications: {
                items:[],
                notificationsDataVersion:0,
                idSeed:0
            }
        })
    }

    setName(n){this.params.name = n;return this}
    setActivated(){this.params.activated=true;return this}
    buildModel() {
        return new AccountDomain(this.build())
    }

    static addNotifications(account, count=1) {
        for (var i=0;i<count;i++) {
            const factory = (id) => AccountDomainBuilder.createRandomNotification((id));
            account.addNotification(factory)
        }
    }

    static createRandomNotification(id) {
        const alertFunctions = [
            (n) => n,
            (n) => n.setError(),
            (n) => n.setWarning()
        ]
        const typeFunctions = [
            (n) => n.setTypeText(),
            (n) => n.setTypeCredentials(),
            (n) => n.setTypeMessage()
        ]
        const textFunctions = [
            (n) => n.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
            (n) => n.setText("Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
            (n) => n.setText("Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        ]

        const randomItem = function(items){return items[Math.floor(Math.random()*items.length)];}

        let n = new NotificationBuilder()
        n = n.setId(id++)
        n = randomItem(alertFunctions)(n)
        n = randomItem(typeFunctions)(n)
        n = randomItem(textFunctions)(n)
        return n.buildModel()
    }
}
