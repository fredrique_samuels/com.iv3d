import {UserParams} from "../../../domain/UserParams";


export class UserDomain {

    constructor(params) {
        this.params = params
    }

    static null() {
        const params = {
            id:1,
            email:"<NULL-EMAIL>",
            dlu:new Date(),
            firstName:"-firstname-null-",
            lastName:"-lastname-null-",
            displayName:"-null-",

        }
        return new UserDomain(params)
    }

    getId(){return this.params.id}
    getEmail(){return this.params.email}
    getFirstName(){return this.params.firstName}
    getLastName(){return this.params.lastName}
    getDisplayName(){return this.params.displayName}
    getDlu(){return this.params.dlu}

    toParams() {
        return new UserParams(this.params)
    }
}