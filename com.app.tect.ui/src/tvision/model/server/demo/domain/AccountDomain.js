import {AccountParams} from "../../../domain/AccountParams";

export class AccountDomain {

    constructor(params) {
        this.params = params
    }
    
    
    static null() {
        const params = {
            id:1,
            name:"<NULL>",
            dlu:new Date(),
            activated:false,
            accountDataVersion:0,
            activationCode:null,
            accessibleUsersIds:[1],
            notifications:{items:[], notificationsDataVersion:0}
        }
        return new AccountDomain(params)
    }

    getId(){return this.params.id}
    getName(){return this.params.name}
    getDlu(){return this.params.dlu}
    isActivated(){return this.params.activated}
    version(){return this.params.accountDataVersion}
    
    activate(token) {
        if(this.params.activationCode==token) {
            this.params.activated = true
            return true
        }
        return false
    }

    userHasAccessRights(user) {
        return this.params.accessibleUsersIds.includes(user.getId())
    }


    getNotificationParams() {
        const {items, notificationsDataVersion} = this.params.notifications
        return  {
            notificationsDataVersion:notificationsDataVersion,
            notifications:items
        }
    }

    addNotification(factory) {
        const id = ++this.params.notifications.notificationsDataVersion
        this.params.notifications.items.push(factory(id))
    }

    toParams() {
        return new AccountParams(this.params)
    }
}