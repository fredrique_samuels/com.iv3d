import AccountDomainBuilder from "../builders/AccountDomainBuilder";
import {UserDomainBuilder} from "../builders/UserDomainBuilder";

export class DemoSetup {
    static createDemoData() {
        const demoData = DemoSetup.setupAccounts()
        return demoData
    }

    static setupAccounts() {
        const demoData = {
            userIdSeed:2,
            users : [],
            accounts : []
        }

        {
            const user = new UserDomainBuilder('demo.user.1', 'jon.snow@mail.com', 'snow')
                .setFirstName("Jon")
                .setLastName("Jon")
                .buildModel();

            const account = new AccountDomainBuilder('demo.account.1', user, "code1")
                .setActivated()
                .buildModel()
            demoData.users.push(user)
            demoData.accounts.push(account)
        }

        {
            const user = new UserDomainBuilder('demo.user.2', 'ned.stark@mail.com', 'stark')
                .setFirstName('Eddard')
                .setLastName('Stark')
                .buildModel();

            const account = new AccountDomainBuilder('demo.account.2', user, "code2")
                .setName("My Account")
                .buildModel()
            AccountDomainBuilder.addNotifications(account, 12)

            demoData.users.push(user)
            demoData.accounts.push(account)
        }

        return demoData
    }
}