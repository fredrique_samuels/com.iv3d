import {DemoServer} from "./DemoServer";
import {AsyncActionResponseBuilder} from "../response/AsyncActionResponseBuilder";

export class Server {

    constructor(handler, useDemo=true) {
        this.handler = handler
        this.useDemo = useDemo
        this.demoServerDelay = 1000
        this.demoServer = new DemoServer(handler)
        this.host = "http://localhost:15080"
    }

    /**
     *
     * @param u Username
     * @param p Password
     * @param responseHandler
     *      {
     *      }
     */


    loadAccountSettings(serverDone, session) {
        if(this.useDemo) setTimeout(() => this.demoServer.loadAccountSettings(serverDone, session), this.demoServerDelay)
        else throw "Real Server Not Available"
    }

    loadNotifications(serverDone, session) {
        if(this.useDemo) setTimeout(() => this.demoServer.loadNotifications(serverDone, session), this.demoServerDelay)
        else throw "Real Server Not Available"
    }

    resendValidationEmail(session, responseHandler) {
        if(this.useDemo) setTimeout(() => this.demoServer.resendValidationEmail(session, responseHandler), this.demoServerDelay)
        else throw "Real Server Not Available"
    }

    activateAccount(token, session, responseHandler) {
        if(this.useDemo) setTimeout(() => this.demoServer.activateAccount(token, session, responseHandler), this.demoServerDelay)
        else throw "Real Server Not Available"
    }

    login(params, responseHandler) {
        this.__requestPost(responseHandler, "/tvision.server.secure/login", params)
    }

    logout(responseHandler) {
        this.__requestPost(responseHandler, "/tvision.server.secure/logout")
    }

    createNewUserAndAccount(params, responseHandler) {
        this.__requestPost(responseHandler, "/tvision.server.secure/register", params)
    }

    __requestGet(path, params) {

    }

    __requestPost(responseHandler, path, params={}) {
        const xhttp = new XMLHttpRequest();

        const fourOhOne = this.__fourOhOne.bind(this)
        const twoHundred = this.__twoHundred.bind(this)
        const serverError = this.__serverError.bind(this)

        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 ) {
                if(this.status==200) {
                    twoHundred(responseHandler, {responseText:this.responseText})
                } else if(this.status==401){
                    fourOhOne()
                } else {
                    serverError(responseHandler)
                }
            }
        }

        xhttp.open("post", this.host+path, true);
        xhttp.send(JSON.stringify(params));
    }

    __fourOhOne() {
        this.handler.setExpireSession()
    }

    __twoHundred(responseHandler, serverResponse) {
        const {responseText} = serverResponse
        var parse = JSON.parse(JSON.parse(responseText));
        responseHandler(parse)
    }

    __serverError(responseHandler) {
        const response = new AsyncActionResponseBuilder()
            .setMessage("Server Error")
            .build();
        responseHandler(response)
    }
}