
export class Session {

    constructor(params={}) {
        this.params = {
            userId : typeof params.userId === "undefined" ?  0 : params.userId,
            accountId : typeof params.accountId === "undefined" ? 0 :  params.accountId,
            sessionId : typeof params.sessionId === "undefined" ? null : params.sessionId
        }
    }

    isOffline() {return this.params.userId==0}
    getUserId() {return this.params.userId}
    getAccountId() {return this.params.accountId}
    getSessionId() {return this.params.sessionId}
    toJson(){return JSON.stringify(this.params)}

    isSameAccount(other) {
        return this.params.accountId == other.getAccountId()
    }

    createAccountNameSpace(ns) {
        const ua = new String(this.params.userId) + "." + new String(this.params.accountId);
        if(ns) return ns + "." + ua
        return ua
    }

    static fromJson(s){return new Session(JSON.parse(s))}
    static createOffline(){return new Session({})}
    static createUserSession(sessionId, uId,aId){
        const params = {
            userId:uId,
            accountId:aId,
            sessionId:sessionId
        }
        return new Session(params)
    }

}