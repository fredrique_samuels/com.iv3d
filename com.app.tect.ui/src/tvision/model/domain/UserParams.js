

export class UserParams {

    constructor(newParams={}) {
        this.params = newParams
    }

    getId(){return this.params.id}
    getEmail(){return this.params.email}
    getFirstName(){return this.params.firstName}
    getLastName(){return this.params.lastName}
    getDisplayName(){return this.params.displayName}
    getDlu(){return this.params.dlu}

    update(newParams) {
        this.params = Object.assign({}, this.params, newParams)
    }

    static createOffline() {
        return new UserParams({
            id:0,
            email:null,
            firstName:"Offline User",
            lastName:"",
            displayName:"Offline User",
            dlu:new Date()
        })
    }
}