

export class AccountParams {
    constructor(params={}) {
        const {id, name, dlu, activated, accountDataVersion} = params
        this.params = {id, name, dlu, activated, accountDataVersion}
    }

    getId(){return this.params.id}
    getName(){return this.params.name}
    getDlu(){return this.params.dlu}
    isActivated(){return this.params.activated}
    version(){return this.params.accountDataVersion}

    update(newParams) {
        this.params = Object.assign({}, this.params, newParams)
    }

    static createOffline() {
        return new AccountParams({
            id:0,
            name:null,
            activated:false,
            dlu:new Date(),
            accountDataVersion:0
        })
    }

}