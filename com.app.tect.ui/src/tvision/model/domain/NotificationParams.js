import AbstractParamsBuilder from "../../../platform/builder/AbstractParamsBuilder";

const NOTIFICATION_TYPE_MESSAGE = "message"
const NOTIFICATION_TYPE_CREDENTIAL = "credentials"
const NOTIFICATION_TYPE_TEXT = "text"

export class NotificationParams {

    constructor(params={}) {
        this.params = params
    }

    getId(){return this.params.id}
    getText(){return this.params.text}
    getType(){return this.params.type}
    getAlertLevel(){return this.params.alertLevel}
    getDate(){return this.params.date}

    toJson() {
        return JSON.stringify(this.params);
    }

    static fromJson(s) {
        return new NotificationParams(JSON.parse(s))
    }
}

export class NotificationBuilder extends AbstractParamsBuilder {

    constructor(cb) {
        super({
            type: NOTIFICATION_TYPE_TEXT,
            text: "",
            date: new Date(),
            alertLevel:null
        },cb)
    }

    setId(id){this.params.id=id;return this}
    setTypeText(){this.params.type=NOTIFICATION_TYPE_TEXT;return this}
    setTypeCredentials(){this.params.type=NOTIFICATION_TYPE_CREDENTIAL;return this}
    setTypeMessage(){this.params.type=NOTIFICATION_TYPE_MESSAGE;return this}

    setText(t){this.params.text = t;return this}
    setDate(d){this.params.date = d;return this}

    setWarning(){this.params.alertLevel='warning';return this}
    setError(){this.params.alertLevel='error';return this}

    buildModel(){return new NotificationParams(this.build())}

}
