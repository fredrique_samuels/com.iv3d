import React from 'react'
import * as tectui from "../../../../tectui/tectui";

export class LogoutForm extends React.Component {
    render() {

        const {layerId, tvisionContext, uicontext} = this.props

        let builder = new tectui.FormViewBuilder()
            .setCloseHandler(() => {uicontext.removeStackedComponent(layerId)})

        builder = this.buildTitleBox(builder, tvisionContext)

        builder = builder
            .createFormButton()
            .setLabel(tvisionContext.i8nMessage("tvision.session.logout.form.cancel"))
            .setCallback(() => tvisionContext.userAccountHandler().setSessionModeUser() )
            .commit()

        builder = builder.createSubmitButton()
            .setLabel(tvisionContext.i8nMessage("tvision.session.logout.form.confirm"))
            .commit()

        const submitHandler = (form, result) => {
            tvisionContext.userAccountHandler().logout()
            result.commit()
        }

        builder.createSubmitAction()
            .setCallback(submitHandler)
            .commit()


        const form = builder.buildView(this.props.uicontext);


        return (
            <div style={{border:"1px solid white", backgroundColor:"black", borderRadius:"4px", minWidth:"600px"}}>
                {form}
            </div>
        )
    }

    buildTitleBox(builder, tvisionContext) {
        return builder.createFormSection()
            .setTitle(tvisionContext.i8nMessage("tvision.session.logout.form.header"))
            .commit()
    }

}