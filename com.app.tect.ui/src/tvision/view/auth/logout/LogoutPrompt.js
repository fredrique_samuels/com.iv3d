import React from 'react'

import {LogoutForm} from './LogoutForm'
import * as tectui from "../../../../tectui/tectui";

export class LogoutPrompt extends React.Component {

    render() {

        const {tvisionContext} = this.props

        const notLogout = !tvisionContext.userAccountHandler().isSessionModeLogout();
        if(notLogout) {
            return null;
        }


        const uiContext = tvisionContext.getUiContext();
        const loginFormFactory  = (layerId, uicontext) => <LogoutForm layerId={layerId} tvisionContext={tvisionContext} uicontext={uicontext} />
        const mountedHandler  = (uicontext) => uicontext.addStackedComponent(loginFormFactory)

        const stackedComps = new tectui.ComponentStackViewBuilder()
            .setMountedHandler(mountedHandler)
            .buildView(uiContext);

        return (
            <div class="center-content-vh fill-parent " style={{top:"0px", position:"absolute", zIndex:"999"}}>
                {stackedComps}
            </div>
        )
    }



}