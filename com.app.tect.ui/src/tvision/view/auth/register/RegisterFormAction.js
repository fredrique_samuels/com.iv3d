import * as tectui from "../../../../tectui/tectui";
export class RegisterFormAction {

    constructor(tvisionContext, uicontext) {
        this.uicontext = uicontext
        this.tvisionContext = tvisionContext
    }

    run(form, formResult) {

        const t = new String(this.tvisionContext.i8nMessage("tvision.session.register.form.validation.loading")) + " ...";
        const loadingLayerId = tectui.ViewUtils.createStackedLoadingLayer(this.uicontext, t)

        const success = () => {
           this.uicontext.removeStackedComponent(loadingLayerId)
            formResult.setError("Testing ...").commit()
        }

        const error = (message, formErrors) => {
            this.uicontext.removeStackedComponent(loadingLayerId)
            formResult.setError(message)
            formResult.addAllFieldErrors(formErrors).commit()
        }
        const cbo = {success:success, error:error};

        const params = {
            email:form.dataMap["email"],
            password:form.dataMap["password"],
            verifyPassword:form.dataMap["verifyPassword"],
            firstName:form.dataMap["firstName"],
            lastName:form.dataMap["lastName"]
        }

        const userAccountHandler = this.tvisionContext.userAccountHandler();
        userAccountHandler.createNewUserAndAccount(params, cbo)

    }
}