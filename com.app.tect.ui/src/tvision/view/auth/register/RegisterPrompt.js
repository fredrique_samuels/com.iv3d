import React from 'react'

import {RegisterForm} from './RegisterForm'
import * as tectui from "../../../../tectui/tectui";

export class RegisterPrompt extends React.Component {

    render() {

        const {tvisionContext} = this.props

        const hide =!tvisionContext.userAccountHandler().isSessionModeRegister();
        if(hide) {
            return null;
        }

        const uiContext = tvisionContext.getUiContext();
        const loginFormFactory  = (layerId, uicontext) => <RegisterForm layerId={layerId} tvisionContext={tvisionContext} uicontext={uicontext} />
        const mountedHandler = (uicontext) => uicontext.addStackedComponent(loginFormFactory)

        const stackedComps = new tectui.ComponentStackViewBuilder()
            .setMountedHandler(mountedHandler)
            .buildView(uiContext);

        return (
            <div class="center-content-vh fill-parent " style={{top:"0px", position:"absolute", zIndex:"999"}}>
                {stackedComps}
            </div>
        )
    }



}