import React from 'react'
import * as tectui from "../../../../tectui/tectui";
import {Lock} from "../../../model/shared/Lock";
import {RegisterFormAction} from "./RegisterFormAction";

export class RegisterForm extends React.Component {
    render() {

        const {layerId, tvisionContext, uicontext} = this.props

        let builder = new tectui.FormViewBuilder()
            .setCloseHandler(() => {uicontext.removeStackedComponent(layerId)})

        builder = builder.createFormSection()
            .setTitle(tvisionContext.i8nMessage("tvision.session.register.form.heading"))
            .commit()

        builder = builder.createFormSection()
        builder = this.buildInputs(builder, tvisionContext)
        builder = builder.commit()
            .createFormButton()
            .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.button.continueoffline"))
            .setCallback(() => tvisionContext.userAccountHandler().logout() )
            .commit()

        builder = builder.createSubmitButton()
            .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.createaccount.button"))
            .commit()


        const lock = new Lock()
        const submitHandler = (form, formResult) => {
            if(lock.isLocked())return
            lock.lock()

            const t = new String(tvisionContext.i8nMessage("tvision.session.register.form.validation.loading")) + " ...";
            const loadingLayerId = tectui.ViewUtils.createStackedLoadingLayer(uicontext, t)

            const success = () => {
                uicontext.removeStackedComponent(loadingLayerId)
                formResult.setError("Testing ...").commit()
            }
            const error = (message, formErrors) => {
                uicontext.removeStackedComponent(loadingLayerId)
                formResult.setError(message)
                formResult.addAllFieldErrors(formErrors).commit()
            }

            const {dataMap} = form
            const params = {
                email:dataMap["email"],
                password:dataMap["password"],
                verifyPassword:dataMap["verifyPassword"],
                firstName:dataMap["firstName"],
                lastName:dataMap["lastName"]
            }
            tvisionContext.userAccountHandler().createNewUserAndAccount(
                params,
                {success:success, error:error}
            )
        }


        const action = new RegisterFormAction(tvisionContext, uicontext);
        builder.createSubmitAction()
            .setCallback((form, result) => {action.run(form, result)} )
            .commit()

        const form = builder.buildView(this.props.uicontext);

        return (
            <div style={{border:"1px solid white", backgroundColor:"black", borderRadius:"4px", minWidth:"600px"}}>
                {form}
            </div>
        )
    }

    buildInputs(builder, tvisionContext) {
        return builder

            .createTextInput()
                .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.input.firstname.label"))
                .setName('firstName')
                .commit()

            .createTextInput()
                .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.input.lastname.label"))
                .setName('lastName')
                .commit()

            .createEmailInput()
                .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.input.email.label"))
                .setName("email")

                .createAddonLeft()
                .setFaIcon('fa-envelope')
                .commit()

            .commit()

            .createPasswordInput()
                .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.input.password.label"))
                .setPlaceholder(tvisionContext.i8nMessage("tvision.session.register.form.input.password.label"))
                .setName("password")

                .createAddonLeft()
                .setFaIcon('fa-lock')
                .commit()
            .commit()

            .createPasswordInput()
                .setLabel(tvisionContext.i8nMessage("tvision.session.register.form.input.reenterpassword.label"))
                .setName("verifyPassword")

                .createAddonLeft()
                .setFaIcon('fa-lock')
                .commit()


            .commit()
    }
}