import React from 'react'
import * as tectui from "../../../../tectui/tectui";
import {Lock} from "../../../model/shared/Lock";
import {LoginFormAction} from "./LoginFormAction";

export class LoginFormView extends React.Component {
    render() {

        const {layerId, tvisionContext, uicontext} = this.props

        let builder = new tectui.FormViewBuilder()
            .setCloseHandler(() => {uicontext.removeStackedComponent(layerId)})

        builder = this.buildTitleBox(builder, tvisionContext)

        builder = builder.createFormSection()
        builder = this.buildInputs(builder, tvisionContext)
        builder = builder.commit()
            .createFormButton()
            .setLabel(tvisionContext.i8nMessage("tvision.session.login.continue.offline"))
            .setCallback(() => tvisionContext.userAccountHandler().logout() )
            .commit()

        builder = builder.createSubmitButton()
            .setLabel(tvisionContext.i8nMessage("tvision.session.login.button"))
            .commit()

        const loginAction = new LoginFormAction(tvisionContext, uicontext);
        builder.createSubmitAction()
            .setCallback((form, result) => {loginAction.run(form, result)} )
            .commit()

        const form = builder.buildView(this.props.uicontext);
        return (
            <div style={{border:"1px solid white", backgroundColor:"black", borderRadius:"4px", minWidth:"600px"}}>
                {form}
            </div>
        )
    }

    buildTitleBox(builder, tvisionContext) {
        return builder.createFormSection()
            .setTitle(tvisionContext.i8nMessage("tvision.session.login.header"))
            .commit()
    }

    buildInputs(builder, tvisionContext) {
        return builder
            .createEmailInput()
            .setLabel(tvisionContext.i8nMessage("tvision.session.login.input.username.label"))
            .setPlaceholder(tvisionContext.i8nMessage("tvision.session.login.input.username.label"))
            .setName("email")

            .createAddonLeft()
            .setFaIcon('fa-envelope')
            .commit()

            .commit()

            .createPasswordInput()
            .setLabel(tvisionContext.i8nMessage("tvision.session.login.input.password.label"))
            .setPlaceholder(tvisionContext.i8nMessage("tvision.session.login.input.password.label"))
            .setName("password")

            .createAddonLeft()
            .setFaIcon('fa-lock')
            .commit()

            .commit()
    }
}