import * as tectui from "../../../../tectui/tectui";

export class LoginFormAction {

    constructor(tvisionContext, uicontext) {
        this.uicontext = uicontext
        this.tvisionContext = tvisionContext
    }

    run(form, result) {

        const loadingLayerId = this.showLoadingBox()

        const success = () => {
            this.hideLoadingBox(loadingLayerId)
            result.commit()
        }

        const error = (message) => {
            this.hideLoadingBox(loadingLayerId)
            result.setError(message).commit()
        }

        const params = {
            email:form.dataMap["email"],
            password:form.dataMap["password"]
        }

        const userAccountHandler = this.tvisionContext.userAccountHandler();
        userAccountHandler.validateCredentials(params,{success, error })
    }

    showLoadingBox() {
        const t = new String(this.tvisionContext.i8nMessage("tvision.session.login.validation.loading")) + " ...";
        return tectui.ViewUtils.createStackedLoadingLayer(this.uicontext, t)
    }

    hideLoadingBox(id) {
        this.uicontext.removeStackedComponent(id)
    }
}