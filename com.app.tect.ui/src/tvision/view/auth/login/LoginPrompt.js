import React from 'react'

import {LoginFormView} from './LoginFormView'
import * as tectui from "../../../../tectui/tectui";

export class LoginPrompt extends React.Component {

    render() {

        const {tvisionContext} = this.props

        const notExpired = !tvisionContext.userAccountHandler().isSessionModeExpired();
        const notLogin = !tvisionContext.userAccountHandler().isSessionModeLogin();

        const shouldHide = notExpired && notLogin
        if(shouldHide) {
            return null;
        }


        const uiContext = tvisionContext.getUiContext();
        const loginFormFactory  = (layerId, uicontext) => <LoginFormView layerId={layerId} tvisionContext={tvisionContext} uicontext={uicontext} />
        const mountedHandler = (uicontext) => uicontext.addStackedComponent(loginFormFactory)

        const stackedComps = new tectui.ComponentStackViewBuilder()
            .setMountedHandler(mountedHandler)
            .buildView(uiContext);

        return (
            <div class="center-content-vh fill-parent " style={{top:"0px", position:"absolute", zIndex:"999"}}>
                {stackedComps}
            </div>
        )
    }



}