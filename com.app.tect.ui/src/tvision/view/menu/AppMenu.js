import React from 'react'
import MenuItem from './MenuItem'


export default class AppMenu extends React.Component {
  render() {
    const {tvisionContext, uicontext} = this.props
    const appsList = tvisionContext.applicationHandler().getAppList()
    const menuComps = appsList.map( app => <MenuItem app={app} /> )

    const shouldDisplayMenu = tvisionContext.applicationHandler().inDisplayModeMenu()
    const menuStyle = {
      // backgroundColor:"rgba(0,0,0,.4)",
      padding:"15px",
      display:shouldDisplayMenu?"block":"none"
    }

    return (
      <div class="fill-parent" style={menuStyle}>
        {menuComps}
      </div>
    )
    }
}
