import React from 'react'
import MenuItemIcon from './MenuItemIcon'

export default class MenuItem extends React.Component {
    render() {
        const { app } = this.props
        if(!app.isVisibleInMenu()) return null

        return (
            <div class="center-content-h inline-element panel-text" style={{width:"180px", height:"160px", margin:"10px"}}>
                <div style={{borderRadius:"4px", backgroundColor:"rgba(0,0,0,.4)", position:"relative",left:"55px", width:"70px", height:"70px"}}>
                    <MenuItemIcon icon={app.getIcon()} action={this.openApp.bind(this)} />
                </div>
                <div class="center-content-h text-outline-black" style={{fontSize:"12px", position:"relative",fontWeight:"bold", color:"white", paddingTop:"3px", height:"60px", width:"100%", textAlign:"center"}}>
                    {app.getTitle()}
                </div>
            </div>
        )
    }

    openApp() {
        const { app } = this.props
        app.open()
    }
}