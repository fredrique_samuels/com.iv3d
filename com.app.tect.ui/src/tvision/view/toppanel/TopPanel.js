import React from 'react'
import { connect } from 'react-redux'

import {CompanyCard} from './companycard/CompanyCard'
import {UserCard} from './usercard/UserCard'
import {ActivateAccountButton} from './buttons/ActivateAccountButton'
import {HomeButton} from './buttons/HomeButton'
import {MenuButton} from './buttons/MenuButton'
import {AppRestoreButton} from './buttons/AppRestoreButton'
import {StatusBar} from './statusbar/StatusBar'

export class TopPanel extends React.Component {
    render() {

        const {tvisionContext} = this.props
        const isOffline = tvisionContext.userAccountHandler().isSessionModeOffline()
        const userCardView = (
            <div class="inline-element" style={{backgroundColor: "rgba(255, 0,0,0)"}}>
                <UserCard tvisionContext={tvisionContext} />
            </div>
        )

        return (
            <div style={{display:"flex", position:"relative", width:"100%"}}>
                <div class="inline-element" style={{backgroundColor:"rgba(255, 0,0,0)"}} >
                    <CompanyCard />
                </div>
                <div class="inline-element" style={{display:"flex", flex:"1",backgroundColor:"rgba(255, 255,0,0)"}} >
                    <div style={{width:"100%", display:"flex"}}>
                        <div class="inline-element" style={{flex:"1"}} >
                            <StatusBar tvisionContext={tvisionContext} />
                            <div style={{width:"100%", height:"30px", display:"flex", justifyContent:"center"}}>
                                <ActivateAccountButton tvisionContext={tvisionContext} />
                                <HomeButton tvisionContext={tvisionContext} />
                                <MenuButton tvisionContext={tvisionContext}/>
                                <AppRestoreButton tvisionContext={tvisionContext}/>
                            </div>
                        </div>
                        { isOffline ? null : userCardView }
                    </div>
                </div>
            </div>
        )
    }
}