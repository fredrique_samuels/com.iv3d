import React from 'react'

import {UserProfilePic} from './UserProfilePic'

import {UserCardMenu} from './UserCardMenu'

export class UserCard extends React.Component {
    render() {
        const {tvisionContext} = this.props
        return (
            <div class="tvision-user-card">
                <div class="background-color-black-transparent" style={{height:"57px", padding:"1px",borderRadius:"0 0 0px 5px", boxSizing:"borderBox", position:"relative"}} >
                    <div class="inline-element" style={{padding:"2px", border:"1px solid slategrey", borderRadius:"4px", width:"55px", height:"55px", boxSizing:"borderBox"}}>
                        <UserProfilePic />
                    </div>
                </div>
                <UserCardMenu tvisionContext={tvisionContext}/>
            </div>
        )
    }
}