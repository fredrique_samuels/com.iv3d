import React from 'react'

export class UserProfilePic extends React.Component {
    render() {
        return (

            <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                <a>
                    <image x="0" y="0" height="100%" width="100%" xlinkHref="/res/images/icons/user.png" />
                </a>
            </svg>
        )
    }
}