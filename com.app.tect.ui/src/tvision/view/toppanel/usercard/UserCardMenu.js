import React from 'react'
import {LogOutInButton} from './../statusbar/LogOutInButton'

export class UserCardMenu extends React.Component {
    render() {
        const {tvisionContext} = this.props

        const newVar = {
            minWidth: "100px",
            justifyContent: "left",
            right: "2px",
            zIndex: "999",
            position: "absolute",
            padding: "3px 15px 3px 15px",
            border: "1px solid white"
        }

        return (
            <div class="tvision-user-card-menu background-color-black-transparent panel-text " style={newVar} >
                <LogOutInButton tvisionContext={tvisionContext} />
            </div>
        )
    }
}