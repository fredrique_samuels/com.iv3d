import React from 'react'
import * as TVisionConstants from "../../../TVisionConstants";

export class ActivateAccountButton extends React.Component {
    render() {
        const {tvisionContext} = this.props

        const css = {
            marginTop:"2px",
            marginRight:"15px",
            padding:"3px 5px 3px 5px",
            fontSize:"20px",
            color:"white",
            borderRadius:"4px",
            cursor:"pointer",
            position:"absolute",
            left:"70px"
        };

        const settingsHandler = tvisionContext.settingsHandler();
        const userAccountHandler = tvisionContext.userAccountHandler();

        const isOffline = userAccountHandler.isSessionModeOffline()
        const accountActivated = settingsHandler.getAccount().isActivated();

        if(accountActivated || isOffline) {
            return null;
        }

        const action = () => tvisionContext.applicationHandler().runApp("com.tvision.app.account.activation")
        return (
            <div class={"inline-element hover-shadow color-error"} style={css} onClick={action} >
                {tvisionContext.i8nMessage("tvision.desktop.apps.button.active.account")}
            </div>
        )
    }
}