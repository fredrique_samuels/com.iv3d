import React from 'react'

export class DashboardButton extends React.Component {
    render() {
        const {faIcon, isActiveHandler, actionHandler} = this.props

        const css = {
            width:"40px",
            display:"flex",
            justifyContent:"center",
            marginTop:"2px",
            marginRight:"15px",
            padding:"3px",
            fontSize:"20px",
            color:"white",
            borderRadius:"4px",
            cursor:"pointer"
        };

        const activeHandler = isActiveHandler();
        const classV = activeHandler?" background-color-primary":" background-color-black-transparent"
        return (
            <div onClick={actionHandler} class={"inline-element hover-shadow"  + classV} style={css}>
                <i onClick={actionHandler} class={"fa " + faIcon}></i>
            </div>
        )
    }
}