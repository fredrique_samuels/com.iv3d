import React from 'react'
import {DashboardButton} from './DashboardButton'

export class MenuButton extends React.Component {
    render() {
        const {tvisionContext} = this.props

        const actionHandler = () => tvisionContext.applicationHandler().setDisplayModeMenu()
        const isActiveHandler = () => tvisionContext.applicationHandler().inDisplayModeMenu()
        return <DashboardButton isActiveHandler={isActiveHandler} actionHandler={actionHandler} faIcon="fa-th" />
    }
}