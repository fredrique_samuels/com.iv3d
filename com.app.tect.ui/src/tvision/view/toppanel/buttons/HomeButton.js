import React from 'react'
import {DashboardButton} from './DashboardButton'

export class HomeButton extends React.Component {
    render() {
        const {tvisionContext} = this.props

        const actionHandler = () => tvisionContext.applicationHandler().setDisplayModeHome()
        const isActiveHandler = () => tvisionContext.applicationHandler().inDisplayModeHome()
        return <DashboardButton isActiveHandler={isActiveHandler} actionHandler={actionHandler} faIcon="fa-home" />
    }
}