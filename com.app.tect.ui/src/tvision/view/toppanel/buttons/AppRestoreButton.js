import React from 'react'
import {DashboardButton} from './DashboardButton'

export class AppRestoreButton extends React.Component {
    render() {
        const {tvisionContext} = this.props
        const actionHandler = () => tvisionContext.applicationHandler().setDisplayModeAppList()
        const isActiveHandler = () => tvisionContext.applicationHandler().inDisplayModeAppList()
        return <DashboardButton isActiveHandler={isActiveHandler} actionHandler={actionHandler} faIcon="fa-window-restore" />
    }
}
