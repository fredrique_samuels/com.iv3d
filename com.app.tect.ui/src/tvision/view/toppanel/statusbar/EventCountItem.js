import React from 'react'

export class EventCountItem extends React.Component {
    render() {
        const { iconClass, count, action } = this.props

        if(count<=0)
            return null

        const css = {
            fontSize:"16px",
            margin:"1px 15px 1px 3px",
            float:"left",
            cursor:"pointer",
            position:"relative"
        }
        const newVar = {top:"-5px", left:"90%", padding:"0px 2px 0px 2px", borderRadius:"4px", fontSize:"11px", position:"absolute"};
        return (
            <div class="" style={css} onClick={() => {if(action) action()} } >
                <span class={iconClass} ></span>
                <div class="background-color-error" style={newVar}><span>{count}</span></div>
            </div>
        )
    }
}