import React from 'react'

export class RegisterButton extends React.Component {
    render() {
        const { tvisionContext } = this.props
        const css = {
            fontSize:"16px",
            float:"left",
            cursor:"pointer",
            position:"relative"
        }

        return (
            <div style={css} onClick={() => tvisionContext.userAccountHandler().setSessionModeRegister()} >
                <span class="hover-text-color-primary fa fa-user" >{tvisionContext.i8nMessage("tvision.desktop.statusbar.register")}</span>
            </div>
        )
    }
}