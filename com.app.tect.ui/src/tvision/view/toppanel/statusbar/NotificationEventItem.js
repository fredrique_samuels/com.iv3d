import React from 'react'

import {EventCountItem} from './EventCountItem'

import {PACKAGE_ID} from "../../../../apps/com.tvision.app.notifications/com.tvision.app.notifications";

export class NotificationEventItem extends React.Component {
    render() {
        const {tvisionContext} = this.props

        const notifications = tvisionContext.notificationHandler().notifications()
        const action = () => tvisionContext.applicationHandler().runApp(PACKAGE_ID)

        return <EventCountItem iconClass={"event-icon-flag"} count={notifications.length} action={action}/>
    }
}