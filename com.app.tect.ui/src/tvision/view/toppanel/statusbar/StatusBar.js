import React from 'react'
import {EventCountItem} from './EventCountItem'
import {NotificationEventItem} from './NotificationEventItem'
import {RegisterButton} from './RegisterButton'
import {LogOutInButton} from './LogOutInButton'
import * as TVisionConstants from "../../../TVisionConstants";

export class StatusBar extends React.Component {
    render() {
        const {tvisionContext} = this.props

        const notificationsView = <NotificationEventItem  tvisionContext={tvisionContext} />

        const isOffline = tvisionContext.userAccountHandler().isSessionModeOffline();

        const user = tvisionContext.settingsHandler().getUser();
        const session = tvisionContext.userAccountHandler().getSession();

        const firstName = user.getFirstName()
        const accountName = user.getLastName()
        const displayName = firstName ? firstName : session.getUserId()

        const loginView = (
            <div class="center-content-v" style={{marginLeft:"15px", float:"right", height:"100%"}} >
                <LogOutInButton tvisionContext={tvisionContext} />
            </div>
        )

        const registerView = (
            <div class="center-content-v" style={{marginLeft:"15px", float:"right", height:"100%"}} >
                <RegisterButton tvisionContext={tvisionContext} />
            </div>
        )
        const accountNameView = (
            <div style={{display:"flex", justifyContent:"right"}}>
                {accountName}
            </div>
        )

        return (
            <div class="background-color-black-transparent panel-text" style={{padding:"0px 10px 0px 10px", color:"white", width:"100%", height:"40px"}}>
                <div class="center-content-v" style={{float:"left", height:"100%"}} >
                    {notificationsView}
                </div>
                {isOffline?registerView:null}
                {isOffline?loginView:null}
                <div class="center-content-v" style={{float:"right", height:"100%"}} >
                    <div>
                        <div style={{display:"flex", justifyContent:"right"}}>
                            {displayName}
                        </div >
                        { accountName ? accountNameView : null }
                    </div>
                </div>

            </div>
        )
    }
}