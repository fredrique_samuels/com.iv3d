import React from 'react'

import {PACKAGE_ID} from "../../../../apps/com.tvision.app.notifications/com.tvision.app.notifications";

export class LogOutInButton extends React.Component {
    render() {
        const { tvisionContext } = this.props
        const css = {
            fontSize:"16px",
            float:"left",
            cursor:"pointer",
            position:"relative"
        }

        const login = (
            <div style={css} onClick={() => tvisionContext.userAccountHandler().setSessionModeLogin()} >
                <span class="hover-text-color-primary fa fa-sign-in" > {tvisionContext.i8nMessage("tvision.desktop.statusbar.signin")}</span>
            </div>
        )

        const logout = (
            <div style={css} onClick={() => tvisionContext.userAccountHandler().setSessionModeLogout()} >
                <span class="hover-text-color-primary fa fa-sign-out" > {tvisionContext.i8nMessage("tvision.desktop.statusbar.signout")}</span>
            </div>
        )

        const signedOut = tvisionContext.userAccountHandler().isSessionModeOffline() ;
        return signedOut ? login : logout
    }
}