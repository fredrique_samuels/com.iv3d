import React from 'react'
import {LogoBox} from './LogoBox'

export class CompanyCard extends React.Component {
    render() {
        return (
            <div class="background-color-black-transparent inline-element" style={{width:"57px", height:"57px", padding:"1px",borderRadius:"0 0 5px 0px", boxSizing:"borderBox"}} >
                <LogoBox />
            </div>
        )
    }
}