import React from 'react'

export class LogoBox extends React.Component {
    render() {
        return (
            <div class="inline-element" style={{border:"1px solid slategrey",borderRadius:"4px",  padding:"2px", width:"55px", height:"55px", boxSizing:"borderBox"}}>
                <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <a>
                        <image x="0" y="0" height="100%" width="100%" xlinkHref="/res/images/tt_logo.svg" />
                    </a>
                </svg>
            </div>
        )
    }
}
