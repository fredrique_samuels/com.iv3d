import React from 'react'
import MenuItemIcon from '../menu/MenuItemIcon'

class AppView extends React.Component {
  render() {
    const { process, uicontext, processIndex, tvisionContext } = this.props

    const selected = process.visible
    const appListMode = tvisionContext.applicationHandler().inDisplayModeAppList()
    const appMode = tvisionContext.applicationHandler().inDisplayModeApp()


    let appCssStyle = {
      position:"relative"
    }

    let appSelectCss = {
      display:"none"
    }

    let topCss = {
      position:"relative",
      display:"none"
    }

    if(appMode && selected) {
      topCss = Object.assign({}, topCss, {
          display:"block",
          zIndex:""+processIndex
      })
    }

    if(appListMode) {
      appCssStyle = Object.assign({}, appCssStyle, {
          transform:"scale(.75 , .75) translate(-15%, 0%)"
      })
      appSelectCss = Object.assign({}, appSelectCss, {display:"block"})
      topCss = Object.assign({}, topCss, {display:"block",height:"100%"})
    }

    return (
      <div class="fill-parent app-cntr" style={topCss} >
        <div class="fill-parent fade-in-element" style={appCssStyle} >
          {process.app.createView()}
          <button class="applist-select-button" onClick={this.select.bind(this)} style={appSelectCss} ></button>
        </div>

        {appListMode?this.getAppTitleBox(process.app.getTitle()):null}
      </div>
    )
  }

  select() {
    const { selectAppCallback, process } = this.props
    selectAppCallback(process)
  }

  getAppTitleBox(title) {

    const { process } = this.props

    const css = {
      position:"absolute",
      left:"75%",
      top:"50%",
      width:"25%",
      color:"white",
      textAlign:"center",
      textShadow:"0px 0px 10px black",
    }
    return (<div class="inline-element" style={css} >
      <div style={{height:"30px"}}>
          <i class="fa fa-close fa-1x" style={{paddingRight:"5px"}} onClick={this.stopProcess.bind(this)}></i>{process.app.getTitle()}
      </div>
      <div style={{width:"100%", display:"flex", justifyContent:"center"}}>
          <div style={{paddingBottom:"20px", height:"70px", width:"50px"}} >
              <MenuItemIcon icon={process.app.getIcon()} />
          </div>
      </div>
    </div>)
  }

  stopProcess() {
      const { process, tvisionContext } = this.props
      tvisionContext.applicationHandler().destroyProcess(process)
  }

}

export default class AppSection extends React.Component {

  componentWillMount() {
  }

  render() {
    const { tvisionContext, uicontext } = this.props
    const appMode = tvisionContext.applicationHandler().inDisplayModeApp()
    const appListMode = tvisionContext.applicationHandler().inDisplayModeAppList()
    const processList = tvisionContext.applicationHandler().getProcessList();

    let viewStyle = {
      display:"none",
      overflow:"auto",
      position:"relative"
    }
    if(appMode || appListMode) {
        if(processList.length==0) {
          return (<div class="fill-parent" style={{textShadow:"0px 0px 10px black",color:"white", backgroundColor:"rgba(0,0,0,.4)",display:"flex", alignItems:"center",textAlign:"center", justifyContent:"center"}}>
              {tvisionContext.i8nMessage("tvision.desktop.apps.noapps")}
          </div>)
        }

        viewStyle  = Object.assign({}, viewStyle, {display:"block"})
    }

    const processViews = processList.map( (process, index) =>  <AppView tvisionContext={tvisionContext} uicontext={uicontext} selectAppCallback={this.selectApp.bind(this)} process={process} processIndex={index}/> )
    return (<div class="fill-parent" style={viewStyle}>
      {processViews}
    </div>)
  }

  selectApp(process) {
      const { tvisionContext } = this.props
      tvisionContext.applicationHandler().setProcessVisible(process)
  }
}
