import React from 'react'
import { connect } from 'react-redux'

import LayerComponent from '../../../tectui/framework/comps/layers/view/LayerComponent'

import AppSection from './AppsView'
import AppMenu from './../menu/AppMenu'

const MAIN_LAYER_COMPONENT = "component.model.layermanager.main"

class ContentContainer extends React.Component {
    render() {

        const {tvisionContext} = this.props

        const style={
            width:"100%",
            height:"100%",
            boxSizing:"border-box",
            margin:"10px"
        }

        const apps = (
            <div class="fill-parent">
                <LayerComponent modelId={MAIN_LAYER_COMPONENT} mountedCallback={this.onLayerMounted.bind(this)}/>
            </div>
        )

        return (
            <div class="inline-element" style={style} >
                {tvisionContext.applicationHandler().inDisplayModeMenu()?<AppMenu tvisionContext={tvisionContext}  uicontext={this.props.uicontext}/>:null}
                <AppSection tvisionContext={tvisionContext}  uicontext={this.props.uicontext} />
            </div>
        )
    }

    onLayerMounted(id){}
}

export class MainContent extends React.Component {
    render() {

        const {tvisionContext} = this.props

        const css = {
            padding:"10px",
            boxSizing:"border-box",
            width:"calc(100% - 10px)",
            height:"calc(100% - 100px)"
        }

        return (
            <div style={css}>
                <ContentContainer tvisionContext={tvisionContext} uicontext={this.props.uicontext} />
            </div>
        )
    }
}