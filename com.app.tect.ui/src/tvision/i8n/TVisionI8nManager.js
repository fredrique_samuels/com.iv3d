import messages_en from './messages_en.json'
import * as tectui from "../../tectui/tectui";

export class TVisionI8nManager extends tectui.I8nManager {
    constructor(uicontext) {
       super(uicontext, new tectui.I8nMessageSource(messages_en))
    }
}