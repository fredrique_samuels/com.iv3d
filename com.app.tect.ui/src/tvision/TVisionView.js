import React from 'react'
import { connect } from 'react-redux'

import {setDispatcher} from "../tectui/tectui";
import TVisionContext from "./model/TVisionContext";


import {TopPanel} from './view/toppanel/TopPanel'
import {MainContent} from './view/appsection/MainContent'
import {LoginPrompt} from './view/auth/login/LoginPrompt'
import {LogoutPrompt} from './view/auth/logout/LogoutPrompt'
import {RegisterPrompt} from './view/auth/register/RegisterPrompt'
import {UiContext} from "../tectui/tectui";

@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export class TVisionView extends React.Component {

  render() {
      const tvisionContext = new TVisionContext(this.uicontext, this);

    return (
        <div class=" back-panel fill-parent" style={{backgroundSize:"100% 100%", backgroundRepeat:"no-repeat", backgroundImage:"url(/res/images/blue-wallpaper-6.jpg)", position:"relative"}}>
            <TopPanel tvisionContext={tvisionContext}/>
            <MainContent uicontext={this.uicontext} tvisionContext={tvisionContext} />
            <LoginPrompt uicontext={this.uicontext} tvisionContext={tvisionContext} />
            <LogoutPrompt uicontext={this.uicontext} tvisionContext={tvisionContext} />
            <RegisterPrompt uicontext={this.uicontext} tvisionContext={tvisionContext} />
        </div>
    )
  }

  constructor(){
      super()
      this.uicontext = new UiContext()
  }

  componentWillMount() {

      setDispatcher(this.props.dispatch)

      const session = TVisionContext.loadLastActiveSession(this.uicontext)
      const sessionState = TVisionContext.loadSessionState(this.uicontext, session);

      this.state = sessionState

      TVisionContext.loadServerState(this, ()=>{}, this.uicontext, session)
  }

}
