

export class SuccessFailureMessage {

    constructor(success, msg, data=null) {
        this.success = success
        this.msg = msg
        this.data = data
    }

    failed() {return !this.success}
    succeeded() {return this.success}
    message(){return this.msg}
    data(){return this.data}


    static createFailed(msg="", data=null){return new SuccessFailureMessage(false, msg, data)}
    static createSuccess(msg="", data=null){return new SuccessFailureMessage(true, msg, data)}
}