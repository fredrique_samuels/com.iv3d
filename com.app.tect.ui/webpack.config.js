
const path = require('path');
const webpack = require('webpack');

const prod = process.argv.indexOf('-p') !== -1;

module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    app: prod ? './sites/tvision_prod.js' : './sites/tvision_dev.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'tvision.bundle.js',
  },
  // resolve: {
  //       modules: [
  //           path.resolve('./src'),
  //           path.resolve('./node_modules')
  //       ]
  // },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react'],
            plugins: ['react-html-attrs', 'transform-decorators-legacy2', 'transform-class-properties']
          },
        }],
      },

      // Loaders for other file types can go here

    ],
  },
  devServer: {
    contentBase: path.resolve(__dirname, './src'),
    port: 58090
  }
};
