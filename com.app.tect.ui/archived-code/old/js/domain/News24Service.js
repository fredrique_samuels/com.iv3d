import * as NotificationActions from './../notifications/NotificationActions'
import * as Notification from './../notifications/Notification'

const DemoFilters = ['crime', 'earthquake', 'robbery']

// const capeTown = new DotOverlay(2, -33.9249, 18.4241)
// const newYork = new DotOverlay(3, 40.7128, -74.0059)
// const sydney = new DotOverlay(4, -33.865143, 151.209900)


const DemoArticles = []
DemoArticles.push({
  title:"Cape Town Event",
  summary:"Summary 1",
  location:"Cape Town",
  latlng : {
      'latitude':-33.9249,
      'longitude' : 18.4241
    }
  })

DemoArticles.push({
  title:"New York Event",
  summary:"Summary 1",
  location:"New York",
  latlng : {
      'latitude' : 40.7128,
      'longitude' : -74.0059
    }
  })
DemoArticles.push({
  title:"Sydney Event",
  summary:"Summary 1",
  location:"Sydney",
  latlng : {
      'latitude' : 40.7128,
      'longitude' : -74.0059
    }
  })

export default class News24Service {
  constructor(dispatch) {
      this.dispatch = dispatch
      this.timer = null
      this.uid = Math.floor(Math.random()*100)+1
  }

  stop() {
    if(this.timer) {
      clearInterval(this.timer)
    }
  }

  start() {
      this.timer = setTimeout(this.process.bind(this), 5000)
  }

  process() {
    console.log("Checking for new news" + this.uid);
    this.dispatch
    this.timer = setTimeout(this.process.bind(this), 5000)
  }
}
