import React from 'react'
import { connect } from 'react-redux'

class ClearableContentHandler {
    createContent(layerModel) {
        const style={
            backgroundColor:"red"
        }

        return (<div class="fill-parent" style={style}>
            Click the clear button at hte bottom of the page
        </div>)
    }
}

class DemoContent extends React.Component {
    render() {
        const style={
            position:"relative",
            top:"0%",
            left:"0%",
            width:"100%",
            height:"100%",
            backgroundColor:"blue"
        }

        return (<div style={style}>
            Click the button to create a clearable layer.
            <button onClick={this.createNewLayer.bind(this)}>Run</button>
        </div>)
    }

    createNewLayer() {
        const {layerModel} = this.props
        const { uicontext } = layerModel.contentParams

        var contentParams = {
            contentHandlerFactory: new ClearableContentHandler(),
            layoutParams: {
                position:"relative",
                top:"5%",
                left:"5%",
                width:"40%",
                height:"4%",
                backgroundColor:"blue"
            }
        };
        uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:true})
    }
}

export class TestLayerComponent {
  run(uicontext) {
      var contentParams = {
          contentHandlerFactory: {createContent:(layerModel) => <DemoContent layerModel={layerModel} />},
          layoutParams: {
              position:"relative",
              top:"10%",
              left:"10%",
              width:"80%",
              height:"80%",
              backgroundColor:"blue"
          }
      };
      uicontext.layerComponentManager().addLayer(contentParams)
  }
}
