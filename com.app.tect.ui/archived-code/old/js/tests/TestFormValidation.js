import React from 'react'
import { connect } from 'react-redux'

export class TestFormSubmit {
  run(uicontext) {
      uicontext.formComponentManager().createLayeredForm(this.createForm())
  }

  validateForm(form) {
    const { dataArray, data , dataMap} = form
    const errors = []

    const username = dataMap["username"]
    if(username) {
      if(username!="Joe Parmer") {
          errors.push({field:"username", message:"User name must be 'Joe Parmer'"})
      }
    }
    else {
      errors.push({field:"username", message:"No User name provided"})
    }

    const password = dataMap["password"]
    if(password) {
      if(password!="p@ssword") {
          errors.push({field:"password", message:"Password must be 'p@ssword'"})
      }
    }
    else {
      errors.push({field:"password", message:"No Password provided"})
    }

    return {
      passed:errors.length==0,
      errors: errors
    }
  }

  submitForm(form) {
    console.log("Submitting form!!");
    return true
  }

  createForm() {
    const formDescription = {
        validation : {
          type:"callback",
          callback : this.validateForm.bind(this)
        },
        submit : {
          type: "callback",
          callback : this.submitForm.bind(this)
        },
        actions : [
          {
            title : "Login",
            type : "form.action.type.submit"
          }
        ],
        inputs : [
          {
            type :'form.input.type.text',
            title : 'User Name',
            placeholder : 'User Name',
            exampleText : 'Enter user name.',
            helpText : 'Help',
            name: "username",
            addonLeft:{faIcon:"fa-user"},
          },
          {
            type :'form.input.type.password',
            title : 'Password',
            addonLeft:{faIcon:"fa-lock"},
            placeholder:"Password",
            name: "password"
          }
        ]
    }

  return formDescription
}
}
