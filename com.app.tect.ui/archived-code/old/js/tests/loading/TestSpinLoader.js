import React from 'react'
import { connect } from 'react-redux'
import LoadingDialogCloser from "../../framework/form/loading/LoadingDialogCloser";
import {Timeout} from "../../common";

class DemoLoadingController {

    start(loadingModel) {
        const loadingDialogCloser = new LoadingDialogCloser(loadingModel);
        new Timeout(2500, loadingDialogCloser.close.bind(loadingDialogCloser))
    }
}


export default class TestSpinLoader {

  run(uicontext) {
      uicontext.formComponentManager().createLayeredSpinLoader(new DemoLoadingController())
  }

}
