import React from 'react'
import TableTestData from './TableTestData'
import {AbstractActionConfigBuilder} from "../../../../tectui/framework/comps/action/builder/AbstractActionConfigBuilder";
import Icon from "../../../../tectui/framework/comps/icon/builder/Icon";

export class TableTestActions {
    run(uicontext) {
        uicontext.tableManager().createLayeredTable(this.createDemoTableViewParams())
    }

    createDemoTableViewParams() {
        const testData = new TableTestData();
        const data = testData.columnActionDataRHA()
        const metaData = testData.singlePageMetaData()

        return Object.assign( {}, data, metaData )
    }

}
