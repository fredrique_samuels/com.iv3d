import React from 'react'

import {CELL_DISPLAY_TYPE_BOOLEAN} from "../../framework/table/TableConstants";
import {CELL_DISPLAY_TYPE_STATUS} from "../../framework/table/TableConstants";
import {CELL_DISPLAY_TYPE_IMAGE} from "../../framework/table/TableConstants";
import * as TableConstants from "../../framework/table/TableConstants";
import {Icon} from "../../../../tectui/framework/comps/icon/builder/Icon";
import {AbstractActionConfigBuilder} from "../../../../tectui/framework/comps/action/builder/AbstractActionConfigBuilder";
import TableViewBuilder from "../../framework/table/TableViewBuilder";

export default class TableTestData {
    dataTypesDataRH(){
        const builder = this.dataTypesBuilder();
        return builder.buildConfig()
    }
    dataTypesBuilder() {

        const HEADING_KEY = "KEY"
        const HEADING_VALUE = "VALUE"

        return new TableViewBuilder()

                .createHeading(HEADING_KEY)
                .setInfo("This is heading 1")
                .commit()

                .createHeading(HEADING_VALUE)
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Some Text")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("$101")
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Some Text")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("$101")
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Boolean False")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("FALSE")
                    .setDisplayType(CELL_DISPLAY_TYPE_BOOLEAN)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Boolean True")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("TRUE")
                    .setDisplayType(CELL_DISPLAY_TYPE_BOOLEAN)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Boolean 0")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("0")
                    .setDisplayType(CELL_DISPLAY_TYPE_BOOLEAN)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Boolean 1")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("1")
                    .setDisplayType(CELL_DISPLAY_TYPE_BOOLEAN)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Boolean OFF")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("OFF")
                    .setDisplayType(CELL_DISPLAY_TYPE_BOOLEAN)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Boolean ON")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("ON")
                    .setDisplayType(CELL_DISPLAY_TYPE_BOOLEAN)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("SUCCESS")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue({status:TableConstants.CELL_STATUS_SUCCESS})
                    .setDisplayType(CELL_DISPLAY_TYPE_STATUS)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("WARNING")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue({status:TableConstants.CELL_STATUS_WARNING})
                    .setDisplayType(CELL_DISPLAY_TYPE_STATUS)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("ERROR")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue({status:TableConstants.CELL_STATUS_ERROR})
                    .setDisplayType(CELL_DISPLAY_TYPE_STATUS)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("STOPPED")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue({status:TableConstants.CELL_STATUS_STOPPED})
                    .setDisplayType(CELL_DISPLAY_TYPE_STATUS)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("WAITING")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue({status:TableConstants.CELL_STATUS_WAITING})
                    .setDisplayType(CELL_DISPLAY_TYPE_STATUS)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Status List")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue([{status:"ERROR"},{status:"SUCCESS"},{status:"ERROR"},{status:"WAITING"}])
                    .setDisplayType(CELL_DISPLAY_TYPE_STATUS)
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Long Text")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue("Hello this ia a very long text1. Hello this ia a very long text1. Hello this ia a very long text1. Hello this ia a very long text1. Hello this ia a very long text1. Hello this ia a very long text1.")
                    .commit()
                .commit()

                .createRow()
                    .createColumn(HEADING_KEY)
                    .setValue("Image")
                    .commit()

                    .createColumn(HEADING_VALUE)
                    .setValue({
                        url:"https://fredriquesamuels.github.io/dashboard-ui/res/tt_logo_entry_animated.svg"
                     })
                    .setDisplayType(TableConstants.CELL_DISPLAY_TYPE_IMAGE)
                    .commit()
                .commit()
    }

    columnScrollDataRH(valuePrefix="") {
        return {
            actions:[],
            headings:[
                {
                    key:"heading.1",
                    display:"Key",
                    info:{
                        description:"This is heading 1"
                    },
                },
                {
                    key:"heading.2",
                    display:"Column1"
                },
                {
                    key:"heading.3",
                    display:"Column2"
                },
                {
                    key:"heading.4",
                    display:"Column3"
                },
                {
                    key:"heading.5",
                    display:"Column4"
                },
                {
                    key:"heading.6",
                    display:"Column5"
                },
                {
                    key:"heading.7",
                    display:"Column6"
                },
                {
                    key:"heading.8",
                    display:"Column7"
                },
                {
                    key:"heading.9",
                    display:"Column8"
                },
                {
                    key:"heading.10",
                    display:"Column9"
                }
            ],
            rows:[
                {display:[
                    {
                        key: "heading.1",
                        value: valuePrefix + "Some Text"
                    },
                    {
                        key: "heading.2",
                        value: valuePrefix + "c1"
                    },
                    {
                        key: "heading.3",
                        value:valuePrefix +  "c2"
                    },
                    {
                        key: "heading.4",
                        value:valuePrefix +  "c3"
                    },
                    {
                        key: "heading.5",
                        value:valuePrefix +  "c4"
                    },
                    {
                        key: "heading.6",
                        value:valuePrefix +  "c5"
                    },
                    {
                        key: "heading.7",
                        value:valuePrefix +  "c5"
                    },
                    {
                        key: "heading.8",
                        value:valuePrefix +  "c5"
                    },
                    {
                        key: "heading.9",
                        value:valuePrefix +  "c5"
                    },
                    {
                        key: "heading.10",
                        value:valuePrefix +  "c5"
                    }
                ]
                }
            ]
        }
    }

    columnActionDataRHA(valuePrefix="") {
        return {
            actions: [
                new AbstractActionConfigBuilder()
                    .setLabel("View")
                    .setIcon(Icon.createForFontAwesome("fa-eye"))
                    .setAction((data, uicontext) => {
                        const helpBoxParams = {
                            heading:"Bio Info",
                            graphics: <i class="fa fa-user fa-2x color-primary-text"></i>,
                            text: JSON.stringify(data)
                        }
                        uicontext.formComponentManager().createLayeredHelpBox(helpBoxParams)
                    })
                    .build()
            ],
            headings:[
                {
                    key:"name",
                    display:"Name",
                    info:{
                        description:"This is heading 1"
                    },
                },
                {
                    key:"age",
                    display:"Age"
                }
            ],
            rows:[
                {
                    data:{
                        name:"John",
                        age:33
                    },
                    display:[
                        {
                            key: "name",
                            value: valuePrefix + "John"
                        },
                        {
                            key: "age",
                            value: valuePrefix + "33"
                        }
                    ]
                },
                {
                    data:{
                        name:"Peter",
                        age:50
                    },
                    display:[
                        {
                            key: "name",
                            value: valuePrefix + "Peter"
                        },
                        {
                            key: "age",
                            value: valuePrefix + "50"
                        }
                    ]
                }
            ]
        }
    }

    singlePageMetaData() {
        return {
            pageNumber:1,
            totalPages:1,
            prevPage:null,
            nextPage:null,
        }
    }

    pagedData(pageNum, totalPages, dataSource) {
        return Object.assign({},{
            pageNumber:pageNum,
            totalPages:totalPages,
            prevPage:pageNum > 1 ? function(onDoneCallback){
                const data = this.pagedData(pageNum - 1, totalPages, dataSource)
                onDoneCallback(data)
            }.bind(this) : null,
            nextPage:pageNum < totalPages ? function(onDoneCallback){
                const data = this.pagedData(pageNum + 1, totalPages, dataSource)
                onDoneCallback(data)
            }.bind(this) : null
        },dataSource(pageNum - 1))
    }

    paginatedTableData(dataArray=[]) {
        return this.pagedData(1,dataArray.length, (index)=>dataArray[index])
    }

}