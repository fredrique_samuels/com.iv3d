import React from 'react'
import TableTestData from './TableTestData'

export class TestTableBasic {
    run(uicontext) {
        new TableTestData().dataTypesBuilder().buildToLayer(uicontext)
    }

    createDemoTableViewParams() {
        const testData = new TableTestData();
        const data = testData.dataTypesDataRH()
        const metaData = testData.singlePageMetaData()
        return Object.assign( {}, data, metaData )
    }

}
