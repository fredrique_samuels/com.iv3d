import React from 'react'
import TableTestData from './TableTestData'

export class TestTablePaging {
    run(uicontext) {
        uicontext.tableManager().createLayeredTable(this.createDemoTableViewParams())
    }

    createDemoTableViewParams() {
        const testData = new TableTestData();
        const data = [
            testData.columnScrollDataRH("Page 1 "),
            testData.columnScrollDataRH("Page 2 "),
            testData.columnScrollDataRH("Page 3 "),
            testData.columnScrollDataRH("Page 4 ")
        ]
        return testData.paginatedTableData(data)
    }

}
