import React from 'react'

import TableTestData from './TableTestData'

export class TestTableColumnScroll {
    run(uicontext) {
        uicontext.tableManager().createLayeredTable(this.createDemoTableViewParams())
    }

    createDemoTableViewParams() {
        const testData = new TableTestData();
        const data = testData.columnScrollDataRH()
        const metaData = testData.singlePageMetaData()
        return Object.assign( {}, data, metaData )
    }

}
