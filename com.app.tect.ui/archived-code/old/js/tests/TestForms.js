import React from 'react'
import { connect } from 'react-redux'

import FormView from '../framework/FormComponent'
import * as FormConstants from '../framework/form/FormConstants'


class FormContentFactory {
    createContent(layerModel) {
        return <FormView form={layerModel.contentParams.form} layerModel={layerModel} />
    }
}

export class TestFormBasic {
  run(uicontext) {
      var contentParams = {
          contentHandlerFactory: new FormContentFactory(),
          layoutParams: {
              position:"relative",
              top:"10%",
              left:"10%",
              width:"80%",
              maxHeight:"80%",
              backgroundColor:"blue",
              overflow:"auto",
              backgroundColor:"rgba(0,0,0,.7)",
          },
          form : this.createForm()
      };
      uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:true})
  }

  createForm() {
      const formDescription = {
          inputs : [
            {
              type :'form.input.type.text',
              title : 'Text Input',
              placeholder : 'Placeholder',
              exampleText : 'Text Input',
              helpText : 'Help',
              errorText : 'Please enter a valid value',
              name: "text.input1",
              value: "text.input1"
            },
            {
              type :'form.input.type.text',
              title : 'Text Addon Left',
              addonLeft:{faIcon:"fa-user"},
              placeholder:"This is a placeholder",
              name: "text.input2",
              value: "text.input2"
            },
            {
              type :'form.input.type.text',
              title : 'Text Addon Right',
              addonRight:{text:".00"},
              placeholder:"This is a placeholder",
              name: "text.input3",
              value: "text.input3"
            },
            {
              type :'form.input.type.text',
              title : 'Text Addon Both',
              addonRight:{text:".00"},
              addonLeft:{faIcon:"fa-user"},
              placeholder:"This is a placeholder",
              name: "text.input4",
              value: "text.input4"
            },
            {
              type :'form.input.type.text',
              title : 'Text Data List of Browsers',
              placeholder:"Enter browser Name",
              name: "text.browser",
              dataList:["Internet Explorer","Firefox","Chrome","Opera","Safari"]
            },
            {
              type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
              title : "Form Section Inputs",
              inputs : [
                  {
                    type :'form.input.type.password',
                    title : 'Password',
                    exampleText : 'Text Input',
                    helpText : 'Help',
                    errorText : 'Please enter a valid value',
                    name: "textarea.input",
                    required:true,
                    addonLeft:{faIcon:"fa-lock"},
                    value:"Text Input"
                  },
                  {
                    type :'form.input.type.textarea',
                    title : 'Text Area',
                    exampleText : 'Text Input',
                    helpText : 'Help',
                    errorText : 'Please enter a valid value',
                    name: "textarea.input",
                    required:true,
                    value:"textarea.input"
                  },
                  {
                    type :'form.input.type.multiselect',
                    title : 'Multi Select',
                    helpText : 'Help',
                    errorText : 'Please enter a valid value',
                    name: "multiselect.input",
                    value:["ORANGE"],
                    options : [
                      {display:"Apple", value: "APPLE"},
                      {display:"Orange", value: "ORANGE"}
                    ]
                  },
                  {
                    type :'form.input.type.singleselect',
                    title : 'Single Select',
                    helpText : 'Help',
                    errorText : 'Please enter a valid value',
                    name: "singleselect.input",
                    value:"TRAIN",
                    options : [
                      {display:"Car", value: "CAR"},
                      {display:"Train", value: "TRAIN"},
                      {display:"Bus", value: "BUS"}
                    ]
                  },
                  {
                    type :'form.input.type.dropdownselect',
                    title : 'Dropdown Select',
                    helpText : 'Help',
                    name: "dropdownselect.input",
                    value: "TRAIN",
                    options : [
                      {display:"Car", value: "CAR"},
                      {display:"Train", value: "TRAIN"},
                      {display:"Bus", value: "BUS"}
                    ]
                  },
                  {
                    type :'form.input.type.file',
                    title : 'File Select',
                    helpText : 'Help',
                    errorText : 'Please enter a valid value',
                    name: "file.input"
                  },
                  {
                    type :'form.input.type.color',
                    title : 'Color Select',
                    helpText : 'Help',
                    name: "color.input",
                    value: "#bd001f"
                  },
                  {
                    type :'form.input.type.url',
                    title : 'Url Select',
                    helpText : 'Help',
                    name: "url.input"
                  },
                  {
                    type :'form.input.type.email',
                    title : 'Email Select',
                    helpText : 'Help',
                    name: "email.input"
                  },
                  {
                      type: FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT,
                      text:"This is a Help text block"
                  }
              ]
            },
              {
                  type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
                  title : "Block Text",
                  inputs : [
                      {
                          type: FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT,
                          text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                      }
                  ]
              },
              {
                  type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
                  title : "Image",
                  inputs : [
                      {
                          type: FormConstants.FORM_DISPLAY_TYPE_IMAGE,
                          url:"https://www.w3schools.com/html/pic_mountain.jpg",
                          justify:'LEFT',
                          noTitle:true
                      }
                  ]
              },
              {
                  type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
                  title : "Info Block",
                  inputs : [
                      {
                          type: FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK,
                          heading:"Lorem Ipsum?",
                          graphics: <i class="fa fa-home fa-2x color-primary-text"></i>,
                          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                          noTitle:true
                      }
                  ]
              },
              {
                  type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
                  title : "Info Block With Image",
                  inputs : [
                      {
                          type: FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK,
                          heading:"Lorem Ipsum?",
                          graphics: <img style={{maxWidth:"100%", maxHeight:"100%"}} src="/res/images/demo-info-block.png"></img>,
                          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                          noTitle:true
                      }
                  ]
              },
              {
                  type: FormConstants.FORM_INPUT_TYPE_FORMSECTION,
                  title : "Text Label",
                  inputs : [
                      {
                          type: FormConstants.FORM_DISPLAY_TYPE_TEXT,
                          title:"Label",
                          text: "Value",
                      }
                  ]
              }

          ]
      }

    return formDescription
  }
}
