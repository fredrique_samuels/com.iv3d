/*
A Data Node consist of a set of properties that will
be used to display it to the screen.

Nodes constist of,
  type : A string representing the type of data.
  content : The actual data content. This will be specific to every data type.
  params : A set of optionl parameters wrapped in a js object.
        -  title : An string used to title the data.
*/
export class DataNodeBuilder {
  /**
  Returns a list of one data node object.
  */
  static createNew(type, content, params) {
    return {
      type,
      content,
      params
    }
  }
}
