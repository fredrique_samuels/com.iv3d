import DialogWindow from './../components/dialog/DialogWindow'

export function timeout(callback, millsec) {
  setTimeout(callback, millsec)
}


export function createLayerItemForDataNodeViaLayouProvider(dn, layerParams, layoutProvider) {
  const dialogContent = createDialogContentFromDataNode(dn)
  const dialog = DialogWindow.create(dialogContent)
  const layoutParams = layoutProvider.next(dn, layerParams).layout
  return {
    id:"layerItem"+dn.id,
    content:dialog,
    layoutParams:layoutParams,
  }
}

export function createLayerItemForDataNode(dn, layerId) {
  const dialogContent = createDialogContentFromDataNode(dn)
  const dialog = DialogWindow.create(dialogContent)
  return {
    id:layerId+"layerItem"+dn.id,
    content:dialog,
    layoutParams:{},
  }
}


export function createDialogContentFromDataNode(dn) {
  return {
    id:"dialog"+dn.id,
    title:dn.title,
  }
}
