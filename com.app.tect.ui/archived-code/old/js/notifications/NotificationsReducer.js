import Immutable from 'immutable'
import * as NotificationActions from './NotificationActions'
import NotificationCache from './NotificationCache'


function newNotification(newState, n) {
  newState.notifications = newState.notifications.set(n.type, n)
}

const NotificationsReducer = ( state={notifications:new Immutable.Map()} , action) => {
  const newState = Object.assign({}, state)

  switch(action.type) {
    case NotificationActions.NEW_NOTIFICATION: {
      newNotification(newState, action.payload)
      break
    }
  }
  // console.log(newState);
  return newState
}

export default NotificationsReducer
