import Immutable from 'immutable'
import * as Notification from './Notification'

export default class NotificationCache {
  constructor(values, limit) {
    this.notifications = values
    this.limit = limit
    while (this.notifications.size>limit) {
      this.notifications = this.notifications.delete(0)
    }
  }

  add(n) {
    return new NotificationCache(this.notifications.push(n), this.limit)
  }
}
