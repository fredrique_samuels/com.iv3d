import React from 'react'
import DialogWindow from './../../../components/dialog/DialogWindow'
import Immutable from 'Immutable'

const MODE_CREATE_NODE = "node.edit.create"

/**
node view
params = {
  id:  The node id.
  icon[optional, ] : node Font awesome icon.
  iconColor [optional, defaults to white]: icon color.
}
**/
export default class DataNodeInfoPanel extends React.Component {
  render() {

    const {id, label, lightBg, icon, iconColor, mode, createNewNodeCallback} = this.getParams()

    if(mode==MODE_CREATE_NODE) {
      return (
        <div class="node-panel node-panel-default node-panel-create-new">
          <div class="node-icon-panel">
            <button onClick={createNewNodeCallback} class="btn btn-success btn-sm fill-parent"><i class={"fa fa-plus fa-2x"}></i></button>
          </div>
        </div>
      )
    }


    // <div class="node-icon-panel-sm">
    //   <button class="btn btn-info btn-sm"><i class="fa fa-navicon fa-2x"></i></button>
    // </div>
    // <div class="node-icon-panel-sm">
    //   <button class="btn btn-primary btn-sm"><i class="fa fa-eye fa-2x"></i></button>
    // </div>
    // <div class="node-icon-panel-sm">
    //   <button class="btn btn-success btn-sm"><i class="fa fa-caret-left fa-2x"></i></button>
    // </div>

    const iconComp = icon?<i class={"fa "+icon+" fa-2x"}></i>:null
    return (<div key={id} class="node-panel node-panel-default node-panel-has-sub-items">
      <button class="node-label node-label-info node-label-xs">{label?label:"No Label Set"}</button>
      <div class="node-icon-panel">
        <button class="btn btn-default btn-sm fill-parent">{iconComp}</button>
      </div>
    </div>)
  }

  getParams() {
    const defaultValues = {
      iconColor:"white",
    }
    return Object.assign({}, defaultValues, this.props.params)
  }
}

export class AddDataNodeInfoPanel extends React.Component {
  render() {
    const p = Object.assign({}, {mode:MODE_CREATE_NODE}, this.props.params)
    return <DataNodeInfoPanel id={this.props.id} params={p} />
  }
}
