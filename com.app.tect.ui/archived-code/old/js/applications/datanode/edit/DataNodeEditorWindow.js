import React from 'react'
import DialogWindow from './../../../components/dialog/DialogWindow'

import Immutable from 'Immutable'
import Interface from './../../../api/Interface'

import { AddDataNodeInfoPanel } from './DataNodeInfoPanel'
import {IdGenerator} from './../../../common'
import DataNodeInfoPanel from './DataNodeInfoPanel'
import {dispatcher} from "../../../../../tectui/tectui";


const nodeItemIdGen = new IdGenerator("node_item_")
class NodeItem {
  constructor(model={}) {
    this.id = nodeItemIdGen.generate();
    this.model = Object.assign({}, {id:this.id, expand:false, parent:null, label:this.id}, model)
  }

  getId(){return this.id}
  getView(viewFactory) {return viewFactory.createView(this.model)}
}


class NodeItemViewFactory {
  constructor(nodes) {
    this.nodes = nodes;
  }

  createView(model) {

    const children = this.nodes.getChildrenForId(model.id)
    const childrenComps = model.expand?children.map( c => c.getView(new NodeItemViewFactory(this.nodes))):[]

    return (<div class="content-row">
      <div class="item inline-comp"><DataNodeInfoPanel params={model}/></div>
      <div class="inline-comp">{childrenComps}</div>
    </div>)
  }
}


class NodeContainer {
  constructor() {
    this.nodes = Immutable.List()
  }

  // mutate methods
  push(i){ this.nodes = this.nodes.push(i);return this}
  setFocus(id){
    this.all().map(i=>i.model.expand=false)
    let n = this.getById(id)
    n.model.expand=true
    while(n.model.parent) {
      let p = this.getById(n.model.parent)
      p.model.expand=true
      n=p
    }
    return this
  }

  // query methods
  all() {return this.nodes.toArray()}
  rootNodes(){return this.nodes.toArray().filter(i => i.model.parent==null)}
  getById(id){return this.nodes.toArray().filter(i => i.id==id)[0]}
  getChildrenForId(parentId){return this.nodes.toArray().filter(i => i.model.parent==parentId)}
}

class FormContentHandler extends React.Component{

    constructor(){
        super();
        this.formId = IdGenerator.generateGlobalId();
    }

    componentDidMount() {
        $('#'+ this.formId).submit(this.formSubmit.bind(this))
        // console.log($('#'+ this.formId).serializeArray())
        // $('#'+ this.formId).submit(function() {
        //     let theForm = $(this);
        //
        //     console.log($theForm.serialize())
        //     console.log($theForm.serializeArray())
        //
        //     // send xhr request
        //     // $.ajax({
        //     //     type: $theForm.attr('method'),
        //     //     url: $theForm.attr('action'),
        //     //     data: $theForm.serialize(),
        //     //     success: function(data) {
        //     //         console.log('Yay! Form sent.');
        //     //     }
        //     // });
        //
        //     // prevent submitting again
        //     return false;
        // });
    }

    formSubmit() {
        let theForm = $('#'+ this.formId);

        console.log(theForm.serialize())
        console.log(theForm.serializeArray())

        // send xhr request
        // $.ajax({
        //     type: $theForm.attr('method'),
        //     url: $theForm.attr('action'),
        //     data: $theForm.serialize(),
        //     success: function(data) {
        //         console.log('Yay! Form sent.');
        //     }
        // });

        // prevent submitting again
        return false;
    }


  onTestClick(event) {
      event.preventDefault();
      console.log($('#'+ this.formId).serializeArray())
  }
    

  render() {
    const containerStyle = {
      position:"relative",
      width:"400px",
      height:"500px",
      backgroundColor:"rgba(0,0,0,.7)",
      padding:"12px",
      top:"10%",
      left:"calc( (100% - 400px) / 2)" ,
      overflow:"auto"
    }
    const formContent = (<div style={containerStyle}>
        <div class="panel-text" style={{width:"100%"}}>
            <form>

                <div class="form-section">
                    <h1 class="form-section-heading">Form Styling</h1>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Text Input</label>
                        </div>
                        <div class="form-input-container">
                            <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                            <p class="help-block">Example block-level help text here. hudif isadf isdf asfoasnfo sdfoas o fdsoaf dos</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Text Input</label>
                        </div>
                        <div class="form-input-container">

                            <p class="help-block error-block">Please enter a valid value!</p>
                            <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                            <p class="help-block">Example block-level help text here. hudif isadf isdf asfoasnfo sdfoas o fdsoaf dos</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Text Input</label> <i class=" fa fa-question-circle form-help"></i>
                        </div>
                        <div class="form-input-container">
                            <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Text Input</label>
                        </div>
                        <div class="form-input-container input-group">

                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                            <span class="input-group-addon">.00</span>
                        </div>
                    </div>
                </div>

                <div class="form-section">
                    <h1 class="form-section-heading">Form Input Types</h1>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Text Area</label>
                        </div>
                        <div class="form-input-container">
                            <textarea class="form-control form-textarea" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Check boxes</label>
                        </div>
                        <div class="form-input-container">
                            <div><input value="" type="checkbox"/><label class="form-list-select-label">This is a label</label></div>
                            <div><input value="" type="checkbox"/><label class="form-list-select-label">This is a label</label></div>
                            <div><input value="" type="checkbox"/><label class="form-list-select-label">This is a label</label></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Radio</label>
                        </div>
                        <div class="form-input-container">
                            <div><input name="s" value="" type="radio"/><label class="form-list-select-label">This is a label</label></div>
                            <div><input name="s" value="" type="radio"/><label class="form-list-select-label">This is a label</label></div>
                            <div><input name="s" value="" type="radio"/><label class="form-list-select-label">This is a label</label></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">File</label>
                        </div>
                        <div class="form-input-container">
                            <div class="form-control-file-input-container">
                                <input type="file" class="form-control"/>
                            </div>
                            <span class="file-input-left-corner"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Color</label>
                        </div>
                        <div class="form-input-container">
                            <input type="color" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Url</label>
                        </div>
                        <div class="form-input-container">
                            <input type="url" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Email</label>
                        </div>
                        <div class="form-input-container">
                            <input type="email" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Range</label>
                        </div>
                        <div class="form-input-container">
                            <label id="demo-range" >2</label>
                            <input type="range" class="form-control" name="points" min="0" max="100" value="2" onchange="updateRangeDisplay(this.value)"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-container">
                            <label class="form-label">Number</label>
                        </div>
                        <div class="form-input-container">
                            <input type="number" min="0" max="10" value="3" class="form-control"/>
                        </div>
                    </div>

                </div>
                <input  class="form-button btn btn-error btn-sm" type="submit" value="Cancel"/>
                <input class="form-button btn btn-primary btn-sm" type="submit" value="Create Item"/>
            </form>
        </div>
    </div>)

    const panelStyleTextOnly = {
      padding:"10px",
      position:"relative",
      maxWidth:"100%",
      overflowX:"auto",
      overflowY:"auto",
      maxHeight:"100%",
    }

    const content =  (<div class="panel panel-default panel-text" style={panelStyleTextOnly}>
      <div style={{width:"100%",height:"41px"}}></div>
      {formContent}
      </div>)

    const dialogParams = {
      title:"This is a form",
      content:content,
    }
    return formContent
  }
}

class FormContentHandlerFactory {
  createContentHandler(args) {
    const {contentParams, layerId} = args
    return <FormContentHandler />
  }
}

export default class DataNodeEditorWindow extends React.Component {

  constructor() {
    super()
    this.idSeed = 0
  }

  componentWillMount() {
    this.state = {
      nodes:new NodeContainer(),
    }

    const r1 = new NodeItem({})
    const r2 = new NodeItem({})
    const c1 = new NodeItem({parent:r2.getId()})
    const c2 = new NodeItem({parent:r2.getId()})
    const c3 = new NodeItem({parent:r1.getId()})
    const c4 = new NodeItem({parent:r1.getId()})

    this.state.nodes = this.state.nodes.push(r1)
    this.state.nodes = this.state.nodes.push(r2)
    this.state.nodes = this.state.nodes.push(c1)
    this.state.nodes = this.state.nodes.push(c2)
    this.state.nodes = this.state.nodes.push(c3)
    this.state.nodes = this.state.nodes.setFocus(c3.getId())
  }

  render() {
    const panelStyleTextOnly = {
      padding:"10px",
      position:"relative",
      maxWidth:"100%",
      overflowX:"auto",
      overflowY:"auto",
      maxHeight:"100%",
    }
    const nodeComps = this.state.nodes.rootNodes().map(i => i.getView(new NodeItemViewFactory(this.state.nodes)))
    const textContent = (
      <div class="scroll-parent">
        <div class="content-row">
          {nodeComps}
        </div>
        <div class="item inline-comp"><AddDataNodeInfoPanel params={{createNewNodeCallback:this.showNewNodeForm.bind(this)}} /></div>
      </div>
    )

    const content =  (<div class="panel panel-default panel-text" style={panelStyleTextOnly}>
      <div style={{width:"100%",height:"41px"}}></div>
      {textContent}
      </div>)

    const dialogParams = {
      title:"This is a test",
      content:content,
    }
    return DialogWindow.create(dialogParams)
  }

  showNewNodeForm() {
    const contentParams = {
      layerComponentModelId:"datanode.edit.demo.modelid",
      contentHandlerFactory:new FormContentHandlerFactory()
    }
    const layerParams = {
      canBeCleared:false
    }
    Interface.renderDataToNewLayer(dispatcher(), contentParams, layerParams)
  }

  newId() {
    return ++this.idSeed
  }

}
