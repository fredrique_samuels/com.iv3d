import React from 'react'

import * as UiCommon from './../ui/UiCommon'


const mainStyle = {
  width:"100%",
  height:"100%",
  position:"relative",
}

const panelHeadingStyle = {
  top:"0px",
  position:"absolute",
  width:"100%",
  paddingLeft:'10px',
}

const panelHeadingButtonStyle = {
  display:"inline-block",
  float:'right',
  marginRight:'5px',
  borderColor:'#ffffff',
  border:'1px solid',
}

const panelStyleTextOnly = {
  paddingTop:"0px",
  position:"relative",
  width:"100%",
  overflowX:"hidden",
  overflowY:"auto",
  maxHeight:"100%"
}

const panelStyle = {
  paddingTop:"0px",
  position:"relative",
  width:"100%",
  height:"100%",
  overflowX:"hidden",
  overflowY:"hidden",
}

export default class Version0_2 {
    static render(params, dialog) {
      const {icon, title, content, dataNode, viewDetailHint} = params;

      const actionComps = []
      if(viewDetailHint==UiCommon.VIEW_DETAIL_HINT_PREVIEW) {
        actionComps.push((<button onClick={dialog.expand.bind(dialog)} class="btn btn-default btn-xs" style={panelHeadingButtonStyle}>
            <i class="fa fa-expand"></i>
        </button>))
      }

      const hasDecorations = icon || title || actionComps.length>0;

      let contentComp = <div class="panel panel-default panel-text" style={panelStyleTextOnly}></div>

      if(dataNode) {
        contentComp = this.getDataNodeContentComp(dataNode,content, hasDecorations)
      } else {
        contentComp = content
      }

      let iconComp = null
      if(icon) {
        iconComp = icon.type==UiCommon.ICON_TYPE_FONT_AWESOME ? ( <h3 class="panel-title">
            <i class={"fa " + icon.name}></i>
          </h3>
        ) : null
      } else if(dataNode){
        const dataNodeFactory = UiCommon.dataNodeFactoryFromTypeString(dataNode.type)
        const dnIcon = dataNodeFactory.getIcon()
        iconComp = dnIcon.type==UiCommon.ICON_TYPE_FONT_AWESOME ? ( <h3 class="panel-title">
            <i class={"fa " + dnIcon.name}></i>
          </h3>) : null
      }

      let titleComp = null
      if(title) {
        titleComp = <h3 class="panel-title">{title}</h3>
      }


      return ( <div class="dialog-main" style={mainStyle} >
            {contentComp}
            {
             hasDecorations ? (<div class="panel-heading panel-heading-default panel-text" style={panelHeadingStyle} >
              {iconComp}
              {title}
              {actionComps}
            </div> ) : null
            }
            </div> )
      }

    getDataNodeContentComp(dataNode,content, hasDecorations) {
      if(dataNode.type=='datanode.type.text') {
        const textContent = (<div>
          { hasDecorations ? <div style={{width:'100%',height:'41px'}}></div> : null }
          {content}
          </div>
        )

        return (<div class="panel panel-default panel-text" style={panelStyleTextOnly}>
          {textContent}
          </div>)
      } else {

        const isTwitter = dataNode.type=='datanode.type.twitter.tweet'
        const fitContent = isTwitter

        const filledContent = (<div style={{width:'100%',top: (hasDecorations?'40px':'0px'),bottom:'0',position:'absolute'}}>
            {content}
          </div>)

        return (<div class="panel panel-default panel-text" style={panelStyle}>
          {filledContent}
          </div>)
      }
    }
}
