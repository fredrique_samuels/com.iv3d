import $ from 'jquery'
import Immutable from 'immutable'
import Interface from './../../api/Interface'
import React from 'react'
import Version0_2 from './Version0_2'

import { connect } from 'react-redux'

import * as UiCommon from './../ui/UiCommon'
import {dispatcher} from "../../../../tectui/tectui";

//import Themes from "./../../themes/Themes"

@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export default class DialogWindow extends React.Component {

  constructor() {
    super()
  }

  render() {
    const params = this.getParams()
    // return this.version_0_1(params, this)
    return Version0_2.render(params, this)
  }

  expand() {
    const dispatch = dispatcher()
    const {layerComponentModelId} = this.getParams()
    Interface.renderDataToNewLayer(dispatch, {layerComponentModelId:layerComponentModelId, dataNodes:[this.getParams().dataNode,]})
  }

  version_0_1(params) {
    let titleComp = null
    if (params.icon || params.title) {
      titleComp = (
          <div style={dialogTitleSection} class="dialog-header border-box" >
            <h3 class="dialog-title">
              {params.icon}
              {params.title}
            </h3>
          </div>
        )
    }

    const dialogTitleSection = {
    	width: "100%",
      height: "30px",
    }

    const dialogBodySection = {
      width: "100%",
      position: "absolute",
      top: titleComp?"30px":"0px",
      bottom: "0",
    }

    const { dataNode } = params
    let content = params.content;

    if(dataNode.type=='datanode.type.text') {
      content = (<div class="dialog-text-cnt fill-parent" >
          {params.content}
      </div>)
    }

    return (
      <div class="dialog-main">
        {titleComp}
        <div style={dialogBodySection} class="dialog-body-padding border-box">
          <div class="fill-parent dialog-body-container border-box">
             {content}
          </div>
        </div>
      </div>
    )
  }

  getParams() {
    const defaultParams = {
      title : null,
      icon : null,
      content : null,
    }

    return Object.assign({}, defaultParams, this.props.params)
  }

  static create(params={}) {
    return <DialogWindow params={params}/>
  }

}
