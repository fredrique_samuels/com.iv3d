import React from 'react'
import AbstractLayerContentHandler from './AbstractLayerContentHandler'
import FillParent from './../UiComponents'

class ItemLayout {
  create(args) {
    const layoutParams1 = {
         width:"47%",
         height:"90%",
         top: "5%",
         left: "2%",
      }

    const layoutParams2 = {
         width:"47%",
         height:"90%",
         top: "5%",
         left: "51%",
    }

    if(args.index===0) return layoutParams1
    return layoutParams2
  }
}

export default class TwoItemsLayerContentHandler extends React.Component {
  render() {
    const items = AbstractLayerContentHandler.createItemsFromDataNodes(this.props.args, new ItemLayout())
    return <FillParent>{items}</FillParent>
  }
}
