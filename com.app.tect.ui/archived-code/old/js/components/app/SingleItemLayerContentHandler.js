import React from 'react'

import AbstractLayerContentHandler from './AbstractLayerContentHandler'
import FillParent from './../UiComponents'

class ItemLayout {
  create(args) {
    return  {
         width:"50%",
         height:"70%",
         top: "15%",
         left: "25%",
      }
  }
}

export default class SingleItemLayerContentHandler extends React.Component {
  render() {
    const items = AbstractLayerContentHandler.createItemsFromDataNodes(this.props.args, new ItemLayout())
    return <FillParent>{items}</FillParent>
  }
}
