import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import Transition from 'react-inline-transition-group'

function offsetStringInt(value, ext, offset) {
    let topVal = Number.parseInt(value.replace(ext, ''))
    return new String(topVal+offset).valueOf()+ext
}

function offsetPixelValue(value, offset) {
  if(value.includes('%')) {
    return offsetStringInt(value, '%', offset['%']? offset['%'] : offset)
  } else if(params.top.includes('px')) {
    return offsetStringInt(value, 'px', offset['px']? offset['px'] : offset)
  }
  return value
}

/**
 AppLayer is a wrapping div to render it's content ontop of all the contents
 of the parent container.
**/
export default class AppLayerItem extends React.Component {
    constructor() {
      super()
    }

    render() {

      const {children, params} = this.props


      const base = {
        opacity:0 ,
        top: offsetPixelValue(params.top, -5),
        left: params.left,
      }

      const appear = {
        opacity: params.opacity,
        top: params.top,
        left: params.left,
        transition: 'all 1000ms',
      }

      const leave = {
        transition: 'all 200ms',
        opacity:0,
      }

      const transitionStyles = {
        base,
        appear,
        leave,
      }

      const divStyle = {
        position: "absolute",
        width: params.width,
        height: params.height,
        top: params.top,
        left: params.left,
        zIndex : params.zIndex,
        opacity : params.opacity,
      }

      return (<Transition
          childrenBaseStyle={transitionStyles.base}
          childrenAppearStyle={transitionStyles.appear}
          childrenEnterStyle={transitionStyles.appear}
          childrenLeaveStyle={transitionStyles.leave}
        >
        <div class="fill-parent" style={divStyle} key={this.props.key}>
          {children}
        </div>
        </Transition>)
    }
}
