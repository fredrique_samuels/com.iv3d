import AbstractLayerContentHandler from './AbstractLayerContentHandler'
import FillParent from './../UiComponents'
import React from 'react'

import * as UiCommmon from './../ui/UiCommon'

class ItemLayout {
  create(args) {
    if(args.index===0) {
      return { width:"47%", height:"90%", top: "5%", left: "2%",}
    } else if(args.index===1) {
      return { width:"23%", height:"44.5%", top: "5%", left: "51%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if(args.index===2) {
      return { width:"23%", height:"44.5%", top: "5%", left: "74.5%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if(args.index===3) {
      return { width:"23%", height:"44.5%", top: "50%", left: "51%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if (arg.index==4) {
      return { width:"23%", height:"44.5%", top: "50%", left: "74.5%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    }
    return null
  }
}

export default class TreeToFiveItemsLayerContentHandler extends React.Component {

  render() {
    const items = AbstractLayerContentHandler.createItemsFromDataNodes(this.props.args, new ItemLayout())
    return <FillParent>{items}</FillParent>
  }
}
