import React from 'react'
import { connect } from 'react-redux'

import Header from './Header'
import Immutable from 'immutable'

import * as App from './ApplicationContentComponent'
import * as common from "../common"
import * as Common from './../api/Common'
import * as UiConstants from './ui/UiConstants'
import * as Notification from './../notifications/Notification'
import * as NotificationActions from './../notifications/NotificationActions'


import ApplicationContentComponent from './ApplicationContentComponent'
import DataNodeFactory from './ui/DataNodeFactory'
import DialogWindow from "./dialog/DialogWindow"
import DotOverlay from './worldmap/DotOverlay'
import Interface from './../api/Interface'
import MainMenu from './MainMenu'
import ServiceManager from './../domain/ServiceManager'
import WorldMapWidget from './worldmap/WorldMapWidget'
import WorldMapEventsWidget from './worldevents/WorldMapEventsWidget'

export function demoEventData(dispatcher) {
  const capeTown = new common.LatLng( -33.9249, 18.4241)
  const newYork = new common.LatLng( 40.7128, -74.0059)
  const sydney = new common.LatLng( -33.865143, 151.209900)

  setTimeout(() => {
    {
      const pl = new Notification.News24ItemPayload()
      pl.tags.push("Crime")
      pl.location = capeTown
      pl.newsId = "newsitem_1"
      const n = new Notification.News24Item(pl)
      dispatcher(NotificationActions.newNotification(n))
    }

    {
      const pl = new Notification.News24ItemPayload()
      pl.tags.push("Crime")
      pl.newsId = "newsitem_2"
      pl.location = newYork
      const n = new Notification.News24Item(pl)
      dispatcher(NotificationActions.newNotification(n))
    }

    {
      const pl = new Notification.News24ItemPayload()
      pl.tags.push("Robbery")
      pl.newsId = "newsitem_3"
      pl.location = sydney
      const n = new Notification.News24Item(pl)
      dispatcher(NotificationActions.newNotification(n))
    }
  },1000)
}

export function pushEmptyDialogsToLayer(dispatcher, dnCount, layerComponentModelId) {
  var dataNodes = []
  for (var x=0;x<dnCount;x++) {
    const c = DataNodeFactory.createDataNode(UiConstants.DATA_NODE_TYPE_TEXT, {text:"This is a Data Node"})
    dataNodes.push(c)
  }
  Interface.renderDataToNewLayer(dispatcher, {layerComponentModelId:layerComponentModelId,dataNodes:dataNodes})
}

export function testLayers(dispatcher, layerComponentModelId) {
    console.log("testLayers", dispatcher, layerComponentModelId);
    const textNodeContent = {
      text:"This is a Data Node"
    }
    const textNode = DataNodeFactory.createDataNode(UiConstants.DATA_NODE_TYPE_TEXT, textNodeContent)


    const imageContent = {url:"res/images/demo/captain-america-thor-iron-man-why-these-3-avengers-will-not-survive-the-infinity-war-494516.jpg",
        caption:"This is Tony Stark"}
    const imageDn = DataNodeFactory.createDataNode(UiConstants.DATA_NODE_TYPE_IMAGE, imageContent)
    Interface.renderDataToNewLayer(dispatcher,
      {
        layerComponentModelId:layerComponentModelId,
        dataNodes:[imageDn,]
      },
      {
        canBeCleared:false
      })


    const videoContent = {
        videoId:"XGSy3_Czz8k",
        // url:"https://www.youtube.com/embed/XGSy3_Czz8k",
        caption:"This is Tony Stark",
        title:"This is a video",
    }
    const videoDn = DataNodeFactory.createDataNode(UiConstants.DATA_NODE_TYPE_VIDEO_YOUTUBE, videoContent)
    const videoDn2 = DataNodeFactory.createDataNode(UiConstants.DATA_NODE_TYPE_VIDEO_YOUTUBE, videoContent)

    const tweetContent = {
        url:"https://twitter.com/Interior/status/463440424141459456",
    }
    const tweetDn = DataNodeFactory.createDataNode(UiConstants.DATA_NODE_TYPE_TWITTER_TWEET, tweetContent)

    Interface.renderDataToNewLayer(dispatcher,
      {
        layerComponentModelId:layerComponentModelId,
        dataNodes:[textNode, videoDn,tweetDn,videoDn2,]
      })
    // pushEmptyDialogsToLayer(dispatcher, 14, layerComponentModelId)
    // Common.timeout(
    //   ()=>pushEmptyDialogsToLayer(dispatcher, 2),
    //   1000);
    // Common.timeout(
    //   ()=>Interface.renderDataToNewLayer(dispatcher, {layerComponentModelId:layerComponentModelId,dataNodes:[{id:1},{id:2},{id:3}]}),
    //   2000);
    // Common.timeout(
    //   ()=>Interface.renderDataToNewLayer(dispatcher, {layerComponentModelId:layerComponentModelId,dataNodes:[{id:1},{id:2},{id:3},{id:4}]}),
    //   3000);
    // Common.timeout(
    //   ()=>{
    //     const layerId = Interface.renderDataToNewLayer(dispatcher, {layerComponentModelId:layerComponentModelId,dataNodes:[{id:1},{id:2},{id:3},{id:4},{id:5}]})
    //
    //     // Common.timeout(
    //     //   ()=>Interface.removeLayeredComponents(dispatcher, layerId),
    //     //   1000);
    //   },
    //   4000);
    // Common.timeout(
    //   ()=>{
    //     const dn = {
    //       layerComponentModelId:layerComponentModelId,
    //       dataNodes:[{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}]
    //     }
    //     const layerId = Interface.renderDataToNewLayer(dispatcher, dn)
    //   },
    //   5000);
}
