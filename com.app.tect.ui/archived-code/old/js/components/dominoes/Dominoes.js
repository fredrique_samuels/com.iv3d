import React from 'react'

class DominoesGame extends React.Component {

  constructor() {
  }

  componentWillMount() {
    this.state = {}
  }

  render() {
    const canvas = document.getElementById("DominoesGameCanvas");
    var ctx = c.getContext("2d");

    // Create gradient
    var grd = ctx.createRadialGradient(75,50,5,90,60,100);
    grd.addColorStop(0,"red");
    grd.addColorStop(1,"white");

    // Fill with gradient
    ctx.fillStyle = grd;
    ctx.fillRect(10,10,150,80);

    return <canvas id="DominoesGameCanvas" style={{width:"500px", height:"500px"}} />
  }
}
