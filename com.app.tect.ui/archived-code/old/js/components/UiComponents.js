import React from 'react'
import Transition from 'react-inline-transition-group'


export default class FillParent extends React.Component {
  render() {
    return <div class='fill-parent-absolute'>{this.props.children}</div>
  }
}

export class FadeInAndOutTransition {
  static render(children) {
    const transitionStyles = {
        base: { opacity:0 },
        appear: {
          opacity:1,
          transition: 'all 500ms',
        },
        leave: {
          transition: 'all 500ms',
          opacity:0,
        },
      };

    return (<Transition
        childrenBaseStyle={transitionStyles.base}
        childrenAppearStyle={transitionStyles.appear}
        childrenEnterStyle={transitionStyles.appear}
        childrenLeaveStyle={transitionStyles.leave}
      >
        {children}
      </Transition>)
  }
}

export class FadeAndASlideInAndOutTransition {
  static render(comp, params) {
    const transitionStyles = {
        base: { opacity:0 },
        appear: {
          opacity:1,
          transition: 'all 500ms',
        },
        leave: {
          transition: 'all 500ms',
          opacity:0,
        },
      };

    return (<Transition
        childrenBaseStyle={transitionStyles.base}
        childrenAppearStyle={transitionStyles.appear}
        childrenEnterStyle={transitionStyles.appear}
        childrenLeaveStyle={transitionStyles.leave}
      >
        {comp}
      </Transition>)
  }
}
