import React from 'react'
import DataNodeFactory from './../../DataNodeFactory'
import { CaptionedMediaDataNode } from './../../DataNodeFactory'
import * as UiCommon from './../../UiCommon'

export default class VideoYoutubeDataNodeFactory extends DataNodeFactory {
  type(){return 'datanode.type.video.youtube'}

  getIcon(){return UiCommon.Icon.createFontAwesome("fa-youtube-play")}

  createDataNode(params={}) {
    const { videoId, url, caption } = params
    return this.createNodeFromContent({videoId, url, caption}, params)
  }

  createView(dataNode, params={}) {
    const media = this.createMediaContent(dataNode)
    return CaptionedMediaDataNode.createCaptionedMediaView(media, dataNode.content.caption)
  }

  createPreviewView(dataNode, params={}) {
    const media = this.createPreviewMediaContent(dataNode)
    const overlay = <i class="fa fa-youtube-play fa-5x"></i>
    return CaptionedMediaDataNode.createCaptionedPreviewMediaView(media, dataNode.content.caption, overlay)
  }

  createMediaContent(dataNode) {
    const {caption, videoId} = dataNode.content
    const videoUrl = "https://www.youtube.com/embed/"+videoId

    const s = {
      objectFit:'contain'
    }
    return <iframe style={s} width="100%" height="100%" src={videoUrl}></iframe>
  }

  createPreviewMediaContent(dataNode) {
    const {caption, videoId} = dataNode.content
    const url = "http://img.youtube.com/vi/"+videoId+"/0.jpg"
    const imgStyle = {width:'100%',
      height:'100%',
      objectFit:'contain',
    }
    return <img src={url} style={imgStyle}/>
  }

}
