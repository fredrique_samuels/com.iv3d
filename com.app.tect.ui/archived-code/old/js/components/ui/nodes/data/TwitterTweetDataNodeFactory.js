import React from 'react'
import DataNodeFactory from './../../DataNodeFactory'
import $ from 'jquery'

let twitterIdSeed = 0

class TwitterTweetComponent extends React.Component {

  constructor() {
    super()
    this.state = {}
    this.id = (++twitterIdSeed)
  }

  componentWillMount() {
    const { tweetUrl, urlParams} = this.props

    const tweetGet = {
        method: "GET",
        url: "https://publish.twitter.com/oembed?"+(urlParams?urlParams:"")+"url="+tweetUrl,
        dataType: "jsonp",
      };

    var callbackDeferred = $.Deferred();
    callbackDeferred.resolve(this.updateTweetContent.bind(this));
    $.when(callbackDeferred, $.ajax(tweetGet)).done(
        function(callback, tweetResult) {
          callback(tweetResult)
        }
    );
  }

  updateTweetContent(content) {
    $("#"+this.id).html(content[0].html)
  }

  render() {
    return (<div id={this.id} class="fill-parent"></div>)
  }

}

export default class TwitterTweetDataNodeFactory extends DataNodeFactory {
  type(){return 'datanode.type.twitter.tweet'}

  createDataNode(params={}) {
    const { url } = params
    return this.createNodeFromContent({url}, params)
  }

  createView(dataNode, params={}) {
    const { url } = dataNode.content
    return <TwitterTweetComponent tweetUrl={url}/>
  }

  createPreviewView(dataNode, params={}) {
    const { url } = dataNode.content
    return <TwitterTweetComponent tweetUrl={url} urlParams="hide_media=true&" />
  }
}
