import React from 'react'
import * as UiCommon from './UiCommon'

let dataNodeIdSeed = 1


export class CaptionedMediaDataNode {
  static createCaptionedMediaView(media, caption) {
    const captionComp = CaptionedMediaDataNode.createMediaCaptionComp(caption)
    const mediaContStyle = CaptionedMediaDataNode.createMediaStyle(captionComp)

    return (<div class="fill-parent" >
        <div style={mediaContStyle}>
          {media}
        </div>
        {captionComp}
      </div>)
  }

  static createCaptionedPreviewMediaView(media, caption, overlay=null) {
    const captionComp = CaptionedMediaDataNode.createPreviewMediaCaptionComp(caption)
    const mediaContStyle = CaptionedMediaDataNode.createPreviewMediaStyle(captionComp)

    const s = {
      position:"absolute",
      top:"0%",
      left:"0%",
      height:"100%",
      width:"100%",
      zIndex:2,
      margin:0, padding:0,
      overflow: "hidden",
    }

    const centerText = {
      textAlign: "center",
      verticalAlign: "middle",
      lineHeight: "90px",

      marginTop:"auto",
      marginBottom: "auto",

      position: "absolute",
      top: "50%",
      transform: "translateY(-50%)",
      width:"100%",

      color:'rgba(255,255,255,.9)'
    }

    let overlayComp = null
    if(overlay) {
      overlayComp = (<div style={s}>
        <div style={centerText}>
          <p><i class="fa fa-youtube-play fa-5x"></i></p>
        </div>
      </div>)
    }

    return (<div class="fill-parent" >
        <div class="fill-parent" style={{position:"relative"}}>
          <div class="fill-parent" style={s}>
            <div style={mediaContStyle}>
              {media}
            </div>
            {captionComp}
          </div>
          {overlayComp}
        </div>
      </div>)
  }

  static createMediaCaptionComp(caption){
    if(caption) {
      const captionStyle={
          width:"100%",
          height:"20%",
      }
      return (<div class="dialog-text-cnt" style={captionStyle}>
        <p><center>{caption}</center></p>
      </div>)
    }
    return null
  }

  static createPreviewMediaCaptionComp(caption){
    if(caption) {
      const captionStyle = {
          top:"80%",
          width:"100%",
          height:"20%",
      }
      return (<div class="dialog-text-cnt" style={captionStyle}>
        <p><center>{caption}</center></p>
      </div>)
    }
    return null
  }

  static createMediaStyle(hasCaption){
    if(hasCaption) {
      return {
          width:"100%",
          height:"80%",
          // position:"relative",
          // left:"10%",
      }
    }
    return {
        width:"100%",
        height:"100%",
    }
  }

  static createPreviewMediaStyle(hasCaption){
    if(hasCaption) {
      return {
          width:"100%",
          height:"80%",
      }
    }
    return {
        width:"100%",
        height:"100%",
    }
  }
}

export default class DataNodeFactory {
  createNodeFromContent(content, params={}) {
    return {
      type:this.type(),
      id:content.id?content.id:'datanode.uid.'+ (++dataNodeIdSeed),
      content:content,
      title:params.title,
      icon:params.icon,
      timestamp:params.timestamp,
      dataNode:params.dataNode,
    }
  }
  type(){return null}
  createDataNode(params={}) {return null}
  createView(dataNode, params={}) {return null}
  createPreviewView(dataNode, params={}){return this.createView(dataNode, params)}
  createMinimizedView(dataNode, params={}){return this.createPreviewView(dataNode, params)}
  getIcon(){return null}

  /*
  Create a new datanode from the given type and content params.
  */
  static createDataNode(nodeType, params) {
    return nodeType.createDataNode(params)
  }

  /*
  Create a full view for the data node given
  */
  static createDataNodeView(dataNode, params={}) {
    const nodeType = UiCommon.dataNodeFactoryFromTypeString(dataNode.type)

    const {viewDetailHint} = params
    if(viewDetailHint==UiCommon.VIEW_DETAIL_HINT_PREVIEW) {
      return nodeType.createPreviewView(dataNode, params)
    }

    return nodeType.createView(dataNode, params)
  }
}
