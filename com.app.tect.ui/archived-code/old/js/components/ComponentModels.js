import Immutable from 'immutable'

export const CREATE_MODEL = "COMPONENT_MODELS_CREATE"
export function createModel(iId, iModel) {
  return {
    type :CREATE_MODEL,
    payload : {
      id : iId,
      model : iModel,
    }
  }
}

export const UPDATE_MODEL = "COMPONENT_MODELS_UPDATE"
export function updateModel(dispatch, id, func, params) {
  console.log("ComponentModels.updateModel", dispatch, id, func, params);
  dispatch( {
      type :UPDATE_MODEL,
      payload : {
        id : id,
        func : func,
        params : params,
      }
    })
}

export const DELETE_MODEL = "COMPONENT_MODELS_DELETE"
export function deleteModel(id) {
  return {
    type :DELETE_MODEL,
    payload : {
      id : id
    }
  }
}

function createModelBiz(state, action) {
    const {id, model} = action.payload
    if(state.has(id)) return state
    return state.set(id, model)
}

function updateModelBiz(state, action) {
  const {id, func, params} = action.payload
  console.log("updateModelBiz", id, func, params);
  const oldModel = state.get(id)
  const newModel = func(oldModel, params)
  return state.set(id,  newModel)
}

function deleteModelBiz(state, action) {
    const {id} = action.payload
    if(state.has(id)) return state.delete(id)
    return state
}

const ComponentModelsReducer = (state=Immutable.Map(), action) => {
  switch (action.type) {
    case CREATE_MODEL:
      return createModelBiz(state, action)
      break
    case UPDATE_MODEL:
      return updateModelBiz(state, action)
      break
    case DELETE_MODEL:
      return deleteModelBiz(state, action)
      break
    default:
  }
  return state
}

export default ComponentModelsReducer

class CompModelIdSeed {
  constructor() {
    this.seed = 0
  }

  generateId() {
    return "comp_model_" + (++this.seed)
  }
}
const compModelIdSeed = new CompModelIdSeed()

export class ComponentModelGateway {
  constructor(params={}) {
    this.params = params
    this.modelId = null
  }

  createModel(comp) {
    this.createModelId(comp)
    const {modelFactory} = this.params
    if(!this.modelId) return
    if(!modelFactory) {
      return
    }
    comp.props.dispatch(createModel(this.modelId, modelFactory.build()))
  }

  deleteModel() {
    if(!this.modelId) { return }
    comp.props.dispatch(deleteModel(this.modelId))
  }

  getModel(comp) {
    const {componentModels} = comp.props
    if(!componentModels) { return null }
    return componentModels.get(this.modelId)
  }

  createModelId(comp) {
    if(this.modelId) return
    const {modelId} = comp.props
    if(modelId)this.modelId = modelId
    else this.modelId = compModelIdSeed.generateId()
  }

  getModelId() {
    return this.modelId
  }

}
