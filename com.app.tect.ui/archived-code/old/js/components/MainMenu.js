import $ from 'jquery'
import { IdGenerator } from './../common'

import React from 'react'
import Transition from 'react-inline-transition-group'

import * as UiCommon from './ui/UiCommon'


export default class MainMenu extends React.Component {

  constructor() {
    super()
    this.id = IdGenerator.generateGlobalId()
  }

  componentWillMount() {
      this.state = {pageSize:null}
  }

  componentDidMount() {
    this.updatePageSize()
  }

  componentDidUpdate() {
    this.updatePageSize()
  }

  render() {

    let content = [
      this.createMenuItem(1, "Rss Feed", UiCommon.Icon.createFontAwesome("fa-rss-square")),
      this.createMenuItem(2, "Avengers", UiCommon.Icon.createSvg("https://upload.wikimedia.org/wikipedia/commons/0/0b/Symbol_from_Marvel%27s_The_Avengers_logo.svg")),
      this.createMenuItem(3, "Image Gallery", UiCommon.Icon.createImage("http://www.w3schools.com/html/pic_mountain.jpg")),
    ]

    if(this.state.pageSize) {
      content = content.filter((x, i)=>{return i<this.state.pageSize})
    }

    return (<div id={this.id} class="fill-parent-absolute" style={{backgroundColor:"rgba(0,0,0,.1)"}}>
      <div id={"content"+this.id} class="fill-parent-absolute" style={{overflow:"hidden"}}>
      {content}
      </div>
    </div>)
  }

  updatePageSize() {
      var element = $("#"+ "content"+this.id )['0'];
      let newPageSize = this.getVisibleElementCount(element)
      this.setPageSize(newPageSize)
  }

  setPageSize(newPageSize) {
    if(this.state.pageSize!=newPageSize) {
      this.setState({pageSize:newPageSize})
    }
  }

  isChildHidden(element, child) {
    return child.offsetTop + child.offsetHeight >
        element.offsetTop + element.offsetHeight ||
        child.offsetLeft + child.offsetWidth >
        element.offsetLeft + element.offsetWidth
  }

  elementHasScrollContent(element) {
    return element.offsetHeight < element.scrollHeight ||
          element.offsetWidth < element.scrollWidth
  }

  getHiddenElementCount(element) {
    if(this.elementHasScrollContent(element)) {
      let hiddenChildCount = 0
      for(var i=0; i<element.childElementCount; i++){
        if (this.isChildHidden(element, element.children[i])) {
          hiddenChildCount+=1
        }
      }
      return hiddenChildCount
    }
    return 0
  }

  getVisibleElementCount(element) {
    const totalChildren = element.childElementCount
    if(this.elementHasScrollContent(element)) {
      let hiddenChildCount = this.getHiddenElementCount(element)
      return totalChildren-hiddenChildCount
    }
    return totalChildren
  }

  wrapInTransition(comp) {
    const base = {
      opacity:0,
    }

    const appear = {
      opacity:1,
      transition: 'all 1000ms',
    }

    const leave = {
      opacity:0,
      transition: 'all 1000ms',
    }

    const layerTransition = {
      base,
      appear,
      leave,
    }

    return (<Transition
        childrenBaseStyle={layerTransition.base}
        childrenAppearStyle={layerTransition.appear}
        childrenEnterStyle={layerTransition.appear}
        childrenLeaveStyle={layerTransition.leave}
      >
      {comp}
    </Transition>)
  }

  createMenuItem(id, text, icon) {
    const buttonStyle = {
      width:"100px",
      height:"100px",
      borderRadius:"20px",
    }

    const itemStyle = {
        // backgroundColor:"red",
        width:"100px",
        height:"140px",
        maxHeight:"140px",
        color:"white",
        margin:"15px",
        display:"inline-block",

        // border:"1px solid",
        // borderColor:"white",
        // overflow:"hidden"
    }

    const textPaneStyle = {
        height:"40px",
        maxHeight:"40px",
        textAlign:"center",
    }

    // var htmlInput = '<div><h1>Title</h1><p>A paragraph</p></div>';
    // var reactComponent = ReactHtmlParser(htmlInput);
    // console.log(reactComponent);

    // const iconComp = (
    //   <svg width="100px" height="100px" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    //       <rect x="0" y="0" rx="20" ry="20" width="100" height="100" style={{fill:'red',stroke:'black',strokeWidth:'0'}}/>
    //        <a>
    //   	<image x="10" y="10" height="80px" width="80px" xlinkHref="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Symbol_from_Marvel's_The_Avengers_logo.svg/2000px-Symbol_from_Marvel's_The_Avengers_logo.svg.png" />
    //       </a>
    //   </svg>
    // )

    let iconComp = null

    if(UiCommon.Icon.isSvg(icon) || UiCommon.Icon.isImage(icon)) {
      iconComp = (
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
               <a>
               <image x="0" y="0" height="100%" width="100%" xlinkHref={icon.url} />
              </a>
          </svg>
      )
    } else if (UiCommon.Icon.isFontAwesome(icon)){
      iconComp = <i class={"fa "+icon.name+" fa-2x"}></i>
    }

    const buttonCmp = (
        <button id={id} class="btn btn-default btn-xlg btn-icon"  style={buttonStyle}>
          {iconComp}
        </button>)

    const comp = (
      <div style={itemStyle}>
      {buttonCmp}
      <div style={textPaneStyle}>{text}</div>
      </div>
    )

    return comp




  }

  static createItem(name, params={icon:{type:"fa"}}) {

  }

}
