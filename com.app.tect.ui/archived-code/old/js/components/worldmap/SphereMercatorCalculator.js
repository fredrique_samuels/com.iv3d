import LatLng from './LatLng'
import Point2D from './Point2D'
import SphericalMercatorProjection from './SphericalMercatorProjection'

export default class SphereMercatorCalculator {
  constructor (topLeftLatLng, bottomRightLatLng, width, height) {
    let sphericalMercatorProjection = new SphericalMercatorProjection(100.0,100.0);
    let topLeftPerc = sphericalMercatorProjection.toPoint(topLeftLatLng);
    let bottomRightPerc = sphericalMercatorProjection.toPoint(bottomRightLatLng);

    let pppx =  (bottomRightPerc.x-topLeftPerc.x);
    let normalizedWidth = (width/pppx)*100;

    let pppy =  (bottomRightPerc.y-topLeftPerc.y);
    let normalizedHeight = (height/pppy)*100;

    this.offsetX = normalizedWidth * (topLeftPerc.x*0.01);
    this.offsetY = normalizedHeight * (topLeftPerc.y*0.01);
    this.smp = new SphericalMercatorProjection(normalizedWidth, normalizedHeight);
  }

  latLngToPoint = function(latLng) {
      var point = this.smp.toPoint(latLng);
      return new Point2D(point.x-this.offsetX, point.y-this.offsetY);
  }

}
