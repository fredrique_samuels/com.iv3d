import * as common from "./../../common"

const mapOverlayIdGenerator = new common.IdGenerator("worldMapSvgOverlay_uid_")

export default class WorldMapSvgOverlay {
  constructor(id, latitude, longitude) {
    this.id = id
    this.latLng = new common.LatLng(latitude, longitude)
  }

  getLatLng() {
    return this.latLng
  }

  render(point2D) {
    return null;
  }
}
