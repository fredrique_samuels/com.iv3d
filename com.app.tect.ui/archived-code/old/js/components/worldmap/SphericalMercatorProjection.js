import Point2D from './Point2D'
import LatLng from './LatLng'
import Iv3dMath from './../../iv3d-base'

export default class SphericalMercatorProjection {
   constructor(worldWidth, worldHeight) {
    this.mWorldWidth = worldWidth;
    this.mWorldHeight = worldHeight;
  }

  toPoint (latLng) {
        var x = (latLng.longitude/360) + .5;
        var siny = Math.sin(Iv3dMath.toRadians(latLng.latitude));
        var y = 0.5 * Math.log((1 + siny) / (1 - siny)) / -(2 * Math.PI) + .5;
        return new Point2D(x * this.mWorldWidth, y * this.mWorldHeight);
  }

  toLatLng (point) {
        var x = point.x / this.mWorldWidth - 0.5;
        var lng = x * 360;
        var y = .5 - (point.y / this.mWorldHeight);
        var lat = 90 - Iv3dMath.toDegrees(Math.atan(Math.exp(-y * 2 * Math.PI)) * 2);
        return new LatLng(lat, lng);
  }
}
