import Immutable from "immutable"
import * as  Notification from './../../notifications/Notification'

class MapEventItem {
  constructor() {
    this.categories = []
    this.name = ""
    this.id = ""
    this.latlng = null
  }
}

class News24itemTransformer {
  transform(n) {
    let item = new MapEventItem()
    const { location, tags, name} = n.payload
    const id = n.id

    item.id = id
    item.name = name
    item.categories = tags
    item.latlng = location
    return item
  }
}

class WorldMapEventItemTransformer {
  constructor() {
    this.transformers = Immutable.Map()
    this.transformers = this.transformers.set(Notification.NEWS24_ITEM, new News24itemTransformer())
  }

  get(n){
    const t = this.transformers.get(n.type)
    return t ? t.transform(n) : null;
  }
}

const worldMapEventItemTransformer = new WorldMapEventItemTransformer()
export default worldMapEventItemTransformer;
