
import React from 'react'
import Immutable from 'immutable'
import * as Themes from '../../themes/Themes'

import DotOverlay from './../worldmap/DotOverlay'
import WorldMapWidget from './../worldmap/WorldMapWidget'

import * as NotificationActions from './../../notifications/NotificationActions'
import * as Notification from './../../notifications/Notification'

import worldMapEventItemTransformer from './MapEventItem'
import * as WorldMapEventsWidgetActions from "./WorldMapEventsWidgetActions"

import * as common from "./../../common"

import $ from 'jquery'

import { connect } from 'react-redux'


class Category {

  constructor(name) {
    this.name = name
    this.items = Immutable.OrderedMap()
    this.active = false
  }

  addItem(item) {
    const c = new Category()
    c.name = this.name
    c.active = this.active
    c.items = this.items.set(item.id, item)
    return c
  }

  setActive() {
    this.active = true
  }

  setInactive() {
    this.active = false
  }
}

class WorldMapEventsWidgetModel {
  constructor(v = {categoriesMap:Immutable.OrderedMap(), activeCategory:null}) {
    this.value = v
  }

  updateCatagory(c, item) {
    const { categoriesMap } = this.value
    if(categoriesMap.has(c)) {
      const oldCat = categoriesMap.get(c)
      const newValue = Object.assign({}, this.value, {categoriesMap:categoriesMap.set(c, oldCat.addItem(item))})
      return new WorldMapEventsWidgetModel(newValue)
    }

    const newValue = Object.assign({}, this.value, {categoriesMap:categoriesMap.set(c, new Category(c).addItem(item))})
    return new WorldMapEventsWidgetModel(newValue)
  }

  update(items) {
    let newModel = this
    for(var item of items) {
      for(var c of item.categories) {
        newModel = newModel.updateCatagory(c, item)
      }
    }
    newModel.updateSelectedFilter()
    return newModel
  }

  updateSelectedFilter() {
    this.value.activeCategory = null
    let setNext = false
    let nextFound = false
    for(var entry of this.value.categoriesMap) {
      if(entry[1].active) {
        setNext = true
        entry[1].setInactive()
        continue
      }

      if(setNext) {
        entry[1].setActive()
        this.value.activeCategory = entry[1]
        nextFound = true
        break
      }
    }

    if(!setNext || (setNext && !nextFound)) {
      for(var entry of this.value.categoriesMap) {
        entry[1].setActive()
        this.value.activeCategory = entry[1]
        break
      }
    }
  }
}

const worldMapEventsWidgetIDGenerator = new common.IdGenerator("WorldMapEventsWidget")

@connect((store) => {
  return {
    environment : store.environment,
    notifications : store.notifications.notifications,
    worldMapEventsWidgetModels : store.worldMapEventsWidgetModels,
    themes : store.themes,
  }
})
export default class WorldMapEventsWidget extends React.Component {

  constructor() {
    super()
    this.widgetId = worldMapEventsWidgetIDGenerator.generate()
    this.updateTask = null
  }

  componentWillMount() {
    this.props.worldMapEventsWidgetModels[this.widgetId] = new WorldMapEventsWidgetModel()
    this.updateTask = setInterval(this._updateEvents.bind(this), 5000)
  }

  componentWillUnmount() {
    delete this.props.worldMapEventsWidgetModels[this.widgetId]
    if(this.updateTask) {
      clearInterval(this.updateTask)
    }
  }

  render() {
    const styles = this._getStyles()
    const model = this.props.worldMapEventsWidgetModels[this.widgetId]

    let catId = 0
    const categoriesComps = model.value.categoriesMap.map((c) => {
      return this._createFilterComp(++catId, c.name, c.items.size, c.active)
    })

    let overlays = new Immutable.List()
    if(model.value.activeCategory) {
      overlays = model.value.activeCategory.items.map(
        (item) => {
          return new DotOverlay(item.id, item.latlng.latitude, item.latlng.longitude)
        }
      )
    }

    return (<div class="fill-parent">
              <div  class="fill-parent layer-root" >
                <WorldMapWidget widgetId={this.worldMapId} overlays={overlays}/>
                <div style={styles.topleft}>
                    {categoriesComps}
                </div>
              </div>
            </div>)
  }

  _updateEvents() {
    const model = this.props.worldMapEventsWidgetModels[this.widgetId]
    const items = this._getCurrentWorldItems()
    this.props.dispatch(WorldMapEventsWidgetActions.updateWidgetEvents(this.widgetId, items))
  }

  _getCurrentWorldItems() {
    const { notifications } = this.props
    const newsItems = notifications.get(Notification.NEWS24_ITEM)

    if(newsItems) {
      return newsItems.notifications
        .map( (n) => worldMapEventItemTransformer.get(n) )
    }
    return Immutable.List()
  }

  _getStyles() {

    const { themes } = this.props
    const theme = themes.get(Themes.WORLDMAP_EVENTS_WIDGET_THEME)

    const container = {
      position: "relative"
    }

    const topleft = {
        position: "absolute",
        width:"auto",
        height:"80%",
        top: "50px",
        left: "10px",
        backgroundColor:theme.categoryPanelColor,
        opacity:theme.categoryPanelOpacity,
        overflow:"hidden",
    }

    return {container:container, topleft:topleft}
  }

  _createFilterComp(id, name, itemCount, active) {
    const styles = this._getStyles()
    let input = <input type="radio" name="type" value="EMAIL" />
    if(active) {
      input = <input type="radio" name="type" value="EMAIL" checked="checked" />
    }

    const displayName = name + " (" + itemCount + ")"
    return (<div key={id}>
      <label  class="worldevent-filter-button">
          {input}
          <i> {displayName}</i>
      </label>
      <br/>
    </div>)
  }
}
