import * as WorldMapEventsWidgetActions from "./WorldMapEventsWidgetActions"

const WorldMapEventsWidgetReducer = (state={}, action) => {
  const newState = Object.assign({}, state)
  switch(action.type) {
    case WorldMapEventsWidgetActions.UPDATE_CATEGORIES: {
        const { widgetId, items} = action.payload
        const model = state[widgetId]
        if(model) {
          newState[widgetId] = state[widgetId].update(items)
        }
    }
  }
  return newState
}
export default WorldMapEventsWidgetReducer
