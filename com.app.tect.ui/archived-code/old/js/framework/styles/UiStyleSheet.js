import Immutable from 'immutable'

export const STYLE_DEFAULT = 'default-style'

export class StyleSheet {
    constructor() {
        this.styles = new Immutable.Map()
    }
    set(id, value) {
        this.styles=this.styles.set(id, value)
        return this
    }
    get(id) {
        const cached = this.styles.get(id)
        if(cached) {
            return Object.assign({}, cached)
        }
        return {}
    }
    getList(ids) {
        let css = {}
        for(let index in ids) {
            const s = this.get(ids[index])
            if(s) {
                css = Object.assign({}, css, s)
            }
        }
        return css
    }
}

export default class UiStyleManager {
    constructor() {
        this.styleSheets = new Immutable.Map()
        this.__createSheet(STYLE_DEFAULT)
    }

    setStyle(styleId, css, sheetName=STYLE_DEFAULT) {
        const sheet = this.__getSheet(sheetName);
        this.styleSheets = this.styleSheets.set(sheet, sheet.set(styleId, css))
        return this
    }

    get(styleId, sheetName=STYLE_DEFAULT) {
        const sheet = this.__getSheet(sheetName);
        return sheet.get(styleId)
    }

    getList(styleIds, sheetName=STYLE_DEFAULT) {
        const sheet = this.__getSheet(sheetName);
        return sheet.getList(styleIds)
    }

    __getSheet(name){
        const sheet = this.styleSheets.get(name);
        if(sheet) return sheet
        return this.styleSheets.get(STYLE_DEFAULT)
    }
    __createSheet(name) {
        this.styleSheets = this.styleSheets.set(name, new StyleSheet())
        return this
    }
}
export const uiStyleManager = new UiStyleManager()