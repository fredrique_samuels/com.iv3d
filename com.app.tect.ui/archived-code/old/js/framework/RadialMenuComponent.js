import React from 'react'
import {RadialMenuView} from '../../../tectui/framework/comps/radialmenu/view/RadialMenuView'

class DemoContentHandler {
    createContent(layerModel) {
      const css = {
        position:"relative",
        left:"calc(50% - 150px)",
        top:"calc(50% - 150px)"
      }
      return <div style={css}><RadialMenuView menuItems={layerModel.contentParams.menuItems} layerModel={layerModel} /></div>
    }
}

export class RadialMenuManager {
  constructor(uicontext) {
    this.uicontext = uicontext
  }

  createLayeredRadialMenu(menuItems) {
      var contentParams = {
          contentHandlerFactory: new DemoContentHandler(),
          layoutParams: {
              position:"relative",
              top:"0%",
              left:"0%",
              width:"100%",
              height:"100%"
          },
          menuItems:menuItems
      };
      this.uicontext.layerComponentManager().addLayer(contentParams)
  }
}
