import React from 'react'

export default class LoadingDialogCloser {
    constructor(model) {
        this.model = model
    }

    close() {this.model.close()}
}