import React from 'react'
import LoadingModelBuilder from "./LoadingModelBuilder";

class SpinnerLoaderComponent extends React.Component {

    componentWillMount() {
        const {controller} = this.props
        const model = this.createModel()
        this.state = model.newState()
        if(controller) {
            controller.start(model)
        }
    }

    componentWillUnmount() {
        const {controller} = this.props
        const model = this.createModel()
        if(controller && model.busy() && controller.stop)controller.stop()
    }

    render() {
        const model = this.createModel();
        return (
            <div style={{textShadow:"0px 0px 20px black", color:"white", position:"relative", marginRight:"10%",marginBottom:"15%",marginLeft:"10%", marginTop:"15%", width:"80%"}} >
                <div class="center-content-h" style={{width:"100%"}}>
                    <i class="tt-status-icon fa fa-refresh fa-spin fa-fw fa-2x color-primary-text"></i>
                </div>
                <div style={{width:"100%", textAlign:"center"}}>
                    {model.text()}
                </div>
            </div>
        )
    }
    createModel() {
        return new LoadingModelBuilder(this)
            .setAutoClose()
            .build()
    }
}

export default class SpinLoaderComponentFactory {

    constructor(controller) {
        this.controller = controller
    }

    render(uicontext, layerModel) {
        return <SpinnerLoaderComponent layerModel={layerModel} controller={this.controller}/>
    }
}