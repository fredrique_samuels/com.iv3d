import LoadingModel from "./LoadingModel";

export default class LoadingModelBuilder {
    constructor(parent) {
        this.autoClose = false;
        this.parent = parent
    }
    setAutoClose(){this.autoClose=true;return this}
    build(){return new LoadingModel(this.parent, {autoClose:this.autoClose})}
}
