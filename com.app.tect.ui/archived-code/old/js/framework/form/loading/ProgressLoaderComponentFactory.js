import React from 'react'
import LoadingModelBuilder from "./LoadingModelBuilder";



class SuccessCheck extends React.Component {
    render() {
        const {model} = this.props
        if(!model.success())return null;
        return (
            <div style={{width: "100%", textAlign: "center"}}>
                <i class="fa-action fa fa-check-circle-o color-success-text fa-2x" onClick={model.close.bind(model)}></i>
            </div>
        )
    }
}

class FailCross extends React.Component {
    render() {
        const {model} = this.props
        if(!model.error())return null;
        return (
            <div style={{width: "100%", textAlign: "center"}}>
                <i class="fa-action fa fa-times-circle-o color-error-text fa-2x" onClick={model.close.bind(model)}></i>
            </div>
        )
    }
}

class ProgressLoaderComponent extends React.Component {

    render() {
        const model = this.createModel();
        const meterClasses = ["progress-meter"]
        if (model.busy()) {
            meterClasses.push("progress-meter-orange")
        } else {
            meterClasses.push("progress-meter-complete")
        }

        if (model.error()) {
            meterClasses.push("progress-meter-red")
        }

        return (
            <div style={{textShadow:"0px 0px 20px black", color:"white", position:"relative", marginRight:"10%",marginBottom:"15%",marginLeft:"10%", marginTop:"15%", width:"80%"}} >
                <div class={meterClasses.join(" ")} style={{width:"100%", height:"30px"}}>
                    <span style={{width:model.progressStrPerc()}} ></span>
                </div>
                {model.text()}
                <SuccessCheck model={model} />
                <FailCross model={model} />
            </div>
        )
    }

    componentWillMount() {
        const {controller} = this.props
        const model = this.createModel()
        this.state = model.newState()
        if(controller) {
            controller.start(model)
        }
    }

    componentWillUnmount() {
        const {controller} = this.props
        const model = this.createModel()
        if(controller && model.busy() && controller.stop)controller.stop()
    }

    createModel() {
        return new LoadingModelBuilder(this)
            .build()
    }
}

export default class ProgressLoaderComponentFactory {

    constructor(controller) {
        this.controller = controller
    }

    render(uicontext, layerModel) {
        return <ProgressLoaderComponent layerModel={layerModel} controller={this.controller}/>
    }
}