import React from 'react'
import {CELL_DISPLAY_TYPE_TEXT} from "./TableConstants";
import * as TableConstants from './TableConstants'
import AbstractParamsBuilder from "../../../../tectui/framework/comps/shared/builder/AbstractParamsBuilder";


class HeadingBuilder extends AbstractParamsBuilder {
    constructor(text, buildCallback) {
        super({
                key:text,
                display:text,
                info:null
            },
            buildCallback)
    }

    setKey(key){this.params.key = key; return this}
    setInfo(info){this.params.info = {description:info}; return this}
}


class ColumnBuilder extends AbstractParamsBuilder {
    constructor(headingKey, buildCallback) {
        super({key:headingKey, value:null, displayType:CELL_DISPLAY_TYPE_TEXT},buildCallback)
    }
    setValue(v){this.params.value = v;return this}
    setStatusSuccess() {
        return this.setValue({status:TableConstants.CELL_STATUS_SUCCESS})
                   .setDisplayType(TableConstants.CELL_DISPLAY_TYPE_STATUS)
    }
    setDisplayType(dt){this.params.displayType = dt;return this}
}

class RowBuilder extends AbstractParamsBuilder {
    constructor(buildCallback) {
        super({
            display:[]
        },buildCallback)
    }
    createColumn(headingKey){
        return new ColumnBuilder(headingKey, this.__addColumn.bind(this))
    }
    __addColumn(c){this.params.display.push(c);return this}
}


export default class TableViewBuilder extends AbstractParamsBuilder {
    constructor(buildCallback=null){
        super({
            headings:[],
            rows:[],
            actions:[],
            pageNumber:1,
            totalPages:1,
            prevPage:null,
            nextPage:null
        },buildCallback)
    }

    createHeading(text){return new HeadingBuilder(text, this.__addHeading.bind(this))}
    __addHeading(h){this.params.headings.push(h);return this}

    createRow(){return new RowBuilder(this.__addRow.bind(this))}
    __addRow(r){this.params.rows.push(r);return this}

    setPageNumber(n){this.params.pageNumber = n;return this}
    setTotalPages(n){this.params.totalPages = n;return this}
    setNextPageRowDataSource(n){this.params.nextPage = n;return this}
    setPrevPageRowDataSource(n){this.params.prevPage = n;return this}

    buildConfig(){return this.build()}
    buildToLayer(uicontext){uicontext.tableManager().createLayeredTable(this.buildConfig())}
    buildView(uicontext){return uicontext.tableManager().createTable(this.buildConfig())}
}
