import React from 'react'
import {CELL_DISPLAY_TYPE_TEXT, CELL_DISPLAY_TYPE_ACTIONS} from './TableConstants'

export default class TableRow extends React.Component {
    render() {
        const { rowParams, tableModel } = this.props
        const tdComps = rowParams.display.map( tdParams => this.getView(tableModel, tdParams))

        return (
            <tr>
                {tableModel.hasRowActions()?CELL_DISPLAY_TYPE_ACTIONS.transform(tableModel.uicontext(), rowParams.data, tableModel.actions()):null}
                {tdComps}
            </tr>
        )
    }
    getView(tableModel, tdParams) {
        const { key, value, displayType} = tdParams
        const header = tableModel.headings().find(h => h.key==key);
        if(!header)return null
        if(!header.visible) return null
        return displayType?displayType.transform(value):CELL_DISPLAY_TYPE_TEXT.transform(value)
    }
}