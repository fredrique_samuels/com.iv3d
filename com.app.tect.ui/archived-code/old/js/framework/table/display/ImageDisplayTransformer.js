import React from 'react'
import AbstractCellDisplayTransformer from "../AbstractCellDisplayTransformer";

export default class ImageDisplayTransformer extends AbstractCellDisplayTransformer {
    constructor() {
        super()
    }

    transform(value) {
        const comp = (
            <div style={{width:"100%",height:"70px"}}>
                <img src={value.url} style={{width:"100%",height:"100%"}} />
            </div>
        )
        return this.createDataCell(comp)
    }
}