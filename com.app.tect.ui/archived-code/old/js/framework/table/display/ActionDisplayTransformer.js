import React from 'react'
import AbstractCellDisplayTransformer from '../AbstractCellDisplayTransformer'
import {AbstractActionConfigBuilder} from "../../../../../tectui/framework/comps/action/builder/AbstractActionConfigBuilder";

class MenuItemActionProxy {
    constructor(action, uicontext, rowData) {
        this.action = action
        this.rowData = rowData
        this.uicontext = uicontext
    }
    execute(){
        if(this.action)this.action(this.rowData, this.uicontext)
    }
}

class RowActionProxy {
    constructor(uicontext, rowdata, actions) {
        this.rowdata = rowdata
        this.actions = actions
        this.uicontext = uicontext
    }

    execute() {
        const items = this.getItemList()
        this.uicontext.radialMenuManager().createLayeredRadialMenu({items:items})
    }

    getItemList() {
        if(this.actions){
            const na =  this.actions.map(a => {
                const miap = new MenuItemActionProxy(a.getAction(), this.uicontext, this.rowdata)
                return new AbstractActionConfigBuilder().set(a).setAction(miap.execute.bind(miap)).build()
            })
            return na
        }
        return []
    }
}

export default class ActionDisplayTransformer extends AbstractCellDisplayTransformer {

    constructor() {
        super()
    }

    transform(uicontext, rowdata, actions) {
        const ap = new RowActionProxy(uicontext, rowdata, actions)
        return (
            <td class="tt-table-data-cell">
                <i class="tt-status-icon fa fa-th fa-action color-default-text" onClick={ap.execute.bind(ap)}></i>
            </td>
            )
    }
}