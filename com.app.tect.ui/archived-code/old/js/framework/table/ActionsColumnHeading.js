import React from 'react'

export default class ActionsColumnHeading extends React.Component {

    render() {
        const {tableModel} = this.props
        if(!tableModel.hasRowActions())return null

        return (
            <th class="tt-table-data-cell">
                ACTIONS
            </th>
        )
    }
}