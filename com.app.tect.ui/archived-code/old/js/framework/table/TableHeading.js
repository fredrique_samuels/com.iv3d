import React from 'react'

export default class TableHeading extends React.Component {
    render() {
        const {header, applyIds} = this.props
        const {visible, display, info, widgetId} = header
        if(!visible)return null
        const infoComp = info?<i class="fa fa-info-circle tt-th-info "></i>:null

        return (
            <th id={applyIds?widgetId:null} class="tt-table-data-cell">
                {infoComp}
                {display}
            </th>
        )
    }
}