

export default class TableModel {

    constructor(parent) {
        this.parent = parent
    }

    state(){return this.parent.state}
    setState(s) {
        this.parent.setState(s)
    }
    uicontext(){return this.parent.props.uicontext}
    init(){
        const {table} = this.parent.props
        this.initHeaderStates()
        this.parent.state = {
            table:table,
            headings:table.headings,
            scrollContainerId:this.parent.widgetId+"_tt-table-scroll-panel",
            currentIndex:0,
            groupSize:0,
            maxIndex:0,
            leftCount:0,
            rightCount:0
        }
    }
    hasPaging(){return this.state().table.totalPages>1}
    pageCountText(){
        const {totalPages, pageNumber, rows} = this.state().table
        return pageNumber + "/" + totalPages + " [" + rows.length + "]"
    }
    pageUp(){
        const {nextPage} = this.state().table
        if(nextPage) {
            nextPage(this.updateTableData.bind(this))
        }
    }
    pageDown(){
        const {prevPage} = this.state().table
        if(prevPage) {
            prevPage(this.updateTableData.bind(this))
        }
    }
    updateTableData(newTable) {
        const table = this.state().table
        table.rows = newTable.rows
        table.pageNumber = newTable.pageNumber
        table.nextPage = newTable.nextPage
        table.prevPage = newTable.prevPage
        this.setState(
            {
                table:table,
            }
        )
    }
    headings(){return this.state().headings}
    hasRowActions() {
        return this.state().table.actions && this.state().table.actions.length>0
    }
    actions(){return this.state().table.actions}
    groupSize(){return this.state().groupSize}
    leftCount(){return this.state().leftCount}
    rightCount(){return this.state().rightCount}

    initHeaderStates() {
        const {table} = this.parent.props
        const headings = table.headings;
        headings.forEach(h => {
            h.widgetId = this.widgetId + "_" + h.key
            h.visible = true
        })
    }

    initScrollState() {
        const headings = this.headings();
        let updateView = false;
        headings.forEach( (h, i) => {
            const headerHidden = this.isHeaderHidden(h.widgetId) && i!=0;
            if(headerHidden && h.visible) {
                h.visible = false;
                updateView = true;
            } else if(!headerHidden && !h.visible) {
                h.visible = true;
                updateView = true;
            }
        })

        if(updateView) {
            var currentIndex =  headings.findIndex((f,i) => f.visible && i!=0);
            var groupSize = headings.filter((f,i) => f.visible && i!=0).length;
            var maxIndex = headings.length - groupSize;
            var leftCount = currentIndex-1;
            var rightCount = maxIndex-currentIndex;
            const scrollState = {headings, currentIndex, groupSize, maxIndex, leftCount, rightCount};
            this.setState(scrollState)
        }
    }
    isHeaderHidden(hId) {
        const element = document.getElementById(hId)
        const parent =  document.getElementById(this.state().scrollContainerId)//$('#' + this.state.scrollContainerId);
        return this.isChildHidden(parent, element)
    }


    isChildHidden(parent, child) {
        return child.offsetTop + child.offsetHeight >
            parent.offsetTop + parent.offsetHeight ||
            child.offsetLeft + child.offsetWidth >
            parent.offsetLeft + parent.offsetWidth
    }

    updateColumnScrollState(currentIndex) {
        const state = this.state();
        const { headings, maxIndex, groupSize } = state
        headings.forEach((f,i)=> f.visible=(i>=currentIndex && i<=currentIndex+ groupSize-1) || i==0 )
        var leftCount = currentIndex-1;
        var rightCount = maxIndex-currentIndex;
        this.setState({headings, currentIndex, leftCount, rightCount})
    }

    scrollColumnsLeft() {
        const state = this.state();
        const currentIndex = state.currentIndex
        if(currentIndex>1) {
            this.updateColumnScrollState(currentIndex-1);
        }
    }

    scrollColumnsRight() {
        const state = this.state();
        const { currentIndex, maxIndex } = state
        if(currentIndex<maxIndex) {
            this.updateColumnScrollState(currentIndex+1);
        }
    }
}