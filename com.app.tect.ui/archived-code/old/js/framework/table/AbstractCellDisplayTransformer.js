import React from 'react'
import DataCell from './DataCell'

export default class AbstractCellDisplayTransformer {
    transform(value){return this.createDataCell(new String(value))}
    createDataCell(comp, options={}){
        const optionParams = Object.assign({},
            {cssClasses:[]},
            options)
        return <DataCell cssClasses={optionParams.cssClasses} >{comp}</DataCell>
    }
}
