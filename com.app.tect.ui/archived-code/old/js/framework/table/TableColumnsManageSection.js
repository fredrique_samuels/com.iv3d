import React from 'react'

export default class TableColumnsManageSection extends React.Component {
    render() {
        const { tableModel } = this.props

        if(tableModel.groupSize()==0)return null;

        return (
            <tr>
                {tableModel.hasRowActions()?<td></td>:null}
                <td>KEY</td>
                <td class="tt-table-column-manage-panel" colSpan={""+tableModel.groupSize()}>
                    {tableModel.leftCount()>0?
                        <i class="fa-action tt-table-scroll-left" onClick={tableModel.scrollColumnsLeft.bind(tableModel)}>
                            <i class="fa fa-arrow-left"></i>
                            <i class="tt-table-scroll-index">{tableModel.leftCount()}</i>
                        </i>:
                        null}
                    <i class="fa-action"><i class="tt-table-data-action" class="fa fa-list"></i>DATA</i>
                    {tableModel.rightCount()>0?
                        <i class="fa-action tt-table-scroll-right" onClick={tableModel.scrollColumnsRight.bind(tableModel)}>
                            <i class="tt-table-scroll-index">{tableModel.rightCount()}</i>
                            <i class="fa fa-arrow-right"></i>
                        </i>:null}
                </td>
            </tr>
        )
    }
}