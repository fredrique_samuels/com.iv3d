

class Iv3dMath {
  static toRadians (degrees) {
      return degrees * Math.PI / 180
  }
  static toDegrees (radians) {
      return radians * 180 / Math.PI
  }
}

export default Iv3dMath
