

class DevEnv {
  isDev(){return true}
  debug(obj){console.log(obj)}
}

class ProdEnv {
  isDev(){return false}
  debug(obj){}
}


const activeEnv = new DevEnv
// const activeEnv = new ProdEnv

const EnvironmentReducer = (state={environment:activeEnv}, action) => {
  return state;
}

export default EnvironmentReducer
