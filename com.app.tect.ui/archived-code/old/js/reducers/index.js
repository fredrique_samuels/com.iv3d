import { combineReducers } from 'redux'

import UserReducer from './UserReducer'
import TweetsReducer from './TweetsReducer'
import TodosReducer from './TodosReducer'

import EnvironmentReducer from './EnvironmentReducer'
import NotificationsReducer from './../notifications/NotificationsReducer'
import ThemesReducer from './../themes/ThemesReducer'

import WorldMapEventsWidgetReducer from './../components/worldevents/WorldMapEventsWidgetReducer'
import ComponentModelsReducer from './../components/ComponentModels'

export default combineReducers({
  /// used for testing
  // user: UserReducer,
  // tweets: TweetsReducer,
  // todos: TodosReducer,

  //domain
  environment: EnvironmentReducer,
  notifications: NotificationsReducer,
  themes: ThemesReducer,
  componentModels : ComponentModelsReducer,

  //widgets
  //TODO This can be done using a state
  worldMapEventsWidgetModels : WorldMapEventsWidgetReducer,
})
