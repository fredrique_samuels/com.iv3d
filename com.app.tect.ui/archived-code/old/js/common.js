
// const globalDispatcher = {d:null}
//
// export default class Dispatcher {
//
//   static setGlobal(dispatcher) {
//     globalDispatcher.d = dispatcher
//   }
//
//   static getGlobal() {
//     return globalDispatcher.d
//   }
// }

export class Pager {
  
}


const globalSeed = {s:1}
export class IdGenerator {
  constructor(prefix) {
    this.seed = 1
    this.prefix = prefix
  }

  generate() {
    return this.prefix + (this.seed++)
  }

  static generateGlobalId() {
    globalSeed.s = globalSeed.s+1
    return "ui_comp_"+globalSeed.s
  }
}

export class LatLng {
  constructor(latitude, longitude) {
    this.latitude = latitude
    this.longitude = longitude
  }

  getLongitude(){return this.longitude}
  getLatitude(){return this.latitude}
}

export class Point2D {
  constructor(x, y) {
    this.x = x
    this.y = y
  }
}

export class Timer {
    constructor(interval, doneCallback){
        this.timer = setInterval(doneCallback, interval);
    }

    stop() {
        clearInterval(this.timer)
    }
}

export class Timeout {
    constructor(interval, doneCallback){
        this.timer = setTimeout(doneCallback, interval)
    }

    stop() {
        clearTimeout(this.timer)
    }
}
