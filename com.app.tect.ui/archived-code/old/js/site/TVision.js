import React from 'react'
import { connect } from 'react-redux'

import * as App from './../components/ApplicationContentComponent'


import ApplicationContentComponent from './../components/ApplicationContentComponent'
import ServiceManager from './../domain/ServiceManager'
import WorldMapEventsWidget from './../components/worldevents/WorldMapEventsWidget'


import * as TestData from './../components/TestData'
import {setDispatcher} from "../../../tectui/tectui";



@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export default class TVision extends React.Component {

  constructor() {
    super()
    this.services = new ServiceManager()
    // console.log(dispatcher())
  }

  componentWillMount() {
    setDispatcher(this.props.dispatch)
    this.services.startAll()
    ApplicationContentComponent.createDefaultModel(this.props.dispatch)
    TestData.testLayers(this.props.dispatch, App.DEFAULT_APP_CONTENT_COMPONENT)
    // TestData.demoEventData(this.props.dispatch)
  }

  componentWillUnmount() {
    this.services.stopAll()
  }

  componentDidUpdate() {
  }

  render() {

    const g = {
      backgroundColor:"black"
    }

    const img = {
      width:"100%",
      height:"100%",
    }

    const mainAppContentId = App.DEFAULT_APP_CONTENT_COMPONENT
    const worldMap = (<div class="fill-parent-absolute" >
      <div class="fill-parent layer-root">
        <WorldMapEventsWidget />
      </div>
    </div>)

    const contentArea = {
      position:"relative",
      width:"100%",
      height:"100%",
    }

    const contentLayer = <ApplicationContentComponent modelId={mainAppContentId} />

    const menuItems = []
    menuItems.push()

    return ( <div class="fill-parent" style={contentArea}>
        <div class="fill-parent" style={g}>
          <img src="res/images/blue-gradient-background-2.jpg" style={img}/>
        </div>
        {worldMap}
        {contentLayer}
    </div> )
  }
}
