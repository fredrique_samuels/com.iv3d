'use strict'

import axios from 'axios'
import React from 'react'
import ReactDom from 'react-dom'
import store from './../store'

import { Provider } from 'react-redux'

import SiteHome from '../../../sites/SiteHome'

const app = document.getElementById('app')
ReactDom.render(
  <Provider store={store}>
    <SiteHome />
  </Provider>,
  app)
