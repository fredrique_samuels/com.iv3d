'use strict'

import axios from 'axios'
import React from 'react'
import ReactDom from 'react-dom'
import store from './../store'

import { Provider } from 'react-redux'

import AppDataNodeEditDemo from './AppDataNodeEditDemo'

const app = document.getElementById('app')
ReactDom.render(
  <Provider store={store}>
    <AppDataNodeEditDemo />
  </Provider>,
  app)
