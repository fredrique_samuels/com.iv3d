/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.jfx.control;

import java.util.ArrayList;

/**
 * Builder used to create String representation of a function call. 
 */
public final class FunctionCallCodeBuilder {
	private String name;
	private ArrayList<String> arguments;
	
	/**
	 * Default Constructor.
	 */
	public FunctionCallCodeBuilder() {
		arguments = new ArrayList<String>();
	}
	
	/**
	 * Set the function name.
	 * 
	 * @param name The function name.
	 * @return {@link FunctionCallCodeBuilder}
	 */
	public final FunctionCallCodeBuilder setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Add an argument value.
	 * @param argument an argument value.
	 * @return {@link FunctionCallCodeBuilder}
	 */
	public final FunctionCallCodeBuilder addArgument(String argument) {
		this.arguments.add(argument);
		return this;
	}

	/**
	 * Build the function call.
	 * @return The function call string.
	 */
	public String build() {
		StringBuilder builder = new StringBuilder();
		builder.append(name);
		builder.append("(");
		for(int i=0;i<arguments.size();i++) {
			if(i>0) 
				builder.append(",");
			builder.append(arguments.get(i));
		}
		builder.append(")");
		return builder.toString();
	}
	
}
