/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.jfx.control;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class JsObjectCodeBuilder {

	private Map<String, String> values;
	
	public JsObjectCodeBuilder() {
		values = new LinkedHashMap<String, String>();
	}
	
	public final String build() {
		StringBuilder builder = new StringBuilder();
		builder.append("JsObjectCodeBuilder_var={");
		appendValues(builder);
		builder.append("}");
		return builder.toString();
	}

	private void appendValues(StringBuilder builder) {
		int counter = 0;
		for(Entry<String, String> entry : values.entrySet()) {
			if(counter>0) {
				builder.append(",");
			}
			builder.append(entry.getKey());
			builder.append(":");
			builder.append(entry.getValue());
			counter++;
		}
	}

	public final JsObjectCodeBuilder set(String property, String value) {
		values.put(property, value);
		return this;
	}

}
