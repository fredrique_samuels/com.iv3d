/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.jfx.control;

import com.iv3d.common.core.Callback;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.ui.PropertiesContainer;
import com.iv3d.common.ui.Property;
import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskStatus;
import com.iv3d.common.utils.ExceptionUtils;
import javafx.collections.ObservableMap;
import org.apache.log4j.Logger;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

class JsContext {
	private final String VIEW_SETTER_NAME = "__set_view__";
	private final String VIEW_GETTER_NAME = "__get_view__";
	private static Logger logger = Logger.getLogger(JsContext.class);
	private final ScriptEngine jsEngine;
	private final ObservableMap<String,Object> fxIds;
	private final ViewStateSetterFunctionImpl viewStateSetter;
	private final ViewStateGetterFunctionImpl viewStateGetter;
	private final FxControllerProvider controllerSource;

	public JsContext(String jsFile, ObservableMap<String,Object> jsNamespace, FxControllerProvider controllerSource) {
		this.fxIds = jsNamespace;
		this.controllerSource = controllerSource;
		this.jsEngine = initJsEngine(jsFile);
		this.viewStateSetter = createViewSetter(jsNamespace);;
		this.viewStateGetter = createViewGetter(jsNamespace);
	}

	private ViewStateSetterFunctionImpl createViewSetter(
			ObservableMap<String, Object> jsNamespace) {
		Object viewSetterFunction = jsNamespace.containsKey(VIEW_SETTER_NAME)?jsNamespace.get(VIEW_SETTER_NAME):null;
		return new ViewStateSetterFunctionImpl(viewSetterFunction);
	}

	private ViewStateGetterFunctionImpl createViewGetter(
			ObservableMap<String, Object> jsNamespace) {
		Object viewGetterFunction = jsNamespace.containsKey(VIEW_GETTER_NAME)?jsNamespace.get(VIEW_GETTER_NAME):null;
		return new ViewStateGetterFunctionImpl(this.jsEngine, viewGetterFunction);
	}

	private ScriptEngine initJsEngine(String jsFile) {
		if(jsFile==null) {
			return null;
		}
		
		String script = ResourceLoader.readAsString(jsFile);
		if(script==null) {
			return null;
		}
		
		ScriptEngine jsEngine = new ScriptEngineManager()
				.getEngineByExtension("js");
		try {
			jsEngine.eval(script);
		} catch (ScriptException e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		}

		return jsEngine;
	}
	
	public final Callback<UiTaskStatus> getUpdateCallable(Object callback) {
		return new UpdateCallBackImpl(callback);
	}

	public final Callback<UiTaskResult> getDoneCallable(Object callback) {
		if (callback == null)
			return null;
		return new DoneCallbackImpl(callback);
	}

	public final ViewModelSetterFunction getViewStateSetterCallable() {
		return viewStateSetter;
	}

	public final ViewModelGetterFunction getViewStateGetterCallable() {
		return viewStateGetter;
	}

	public final Object getObjectAttributeOrNull(String attr, Object taskParams) {
		if(jsEngine==null) {
			return null;
		}
		jsEngine.getContext().setAttribute("__taskParams", Integer.valueOf(0),
				ScriptContext.ENGINE_SCOPE);
		try {
			return jsEngine.eval(String.format("__taskParams.%s", attr));
		} catch (ScriptException e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}

	private void setupEnvironment() {
		if(jsEngine==null) {
			return;
		}
		for (Entry<String, Object> entry : fxIds.entrySet()) {
			jsEngine.getContext().setAttribute(entry.getKey(), entry.getValue(),
					ScriptContext.ENGINE_SCOPE);
		}

		jsEngine.getContext().setAttribute("controller", controllerSource.get(),
				ScriptContext.ENGINE_SCOPE);
	}

	public final Object executeFunctionObject(Object function, Object... args) {
		if(jsEngine==null) {
			return null;
		}
		String name = "__function";
		jsEngine.getContext().setAttribute(name, function,
				ScriptContext.ENGINE_SCOPE);
		ScriptFunction scriptFunction = getScriptFunction(name, args);
		return executeFunction(scriptFunction);
	}
	
	public final Object executeFunctionByName(String functionName, Object... args) {
		ScriptFunction scriptFunction = getScriptFunction(functionName, args);
		return executeFunction(scriptFunction);
	}

	private final Object executeFunction(ScriptFunction scriptFunction) {
		setupEnvironment();
		try {
			return scriptFunction.invoke();
		} catch (Exception e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}

	private ScriptFunction getScriptFunction(String name, Object... args) {
		JsFunctionBuilder jsFunctionCallBuilder = new JsFunctionBuilder()
				.setJsEngine(jsEngine).setFunctionName(name);

		for (Object arg : args) {
			jsFunctionCallBuilder.addArgument(arg);
		}

		ScriptFunction scriptFunction = jsFunctionCallBuilder.build();
		return scriptFunction;
	}

	private class UpdateCallBackImpl implements Callback<UiTaskStatus> {

		private final Object callback;

		public UpdateCallBackImpl(Object callback) {
			this.callback = callback;
		}

		@Override
		public void invoke(UiTaskStatus status) {
			if(callback==null){return;}
			executeFunctionObject(callback, status);
		}
	}

	private class DoneCallbackImpl implements Callback<UiTaskResult> {

		private final Object callback;

		public DoneCallbackImpl(Object callback) {
			this.callback = callback;
		}

		@Override
		public void invoke(final UiTaskResult result) {
			new FxRunner() {
				
				@Override
				public void run() {
					executeFunctionObject(callback, result);
				}
			}.execute();
		}
	}

	private class ViewStateSetterFunctionImpl implements
			ViewModelSetterFunction {

		private final Object callback;

		public ViewStateSetterFunctionImpl(Object viewSetterFunction) {
			this.callback = viewSetterFunction;
		}

		@Override 
		public void invoke(final PropertiesContainer model) {
			if(callback==null){return;}
			new FxRunner() {
				
				@Override
				public void run() {
					if(model instanceof  ViewModelImpl) {
						try {
							executeFunctionObject(callback, ((ViewModelImpl)model).getJsObject());
						} catch (ScriptException e) {
							logger.warn(ExceptionUtils.getStackTrace(e));
						}
					}
				}
			}.execute();
		}
	}

	private class ViewStateGetterFunctionImpl implements
			ViewModelGetterFunction {

		private final ScriptEngine scriptEngine;
		private final Object viewGetterFunction;

		public ViewStateGetterFunctionImpl(ScriptEngine jsEngine,
				Object viewGetterFunction) {
			this.scriptEngine = jsEngine;
			this.viewGetterFunction = viewGetterFunction;
		}

		@Override
		public PropertiesContainer invoke() {
			if(viewGetterFunction!=null) {
				Object jsState = executeFunctionObject(viewGetterFunction);
				return new ViewModelImpl(getAttributeList(jsState));
			}
			return new ViewModelImpl();
		}
		
		protected Map<String, Object> getAttributeList(Object jsObject) {
			Map<String, Object> result = new HashMap<String, Object>();
			if(jsObject==null) {return result;}

			this.scriptEngine.getContext().setAttribute("__jsObject", jsObject,
					ScriptContext.ENGINE_SCOPE);
			this.scriptEngine.getContext().setAttribute("__result", result,
					ScriptContext.ENGINE_SCOPE);
			try {
				this.scriptEngine
						.eval("for (x in __jsObject) {__result.put(x, __jsObject[x]);}");
			} catch (ScriptException e) {
				logger.warn(ExceptionUtils.getStackTrace(e));
			}
			return result;
		}
	}

	private class ViewModelImpl implements PropertiesContainer {

		private final Map<String, Object> values;

		public ViewModelImpl(Map<String, Object> valueMap) {
			this.values = valueMap;
		}
		
		public ViewModelImpl() {
			this(new HashMap<String, Object>());
		}
		
		protected  Object getJsObject() throws ScriptException {
			JsObjectBuilder builder = new JsObjectBuilder();
			for(Entry<String, Object> entry: values.entrySet()) {
				builder.set(entry.getKey(), entry.getValue());
			}
			return builder.build();
		}

		@Override
		public Property getProperty(final String property) {
			return new Property() {
				
				@Override
				public void setValue(Object value) {
					if(isUsable()) {
						values.put(property, value);
					}
				}
				
				@Override
				public boolean isUsable() {
					return values.containsKey(property);
				}
				
				@Override
				public <T> T getValue(Class<T> type) {
					return isUsable()?type.cast(values.get(property)):null;
				}
			};
		}
	}

	public final PropertiesContainer getViewModel() {
		return this.viewStateGetter.invoke();
	}
	
	public final void setViewModel(PropertiesContainer model) {
		this.viewStateSetter.invoke(model);
	}

}
