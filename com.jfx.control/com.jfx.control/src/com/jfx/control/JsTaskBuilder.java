/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.jfx.control;

import com.iv3d.common.core.Callback;
import com.iv3d.common.ui.*;
import com.iv3d.common.ui.executor.UiTaskExecutor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public final class JsTaskBuilder {
	private JfxTask task;
	private Object updateViewFunction;
	private Object doneFunction;
	private long timeout;
	private Map<String, Object> params;
	private JsContext jsContext;
	private NodeLookup nodeLookup;
	private ExecutorService executorService;
	
	public JsTaskBuilder() {
		this.timeout = 0;
		this.params = new HashMap<String, Object>();   
	}
	
	final JsTaskBuilder setTask(JfxTask task) {
		this.task = task;
		return this;
	}

	final JsTaskBuilder setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
		return this;
	}

	public final JsTaskBuilder setUpdateFunction(Object jsfunction) {
		this.updateViewFunction = jsfunction;
		return this;
	}

	public final JsTaskBuilder setDoneFunction(Object jsFunction) {
		this.doneFunction = jsFunction;
		return this;
	}

	public final JsTaskBuilder setTimeout(long milliseconds) {
		this.timeout = milliseconds;
		return this;
	}

	public final JsTaskBuilder setParam(String param, Object value) {
		params.put(param, value);
		return this;
	}
	
	final JsTaskBuilder setJsContext(JsContext jsContext) {
		this.jsContext = jsContext;
		return this;
	}

	final JsTaskBuilder setNodeLookup(NodeLookup nodeLookup) {
		this.nodeLookup = nodeLookup;
		return this;
	}

	public final UiCancelable execute() {
		if(task==null) {
			return new NoOpCancelable();
		}
		
		DefaultTaskContext context = getUiContext();
		UiTaskExecutor<JfxTask> executor = getExecutor(context);
		executor.execute(task);
		return context;
	}

	private UiTaskExecutor<JfxTask> getExecutor(DefaultTaskContext context) {
		UiTaskExecutor<JfxTask> executor;
		final PropertiesContainer viewModel = jsContext.getViewModel();
		Callback<UiTaskResult> postExecuteCallable = getPostCallExecutable(viewModel);
		executor = new UiTaskExecutor<JfxTask>(executorService, context)
				.setTimeout(timeout)
				.setDoneCallback(postExecuteCallable)
				.setTaskBootstrap(new JfxBoostrap(viewModel));
		return executor;
	}

	private DefaultTaskContext getUiContext() {
		Callback<UiTaskStatus> updateCallable = jsContext.getUpdateCallable(updateViewFunction);
		DefaultTaskContext context = new DefaultTaskContext();
		context.setTaskUpdateCallback(updateCallable);
		if(params!=null) {
			for(Map.Entry<String, Object> entry: params.entrySet()) {
				context.addParam(entry.getKey(), entry.getValue());				
			}
		}
		return context;
	}

	private Callback<UiTaskResult> getPostCallExecutable(final PropertiesContainer viewModel) {
		
		final Callback<UiTaskResult> doneCallable = jsContext.getDoneCallable(doneFunction);
		final ViewModelSetterFunction viewStateSetterCallable = jsContext.getViewStateSetterCallable();
		
		Callback<UiTaskResult> postExecuteCallable = new Callback<UiTaskResult>() {
			
			@Override
			public void invoke(UiTaskResult result) {
				viewStateSetterCallable.invoke(viewModel);
				if(doneCallable!=null)
					doneCallable.invoke(result);
			}
		};
		return postExecuteCallable;
	}
	
	class JfxBoostrap implements UiTaskBootstrap<JfxTask> {

		private PropertiesContainer viewModel;

		public JfxBoostrap(PropertiesContainer viewModel) {
			this.viewModel = viewModel;
		}

		@Override
		public void bootstrap(JfxTask task) {
			task.setNodeSource(nodeLookup);
			task.setPropertySource(viewModel);
		}
	}
	
	class NoOpCancelable implements UiCancelable {

		@Override
		public void cancel() {
			
		}
	}
}
