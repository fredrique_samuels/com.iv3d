/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.jfx.control;

import com.iv3d.common.ui.UiTaskProvider;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * Builder for creating a {@link FxProfile}.
 */
public final class FxProfileBuilder {
	private String jsFile;
	private String fxmlFile;
	private UiTaskProvider<JfxTask> taskProvider;
	private ViewInitializer viewInitializer;
	private PropertyChangeListener propertyChangeListener;
	private ModelPropertiesInitializer propertiesInitializer;
	private final ExecutorService executorService;
	private Map<String, String> initSettings;
	
	/**
	 * Default constructor. 
	 */
	public FxProfileBuilder(ExecutorService service) {
		executorService = service;
	}
	
	/**
	 * Set settings to be used by the initialization object to setup
	 * the view.
	 * @param initSettings
	 */
	public final FxProfileBuilder setParams(Map<String, String> initSettings) {
		this.initSettings = Collections.unmodifiableMap(initSettings);
		return this;
	}

	/**
	 * Set the path of the FXML file.
	 * 
	 * @param fxmlFile The FXML file.
	 * @return The builder.
	 */
	public final FxProfileBuilder setFxmlFile(String fxmlFile) {
		this.fxmlFile = fxmlFile;
		return this;
	}

	/**
	 * Set the task provider.
	 * 
	 * @param provider The task provider.
	 * @return The builder.
	 */
	public final FxProfileBuilder setTaskProvider(UiTaskProvider<JfxTask> provider) {
		this.taskProvider = provider;
		return this;
	}

	/**
	 * Set the model properties initializer.
	 * 
	 * @param propertiesInitializer The model properties initializer.
	 * @return The builder.
	 */
	public final FxProfileBuilder setModelPropertyInitializer(ModelPropertiesInitializer propertiesInitializer) {
		this.propertiesInitializer = propertiesInitializer;
		return this;
	}

	/**
	 * The name of the java script file without the path.
	 * 
	 * @param jsFile The name of the java script file without the path.
	 * @return The builder.
	 */
	public final FxProfileBuilder setJsFile(String jsFile) {
		this.jsFile = jsFile;
		return this;
	}

	
	/**
	 * Set the view initializer.
	 * @param viewInitializer The {@link ViewInitializer}.
	 * @return The builder.
	 */
	public final FxProfileBuilder setViewInitializer(ViewInitializer viewInitializer) {
		this.viewInitializer = viewInitializer;
		return this;
	}

	/**
	 * Set the property changer listener on the parent node.
	 * 
	 * @param propertyChangeListener Set the property changer listener on the parent node.
	 * @return The builder.
	 */
	public final FxProfileBuilder setPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
		this.propertyChangeListener = propertyChangeListener;
		return this;
	}

	/**
	 * Build the profile.
	 * 
	 * @return The {@link FxProfile} created.
	 */
	public FxProfile build() {
		FxProfileImpl profile = new FxProfileImpl(fxmlFile, 
				jsFile, taskProvider, propertiesInitializer, viewInitializer,
				initSettings, propertyChangeListener,
				executorService);
		return profile;
	}
	
}
