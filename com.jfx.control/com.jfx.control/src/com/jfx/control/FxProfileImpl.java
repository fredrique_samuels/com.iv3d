/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.jfx.control;

import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.ui.PropertiesContainer;
import com.iv3d.common.ui.UiTaskProvider;
import com.iv3d.common.utils.ExceptionUtils;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Region;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutorService;

final class FxProfileImpl implements FxProfile {
	private final static Logger logger = Logger.getLogger(FxProfileImpl.class);

	private final FXMLLoader loader;
	private final Parent parent;
	private final DefaultJavaFxController controller;
	private final ExecutorService executorService;

	FxProfileImpl(String fxmlFileName, String jsFile, UiTaskProvider<JfxTask> taskFactory,
			ModelPropertiesInitializer propertyModelInitializer,
			ViewInitializer viewInitializer,
			Map<String, String> initSettings, 
			PropertyChangeListener propertyChangeListener,
			ExecutorService executorService) {
		loader = new FXMLLoader();
		controller = new DefaultJavaFxController();
		controller.setTaskFactory(taskFactory);
		loader.setController(controller);
		parent = loadParent(fxmlFileName, jsFile);
		this.executorService = executorService;
		
		initUi(getParentUrlOrNull(fxmlFileName), jsFile, 
				viewInitializer,
				propertyChangeListener,
				initSettings);
		controller.initViewProperties(propertyModelInitializer, initSettings);
	}

	@Override
	public final Parent getParent() {
		return parent;
	}

	@Override
	public void setTaskFactory(UiTaskProvider<JfxTask> provider) {
		controller.setTaskFactory(provider);
	}

	private URL getParentUrlOrNull(String filename) {
		try {
			URL url = ResourceLoader.getUrl(filename);
			String path = url.getFile();
			File parentFile = new File(path).getParentFile();
			if (parentFile == null) {
				return null;
			}
			return parentFile.toURI().toURL();
		} catch (MalformedURLException e) {
			return null;
		}
	}

	private Parent loadParent(String fxmlFileName, String jsFile) {
		try {
			URL url = ResourceLoader.getUrl(fxmlFileName);
			InputStream inputStream = getInputStream(url, jsFile);
			URL parentUrlOrNull = getParentUrlOrNull(fxmlFileName);
			logger.info("Setting base location to " + url.getPath());
			loader.setLocation(parentUrlOrNull);
			return (Parent) loader.load(inputStream);
		} catch (IOException e) {
			throw new LoadingError(e);
		}
	}

	private JsContext setupJsEngineContext(String jsFile) {
		FxControllerProvider controllerSource = new FxControllerProvider() {
			@Override
			public FxController get() {
				return controller;
			}
		};
		JsContext jsContext = new JsContext(jsFile, loader
				.getNamespace(), controllerSource);
		controller.setContext(jsContext);
		controller.setNodeLookup(getNodeLookup());
		return jsContext;
	}

	private void initUi(URL parentFolder, String jsFile,
			ViewInitializer viewInitializer, PropertyChangeListener propertyChangeListener, Map<String, String> initSettings) {
		initJsModel(parentFolder, jsFile);
		initParentNode(viewInitializer, initSettings);
		initPropertyListener(propertyChangeListener);
	}

	private void initPropertyListener(
			PropertyChangeListener propertyChangeListener) {
		if(propertyChangeListener==null)
			return;
		if(parent instanceof Region) {
			Region region = (Region) parent;
			region.heightProperty().addListener(new ParentPropertyChangeListener("height", propertyChangeListener));
			region.widthProperty().addListener(new ParentPropertyChangeListener("width", propertyChangeListener));
		}
	}

	private void initParentNode(ViewInitializer viewInitializer, Map<String, String> initSettings) {
		if (viewInitializer != null) {
			try {
				viewInitializer.setup(parent, loader.getNamespace(),
						initSettings);
			} catch (Exception e) {
				logger.warn(ExceptionUtils.getStackTrace(e));
			}
		}
	}

	private void initJsModel(URL parentFolder, String jsFile) {
		if(jsFile==null) {
			return;
		}
		try {
			String parentFolderPath = parentFolder.getFile();
			File file = new File(parentFolderPath, jsFile);

			JsContext jsContext = setupJsEngineContext(
					file.exists() ? file .getPath() : null);
			jsContext.executeFunctionByName("__init__");
		} catch (Exception e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		}
	}

	private InputStream getInputStream(URL url, String jsFile) {
		InputStream read = ResourceLoader.read(url.getPath());
		return getJsFileToStream(read, url.getPath(), jsFile);
	}

	private InputStream getJsFileToStream(InputStream read, String path,
			String jsFile) {
		try {
			Document doc = getDocumentFromFxmlFile(path);
			Element root = getRootElement(doc);
			insertJsSourceToDoc(doc, root, jsFile);
			return docToInputStream(doc);
		} catch (ParserConfigurationException pce) {
			throw new ParserError(pce);
		} catch (SAXException e) {
			throw new ParserError(e);
		} catch (IOException e) {
			throw new ParserError(e);
		}
	}

	private void insertJsSourceToDoc(Document doc, Element root, String jsFile) {
		if (jsFile == null)
			return;
		Element script = doc.createElement("fx:script");
		script.setAttribute("source", jsFile);
		Element rootElement = getRootElement(root);
		root.insertBefore(script, rootElement);

		ProcessingInstruction pi = doc.createProcessingInstruction("language",
				"javascript");
		root.insertBefore(pi, script);
	}

	private Document getDocumentFromFxmlFile(String path)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(path);
		return doc;
	}

	private Element getRootElement(Node doc) {
		NodeList childNodes = doc.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node item = childNodes.item(i);
			if (item.getNodeType() == Node.ELEMENT_NODE) {
				return (Element) item;
			}
		}
		throw new NoRootElementError();
	}

	private InputStream docToInputStream(Node doc) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		Source xmlSource = new DOMSource(doc);
		Result outputTarget = new StreamResult(outputStream);
		try {
			TransformerFactory.newInstance().newTransformer().transform(
					xmlSource, outputTarget);
		} catch (TransformerConfigurationException e) {
			throw new TransformError(e);
		} catch (TransformerException e) {
			throw new TransformError(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new TransformError(e);
		}
		return new ByteArrayInputStream(outputStream.toByteArray());
	}

	private class DefaultJavaFxController implements FxController {
		private final Logger logger = Logger
				.getLogger(DefaultJavaFxController.class);
		private UiTaskProvider<JfxTask> factory;
		private JsContext jsContext;
		private NodeLookup nodeLookup;


		public void setNodeLookup(NodeLookup nodeLookup) {
			this.nodeLookup = nodeLookup;
		}

		public final void setTaskFactory(UiTaskProvider<JfxTask> factory) {
			this.factory = factory;
		}

		public final JsTaskBuilder buildTask(String id) {
			return new JsTaskBuilder()
				.setNodeLookup(nodeLookup)
				.setTask(getTask(id))
				.setExecutorService(executorService)
				.setJsContext(jsContext);
		}

		private JfxTask getTask(String id) {
			if (factory == null) {
				return null;
			}

			JfxTask task = factory.getTask(id);
			if (task == null) {
				String s = String.format(
						"Unable to create task with id %s using factory %s",
						id, factory);
				logger.warn(s);
			}
			return task;
		}

		public void setContext(JsContext jsContext) {
			this.jsContext = jsContext;
		}

		@Override
		public void initViewProperties(
				ModelPropertiesInitializer modelPropertiesInitializer, 
				Map<String, String> initSettings) {
			if (modelPropertiesInitializer == null || jsContext==null) {
				return;
			}
			PropertiesContainer model = jsContext.getViewModel();
			modelPropertiesInitializer.setup(model, initSettings);
			jsContext.setViewModel(model);
		}

	}

	public final class FileNotAnFxmlFile extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public FileNotAnFxmlFile(String filename) {
			super("File name must end with '.fxml' was " + filename);
		}
	}

	public final class LoadingError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public LoadingError(IOException e) {
			super(e);
		}

	}

	public final class ParserError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ParserError(Exception pce) {
			super(pce);
		}
	}

	public final class TransformError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public TransformError(Throwable e) {
			super(e);
		}
	}

	public final class NoRootElementError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public NoRootElementError() {
			super();
		}
	}
	
	private class ParentPropertyChangeListener implements ChangeListener<Object> {

		private final String property;
		private final PropertyChangeListener listener;

		public ParentPropertyChangeListener(String property, PropertyChangeListener listener) {
			this.property = property;
			this.listener = listener;
		}
		
		@Override
		public void changed(ObservableValue<? extends Object> arg0,
				Object oldValue, Object newValue) {
			listener.onChange(parent, property, oldValue, newValue);
		}
		
	}

	@Override
	public NodeLookup getNodeLookup() {
		return new NodeLookup() {

			@Override
			public javafx.scene.Node lookup(String id) {
				Object object = loader.getNamespace().get(id);
				if (object instanceof javafx.scene.Node) {
					return (javafx.scene.Node) object;
				}
				return null;
			}
		};
	}
}
