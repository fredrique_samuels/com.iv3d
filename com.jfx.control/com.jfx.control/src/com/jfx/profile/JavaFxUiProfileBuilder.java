package com.jfx.profile;

import com.iv3d.common.core.ResourceLoader;
import com.iv3d.ui.profile.UiProfileBuilder;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutorService;

/**
 * Created by fred on 2016/09/06.
 */
public class JavaFxUiProfileBuilder implements UiProfileBuilder {

    private static Logger logger = Logger.getLogger(JavaFxUiProfileBuilder.class);

    private ExecutorService executorService;
    private final String fxmlFile;
    private final FXMLLoader loader;
    private JavaFxController controller;
    private String jsFile;

    public JavaFxUiProfileBuilder(ExecutorService executorService, String fxmlFile) {
        this.executorService = executorService;
        this.fxmlFile = fxmlFile;
        this.loader = new FXMLLoader();
    }

    @Override
    public JavaFxUiProfile build() {
        return new UiProfileImpl(this);
    }

    private Parent loadParent() {
        try {
            URL url = ResourceLoader.getUrl(fxmlFile);
            InputStream inputStream;
            if(jsFile!=null) {
                inputStream = getInputStream(url, jsFile);
            } else {
                inputStream = ResourceLoader.read(url);
            }
            URL parentUrlOrNull = ResourceLoader.getParentUrlOrNull(fxmlFile);
            logger.info("Setting base location to " + url.getPath());
            loader.setLocation(parentUrlOrNull);
            loader.setController(controller);
            if(controller!=null) {
                controller.setExecutorServiceProvider(() -> executorService);
            }
            return (Parent) loader.load(inputStream);
        } catch (IOException e) {
            throw new LoadingError(e);
        }
    }

    public JavaFxUiProfileBuilder setController(JavaFxController controller) {
        this.controller = controller;
        return this;
    }

    public JavaFxUiProfileBuilder setJsFile(String jsFile) {
        this.jsFile = jsFile;
        return this;
    }

    private InputStream getInputStream(URL url, String jsFile) {
        InputStream read = ResourceLoader.read(url.getPath());
        return getJsFileToStream(read, url.getPath(), jsFile);
    }

    private InputStream getJsFileToStream(InputStream read, String path,
                                          String jsFile) {
        try {
            Document doc = getDocumentFromFxmlFile(path);
            Element root = getRootElement(doc);
            insertJsSourceToDoc(doc, root, jsFile);
            return docToInputStream(doc);
        } catch (ParserConfigurationException pce) {
            throw new ParserError(pce);
        } catch (SAXException e) {
            throw new ParserError(e);
        } catch (IOException e) {
            throw new ParserError(e);
        }
    }

    private void insertJsSourceToDoc(Document doc, Element root, String jsFile) {
        if (jsFile == null)
            return;
        Element script = doc.createElement("fx:script");
        script.setAttribute("source", jsFile);
        Element rootElement = getRootElement(root);
        root.insertBefore(script, rootElement);

        ProcessingInstruction pi = doc.createProcessingInstruction("language",
                "javascript");
        root.insertBefore(pi, script);
    }

    private Document getDocumentFromFxmlFile(String path)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(path);
        return doc;
    }

    private Element getRootElement(org.w3c.dom.Node doc) {
        NodeList childNodes = doc.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            org.w3c.dom.Node item = childNodes.item(i);
            if (item.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                return (Element) item;
            }
        }
        throw new NoRootElementError();
    }

    private InputStream docToInputStream(org.w3c.dom.Node doc) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(doc);
        Result outputTarget = new StreamResult(outputStream);
        try {
            TransformerFactory.newInstance().newTransformer().transform(
                    xmlSource, outputTarget);
        } catch (TransformerConfigurationException e) {
            throw new TransformError(e);
        } catch (TransformerException e) {
            throw new TransformError(e);
        } catch (TransformerFactoryConfigurationError e) {
            throw new TransformError(e);
        }
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    public final static class LoadingError extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public LoadingError(IOException e) {
            super(e);
        }

    }

    public final static class ParserError extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public ParserError(Exception pce) {
            super(pce);
        }
    }

    public final static class TransformError extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public TransformError(Throwable e) {
            super(e);
        }
    }

    public final static class NoRootElementError extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public NoRootElementError() {
            super();
        }
    }

    private class UiProfileImpl implements JavaFxUiProfile {

        private final Parent parent;

        public UiProfileImpl(JavaFxUiProfileBuilder b) {
            this.parent = b.loadParent();

            if(b.controller!=null) {
                b.controller.setProfile(this);
                b.controller.initNode();
            }
        }

        @Override
        public Parent getParent() {
            return parent;
        }

        @Override
        public NodeLookup getNodeLookup() {
            return id -> {
                Object object = loader.getNamespace().get(id);
                if (object instanceof Node) {
                    return (Node) object;
                }
                return null;
            };
        }
    }
}
