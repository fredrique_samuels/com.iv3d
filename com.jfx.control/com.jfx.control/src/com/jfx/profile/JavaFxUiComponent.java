package com.jfx.profile;

import com.iv3d.ui.profile.UiComponent;
import javafx.scene.Node;

/**
 * Created by fred on 2016/09/06.
 */
public class JavaFxUiComponent implements UiComponent {
    private final Node node;

    public JavaFxUiComponent(Node node) {
        this.node = node;
    }

    @Override
    public void setText(String text) {
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public void select(String text) {

    }

    @Override
    public void deselect(String text) {

    }

    @Override
    public void disable(String text) {

    }

    @Override
    public void enable(String text) {

    }

    @Override
    public void hide() {

    }

    @Override
    public void show() {

    }

    @Override
    public void add(UiComponent component) {

    }

    @Override
    public void remove(UiComponent component) {

    }

    @Override
    public UiComponent getComponent(String id) {
        return null;
    }
}
