package com.jfx.profile;

import com.iv3d.ui.profile.UiProfile;
import javafx.scene.Parent;

/**
 * Created by fred on 2016/09/06.
 */
public interface JavaFxUiProfile extends UiProfile {
    Parent getParent();
    NodeLookup getNodeLookup();
}
