package com.jfx.profile;

import com.iv3d.common.core.Callback;
import com.iv3d.common.core.Provider;
import com.iv3d.common.ui.*;
import com.iv3d.common.ui.executor.UiTaskExecutor;
import com.iv3d.ui.profile.UiTaskBuilder;
import javafx.scene.Node;
import javafx.scene.Parent;

import java.util.concurrent.ExecutorService;

/**
 * Created by fred on 2016/09/06.
 */
public class JavaFxController extends MappedTaskProvider<UiTask>{
    protected Parent parent;
    private NodeLookup lookup;
    private Provider<ExecutorService> serviceProvider;

    public void initNode(){}
    public final Node findNodeById(String fxid){
        Node lookup = this.lookup.lookup(fxid);
        if(lookup==null) {
            throw new NodeNotFoundError(fxid);
        }
        return lookup;
    }

    public Node getNode(){return parent;}
    public void execute(String command, Object ... args) {
    }

    public UiTaskBuilder createTaskRunner(UiTask task) {
        return new UiTaskBuilderImpl(getExecutorService(), task);
    }

    private UiTaskExecutor<UiTask> getExecutor(DefaultTaskContext context) {
        return new UiTaskExecutor<>(getExecutorService(), context)
                .setTimeout(5000);
    }

    void setProfile(JavaFxUiProfile uiProfile){
        this.parent = uiProfile.getParent();
        this.lookup = uiProfile.getNodeLookup();
    }

    void setExecutorServiceProvider(Provider<ExecutorService> serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    private ExecutorService getExecutorService() {
        if(serviceProvider==null) {
            return null;
        }
        return serviceProvider.get();
    }

    private static class UiTaskBuilderImpl implements UiTaskBuilder {

        private ExecutorService executorService;
        private UiTask task;
        private Callback<UiTaskResult> doneAction;
        private long timeout=0;
        private Callback<UiTaskStatus> statusUpdateAction;

        public UiTaskBuilderImpl(ExecutorService executorService, UiTask task) {
            this.executorService = executorService;
            this.task = task;
        }

        @Override
        public UiTaskBuilder setDoneAction(Callback<UiTaskResult> doneAction) {
            this.doneAction = doneAction;
            return this;
        }

        @Override
        public UiTaskBuilder setStatusUpdateAction(Callback<UiTaskStatus> statusUpdateAction) {
            this.statusUpdateAction = statusUpdateAction;
            return this;
        }

        @Override
        public UiTaskBuilder setTimeout(long timeout) {
            this.timeout = timeout;
            return this;
        }

        @Override
        public UiCancelable execute() {
            DefaultTaskContext context = new DefaultTaskContext();
            context.setTaskUpdateCallback(statusUpdateAction);
            UiTaskExecutor<UiTask> executor = new UiTaskExecutor<>(executorService, context)
                    .setTimeout(timeout)
                    .setDoneCallback(doneAction);

            executor.execute(task);
            return context;
        }
    }

    private class NodeNotFoundError extends RuntimeException {
        public NodeNotFoundError(String fxid) {
            super("Node with id '"+fxid+"' could not be located.");
        }
    }
}
