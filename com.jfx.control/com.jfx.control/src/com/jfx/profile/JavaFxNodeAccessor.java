package com.jfx.profile;

import javafx.scene.Node;
import javafx.scene.control.Labeled;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextInputControl;

/**
 * Created by fred on 2016/09/07.
 */
public final class JavaFxNodeAccessor {
    public final String getText(Node node) {
        if(node instanceof TextInputControl) {
            return ((TextInputControl)node).getText();
        }
        else if(node instanceof Labeled) {
            return ((Labeled)node).getText();
        }
        return null;
    }

    public final void setText(Node node, String text) {
        if(node instanceof TextInputControl) {
            ((TextInputControl)node).setText(text);
        }
        else if(node instanceof Labeled) {
            ((Labeled)node).setText(text);
        }
    }

    public final void disable(Node node) {
        node.setDisable(true);
    }

    public final void enable(Node node) {
        node.setDisable(false);
    }

    public void setProgress(Node node, double v) {
        if(node instanceof ProgressIndicator) {
            ((ProgressIndicator)node).setProgress(v);
        }

    }
}
