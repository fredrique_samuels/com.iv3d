package com.iv3d.ui.profile;

/**
 * Created by fred on 2016/09/06.
 */
public interface UiComponent {
    void setText(String text);
    String getText();

    void select(String text);
    void deselect(String text);

    void disable(String text);
    void enable(String text);

    void hide();
    void show();

    void add(UiComponent component);
    void remove(UiComponent component);

    UiComponent getComponent(String id);
}
