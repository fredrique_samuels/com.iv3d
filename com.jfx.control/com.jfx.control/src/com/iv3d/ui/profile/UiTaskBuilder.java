package com.iv3d.ui.profile;

import com.iv3d.common.core.Callback;
import com.iv3d.common.ui.UiCancelable;
import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskStatus;

/**
 * Created by fred on 2016/09/07.
 */
public interface UiTaskBuilder {
    UiTaskBuilder setDoneAction(Callback<UiTaskResult> doneAction);
    UiTaskBuilder setStatusUpdateAction(Callback<UiTaskStatus> statusUpdateAction);
    UiTaskBuilder setTimeout(long timeout);
    UiCancelable execute();
}
