package com.iv3d.ui.profile;


public interface UiProfileBuilder {
    UiProfile build();
}
