package com.jfx.control;

import com.iv3d.common.ui.UiTask;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskResultBuilder;
import com.jfx.profile.JavaFxController;
import com.jfx.profile.JavaFxNodeAccessor;
import com.jfx.profile.JavaFxUiProfile;
import com.jfx.profile.JavaFxUiProfileBuilder;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ActionWithControllersDemo extends Application {


    class MyController extends JavaFxController {

        JavaFxNodeAccessor nodeAccessor = new JavaFxNodeAccessor();

        @Override
        public void execute(String command, Object... args) {
            if("print_task".equals(command)) {
                createTaskRunner(new PrintTask(this, nodeAccessor))
                        .setTimeout(5000)
                        .execute();
            }
        }
    }
	
	public static void main(String[] args) {
		Application.launch(ActionWithControllersDemo.class, args);
	}

	public void start(Stage stage) throws Exception {
		/**
		 * Set the task factory 
		 * using #setTaskFactory()
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(5);
        JavaFxUiProfile fxUiProfile = new JavaFxUiProfileBuilder(executorService,
                "/com/jfx/control/demo/resources/controller_actions.fxml")
                .setController(new MyController())
			    .setJsFile("controller_actions.js")
                .build();
        Parent parent = fxUiProfile.getParent();
	
		// Stage and show :D
	    stage.setTitle("FXML Welcome");
		stage.setScene(new Scene(parent, 600, 275));
	    stage.show();
	}

	class PrintTask implements UiTask {

        private JavaFxController controller;
        private JavaFxNodeAccessor nodeAccessor;

        public PrintTask(JavaFxController controller, JavaFxNodeAccessor nodeAccessor) {
            this.controller = controller;
            this.nodeAccessor = nodeAccessor;
        }

        @Override
        public UiTaskResult run(UiTaskContext context) {
            String text = nodeAccessor.getText(controller.findNodeById("inputField"));
            new FxRunner() {
                @Override
                public void run() {
                    nodeAccessor.setText(controller.findNodeById("output"), text);
                }
            }.execute();

            return new UiTaskResultBuilder()
                    .build();
        }
    }
}
