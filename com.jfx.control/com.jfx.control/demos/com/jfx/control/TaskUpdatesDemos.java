package com.jfx.control;

import com.iv3d.common.core.Callback;
import com.iv3d.common.core.Percentage;
import com.iv3d.common.core.ThreadUtils;
import com.iv3d.common.ui.*;
import com.jfx.profile.JavaFxController;
import com.jfx.profile.JavaFxNodeAccessor;
import com.jfx.profile.JavaFxUiProfile;
import com.jfx.profile.JavaFxUiProfileBuilder;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TaskUpdatesDemos extends Application {

    class MyController extends JavaFxController {

        JavaFxNodeAccessor nodeAccessor = new JavaFxNodeAccessor();
        private UiCancelable currentTask;

        @Override
        public void initNode() {
            new FxRunner(){
                @Override
                public void run() {
                    nodeAccessor.disable(findNodeById("cancelButton"));
                }}.execute();

        }

        @Override
        public void execute(String command, Object... args) {
            if("cancel".equals(command) && currentTask!=null) {
                currentTask.cancel();
            }
            else if("run".equals(command)) {
                Callback<UiTaskStatus> statusUpdateAction = status -> {
                    new FxRunner(){
                        @Override
                        public void run() {
                            Percentage progress = status.getPercentage();
                            if(progress!=null) {
                                nodeAccessor.setProgress(findNodeById("progressBar"),
                                        0.01*progress.get());
                            }
                        }
                    }.execute();
                };

                Callback<UiTaskResult> doneAction = arg -> {
                    new FxRunner(){
                        @Override
                        public void run() {
                            nodeAccessor.enable(findNodeById("runButton"));
                            nodeAccessor.disable(findNodeById("cancelButton"));
                            nodeAccessor.setText(findNodeById("output"),
                                    String.valueOf(arg.getValue()));
                        }
                    }.execute();
                };

                new FxRunner(){
                    @Override
                    public void run() {
                        nodeAccessor.disable(findNodeById("runButton"));
                        nodeAccessor.enable(findNodeById("cancelButton"));
                    }}.execute();

                currentTask = createTaskRunner(new ProgressTask(this, nodeAccessor))
                        .setTimeout(0)
                        .setDoneAction(doneAction)
                        .setStatusUpdateAction(statusUpdateAction)
                        .execute();
            }
        }
    }
	
	public static void main(String[] args) {
		Application.launch(TaskUpdatesDemos.class, args);
	}

	public void start(Stage stage) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        JavaFxUiProfile fxUiProfile = new JavaFxUiProfileBuilder(executorService,
                "/com/jfx/control/demo/resources/task_progress.fxml")
                .setController(new MyController())
                .setJsFile("task_progress.js")
                .build();
        Parent parent = fxUiProfile.getParent();
	
		// Stage and show :D
	    stage.setTitle("FXML Welcome");
		stage.setScene(new Scene(parent, 600, 275));
	    stage.show();
	}

    class ProgressTask implements UiTask {

        private JavaFxController controller;
        private JavaFxNodeAccessor nodeAccessor;

        public ProgressTask(JavaFxController controller, JavaFxNodeAccessor nodeAccessor) {
            this.controller = controller;
            this.nodeAccessor = nodeAccessor;
        }

        @Override
        public UiTaskResult run(UiTaskContext context) {
            for(int i=0;i<100 && !context.isCanceled() ;++i) {
                System.out.println(i);
                UiTaskStatus status = new UiTaskStatusBuilder().setPercentage(new Percentage(i)).build();
                context.postUpdate(status);
                ThreadUtils.delay(50);
            }

            return new UiTaskResultBuilder()
                    .setValue(context.isCanceled()?"Task Canceled":"Task Done")
                    .build();
        }
    }
}
