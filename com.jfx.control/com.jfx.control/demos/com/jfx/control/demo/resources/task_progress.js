var task;

function __init__() {
	cancelButton.setDisabled(true);
}

// This method is bound to the print button
// Remember to set the editor input to
// Script Mode and set
// the script string to 'printAction(event)'
function runAction(event) {
    controller.execute("run", event);
}

// Called when the task is completed.
function runTaskDone(result) {
	runButton.setDisable(false);
	output.setText(result.getValue());
}

function cancelAction(event) {
    controller.execute("cancel", event);
}

function updateTaskStatus(status) {
	progress = status.getPercentage();
	if(progress) {
		progressBar.setProgress(0.01*progress.get())
	}
}