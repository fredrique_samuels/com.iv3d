/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.ui.javafx.controller;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.jfx.control.JsFunctionBuilder;
import com.jfx.control.ScriptFunction;

import junit.framework.TestCase;

public class JsFunctionCallBuilderTest extends TestCase {
	
	private ScriptEngine getJsEngine(String code) throws ScriptException {
		ScriptEngine jsEngine = new ScriptEngineManager().getEngineByExtension("js");
		jsEngine.eval(code);
		return jsEngine;
	}
	
	public void testNoArgs() throws Exception {
		ScriptEngine jsEngine = getJsEngine("function test_func(){return 'called';}");
		
		JsFunctionBuilder builder = new JsFunctionBuilder();
		builder.setFunctionName("test_func");
		builder.setJsEngine(jsEngine);
		
		ScriptFunction function = builder.build();
		assertEquals("called", function.invoke());
	}
	

	public void testNoFunctionNameSet() throws Exception {
		ScriptEngine jsEngine = getJsEngine("function test_func(){return 'called';}");
		
		JsFunctionBuilder builder = new JsFunctionBuilder();
		builder.setJsEngine(jsEngine);
	
		ScriptFunction function = builder.build();
		try {
			function.invoke();
			fail();
		} catch (ScriptException e) {
			
		}
	}
	
	public void testOneArg() throws Exception {
		ScriptEngine jsEngine = getJsEngine("function test_func(arg0){return arg0;}");
		
		JsFunctionBuilder builder = new JsFunctionBuilder();
		builder.setFunctionName("test_func");
		builder.addArgument("test_value");
		builder.setJsEngine(jsEngine);
		
		ScriptFunction function = builder.build();
		assertEquals("test_value", function.invoke());
	}
	
	public void testManyArgs() throws Exception {
		ScriptEngine jsEngine = getJsEngine("function test_func(arg0, arg1, arg3){return arg0+arg1+arg3;}");
		
		JsFunctionBuilder builder = new JsFunctionBuilder();
		builder.setFunctionName("test_func");
		builder.addArgument("test_value");
		builder.addArgument("test_value2");
		builder.addArgument("test_value3");
		builder.setJsEngine(jsEngine);
		
		ScriptFunction function = builder.build();
		assertEquals("test_valuetest_value2test_value3", function.invoke());
	}
}
