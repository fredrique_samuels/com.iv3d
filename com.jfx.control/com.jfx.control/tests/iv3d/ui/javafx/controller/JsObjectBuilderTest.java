/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.ui.javafx.controller;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.jfx.control.JsObjectBuilder;

import junit.framework.TestCase;

public class JsObjectBuilderTest extends TestCase {
	
	private ScriptEngine getJsEngine(Object obj) throws ScriptException {
		ScriptEngine jsEngine = new ScriptEngineManager().getEngineByExtension("js");
		jsEngine.getContext().setAttribute("target", obj, ScriptContext.ENGINE_SCOPE);
		return jsEngine;
	}
	
	public void testNoFunctionName() throws ScriptException {
		assertNotNull(new JsObjectBuilder().build());
	}
	
	public void testOnePropertyBuilder() throws ScriptException {
		Object object = new JsObjectBuilder()
			.set("p0", "v0")
			.build();

		ScriptEngine jsEngine = getJsEngine(object);
		assertEquals("v0", jsEngine.eval("target['p0']"));
	}
	
	public void testTwoPropertyBuilder() throws ScriptException {
		Object object = new JsObjectBuilder()
			.set("p0", "v0")
			.set("p1", "v1")
			.build();

		ScriptEngine jsEngine = getJsEngine(object);
		assertEquals("v0", jsEngine.eval("target['p0']"));
		assertEquals("v1", jsEngine.eval("target['p1']"));
	}
	
	public void testManyPropertyBuilder() throws ScriptException {
		Object object = new JsObjectBuilder()
			.set("p0", "v0")
			.set("p1", "v1")
			.set("p2", "v2")
			.set("p3", "v3")
			.build();
	
		ScriptEngine jsEngine = getJsEngine(object);
		assertEquals("v0", jsEngine.eval("target['p0']"));
		assertEquals("v1", jsEngine.eval("target['p1']"));
		assertEquals("v2", jsEngine.eval("target['p2']"));
		assertEquals("v3", jsEngine.eval("target['p3']"));
	}
	
	public void testDuplicatePropertyBuilder() throws ScriptException {
		Object object = new JsObjectBuilder()
			.set("p0", "v0")
			.set("p0", "v1")
			.build();
	
		ScriptEngine jsEngine = getJsEngine(object);
		assertEquals("v1", jsEngine.eval("target['p0']"));
	}
}
