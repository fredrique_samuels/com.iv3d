/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.ui.javafx.controller;

import com.jfx.control.JsObjectCodeBuilder;

import junit.framework.TestCase;

public class JsObjectCodeBuilderTest extends TestCase {
	
	public void testBuilder() {
		JsObjectCodeBuilder builder = new JsObjectCodeBuilder();
		String code = builder.build();
		assertEquals("JsObjectCodeBuilder_var={}", code);
	}
	
	public void testOnePropertyBuilder() {
		String code = new JsObjectCodeBuilder()
			.set("p0", "v0")
			.build();
		assertEquals("JsObjectCodeBuilder_var={p0:v0}", code);
	}
	
	public void testTwoPropertyBuilder() {
		String code = new JsObjectCodeBuilder()
			.set("p0", "v0")
			.set("p1", "v1")
			.build();
		assertEquals("JsObjectCodeBuilder_var={p0:v0,p1:v1}", code);
	}
	
	public void testManyPropertyBuilder() {
		String code = new JsObjectCodeBuilder()
			.set("p0", "v0")
			.set("p1", "v1")
			.set("p2", "v2")
			.set("p3", "v3")
			.build();
		assertEquals("JsObjectCodeBuilder_var={p0:v0,p1:v1,p2:v2,p3:v3}", code);
	}
	
	public void testDuplicatePropertyBuilder() {
		String code = new JsObjectCodeBuilder()
			.set("p0", "v0")
			.set("p0", "v1")
			.build();
		assertEquals("JsObjectCodeBuilder_var={p0:v1}", code);
	}
}
