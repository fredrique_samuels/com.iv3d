/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.ui.javafx.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.iv3d.common.core.Percentage;
import com.iv3d.common.ui.MappedTaskProvider;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskResultBuilder;
import com.iv3d.common.ui.UiTaskStatus;
import com.iv3d.common.ui.UiTaskStatusBuilder;
import com.jfx.control.FxProfile;
import com.jfx.control.FxProfileBuilder;
import com.jfx.control.JfxTask;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TestFXMLController extends Application {
	
	class LoginUiTask extends JfxTask {

		@Override
		public UiTaskResult run(UiTaskContext context) {
			String user = context.getParam("username", String.class);
			String password = context.getParam("password", String.class);

			if(user.isEmpty()) {
				return new UiTaskResultBuilder()
					.setMessage("No user specified!")
					.setException(new Exception())
					.build();
			}
			
			doLogin(context, user);
			
			if(context.isCanceled()) {
				UiTaskStatus status = new UiTaskStatusBuilder().setMessage("Canceling Login").build();
				context.postUpdate(status);
				delay(1000);
			}
			
			if("root".equals(password)) {
				return new UiTaskResultBuilder()
					.setMessage("Login succeded")
					.build();
			} 
			
			return new UiTaskResultBuilder()
				.setMessage("Login failed. Passsword must be 'root' :P")
				.setException(new Exception())
				.build();
		}

		private void doLogin(UiTaskContext context, String user) {
			int progress = 0;
			while (!context.isCanceled() && progress <= 100) {
				UiTaskStatus status = new UiTaskStatusBuilder()
					.setMessage("Attempting Login for user " + user)
					.setPercentage(new Percentage(progress))
					.build();
				context.postUpdate(status);
				delay(50);
				progress+=1;
			}
		}

		private void delay(int ms) {
			try {
				Thread.sleep(ms);
			} catch (InterruptedException e) {
				// ignore
			}
		}		
	}

	@Override
	public void start(Stage stage) throws Exception {
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(20);
		
		MappedTaskProvider<JfxTask> factory = new MappedTaskProvider<JfxTask>();
		factory.addTask("login", new LoginUiTask());
		
		FxProfile profile = new FxProfileBuilder(newFixedThreadPool)
			.setFxmlFile("./test_resources/demo.fxml")
			.setTaskProvider(factory)
			.setJsFile("demo.js")
			.build();
		
        stage.setTitle("FXML Welcome");
        stage.setScene(new Scene(profile.getParent(), 600, 275));
        stage.show();
	}
	
	public static void main(String[] args) {
		Application.launch(TestFXMLController.class, args);
	}

}
