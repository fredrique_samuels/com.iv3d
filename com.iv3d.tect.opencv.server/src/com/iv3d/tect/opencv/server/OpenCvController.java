package com.iv3d.tect.opencv.server;

import com.iv3d.common.server.AbstractController;
import com.iv3d.common.server.AbstractRequestHandler;
import com.iv3d.common.server.PathHandler;
import org.eclipse.jetty.server.Request;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by fred on 2017/10/01.
 */
public class OpenCvController extends AbstractController {
    @Override
    public void configure() {
        set("/ping", new AbstractRequestHandler() {
            @Override
            public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                returnOkResponse(response);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });


        set("/process", new ProcessHandler());
    }
}
