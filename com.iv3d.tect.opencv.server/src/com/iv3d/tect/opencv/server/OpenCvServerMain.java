package com.iv3d.tect.opencv.server;


import com.app.server.base.server.HttpSessionService;
import com.iv3d.common.server.RequestPathResolver;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;

/**
 * Created by fred on 2017/10/01.
 */
public class OpenCvServerMain {
    public static void main(String[] args) throws Exception {
        Server server = new Server( 59180 );

        RequestPathResolver requestPathResolver = new RequestPathResolver();
        requestPathResolver.addController(new OpenCvController());

        HandlerList handlerList = new HandlerList();
        handlerList.setHandlers(new Handler[]{requestPathResolver});

        server.setHandler( handlerList );

        // Start the server
        server.start();
        server.join();
    }
}
