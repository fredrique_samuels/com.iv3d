package com.iv3d.myworld.core.platform;

/**
 * Created by fred on 2017/10/07.
 */
public interface LoggingManager {
    void info(Class<?> aClass, String s);
    void error(Class<?> aClass, String message);

    void info(String packageId, String message);
}
