package com.iv3d.myworld.core.platform;

import com.iv3d.common.async.AsyncMonitor;
import com.iv3d.myworld.core.platform.plugin.Plugin;
import com.iv3d.myworld.core.platform.plugin.PluginId;
import com.iv3d.myworld.core.platform.plugin.PluginInstallFile;

import java.util.List;

/**
 * Created by fred on 2017/10/07.
 */
public interface PluginManager {
    AsyncMonitor installCorePluginsIfNotInstalled();
    AsyncMonitor installPlugins(List<PluginInstallFile> pluginInstallFiles);

    Plugin getPlugin(PluginId pluginId);
    void runMain();
    void loadPlugins();
}
