package com.iv3d.myworld.core.platform.config;

import com.iv3d.myworld.core.platform.plugin.PluginId;

import java.util.List;

/**
 * Created by fred on 2017/10/07.
 */
public interface ApplicationConfig {
    int getCommandThreadPoolSize();
    int getUiThreadPoolSize();

    String getPluginsHomePath();
    String getEntryPluginId();
    List<PluginId> getCorePluginIds();
}
