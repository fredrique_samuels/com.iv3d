package com.iv3d.myworld.core.platform;

import com.iv3d.common.core.Percentage;
import com.iv3d.common.async.AsyncMonitor;
import com.iv3d.myworld.core.platform.plugin.PluginInstallFile;

import java.util.List;

/**
 * Created by fred on 2017/10/07.
 */
public interface SplashScreenManager {
    void showSplashScreen();
    void hideSplashScreen();

    AsyncMonitor showMissingInstallFiles(List<PluginInstallFile> missingInstallFiles);
    AsyncMonitor showInstallProgress(String message, Percentage percentage);
}
