package com.iv3d.myworld.core.platform.plugin.config;

import com.iv3d.common.beans.DefaultBeanConfig;

/**
 * Created by fred on 2017/10/10.
 */

/*
<service>
    <bean>
        <path>iv3d.dashboard.service</path>
        <bean>Iv3dDashboardService.Iv3dDashboardService</bean>
        <language>jython</language>
    </bean>
</service>
 */
public class PluginServiceDescription {
    private DefaultBeanConfig bean;

    public DefaultBeanConfig getBean() {
        return bean;
    }

    public void setBean(DefaultBeanConfig bean) {
        this.bean = bean;
    }
}
