package com.iv3d.myworld.core.platform.plugin;

/**
 * Created by fred on 2017/10/07.
 */
public abstract class PluginId {
    public abstract String getName();
    public boolean isValid(){return getName()!=null;}

    public static PluginId forName(String name) {
        return new PluginIdImpl(name);
    }

    private static class PluginIdImpl extends PluginId {

        private final String name;

        public PluginIdImpl(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return String.format("[PluginId : '%s']", name);
        }
    }
}
