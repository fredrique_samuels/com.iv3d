package com.iv3d.myworld.core.platform.plugin;

/**
 * Created by fred on 2017/10/10.
 */
public final class ServiceCommandResult {
    private final PluginId pluginId;
    private final PluginStatusCode statusCode;
    private final Object value;

    public ServiceCommandResult(PluginId pluginId, PluginStatusCode statusCode, Object value) {
        this.pluginId = pluginId;
        this.statusCode = statusCode;
        this.value = value;
    }

    public final PluginStatusCode getStatus() {
        return statusCode;
    }

    public final Object getValue() {
        return value;
    }

    public final PluginId getPluginId(){return pluginId;}
    public final boolean isOk() {
        return PluginStatusCode.OK.equals(this.getStatus());
    }

    public static Builder createNew(PluginId pluginId, PluginStatusCode errorCode) {
        return new BuilderImpl(pluginId, errorCode);
    }

    public interface Builder {
        Builder withValue(Object value);
        ServiceCommandResult build();
    }

    private static class BuilderImpl implements Builder {

        private final PluginId pluginId;
        private final PluginStatusCode status;
        private Object value;

        public BuilderImpl(PluginId pluginId, PluginStatusCode errorCode) {
            this.pluginId = pluginId;
            this.status = errorCode;
        }

        @Override
        public Builder withValue(Object value) {
            this.value = value;
            return this;
        }

        @Override
        public ServiceCommandResult build() {
            return new ServiceCommandResult(pluginId, this.status, this.value);
        }
    }

}
