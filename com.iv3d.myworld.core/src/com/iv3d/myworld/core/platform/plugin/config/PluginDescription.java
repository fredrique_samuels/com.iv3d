package com.iv3d.myworld.core.platform.plugin.config;

/**
 * Created by fred on 2017/10/10.
 */
public class PluginDescription {
    private String id;
    private String name;
    private String version;
    private PluginServiceDescription service;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PluginServiceDescription getService() {
        return service;
    }

    public void setService(PluginServiceDescription service) {
        this.service = service;
    }
}
