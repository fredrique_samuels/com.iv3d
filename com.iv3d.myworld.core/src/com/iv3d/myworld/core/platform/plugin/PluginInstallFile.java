package com.iv3d.myworld.core.platform.plugin;

import java.io.File;

/**
 * Created by fred on 2017/10/08.
 */
public abstract class PluginInstallFile {
    public abstract PluginId getPluginId();
    public abstract File getFile();

    public static PluginInstallFile createFor(PluginId pluginId, File file) {
        return new PluginInstallFile() {
            @Override
            public PluginId getPluginId() {
                return pluginId;
            }

            @Override
            public File getFile() {
                return file;
            }
        };
    }
}
