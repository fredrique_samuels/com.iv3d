package com.iv3d.myworld.core.platform.plugin;

import com.app.server.base.common.inject.BeanInjector;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.async.AsyncMonitor;
import com.iv3d.common.async.DefaultAsyncMonitor;
import com.iv3d.common.async.CommandQueue;
import com.iv3d.myworld.core.platform.plugin.commands.ExternalServiceCommandRunner;
import javafx.stage.Stage;
import org.apache.commons.collections.map.HashedMap;

import java.io.File;
import java.util.Map;

public abstract class PluginService {
    private Map<String, PluginServiceCommandHandler> commandHandlerMap = new HashedMap();
    private Stage primaryStage;
    private PluginId pluginId;
    private BeanInjector beanInjector;
    private File rootDir;

    public final Stage getPrimaryStage() {
        return primaryStage;
    }

    public final void setPrimaryStage(Stage primaryStage) {
        if(this.primaryStage!=null) {
            return;
        }
        this.primaryStage = primaryStage;
    }

    public final void setPluginId(PluginId pluginId) {
        if(pluginId==null) {
            this.pluginId = pluginId;
        }
    }

    public final PluginId getPluginId(){return  pluginId;}

    public final void addCommandHandler(String command, PluginServiceCommandHandler handler) {
        if(command==null) return;
        if(command.isEmpty()) return;
        if(handler==null)return;
        commandHandlerMap.put(command, handler);
    }

    public final ServiceCommandResult execute(String command, Map<String, Object> params) {
        PluginServiceCommandHandler handler = commandHandlerMap.get(command);
        if(handler==null) {
            return ServiceCommandResult
                    .createNew(this.pluginId,PluginStatusCode.COMMAND_NOT_FOUND)
                    .withValue(command)
                    .build();
        }

        try {
            Object result = handler.execute(this, params);
            if(result==null) {
                return ServiceCommandResult
                        .createNew(this.pluginId,PluginStatusCode.OK)
                        .build();
            } else {
                return ServiceCommandResult
                        .createNew(this.pluginId,PluginStatusCode.OK)
                        .withValue(result)
                        .build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ServiceCommandResult
                    .createNew(this.pluginId,PluginStatusCode.INTERNAL_ERROR)
                    .withValue(e)
                    .build();
        }
    }

    public final AsyncMonitor<ServiceCommandResult> executeExternalService(String pluginId, String command, Map<String, Object> params) {
        if(pluginId==null) {
            ServiceCommandResult result = ServiceCommandResult
                    .createNew(this.pluginId, PluginStatusCode.PLUGIN_NOT_FOUND)
                    .withValue(PluginId.forName(pluginId))
                    .build();
            return new DefaultAsyncMonitor<>()
                    .done(result);
        }

        CommandQueue commandQueue = beanInjector.createInstance(CommandQueue.class);
        ExternalServiceCommandRunner runner = beanInjector.createInstance(ExternalServiceCommandRunner.class);
        runner.setParams(PluginId.forName(pluginId), command, params);

        return commandQueue.execute(runner);
    }

    public final void setBeanInjector(BeanInjector beanInjector) {
        if(this.beanInjector==null) {
            this.beanInjector = beanInjector;
        }
    }

    public final void setRootDir(File rootDir) {
        if(this.rootDir==null)
            this.rootDir = rootDir;
    }

    public final String getResourceExternalForm(String resource) {
        return ResourceLoader.getExternalForm(new File(rootDir, resource).getPath());
    }
}
