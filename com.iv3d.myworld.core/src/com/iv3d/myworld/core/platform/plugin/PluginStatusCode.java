package com.iv3d.myworld.core.platform.plugin;

/**
 * Created by fred on 2017/10/10.
 */
public enum PluginStatusCode {
    OK,
    INTERNAL_ERROR,
    PLUGIN_NOT_FOUND,
    NO_SERVICE_AVAILABLE, COMMAND_NOT_FOUND
}
