package com.iv3d.myworld.core.platform.plugin;

import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

/**
 * Created by fred on 2017/10/10.
 */
public class Plugin {
    private PluginService pluginService;
    private PluginId pluginId;


    public Plugin(PluginId pluginId, PluginService pluginService) {
        this.pluginService = pluginService;
        this.pluginId = pluginId;
    }

    public void execute(String command) {
        execute(command, new HashedMap());
    }

    public final ServiceCommandResult execute(String command, Map<String, Object> params) {
        if(pluginService==null) {
            return ServiceCommandResult
                    .createNew(pluginId, PluginStatusCode.NO_SERVICE_AVAILABLE)
                    .withValue(pluginId)
                    .build();
        }
        return pluginService.execute(command, params);
    }
}
