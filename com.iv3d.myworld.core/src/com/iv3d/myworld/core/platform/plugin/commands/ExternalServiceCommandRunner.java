package com.iv3d.myworld.core.platform.plugin.commands;

import com.google.inject.Inject;
import com.iv3d.common.async.AsyncRunnable;
import com.iv3d.common.async.AsyncUpdater;
import com.iv3d.myworld.core.platform.PluginManager;
import com.iv3d.myworld.core.platform.plugin.Plugin;
import com.iv3d.myworld.core.platform.plugin.PluginId;
import com.iv3d.myworld.core.platform.plugin.PluginStatusCode;
import com.iv3d.myworld.core.platform.plugin.ServiceCommandResult;

import java.util.Map;

/**
 * Created by fred on 2017/10/10.
 */
public class ExternalServiceCommandRunner implements AsyncRunnable<ServiceCommandResult> {

    @Inject
    private PluginManager pluginManager;

    private PluginId pluginId;
    private String command;
    private Map<String, Object> params;

    public void setParams(PluginId pluginId, String command, Map<String, Object> params) {
        this.pluginId = pluginId;
        this.command = command;
        this.params = params;
    }

    @Override
    public void run(AsyncUpdater<ServiceCommandResult> updater) {
        Plugin plugin = pluginManager.getPlugin(pluginId);
        if(plugin==null) {
            ServiceCommandResult result = ServiceCommandResult
                    .createNew(pluginId, PluginStatusCode.PLUGIN_NOT_FOUND)
                    .withValue(pluginId)
                    .build();
            updater.newUpdate()
                    .setDone()
                    .setData(result)
                    .execute();
            return;
        }

        try {
            ServiceCommandResult result = plugin.execute(command, params);
            updater.newUpdate()
                    .setDone()
                    .setData(result)
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
            ServiceCommandResult result = ServiceCommandResult
                    .createNew(pluginId, PluginStatusCode.INTERNAL_ERROR)
                    .withValue(new RuntimeException(pluginId.toString(), e))
                    .build();
            updater.newUpdate()
                    .setDone()
                    .setData(result)
                    .execute();
        }
    }

    @Override
    public void terminate() {

    }
}
