package com.iv3d.myworld.core.platform.plugin;

import java.util.Map;

/**
 * Created by fred on 2017/10/10.
 */
public interface PluginServiceCommandHandler {
    Object execute(PluginService pluginService, Map<String, Object> params);
}
