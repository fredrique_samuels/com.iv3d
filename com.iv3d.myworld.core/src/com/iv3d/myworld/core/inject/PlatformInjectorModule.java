package com.iv3d.myworld.core.inject;

import com.app.server.base.common.inject.BeanInjector;
import com.app.server.base.common.inject.InjectorProvider;
import com.google.inject.AbstractModule;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.myworld.core.bizimpl.*;
import com.iv3d.myworld.core.inject.annotations.JfxPrimaryStage;
import com.iv3d.common.async.CommandQueue;
import com.iv3d.myworld.core.platform.LoggingManager;
import com.iv3d.myworld.core.platform.PluginManager;
import com.iv3d.myworld.core.platform.SplashScreenManager;
import com.iv3d.myworld.core.platform.config.ApplicationConfig;
import javafx.stage.Stage;

/**
 * Created by fred on 2017/10/07.
 */
public class PlatformInjectorModule extends AbstractModule {
    private final InjectorProvider injectorProvider;
    private Stage primaryStage;

    public PlatformInjectorModule(InjectorProvider injectorProvider, Stage primaryStage) {
        this.injectorProvider = injectorProvider;
        this.primaryStage = primaryStage;
    }

    @Override
    protected void configure() {
        bind(Stage.class).annotatedWith(JfxPrimaryStage.class).toInstance(primaryStage);
        bind(BeanInjector.class).toProvider(injectorProvider);
        bind(ApplicationConfig.class).toInstance(JsonUtils.readFromFile("./application.config.json", ApplicationConfigImpl.class));

        bind(PluginManager.class).to(PluginManagerImpl.class);
        bind(LoggingManager.class).to(LoggingManagerImpl.class);
        bind(SplashScreenManager.class).to(SplashScreenManagerImpl.class);
        bind(CommandQueue.class).to(PlatformCommandQueue.class);
    }
}
