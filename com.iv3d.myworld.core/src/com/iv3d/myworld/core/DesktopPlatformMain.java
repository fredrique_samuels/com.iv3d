package com.iv3d.myworld.core;

import com.app.server.base.common.inject.CommonInjectorModule;
import com.app.server.base.common.inject.InjectorProvider;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.iv3d.myworld.core.inject.PlatformInjectorModule;
import com.iv3d.myworld.core.platform.SplashScreenManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by fred on 2017/10/06.
 */
public class DesktopPlatformMain extends Application {

    public static void main(String[] args) {
        Application.launch(DesktopPlatformMain.class, args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage, new Module[]{});
    }

    public final void init(Stage primaryStage, Module... modules) {
        primaryStage.setOnCloseRequest(e -> Platform.exit());
        Injector injector = setupInjections(primaryStage, modules);

        SplashScreenManager splashScreenManager = injector.getInstance(SplashScreenManager.class);
        splashScreenManager.showSplashScreen();
    }

    private Injector setupInjections(Stage primaryStage, Module[] modules) {
        InjectorProvider injectorProvider = new InjectorProvider();

        CommonInjectorModule commonInjectorModule = new CommonInjectorModule();
        PlatformInjectorModule platformInjectorModule = new PlatformInjectorModule(injectorProvider, primaryStage);

        ArrayList<Module> moduleArrayList = new ArrayList<>();
        moduleArrayList.add(commonInjectorModule);
        moduleArrayList.add(platformInjectorModule);
        moduleArrayList.addAll(Arrays.asList(modules));

        Injector injector =
                Guice.createInjector(moduleArrayList.toArray(new Module[]{}));
        injectorProvider.setInjector(injector);
        return injector;
    }

}
