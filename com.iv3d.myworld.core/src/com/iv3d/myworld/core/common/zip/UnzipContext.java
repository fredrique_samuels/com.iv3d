package com.iv3d.myworld.core.common.zip;

import com.eroi.migrate.Execute;

import java.io.File;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by fred on 2017/10/09.
 */
public interface UnzipContext {
    ZipEntry getZipEntry();
    ZipInputStream getZipInputStream();
    File getRootDir();
    void dispatchUpdate(UnzipUpdate update);
}
