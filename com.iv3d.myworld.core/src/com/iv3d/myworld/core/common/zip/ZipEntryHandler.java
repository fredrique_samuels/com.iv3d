package com.iv3d.myworld.core.common.zip;

/**
 * Created by fred on 2017/10/09.
 */
public interface ZipEntryHandler {
    void handle(UnzipContext zipContext) throws Exception;
}
