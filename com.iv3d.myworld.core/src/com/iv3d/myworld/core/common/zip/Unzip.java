package com.iv3d.myworld.core.common.zip;

import com.iv3d.common.core.Percentage;
import com.iv3d.common.async.AsyncRunnable;
import com.iv3d.common.async.AsyncUpdater;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by fred on 2017/10/09.
 */
public final class Unzip implements AsyncRunnable<UnzipUpdate> {

    private final File zipFile;
    private final File folder;

    public Unzip(File zipFile, File folder) {
        this.zipFile = zipFile;
        this.folder = folder;
    }

    public final void unZipIt(File zipFile, File folder) {
        unZipIt(null, zipFile, folder);
    }

    public synchronized final void unZipIt(AsyncUpdater<UnzipUpdate> updater, File zipFile, File folder) {

        ZippedFileCountHandler fileCountHandler = new ZippedFileCountHandler();
        processZipFile(updater, zipFile, folder, fileCountHandler);

        ExtractHandler extractHandler = new ExtractHandler(fileCountHandler.getCount());
        processZipFile(updater, zipFile, folder, extractHandler);

        dispatchDone(updater);
    }

    void processZipFile(AsyncUpdater<UnzipUpdate> updater, File zipFile, File folder, ZipEntryHandler unzipHandler) {
        try{
            UnzipContextImpl zipContext = new UnzipContextImpl(updater, zipFile,folder);

            while(zipContext.nextEntry()){
                unzipHandler.handle(zipContext);
            }
            zipContext.close();
        } catch(Exception ex) {
            ex.printStackTrace();
            dispatchDone(updater);
        }
    }

    @Override
    public void run(AsyncUpdater<UnzipUpdate> updater) {
        this.unZipIt(updater, zipFile, folder);
    }

    @Override
    public void terminate() {

    }

    private void dispatchDone(AsyncUpdater<UnzipUpdate> updater) {
        if(updater!=null) {
            updater.newUpdate().setDone().execute();
        }
    }

    private class UnzipContextImpl implements UnzipContext {
        private final AsyncUpdater<UnzipUpdate> updater;
        private final ZipInputStream zis;
        private final File zipFile;
        private final File folder;
        private ZipEntry zipEntry;

        public UnzipContextImpl(AsyncUpdater<UnzipUpdate> updater, File zipFile, File folder) throws FileNotFoundException {
            this.updater = updater;
            this.zis = new ZipInputStream(new FileInputStream(zipFile));
            this.zipFile = zipFile;
            this.folder = folder;
        }

        public boolean nextEntry() throws IOException {
            this.zipEntry = this.zis.getNextEntry();
            return zipEntry!=null;
        }

        @Override
        public ZipEntry getZipEntry() {
            return zipEntry;
        }

        @Override
        public ZipInputStream getZipInputStream() {
            return zis;
        }

        @Override
        public File getRootDir() {
            return folder;
        }

        @Override
        public void dispatchUpdate(UnzipUpdate update) {
            if(updater==null) {
                return;
            }

            updater.newUpdate()
                    .setMessage(update.getDestination())
                    .setPercentage(update.getPercentageComplete())
                    .setData(update)
                    .execute();
        }

        public void close() throws IOException {
            zis.closeEntry();
            zis.close();
        }
    }

    private class ExtractHandler implements ZipEntryHandler {

        private final byte[] buffer = new byte[1024];
        private final int totalFileCount;
        private int fileCount=0;

        public ExtractHandler(int totalFileCount) {
            this.totalFileCount = totalFileCount;
        }

        @Override
        public void handle(UnzipContext zc) throws Exception {
            if(!zc.getRootDir().exists()){
                zc.getRootDir().mkdir();
            }

            ZipInputStream zis = zc.getZipInputStream();
            ZipEntry ze = zc.getZipEntry();
            String name = ze.getName();

            if(ze.isDirectory()) {
                File newFile = new File(folder, name);
                newFile.mkdirs();
            } else {
                fileCount+=1;
                File newFile = new File(folder, name);

                UnzipUpdate update = new UnzipUpdateImpl(totalFileCount, fileCount, name, newFile.getPath());
                zc.dispatchUpdate(update);
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
            }
        }
    }

    private class ZippedFileCountHandler implements ZipEntryHandler {

        private int count;

        public ZippedFileCountHandler() {
            this.count=0;
        }

        @Override
        public void handle(UnzipContext zc) throws Exception {
            if(!zc.getZipEntry().isDirectory()) {
                count+=1;
            }
        }

        public int getCount() {
            return count;
        }
    }

    private class UnzipUpdateImpl implements UnzipUpdate {
        private final int totalFileCount;
        private final int fileCount;
        private final String name;
        private final String path;

        public UnzipUpdateImpl(int totalFileCount, int fileCount, String name, String path) {
            this.totalFileCount = totalFileCount;
            this.fileCount = fileCount;
            this.name = name;
            this.path = path;
        }

        @Override
        public String getDestination() {
            return path;
        }

        @Override
        public String getSource() {
            return name;
        }

        @Override
        public int getCurrentFileCount() {
            return fileCount;
        }

        @Override
        public int getTotalFileCount() {
            return totalFileCount;
        }

        @Override
        public Percentage getPercentageComplete() {
            if(totalFileCount==0) {
                return new Percentage(0);
            }
            return new Percentage(Double.valueOf((double) fileCount / totalFileCount).intValue());
        }
    }
}
