package com.iv3d.myworld.core.common.zip;

import com.iv3d.common.core.Percentage;

/**
 * Created by fred on 2017/10/09.
 */
public interface UnzipUpdate {
    String getDestination();
    String getSource();
    int getCurrentFileCount();
    int getTotalFileCount();
    Percentage getPercentageComplete();
}
