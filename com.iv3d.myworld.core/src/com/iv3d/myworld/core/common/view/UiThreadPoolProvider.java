package com.iv3d.myworld.core.common.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.core.Provider;
import com.iv3d.myworld.core.platform.config.ApplicationConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by fred on 2017/10/09.
 */
@Singleton
public final class UiThreadPoolProvider implements Provider<ExecutorService> {

    private ExecutorService executorService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Override
    public synchronized ExecutorService get() {
        if(executorService==null) {
            executorService = Executors.newFixedThreadPool(applicationConfig.getUiThreadPoolSize());
        }
        return executorService;
    }
}
