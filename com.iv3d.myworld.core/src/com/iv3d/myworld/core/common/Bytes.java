package com.iv3d.myworld.core.common;

/**
 * Created by fred on 2017/10/09.
 */
public class Bytes {

    private final long bytes;
    private final Unit unit;

    public enum Unit {
        BYTE(1, "b"),
        KILO_BYTE(1024, "kb"),
        MEGA_BYTE(1024*1024, "mb"),
        GIGA_BYTE(1024*1024*1024, "gb");

        private final long unitLength;
        private String isoSymbol;

        Unit(long bytes, String isoSymbol) {
            unitLength = bytes;
            this.isoSymbol = isoSymbol;
        }

        boolean isUnit(long value) {
            return value>=unitLength;
        }

        public String getIsoSymbol() {
            return isoSymbol;
        }
    }

    public Bytes(long bytes) {
        this.bytes = bytes;
        this.unit = findMaxUnitSize(bytes);
    }


    public final long getBytes() {
        return bytes;
    }

    public final Unit getUnit() {
        return unit;
    }

    public final String toString() {
        return String.format("%d%s", bytes, unit.getIsoSymbol());
    }

    private Unit findMaxUnitSize(long bytes) {
        Unit unit = Unit.BYTE;

        if(Unit.KILO_BYTE.isUnit(bytes)) {
            unit = Unit.KILO_BYTE;
        }

        if(Unit.MEGA_BYTE.isUnit(bytes)) {
            unit = Unit.MEGA_BYTE;
        }

        if(Unit.GIGA_BYTE.isUnit(bytes)) {
            unit = Unit.GIGA_BYTE;
        }

        return unit;
    }
}
