package com.iv3d.myworld.core.common.ctxwork;

/**
 * Created by fred on 2017/10/08.
 */
public interface ContextWorker<T> {
    void run(T context);
    boolean shouldTerminate(T context);
}
