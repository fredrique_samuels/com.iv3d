package com.iv3d.myworld.core.common.ctxwork;

import com.iv3d.common.core.Callback;
import com.iv3d.common.core.Provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * Created by fred on 2017/10/08.
 */
public final class ContextWorkerStackBuilder<T> {
    private Callback<T> onDoneHandler = (context) -> {};
    private List<ContextWorker<T>> workers = new ArrayList<>();
    private Provider<T> contextProvider = () -> null;

    public final ContextWorkerStackBuilder<T> setOnDoneHandler(Callback<T> onDoneHandler) {
        this.onDoneHandler = onDoneHandler;
        return this;
    }

    public final ContextWorkerStackBuilder<T> setContextProvider(Provider<T> contextProvider) {
        this.contextProvider = contextProvider;
        return this;
    }

    public final ContextWorkerBuilder<T> createWorker(Callback<T> runner) {
        return new ContextWorkerBuilderImpl(this, runner);
    }

    public final ContextWorkerStack<T> build() {
        return new ContextWorkerStackImpl(this);
    }

    private static class ContextWorkerBuilderImpl<T> implements ContextWorkerBuilder<T> {

        private final ContextWorkerStackBuilder workerStackBuilder;
        private final Callback<T> runner;
        private Function<T, Boolean> terminateCondition = (context) -> false;
        private Callback<T> onTerminateHandler = (context) -> {};

        public ContextWorkerBuilderImpl(ContextWorkerStackBuilder workerStackBuilder, Callback<T> runner) {
            this.workerStackBuilder = workerStackBuilder;
            this.runner = runner;
        }

        @Override
        public ContextWorkerBuilder<T> setTerminateCondition(Function<T, Boolean> terminateCondition) {
            this.terminateCondition = terminateCondition;
            return this;
        }

        @Override
        public ContextWorkerBuilder<T> setOnTerminateHandler(Callback<T> onTerminateHandler) {
            this.onTerminateHandler = onTerminateHandler;
            return this;
        }

        @Override
        public ContextWorkerStackBuilder<T> commit() {
            workerStackBuilder.addWorker(new ContextWorkerImpl(runner, terminateCondition, onTerminateHandler));
            return workerStackBuilder;
        }

        private class ContextWorkerImpl<T> implements ContextWorker<T> {
            private final Callback<T> runner;
            private final Function<T, Boolean> terminateCondition;
            private Callback<T> onTerminateHandler;

            public ContextWorkerImpl(Callback<T> runner, Function<T, Boolean> terminateCondition, Callback<T> onTerminateHandler) {
                this.runner = runner;
                this.terminateCondition = terminateCondition;
                this.onTerminateHandler = onTerminateHandler;
            }

            @Override
            public void run(T context) {
                runner.invoke(context);
            }

            @Override
            public boolean shouldTerminate(T context) {
                Boolean terminate = terminateCondition.apply(context);
                if(terminate) {
                    try {
                        onTerminateHandler.invoke(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return terminate;
            }
        }
    }

    private void addWorker(ContextWorker<T> contextWorker) {
        workers.add(contextWorker);
    }

    private class ContextWorkerStackImpl implements ContextWorkerStack<T> {
        private final Callback<T> onDoneHandler;
        private final List<ContextWorker<T>> workers;
        private final Provider<T> contextProvider;

        public ContextWorkerStackImpl(ContextWorkerStackBuilder<T> stackBuilder) {
            this.onDoneHandler = stackBuilder.onDoneHandler;
            this.workers = Collections.unmodifiableList(stackBuilder.workers);
            this.contextProvider = stackBuilder.contextProvider;
        }

        @Override
        public final T execute() {
            try {
                T context = contextProvider.get();
                for(ContextWorker<T> worker : workers) {
                    worker.run(context);
                    if(worker.shouldTerminate(context)){
                        break;
                    }
                }
                onDoneHandler.invoke(context);
                return context;
            } catch (Exception e) {
                onDoneHandler.invoke(null);
                e.printStackTrace();
            }
            return null;
        }
    }
}
