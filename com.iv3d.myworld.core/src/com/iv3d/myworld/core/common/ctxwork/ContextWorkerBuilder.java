package com.iv3d.myworld.core.common.ctxwork;

import com.iv3d.common.core.Callback;

import java.util.function.Function;

/**
 * Created by fred on 2017/10/08.
 */
public interface ContextWorkerBuilder<T> {
    ContextWorkerBuilder<T> setTerminateCondition(Function<T, Boolean> condition);
    ContextWorkerBuilder<T> setOnTerminateHandler(Callback<T> onTerminateHandler);
    ContextWorkerStackBuilder<T> commit();
}
