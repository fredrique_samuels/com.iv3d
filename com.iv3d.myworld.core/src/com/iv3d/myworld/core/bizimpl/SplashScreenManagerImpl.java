package com.iv3d.myworld.core.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.core.Percentage;
import com.iv3d.common.core.ThreadUtils;
import com.iv3d.common.async.*;
import com.iv3d.common.jython.JythonUtils;
import com.iv3d.myworld.core.common.view.UiThreadPoolProvider;
import com.jfx.control.FxRunner;
import com.jfx.profile.JavaFxUiProfile;
import com.jfx.profile.JavaFxUiProfileBuilder;
import com.jfx.profile.NodeLookup;
import com.iv3d.myworld.core.inject.annotations.JfxPrimaryStage;
import com.iv3d.myworld.core.platform.PluginManager;
import com.iv3d.myworld.core.platform.SplashScreenManager;
import com.iv3d.myworld.core.platform.plugin.PluginInstallFile;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.List;


@Singleton
public class SplashScreenManagerImpl implements SplashScreenManager {

    private Stage dialog;
    private JavaFxUiProfile fxUiProfile;
    private Boolean isOpen=false;


    @Inject @JfxPrimaryStage
    private Stage primaryStage;
    @Inject
    private UiThreadPoolProvider uiThreadPoolProvider;
    @Inject
    private PluginManager pluginManager;
    @Inject
    private CommandQueue commandQueue;

    public SplashScreenManagerImpl() {
    }

    @Override
    public void showSplashScreen() {

        new FxRunner() {

            @Override
            public void run() {
                fxUiProfile = new JavaFxUiProfileBuilder(uiThreadPoolProvider.get(),
                        "./resources/tt.default.splashscreen.view.fxml")
                        .build();
                Parent parent = fxUiProfile.getParent();
                initDisplay();

                dialog = new Stage();
                dialog.setScene(new Scene(parent));
                dialog.initOwner(primaryStage);
                dialog.initStyle(StageStyle.UNDECORATED);
                dialog.initModality(Modality.APPLICATION_MODAL);
                dialog.show();

                commandQueue.execute(new AsyncRunnable() {
                    @Override
                    public void run(AsyncUpdater updater) {
                        doStartup();
                    }

                    @Override
                    public void terminate() {

                    }
                });
            }
        }.execute();

    }

    void initDisplay() {
        NodeLookup nodeLookup = fxUiProfile.getNodeLookup();

        ProgressBar progressBar = (ProgressBar) nodeLookup.lookup("progressBar");
        progressBar.setVisible(false);

        Button okButton = (Button) nodeLookup.lookup("okButton");
        okButton.setVisible(false);
    }

    private void doStartup() {
        JythonUtils.init();
        ThreadUtils.delay(2000);
        AsyncMonitor asyncMonitor = pluginManager.installCorePluginsIfNotInstalled();
        asyncMonitor.join();
        ThreadUtils.delay(2000);
        pluginManager.loadPlugins();
        pluginManager.runMain();
        hideSplashScreen();
    }

    @Override
    public void hideSplashScreen() {
        new FxRunner() {
            @Override
            public void run() {
                dialog.hide();
                isOpen=false;
            }
        }.execute();
    }

    @Override
    public AsyncMonitor showMissingInstallFiles(List<PluginInstallFile> missingInstallFiles) {
        return AsyncCommand.createForRunner(new AsyncRunnable() {
            @Override
            public void run(AsyncUpdater updater) {

            }

            @Override
            public void terminate() {

            }
        }).execute();
    }

    @Override
    public AsyncMonitor showInstallProgress(String message, Percentage percentage) {
        DefaultAsyncMonitor asyncMonitor = new DefaultAsyncMonitor();

        new FxRunner() {
            @Override
            public void run() {
                NodeLookup nodeLookup = fxUiProfile.getNodeLookup();
                ProgressBar progressBar = (ProgressBar) nodeLookup.lookup("progressBar");
                progressBar.setProgress(percentage.get());
                progressBar.setVisible(percentage!=null);

                Text loadingText = (Text) nodeLookup.lookup("loadingText");
                loadingText.setText(message);
                asyncMonitor.release();
            }
        }.execute();

        return asyncMonitor;
    }
}
