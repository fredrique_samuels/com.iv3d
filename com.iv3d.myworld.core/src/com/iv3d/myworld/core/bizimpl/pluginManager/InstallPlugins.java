package com.iv3d.myworld.core.bizimpl.pluginManager;

import com.google.inject.Inject;
import com.iv3d.common.core.Percentage;
import com.iv3d.myworld.core.bizimpl.PluginManagerImpl;
import com.iv3d.common.async.AsyncUpdateHandler;
import com.iv3d.common.async.AsyncUpdater;
import com.iv3d.common.async.AsyncRunnable;
import com.iv3d.myworld.core.common.zip.Unzip;
import com.iv3d.myworld.core.common.zip.UnzipUpdate;
import com.iv3d.common.async.CommandQueue;
import com.iv3d.myworld.core.platform.plugin.PluginInstallFile;

import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2017/10/09.
 */
public class InstallPlugins  implements AsyncRunnable<Object> {
    @Inject
    private PluginManagerImpl pluginManager;
    @Inject
    private CommandQueue commandQueue;

    private List<PluginInstallFile> pluginInstallFiles = Collections.emptyList();

    public InstallPlugins() {
    }

    public void setPluginInstallFiles(List<PluginInstallFile> pluginInstallFiles) {
        this.pluginInstallFiles = pluginInstallFiles;
    }

    @Override
    public void run(AsyncUpdater<Object> updater) {
        for(PluginInstallFile file:pluginInstallFiles) {
            Unzip unzip = new Unzip(file.getFile(), pluginManager.getInstallPath());
            commandQueue.execute(unzip)
                    .withUpdateHandler(new AsyncUpdateHandler<UnzipUpdate>() {
                        @Override
                        public void onUpdate(String message, Percentage percentage, UnzipUpdate data) {

                            String updateMessage = String.format("Installing '%s'",
                                    data.getSource());

                            updater.newUpdate()
                                    .setPercentage(percentage)
                                    .setMessage(updateMessage)
                                    .execute();
                        }

                        @Override
                        public void onDone(UnzipUpdate result) {

                        }
                    })
                    .join();
        }

        updater.newUpdate()
                .setDone()
                .execute();
    }

    @Override
    public void terminate() {

    }
}
