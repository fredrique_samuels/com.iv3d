package com.iv3d.myworld.core.bizimpl.pluginManager;

import com.google.inject.Inject;
import com.iv3d.common.core.Percentage;
import com.iv3d.myworld.core.bizimpl.PluginManagerImpl;
import com.iv3d.common.async.AsyncMonitor;
import com.iv3d.common.async.AsyncRunnable;
import com.iv3d.common.async.AsyncUpdateHandler;
import com.iv3d.common.async.AsyncUpdater;
import com.iv3d.myworld.core.common.ctxwork.ContextWorkerStack;
import com.iv3d.myworld.core.common.ctxwork.ContextWorkerStackBuilder;
import com.iv3d.myworld.core.platform.LoggingManager;
import com.iv3d.myworld.core.platform.SplashScreenManager;
import com.iv3d.myworld.core.platform.config.ApplicationConfig;
import com.iv3d.myworld.core.platform.plugin.PluginId;
import com.iv3d.myworld.core.platform.plugin.PluginInstallFile;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/10/07.
 */
public class InstallCoreIfNotPresentHandler implements AsyncRunnable<Object> {

    @Inject
    ApplicationConfig applicationConfig;
    @Inject
    private LoggingManager loggingManager;
    @Inject
    private SplashScreenManager splashScreenManager;
    @Inject
    private PluginManagerImpl pluginManager;

    @Override
    public void run(AsyncUpdater<Object> updater) {
        logInfo("Checking for installed core plugins");

        ContextWorkerStackBuilder<Context> workerStackBuilder = new ContextWorkerStackBuilder<Context>()
                .setContextProvider(() -> new Context())
                .setOnDoneHandler((context) -> updater.newUpdate().setDone().execute());

        workerStackBuilder = getMissingPlugins(workerStackBuilder);
        workerStackBuilder = getPluginInstallFiles(workerStackBuilder);
        workerStackBuilder = installPlugins(workerStackBuilder);

        ContextWorkerStack<Context> workerStack = workerStackBuilder.build();
        workerStack.execute();
    }

    private ContextWorkerStackBuilder<Context> installPlugins(ContextWorkerStackBuilder<Context> workerStackBuilder) {
        return workerStackBuilder.createWorker((context) -> {
            AsyncMonitor asyncMonitor = pluginManager.installPlugins(context.pluginInstallFiles);
            asyncMonitor.withUpdateHandler(new AsyncUpdateHandler() {
                @Override
                public void onUpdate(String message, Percentage percentage, Object data) {
                    splashScreenManager.showInstallProgress(message, percentage);
                }

                @Override
                public void onDone(Object result) {

                }
            });
            asyncMonitor.join();
        })
        .commit();
    }

    ContextWorkerStackBuilder<Context> getPluginInstallFiles(ContextWorkerStackBuilder<Context> workerStackBuilder) {
        workerStackBuilder = workerStackBuilder.createWorker((context) -> {
                    context.pluginInstallFiles = context.missingPlugins.stream()
                            .map(pluginId -> pluginManager.getCorePluginInstallFile(pluginId))
                            .collect(Collectors.toList());

                    context.missingInstallFiles = context.pluginInstallFiles.stream()
                            .filter(file -> !isFileObject(file.getFile()))
                            .collect(Collectors.toList());
                })
                .setTerminateCondition((context) -> !context.missingInstallFiles.isEmpty())
                .setOnTerminateHandler((context) -> {
                    List<String> missingFilesAsString = context.missingInstallFiles.stream()
                            .map(f -> f.getFile().getPath())
                            .collect(Collectors.toList());
                    String format = String.format("Unable to find install files, ['%s']",
                            String.join("','", missingFilesAsString));
                    logInfo(format);
                    splashScreenManager.showMissingInstallFiles(context.missingInstallFiles)
                        .withUpdateHandler(new AsyncUpdateHandler() {
                            @Override
                            public void onUpdate(String message, Percentage percentage, Object data) {

                            }

                            @Override
                            public void onDone(Object result) {
                                System.exit(0);
                            }
                        });
                })
                .commit();
        return workerStackBuilder;
    }

    ContextWorkerStackBuilder<Context> getMissingPlugins(ContextWorkerStackBuilder<Context> workerStackBuilder) {
        workerStackBuilder = workerStackBuilder
                .createWorker((context) -> {
                    List<PluginId> corePluginIds = applicationConfig.getCorePluginIds();
                    context.missingPlugins = corePluginIds.stream()
                            .filter(pId -> !pluginManager.isInstalled(pId))
                            .collect(Collectors.toList());

                    String format = String.format("Found %d missing core plugins, ['%s']",
                            context.missingPlugins.size(),
                            String.join("','", context.missingPlugins.stream()
                                    .map(p -> p.getName())
                                    .collect(Collectors.toList())));
                    logInfo(format);
                })
                .setTerminateCondition((context) -> context.missingPlugins.isEmpty())
                .commit();
        return workerStackBuilder;
    }

    @Override
    public void terminate() {

    }

    private boolean isFileObject(File file) {
        return file.exists() && file.isFile();
    }



    private void logInfo(String message) {
        loggingManager.info(InstallCoreIfNotPresentHandler.class.getName().toLowerCase(), message);
    }

    class Context {
        public List<PluginId> missingPlugins;
        public List<PluginInstallFile> pluginInstallFiles;
        public List<PluginInstallFile> missingInstallFiles;
    }

}
