package com.iv3d.myworld.core.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.async.DefaultCommandQueue;
import com.iv3d.myworld.core.platform.config.ApplicationConfig;

/**
 * Created by fred on 2017/10/14.
 */
@Singleton
public class PlatformCommandQueue extends DefaultCommandQueue {

    @Inject
    ApplicationConfig applicationConfig;

    protected int getThreadPoolSize() {
        return applicationConfig.getCommandThreadPoolSize();
    }
}
