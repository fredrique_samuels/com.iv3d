package com.iv3d.myworld.core.bizimpl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.myworld.core.platform.config.ApplicationConfig;
import com.iv3d.myworld.core.platform.plugin.PluginId;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/10/07.
 */
public class ApplicationConfigImpl implements ApplicationConfig {
    private int commandThreadPoolSize;
    private int uiThreadPoolSize;
    private String pluginsHomePath;
    private @JsonDeserialize(using = JsonPluginIdDeserializer.class ) List<PluginId> corePluginIds;
    private String entryPluginId;

    public int getCommandThreadPoolSize() {
        return commandThreadPoolSize;
    }

    public void setCommandThreadPoolSize(int commandThreadPoolSize) {
        this.commandThreadPoolSize = commandThreadPoolSize;
    }

    @Override
    public int getUiThreadPoolSize() {
        return uiThreadPoolSize;
    }

    public void setUiThreadPoolSize(int uiThreadPoolSize) {
        this.uiThreadPoolSize = uiThreadPoolSize;
    }

    @Override
    public String getPluginsHomePath() {
        return pluginsHomePath;
    }

    public void setPluginsHomePath(String pluginsHomePath) {
        this.pluginsHomePath = pluginsHomePath;
    }

    public String getEntryPluginId() {
        return entryPluginId;
    }

    public void setEntryPluginId(String entryPluginId) {
        this.entryPluginId = entryPluginId;
    }

    public List<PluginId> getCorePluginIds() {
        return Collections.unmodifiableList(corePluginIds);
    }

    public void setCorePluginIds(List<PluginId> corePluginIds) {
        this.corePluginIds = corePluginIds;
    }

    public static class JsonPluginIdDeserializer extends JsonDeserializer<List<PluginId>> {

        @Override
        public List<PluginId> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            String json = jsonParser.readValueAsTree().toString();
            List<String> values = JsonUtils.readFromString(json, List.class);
            return values.stream()
                    .map( value -> PluginId.forName(value) )
                    .collect(Collectors.toList());
        }
    }
}
