package com.iv3d.myworld.core.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.date.UtcClock;
import com.iv3d.myworld.core.platform.LoggingManager;
import org.apache.log4j.Logger;

/**
 * Created by fred on 2017/10/07.
 */
@Singleton
public class LoggingManagerImpl implements LoggingManager {
    private Logger logger = Logger.getLogger(LoggingManagerImpl.class);

    @Inject
    UtcClock utcClock;

    @Override
    public void info(Class<?> aClass, String message) {
        logger.info(utcClock.now().toIso8601String() + ":" + aClass.getName().toLowerCase() + ":" + message);
    }

    @Override
    public void info(String packageId, String message) {
        logger.info(utcClock.now().toIso8601String() + ":" + packageId + ":" + message);
    }

    @Override
    public void error(Class<?> aClass, String message) {
        logger.info(utcClock.now().toIso8601String() + ":" + aClass.getName().toLowerCase() + ":" + message);
    }
}
