package com.iv3d.myworld.core.bizimpl;

import com.app.server.base.common.inject.BeanInjector;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.beans.BeanConfig;
import com.iv3d.common.beans.BeanLoader;
import com.iv3d.common.beans.DefaultBeanConfig;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.myworld.core.bizimpl.pluginManager.InstallCoreIfNotPresentHandler;
import com.iv3d.myworld.core.bizimpl.pluginManager.InstallPlugins;
import com.iv3d.common.async.AsyncMonitor;
import com.iv3d.myworld.core.inject.annotations.JfxPrimaryStage;
import com.iv3d.common.async.CommandQueue;
import com.iv3d.myworld.core.platform.LoggingManager;
import com.iv3d.myworld.core.platform.PluginManager;
import com.iv3d.myworld.core.platform.config.ApplicationConfig;
import com.iv3d.myworld.core.platform.plugin.Plugin;
import com.iv3d.myworld.core.platform.plugin.PluginId;
import com.iv3d.myworld.core.platform.plugin.PluginInstallFile;
import com.iv3d.myworld.core.platform.plugin.PluginService;
import com.iv3d.myworld.core.platform.plugin.config.PluginDescription;
import com.iv3d.myworld.core.platform.plugin.config.PluginServiceDescription;
import javafx.stage.Stage;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/10/07.
 */
@Singleton
public class PluginManagerImpl implements PluginManager {

    private static String FACTORY_DEFAULTS_PATH = "factory-defaults";
    private static String INSTALLED_PATH = "installed";

    @Inject
    ApplicationConfig applicationConfig;
    @Inject
    private LoggingManager loggingManager;
    @Inject
    private CommandQueue commandQueue;
    @Inject
    private BeanInjector beanInjector;
    @Inject @JfxPrimaryStage
    private Stage primaryStage;

    private Map<String, Plugin> pluginMap = new HashMap<>();

    public PluginManagerImpl() {
    }

    @Override
    public AsyncMonitor installCorePluginsIfNotInstalled() {
        InstallCoreIfNotPresentHandler installCoreIfNotPresentHandler = beanInjector.createInstance(InstallCoreIfNotPresentHandler.class);
        return commandQueue.execute(installCoreIfNotPresentHandler);
    }

    public PluginInstallFile getCorePluginInstallFile(PluginId pluginId) {
        String pluginsHomePath = applicationConfig.getPluginsHomePath();
        File file = new File(new File(pluginsHomePath, FACTORY_DEFAULTS_PATH), pluginId.getName() + ".zip");
        return PluginInstallFile.createFor(pluginId, file);
    }

    @Override
    public AsyncMonitor installPlugins(List<PluginInstallFile> pluginInstallFiles) {
        InstallPlugins installPlugins = beanInjector.createInstance(InstallPlugins.class);
        installPlugins.setPluginInstallFiles(pluginInstallFiles);
        return commandQueue.execute(installPlugins);
    }

    public final File getInstallPath() {
        return new File(applicationConfig.getPluginsHomePath(), INSTALLED_PATH);
    }

    @Override
    public void runMain() {
        String entryPluginId = applicationConfig.getEntryPluginId();
        String s = "Starting main plugin " + entryPluginId;
        logInfo(s);

        Plugin plugin = getPlugin(PluginId.forName(entryPluginId));
        if(plugin==null) {
            String message = "Unable to locate plugin " + entryPluginId;
            logError(message);
            return;
        }
        plugin.execute("main", new HashMap<>());
    }


    @Override
    public void loadPlugins() {
        logInfo("Loading plugins ...");

        File installPath = getInstallPath();
        List<File> pluginFolders = Arrays.asList(installPath.listFiles())
                .stream()
                .filter(f -> f.isDirectory())
                .filter(f -> isValidPluginFolder(f))
                .collect(Collectors.toList());
        logInfo(String.format("Found %d plugin(s) ...", pluginFolders.size()));

        BeanLoader beanLoader = new BeanLoader();
        for(File folder : pluginFolders) {
            logInfo(String.format("Loading plugin '%s' ...", folder.getName()));
            File settingsFile = getPluginSettingsFile(folder);
            try {
                PluginDescription pluginDescription = JsonUtils.readFromFile(settingsFile, PluginDescription.class);
                String id = pluginDescription.getId();
                PluginId pluginId = PluginId.forName(id);

                PluginService pluginService = null;
                PluginServiceDescription serviceDescription = pluginDescription.getService();
                if(serviceDescription!=null && serviceDescription.getBean()!=null) {
                    DefaultBeanConfig bean = serviceDescription.getBean();
                    BeanConfig beanConfig = new BeanConfig() {
                        @Override
                        public String getSource() {return bean.getSource();}
                        @Override
                        public String getLanguage() {return bean.getLanguage();}
                        @Override
                        public String getBean() {return bean.getBean();}

                        @Override
                        public String getPath() {
                            File pluginRoot = getPluginRoot(id);
                            return new File(pluginRoot, bean.getPath()).getPath();
                        }
                    };
                    pluginService = beanLoader.load(beanConfig, PluginService.class);
                    pluginService.setPluginId(pluginId);
                    pluginService.setBeanInjector(beanInjector);
                    pluginService.setPrimaryStage(primaryStage);
                    pluginService.setRootDir(getPluginRoot(id));
                }
                Plugin plugin = new Plugin(pluginId, pluginService);
                this.pluginMap.put(id, plugin);
            } catch (Exception e) {
                e.printStackTrace();
                logError(String.format("Error loading plugin '%s'", folder.getName()));
            }
        }
    }

    private boolean isValidPluginFolder(File folder) {
        File settingsFile = getPluginSettingsFile(PluginId.forName(folder.getName()));
        return settingsFile.exists() && settingsFile.isFile();
    }

    public Plugin getPlugin(PluginId pluginId) {
        return pluginMap.get(pluginId.getName());
    }

    public boolean isInstalled(PluginId pluginId) {
        if(!pluginId.isValid())return false;
        File pluginSettingsFile = getPluginSettingsFile(pluginId);
        return pluginSettingsFile.isFile() && pluginSettingsFile.exists();
    }


    public File getPluginSettingsFile(File folder) {
        return getPluginSettingsFile(PluginId.forName(folder.getName()));
    }

    public File getPluginSettingsFile(PluginId pluginId) {
        String name = pluginId.getName();
        File pluginFolder = getPluginRoot(name);
        return new File(pluginFolder, name + ".plugin.json");
    }

    File getPluginRoot(String name) {
        File installPath = getInstallPath();
        return new File(installPath, name);
    }


    void logError(String message) {
        loggingManager.error(PluginManagerImpl.class, message);
    }

    void logInfo(String s) {
        loggingManager.info(PluginManagerImpl.class, s);
    }
}
