import javafx.scene.control.Button as Button;

from com.iv3d.myworld.core.platform.plugin import PluginService
from com.iv3d.myworld.core.platform.plugin import PluginServiceCommandHandler


class DefaultHandler(PluginServiceCommandHandler):

    def __init__(self):
        PluginService.__init__(self)

    def execute(self, plugin_service, params):

        btn = Button();
        btn.setText("Button From PK2'");

        return btn


class Pk2Service (PluginService):

    def __init__(self):
        PluginService.__init__(self)
        self.addCommandHandler('get_btn', DefaultHandler() )