import javafx.scene.Scene as Scene;
import javafx.scene.control.Button as Button;
import javafx.scene.layout.StackPane as StackPane;
import javafx.application.Platform as Platform;
import javafx.event.EventHandler as EventHandler;

import java.lang.System as System

from com.iv3d.myworld.core.platform.plugin import PluginService
from com.iv3d.myworld.core.platform.plugin import PluginServiceCommandHandler

import com.jfx.control.FxRunner as FxRunner


class DefaultHandler(PluginServiceCommandHandler):

    def __init__(self):
        PluginService.__init__(self)

    def __get_button(self, plugin_service):
        async = plugin_service.executeExternalService("myworld.core.demo.pk2", "get_btn", None);
        service_result = async.join()
        if service_result.isOk() :
            return service_result.getValue()

        btn = Button();
        btn.setText("Remove Call Failed");
        return btn

    def execute(self, plugin_service, params):
        print 'Running Handler', plugin_service.getPluginId(), params, self

        stage = plugin_service.getPrimaryStage()

        button = self.__get_button(plugin_service)

        class ShowUi(FxRunner):

            def __init__(self):
                FxRunner.__init__(self)

            def run(self):
                root = StackPane();
                root.getChildren().add(button);

                image = plugin_service.getResourceExternalForm("default-background.jpeg")
                root.setStyle("-fx-background-image: url('" + image + "'); " +
                              "-fx-background-position: center center; " +
                              "-fx-background-size: cover;");

                class CloseHandler(EventHandler):

                    def __init__(self):
                        EventHandler.__init__(self)

                    def handle(self, event):
                        Platform.exit()
                        System.exit(1)

                stage.setOnCloseRequest(CloseHandler());
                stage.setScene(Scene(root, 300, 250))
                # stage.setFullScreen(True);
                stage.show();


        ShowUi().execute()


class MyWorldMainService (PluginService):

    def __init__(self):
        PluginService.__init__(self)
        self.addCommandHandler('main', DefaultHandler() )