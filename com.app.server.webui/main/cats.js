'use strict'

import React from 'react';
import jsonValue from './test.json';

console.log(jsonValue);
module.exports = React.createClass({
    render: function(){
        var value = 5;
        var hello = `This is a pretty little template string. ${value}`;
        return <p>{hello}</p>;
    }
});
