precision mediump float;

uniform sampler2D texture0;

varying float texture0_enabled;

varying vec4 vVertex;
varying vec3 vNormal;
varying vec2 vTextureCoord;

varying mat4 light0;
varying mat4 material;
varying float material_shininess;

void main(void) {
    vec3 E = normalize(-vVertex.xyz); // we are in Eye Coordinates, so EyePos is (0,0,0)

    vec3 light0_dir = light0[3].xyz-vVertex.xyz;
    vec3 R = normalize(-reflect(light0_dir,vNormal));

    vec4 final_color = material[0];

    if(texture0_enabled>0.0) {
      final_color *= texture2D(texture0, vec2(vTextureCoord.s, vTextureCoord.t));
    }

    if(light0[3][3]>0.0) {
      vec4 Iamb = light0[1];

      vec4 Idiff = light0[0] * max(dot(vNormal, light0_dir), 0.0);
      Idiff = clamp(Idiff, 0.0, 1.0);

      vec4 Ispec = light0[2] * pow(max(dot(R,E),0.0), 0.3 * material_shininess);
      Ispec = clamp(Ispec, 0.0, 1.0);

      final_color *= Iamb*material[1] + Idiff + Ispec*material[2];
    }

    gl_FragColor = final_color;
}
