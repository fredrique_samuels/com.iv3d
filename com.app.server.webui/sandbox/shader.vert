attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 aTextureCoord;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat3 uNMatrix;

uniform mat4 uLight0;
uniform mat4 uMaterialColors;
uniform float uMaterialShininess;

uniform float uTexture0Enabled;

varying vec4 vVertex;
varying vec3 vNormal;
varying vec2 vTextureCoord;

varying mat4 material;
varying float material_shininess;

varying float texture0_enabled;
varying mat4 light0;

varying vec4 material_diffuse;
varying vec4 material_ambient;
varying vec4 material_emission;
varying vec4 material_specular;

void main(void) {
    gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);

    vVertex = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
    vNormal = uNMatrix * aVertexNormal;
    vTextureCoord = aTextureCoord;

    texture0_enabled = uTexture0Enabled;

    light0 = uLight0;

    material = uMaterialColors;
    material_shininess =uMaterialShininess;
}
