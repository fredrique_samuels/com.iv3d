/**
Create a SHADER OBJECT

src : The fragment shader source
return : JS Object
  shader : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
*/
function iv3d_gl_shader_createFragment(gl,src) {
  var shaderId = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(shaderId, src);
  gl.compileShader(shaderId);

  if (!gl.getShaderParameter(shaderId, gl.COMPILE_STATUS)) {
      var message = gl.getShaderInfoLog(shaderId);
      gl.deleteShader(shaderId);
      return {
          shader:-1,
          message:"Fragment Shader Error : "+message,
          error:true,
      };
  }

  return {
      shader:shaderId,
      message:"",
      error:false,
  };
}

/**
Create a SHADER OBJECT

src : The vertex shader source
return : JS Object
  shader : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
*/
function iv3d_gl_shader_createVertex(gl,src) {
  var shaderId = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(shaderId, src);
  gl.compileShader(shaderId);

  if (!gl.getShaderParameter(shaderId, gl.COMPILE_STATUS)) {
      gl.deleteShader(shaderId);
      var message = gl.getShaderInfoLog(shaderId);
      return {
          shader:-1,
          message:"Vertex Shader Error : "+message,
          error:true,
      };
  }

  return {
      shader:shaderId,
      message:"",
      error:false,
  };
}

/**
Delete a shader.
*/
function iv3d_gl_shader_delete(gl,shader) {
    gl.deleteShader(shader.shader);
}



/**
Create a SHADER PROGRAM

fragSource : The fragment shader source
vertSource : The vertex shader source
return : JS Object
  shaderProgram : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
  pMatrixUniform : Projection matrix uniform
  mvMatrixUniform : Model view uniform
  vertexPositionAttribute : The vertex attribute
*/
function iv3d_gl_shader_createProgram(gl,fragSource, vertSource) {
  var fragmentShader = iv3d_gl_shader_createFragment(gl,fragSource)
  if(fragmentShader.error) {
      return {
        program:-1,
        message:fragmentShader.message,
        error:true,
      }
  }

  var vertexShader = iv3d_gl_shader_createVertex(gl,vertSource)
  if(vertexShader.error) {
      iv3d_gl_shader_delete(gl,fragmentShader)
      return {
        program:-1,
        message:vertexShader.message,
        error:true,
      }
  }


  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader.shader);
  gl.attachShader(shaderProgram, fragmentShader.shader);
  gl.linkProgram(shaderProgram);

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      iv3d_gl_shader_delete(gl,fragmentShader)
      iv3d_gl_shader_delete(gl,vertexShader)
      gl.deleteProgram(shaderProgram);
      return {
        program:-1,
        message:"Could not initialise shaders",
        error:true,
      }
  }

  gl.useProgram(shaderProgram);

  const vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
  gl.enableVertexAttribArray(vertexPositionAttribute);

  var aTextureCoord0 = gl.getAttribLocation(shaderProgram, "aTextureCoord0");
  gl.enableVertexAttribArray(aTextureCoord0);

  var pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  var mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
  var texture0 = gl.getUniformLocation(shaderProgram, "texture0");

  return {
    shaderProgram:shaderProgram,
    vs:vertexShader,
    fs:fragmentShader,
    message:"",
    error:false,
    pMatrixUniform:pMatrixUniform,
    mvMatrixUniform:mvMatrixUniform,
    vertexPositionAttribute:vertexPositionAttribute,
    texCoordAttribute : aTextureCoord0,
    texture0:texture0,
  }
}

/**
Create a SHADER PROGRAM from script tags using the script ids.
See iv3d_gl_getScriptTagSourceById()

fragSource : The fragment shader source
vertSource : The vertex shader source
return : JS Object
  shaderProgram : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
  pMatrixUniform : Projection matrix uniform
  mvMatrixUniform : Model view uniform
  vertexPositionAttribute : The vertex attribute
*/
function iv3d_gl_shader_createProgramFromScriptTag(gl,fragScriptId, vertScriptId) {
  var fragSource = iv3d_gl_getScriptTagSourceById(fragScriptId).source
  var vertSource = iv3d_gl_getScriptTagSourceById(vertScriptId).source
  return iv3d_gl_shader_createProgram(gl,fragSource, vertSource)
}


/**
Bind a shader program as the current active shader.
*/
function iv3d_gl_shader_bindShaderProgram(gl, shaderProgram) {
  gl.useProgram(shaderProgram.shaderProgram);
  gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
  gl.enableVertexAttribArray(shaderProgram.texCoordAttribute);
}

/**
 Delete a shader program.
 */
function iv3d_gl_shader_deleteProgram(gl,shaderProgram) {
  iv3d_gl_shader_delete(gl, shaderProgram.vs);
  iv3d_gl_shader_delete(gl, shaderProgram.fs);
  gl.deleteProgram(gl, shaderProgram.shaderProgram);
}

/**
Read the content of a script tag and return it as a string.
Also returned is the script type if specified.

Example

< script id="shader-fs" type="x-shader/x-fragment">
    precision mediump float;

    void main(void) {
        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
</ script>

Given the iv3d_gl_getScriptTagSourceById("shader-fs")
returns {
  source : The script contents
  type: 'x-shader/x-fragment'
}

**/
function iv3d_gl_getScriptTagSourceById(id) {
  var shaderScript = document.getElementById(id);
  if (!shaderScript) {
      return null;
  }

  var str = "";
  var k = shaderScript.firstChild;
  while (k) {
      if (k.nodeType == 3) {
          str += k.textContent;
      }
      k = k.nextSibling;
  }

  return {
    source:str,
    type:shaderScript.type,
  }
}

function iv3d_gl_degToRad(degrees) {
  return degrees * Math.PI / 180;
}

/**
A matrix stack implimentation to manage the Projection and Model View matrixes

Usage:
var matrixStack = glMatrixStack();

functions:

getProjMatrix() : Get the Project matrix
perspective(fovy, aspect, n, f) : Generates a perspective projection matrix with the given bounds

getModelViewMatrix() : Get the Model View matrix
lookAt(eye, center, up) : Set the Model View to a generated matrix with the given eye position, focal point, and up axis
pushMatrix() : push a copy of the current Model View to a stack.
popMatrix() : pop the top matrix from the Model View stack.
translate(vec) : Translates the Model View by the given vector.
*/
function glMatrixStack() {
  var result = {};
  result.mvMatrixStack = [mat4.create()];
  mat4.identity(result.mvMatrixStack[result.mvMatrixStack.length-1]);

  result.pMatrix = mat4.create();

  result.perspective = function(fovy, aspect, n, f) {
    mat4.perspective(fovy, aspect, n, f, this.pMatrix);
  }
  result.__getActiveMvMat = function() {
    return this.mvMatrixStack[this.mvMatrixStack.length-1];
  }
  result.getProjMatrix = function() {
    return this.pMatrix;
  }
  result.getModelViewMatrix = function() {
    return this.__getActiveMvMat();
  }
  result.pushMatrix=function() {
    var copy = mat4.create();
    mat4.set(this.__getActiveMvMat(), copy);
    this.mvMatrixStack.push(copy);
  }
  result.popMatrix=function() {
    if (this.mvMatrixStack.length == 1) {
      return;
    }
    this.mvMatrixStack.pop();
  }
  result.lookAt=function(eye,center,up) {
    mat4.lookAt(eye, center, up, this.__getActiveMvMat());
  }
  result.translate=function(v) {
    mat4.translate(this.__getActiveMvMat(), v);
  }
  return result;
}



/**
A graphics camera object
*/
function glCamera() {
  var result = {};

  result.eye = [0,0,8];
  result.center = [0,0,0];
  result.up = [0,1,0];
  result.perspective={fovy:60,nearDist:0.1,farDist:100,};
  result.projMatrix = mat4.create();
  result.viewport=[0,0,1.0,1.0];
  result.setEye=function(v){this.eye=v;}
  result.setCenter=function(v){this.eye=v;}
  result.setUp=function(v){this.eye=v;}
  result.setViewport=function(x,y,w,h){this.viewport=[x,y,w,h];}
  result.getViewport=function(){return this.viewport;}
  result.setPerspective=function(fovy, n, f){this.perspective={fovy:fovy,nearDist:n,farDist:f,};}
  result.getPerspective=function(){return this.perspective;}

  result.setPerspective(60, 0.1, 100.0);
  return result;
}

/**
Object representing a 3d Model structure made up of triangles.

Methods:
destroy() : cleans the object grphic buffers.
render(gl, args) : Render the mesh to the screen.
    args={
      matrixStack,
      shaderProgram,
      objParams,
    }
*/
function glModel(gl, params) {
  var result = {}
  result.vertexCount=params.vertexCount;
  result.vertBuffer=gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, result.vertBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(params.vertices), gl.STATIC_DRAW);
  result.indexBuffer=gl.createBuffer();


  if(params.texCoords) {
    result.texCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, result.texCoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(params.texCoords), gl.STATIC_DRAW);
    result.texCoordCount=params.texCoordCount;
  }

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, result.indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(params.indices), gl.STATIC_DRAW);
  result.indexCount=params.indexCount;

  result.destroy=function(gl){
    gl.deleteBuffer(this.indexBuffer);
    gl.deleteBuffer(this.vertBuffer);
    gl.deleteBuffer(this.texCoordBuffer);
  }
  result.render=function(gl, args) {
    var matrixStack = args.matrixStack;
    var shaderProgram = args.shaderProgram;
    var glObj = args.glObj;
    var texture0 = glObj.texture0;

    if(shaderProgram) {
      matrixStack.pushMatrix();
      matrixStack.translate(glObj.position);

      gl.bindBuffer(gl.ARRAY_BUFFER, this.vertBuffer);
      gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

      if(this.texCoordBuffer && texture0) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordBuffer);
        console.log(this.texCoordBuffer);
        gl.vertexAttribPointer(shaderProgram.texCoordAttribute, 2, gl.FLOAT, false, 0, 0);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture0);
        gl.uniform1i(shaderProgram.texture0, 0);
      }

      gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, matrixStack.getProjMatrix());
      gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, matrixStack.getModelViewMatrix());

      // gl.drawArrays(gl.TRIANGLES, 0, this.vertexCount);
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
      gl.drawElements(gl.TRIANGLES, this.indexCount, gl.UNSIGNED_SHORT, 0);
      matrixStack.popMatrix();
    }
  }
  return result;
}


function glObject(shaderName, modelName) {
  var result = {};
  result.position=[0,0,0];
  result.shaderName = shaderName;
  result.modelName = modelName;
  result.setPosition=function(v){this.position=v;}
  result.setShaderName=function(n){this.shaderName=n;}
  result.setModelName=function(n){this.modelName=n;}
  return result;
}

function glSceneContent() {
  var result = {};
  result.objectList = {};
  result.getObjects=function() {
    var l = [];
    for (var s in this.objectList) {
      if (this.objectList.hasOwnProperty(s)) {
          l.push(this.objectList[s])
      }
    }
    return l;
  }
  result.createObject=function(id,shaderName,modelName) {
    this.objectList[id]=glObject(shaderName, modelName);
    return this.objectList[id];
  }
  result.getObject=function(id) {
    return this.objectList[id];
  }
  return result;
}

function glScene() {
  var result={};

  result.camera = glCamera();
  result.content = null;
  result.getCamera=function(){return this.camera;}
  result.setContent=function(c){this.content=c;}
  result.render=function(context) {
    var c = context.getSceneContent(this.content);
    if(c) {
      this._setupView(context.gl, context.matrixStack, context.width,context.height);
      var objs = c.getObjects();
      for (glObjInd in objs) {
        this._renderObj(context, objs[glObjInd]);
      }
    }
  }
  result._renderObj=function(context, glObj) {
    var gl = context.gl;
    var matrixStack = context.matrixStack;

    var model = context.getModel(glObj.modelName);
    var shaderProgram = context.bindShaderProgram(glObj.shaderName);

    // bind camera
    matrixStack.pushMatrix();
    matrixStack.lookAt(this.camera.eye, this.camera.center, this.camera.up);

    // draw objects
    matrixStack.pushMatrix();
    if(model) {
      model.render(gl,{
        matrixStack:matrixStack,
        glObj:glObj,
        shaderProgram:shaderProgram,
      })
    }
    matrixStack.popMatrix();
    matrixStack.popMatrix();
  }
  result._setupView=function(gl, matrixStack, w, h){
    gl.viewport(0, 0, w, h);
    var pers = this.camera.getPerspective();
    matrixStack.perspective(pers.fovy,
      w/h, pers.nearDist, pers.farDist);
  }
  return result;
}

/**
Graphics context object.


*/
function glContext(canvas, errorHandler) {

  var result = {}
  result.gl = canvas.getContext("experimental-webgl");
  result.error = false;
  if (!result.gl) {
      result.error = true;
  }
  result.width = canvas.width;
  result.height = canvas.height;
  result.matrixStack = glMatrixStack();

  result.gl.clearColor(0.1, 0.1, 0.1, 1.0);
  result.gl.enable(result.gl.DEPTH_TEST);
  result.shaders ={};
  result.sceneMap ={};
  result.modelMap={};
  result.contentMap={};
  result._activeShader = null;
  result.createShaderProgramFromSource=function(id, fragSource, vertSource){
    if(this.shaders[id]){
      iv3d_gl_shader_deleteProgram(this.shaders[id])
      delete this.shaders[id];
    }
    var newShader = iv3d_gl_shader_createProgram(this.gl, fragSource, vertSource);
    if(newShader.error) {
      errorHandler(newShader.message)
    } else {
      this.shaders[id]=newShader;
    }
  }
  result.createShaderProgramFromScriptTag=function(id, fragScriptId, vertScriptId){
    if(this.shaders[id]){
      iv3d_gl_shader_deleteProgram(this.shaders[id])
      delete this.shaders[id];
    }
    var newShader = iv3d_gl_shader_createProgramFromScriptTag(this.gl, fragScriptId, vertScriptId);
    if(newShader.error) {
      errorHandler(newShader.message)
    } else {
      this.shaders[id]=newShader;
    }
  }
  result.getActiveShaderProgram=function(){return this._activeShader;}
  result.bindShaderProgram=function(id) {
    if(this.shaders[id]) {
      iv3d_gl_shader_bindShaderProgram(this.gl, this.shaders[id]);
      this._activeShader=this.shaders[id];
    }
    return this._activeShader;
  }
  result.clearShaders=function() {
    for (var s in this.shaders) {
      if (this.shaders.hasOwnProperty(s)) {
          iv3d_gl_shader_bindShaderProgram(this, this.shaders[s]);
      }
    }
    this.shaders={};
  }
  result.destroy=function() {
    this.clearShaders();
    this.clearModels();
  }
  result.createScene=function(id) {
    this.deleteScene(id);
    var newScene = glScene();
    this.sceneMap[id] = newScene;
    return newScene;
  }
  result.deleteScene=function(id) {
      if(this.sceneMap[id]) {
        var scene = this.sceneMap[id];
        delete this.sceneMap[id];
        scene.destroy();
      }
  }
  result.getScene=function(id){return this.sceneMap[id];}
  result.forEachScene=function(callback) {
      for (var s in this.sceneMap) {
        if (this.sceneMap.hasOwnProperty(s)) {
            callback(this.sceneMap[s]);
        }
      }
  }
  result.render=function() {
    var gl = this.gl;
    gl.viewport(0, 0, this.width, this.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    for (var s in this.sceneMap) {
      if (this.sceneMap.hasOwnProperty(s)) {
          this.sceneMap[s].render(this);
      }
    }
  }
  result.createModel=function(id, modelParams) {
    this.deleteModel(id);
    var m =glModel(this.gl, modelParams);
    this.modelMap[id] = m;
    return m;
  }
  result.deleteModel=function(id){
    var m = this.modelMap[id];
    if(m) {
      m.destroy(this.gl);
      delete this.modelMap[id];
    }
  }
  result.getModel=function(id){return this.modelMap[id];}
  result.clearModels=function(){
    for (var s in this.modelMap) {
      if (this.modelMap.hasOwnProperty(s)) {
          this.modelMap[s].destroy(this);
      }
    }
    this.modelMap={};
  }
  result.createSceneContent=function(id) {
    this.deleteSceneContent(id);
    var m =glSceneContent();
    this.contentMap[id] = m;
    return m;
  }
  result.deleteSceneContent=function(id){
    var m = this.contentMap[id];
    if(m) {
      delete this.contentMap[id];
    }
  }
  result.getSceneContent=function(id){return this.contentMap[id];}
  return result;
}

function gl_resource_loader() {
  var result={}
  result.loadShaderProgram=function(context, id, fsUrl, vsUrl) {
    var vertGet = {
          method: "GET",
          url: vsUrl,
          dataType: "text",
        };
    var fragGet = {
          method: "GET",
          url: fsUrl,
          dataType: "text",
        };

    var contextDeferred = $.Deferred();
    contextDeferred.resolve(context);
    $.when(contextDeferred, id, $.ajax(vertGet), $.ajax(fragGet)).done(
        function(context, id, vs, fs) {
          console.log(fs[0], vs[0]);
          context.createShaderProgramFromSource(id, fs[0], vs[0]);
          context.render();
        }
    );
  }
  return result;
}

var glResourceLoader = gl_resource_loader()
