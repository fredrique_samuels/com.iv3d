/**
Create a SHADER OBJECT

src : The fragment shader source
return : JS Object
  shader : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
*/
function iv3d_gl_shader_createFragment(gl,src) {
  var shaderId = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(shaderId, src);
  gl.compileShader(shaderId);

  if (!gl.getShaderParameter(shaderId, gl.COMPILE_STATUS)) {
      var message = gl.getShaderInfoLog(shaderId);
      gl.deleteShader(shaderId);
      return {
          shader:-1,
          message:"Fragment Shader Error : "+message,
          error:true,
      };
  }

  return {
      shader:shaderId,
      message:"",
      error:false,
  };
}

/**
Create a SHADER OBJECT

src : The vertex shader source
return : JS Object
  shader : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
*/
function iv3d_gl_shader_createVertex(gl,src) {
  var shaderId = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(shaderId, src);
  gl.compileShader(shaderId);

  if (!gl.getShaderParameter(shaderId, gl.COMPILE_STATUS)) {
      gl.deleteShader(shaderId);
      var message = gl.getShaderInfoLog(shaderId);
      return {
          shader:-1,
          message:"Vertex Shader Error : "+message,
          error:true,
      };
  }

  return {
      shader:shaderId,
      message:"",
      error:false,
  };
}

/**
Delete a shader.
*/
function iv3d_gl_shader_delete(gl,shader) {
    gl.deleteShader(shader.shader);
}



/**
Create a SHADER PROGRAM

fragSource : The fragment shader source
vertSource : The vertex shader source
return : JS Object
  shaderProgram : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
  pMatrixUniform : Projection matrix uniform
  mvMatrixUniform : Model view uniform
  vertexPositionAttribute : The vertex attribute
*/
function iv3d_gl_shader_createProgram(gl,fragSource, vertSource) {
  var fragmentShader = iv3d_gl_shader_createFragment(gl,fragSource)
  if(fragmentShader.error) {
      return {
        program:-1,
        message:fragmentShader.message,
        error:true,
      }
  }

  var vertexShader = iv3d_gl_shader_createVertex(gl,vertSource)
  if(vertexShader.error) {
      iv3d_gl_shader_delete(gl,fragmentShader)
      return {
        program:-1,
        message:vertexShader.message,
        error:true,
      }
  }


  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader.shader);
  gl.attachShader(shaderProgram, fragmentShader.shader);
  gl.linkProgram(shaderProgram);

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      iv3d_gl_shader_delete(gl,fragmentShader)
      iv3d_gl_shader_delete(gl,vertexShader)
      gl.deleteProgram(shaderProgram);
      return {
        program:-1,
        message:"Could not initialise shaders",
        error:true,
      }
  }

  gl.useProgram(shaderProgram);

  const vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
  gl.enableVertexAttribArray(vertexPositionAttribute);

  var aTextureCoord0 = gl.getAttribLocation(shaderProgram, "aTextureCoord");
  gl.enableVertexAttribArray(aTextureCoord0);

  var aVertexNormal = gl.getAttribLocation(shaderProgram, "aVertexNormal");
  gl.enableVertexAttribArray(aVertexNormal);

  var pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  var mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
  var uNMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
  var texture0 = gl.getUniformLocation(shaderProgram, "texture0");

  return {
    shaderProgram:shaderProgram,
    vs:vertexShader,
    fs:fragmentShader,
    message:"",
    error:false,
    pMatrixUniform:pMatrixUniform,
    mvMatrixUniform:mvMatrixUniform,
    uNMatrixUniform:uNMatrixUniform,
    vertexPositionAttribute:vertexPositionAttribute,
    aVertexNormalAttribute:aVertexNormal,
    texCoordAttribute : aTextureCoord0,

    texture0:texture0,
    texture0Enabled:gl.getUniformLocation(shaderProgram, "uTexture0Enabled"),
    materialColors:gl.getUniformLocation(shaderProgram, "uMaterialColors"),
    materialShininess:gl.getUniformLocation(shaderProgram, "uMaterialShininess"),
    light0:gl.getUniformLocation(shaderProgram, "uLight0"),
  }
}

/**
Create a SHADER PROGRAM from script tags using the script ids.
See iv3d_gl_getScriptTagSourceById()

fragSource : The fragment shader source
vertSource : The vertex shader source
return : JS Object
  shaderProgram : The new shader id or -1 if and error occured
  error : true if error, false otherwise.
  message : The error message if error is true.
  pMatrixUniform : Projection matrix uniform
  mvMatrixUniform : Model view uniform
  vertexPositionAttribute : The vertex attribute
*/
function iv3d_gl_shader_createProgramFromScriptTag(gl,fragScriptId, vertScriptId) {
  var fragSource = iv3d_gl_getScriptTagSourceById(fragScriptId).source
  var vertSource = iv3d_gl_getScriptTagSourceById(vertScriptId).source
  return iv3d_gl_shader_createProgram(gl,fragSource, vertSource)
}


/**
Bind a shader program as the current active shader.
*/
function iv3d_gl_shader_bindShaderProgram(gl, shaderProgram) {
  gl.useProgram(shaderProgram.shaderProgram);
  gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
  gl.enableVertexAttribArray(shaderProgram.texCoordAttribute);
}

/**
 Delete a shader program.
 */
function iv3d_gl_shader_deleteProgram(gl,shaderProgram) {
  iv3d_gl_shader_delete(gl, shaderProgram.vs);
  iv3d_gl_shader_delete(gl, shaderProgram.fs);
  gl.deleteProgram(gl, shaderProgram.shaderProgram);
}

/**
Read the content of a script tag and return it as a string.
Also returned is the script type if specified.

Example

< script id="shader-fs" type="x-shader/x-fragment">
    precision mediump float;

    void main(void) {
        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
</ script>

Given the iv3d_gl_getScriptTagSourceById("shader-fs")
returns {
  source : The script contents
  type: 'x-shader/x-fragment'
}

**/
function iv3d_gl_getScriptTagSourceById(id) {
  var shaderScript = document.getElementById(id);
  if (!shaderScript) {
      return null;
  }

  var str = "";
  var k = shaderScript.firstChild;
  while (k) {
      if (k.nodeType == 3) {
          str += k.textContent;
      }
      k = k.nextSibling;
  }

  return {
    source:str,
    type:shaderScript.type,
  }
}

function iv3d_gl_degToRad(degrees) {
  return degrees * Math.PI / 180;
}

/**
A matrix stack implimentation to manage the Projection and Model View matrixes

Usage:
var matrixStack = glMatrixStack();

functions:

getProjMatrix() : Get the Project matrix
perspective(fovy, aspect, n, f) : Generates a perspective projection matrix with the given bounds

getModelViewMatrix() : Get the Model View matrix
lookAt(eye, center, up) : Set the Model View to a generated matrix with the given eye position, focal point, and up axis
pushMatrix() : push a copy of the current Model View to a stack.
popMatrix() : pop the top matrix from the Model View stack.
translate(vec) : Translates the Model View by the given vector.
*/
function glMatrixStack() {
  var result = {};
  result.mvMatrixStack = [mat4.create()];
  mat4.identity(result.mvMatrixStack[result.mvMatrixStack.length-1]);

  result.pMatrix = mat4.create();

  result.perspective = function(fovy, aspect, n, f) {
    mat4.perspective(fovy, aspect, n, f, this.pMatrix);
  }
  result.__getActiveMvMat = function() {
    return this.mvMatrixStack[this.mvMatrixStack.length-1];
  }
  result.getProjMatrix = function() {
    return this.pMatrix;
  }
  result.getModelViewMatrix = function() {
    return this.__getActiveMvMat();
  }
  result.pushMatrix=function() {
    var copy = mat4.create();
    mat4.set(this.__getActiveMvMat(), copy);
    this.mvMatrixStack.push(copy);
  }
  result.popMatrix=function() {
    if (this.mvMatrixStack.length == 1) {
      return;
    }
    this.mvMatrixStack.pop();
  }
  result.lookAt=function(eye,center,up) {
    mat4.lookAt(eye, center, up, this.__getActiveMvMat());
  }
  result.translate=function(v) {
    mat4.translate(this.__getActiveMvMat(), v);
  }
  return result;
}



/**
A graphics camera object
*/
function glCamera() {
  var result = {};

  result.eye = [0,0,8];
  result.center = [0,0,0];
  result.up = [0,1,0];
  result.perspective={fovy:60,nearDist:0.1,farDist:100,};
  result.projMatrix = mat4.create();
  result.viewport=[0,0,1.0,1.0];
  result.setEye=function(v){this.eye=v;}
  result.setCenter=function(v){this.eye=v;}
  result.setUp=function(v){this.eye=v;}
  result.setViewport=function(x,y,w,h){this.viewport=[x,y,w,h];}
  result.getViewport=function(){return this.viewport;}
  result.setPerspective=function(fovy, n, f){this.perspective={fovy:fovy,nearDist:n,farDist:f,};}
  result.getPerspective=function(){return this.perspective;}

  result.setPerspective(60, 0.1, 100.0);
  return result;
}

function glTexture(gl, params) {
  var result = {};
  result.texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, result.texture);
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, params.image);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  result.destroy=function(gl) {
    gl.deleteTexture(this.texture);
  }
  return result;
}


function glLight() {
  var result = {};
  result.enabled = false;
  result.diffuseColor = [0.5,.5,.5,1.0];
  result.ambientColor = [.8,.8,.8,1.0];
  result.specularColor = [1.0,1.0,1.0,1.0];
  result.position = [0,0,0];
  result.enable=function(){this.enabled=true;}
  result.disable=function(){this.enabled=false;}
  result.toMat4=function() {
    var m = mat4.create();
    var e = 0.0;
    if(this.enabled) {
      e=1.0;
    }
    mat4.set([this.diffuseColor[0],this.diffuseColor[1],this.diffuseColor[2],this.diffuseColor[3],
              this.ambientColor[0],this.ambientColor[1],this.ambientColor[2],this.ambientColor[3],
              this.specularColor[0],this.specularColor[1],this.specularColor[2],this.specularColor[3],
              this.position[0],this.position[1],this.position[2],this.specularColor[3], e],m);
    return m;
  }
  return result;
}

/**
Object representing a 3d Model structure made up of triangles.

Methods:
destroy() : cleans the object grphic buffers.
render(gl, args) : Render the mesh to the screen.
    args={
      matrixStack,
      shaderProgram,
      objParams,
    }
*/
function glModel(gl, params) {
  var result = {}
  result.vertexCount=params.vertexCount;
  result.vertBuffer=gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, result.vertBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(params.vertices), gl.STATIC_DRAW);

  result.indexGroups = params.indexGroups;
  result.materials = params.materials;

  for(var indGroupInd in result.indexGroups) {
    var indGroup = result.indexGroups[indGroupInd];
    indGroup.indexBuffer=gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indGroup.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indGroup.indices), gl.STATIC_DRAW);
  }

  if(params.texCoords) {
    result.texCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, result.texCoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(params.texCoords), gl.STATIC_DRAW);
    result.texCoordCount=params.texCoordCount;
  }

  if(params.vertexNormals) {
    result.vertexNormalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, result.vertexNormalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(params.vertexNormals), gl.STATIC_DRAW);
    result.vertexNormalCount=params.vertexNormalCount;
  }

  result.textures = {};

  result.destroy=function(gl){
    gl.deleteBuffer(this.indexBuffer);
    gl.deleteBuffer(this.vertBuffer);
    gl.deleteBuffer(this.texCoordBuffer);
    for(var indGroup in this.indexGroups) {
      gl.deleteBuffer(indGroup.indexBuffer);
    }
    this.clearTextures(gl);
  }
  result.createTexture=function(gl, id, textureParams) {
    this.deleteTexture(gl, id);
    var m=glTexture(gl, textureParams);
    this.textures[id] = m;
    return m;
  }
  result.deleteTexture=function(gl, id){
    var m = this.textures[id];
    if(m) {
      m.destroy(gl);
      delete this.textures[id];
    }
  }
  result.getTexture=function(id){return this.textures[id];}
  result.clearTextures=function(gl){
    for (var s in this.textures) {
      if (this.textures.hasOwnProperty(s)) {
          this.textures[s].destroy(gl);
      }
    }
    this.textures={};
  }
  result.render=function(gl, args) {
    var matrixStack = args.matrixStack;
    var shaderProgram = args.shaderProgram;
    var glObj = args.glObj;
    var lights = args.lights;

    if(shaderProgram) {
      matrixStack.pushMatrix();
      matrixStack.translate(glObj.position);

      gl.bindBuffer(gl.ARRAY_BUFFER, this.vertBuffer);
      gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

      gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, matrixStack.getProjMatrix());
      gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, matrixStack.getModelViewMatrix());

      var light0 = lights[0];
      light0.enable();
      gl.uniformMatrix4fv(shaderProgram.light0, false, light0.toMat4());

      var normalMatrix = mat3.create();
      mat4.toInverseMat3(matrixStack.getModelViewMatrix(), normalMatrix);
      mat3.transpose(normalMatrix);
      gl.uniformMatrix3fv(shaderProgram.uNMatrixUniform, false, normalMatrix);

      if(this.texCoordBuffer) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordBuffer);
        gl.vertexAttribPointer(shaderProgram.texCoordAttribute, 2, gl.FLOAT, false, 0, 0);
      }

      if(this.vertexNormalBuffer) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexNormalBuffer);
        gl.vertexAttribPointer(shaderProgram.aVertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);
      }


      for(var indGroupInd in this.indexGroups) {
        var indGroup = this.indexGroups[indGroupInd];
        var mat = this.materials[indGroup.material];
        if(mat) {
          var colors = mat4.create();
          mat4.set([mat.diffuseColor[0],mat.diffuseColor[1],mat.diffuseColor[2],mat.diffuseColor[3],
            mat.ambientColor[0],mat.ambientColor[1],mat.ambientColor[2],mat.ambientColor[3],
            mat.specularColor[0],mat.specularColor[1],mat.specularColor[2],mat.specularColor[3],
            mat.emissionColor[0],mat.emissionColor[1],mat.emissionColor[2],mat.emissionColor[3]], colors);
          gl.uniformMatrix4fv(shaderProgram.materialColors, false, colors);
          gl.uniform1f(shaderProgram.materialShininess, mat.shininess);

          var texture0 = this.getTexture(mat.texture0);
          if(texture0) {
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, texture0.texture);
            gl.uniform1i(shaderProgram.texture0, 0);
            gl.uniform1f(shaderProgram.texture0Enabled, 1.0);
          } else {
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, null);
            gl.uniform1i(shaderProgram.texture0, 0);
            gl.uniform1f(shaderProgram.texture0Enabled, 0.0);
          }

          gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indGroup.indexBuffer);
          gl.drawElements(gl.TRIANGLES, indGroup.indexCount, gl.UNSIGNED_SHORT, 0);
        }
      }
      matrixStack.popMatrix();
    }
  }
  return result;
}


function glObject(shaderName, modelName) {
  var result = {};
  result.position=[0,0,0];
  result.shaderName = shaderName;
  result.modelName = modelName;
  result.setPosition=function(v){this.position=v;}
  result.setShaderName=function(n){this.shaderName=n;}
  result.setModelName=function(n){this.modelName=n;}
  return result;
}

function glSceneContent() {
  var result = {};
  result.objectList = {};
  result.light0=glLight();
  result.getObjects=function() {
    var l = [];
    for (var s in this.objectList) {
      if (this.objectList.hasOwnProperty(s)) {
          l.push(this.objectList[s])
      }
    }
    return l;
  }
  result.createObject=function(id,shaderName,modelName) {
    this.objectList[id]=glObject(shaderName, modelName);
    return this.objectList[id];
  }
  result.getObject=function(id) {
    return this.objectList[id];
  }
  result.getLight0=function() {
    return this.light0;
  }
  return result;
}

function glScene() {
  var result={};

  result.camera = glCamera();
  result.content = null;
  result.getCamera=function(){return this.camera;}
  result.setContent=function(c){this.content=c;}
  result.render=function(context) {
    var c = context.getSceneContent(this.content);
    if(c) {
      this._setupView(context.gl, context.matrixStack, context.width,context.height);
      var objs = c.getObjects();
      var lights = [c.getLight0()];
      for (glObjInd in objs) {
        this._renderObj(context, objs[glObjInd], lights);
      }
    }
  }
  result._renderObj=function(context, glObj, lights) {
    var gl = context.gl;
    var matrixStack = context.matrixStack;

    var model = context.getModel(glObj.modelName);
    var shaderProgram = context.bindShaderProgram(glObj.shaderName);

    // bind camera
    matrixStack.pushMatrix();
    matrixStack.lookAt(this.camera.eye, this.camera.center, this.camera.up);

    // draw objects
    matrixStack.pushMatrix();
    if(model) {
      model.render(gl,{
        matrixStack:matrixStack,
        glObj:glObj,
        shaderProgram:shaderProgram,
        lights:lights,
      })
    }
    matrixStack.popMatrix();
    matrixStack.popMatrix();
  }
  result._setupView=function(gl, matrixStack, w, h){
    gl.viewport(0, 0, w, h);
    var pers = this.camera.getPerspective();
    matrixStack.perspective(pers.fovy,
      w/h, pers.nearDist, pers.farDist);
  }
  return result;
}

/**
Graphics context object.


*/
function glContext(canvas, errorHandler) {

  var result = {}
  result.gl = canvas.getContext("experimental-webgl");
  result.error = false;
  if (!result.gl) {
      result.error = true;
  }
  result.width = canvas.width;
  result.height = canvas.height;
  result.matrixStack = glMatrixStack();

  result.gl.clearColor(0.5, 0.5, 0.5, 1.0);
  result.gl.enable(result.gl.DEPTH_TEST);
  result.shaders ={};
  result.sceneMap ={};
  result.modelMap={};
  result.contentMap={};
  result._activeShader = null;
  result.createShaderProgramFromSource=function(id, fragSource, vertSource){
    if(this.shaders[id]){
      iv3d_gl_shader_deleteProgram(this.shaders[id])
      delete this.shaders[id];
    }
    var newShader = iv3d_gl_shader_createProgram(this.gl, fragSource, vertSource);
    if(newShader.error) {
      errorHandler(newShader.message)
    } else {
      this.shaders[id]=newShader;
    }
  }
  result.createShaderProgramFromScriptTag=function(id, fragScriptId, vertScriptId){
    if(this.shaders[id]){
      iv3d_gl_shader_deleteProgram(this.shaders[id])
      delete this.shaders[id];
    }
    var newShader = iv3d_gl_shader_createProgramFromScriptTag(this.gl, fragScriptId, vertScriptId);
    if(newShader.error) {
      errorHandler(newShader.message)
    } else {
      this.shaders[id]=newShader;
    }
  }
  result.getActiveShaderProgram=function(){return this._activeShader;}
  result.bindShaderProgram=function(id) {
    if(this.shaders[id]) {
      iv3d_gl_shader_bindShaderProgram(this.gl, this.shaders[id]);
      this._activeShader=this.shaders[id];
    }
    return this._activeShader;
  }
  result.clearShaders=function() {
    for (var s in this.shaders) {
      if (this.shaders.hasOwnProperty(s)) {
          iv3d_gl_shader_bindShaderProgram(this, this.shaders[s]);
      }
    }
    this.shaders={};
  }
  result.destroy=function() {
    this.clearShaders();
    this.clearModels();
  }
  result.createScene=function(id) {
    this.deleteScene(id);
    var newScene = glScene();
    this.sceneMap[id] = newScene;
    return newScene;
  }
  result.deleteScene=function(id) {
      if(this.sceneMap[id]) {
        var scene = this.sceneMap[id];
        delete this.sceneMap[id];
        scene.destroy();
      }
  }
  result.getScene=function(id){return this.sceneMap[id];}
  result.forEachScene=function(callback) {
      for (var s in this.sceneMap) {
        if (this.sceneMap.hasOwnProperty(s)) {
            callback(this.sceneMap[s]);
        }
      }
  }
  result.render=function() {
    var gl = this.gl;
    gl.viewport(0, 0, this.width, this.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    for (var s in this.sceneMap) {
      if (this.sceneMap.hasOwnProperty(s)) {
          this.sceneMap[s].render(this);
      }
    }
  }
  result.createModel=function(id, modelParams) {
    this.deleteModel(id);
    var m=glModel(this.gl, modelParams);
    this.modelMap[id] = m;
    return m;
  }
  result.deleteModel=function(id){
    var m = this.modelMap[id];
    if(m) {
      m.destroy(this.gl);
      delete this.modelMap[id];
    }
  }
  result.getModel=function(id){return this.modelMap[id];}
  result.clearModels=function(){
    for (var s in this.modelMap) {
      if (this.modelMap.hasOwnProperty(s)) {
          this.modelMap[s].destroy(this);
      }
    }
    this.modelMap={};
  }
  result.createSceneContent=function(id) {
    this.deleteSceneContent(id);
    var m =glSceneContent();
    this.contentMap[id] = m;
    return m;
  }
  result.deleteSceneContent=function(id){
    var m = this.contentMap[id];
    if(m) {
      delete this.contentMap[id];
    }
  }
  result.getSceneContent=function(id){return this.contentMap[id];}
  return result;
}

function gl_resource_loader() {
  var result={}
  result.loadShaderProgram=function(context, id, fsUrl, vsUrl) {
    var vertGet = {
          method: "GET",
          url: vsUrl,
          dataType: "text",
        };
    var fragGet = {
          method: "GET",
          url: fsUrl,
          dataType: "text",
        };

    var contextDeferred = $.Deferred();
    contextDeferred.resolve(context);
    $.when(contextDeferred, id, $.ajax(vertGet), $.ajax(fragGet)).done(
        function(context, id, vs, fs) {
          // console.log(fs[0], vs[0]);
          context.createShaderProgramFromSource(id, fs[0], vs[0]);
          context.render();
        }
    );
  }
  result.loadTexture=function(context, model, id, url, imageType) {

    function createOnLoadFunction(context, model, id, img) {
      return function() {
        model.createTexture(context.gl, id, {image:img});
        context.render();
      }
    }

      function createCallback(img) {
        return function(e) {
          if (this.status == 200) {
            var arr = new Uint8Array(this.response);
            var raw = String.fromCharCode.apply(null,arr);
            var b64=btoa(raw);
            var dataURL="data:image/png;base64,"+b64;
            img.src = dataURL;
          }
        };
    }

    var image = new Image();
    image.onload = createOnLoadFunction(context, model, id ,image);

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = createCallback(image);
    xhr.send();
  }
  result.loadModel = function(context, id, modelUrl) {
    var modelGet = {
          method: "GET",
          url: modelUrl,
          dataType: "text",
        };

    var contextDeferred = $.Deferred();
    contextDeferred.resolve(context);
    var idDeferred = $.Deferred();
    idDeferred.resolve(id);
    $.when(contextDeferred, idDeferred, $.ajax(modelGet)).done(
        function(context, id, modelData) {
          var jsonModel = JSON.parse(modelData[0]);
          var model = context.createModel(id, jsonModel);
          context.render();

          for (var t in jsonModel.textures) {
            var jsonTex = jsonModel.textures[t];
            glResourceLoader.loadTexture(context, model,
              jsonTex.id, jsonTex.url,jsonTex.format);
          }
        }
    );
  }

  return result;
}

var glResourceLoader = gl_resource_loader()
