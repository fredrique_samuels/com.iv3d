var path = require('path')
const debug = process.env.NODE_ENV !== "production";
const webpack = require('webpack');
console.log(__dirname);

module.exports = {

    entry: {
      main: "./src/iv3d/apps/server/webui/js/site/sitehome_provider",
      // app_datanode_edit_demo: "./src/iv3d/apps/server/webui/js/site/app_datanode_edit_demo_provider",
      // tvision: "./src/iv3d/apps/server/webui/js/site/tvision_provider"

      // test_all: "./src/iv3d/apps/server/webui/js/tests/test_all_provider"
    },
    output: {
        path: __dirname,
        // publicPath: "./../yugioh.game/card_images_lr",
        filename: "[name].bundle.js",
        chunkFilename: "[id].chunk.js"
    },
    module: {
        loaders: [
            { test: /\.json$/, loader: 'json' },
            {
                test: /\.js$/,
                //exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                  presets: ['es2015', 'react'],
                  plugins: ['react-html-attrs', 'transform-decorators-legacy2', 'transform-class-properties'],
                }
            }
        ]
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin()
    ],
    devtool: 'source-map'
};
