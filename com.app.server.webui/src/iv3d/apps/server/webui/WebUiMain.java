package iv3d.apps.server.webui;

import com.app.server.base.server.AppServerLauncher;

/**
 * Created by fred on 2016/09/26.
 */
public class WebUiMain {
    public static void main(String[] args) {
        new AppServerLauncher().run();
    }
}
