package iv3d.apps.server.webui.home.handlers;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.web.AppPathHandler;
import com.google.inject.Inject;
import com.iv3d.common.ResourceLoader;
import iv3d.apps.server.webui.home.HomeApp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomePageHandler extends AppPathHandler {

    private final String homeHtml;

    @Inject
    public HomePageHandler(LocalAppSettingsManager cache) {
        AppContext appContext = cache.get(HomeApp.APP_ID);
        this.homeHtml = appContext.getPrivateResource("index.html");
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String s = ResourceLoader.readAsString(homeHtml);
        returnHtml(s);
    }

    @Override
    public String getMethod() {
        return "GET";
    }
}