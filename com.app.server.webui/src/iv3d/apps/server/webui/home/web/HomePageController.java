package iv3d.apps.server.webui.home.web;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import iv3d.apps.server.webui.home.HomeApp;
import iv3d.apps.server.webui.home.handlers.HomePageHandler;

import javax.inject.Inject;


public class HomePageController extends AppWebController {
    @Inject
    private HomePageHandler homePageHandler;

    @Inject
    public HomePageController(LocalAppSettingsManager cache) {
        super(cache.get(HomeApp.APP_ID));
    }

    @Override
    public void configure() {
        set("/", homePageHandler);
    }
}
