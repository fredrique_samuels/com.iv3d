
class Theme {
  constructor(value) {
    this.defaultValue = value;
    this.currentValue = value;
  }

  set(properties) {
    this.currentValue = Object.assign({}, this.currentValue, properties)
  }

  reset() {
    this.currentValue = defaultValue;
  }

  get() {
    return Object.assign({}, this.currentValue)
  }
}

export const WORLDMAP_WIDGET_THEME = 'WORLDMAP_WIDGET_THEME';
const worldmapTheme = {
  backgroundColor: "rgba(1, 1, 1, .5)",
  opacity:0.5,
  fillOpacity:.8,
  landFillColor:"#005c99",
  landStrokeColor:"none",
}

export const WORLDMAP_EVENTS_WIDGET_THEME = 'WORLDMAP_EVENTS_WIDGET_THEME'
const worldEventsMapTheme = {
  categoryPanelColor:"#005c99",
  categoryPanelOpacity:.4,
}

export default class Themes {
  constructor() {
    this.values = {
      WORLDMAP_WIDGET_THEME:new Theme(worldmapTheme),
      WORLDMAP_EVENTS_WIDGET_THEME:new Theme(worldEventsMapTheme)
    }
  }

  get(theme) {
    return this.values[theme].get()
  }
}
