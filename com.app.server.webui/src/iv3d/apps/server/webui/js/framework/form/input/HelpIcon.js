import React from 'react'

export default class HelpIcon extends React.Component {
    render() {
        return <i class=" fa fa-question-circle form-help"></i>
    }
}