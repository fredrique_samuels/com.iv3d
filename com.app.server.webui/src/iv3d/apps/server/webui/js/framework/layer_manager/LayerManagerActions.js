
export function setAppLayerBiz(oldModel, params) {
    const {layerId, layoutParams, content, contentParams, layerParams, uicontext} = params
    const oldLayers = oldModel.layers
    const n = {
        layoutParams : layoutParams,
        content : content,
        layerId : layerId,
        contentParams : contentParams,
        layerParams : layerParams,
        uicontext:uicontext,
    }

    const newState = Object.assign({}, oldModel, {layers: oldLayers.set(layerId,n)})
    return newState
}

export function removeAppLayerBiz(oldModel, params) {
    const {layerId} = params
    const oldLayers = oldModel.layers
    return Object.assign({}, oldModel, {layers: oldLayers.delete(layerId)})
}


export function removeAllAppLayerBiz(oldModel, params) {
    const oldLayers = oldModel.layers
    return Object.assign({}, oldModel, {layers: oldLayers.clear()})
}