import React from 'react'

export default class ActionCell extends React.Component {

    render() {
        const {table} = this.props
        if(!table.rowActions) return null
        if(table.rowActions.length==0) return null

        return (
            <td class="tt-table-data-cell"><i class="fa fa-th"></i></td>
        )
    }
}