

export class ActionConfig {
    constructor(params){
        this.params = params
    }

    getIcon(){return this.params.icon}
    getLabel(){return this.params.label}
    getAction(){return this.params.action}
    getStyle(){return this.params.style}
}

export default class ActionConfigBuilder {
    constructor() {
        this.params = {
            icon:null,
            label:"",
            action:null,
            style:{}
        }
    }

    setLabel(l){this.params.label = l;return this}
    setIcon(icon){this.params.icon = icon;return this}
    setAction(a){this.params.action = a;return this}
    setStyle(s){this.params.style = s;return this}
    set(ac){this.params = Object.assign({}, ac.params);return this}
    build(){ return new ActionConfig(this.params)}
}