import React from 'react'
import AbstractCellDisplayTransformer from '../AbstractCellDisplayTransformer'
import {CELL_STATUS_WARNING} from "../TableConstants";
import {CELL_STATUS_ERROR} from "../TableConstants";
import {CELL_STATUS_SUCCESS} from "../TableConstants";
import {CELL_STATUS_WAITING} from "../TableConstants";
import {CELL_STATUS_RUNNING} from "../TableConstants";
import {CELL_STATUS_STOPPED} from "../TableConstants";

export default class StatusDisplayTransformer extends AbstractCellDisplayTransformer {
    constructor() {
        super()
    }

    transform(value) {
        if(Array.isArray(value)) {
            const comps = value.map(v => this.createComp(v));
            return this.createDataCell(comps)
        }
        return this.createDataCell(this.createComp(value))
    }

    createComp(value) {
        switch (value.status) {
            case CELL_STATUS_WARNING:
                return <i class="tt-status-icon fa fa-warning color-warning-text" onClick={value.action}></i>
            case CELL_STATUS_ERROR:
                return <i class="tt-status-icon fa fa-exclamation-circle color-error-text" onClick={value.action}></i>
            case CELL_STATUS_SUCCESS:
                return <i class="tt-status-icon fa fa-check-circle color-success-text" onClick={value.action}></i>
            case CELL_STATUS_WAITING:
                return <i class="tt-status-icon fa fa-ellipsis-h color-primary-text" onClick={value.action}></i>
            case CELL_STATUS_RUNNING:
                return <i class="tt-status-icon fa fa-refresh fa-spin fa-fw color-primary-text" onClick={value.action}></i>
            case CELL_STATUS_STOPPED:
                return <i class="tt-status-icon fa fa-ban color-default-text" onClick={value.action}></i>
            default: null
        }
    }
}