import React from 'react'
import ErrorText from './ErrorText'

export default class FormInputContainer extends React.Component {
    render() {
        const { inputContainerCssClasses, noTitle, style } = this.props.viewParams
        let cssClasses = "form-input-container"
        if(noTitle) {
            cssClasses+=' form-input-container-no-title'
        }
        if(inputContainerCssClasses) {
            for(let c in inputContainerCssClasses) {
                cssClasses+= " "+inputContainerCssClasses[c]
            }
        }

        return (<div class={cssClasses} style={style}>
            <ErrorText ignoreError={this.props.viewParams.ignoreError} text={this.props.viewParams.errorText} />
            <div style={{position:"relative"}} >
                {this.props.children}
            </div>
        </div>)
    }
}
