import React from 'react'
import OptionsSelect from './OptionsSelect'
import BoxOptionCompFactory from './BoxOptionCompFactory'

export default class MultiSelect extends React.Component {
    render() {
        const viewParams = Object.assign({}, this.props.viewParams, {optionsType:"checkbox", optionCompFactory:new BoxOptionCompFactory()})
        return <OptionsSelect viewParams={viewParams} />
    }
}
