import React from 'react'
import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'

export default class OptionsSelect extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        return (
            <FormInputContainer viewParams={this.props.viewParams}>
                {this.getOptionComps()}
            </FormInputContainer>
        )
    }
    getOptionComps() {
        const {optionCompFactory} = this.props.viewParams
        return optionCompFactory.build(this.props.viewParams)
    }
}