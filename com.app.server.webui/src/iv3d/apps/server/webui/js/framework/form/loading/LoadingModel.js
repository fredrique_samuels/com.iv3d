
class LoadingModelUpdater {
    constructor(model) {
        this.params = Object.assign({}, model.state())
        this.model = model
    }
    setText(v){this.params.text = v;return this}
    setProgress(v){this.params.progress = v;return this}
    setDone(){this.params.done = true;return this}
    setError(){this.params.error = true;return this}
    run() {
        if (this.model.autoClose()) {
            this.model.close()
        } else {
            this.model.setState(this.params)
        }
    }
}


export default class LoadingModel {
    constructor(parent, settings={}) {
        this.parent = parent
        this.settings =  settings
    }
    newState(){return {text:"Loading...", progress:0.0, done:false, error:false}}
    state(){return this.parent.state}
    setState(s){return this.parent.setState(s)}
    uicontext(){return this.parent.props.layerModel.contentParams.uicontext}
    autoClose(){return this.settings.autoClose}
    done(){return this.state().done}
    error(){return this.state().error}
    text(){return this.state().text}
    progress(){return this.state().progress}
    progressStrPerc(deci=2){return this.state().progress.toFixed(deci)+"%"}
    busy(){return !this.done() && !this.error()}
    success(){return this.done() && !this.error()}
    close(){
        const { layerId } = this.parent.props.layerModel
        const { uicontext } = this.parent.props.layerModel.contentParams
        uicontext.layerComponentManager().removeLayer(layerId)
    }
    update() {
        return new LoadingModelUpdater(this)
    }
}


