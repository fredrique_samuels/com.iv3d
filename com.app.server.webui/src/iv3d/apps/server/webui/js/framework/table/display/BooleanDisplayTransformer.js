import React from 'react'
import AbstractCellDisplayTransformer from '../AbstractCellDisplayTransformer'
import DataCell from '../DataCell'

export default class BooleanDisplayTransformer extends AbstractCellDisplayTransformer {

    constructor() {
        super()
    }

    transform(value) {
        if(!value) return this.returnFalse()
        const stringValue = new String(value);
        var lowerCase = stringValue.toLowerCase();
        if(lowerCase.localeCompare("on")==0) return this.returnTrue("ON")
        if(lowerCase.localeCompare("off")==0) return this.returnFalse("OFF")
        if(lowerCase.localeCompare("1")==0) return this.returnTrue("TRUE")
        if(lowerCase.localeCompare("0")==0) return this.returnFalse("FALSE")
        if(lowerCase.localeCompare("true")==0) return this.returnTrue("TRUE")
        if(lowerCase.localeCompare("false")==0) return this.returnFalse("FALSE")
        if(lowerCase.localeCompare("null")==0) return this.returnFalse("NULL")
        if(lowerCase.localeCompare("none")==0) return this.returnFalse("NONE")
        return Boolean(value)?this.returnTrue(stringValue):this.returnFalse(stringValue);
    }
    returnFalse(value) {
        return <DataCell cssClasses={["boolean-cell-false"]} >{value?value:"FALSE"}</DataCell>
    }
    returnTrue(value) {
        return <td class="tt-table-data-cell boolean-cell-true">{value?value:"TRUE"}</td>
    }
}