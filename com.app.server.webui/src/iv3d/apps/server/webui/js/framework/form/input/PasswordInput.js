import React from 'react'
import SingleLineInput from './SingleLineInput'

export default class PasswordInput extends React.Component {
    render() {
        const lineParams = Object.assign({}, this.props.viewParams, {inputType:"password"})
        return <SingleLineInput viewParams={lineParams} />
    }
}
