export default class ViewModel {
  constructor(parent) {
      this.parent = parent
  }
  init(state){if(state){this.parent.state = state}}
  state(){return this.parent.state}
  setState(s) {
      this.parent.setState(s)
  }
  i8n(){return this.uicontext().i8n()}
  uicontext(){return this.parent.props.uicontext}
}
