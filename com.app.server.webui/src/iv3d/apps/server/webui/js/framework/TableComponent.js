import React from 'react'
import {IdGenerator} from "../common";

import TableHeadSection from './table/TableHeadSection'
import TableRow from './table/TableRow'
import TableModel from './table/TableModel'


class TablePageControls extends React.Component {
    render() {
        const { tableModel } = this.props;
        if(tableModel.hasPaging()) {
            return (
                <div class="center-content-vh color-primary"
                     style={{position: "relative", border: "1px", height: "50px"}}>
                    <i class="fa-action fa fa-arrow-left" onClick={tableModel.pageDown.bind(tableModel)} style={{left: "10px", position: "absolute"}}></i>
                    <label><i class="fa-action fa fa-info-circle" style={{marginRight: "5px"}}></i>{tableModel.pageCountText()}</label>
                    <i class="fa-action fa fa-arrow-right" onClick={tableModel.pageUp.bind(tableModel)} style={{right: "10px", position: "absolute"}}></i>
                </div>
            )
        }
        return null;
    }
}

export class TableComponent extends React.Component {

    constructor(){
        super()
        this.widgetId = IdGenerator.generateGlobalId();
    }

    componentWillMount() {
        const tableModel = new TableModel(this);
        tableModel.init();
    }

    componentDidMount() {
        const tableModel = new TableModel(this);
        tableModel.initScrollState();
    }

    render() {
        const tableModel = new TableModel(this);
        const { table, scrollContainerId } = this.state;
        const rowsComps =  table.rows.map( r => <TableRow tableModel={tableModel} rowParams={r} />)
        const hasPaging = tableModel.hasPaging()
        return (
            <div class="tt-table-panel" >
                <div style={{position:"relative",height:"100%", maxWidth:"100%"}}>
                    <div id={scrollContainerId} class="tt-table-scroll-panel" style={hasPaging?{height:"calc(100% - 50px)"}:{}}>
                        <table class="tt-table">
                            <TableHeadSection tableModel={tableModel} applyIds={false} />
                            <tbody>
                            {rowsComps}
                            </tbody>
                        </table>
                        <table class="tt-table tt-table-heading-ovelay">
                            <TableHeadSection tableModel={tableModel} applyIds={true} />
                        </table>
                    </div>
                    <TablePageControls tableModel={tableModel} />
                </div>
            </div>
        )
    }

}



class TableContentFactory {
    createContent(layerModel) {
        return <TableComponent uicontext={layerModel.contentParams.uicontext} table={layerModel.contentParams.table} />
    }
}

export class TableComponentManager  {
    constructor(uicontext) {
        this.uicontext = uicontext
    }

    createTable(table){
        return <TableComponent uicontext={this.uicontext} table={table} />
    }

    createLayeredTable(table, layoutParams={}) {

        const defaultLayoutParams = {
            position:"relative",
            top:"0%",
            left:"0%",
            width:"100%",
            height:"100%",
            overflow:"hidden",
            padding:"10px",
            backgroundColor:"rgba(0,0,0,.7)",
            boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
        }


        var contentParams = {
            contentHandlerFactory: new TableContentFactory(),
            layoutParams: Object.assign({},defaultLayoutParams, layoutParams),
            table : table
        };
        return this.uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:true})
    }
}
