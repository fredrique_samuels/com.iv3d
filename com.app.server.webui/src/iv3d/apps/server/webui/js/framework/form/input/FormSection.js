import React from 'react'

export default class FormSection extends React.Component {
    render() {
        const { title } = this.props.viewParams
        return (
            <div class="form-section">
                <h1 class="form-section-heading">{title}</h1>
                {this.props.children}
            </div>
        )
    }
}