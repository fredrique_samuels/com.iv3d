import React from 'react'
import SingleLineInput from './SingleLineInput'

export default class EmailInput extends React.Component {
    render() {
        const lineParams = Object.assign({}, this.props.viewParams, {inputType:"email"})
        return <SingleLineInput viewParams={lineParams} />
    }
}