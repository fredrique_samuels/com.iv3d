import React from 'react'

import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'


export default class FileInput extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { name } = this.props.viewParams
        return (
            <FormInputContainer viewParams={this.props.viewParams}>
                <div class="form-control-file-input-container">
                    <input type="file" class="form-control"/>
                </div>
                <i class="file-input-left-corner"></i>
            </FormInputContainer>
        )
    }
}