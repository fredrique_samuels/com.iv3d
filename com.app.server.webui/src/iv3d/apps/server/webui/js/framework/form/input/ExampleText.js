import React from 'react'

export default class ExampleText extends React.Component {
    render() {
        if(this.props.ignoreError)
            return null
        if(this.props.text)
            return <p class="help-block">{this.props.text}</p>
        return null
    }
}