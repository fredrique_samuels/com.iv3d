import React from 'react'

export default class ErrorText extends React.Component {
    render() {
        if(this.props.text)
            return <p class="help-block error-block">{this.props.text}</p>
        return null
    }
}