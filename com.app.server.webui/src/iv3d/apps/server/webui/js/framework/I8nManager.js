import messages_en from './i8n/messages_en.json'

export default class I8nManager {
    constructor(uicontext) {
       this.uicontext = uicontext
    }

    getMessage(key) {
        const msg = messages_en.messages[key]
        if(msg) {
            return msg
        }
        console.error("I8n message not found for key:", key)
        return key
    }
}