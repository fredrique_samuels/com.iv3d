import React from 'react'
import SingleLineInput from './SingleLineInput'

export default class UrlInput extends React.Component {
    render() {
        const lineParams = Object.assign({}, this.props.viewParams, {inputType:"url"})
        return <SingleLineInput viewParams={lineParams} />
    }
}