import React from 'react'
import $ from 'jquery'
import * as FormConstants from './form/FormConstants'

import { FormInputIdGenerator } from './form/FormConstants'

import TextInput from './form/input/TextInput'
import TextArea from './form/input/TextArea'
import NumberInput from './form/input/NumberInput'
import MultiSelect from './form/input/MultiSelect'
import SingleSelect from './form/input/SingleSelect'
import DropdownSelect from './form/input/DropdownSelect'
import FileInput from './form/input/FileInput'
import PasswordInput from './form/input/PasswordInput'
import ColorInput from './form/input/ColorInput'
import UrlInput from './form/input/UrlInput'
import EmailInput from './form/input/EmailInput'
import FormSection from './form/input/FormSection'

import BlockTextDisplay from './form/display/BlockTextDisplay'
import TextDisplay from './form/display/TextDisplay'
import ImageDisplay from './form/display/ImageDisplay'
import InfoBlock from './form/display/InfoBlock'
import TabsView from './form/display/TabsView'

import SpinLoaderComponentFactory from "./form/loading/SpinLoaderComponentFactory";
import ProgressLoaderComponentFactory from "./form/loading/ProgressLoaderComponentFactory";

class FormInputFactory {
    buildFromInputs(inputs) {
      if(inputs) {
        return inputs.map(p => this.build(p))
      }
      return []
    }
    build(viewParams) {

        if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXT) {
            return <TextInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXTAREA) {
            return <TextArea viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT) {
            return <MultiSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT) {
            return <SingleSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT) {
            return <DropdownSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FILE) {
            return <FileInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_COLOR) {
            return <ColorInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_URL) {
            return <UrlInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_EMAIL) {
            return <EmailInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_PASSWORD) {
            return <PasswordInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_NUMBER) {
            return <NumberInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FORMSECTION) {
            return ( <FormSection viewParams={viewParams} >
                    {this.buildFromInputs(viewParams.inputs)}
                </FormSection>
            )
        }

        if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT) {
            return <BlockTextDisplay viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_IMAGE) {
            return <ImageDisplay viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK) {
            return <InfoBlock viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_TABS) {
            return <TabsView tabsParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_DISPLAY_TYPE_TEXT) {
            return <TextDisplay textParams={viewParams} />
        }

        return null
    }

    buildInput(viewParams) {
        if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXT) {
            return <TextInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_TEXTAREA) {
            return <TextArea viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT) {
            return <MultiSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT) {
            return <SingleSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT) {
            return <DropdownSelect viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FILE) {
            return <FileInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_COLOR) {
            return <ColorInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_URL) {
            return <UrlInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_EMAIL) {
            return <EmailInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_PASSWORD) {
            return <PasswordInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_NUMBER) {
            return <NumberInput viewParams={viewParams} />
        } else if(viewParams.type==FormConstants.FORM_INPUT_TYPE_FORMSECTION) {
            return ( <FormSection viewParams={viewParams} >
                    {this.buildFromInputs(viewParams.inputs)}
                </FormSection>
            )
        }
        return null
    }
}

class FormActionFactory {
    buildFromActions(parent, actions) {
      if(actions) {
        return actions.map(p => this.build(parent, p))
      }
      return []
    }
    build(form, viewParams) {
      if(viewParams.type==FormConstants.FORM_ACTION_TYPE_CANCEL) {
        return  <button type="button" class="form-button btn btn-error btn-sm" onClick={form.close.bind(form)} >Cancel</button>
      } else if(viewParams.type==FormConstants.FORM_ACTION_TYPE_ACTION) {
            return  <button type="button" class="form-button btn btn-error btn-sm" onClick={(event) => {viewParams.action(event, form)}} >{viewParams.title}</button>
      } else if(viewParams.type==FormConstants.FORM_ACTION_TYPE_SUBMIT) {
        return <input class="form-button btn btn-primary btn-sm" id={form.submitButtonId} type="submit" defaultValue={viewParams.title}/>
      }
      return null
    }
}

class FormInputIterator {
  iterate(inputs, callback)  {
    if(inputs) {
      return inputs.map(p => {
        callback(p)
        this.iterate(p.inputs, callback)
        return null
      })
    }
    return []
  }
}

export default class FormComponent extends React.Component {

    constructor() {
      super()
      this.formId = FormInputIdGenerator.generate()
    }

    render() {
        const style={
            backgroundColor:"grey"
        }

        const containerStyle = {
            position:"relative",
            width:"100%",
            padding:"12px"
        }

        const items = new FormInputFactory().buildFromInputs(this.state.form.inputs)
        const actions = new FormActionFactory().buildFromActions(this, this.state.form.actions)

        const formContent = (<div style={containerStyle}>
            <div class="panel-text" style={{width:"100%"}}>
              <form id={this.formId}>
                {items}
                {actions}
              </form>
            </div>
          </div>)
        return formContent
    }

    componentWillMount() {
      this.state = {form : this.props.form}
    }

    componentDidMount() {
      const submitCallback =  this.submit.bind(this)
      const callback = function(event)
      {
          console.log('FormComponent', event)
         var theForm = $(this);
         submitCallback(theForm.serialize(), theForm.serializeArray())

         // send xhr request
        //  $.ajax({
        //      type: $theForm.attr('method'),
        //      url: $theForm.attr('action'),
        //      data: $theForm.serialize(),
        //      success: function(data) {
        //          console.log('Yay! Form sent.');
        //      }
        //  });

         // prevent submitting again
         return false;
      }
      $('#' + this.formId).submit(callback);
    }

    submit(data, dataArray) {
      const dataMap = {}
      dataArray.map(p => dataMap[p.name] = p.value)
      const validation = this.props.form.validation
      if(validation) {
        const { type, url, enctype, callback } = validation
        if(type=='callback') {
          try {
            const result = callback({data, dataArray, dataMap})
            if(result.passed) {
              this.submitForm(data, dataArray, dataMap)
            }
            else {
              this.processValidationErrors(result)
            }
          } catch (e){
            console.error("An error occured", e);
          }
        }
      } else {
        try {
            this.submitForm(data, dataArray)
        } catch (e){
          console.error("An error occured", e);
        }
      }
    }

    submitForm(data, dataArray, dataMap) {
      const submit = this.props.form.submit
      if(submit) {
        const { type, url, enctype, callback } = submit
        if(type=='callback') {
          if(callback({data, dataArray, dataMap})) {
              this.close()
          }
        }
      } else {
          this.close()
      }
    }

    processValidationErrors(result) {
       const { errors } = result
       const newState = Object.assign({}, this.state.form, {})
       new FormInputIterator().iterate(newState.inputs, function(input) {
         input.errorText=null
         for(let v in result.errors) {
           if(errors[v].field==input.name && input.name) {
             input.errorText = errors[v].message
           }
         }
       })
       this.setState(newState)
    }

    close()  {
      if(this.props.layerModel) {
        const { layerId } = this.props.layerModel
        const { uicontext } = this.props.layerModel.contentParams
        uicontext.layerComponentManager().removeLayer(layerId)
      }
    }

}


class FormContentFactory {
    createContent(layerModel) {
        return <FormComponent form={layerModel.contentParams.form} layerModel={layerModel} />
    }
}

export class FormComponentManager  {
  constructor(uicontext) {
    this.uicontext = uicontext
  }

  createForm(form){
    return <FormComponent form={form} />
  }

  __createInput(viewParams, inputType){return new FormInputFactory().build(Object.assign({}, viewParams, {type:inputType}))}
  createTextInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_TEXT)}
  createTextAreaInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_TEXTAREA)}
  createMultiSelect(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_MULTIOPTION_SELECT)}
  createSingleSelect(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_SINGLEOPTION_SELECT)}
  createDropdownSelect(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_DROPDOWN_SELECT)}
  createColorInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_COLOR)}
  createUrlInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_URL)}
  createEmailInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_EMAIL)}
  createFileInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_FILE)}
  createPasswordInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_PASSWORD)}
  createNumberInput(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_INPUT_TYPE_NUMBER)}

  createBlockTextDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_BLOCK_TEXT)}
  createTextDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_TEXT)}
  createImageDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_IMAGE)}
  createInfoBlockDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK)}
  createTabsDisplay(viewParams={}) {return this.__createInput(viewParams, FormConstants.FORM_DISPLAY_TYPE_TABS)}

  createLayeredForm(form, layoutParams={}) {

    const defaultLayoutParams = {
        position:"relative",
        top:"25%",
        left:"20%",
        width:"60%",
        maxHeight:"50%",
        overflow:"auto",
        backgroundColor:"rgba(0,0,0,.7)",
        boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
    }


    var contentParams = {
        contentHandlerFactory: new FormContentFactory(),
        layoutParams: Object.assign({},defaultLayoutParams, layoutParams),
        form : form
    };
    return this.uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:false})
  }
  containsCancelInput(form) {
      const { actions } = form
      if(actions) {
          for( let aIndex in actions) {
              if (actions[aIndex].type==FormConstants.FORM_ACTION_TYPE_CANCEL) {
                  return true
              }
          }
      }
      return false
  }


    createLayeredHelpBox(helpParams) {
        const form = this.createForm(
            {
                inputs : [
                    {
                        type: FormConstants.FORM_DISPLAY_TYPE_INFO_BLOCK,
                        heading:helpParams.heading,
                        graphics: helpParams.graphics,
                        text: helpParams.text,
                        noTitle:true
                    }
                ]
            }
        )

        const titledBoxParams = {
            title:<i class="fa fa-info-circle fa-1x color-primary-text"></i>,
            content:form,
            style:{
                backgroundColor:"rgba(0,0,0,.7)",
                boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
            }
        }

        this.uicontext.titledBoxManager().createLayeredTitledBox(titledBoxParams, {}, {canBeCleared:false})
    }

    createLayeredSpinLoader(controller) {
        const titledBoxParams = {
            title:"",
            contentFactory: new SpinLoaderComponentFactory(controller),
            noCloseButton:true,
            headless:true,
            style:{
                backgroundColor:"rgba(0,0,0,.7)",
                boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
            }
        }
        this.uicontext.titledBoxManager().createLayeredTitledBox(titledBoxParams,{}, {canBeCleared:false})
    }

    createLayeredProgressLoader(controller) {
        const titledBoxParams = {
            title:"",
            contentFactory: new ProgressLoaderComponentFactory(controller),
            noCloseButton:true,
            headless:true,
            style:{
                backgroundColor:"rgba(0,0,0,.7)",
                boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
            }
        }
        this.uicontext.titledBoxManager().createLayeredTitledBox(titledBoxParams,{}, {canBeCleared:false})
    }
}
