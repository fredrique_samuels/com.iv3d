import React from 'react'
import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'
import ExampleText from './ExampleText'

export default class TextArea extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { name, placeholder, exampleText, rows} = this.props.viewParams
        return (
            <FormInputContainer viewParams={this.props.viewParams}>
                <textarea name={name} class="form-control form-textarea" rows={rows?rows:"3"}></textarea>
                <ExampleText text={exampleText} />
            </FormInputContainer>
        )
    }
}