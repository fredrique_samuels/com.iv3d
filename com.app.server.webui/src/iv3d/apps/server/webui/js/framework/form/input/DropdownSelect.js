import React from 'react'
import FormGroup from './FormGroup'
import FormInputContainer from './FormInputContainer'

export default class DropdownSelect extends React.Component {
    render() {
        const inputComp = this.createView()
        return <FormGroup viewParams={Object.assign({}, this.props.viewParams, {inputComp})} />
    }
    createView() {
        const { name } = this.props.viewParams
        const containerViewParams = Object.assign({}, this.props.viewParams, {ignoreError:true})
        return (
            <FormInputContainer viewParams={containerViewParams} >
                <select name={name} class="form-control">
                    {this.getOptions()}
                </select>
                <span class="select-right-corner"></span>
            </FormInputContainer>
        )
    }
    getOptions() {
        const {options, value} = this.props.viewParams
        if(options) {
            return options.map(p => this.getOptionView(p, value) )
        }
        return null
    }
    getOptionView(p, selectedValue) {
        if(p.value==selectedValue)
            return <option value={p.value} selected>{p.display}</option>
        return <option value={p.value}>{p.display}</option>
    }
}
