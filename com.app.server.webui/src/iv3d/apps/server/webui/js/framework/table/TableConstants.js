import React from 'react'

import BooleanDisplayTransformer from "./display/BooleanDisplayTransformer";
import DefaultDisplayTransformer from "./display/DefaultDisplayTransformer";
import StatusDisplayTransformer from "./display/StatusDisplayTransformer";
import ImageDisplayTransformer from "./display/ImageDisplayTransformer";
import ActionDisplayTransformer from "./display/ActionDisplayTransformer";

export const CELL_DISPLAY_TYPE_TEXT =  new DefaultDisplayTransformer()
export const CELL_DISPLAY_TYPE_BOOLEAN =  new BooleanDisplayTransformer()
export const CELL_DISPLAY_TYPE_STATUS = new StatusDisplayTransformer()
export const CELL_DISPLAY_TYPE_IMAGE = new ImageDisplayTransformer()
export const CELL_DISPLAY_TYPE_ACTIONS = new ActionDisplayTransformer()

export const CELL_STATUS_WARNING = "WARNING"
export const CELL_STATUS_ERROR = "ERROR"
export const CELL_STATUS_SUCCESS = "SUCCESS"
export const CELL_STATUS_WAITING = "WAITING"
export const CELL_STATUS_RUNNING = "RUNNING"
export const CELL_STATUS_STOPPED = "STOPPED"