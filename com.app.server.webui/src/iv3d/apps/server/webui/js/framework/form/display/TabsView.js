import React from 'react'
import ViewModel from "../../ViewModel";

class TabButton extends React.Component {
    render() {
        const { tabsModel, tabIndex, title} = this.props
        const active = tabsModel.activeTabIndex()==tabIndex
        const colorClass = active ? "color-primary" : "color-disabled"

        const cssClasses = "btn btn-sm " + colorClass
        const event = this.clicked.bind(this)
        const text = title ? title : "Tab " + (tabIndex + 1)
        const css = {
            borderBottomRightRadius:"0px",
            borderBottomLeftRadius:"0px"
        }
        return (
            <div class="inline-element">
                <button onClick={event} style={css} class={cssClasses} >{text}</button>
            </div>
        )
    }
    clicked() {
        const { tabsModel, tabIndex} = this.props
        tabsModel.setTabActive(tabIndex)
    }
}

class TabContent extends React.Component {
    render() {
        const { tabsModel, tabIndex, view } = this.props
        const active = tabsModel.activeTabIndex()==tabIndex
        return (
            <div style={{width:"100%", display:active?"block":"none"}}>
                {view}
            </div>
        )
    }
}

class TabsModel extends ViewModel {
    constructor(parent) {
        super(parent)
    }

    newState(){
        const { tabsParams } = this.parent.props
        const { displays } = tabsParams
        return {
            tabIndex: (displays && displays.length) > 0 ? 0 : -1,
            tabCount: (displays && displays.length) > 0 ? displays.length : 0
        }
    }
    displays(){
        const { tabsParams } = this.parent.props
        if(!tabsParams)return []

        const { displays } = tabsParams
        if(!displays)return []

        return displays
    }
    activeTabIndex(){ return this.state().tabIndex}
    setTabActive(index){
        this.setState({tabIndex:index})
    }
}


export default class TabsView extends React.Component {

    componentWillMount() {
        this.state = new TabsModel(this).newState()
    }

    render() {

        const tabsModel = new TabsModel(this)
        const viewComps = tabsModel.displays().map( (d, index) => <TabContent view={d.viewFactory.render()} tabIndex={index} tabsModel={tabsModel}/> )
        const buttonComps = tabsModel.displays().map( (d, index) => <TabButton title={d.title} tabIndex={index} tabsModel={tabsModel}/> )
        const tabButtons = (
            <div style={{width:"100%"}}>
                {buttonComps}
            </div>
        )
        return (
            <div style={{width:"100%", padding:"5px"}}>
                {buttonComps.length>1?tabButtons:null}
                <div style={{position:"relative",width:"100%"}}>
                    {viewComps}
                </div>
            </div>
        )
    }
}
