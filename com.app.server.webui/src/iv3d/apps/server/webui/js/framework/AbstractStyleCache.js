import Immutable from 'immutable'

export default class AbstractStyleCache {
    constructor(uicontext) {
        this.styles = new Immutable.Map()
    }
    set(id, value) {
        this.styles=this.styles.set(id, value)
    }
    get(id, userValues={}) {
        const cached = this.styles.get(id)
        if(cached) {
            return Object.assign({}, cached, userValues)
        }
        return userValues
    }
    getList(ids, userValues={}) {
        let css = {}
        for(let index in ids) {
            const s = this.get(ids[index])
            if(s) {
                css = Object.assign({}, css, s)
            }
        }
        return css //Object.assign({}, css, userValues)
    }
}