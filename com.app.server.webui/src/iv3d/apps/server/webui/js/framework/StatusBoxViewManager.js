import React from 'react'
import Immutable from 'immutable'

import TsbStyleCache from './taskstatusbox/TsbStyleCache'
import TabsView from './form/display/TabsView'
import ViewModel from "./ViewModel";
import TableViewBuilder from "./table/TableViewBuilder";


class StatusBoxModel extends ViewModel {

    constructor(parent, statusBoxParams) {
        super(parent);
        this.params = statusBoxParams ? statusBoxParams : parent.props.statusBoxParams
    }
    status(){return this.params.status}
    heading(){return this.params.heading}
    statusText(){return this.params.statusText}
    displays() {
        const viewParams = {displays:this.params.displays};
        return this.uicontext().formComponentManager().createTabsDisplay(viewParams)
    }
}

class StatusBoxView extends React.Component {


    render() {
        const styleCache = new TsbStyleCache();
        const boxModel = new StatusBoxModel(this);

        return (
            <div class="fill-parent" style={styleCache.get('tsb-ctnr-panel')}>
                {boxModel.heading()?<div class="text-heading" style={styleCache.get('tsb-heading-cntr')}>{boxModel.heading()}</div>:null}
                {boxModel.status()?<div class={"color-"+boxModel.status() + " panel-font"} style={styleCache.get('tsb-status')}>{boxModel.statusText()}</div>:null}
                {boxModel.displays()}
            </div>
        )
    }
}



class TaskStatus {
    constructor(params) {
        this.params = params
    }
    attributes(){return this.params.attributes ? this.params.attributes : []}
    hasError(){ return this.params.error }
    errorMessage(){return this.params.errorMessage}
    status(){
        const attrStatus = this.__getTaskStatusFromAttribute()
        if(attrStatus) {
            return attrStatus
        }

        if(this.hasError()) {
            return "error"
        }

        return "success"
    }
    name(){return "No Name Yet!!!"}
    runId() {
        if(this.params.id > 0)
            return this.params.id

        const attrRunId = this.__getRunIdFromAttribute()
        if(attrRunId > 0) return attrRunId

        return -1
    }

    __getRunIdFromAttribute() {
        const taskIdJsonStr = this.__getAttributeValue('taskId')
        if(taskIdJsonStr) {
            const taskRunId = JSON.parse(taskIdJsonStr)
            return taskRunId.runId
        }
        return -1
    }

    __getTaskStatusFromAttribute() {
        const status = this.__getAttributeValue('taskStatus')
        if(status) {
            return status.toLowerCase()
        }
        return null
    }

    __getAttributeValue(attrName) {
        const filtered = this.attributes().filter( attr => attr.name == attrName )
        if(filtered.length==0) return null
        return filtered[0].value
    }
}


class TaskStatusBoxModel extends ViewModel {

    constructor(parent) {
        super(parent);
        this.taskStatus = new TaskStatus(this.parent.props.taskStatusParams)
    }
    statusViewParams() {
        return {
            heading:this.taskStatus.name(),
            statusText:this.__statusText(),
            status:this.taskStatus.status(),
            displays:this.__getDisplays()
        }
    }

    __statusText() {
        const status = this.taskStatus.status()
        const runId = this.taskStatus.runId()
        let text = ""

        if(runId>0)
            text += '#' + runId + " "

        if(status)
            text += status.toUpperCase()

        return text
    }
    __getDisplays() {
        return [
            this.__statsTab(),
            this.__childrenTab(),
        ].filter( t => t != null)
    }
    __childrenTab() {
        return {
            title: "Children",
            viewFactory: {render:(() => {
                return new TableViewBuilder()
                    .createHeading("Id")
                    .commit()
                    .createHeading("Type")
                    .commit()
                    .createHeading("Status")
                    .commit()
                    .buildView(this.uicontext())
            }).bind(this)}
        };
    }
    __statsTab() {
        return {
            title: "Stats",
            viewFactory: this.__getStatViewFactory()
        };
    }
    __getStatViewFactory() {
        const attributes = this.taskStatus.attributes()
        if(!attributes)return {render:() => []}
        const attrComps = attributes.map( attr => {
            const params = {
                title:this.__getAttributeLabel(attr),
                text: this.__getAttributeValue(attr)
            }
            return this.uicontext().formComponentManager().createTextDisplay(params)
        })
        return {render:() => attrComps}
    }
    __getAttributeValue(attr){
        const transformer = this.__valueTransformers().get(attr.name);
        return transformer ? transformer(attr.value) : String(attr.value)
    }
    __getAttributeLabel(attr){
        return this.i8n().getMessage("task.status.attribute." + attr.name)
    }
    __valueTransformers(){
        let valueTransformers =  Immutable.Map()
        return valueTransformers.set('taskId', (v) => "#" + JSON.parse(v).runId)
    }
}

class TaskStatusBoxView extends React.Component {

    render() {
        const { uicontext } = this.props
        const taskStatusBoxModel = new TaskStatusBoxModel(this);
        return <StatusBoxView uicontext={uicontext} statusBoxParams={taskStatusBoxModel.statusViewParams()}/>
    }
}

class TaskStatusBoxFactory {
    createContent(layerModel) {
        return <TaskStatusBoxView uicontext={layerModel.contentParams.uicontext} taskStatusParams={layerModel.contentParams.taskStatusParams} layerModel={layerModel} />
    }
}

export default class StatusBoxViewManager {

    constructor(uicontext) {
        this.uicontext = uicontext
    }

    createStatusBox(statusBoxParams) {
        return <TaskStatusBoxView statusBoxParams={statusBoxParams} />
    }

    createLayeredTaskStatusBox(taskStatusParams, layoutParams={}, layerParams={canBeCleared:true}) {
        const defaultLayoutParams = {
            position:"relative",
            width:"60%",
            height:"70%",
            overflow:"hidden",
            backgroundColor:"rgba(0,0,0,.7)",
            boxShadow: "0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 24px 60px 0 rgba(0, 0, 0, 0.5)"
        }

        var contentParams = {
            contentHandlerFactory: new TaskStatusBoxFactory(),
            layoutParams: Object.assign({},defaultLayoutParams, layoutParams),
            taskStatusParams : taskStatusParams,
            uicontext:this.uicontext
        };

        return this.uicontext.layerComponentManager().addLayer(contentParams,layerParams)
    }
}