class LayerIdGenerator {
    constructor() {
        this.seed = 0
    }

    generate() {
        return "application_layer_"  + (++this.seed)
    }
}

export const layerIdGenerator = new LayerIdGenerator()