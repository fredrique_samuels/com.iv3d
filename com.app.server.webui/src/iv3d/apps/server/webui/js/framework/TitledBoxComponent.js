import React from 'react'
import {TITLED_BOX_COLOR_WARNING} from "./titledbox/TitleBoxConstants";
import {TITLED_BOX_COLOR_DEFAULT} from "./titledbox/TitleBoxConstants";
import {TITLED_BOX_COLOR_PRIMARY} from "./titledbox/TitleBoxConstants";
import {TITLED_BOX_COLOR_ERROR} from "./titledbox/TitleBoxConstants";
import {TITLED_BOX_COLOR_SUCCESS} from "./titledbox/TitleBoxConstants";

export const ICON_TYPE_FONTAWESOME = 'icon.type.fontawesome'
export const ICON_TYPE_IMAGE = 'icon.type.image'

class FontAwesomeIcon extends React.Component {
  render() {
    const {graphic} = this.props
    return (<h3 class="panel-title">
      <i class={"fa "+graphic}></i>
    </h3>)
  }
}

class ImageIcon extends React.Component {
  render() {
    const {graphic} = this.props
    return  (<div class="titledbox-image-icon inline">
      <svg viewBox="0 0 300 300" width="100%" height="100%">
           <image width="300" height="300" xlinkHref={graphic}/>
       </svg>
    </div>)
  }
}

class TitledBoxIcon  extends React.Component {
  render() {
    const {icon} = this.props
    if (icon) {
        const {type, graphic} = icon
        if(type==ICON_TYPE_FONTAWESOME) {
          return <FontAwesomeIcon graphic={graphic}/>
        }
        if(type==ICON_TYPE_IMAGE) {
          return <ImageIcon graphic={graphic}/>
        }
    }
    return null
  }
}

class TitledBoxHeading extends React.Component {
    render() {
        const {title, icon, noCloseButton, headless } = this.props.params
        if(headless)return null
        return (<div class={"titledbox-heading-bar border-box " + this.getColorClass()} >
          <TitledBoxIcon icon={icon}/>
          <h3 class="panel-title">{title}</h3>
          {noCloseButton?null:<i class="fa fa-window-close color-error-text" onClick={this.close.bind(this)} style={{cursor:"pointer",position:"absolute",right:"10px"}}></i>}
        </div>)
    }

    getColorClass() {
        const { color } = this.props.params
        switch(color) {
            case TITLED_BOX_COLOR_WARNING:
                return "color-warning"
            case TITLED_BOX_COLOR_ERROR:
                return "color-error"
            case TITLED_BOX_COLOR_SUCCESS:
                return "color-success"
            case TITLED_BOX_COLOR_PRIMARY:
                return "color-primary"
            case TITLED_BOX_COLOR_DEFAULT:
                return "color-default"
        }
        return "color-info"
    }
    close() {
        const { layerId } = this.props.layerModel
        const { uicontext } = this.props.layerModel.contentParams
        uicontext.layerComponentManager().removeLayer(layerId)
    }
}

export default class TitledBoxComponent extends React.Component {
    render() {
        const {style, content, contentFactory} = this.props.params
        const {layerModel} = this.props

        return (<div class="back-panel titledbox" style={style}>
            <div class="fill-parent" style={{position:"relative"}}>
             <div class="titledbox-content-spacer"></div>
             <div class="titledbox-content-container">
                {contentFactory?contentFactory.render(layerModel.contentParams.uicontext, layerModel):content}
             </div>
              <TitledBoxHeading layerModel={this.props.layerModel} params={this.props.params}/>
            </div>
          </div>)
    }
}

class TitledBoxFactory {
    createContent(layerModel) {
        return <TitledBoxComponent params={layerModel.contentParams.titledBoxParams} layerModel={layerModel} />
    }
}

export class TitledBoxManager {
  constructor(uicontext) {
    this.uicontext = uicontext
  }

  createBox(params) {
    return <TitledBoxComponent params={params} />
  }

  createLayeredTitledBox(titledBoxParams, layoutParams={}, layerParams={canBeCleared:true}) {
    const defaultLayoutParams = {
        position:"relative",
        top:"0%",
        left:"0%",
        width:"100%",
        height:"100%",
        display:"flex",
        alignItems: "center",
        justifyContent: "center"
    }

    var contentParams = {
        contentHandlerFactory: new TitledBoxFactory(),
        layoutParams: Object.assign({},defaultLayoutParams, layoutParams),
        titledBoxParams : titledBoxParams,
        uicontext:this.uicontext
    };

    return this.uicontext.layerComponentManager().addLayer(contentParams,layerParams)
  }

}
