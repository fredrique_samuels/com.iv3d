import React from 'react'

export default class DataCell extends React.Component {
    render() {
        const { cssClasses } = this.props
        return (
            <td class={"tt-table-data-cell " + cssClasses.join(' ')}>
                {this.props.children}
            </td>
        )
    }
}