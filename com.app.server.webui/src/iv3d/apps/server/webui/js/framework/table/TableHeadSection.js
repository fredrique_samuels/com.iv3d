import React from 'react'

import ActionsColumnHeading from './ActionsColumnHeading'
import TableColumnsManageSection from './TableColumnsManageSection'
import TableHeading from './TableHeading'

export default class TableHeadSection extends React.Component {
    render() {
        const {applyIds, tableModel} = this.props
        const headings = tableModel.headings()
        const headingComps = headings?headings.map(h => <TableHeading header={h} applyIds={applyIds}/>):null

        return (
            <thead>
                <TableColumnsManageSection tableModel={tableModel} />
                <tr>
                    <ActionsColumnHeading tableModel={tableModel} />
                    {headingComps}
                </tr>
            </thead>
        )
    }
}