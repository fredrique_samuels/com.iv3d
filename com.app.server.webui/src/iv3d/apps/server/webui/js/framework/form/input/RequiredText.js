import React from 'react'

export default class RequiredText extends React.Component {
    render() {
        if(this.props.viewParams.required)
            return <span><label class="form-required">*</label></span>
        return null
    }
}