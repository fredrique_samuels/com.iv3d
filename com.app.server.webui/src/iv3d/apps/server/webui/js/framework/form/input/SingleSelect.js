import React from 'react'
import OptionsSelect from './OptionsSelect'
import BoxOptionCompFactory from './BoxOptionCompFactory'

export default class SingleSelect extends React.Component {
    render() {
        const viewParams = Object.assign({}, this.props.viewParams, {optionsType:"radio", optionCompFactory:new BoxOptionCompFactory()})
        return <OptionsSelect viewParams={viewParams} />
    }
}