import Immutable from 'immutable'
import News24Service from './News24Service'

export default class ServiceManager {
  constructor() {
      this.services = Immutable.List.of(
        new News24Service()
      )
  }

  startAll() {
      this.services.forEach((s) => {s.start()})
  }

  stopAll() {

  }
}
