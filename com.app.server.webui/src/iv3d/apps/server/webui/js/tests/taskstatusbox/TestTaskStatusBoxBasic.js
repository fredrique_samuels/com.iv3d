import React from 'react'
import Immutable from 'immutable'
import { connect } from 'react-redux'


export default class TestTaskStatusBoxBasic {

  run(uicontext) {
      const data = {
              "id": -1,
              "doe": {
                  "millisecondsSinceEpoc": 1497360765581,
                      "year": 2017,
                      "month": 6,
                      "hour": 13,
                      "minute": 32,
                      "second": 45,
                      "day": 13
              },
              "parentId": -1,
              "startDate": {
                  "millisecondsSinceEpoc": 1497360765581,
                      "year": 2017,
                      "month": 6,
                      "hour": 13,
                      "minute": 32,
                      "second": 45,
                      "day": 13
              },
              "durationInMs": 0,
              "errorMessage": "",
              "errorTrace": "",
              "error": false,
              "type": "TASK",
              "children": [],
              "attributes": [
                  {
                      "name": "taskId",
                      "value": "{\"taskBeanId\":\"iv3d.apps.server.operations.demojob\",\"runId\":5,\"jobId\":2}"
                  },
                  {
                      "name": "taskStatus",
                      "value": "RUNNING"
                  }
              ]
           }

      uicontext.statusBoxManager()
          .createLayeredTaskStatusBox(data)
  }

}
