'use strict'

import React from 'react'
import ReactDom from 'react-dom'
import store from './../store'

import { Provider } from 'react-redux'

import TestAll from './TestAll'

const app = document.getElementById('app')
ReactDom.render(
    <Provider store={store}>
        <TestAllWidgets />
    </Provider>,
    app)
