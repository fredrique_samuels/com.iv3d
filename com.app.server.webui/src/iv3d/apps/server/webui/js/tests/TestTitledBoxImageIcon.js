import React from 'react'
import { connect } from 'react-redux'

import UiContext from '../uicontext'
import LayerComponent from '../framework/LayerComponent'
import TitledBoxComponent from '../framework/TitledBoxComponent'
import Dispatcher from './../common'

import {TitledBoxManager} from '../framework/TitledBoxComponent'

class DemoContentHandler {
    createContent(layerModel) {
        return <TitledBoxComponent params={layerModel.contentParams.titledBoxParams} layerModel={layerModel} />
    }
}

export class TestTitledBoxImageIcon {
  run(uicontext) {

     const titledBoxParams = {
          title:"Hello",
          icon:{
            type:'icon.type.image',
            graphic:"http://vignette3.wikia.nocookie.net/yugioh/images/3/3b/MysticalSpaceTyphoon-LDK2-EN-C-1E.png/revision/latest?cb=20161007084018"
          },
          content:<div class="has-text"><p>This is a value</p></div>
        }

      uicontext.titledBoxManager().createLayeredTitledBox(titledBoxParams)
  }
}
