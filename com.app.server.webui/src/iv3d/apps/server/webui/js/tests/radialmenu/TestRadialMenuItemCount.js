import React from 'react'
import { connect } from 'react-redux'
import Icon from "../../framework/Icon";
import {ActionConfig} from "../../framework/ActionConfig";

export default class TestRadialMenuComponentCount {

   constructor(count = 1) {
       this.count = count
   }

  run(uicontext) {
      uicontext.radialMenuManager().createLayeredRadialMenu(this.generateItems(uicontext))
  }

  generateItems(uicontext) {
      const items = []
      items.push(this.createItem(uicontext, "fa-share", null, 'Share'))

      if(this.count>1) items.push(this.createItem(uicontext, "fa-facebook", null, "Facebook"))
      if(this.count>2) items.push(this.createItem(uicontext, "fa-lock", null, "Security"))
      if(this.count>3) items.push(this.createItem(uicontext, "fa-user", null, "User"))
      if(this.count>4) items.push(this.createItem(uicontext, "fa-skype", null, "Skype"))
      if(this.count>5) items.push(this.createItem(uicontext, "fa-twitter", null, "Twitter"  ))

      return {items:items}
  }

  createItem(uicontext, faIcon, action, label) {
      const icon = Icon.createForFontAwesome(faIcon)
      return new ActionConfig({icon, action:action, label, style:{}})
  }
}