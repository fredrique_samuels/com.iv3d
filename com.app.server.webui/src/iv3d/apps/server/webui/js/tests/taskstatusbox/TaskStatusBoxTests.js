import TestTaskStatusBoxBasic from './TestTaskStatusBoxBasic'

export default class TaskStatusBoxTests {
    get(menu) {
        return [
            menu.createHeading("Status Box"),
            menu.createTestButton("Task Status",function(){ new TestTaskStatusBoxBasic().run(menu.getUiContext())}.bind(menu))
        ]
    }
}