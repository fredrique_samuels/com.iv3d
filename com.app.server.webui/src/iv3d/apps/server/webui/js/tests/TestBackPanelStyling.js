import React from 'react'
import { connect } from 'react-redux'

import UiContext from '../uicontext'
import LayerComponent from '../framework/LayerComponent'
import Dispatcher from './../common'

class DemoContent extends React.Component {
    render() {
        const style={
            position:"relative",
            top:"25%",
            left:"25%",
            width:"50%",
            height:"50%"
        }

        return (<div class="back-panel" style={style}>
        </div>)
    }
}

class DemoContentHandler {
    createContent(layerModel) {
        return <DemoContent layerModel={layerModel} />
    }
}


export class TestBackPanelStyling {
  run(uicontext) {
      var contentParams = {
          contentHandlerFactory: new DemoContentHandler(),
          layoutParams: {
              position:"relative",
              top:"0%",
              left:"0%",
              width:"100%",
              height:"100%"
          }
      };
      uicontext.layerComponentManager().addLayer(contentParams)
  }
}
