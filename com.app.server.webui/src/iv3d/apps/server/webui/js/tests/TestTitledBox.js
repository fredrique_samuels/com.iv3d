import React from 'react'
import { connect } from 'react-redux'

export class TestTitledBox {
  run(uicontext) {

     const titledBoxParams = {
          title:"Helo",
          content:<div class="has-text"><p>This is a value</p></div>
        }

      uicontext.titledBoxManager().createLayeredTitledBox(titledBoxParams)
  }
}
