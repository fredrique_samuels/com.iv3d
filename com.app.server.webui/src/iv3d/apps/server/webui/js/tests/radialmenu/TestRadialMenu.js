import React from 'react'
import { connect } from 'react-redux'

import Icon from '../../framework/Icon'
import { ActionConfig } from "../../framework/ActionConfig";


export default class TestRadialMenuComponent {

  run(uicontext) {
      uicontext.radialMenuManager().createLayeredRadialMenu(this.generateItems(uicontext))
  }

  generateItems(uicontext) {
    const items = []
    items.push(this.createItem(uicontext, "fa-share", null, "Share"))
    items.push(this.createItem(uicontext, "fa-facebook", null, "Facebook"))
    items.push(this.createItem(uicontext, "fa-lock", null, "Security"))
    items.push(this.createItem(uicontext, "fa-user", null, "User"))
    items.push(this.createItem(uicontext, "fa-skype", null, "Skype"))

    items.push(this.createItem(uicontext, "fa-twitter", null, "Twitter"))
    items.push(this.createItem(uicontext, "fa-linkedin", null, "Linkedin"))
    items.push(this.createItem(uicontext, "fa-envelope-open-o", null, "Mail"))
    items.push(this.createItem(uicontext, "fa-clock-o", null, "Clock"))
    items.push(this.createItem(uicontext, "fa-share", null, "Share"))

    items.push(this.createItem(uicontext, "fa-facebook", null, "Facebook"))
    items.push(this.createItem(uicontext, "fa-lock", null, "Security"))
    items.push(this.createItem(uicontext, "fa-user", null, "User"))
    items.push(this.createItem(uicontext, "fa-skype", null, "Skype"))
    items.push(this.createItem(uicontext, "fa-twitter", null, "Twitter"))

    items.push(this.createItem(uicontext, "fa-linkedin", null, "Linkedin"))
    items.push(this.createItem(uicontext, "fa-envelope-open-o", null, "Mail"))
    items.push(this.createItem(uicontext, "fa-clock-o", null, "Clock"))

    return {items:items}
  }

    createItem(uicontext, faIcon, action, label) {
        const icon = Icon.createForFontAwesome(faIcon)
        return new ActionConfig({icon, action:action, label, style:{}})
    }
}
