import React from 'react'
import { connect } from 'react-redux'

import TitledBoxComponent from '../framework/TitledBoxComponent'

class DemoContentHandler {
    createContent(layerModel) {
        return <TitledBoxComponent params={layerModel.contentParams.titledBoxParams} layerModel={layerModel} />
    }
}

export class TestTitledBoxFaIcon {
  run(uicontext) {

     const titledBoxParams = {
          title:"Hello",
          icon:{
            type:'icon.type.fontawesome',
            graphic:'fa-clock-o'
          },
          content:<div class="has-text"><p>This is a value</p></div>
        }

      uicontext.titledBoxManager().createLayeredTitledBox(titledBoxParams)
  }
}
