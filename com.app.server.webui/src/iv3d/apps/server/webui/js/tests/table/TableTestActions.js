import React from 'react'
import TableTestData from './TableTestData'
import ActionConfigBuilder from "../../framework/ActionConfig";
import Icon from "../../framework/Icon";

export class TableTestActions {
    run(uicontext) {
        uicontext.tableManager().createLayeredTable(this.createDemoTableViewParams())
    }

    createDemoTableViewParams() {
        const testData = new TableTestData();
        const data = testData.columnActionDataRHA()
        const metaData = testData.singlePageMetaData()

        return Object.assign( {}, data, metaData )
    }

}
