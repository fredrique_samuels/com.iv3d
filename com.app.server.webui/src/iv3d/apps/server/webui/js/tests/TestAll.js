import React from 'react'
import { connect } from 'react-redux'

import UiContext from '../uicontext'
import LayerComponent from '../framework/LayerComponent'
import Dispatcher from './../common'

import { TestLayerComponent } from './TestLayers'
import { TestFormBasic } from './TestForms'
import { TestFormSubmit } from './TestFormValidation'

import TestRadialMenuComponent from './radialmenu/TestRadialMenu'
import TestRadialMenuComponentCount from "./radialmenu/TestRadialMenuItemCount";
import RadialMenuTests from './radialmenu/RadialMenuTests'
import LoadingDialogsTests from './loading/LoadingDialogsTests'
import TaskStatusBoxTests from './taskstatusbox/TaskStatusBoxTests'

import { TestBackPanelStyling } from './TestBackPanelStyling'
import { TestTitledBox } from './TestTitledBox'
import { TestTitledBoxFaIcon } from './TestTitledBoxFaIcon'
import { TestTitledBoxImageIcon } from './TestTitledBoxImageIcon'
import { TestTableBasic } from "./table/TestTableBasic";
import { TestInfoPopup } from "./form/TestInfoPopup"
import {TestTableColumnScroll} from "./table/TestTableColumnScroll";
import {TestTablePaging} from "./table/TestTablePaging";
import {TableTestActions} from "./table/TableTestActions";
import ItemListTests from "./itemlist/ItemListTests";
const LAYER_MANAGER_ID = "test.layer.manager"



class DemosMenu extends React.Component {
    render() {
        const style={
            position:"relative",
            overflow:"auto",
            padding:"10px"
        }

        return (<div class="back-panel fill-parent" style={style}>
            {this.createHeading("Layer Tests")}
            {this.createTestButton("Test Layers", this.runLayersDemo.bind(this))}

            {this.createHeading("Form Tests")}
            {this.createTestButton("Form Basic", this.runFormBasicDemo.bind(this))}
            {this.createTestButton("Form Submit", this.runFormSubmitDemo.bind(this))}
            {this.createTestButton("Info Popup", this.runInfoPopupDemo.bind(this))}

            {this.createHeading("Radial Menu")}
            {this.createTestButton("Radial Menu More",function(){ new TestRadialMenuComponent().run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Radial Menu 1",function(){ new TestRadialMenuComponentCount(1).run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Radial Menu 2",function(){ new TestRadialMenuComponentCount(2).run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Radial Menu 3",function(){ new TestRadialMenuComponentCount(3).run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Radial Menu 4",function(){ new TestRadialMenuComponentCount(4).run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Radial Menu 5",function(){ new TestRadialMenuComponentCount(5).run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Radial Menu 6",function(){ new TestRadialMenuComponentCount(6).run(this.getUiContext())}.bind(this))}

            {new RadialMenuTests().get(this)}
            {new LoadingDialogsTests().get(this)}
            {new TaskStatusBoxTests().get(this)}
            {new ItemListTests().get(this)}

            {this.createHeading("Default Styling")}
            {this.createTestButton("Back Panel", this.runBackPanelStylingDemo.bind(this))}

            {this.createHeading("Titled Box")}
            {this.createTestButton("Titled Box ", this.runTestTitledBox.bind(this))}
            {this.createTestButton("Titled Box Fa Icon", this.runTestTitledBoxFaIcon.bind(this))}
            {this.createTestButton("Titled Box Image Icon", this.runTestTitledBoxImageIcon.bind(this))}

            {this.createHeading("Tables")}
            {this.createTestButton("Table Types",function(){new TestTableBasic().run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Table Columns",function(){new TestTableColumnScroll().run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Table Paging", function(){new TestTablePaging().run(this.getUiContext())}.bind(this))}
            {this.createTestButton("Table Actions", function(){new TableTestActions().run(this.getUiContext())}.bind(this))}
        </div>)
    }

    runLayersDemo() { new TestLayerComponent().run(this.getUiContext())}
    runFormBasicDemo() {new TestFormBasic().run(this.getUiContext())}
    runInfoPopupDemo(){ new TestInfoPopup().run(this.getUiContext())}
    runFormSubmitDemo() {new TestFormSubmit().run(this.getUiContext())}
    runRadialMenuDemo(event) {
      new TestRadialMenuComponent().run(this.getUiContext())
    }

    runBackPanelStylingDemo() {new TestBackPanelStyling().run(this.getUiContext())}
    runTestTitledBox() {new TestTitledBox().run(this.getUiContext())}
    runTestTitledBoxFaIcon() {new TestTitledBoxFaIcon().run(this.getUiContext())}
    runTestTitledBoxImageIcon() {new TestTitledBoxImageIcon().run(this.getUiContext())}

    getUiContext(){
      const {layerModel} = this.props
      const { uicontext } = layerModel.contentParams
      return uicontext
    }

    createTestButton(text, onClick) {
      return <div style={{margin:"3px"}} class="inline-element"><button  class="btn btn-primary" onClick={onClick}>{text}</button></div>
    }
    createHeading(text) {
      return <h2 style={{color:"grey"}}>{text}</h2>
    }
}

class DemoContentHandler {
    createContent(layerModel) {
        return <DemosMenu layerModel={layerModel} />
    }
}

@connect((store) => {
    return {
        environment : store.environment,
        componentModels : store.componentModels,
    }
})
export default class TestAllWidgets extends React.Component {

    constructor() {
        super()
        this.uicontext = new UiContext();
    }

    componentWillMount() {
        Dispatcher.setGlobal(this.props.dispatch)
    }

    render() {
        return (
          <div class="back-panel fill-parent">
            <LayerComponent modelId={LAYER_MANAGER_ID} mountedCallback={this.onLayerMounted.bind(this)}/>
          </div>)
    }

    onLayerMounted(id) {
        console.log(id+" mounted successfully")
        this.uicontext = this.uicontext.setProperties({layer_component_id:id})

        {
            var contentParams = {
                contentHandlerFactory: new DemoContentHandler()
            };
            this.uicontext.layerComponentManager().addLayer(contentParams, {canBeCleared:false})
        }
    }
}
