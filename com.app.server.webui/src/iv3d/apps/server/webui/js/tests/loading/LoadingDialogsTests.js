import TestSpinLoader from './TestSpinLoader'
import TestProgressLoader from './TestProgressLoader'
import TestProgressLoaderFail from './TestProgressLoaderFail'

export default class LoadingDialogsTests {
    get(menu) {
        return [
            menu.createHeading("Loading Dialogs"),
            menu.createTestButton("Spin Loader (2.5s)",function(){ new TestSpinLoader().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Progress Loader",function(){ new TestProgressLoader().run(menu.getUiContext())}.bind(menu)),
            menu.createTestButton("Progress Loader Fail",function(){ new TestProgressLoaderFail().run(menu.getUiContext())}.bind(menu))
        ]
    }
}