import React from 'react'
import AbstractParamsBuilder from "../../framework/AbstractParamsBuilder";
import ViewModel from "../../framework/ViewModel";

class ItemListViewModel extends ViewModel {

    constructor(parent) {
        super(parent);
    }

    heading(){return "Heading"}

}

class CardOptionView extends React.Component  {
    render() {
        const { model } = this.props

        const itemPane = {
            width:"100px",
            minHeight:"100px",
            margin:"2px",
            display:"inline-block",
            verticalAlign:"top",
            overflow:"hidden"
        }
        const itemContent = {
            width:"100px",
            height:"100px",
            overflow:"hidden",
            border:"1px solid slategrey",
            backgroundColor:"rgba(0,0,0,.4)",
            position:"relative"
        }
        const itemLabel = {
            width:"100%",
            color:"white",
            fontSize:"10px",
            textAlign:"center",
            backgroundColor:"rgba(0,0,0,.7)"
        }

        const contentLayer = {
            width:"100%",
            height:"100%",
            position:"absolute",
            color:"slategrey",
            top:"0px"
        }


        const statusColorBar = {
            width:"100%",
            height:"30%",
            position:"relative",
            top:"70%",
            opacity:".5"
        }

        return (
            <div class="item-pane" style={itemPane}>
                <div class="item-content" style={itemContent}>
                    <div class="center-content-vh" style={Object.assign({}, contentLayer, {zIndex:0})}>
                        <i class="fa fa-file fa-4x"></i>
                    </div>
                    <div class="center-content-vh" style={Object.assign({}, contentLayer, {zIndex:1})}>
                        <div class="color-error" style={statusColorBar}>
                        </div>
                    </div>
                </div>
                <div class="item-label" style={itemLabel}>Hello</div>
            </div>
        )
    }
}

class CardOptionsListView extends React.Component {
    componentWillMount() {
        new ItemListViewModel(this)
            .init(this.props.model)
    }

    render() {

        const viewModel = new ItemListViewModel(this);

        const itemCntr = {
            borderRadius:"3px 0px 0px 3px",
            overflowY:"auto",
            paddingTop:"30px"
        }

        const panelHeading = {
            position:"absolute",
            top:"0",
            left:"0",
            width:"100%",
            textAlign:"center",
            backgroundColor:"rgba(0,0,0,0.7)",
            color:"#eeeeee"
        }

        return (<div class="fill-parent item-cntr" style={itemCntr}>
            <div class="heading-pane" style={panelHeading}>{viewModel.heading()}</div>
            <CardOptionView />
        </div>)
    }
}

class CardOptionsViewBuilder extends AbstractParamsBuilder {

    constructor() {
        super({
            options: [],
            itemViewFactory:{render: (data) => <div class="fill-parent"></div>},
        }, null)
    }

    buildView(uicontext) {
        const params = this.build();
        return null
    }
    buildToLayer(uicontext) {

    }
}

class TestItemList {
    run(uicontext) {

        var contentParams = {
            contentHandlerFactory: {createContent:(layerModel) => <CardOptionsListView layerModel={layerModel} />},
            layoutParams: {
                position:"relative",
                width:"90%",
                height:"90%",
                backgroundColor:"rgba(0,0,0,.7)"
            },
            itemListViewParams:{

            }
        };
        uicontext.layerComponentManager().addLayer(contentParams)
    }
}


export default class ItemListTests {
    get(menu) {
        return [
            menu.createHeading("Items Lists"),
            menu.createTestButton("Radial Menu More",function(){ new TestItemList().run(menu.getUiContext())}.bind(menu))
        ]
    }
}