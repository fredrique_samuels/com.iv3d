import React from 'react'
import { connect } from 'react-redux'
import LoadingDialogCloser from "../../framework/form/loading/LoadingDialogCloser";
import {Timeout} from "../../common";


class ProgressUpdater {
    constructor(model, controller) {
        this.model = model
        this.controller = controller
    }

    update() {
        this.controller.update(this.model)
    }
}

class DemoLoadingController {

    constructor(){
        this.counter = 1
        this.canceled = false
    }

    start(loadingModel) {
        // const loadingDialogCloser = new LoadingDialogCloser(loadingModel);
        // new Timer(2500, loadingDialogCloser.close.bind(loadingDialogCloser))

        loadingModel.update()
            .setText("Running Demo...")
            .setProgress(10.0)
            .run()

        this.startUpdateTimer(loadingModel);
    }

    startUpdateTimer(loadingModel) {
        const progressUpdater = new ProgressUpdater(loadingModel, this);
        new Timeout(500, progressUpdater.update.bind(progressUpdater))
    }

    update(loadingModel) {
        if(this.canceled)return
        this.counter = this.counter + 1

        if(this.counter>=10) {
            loadingModel.update()
                .setText("Loading Failed")
                .setProgress(100.0)
                .setError()
                .setDone()
                .run()
        } else {
            loadingModel.update()
                .setProgress(this.counter*10.0)
                .run()
            this.startUpdateTimer(loadingModel);
        }
    }

    stop() {
        this.canceled = true
    }
}


export default class TestProgressLoaderFail {

  run(uicontext) {
      uicontext.formComponentManager().createLayeredProgressLoader(new DemoLoadingController())
  }

}
