export function fetchTodos() {
  return function(dispatch) {
    const result = {
      type: "FETCH_TODOS_FULFILLED",
      payload: [{ id: 23448035435, value: "Todo1"}]
    }
    dispatch(result)
  }
}

export function createTodo(value) {
  return {
    type:"CREATE_TODO",
    payload: value
  }
}
