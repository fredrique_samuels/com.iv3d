

export const UPDATE_CATEGORIES = "WORLDMAPEVENTS_WIDGET_UPDATE_CATEGORIES"
export function updateWidgetEvents(widgetId, items) {
  return {
    type :UPDATE_CATEGORIES,
    payload : {
      widgetId : widgetId,
      items : items
    }
  }
}
