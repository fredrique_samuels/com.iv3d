import worldMapSvgJson from './data/worldmapSvg'
import worldMapBBJson from './data/worldmapBB'


class WorldSvgData {
  constructor () {
    this.__bb = worldMapBBJson;
    this.__svgPaths = worldMapSvgJson.body
  }

  getBoundingBox() { return this.__bb }
  getSvgPaths() { return this.__svgPaths }
}

export default class WorldMapSvgDao {
  getWorldSvg() {
      return new WorldSvgData()
  }
}
