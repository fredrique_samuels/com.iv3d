
import AppLayer from './app/AppLayer'
import DialogWindow from "./dialog/DialogWindow"
import Immutable from 'immutable'
import MainMenu from './MainMenu'
import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import Transition from 'react-inline-transition-group'

import * as ComponentModels from "./ComponentModels"

import { connect } from 'react-redux'
import { FadeInAndOutTransition } from './UiComponents'



class LayerIdGenerator {
  constructor() {
    this.seed = 0
  }

  generate() {
    return "application_layer_"  + (++this.seed)
  }
}

const layerIdGenerator = new LayerIdGenerator()

class ApplicationContentComponentModelFactory {
  build() {

    const model = {
      layers : Immutable.OrderedMap()
    }

    return model
  }
}

function setAppLayerBiz(oldModel, params) {
  const {layerId, layoutParams, content, contentParams, layerParams} = params
  const oldLayers = oldModel.layers
  const n = {
      layoutParams : layoutParams,
      content : content,
      layerId : layerId,
      contentParams : contentParams,
      layerParams : layerParams,
    }

  const newState = Object.assign({}, oldModel, {layers: oldLayers.set(layerId,n)})
  return newState
}

function removeAppLayerBiz(oldModel, params) {
  const {layerId} = params
  const oldLayers = oldModel.layers
  return Object.assign({}, oldModel, {layers: oldLayers.delete(layerId)})
}

export const DEFAULT_APP_CONTENT_COMPONENT = "default-application-content-component"

export const DISPLAY_MODE_HOME = "display.mode.home"
export const DISPLAY_MODE_MENU = "display.mode.menu"
export const DISPLAY_MODE_CONTENT = "display.mode.content"

/**
Creates a fixed layout and content.
**/
export const FUNCTION_MODE_DESKTOP = "function.mode.desktop"

/**
Allows user configured actions on content.

params : {
  actions : [{
    title : Example -> <i class="fa fa-home"></i>home
    action : Js callback function
  },...]
}
*/
export const FUNCTION_MODE_CUSTOM = "function.mode.custom"


/**
Component inputs:
  params: {
    displayMode [optional, defaults to DISPLAY_MODE_HOME]: DISPLAY_MODE_HOME,DISPLAY_MODE_MENU or DISPLAY_MODE_CONTENT
    functionMode [optional, defaults to FUNCTION_MODE_DESKTOP]: FUNCTION_MODE_DESKTOP or FUNCTION_MODE_CUSTOM
  }
*/
@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
class ApplicationContentComponent extends React.Component {
  constructor() {
    super()
    this.cmg = new ComponentModels.ComponentModelGateway({
      modelFactory:new ApplicationContentComponentModelFactory()
    })
  }

  componentWillMount() {
    this.cmg.createModel(this)
    this.state = {displayMode:this.getDefaultDisplayMode(), functionMode:this.getDefaultFunctionMode()}
  }

  componentDidMount() {
    const {mountedCallback} = this.props
    if(mountedCallback) mountedCallback(this.cmg.getModelId())
  }

  componentWillUnmount() {
    this.cmg.deleteModel()
  }

  render() {
    const model = this.cmg.getModel(this)
    if(!model) return null
    let contentComp = this.createContentComponent(model)
    return (
      <div class="fill-parent-absolute applicationContentComponent layer-root">
          {this.state.functionMode==FUNCTION_MODE_CUSTOM?this.getCustomModeComp(contentComp):this.getDesktopModeComp(contentComp)}
      </div>
    )
  }

  getDesktopModeComp(contentComp) {
    return (
      <div class="fill-parent-absolute">
        <div style={{backgroundColor:"rgba(0,0,0,0)", display:"inline-block", overflow:"hidden", width:"50px",position:"relative", height:"100%"}}>
          <button onClick={this.displayHome.bind(this)} class={"btn btn-"+(this.shouldDisplayHome()?"primary":"default")+" btn-lg"} style={{width:"50px",height:"50px",display:'inline', marginTop:"30px", borderBottomRightRadius:"0px", borderBottomLeftRadius:"0px"}}><i class="fa fa-home"></i></button>
          <button onClick={this.displayMenu.bind(this)} class={"btn btn-"+(this.shouldDisplayMenu()?"primary":"default")+" btn-lg"} style={{width:"50px",height:"50px",display:'inline',borderRadius:"0px"}}><i class="fa fa-th"></i></button>
          <button onClick={this.displayContent.bind(this)} class={"btn btn-"+(this.shouldDisplayContent()?"primary":"default")+" btn-lg"} style={{width:"50px",height:"50px",display:'inline',borderTopRightRadius:"0px", borderTopLeftRadius:"0px"}}><i class="fa fa-list-alt"></i></button>
        </div>
        <div style={{display:"inline-block", overflow:"hidden", width:"calc(100% - 50px)",position:"relative", height:"100%"}}>
          {contentComp}
        </div>
      </div>)
  }

  getCustomModeComp(contentComp) {
      const baseButtonStyle = {
        width:"50px",
        height:"50px",
        display:'inline'
      }

      const topButtonStyle = Object.assign({}, baseButtonStyle, {
        marginTop:"30px",
        borderBottomRightRadius:"0px",
        borderBottomLeftRadius:"0px"
      })

      const centerButtonStyle = Object.assign({}, baseButtonStyle, {
        borderRadius:"0px"
      })

      const bottomButtonStyle = Object.assign({}, baseButtonStyle, {
        borderTopRightRadius:"0px",
        borderTopLeftRadius:"0px"
      })

      const singleButtonStyle = Object.assign({}, baseButtonStyle, {
        marginTop:"30px"
      })

      const {actions} = this.getParams()
      let buttonContainer=null
      let contentComponentContainerWidth = "100%"
      if(actions) {
        if(actions.length>0) {
          contentComponentContainerWidth = "calc(100% - 50px)"
          let buttons = null

          if(actions.length==1) {
            const a = actions[0]
            buttons = <button onClick={a.action} class="btn btn-default btn-lg" style={singleButtonStyle}>{a.title}</button>
          } else if(actions.length==2) {
            buttons = [
              <button onClick={actions[0].action} class="btn btn-default btn-lg" style={singleButtonStyle}>{actions[0].title}</button>,
              <button onClick={actions[1].action} class="btn btn-default btn-lg" style={singleButtonStyle}>{actions[1].title}</button>
            ]
          } else {
            const lastIndex = actions.length-1
            buttons = actions.map( a, i => {
              let style = centerButtonStyle
              if(i==0) {
                style = topButtonStyle
              } else if (i==lastIndex) {
                style = bottomButtonStyle
              }
              return <button onClick={a.action} class="btn btn-default btn-lg" style={style}>{a.title}</button>
            })
          }

          buttonContainer = (<div style={{backgroundColor:"rgba(0,0,0,0)", display:"inline-block", overflow:"hidden", width:"50px",position:"relative", height:"100%"}}>
            {buttons}
          </div>)
        }
      }

      return (<div class="fill-parent-absolute">
        {buttonContainer}
        <div style={{display:"inline-block", overflow:"hidden", width:contentComponentContainerWidth,position:"relative", height:"100%"}}>
          {contentComp}
        </div>
      </div>)
  }
  getParams() { return this.props.params?this.props.params:{} }
  getDefaultDisplayMode() {
    const {displayMode} = this.getParams()
    return displayMode?displayMode:DISPLAY_MODE_HOME
  }
  getDefaultFunctionMode() {
    const {functionMode} = this.getParams()
    return functionMode?functionMode:FUNCTION_MODE_DESKTOP
  }
  inCustomFunctionMode(){
    return this.getDefaultFunctionMode()==FUNCTION_MODE_CUSTOM
  }
  createContentComponent(model) {
    const {lastLayer, layers} = this.extractLayersFromModel(model)
    if(this.shouldDisplayContent() ||  this.inCustomFunctionMode()){
      const showClearButton = lastLayer?lastLayer.layerParams.canBeCleared:false
      const utilComp = (<div style={{height:"50px", width:"100%", top:"0px"}} >
            {showClearButton?this.createBackButton():null}
          </div>)
      // const layersComps = FadeInAndOutTransition.render(layers)
      return ( <div class="fill-parent-absolute">
            <div style={{width:"100%",position:"relative", height:"calc(100% - 50px)"}}>
              <div style={{position:'relative',width:'100%',height:"100%"}}>
                {layers}
              </div>
            </div>
            {utilComp}
        </div>
      )
    } else if(this.shouldDisplayMenu()) {
      return ( <div class="fill-parent-absolute">
        <MainMenu />
      </div> )
    }
  }
  getLayerTransition(base={}) {

    const appear = {
      transition: 'all 1000ms',
    }

    const leave = {
      transition: 'all 400ms',
      opacity:0,
    }

    const transitionStyles = {
      base,
      appear,
      leave,
    }

    return transitionStyles
  }

  clearLastLayer() {
      const model = this.cmg.getModel(this)
      const modelId = this.cmg.getModelId()

      let lastLayer = null
      model.layers.valueSeq().toList().map( arg => lastLayer=arg.layerId )
      ApplicationContentComponent.removeLayeredComponents(this.props.dispatch, modelId, lastLayer)
  }

  displayHome() {this.setState({displayMode:DISPLAY_MODE_HOME})}
  displayMenu() {this.setState({displayMode:DISPLAY_MODE_MENU})}
  displayContent() {this.setState({displayMode:DISPLAY_MODE_CONTENT})}
  shouldDisplayMenu() {return this.state.displayMode==DISPLAY_MODE_MENU}
  shouldDisplayHome() {return this.state.displayMode==DISPLAY_MODE_HOME}
  shouldDisplayContent() {return this.state.displayMode==DISPLAY_MODE_CONTENT}

  extractLayersFromModel(model) {
    var layerIndex = 4
    var i=0
    var layoutCount = model.layers.size
    var opacityForHiddenLayer = .2//layoutCount>1?.1/layoutCount:.1;
    let lastLayer = null
    const layers = model.layers.valueSeq().toList().map( arg =>
      {
        lastLayer = arg
        i+=1
        const v = {
          zIndex:(layerIndex++),
          opacity: i==model.layers.size?1:opacityForHiddenLayer,
          key: arg.layerId,
          layerId: arg.layerId,
        }
        const nv = Object.assign({},arg.layoutParams,v)
        const layerComp = AppLayer.createForItems(v, arg.contentParams)

        const base = {
          opacity:0,
        }

        const appear = {
          opacity:v.opacity,
          transition: 'all 1000ms',
        }

        const leave = {
          opacity:0,
          transition: 'all 1000ms',
        }

        const layerTransition = {
          base,
          appear,
          leave,
        }

        return (<Transition
            childrenBaseStyle={layerTransition.base}
            childrenAppearStyle={layerTransition.appear}
            childrenEnterStyle={layerTransition.appear}
            childrenLeaveStyle={layerTransition.leave}
          >
          {layerComp}
        </Transition>)

        // return layerComp
      }
    )

    return {layers, lastLayer}
  }

  createBackButton() {
    return (<div >
      <button id="back-button" onClick={this.clearLastLayer.bind(this)} class="btn btn-default btn-lg" style={{marginLeft:"calc(50% - 40px)", width:"80px",height:"50px", positon:"relative",display:"inline",marginRight:"50px"}}><i class="fa fa-reply"></i></button>
    </div>)
  }

  static createDefaultModel(dispatcher) {
    dispatcher(ComponentModels.createModel(DEFAULT_APP_CONTENT_COMPONENT,
      new ApplicationContentComponentModelFactory().build()))
  }

  static createModel(dispatcher, modelId) {
    dispatcher(ComponentModels.createModel(modelId,
      new ApplicationContentComponentModelFactory().build()))
  }

  static addLayeredComponents(dispatcher, modelId, contentParams, layerParams={canBeCleared:true}) {
    console.log("Adding Layer Component", dispatcher, modelId, contentParams, layerParams);
    const layerId = layerIdGenerator.generate()
    const bizParams = {
        layerComponentModelId: modelId,
        layerId: layerId,
        contentParams:contentParams,
        layerParams:layerParams,
      }

    ComponentModels.updateModel(dispatcher, modelId, setAppLayerBiz, bizParams)
    return layerId
  }

  static updateLayeredComponents(dispatcher, modelId, layerId, contentParams,layerParams) {
    const bizParams = {
        layerComponentModelId: modelId,
        layerId: layerId,
        contentParams:contentParams,
      }
    ComponentModels.updateModel(dispatcher, modelId, setAppLayerBiz, bizParams)
    return layerId
  }

  static removeLayeredComponents(dispatcher, modelId, layerId) {
    const bizParams = {
        layerId: layerId,
    }
    ComponentModels.updateModel(dispatcher, modelId, removeAppLayerBiz, bizParams)
    return layerId
  }
}

export default ApplicationContentComponent
