import Immutable from 'immutable'

let dataNodeStringTypeMap = new Immutable.Map()
export function mapDataNodeFactoryToStringType(type) {
  dataNodeStringTypeMap = dataNodeStringTypeMap.set(type.type(), type)
}
export function dataNodeFactoryFromTypeString(typeString) {
  return dataNodeStringTypeMap.get(typeString)
}

export const VIEW_DETAIL_HINT_FULL = "view_detail_full"
export const VIEW_DETAIL_HINT_PREVIEW = "view_detail_preview"
export const VIEW_DETAIL_HINT_MINIMIZED = "view_detail_minimized"
