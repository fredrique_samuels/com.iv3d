import Immutable from "immutable"
import * as UiConstants from './UiConstants'


/**
Data node factory.

All datanode params can contain the following value:
  title : A title for the node.
**/
export default class UiDataNodeFactory {

  /**
  Create an image node.

  url : Image url
  params [optional]:
    altText : alternate image text if url is not reachable
    caption : A captio ot go with the image
  */
  static createImage(url, params={}) {
    const node = {
      type: UiConstants.MIME_TYPE_IMAGE,
      url:url,
    }
    return Object.assign({},params,node)
  }

  /**
  Create an text node.

  text : the node text
  */
  static createText(text, params={}) {
      const node = {
        type: UiConstants.MIME_TYPE_TEXT,
        text:text,
      }
      return Object.assign({},params,node)
  }
}
