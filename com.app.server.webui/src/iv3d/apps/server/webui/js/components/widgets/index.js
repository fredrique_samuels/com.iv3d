
class WidgetIdGenerator {
  constructor() {
    this.seed = 0
  }

  generate() {
    return "widget_uid_" + (++this.seed)
  }
}

const widgetIdGenerator = WidgetIdGenerator()
export widgetIdGenerator


class WidgetModelCache {
  constructor(value = {}) {
    this.value = value
  }
}

const WidgetModelReducer = function (state=new WidgetModelCache(), action) {

}
