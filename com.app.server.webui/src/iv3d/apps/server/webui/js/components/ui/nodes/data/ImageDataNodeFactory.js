import React from 'react'
import DataNodeFactory from './../../DataNodeFactory'
import { CaptionedMediaDataNode } from './../../DataNodeFactory'

export default class ImageDataNodeFactory extends DataNodeFactory {
  type(){return 'datanode.type.image'}

  createDataNode(params={}) {
    const { url, altText, caption } = params
    return this.createNodeFromContent({url, altText, caption}, params)
  }

  createView(dataNode, params={}) {
    const {url, altText, caption}=dataNode.content
    const imgStyle = {width:'100%',
      height:'100%',
      objectFit:'contain'
    }
    const media = <img src={url} alt={altText} style={imgStyle}/>
    return CaptionedMediaDataNode.createCaptionedMediaView(media, caption)
  }

  createPreviewView(dataNode, params={}) {
    const {url, altText, caption}=dataNode.content
    const imgStyle = {width:'100%',
      height:'100%',
      objectFit:'contain'
    }
    const media = <img src={url} alt={altText} style={imgStyle}/>
    return CaptionedMediaDataNode.createCaptionedPreviewMediaView(media, dataNode.content.caption)
  }


}
