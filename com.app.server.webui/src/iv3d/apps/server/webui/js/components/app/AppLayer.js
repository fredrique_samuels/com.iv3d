
/* Core module imports */
import React from 'react'
import Immutable from 'immutable'

/* Local alias imports */
import * as Common from './../../api/Common'
import * as ComponentModels from "./../ComponentModels"

/* Local imports */
import AppLayerItem from './AppLayerItem'
import SingleItemLayerContentHandler from './SingleItemLayerContentHandler'
import TwoItemsLayerContentHandler from './TwoItemsLayerContentHandler'
import TreeToFiveItemsLayerContentHandler from './TreeToFiveItemsLayerContentHandler'
import MultiItemsLayerContentHandler from './MultiItemsLayerContentHandler'


class LayerContentHandlerFactory {

  getCompByItemCount(contentParams, layerId) {
    const {dataNodes, layerComponentModelId} = contentParams
    const args = {layerComponentModelId:layerComponentModelId, dataNodes:dataNodes,layerId:layerId}
    const itemCount = dataNodes.length
    if(itemCount==1)
      return <SingleItemLayerContentHandler args={args} />
    else if(itemCount==2)
      return <TwoItemsLayerContentHandler args={args} />
    else if(itemCount>=3 && itemCount<=5)
      return <TreeToFiveItemsLayerContentHandler args={args} />
    else if(itemCount>5)
        return <MultiItemsLayerContentHandler args={args} />
    return null
  }
}

class HandlerModelFactory {
  constructor(handler) {
    this.handler = handler
  }

  build() {
    return this.handler.createModel()
  }
}

class LayerItemManager extends React.Component {

  componentWillMount() {
    this.setContentHandler()
  }

  render () {
    return <div class="fill-parent-absolute" >{this.contentHandler}</div>
  }

  setContentHandler() {
    const {contentParams, layerId} = this.props;
    if(contentParams.contentHandlerFactory) {
      const args = {contentParams, layerId:layerId}
      this.contentHandler = contentParams.contentHandlerFactory.createContentHandler(args)
    } else {
      this.contentHandler = new LayerContentHandlerFactory().getCompByItemCount(contentParams, layerId);
    }
  }
}

class AppLayer extends React.Component {

  render() {
    const {userLayerParam, contentParams} = this.props;

    const defaultLayerParams = {
        opacity: 1,
        zIndex:3
      }

    const layerParams = Object.assign({}, defaultLayerParams, userLayerParam)

    return (<div class="fill-parent-absolute layer-root" style={layerParams} key={layerParams.layerId}>
      <LayerItemManager contentParams={contentParams} layerId={layerParams.layerId}/>
      </div>)
  }

  // Create a new layer div.
  // The layering is controlled by the zIndex param property.
  // Properties not specified defaults to predetermined values.
  //
  static createForItems(userLayerParam, contentParams) {
    return <AppLayer userLayerParam={userLayerParam} contentParams={contentParams}/>
  }
}

export default AppLayer
