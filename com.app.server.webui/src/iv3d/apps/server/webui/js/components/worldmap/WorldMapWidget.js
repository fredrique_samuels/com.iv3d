import $ from 'jquery'
import React from 'react'
import { connect } from 'react-redux'
import Immutable from 'immutable'

import * as Themes from '../../themes/Themes'
import DotOverlay from './DotOverlay'
import LatLng from './LatLng'
import Point2D from './Point2D'
import SphereMercatorCalculator from './SphereMercatorCalculator'

import  Transition from 'react-inline-transition-group'
import WorldMapSvgDao from './WorldMapSvgDao'

@connect((store) => {
  return {
    themes:store.themes,
  }
})
class WorldMapWidget extends React.Component {

  constructor() {
    super()
    this.svgDao = new WorldMapSvgDao()
  }

  getOverlays() {
    if(this.props.overlays) {
      return this.props.overlays
    }
    return []
  }

  render() {

    const { themes } = this.props
    const woldmapTheme = themes.get(Themes.WORLDMAP_WIDGET_THEME)

    const svgStyle = {
      backgroundColor: woldmapTheme.backgroundColor,
      opacity:woldmapTheme.opacity,
      fillOpacity:woldmapTheme.fillOpacity,
      width:"100%",
      height:"100%",
    }

    const pathStyle = {
      stroke:woldmapTheme.landStrokeColor,
      fill:woldmapTheme.landFillColor,
      // fill:"none",
    }

    const mapSvg = this.svgDao.getWorldSvg()
    const bb = mapSvg.getBoundingBox()
    const smc = new SphereMercatorCalculator(
      new LatLng(bb.topLatitude, bb.leftLongitude),
      new LatLng(bb.bottomLatitude, bb.rightLongitude),
      bb.width, bb.height
    )

    const mapSvgComps = mapSvg.getSvgPaths().map((svgJson) => {
      return <path key={svgJson.id} d={svgJson.svgPath} style={pathStyle}/>
    })

    const localOverlays = this.getOverlays()
    const localOverlaysComponents = localOverlays.map((overlay) => {
      const pingLoc = smc.latLngToPoint(overlay.getLatLng())
      return overlay.render(pingLoc)
    })

    const svgViewBox=`0 0 ${bb.maxX} ${bb.maxY}`
    const {widgetId, environment} = this.props

    return (
      <div class="fill-parent">
        <svg id="demo-svg" style={svgStyle} viewBox={svgViewBox}>
          {mapSvgComps}
          {localOverlaysComponents}
        </svg>
      </div>
    )
  }

}

export default WorldMapWidget
