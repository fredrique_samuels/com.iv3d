import React from 'react'
import DataNodeFactory from './../../DataNodeFactory'

export default class TextDataNodeFactory extends DataNodeFactory {
  type(){return 'datanode.type.text'}

  createDataNode(params={}) {
    const { text } = params
    return this.createNodeFromContent({text}, params)
  }

  createView(dataNode, params={}) {
    return (<p>{dataNode.content.text}</p>)
  }
}
