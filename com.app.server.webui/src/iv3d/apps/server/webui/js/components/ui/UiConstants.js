import React from 'react'
import Immutable from 'immutable'

import TextDataNodeFactory from './nodes/data/TextDataNodeFactory'
import ImageDataNodeFactory from './nodes/data/ImageDataNodeFactory'
import VideoYoutubeDataNodeFactory from './nodes/data/VideoYoutubeDataNodeFactory'
import TwitterTweetDataNodeFactory from './nodes/data/TwitterTweetDataNodeFactory'

import * as UiCommon from './UiCommon'

/*
A Data Node consist of a set of properties that will
be used to display it to the screen.

Nodes constist of,
  type : A string representing the type of data.
  content : The actual data content. This will be specific to every data type.
  params : A set of optionl parameters wrapped in a js object.
        -  title : An string used to title the data.
*/

/*
type : datanode.type.text
content.text : The text value for the node.
*/
export const DATA_NODE_TYPE_TEXT = new TextDataNodeFactory()
UiCommon.mapDataNodeFactoryToStringType(DATA_NODE_TYPE_TEXT)

/*
type : datanode.type.image
content.url : The image url.
content.altText (optional): The text to display if the url is not resolved.
content.caption (optional): Text to be displayed with the image.
*/
export const DATA_NODE_TYPE_IMAGE = new ImageDataNodeFactory()
UiCommon.mapDataNodeFactoryToStringType(DATA_NODE_TYPE_IMAGE)

/*
type : datanode.type.video.youtube
content.videoId : The video id. Example XGSy3_Czz8k
*/
export const DATA_NODE_TYPE_VIDEO_YOUTUBE = new VideoYoutubeDataNodeFactory()
UiCommon.mapDataNodeFactoryToStringType(DATA_NODE_TYPE_VIDEO_YOUTUBE)

/*
type : datanode.type.twitter.tweet
content.url : The tweet url. example : https://twitter.com/Interior/status/463440424141459456
*/
export const DATA_NODE_TYPE_TWITTER_TWEET = new TwitterTweetDataNodeFactory()
UiCommon.mapDataNodeFactoryToStringType(DATA_NODE_TYPE_TWITTER_TWEET)


export default class NodeTypeNotSupportedView extends React.Component {
  render() {
    return <div class="fill-parent-absolute"></div>
  }
}
