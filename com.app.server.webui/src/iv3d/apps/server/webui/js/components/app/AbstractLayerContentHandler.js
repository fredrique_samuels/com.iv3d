import AppLayerItem from './AppLayerItem'
import DataNodeFactory from './../ui/DataNodeFactory'
import DialogWindow from './../dialog/DialogWindow'
import React from 'react'

import { LayoutProvider } from './Layout'

import * as UiConstants from './../ui/UiConstants'
import * as UiCommon from './../ui/UiCommon'


export default class AbstractLayerContentHandler {

  static createItemsFromDataNodes(args, itemLayout, options={}) {
    const { layerId, dataNodes, layerComponentModelId } = args
    const layoutProvider = new LayoutProvider(itemLayout)
    const defaultItemParams = {
        width:"60%",
        height:"50%",
        top: "25%",
        left: "20%",
        viewDetailHint:UiCommon.VIEW_DETAIL_HINT_FULL,
      }

    const { filterDataNodes } = options

    return dataNodes
        .filter((dn,index)=> {
          if(filterDataNodes)
            return filterDataNodes(dn,index)
          return true
        })
        .map(dn => {
          const layoutParams = layoutProvider.next(dn).layout
          if(layoutParams) {
            return AbstractLayerContentHandler.createLayerItemForDataNode(layerComponentModelId, dn, layerId, layoutParams)
          }
        })
        .filter(item => {
          return item!== undefined && item!== null
        })
        .map(p => {
            let params = Object.assign({}, defaultItemParams, p.layoutParams)
            return (<AppLayerItem params={params} key={p.id} >
                {p.content}
            </AppLayerItem>)
        })
  }

  static createLayerItemForDataNode(layerComponentModelId, dn, layerId, layoutParams) {
    const dialogContent = AbstractLayerContentHandler.createDialogContentFromDataNode(layerComponentModelId, dn, layoutParams)
    const dialog = DialogWindow.create(dialogContent)
    return {
      id:layerId+"layerItem"+dn.id,
      content:dialog,
      layoutParams:layoutParams,
    }
  }

  static createDialogContentFromDataNode(layerComponentModelId, dn, layoutParams) {
    const {viewDetailHint} = layoutParams
    const  content = DataNodeFactory.createDataNodeView(dn, {viewDetailHint})

    return {
      id:"dialog"+dn.id,
      title:dn.title,
      content:content?content:dn.id,
      dataNode:dn,
      viewDetailHint:viewDetailHint,
      layerComponentModelId:layerComponentModelId,
    }
  }
}
