import React from 'react'
import Transition from 'react-inline-transition-group'

export class FadeInAndOut {
  static animate(components) {

    const base = {
      opacity:0,
    }

    const appear = {
      opacity:1.0,
      transition: 'all 1000ms',
    }

    const leave = {
      opacity:0,
      transition: 'all 1000ms',
    }

    const layerTransition = {
      base,
      appear,
      leave,
    }

    return (<Transition
        childrenBaseStyle={layerTransition.base}
        childrenAppearStyle={layerTransition.appear}
        childrenEnterStyle={layerTransition.appear}
        childrenLeaveStyle={layerTransition.leave}
      >
      {components}
    </Transition>)
  }
}
