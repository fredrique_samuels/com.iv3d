import { connect } from 'react-redux'
import * as UserActions from '../actions/userActions'
import * as TodoActions from '../actions/todoActions'
import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import  Transition from 'react-inline-transition-group'

@connect((store) => {
  console.log("Store header", store)
  return {
    user: store.user.user,
    todos : store.todos.todos,
  }
})
export default class Header extends React.Component {

  componentWillMount() {
    this.props.dispatch(UserActions.fetchUser())
  }

  createTodo() {
    this.props.dispatch(TodoActions.createTodo(Date.now()))
  }

  render() {
    console.log("Header user", this.props)
    const { todos } = this.props
    const todoComponents = todos.map((todo) => {
      return <li>{todo}</li>
    })

    var styles = {
        base: {
          // background: '#FFF',
          // borderRadius: '2px',
          // boxSizing: 'border-box',
          // height: '50px',
          // marginBottom: '5px',
          // padding: '10px',
          opacity:0,
        },

        appear: {
          // background: '#81C784',
          opacity:1,
          transition: 'all 1000ms',
        },

        leave: {
          // background: '#FFF',
          transition: 'all 500ms',
          opacity:0,
        },
      };

    return (
      <div>
        <p>{this.props.user.age}</p>
        <button onClick={this.createTodo.bind(this)}>Create!</button>
        <h1>Todos</h1>
        <ul>
        <Transition
          childrenBaseStyle={styles.base}
          childrenAppearStyle={styles.appear}
          childrenEnterStyle={styles.appear}
          childrenLeaveStyle={styles.leave}
        >
            {todoComponents}
        </Transition>
        </ul>
      </div>
    )
  }
}
