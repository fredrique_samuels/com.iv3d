import AbstractLayerContentHandler from './AbstractLayerContentHandler'
import FillParent from './../UiComponents'
import React from 'react'

import { FadeInAndOutTransition } from './../UiComponents'
import * as UiCommon from './../ui/UiCommon'

class MultiItemsLayoutArrow extends React.Component {
  render() {

    if(this.props.direction==='left') {
      return (
          <div key={this.props.key} style={Object.assign({}, {position:'absolute'}, this.props.layoutParams)}>
            <svg  width="100%" height="100%" viewBox="0 0 2048 2048">
              <path onClick={this.props.callback} d="M1536 1088v-128q0-26-19-45t-45-19h-502l189-189q19-19 19-45t-19-45l-91-91q-18-18-45-18t-45 18l-362 362-91 91q-18 18-18 45t18 45l91 91 362 362q18 18 45 18t45-18l91-91q18-18 18-45t-18-45l-189-189h502q26 0 45-19t19-45zm256-64q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z"/>
            </svg>
          </div>
      )
    }

    return (
      <div key={this.props.key} style={Object.assign({}, {position:'absolute'}, this.props.layoutParams)}>
        <svg width="100%" height="100%" viewBox="0 0 2048 2048" >
            <path onClick={this.props.callback}  d="M1541 1024q0-27-18-45l-91-91-362-362q-18-18-45-18t-45 18l-91 91q-18 18-18 45t18 45l189 189h-502q-26 0-45 19t-19 45v128q0 26 19 45t45 19h502l-189 189q-19 19-19 45t19 45l91 91q18 18 45 18t45-18l362-362 91-91q18-18 18-45zm251 0q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z"/>
        </svg>
      </div>
      )
  }
}

class ItemLayout {
  create(args) {
    if(args.index===0) {
      return { width:"47%", height:"90%", top: "5%", left: "2%",}
    } else if(args.index===1) {
      return { width:"15%", height:"30%", top: "5%", left: "50%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if(args.index===2) {
      return { width:"15%", height:"30%", top: "5%", left: "66%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if(args.index===3) {
      return { width:"15%", height:"30%", top: "5%", left: "82%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if(args.index===4) {
      return { width:"15%", height:"30%", top: "36%", left: "50%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW }
    } else if(args.index===5) {
      return { width:"15%", height:"30%", top: "36%", left: "66%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    } else if(args.index===6) {
      return { width:"15%", height:"30%", top: "36%", left: "82%", viewDetailHint:UiCommmon.VIEW_DETAIL_HINT_PREVIEW}
    }
    return null
  }
}

export default class MultiItemsLayerContentHandler extends React.Component {

  constructor() {
    super()
    this.state = {}
  }

  componentWillMount() {
    this.state = {
      currentPage:1,
      totalPages:this.calculateTotalPages()
    }
  }

  render() {
    const options = {filterDataNodes:this.filterDataNodes.bind(this)}
    const items = AbstractLayerContentHandler
        .createItemsFromDataNodes(this.props.args, new ItemLayout(), options)

    if(this.state.totalPages>1) {
      const { layerId } = this.props
      {
        const itemLeft =  <MultiItemsLayoutArrow direction="left" callback={this.scrollLeft.bind(this)} key={"multi_button_left_"+layerId} layoutParams={{ width:"15%", height:"30%", top: "67%", left: "57%", }} />
        items.push(itemLeft)
      }

      {
        const itemRight =  <MultiItemsLayoutArrow direction="right" callback={this.scrollRight.bind(this)} key={"multi_button_right_"+layerId} layoutParams={{width:"15%", height:"30%", top: "67%", left: "75%", }} />
        items.push(itemRight)
      }
    }

    return <FillParent>{items}</FillParent>
  }

  filterDataNodes(dataNode, index) {
    const {currentPage, totalPages} = this.state
    const basePageIndex = 6*(currentPage-1)
    const displayedIndexes = [0,
            basePageIndex+1,
            basePageIndex+2,
            basePageIndex+3,
            basePageIndex+4,
            basePageIndex+5,
            basePageIndex+6]
    return displayedIndexes.includes(index)
  }

  calculateTotalPages() {
    const { dataNodes } = this.props.args
    if(dataNodes.length<=7) return 1

    const paged = dataNodes.length-7
    const res = paged%6

    if((paged-res)==0) {
      return 2
    }

    return 2 + ((paged-res)/6)
  }

  scrollLeft(e) {
    const {currentPage, totalPages} = this.state
    const newCurrentPage = currentPage==1?totalPages:(totalPages==1?1:currentPage-1)
    this.setState(Object.assign({}, this.state, {dir:'left',currentPage:newCurrentPage}))
  }

  scrollRight(e) {
    const {currentPage, totalPages} = this.state
    const newCurrentPage = currentPage==totalPages?1:(totalPages==1?1:currentPage+1)
    this.setState(Object.assign({}, this.state, {dir:'right',currentPage:newCurrentPage}))
  }

}
