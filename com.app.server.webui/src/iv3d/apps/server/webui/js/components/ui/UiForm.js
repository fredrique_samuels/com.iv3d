import React from 'react'

import IdGenerator from '../../common.js'

/**
 * Form Data
 *
 *
 */
class UiForm extends React.Component {

    constructor() {
        this.formId=IdGenerator.generateGlobalId()
    }

    componentDidMount() {
        $('#'+ this.formId).submit(this.formSubmit.bind(this))
        // console.log($('#'+ this.formId).serializeArray())
        // $('#'+ this.formId).submit(function() {
        //     let theForm = $(this);
        //
        //     console.log($theForm.serialize())
        //     console.log($theForm.serializeArray())
        //
        //     // send xhr request
        //     // $.ajax({
        //     //     type: $theForm.attr('method'),
        //     //     url: $theForm.attr('action'),
        //     //     data: $theForm.serialize(),
        //     //     success: function(data) {
        //     //         console.log('Yay! Form sent.');
        //     //     }
        //     // });
        //
        //     // prevent submitting again
        //     return false;
        // });
    }

    render() {
        const containerStyle = {
            position:"relative",
            width:"400px",
            height:"500px",
            backgroundColor:"rgba(0,0,0,.7)",
            padding:"12px",
            top:"10%",
            left:"calc( (100% - 400px) / 2)" ,
            overflow:"auto"
        }
        const formContent = (<div style={containerStyle}>
            <div class="panel-text" style={{width:"100%"}}>
                <form>

                    <div class="form-section">
                        <h1 class="form-section-heading">Form Styling</h1>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Text Input</label>
                            </div>
                            <div class="form-input-container">
                                <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                                <p class="help-block">Example block-level help text here. hudif isadf isdf asfoasnfo sdfoas o fdsoaf dos</p>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Text Input</label>
                            </div>
                            <div class="form-input-container">

                                <p class="help-block error-block">Please enter a valid value!</p>
                                <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                                <p class="help-block">Example block-level help text here. hudif isadf isdf asfoasnfo sdfoas o fdsoaf dos</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Text Input</label> <i class=" fa fa-question-circle form-help"></i>
                            </div>
                            <div class="form-input-container">
                                <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Text Input</label>
                            </div>
                            <div class="form-input-container input-group">

                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input name="input1" placeholder="This is a placeholder" class="form-control"/>
                                <span class="input-group-addon">.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-section">
                        <h1 class="form-section-heading">Form Input Types</h1>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Text Area</label>
                            </div>
                            <div class="form-input-container">
                                <textarea class="form-control form-textarea" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Check boxes</label>
                            </div>
                            <div class="form-input-container">
                                <div><input value="" type="checkbox"/><label class="form-list-select-label">This is a label</label></div>
                                <div><input value="" type="checkbox"/><label class="form-list-select-label">This is a label</label></div>
                                <div><input value="" type="checkbox"/><label class="form-list-select-label">This is a label</label></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Radio</label>
                            </div>
                            <div class="form-input-container">
                                <div><input name="s" value="" type="radio"/><label class="form-list-select-label">This is a label</label></div>
                                <div><input name="s" value="" type="radio"/><label class="form-list-select-label">This is a label</label></div>
                                <div><input name="s" value="" type="radio"/><label class="form-list-select-label">This is a label</label></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">File</label>
                            </div>
                            <div class="form-input-container">
                                <div class="form-control-file-input-container">
                                    <input type="file" class="form-control"/>
                                </div>
                                <span class="file-input-left-corner"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Color</label>
                            </div>
                            <div class="form-input-container">
                                <input type="color" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Url</label>
                            </div>
                            <div class="form-input-container">
                                <input type="url" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Email</label>
                            </div>
                            <div class="form-input-container">
                                <input type="email" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Range</label>
                            </div>
                            <div class="form-input-container">
                                <label id="demo-range" >2</label>
                                <input type="range" class="form-control" name="points" min="0" max="100" value="2" onchange="updateRangeDisplay(this.value)"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-container">
                                <label class="form-label">Number</label>
                            </div>
                            <div class="form-input-container">
                                <input type="number" min="0" max="10" value="3" class="form-control"/>
                            </div>
                        </div>

                    </div>
                    <input  class="form-button btn btn-error btn-sm" type="submit" value="Cancel"/>
                    <input class="form-button btn btn-primary btn-sm" type="submit" value="Create Item"/>
                </form>
            </div>
        </div>)

        const panelStyleTextOnly = {
            padding:"10px",
            position:"relative",
            maxWidth:"100%",
            overflowX:"auto",
            overflowY:"auto",
            maxHeight:"100%",
        }

        const content =  (<div class="panel panel-default panel-text" style={panelStyleTextOnly}>
            <div style={{width:"100%",height:"41px"}}></div>
            {formContent}
        </div>)

        const dialogParams = {
            title:"This is a form",
            content:content,
        }
        return formContent
    }

    formSubmit() {
        let theForm = $('#'+ this.formId);

        console.log(theForm.serialize())
        console.log(theForm.serializeArray())

        // send xhr request
        // $.ajax({
        //     type: $theForm.attr('method'),
        //     url: $theForm.attr('action'),
        //     data: $theForm.serialize(),
        //     success: function(data) {
        //         console.log('Yay! Form sent.');
        //     }
        // });

        // prevent submitting again
        return false;
    }


    onTestClick(event) {
        event.preventDefault();
        console.log($('#'+ this.formId).serializeArray())
    }

    static createForm(formData) {
        return <UiForm />
    }
}