export default class LatLng {
  constructor(latitude, longitude) {
    this.latitude = latitude
    this.longitude = longitude
  }

  getLongitude(){return this.longitude}
  getLatitude(){return this.latitude}
}
