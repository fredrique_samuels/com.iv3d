
export default class AbstractLayout {
  constructor(){}
  createLayoutItems(layerId, layerParams){
    return [];
  }
}


export class LayoutProvider {
  constructor(factory) {
      this.index = 0
      this.factory = factory
  }

  next(dataNode) {
    const layout = this.factory.create({
      index:this.index,
      dataNode:dataNode,
    })
    const out = {
      layout:layout,
    }
    this.index=this.index+1
    return out
  }
}
