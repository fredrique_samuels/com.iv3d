import React from 'react'

export default class ProductCard extends React.Component {
  render() {
    return (<div class="product-card">
      <img class="product-card-size-helper" src="/res/images/spacer_500x300.png" width="500" height="300"/>
      <div class="product-card-content">
        {this.props.contentComp}
      </div>
    </div>)
  }
}
