
import React from 'react'
import WorldMapSvgOverlay from './WorldMapSvgOverlay'

class OverlayComponent extends React.Component {

  // componentWillMount() {
    // console.log("Mounting SVG " + this.props.id);
  // }

  // componentWillUnmount() {
    // console.log("Unmounting SVG " + this.props.id);
  // }

  render() {
    const style = {
      stroke:"blue",
      fill:"yellow",
      opacity:"0.5"
    }

    return (
      <circle key={this.props.id}
        onClick={this.props.onClickCallback}
        cx={this.props.point2D.x} cy={this.props.point2D.y}
        r="10" style={style} />
      )
  }
}

export default class DotOverlay extends WorldMapSvgOverlay {
  constructor(id, latitude, longitude){
    super(id, latitude, longitude)
  }

  clicked(e) {
    console.log(e)
  }

  render(point2D) {
    return ( <OverlayComponent id={this.id}
      point2D={point2D} onClickCallback={this.clicked.bind(this)} />
    )
  }
}
