
import React from 'react'

import TVisionModel from './TVisionModel'
import TVisionAppManager from './TVisionAppManager'

import * as ComponentModels from './../framework/ComponentModels'
import { Dispatcher } from './../common'

export class TVisionModelFactory {
  build() {
      return new TVisionModel()
  }
}

export default class TVisionManager {
  constructor(uicontext, tvisionCmg, tvisionModel) {
    this.uicontext = uicontext
    this.tvisionCmg = tvisionCmg
    this.tvisionModel = tvisionModel
    this.appManager = new TVisionAppManager(uicontext, {tvisionCmg, tvisionModel})
  }

  updateModel(action, params={}) {
    const modelId = this.tvisionCmg.getModelId()
    const d = this.uicontext.getDispatcher()
    ComponentModels.updateModel(d, modelId, action, params)
  }
  setDisplayMode(mode) { this.updateModel(mode) }
  setDisplayModeHome() {this.setDisplayMode(TVisionModel.setDisplayModeHome)}
  setDisplayModeApp(processId) {
     this.updateModel()
     this.setDisplayMode(TVisionModel.setDisplayModeApp)
   }
  setDisplayModeMenu() {this.setDisplayMode(TVisionModel.setDisplayModeMenu)}
  setDisplayModeAppList() {this.setDisplayMode(TVisionModel.setDisplayModeAppList)}

  inDisplayModeHome(){return this.tvisionModel ? this.tvisionModel.inDisplayModeHome() : false}
  inDisplayModeApp(){return this.tvisionModel ? this.tvisionModel.inDisplayModeApp() : false}
  inDisplayModeMenu(){return this.tvisionModel ? this.tvisionModel.inDisplayModeMenu() : false}
  inDisplayModeAppList(){return this.tvisionModel ? this.tvisionModel.inDisplayModeAppList() : false}

  getAppList() { return this.appManager.getAppList() }
  getProcessList() { return this.tvisionModel ? this.tvisionModel.getProcessList() : [] }
  setProcessVisible(process) { this.appManager.setProcessVisible(process) }
  createAppInstance(app) { return this.appManager.createAppInstance(app) }
  destroyProcess(process) { return this.appManager.destroyProcess(process) }

}
