import React from 'react'
import MenuItem from './MenuItem'


export default class AppMenu extends React.Component {
  render() {
    const appsList = this.props.uicontext.tvisionManager().getAppList()
    const menuComps = appsList.map( app => <MenuItem app={app} /> )

    const shouldDisplayMenu = this.props.uicontext.tvisionManager().inDisplayModeMenu()
    const menuStyle = {
      backgroundColor:"rgba(0,0,0,.4)",
      padding:"15px",
      display:shouldDisplayMenu?"block":"none"
    }

    return (
      <div class="fill-parent" style={menuStyle}>
        {menuComps}
      </div>
    )
  }
}
