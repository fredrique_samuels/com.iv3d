import React from 'react'
import NavButton from './NavButton'
import FormFactory from "./forms/FormFactory";

export default class SaveDeckButton extends React.Component {
    render() {
        const action = this.clicked.bind(this)
        return <NavButton icon="fa-save" disabled={this.disabled()} action={action}/>
    }
    disabled() {
        const { ygoEditorModel } = this.props.params
        const saved = ygoEditorModel.isDeckSaved()
        const notInEditMode = !ygoEditorModel.inDeckEditMode()
        const empty = ygoEditorModel.isDeckEmpty()
        return saved || notInEditMode || empty
    }
    clicked() {
        const { ygoEditorModel } = this.props.params
        new FormFactory(ygoEditorModel).saveDeck()
    }
}