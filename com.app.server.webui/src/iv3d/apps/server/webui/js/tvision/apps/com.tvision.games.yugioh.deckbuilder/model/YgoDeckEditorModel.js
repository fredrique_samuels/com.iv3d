import ViewModeHandler from "./handlers/ViewModeHandler";
import DeckEditHandler from "./handlers/DeckEditHandler";
import PreviewCardHandler from "./handlers/PreviewCardHandler";
import CardSearchHandler from "./handlers/CardSearchHandler";
import DeckStoreHandler from "./handlers/DeckStoreHandler";
import I8nHandler from "./handlers/I8nHandler";
import SampleOpeningHandsHandler from "./handlers/SampleOpeningHandsHandler";
import DeckDao from "./dao/DeckDao";

const LOAD_STATE_DONE = 2
const LOAD_STATE_LOADING = 1
const LOAD_STATE_FAILURE = 0

export default class YgoDeckEditorModel {
    constructor(parentComp, uicontext) {
        this.parentComp = parentComp
        this.uicontext = uicontext
        this.overloadStateData = {}
    }
    getUiContext(){return this.uicontext}
    setUiContext(c){
        this.uicontext=c;
        return this;
    }
    setOverloadState(data){
        this.overloadStateData=data
        return this
    }
    duplicate(){return new YgoDeckEditorModel(this.parentComp, this.uicontext)}
    updateState(stateChanges) {
        this.parentComp.setState(stateChanges)
    }
    getState() {
        return Object.assign({}, this.parentComp.state, this.overloadStateData)
    }
    loadStartupData(){
        this.cardSearchHandler().loadStartupData(this.loadDeckStartupData.bind(this))
    }
    loadDeckStartupData(state) {
        var model = this.duplicate()
        model.setOverloadState(state)
        model.deckStoreHandler().loadStartupData()
    }

    sortByName(cards) {return cards.sort((a,b)=> a.getName().localeCompare(b.getName()))}
    sortCards(results) {
        let cards = []
        // Array.prototype.push.apply(cards, results.filter(c => c.isNormalMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isEffectMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isRitualMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isFusionMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isSynchroMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isXyzMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isPendulumMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isLinkMonster()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isSpell()));
        // Array.prototype.push.apply(cards, results.filter(c => c.isTrap()));
        return results.sort((a,b)=> a.getName().localeCompare(b.getName()))

        // return cards
    }

    i8nMessageHandler(){return new I8nHandler(this)}
    getI8nMessage(messageKey){return this.i8nMessageHandler().getMessage(messageKey)}

    viewModeHandler(){return new ViewModeHandler(this)}
    inDeckEditMode(){return this.viewModeHandler().inDeckEditMode()}
    inDeckSelectMode(){return this.viewModeHandler().inDeckSelectMode()}
    inSampleHandsMode(){return this.viewModeHandler().inSampleHandsMode()}
    inAdvancedSearchMode(){return this.viewModeHandler().inAdvancedSearchMode()}
    setDeckEditMode(){return this.viewModeHandler().setDeckEditMode()}
    setDeckSelectMode(){return this.viewModeHandler().setDeckSelectMode()}
    setSampleHandsMode(){return this.viewModeHandler().setSampleHandsMode()}
    setAdvancedSearchMode(){return this.viewModeHandler().setAdvancedSearchMode()}

    deckEditHandler(){return new DeckEditHandler(this)}
    getMainDeckCards(){return this.deckEditHandler().getMainDeckCards()}
    getExtraDeckCards(){return this.deckEditHandler().getExtraDeckCards()}
    getSideDeckCards(){return this.deckEditHandler().getSideDeckCards()}
    getDeckName(){return this.deckEditHandler().getDeckName()}
    clearDeck(){return this.deckEditHandler().clearDeck()}
    isDeckEmpty(){return this.deckEditHandler().isDeckEmpty()}
    isDeckSaved(){return this.deckEditHandler().isDeckSaved()}
    canAddToMainDeck(cardId){return this.deckEditHandler().canAddToMainDeck(cardId)}
    canAddToSideDeck(cardId){return this.deckEditHandler().canAddToSideDeck(cardId)}
    canMoveToSideDeck(cardId) {return this.deckEditHandler().canMoveToSideDeck(cardId)}
    canMoveToMainDeck(cardId) {return this.deckEditHandler().canMoveToMainDeck(cardId)}
    addToMainDeck(cardId) {this.deckEditHandler().addToMainDeck(cardId)}
    addToSideDeck(cardId) {this.deckEditHandler().addToSideDeck(cardId)}
    moveToSideDeck(cardId) {this.deckEditHandler().moveToSideDeck(cardId)}
    moveToMainDeck(cardId) {this.deckEditHandler().moveToMainDeck(cardId)}
    deleteFromMainDeck(cardId) {this.deckEditHandler().deleteFromMainDeck(cardId)}
    deleteFromSideDeck(cardId) {this.deckEditHandler().deleteFromSideDeck(cardId)}

    previewCardHandler(){return new PreviewCardHandler(this)}
    setPreviewCard(card){this.previewCardHandler().setPreviewCard(card)}
    getPreviewCard(){return this.previewCardHandler().getPreviewCard()}
    getPreviewCardName(){return this.previewCardHandler().getPreviewCardName()}
    getPreviewCardLore(){return this.previewCardHandler().getPreviewCardLore()}

    cardSearchHandler(){return new CardSearchHandler(this)}
    isFilterActive(filterId){return this.cardSearchHandler().isFilterActive(filterId)}
    toggleFilter(filterId, params={}){this.cardSearchHandler().toggleFilter(filterId, params)}
    getCardId(card) {return this.cardSearchHandler().getCardId(card)}
    getCardById(cardId) {return this.cardSearchHandler().getCardById(cardId)}
    getSearchResultCards(){return this.cardSearchHandler().getSearchResultCards()}
    getFilterNumberValue(filterId) {return this.cardSearchHandler().getFilterNumberValue(filterId)}
    updateNumberStateFilter(filterId, value) {this.cardSearchHandler().updateNumberStateFilter(filterId, value)}
    updateSearchTerm(term) {this.cardSearchHandler().updateSearchTerm(term)}

    deckStoreHandler(){return new DeckStoreHandler(this)}
    getDeckNames(){return this.deckStoreHandler().getDeckNames()}
    getDeckByName(deckName){return this.deckStoreHandler().getDeckByName(deckName)}
    getDeckNamesOrderedByLastUpdated(){return this.deckStoreHandler().getDeckNamesOrderedByLastUpdated()}
    setDeckActive(deckName){this.deckStoreHandler().setDeckActive(deckName)}
    saveDeck(deckName){this.deckStoreHandler().saveDeck(deckName)}
    updateDeckSearchTerm(term){this.deckStoreHandler().updateDeckSearchTerm(term)}
    deleteDeck(deckName){this.deckStoreHandler().deleteDeck(deckName)}

    sampleOpeningHAndsHandler(){return new SampleOpeningHandsHandler(this)}
    getSampleOpeningHand() {return this.sampleOpeningHAndsHandler().getSampleOpeningHand()}
    refreshSampleOpeningHand() {return this.sampleOpeningHAndsHandler().refreshSampleOpeningHand()}

    deckDao(){return new DeckDao(this)}

    static getDomains() {
        return [
            new ViewModeHandler(),
            new DeckEditHandler(),
            new PreviewCardHandler(),
            new CardSearchHandler(),
            new DeckStoreHandler(),
            new SampleOpeningHandsHandler()
        ]
    }
    static createState() {
        let state = {
            loadPerc:10.0,
            loadState:LOAD_STATE_DONE,
            resourcesLoading:0
        }

        YgoDeckEditorModel.getDomains().forEach( domain => state = Object.assign({}, state, domain.getDefaults()) )
        return state
    }
}
