import YgoDeckBuilderApp from './apps/com.tvision.games.yugioh.deckbuilder'
import UserSettingsApp from './apps/com.tvision.app.user.settings'
import LocationNewsApp from './apps/com.tvision.app.news.location'
import TVisionModel from './TVisionModel'
import AdminWidgetTestApp from "./apps/com.tvision.app.admin.widgets.test";
import AdminServerOperationsApp from "./apps/com.tvision.app.admin.server.operations";

export default class TVisionAppManager {
  constructor(uicontext) {
    this.uicontext = uicontext
  }

  getAppList() {
      const appList = [
          new YgoDeckBuilderApp(this.uicontext),
          new UserSettingsApp(this.uicontext),
          new LocationNewsApp(this.uicontext),
          new AdminWidgetTestApp(this.uicontext),
          new AdminServerOperationsApp(this.uicontext)
        ]
      return appList.sort((a,b)=> a.getTitle().localeCompare(b.getTitle()))
  }

  setProcessVisible(process) {
    this.uicontext.tvisionManager().updateModel(TVisionModel.setProcessVisible, {process:process})
  }

  createAppInstance(app) {
    this.uicontext.tvisionManager().updateModel(TVisionModel.createAppInstance, {app:app})
  }

    destroyProcess(process) {
        this.uicontext.tvisionManager().updateModel(TVisionModel.destroyProcess, {process:process})
    }

}
