import React from 'react'
import MenuItemIcon from './menu/MenuItemIcon'

class AppView extends React.Component {
  render() {
    const { process, uicontext, processIndex } = this.props

    const selected = process.visible
    const appListMode = uicontext.tvisionManager().inDisplayModeAppList()
    const appMode = uicontext.tvisionManager().inDisplayModeApp()


    let appCssStyle = {
      position:"relative"
    }

    let appSelectCss = {
      display:"none"
    }

    let topCss = {
      position:"relative",
      display:"none"
    }

    if(appMode && selected) {
      topCss = Object.assign({}, topCss, {
          display:"block",
          zIndex:""+processIndex
      })
    }

    if(appListMode) {
      appCssStyle = Object.assign({}, appCssStyle, {
          transform:"scale(.75 , .75) translate(-15%, 0%)"
      })
      appSelectCss = Object.assign({}, appSelectCss, {display:"block"})
      topCss = Object.assign({}, topCss, {display:"block",height:"100%"})
    }

    return (
      <div class="fill-parent app-cntr" style={topCss} >
        <div class="fill-parent" style={appCssStyle} >
          {process.app.createView()}
          <button class="applist-select-button" onClick={this.select.bind(this)} style={appSelectCss} ></button>
        </div>

        {appListMode?this.getAppTitleBox(process.app.getTitle()):null}
      </div>
    )
  }

  select() {
    const { selectAppCallback, process } = this.props
    selectAppCallback(process)
  }

  getAppTitleBox(title) {

    const { process } = this.props

    const css = {
      position:"absolute",
      left:"75%",
      top:"50%",
      width:"25%",
      color:"white",
      textAlign:"center",
      textShadow:"0px 0px 10px black",
    }
    return (<div class="inline-element" style={css} >
      <div style={{height:"30px"}}>
          <i class="fa fa-close fa-1x" style={{paddingRight:"5px"}} onClick={this.stopProcess.bind(this)}></i>{process.app.getTitle()}
      </div>
      <div style={{width:"100%", display:"flex", justifyContent:"center"}}>
          <div style={{paddingBottom:"20px", height:"70px", width:"50px"}} >
              <MenuItemIcon icon={process.app.getIcon()} />
          </div>
      </div>
    </div>)
  }

  stopProcess() {
      const { process, uicontext } = this.props
      uicontext.tvisionManager().destroyProcess(process)
  }

}

export default class AppSection extends React.Component {

  componentWillMount() {
  }

  render() {
    const { uicontext } = this.props
    const appMode = uicontext.tvisionManager().inDisplayModeApp()
    const appListMode = uicontext.tvisionManager().inDisplayModeAppList()
    const processList = uicontext.tvisionManager().getProcessList();

    let viewStyle = {
      backgroundColor:"rgba(0,0,0,.4)",
      display:"none",
      overflow:"auto",
      position:"relative"
    }
    if(appMode || appListMode) {
        if(processList.length==0) {
          return (<div class="fill-parent" style={{textShadow:"0px 0px 10px black",color:"white", backgroundColor:"rgba(0,0,0,.4)",display:"flex", alignItems:"center",textAlign:"center", justifyContent:"center"}}>
            There are no running applications
          </div>)
        }

        viewStyle  = Object.assign({}, viewStyle, {display:"block"})
    }

    const processViews = processList.map( (process, index) =>  <AppView uicontext={uicontext} selectAppCallback={this.selectApp.bind(this)} process={process} processIndex={index}/> )
    return (<div class="fill-parent" style={viewStyle}>
      {processViews}
    </div>)
  }

  selectApp(process) {
    this.props.uicontext.tvisionManager().setProcessVisible(process)
  }
}
