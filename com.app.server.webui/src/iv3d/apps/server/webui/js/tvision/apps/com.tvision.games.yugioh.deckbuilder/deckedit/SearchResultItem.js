import React from 'react'

import * as DndConstants from '../dnd/DndConstants'

import CardDragData from '../dnd/CardDragData'
import StyleCache from '../styles/StyleCache'
import YgoCardView from '../YgoCardView'

export default class SearchResultItem extends React.Component {
    render() {
        const { card, ygoEditorModel } = this.props
        const styleCache = new StyleCache();

        const stars = card.getStars()
        const atkdef  = card.getAtkDefSummaryString()

        return (
            <div style={{width:"100%"}}>
                <div class="inline-element" style={styleCache.get('card-space')} >
                    <YgoCardView card={card} ygoEditorModel={ygoEditorModel} dragStartedCallback={this.cardDragStarted.bind(this)}/>
                </div>
                <div class="inline-element" style={styleCache.get('search-result-item-info')} >
                    <div>{card.getName()}</div>
                    <div>{card.getTypeSummaryString()}
                        {stars?<i class="fa fa-star">{stars}</i>:null}
                    </div>
                    <div>{atkdef?atkdef:""}</div>
                </div>
            </div>
        )
    }
    cardDragStarted(card, event) {
        const { ygoEditorModel } = this.props
        event.dataTransfer.setData(DndConstants.YGO_CARD_DRAG_DATA,new CardDragData(DndConstants.YGO_CARD_DRAG_SOURCE_SEARCH_RESULTS, card, ygoEditorModel).toJson())
    }
}