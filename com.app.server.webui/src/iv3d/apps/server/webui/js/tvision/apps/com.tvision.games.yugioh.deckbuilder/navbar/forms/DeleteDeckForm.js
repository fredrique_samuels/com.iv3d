import {FORM_DISPLAY_TYPE_BLOCK_TEXT} from "../../../../../framework/form/FormConstants";

export default class DeleteDeckForm {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    clearDeck(form) {
        this.ygoEditorModel.deleteDeck(this.ygoEditorModel.getDeckName())
        return true
    }
    getView() {
        const formDescription = {
            submit : {
                type: "callback",
                callback : this.clearDeck.bind(this)
            },
            actions : [
                {
                    title : this.ygoEditorModel.getI8nMessage('ygo.edit.cleardeck.cancel'),
                    type : "form.action.type.cancel"
                },
                {
                    title : this.ygoEditorModel.getI8nMessage('ygo.edit.deletedeck.submit'),
                    type : "form.action.type.submit"
                }
            ],
            inputs : [
                {
                    type : FORM_DISPLAY_TYPE_BLOCK_TEXT,
                    text : this.ygoEditorModel.getI8nMessage('ygo.edit.deletedeck.notice')
                }
            ]
        }

        return formDescription
    }
}