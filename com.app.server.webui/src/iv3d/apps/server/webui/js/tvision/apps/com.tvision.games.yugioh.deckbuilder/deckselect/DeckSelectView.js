import React from 'react'

import AbstractViewContainer from '../AbstractViewContainer'
import DeckListView from '../decklist/DeckListView'
import DeckSearchPanel from './DeckSearchPanel'
import DeckLastEditedPanel from './DeckLastEditedPanel'
import StyleCache from '../styles/StyleCache'


export default class DeckSelectView extends React.Component {
    render() {
        // const styleCache = new StyleCache();
        const { ygoEditorModel } = this.props.params
        return (<AbstractViewContainer hidden={!ygoEditorModel.inDeckSelectMode()} >
            <DeckListView params={this.props.params} />
            <DeckSearchPanel params={this.props.params} />
            <DeckLastEditedPanel params={this.props.params} />
        </AbstractViewContainer>)
    }
}