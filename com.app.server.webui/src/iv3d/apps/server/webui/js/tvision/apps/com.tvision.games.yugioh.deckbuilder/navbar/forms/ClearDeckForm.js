import {FORM_DISPLAY_TYPE_BLOCK_TEXT} from "../../../../../framework/form/FormConstants";

export default class ClearDeckForm {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    clearDeck(form) {
        this.ygoEditorModel.clearDeck()
        return true
    }
    getView() {
        const formDescription = {
            submit : {
                type: "callback",
                callback : this.clearDeck.bind(this)
            },
            actions : [
                {
                    title : this.ygoEditorModel.getI8nMessage('ygo.edit.cleardeck.cancel'),//"Cancel",
                    type : "form.action.type.cancel"
                },
                {
                    title : this.ygoEditorModel.getI8nMessage('ygo.edit.cleardeck.submit'),//"Clear Deck Profile",
                    type : "form.action.type.submit"
                }
            ],
            inputs : [
                {
                    type : FORM_DISPLAY_TYPE_BLOCK_TEXT,
                    text : this.ygoEditorModel.getI8nMessage('ygo.edit.cleardeck.notice')
                }
            ]
        }

        return formDescription
    }
}