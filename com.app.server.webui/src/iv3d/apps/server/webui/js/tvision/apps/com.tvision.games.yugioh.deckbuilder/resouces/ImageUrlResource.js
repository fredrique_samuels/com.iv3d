import $ from 'jquery'

export default class ImageUrlResource {
  constructor(onSuccessCallback, onErrorCallback) {
    this.data = {}
    this.url = "https://raw.githubusercontent.com/fredriquesamuels/dashboard-ui/gh-pages/yugioh-res/deckbuilder/card-image-url-list.json"
    this.onSuccessCallback = onSuccessCallback
    this.onErrorCallback = onErrorCallback
  }
  load() {
    $.getJSON(this.url, this.success.bind(this))
      .fail(this.failure.bind(this));
  }
  success(obj) {
    this.data = obj
    this.onSuccessCallback()
  }
  failure() {
    this.onErrorCallback()
  }
}
