import React from 'react'
import StyleCache from '../styles/StyleCache'
import * as YgoCardConstants from '../model/YgoCardParam'
import * as YgoEditorModelConstants from '../model/YgoEditorModelConstants'



class NumberInput extends React.Component {
    render() {
        const { arrow } = this.props
        const { uicontext } = this.props.params
        const lowerInput = uicontext.formComponentManager().createNumberInput({noTitle:true, min:"0", max:"13", addonRight:{faIcon:"fa-level-"+arrow}, onChangeAction:this.updated.bind(this)})
        return (
            <div class="inline-element" style={{width:"120px"}} >
                {lowerInput}
            </div>
        )
    }
    updated(event) {
        const { ygoEditorModel } = this.props.params
        const { filterId } = this.props
        if(ygoEditorModel) {
            ygoEditorModel.updateNumberStateFilter(filterId, event.target.value)
        }
    }
}

class NumberInputMin extends React.Component {
    render() {
        const {filterId, params} = this.props
        return <NumberInput filterId={filterId} params={params} arrow="down"/>
    }
}

class NumberInputMax extends React.Component {
    render() {
        const {filterId, params} = this.props
        return <NumberInput filterId={filterId} params={params} arrow="up"/>
    }
}


class StarsInputs extends React.Component {
    render(){
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();

        const lowerInput = uicontext.formComponentManager().createNumberInput({noTitle:true, min:"0", max:"13", addonRight:{faIcon:"fa-level-down"}})
        const upperInput = uicontext.formComponentManager().createNumberInput({noTitle:true, min:"0", max:"13", addonRight:{faIcon:"fa-level-up"}})

        return (
            <div class="inline-element" style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Stars</div>
                <NumberInputMin params={this.props.params} filterId={YgoEditorModelConstants.MIN_STARS_FILTER} />
                <NumberInputMax params={this.props.params} filterId={YgoEditorModelConstants.MAX_STARS_FILTER} />
            </div>
        )
    }
}

class ScaleInputs extends React.Component {
    render(){
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();

        const lowerInput = uicontext.formComponentManager().createNumberInput({noTitle:true, min:"0", max:"13", addonRight:{faIcon:"fa-level-down"}})
        const upperInput = uicontext.formComponentManager().createNumberInput({noTitle:true, min:"0", max:"13", addonRight:{faIcon:"fa-level-up"}})

        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Pendulum Scale</div>
                <NumberInputMin params={this.props.params} filterId={YgoEditorModelConstants.MIN_SCALE_FILTER} />
                <NumberInputMax params={this.props.params} filterId={YgoEditorModelConstants.MAX_SCALE_FILTER} />
            </div>
        )
    }
}

class AtkInputs extends React.Component {
    render(){
        const styleCache = new StyleCache();
        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>ATK</div>
                <NumberInputMin params={this.props.params} filterId={YgoEditorModelConstants.MIN_ATK_FILTER} />
                <NumberInputMax params={this.props.params} filterId={YgoEditorModelConstants.MAX_ATK_FILTER} />
            </div>
        )
    }
}

class DefInputs extends React.Component {
    render(){
        const styleCache = new StyleCache();
        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>DEF</div>
                <NumberInputMin params={this.props.params} filterId={YgoEditorModelConstants.MIN_DEF_FILTER} />
                <NumberInputMax params={this.props.params} filterId={YgoEditorModelConstants.MAX_DEF_FILTER} />
            </div>
        )
    }
}


class LinkMarkerToggleButton extends React.Component {
    render() {
        const { rot } = this.props
        const { ygoEditorModel, filterId } = this.props
        const active = ygoEditorModel?ygoEditorModel.isFilterActive(filterId):false
        return (
            <button class={"btn btn-xs color-"+(active?"error":"default")} onClick={this.clicked.bind(this)} style={{width:"24px",height:"24px"}}>
                <i class={"fa fa-caret-up " + rot}></i>
            </button>
        )
    }
    clicked() {
        const { ygoEditorModel, filterId } = this.props
        if(ygoEditorModel) {
            ygoEditorModel.toggleFilter(filterId)
        }
    }
}

class LinkInputs extends React.Component {
    render() {
        const { ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();

        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Link</div>
                <NumberInputMin params={this.props.params} filterId={YgoEditorModelConstants.MIN_LINK_FILTER} />
                <NumberInputMax params={this.props.params} filterId={YgoEditorModelConstants.MAX_LINK_FILTER} />
                <div class="inline-element" style={{width:"100px"}} >
                    <div>
                        <LinkMarkerToggleButton rot="fa-rotate-315" filterId={YgoCardConstants.LINK_MARKER_TOP_LEFT} ygoEditorModel={ygoEditorModel} />
                        <LinkMarkerToggleButton rot=""  filterId={YgoCardConstants.LINK_MARKER_TOP} ygoEditorModel={ygoEditorModel} />
                        <LinkMarkerToggleButton rot="fa-rotate-45"  filterId={YgoCardConstants.LINK_MARKER_TOP_RIGHT} ygoEditorModel={ygoEditorModel} />
                    </div>
                    <div>
                        <LinkMarkerToggleButton rot="fa-rotate-270"  filterId={YgoCardConstants.LINK_MARKER_LEFT} ygoEditorModel={ygoEditorModel} />
                        <button class="btn btn-xs color-default" style={{visibility:"hidden", width:"24px",height:"24px"}}>
                            <i class="fa  fa-circle"></i>
                        </button>
                        <LinkMarkerToggleButton rot="fa-rotate-90"  filterId={YgoCardConstants.LINK_MARKER_RIGHT} ygoEditorModel={ygoEditorModel} />
                    </div>
                    <div>
                        <LinkMarkerToggleButton rot="fa-rotate-225" filterId={YgoCardConstants.LINK_MARKER_BOTTOM_LEFT} ygoEditorModel={ygoEditorModel} />
                        <LinkMarkerToggleButton rot="fa-rotate-180" filterId={YgoCardConstants.LINK_MARKER_BOTTOM} ygoEditorModel={ygoEditorModel}/>
                        <LinkMarkerToggleButton rot="fa-rotate-135" filterId={YgoCardConstants.LINK_MARKER_BOTTOM_RIGHT} ygoEditorModel={ygoEditorModel}/>
                    </div>
                </div>
            </div>
        )
    }
}


class ToggleButton extends React.Component {
    render() {
        const { text, ygoEditorModel, filterId } = this.props
        const active = ygoEditorModel?ygoEditorModel.isFilterActive(filterId):false
        const buttonStyle = {height:"24px", margin:"2px"};
        return (
            <button class={"btn btn-xs color-"+(active?"error":"default")} onClick={this.clicked.bind(this)} style={buttonStyle}>
                {text}
            </button>
        )
    }
    clicked() {
        const { ygoEditorModel, filterId } = this.props
        if(ygoEditorModel) {
            ygoEditorModel.toggleFilter(filterId)
        }
    }

}

class AttributeInputs extends React.Component {
    render() {
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();
        const buttonStyle = {height:"24px", margin:"2px"};
        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Attributes</div>
                <ToggleButton text={"DARK"} filterId={YgoCardConstants.ATTRIBUTE_DARK} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"LIGHT"} filterId={YgoCardConstants.ATTRIBUTE_LIGHT} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"EARTH"} filterId={YgoCardConstants.ATTRIBUTE_EARTH} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"FIRE"} filterId={YgoCardConstants.ATTRIBUTE_FIRE} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"WIND"} filterId={YgoCardConstants.ATTRIBUTE_WIND} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"WATER"} filterId={YgoCardConstants.ATTRIBUTE_WATER} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"DIVINE"} filterId={YgoCardConstants.ATTRIBUTE_DIVINE} ygoEditorModel={ygoEditorModel} />
            </div>
        )
    }
}

class IconInputs extends React.Component {
    render() {
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();
        const buttonStyle = {height:"24px", margin:"2px"};
        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Icon</div>
                <ToggleButton text={"Equip"} filterId={YgoCardConstants.PROPERTY_EQUIP} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Field"} filterId={YgoCardConstants.PROPERTY_FIELD} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Quick-Play"} filterId={YgoCardConstants.PROPERTY_QUICK_PLAY} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Ritual"} filterId={YgoCardConstants.PROPERTY_RITUAL+"Spell"} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Continuous"} filterId={YgoCardConstants.PROPERTY_CONTINUOUS} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Counter"} filterId={YgoCardConstants.PROPERTY_COUNTER} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Normal"} filterId={YgoCardConstants.PROPERTY_NORMAL+"Card"} ygoEditorModel={ygoEditorModel} />
            </div>
        )
    }
}

class MonsterTypeInputs extends React.Component {
    render() {
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();
        const buttonStyle = {height:"24px", margin:"2px"};

        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Monster Types</div>
                <ToggleButton text={"Spellcaster"} filterId={YgoCardConstants.TYPE_SPELLCASTER} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Dragon"} filterId={YgoCardConstants.TYPE_DRAGON} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Zombie"} filterId={YgoCardConstants.TYPE_ZOMBIE} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Warrior"} filterId={YgoCardConstants.TYPE_WARRIOR} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Beast-Warrior"} filterId={YgoCardConstants.TYPE_BEAST_WARRIOR} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Beast"} filterId={YgoCardConstants.TYPE_BEAST} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Winged Beast"} filterId={YgoCardConstants.TYPE_WINGED_BEAST} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Fiend"} filterId={YgoCardConstants.TYPE_FIEND} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Fairy"} filterId={YgoCardConstants.TYPE_FAIRY} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Insect"} filterId={YgoCardConstants.TYPE_INSECT} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Dinosaur"} filterId={YgoCardConstants.TYPE_DINOSAUR} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Reptile"} filterId={YgoCardConstants.TYPE_REPTILE} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Fish"} filterId={YgoCardConstants.TYPE_FISH} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Sea Serpent"} filterId={YgoCardConstants.TYPE_SEA_SERPENT} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Aqua"} filterId={YgoCardConstants.TYPE_AQUA} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Pyro"} filterId={YgoCardConstants.TYPE_PYRO} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Rock"} filterId={YgoCardConstants.TYPE_ROCK} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Plant"} filterId={YgoCardConstants.TYPE_PLANT} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Machine"} filterId={YgoCardConstants.TYPE_MACHINE} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Psychic"} filterId={YgoCardConstants.TYPE_PSYCHIC} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Divine-Beast"} filterId={YgoCardConstants.TYPE_DIVINE_BEAST} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Wyrm"} filterId={YgoCardConstants.TYPE_WYRM} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Cyberse"} filterId={YgoCardConstants.TYPE_CYBERSE} ygoEditorModel={ygoEditorModel}/>
            </div>
        )
    }
}

class CardTypeInputs extends React.Component {
    render() {
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();
        const buttonStyle = {height:"24px", margin:"2px"};
        return (
            <div style={styleCache.get("adv-card-search-panel")}>
                <div style={styleCache.get("adv-search-heading")}>Card Types</div>
                <ToggleButton text={"Normal"} filterId={YgoCardConstants.TYPE_NORMAL} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Effect"} filterId={YgoCardConstants.TYPE_EFFECT} ygoEditorModel={ygoEditorModel} />
                <ToggleButton text={"Ritual"} filterId={YgoCardConstants.TYPE_RITUAL} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Fusion"} filterId={YgoCardConstants.TYPE_FUSION} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Synchro"} filterId={YgoCardConstants.TYPE_SYNCHRO} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Xyz"} filterId={YgoCardConstants.TYPE_XYZ} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Toon"} filterId={YgoCardConstants.TYPE_TOON} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Spirit"} filterId={YgoCardConstants.TYPE_SPIRIT} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Union"} filterId={YgoCardConstants.TYPE_UNION} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Gemini"} filterId={YgoCardConstants.TYPE_GEMINI} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Tuner"} filterId={YgoCardConstants.TYPE_TUNER} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Flip"} filterId={YgoCardConstants.TYPE_FLIP} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Pendulum"} filterId={YgoCardConstants.TYPE_PENDULUM} ygoEditorModel={ygoEditorModel}/>
                <ToggleButton text={"Link"} filterId={YgoCardConstants.TYPE_LINK} ygoEditorModel={ygoEditorModel}/>
            </div>
        )
    }
}

export default class AdvancedCardFilterView extends React.Component {
    render() {
        const { uicontext, ygoEditorModel } = this.props.params
        const styleCache = new StyleCache();

        return (
            <div class="inline-element" style={styleCache.get("adv-search-view-container", (ygoEditorModel.inAdvancedSearchMode()?{}:{display:"none"})) }>
                <StarsInputs params={this.props.params} />
                <ScaleInputs params={this.props.params} />
                <AtkInputs params={this.props.params} />
                <DefInputs params={this.props.params} />
                <LinkInputs params={this.props.params} />
                <AttributeInputs  params={this.props.params} />
                <IconInputs  params={this.props.params} />
                <MonsterTypeInputs  params={this.props.params} />
                <CardTypeInputs params={this.props.params} />
            </div>
        )
    }
}