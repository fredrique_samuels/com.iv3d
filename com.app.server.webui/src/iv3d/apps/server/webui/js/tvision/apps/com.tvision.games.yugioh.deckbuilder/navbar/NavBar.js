import React from 'react'
import StyleCache from '../styles/StyleCache'

import EditDeckModeButton from './EditDeckModeButton'
import DeckSelectModeButton from './DeckSelectModeButton'
import SampleHandsModeButton from './SampleHandsModeButton'
import SaveDeckButton from './SaveDeckButton'
import ClearDeckButton from './ClearDeckButton'
import DeleteDeckButton from './DeleteDeckButton'

export default class NavBar extends React.Component {
  render() {
      const navButtongroup = {
        marginBottom:"40px"
      }
    return <div class="inline-element back-panel" style={new StyleCache().get('nav-bar')}>
        <div style={navButtongroup}>
            <SaveDeckButton params={this.props.params} />
            <ClearDeckButton params={this.props.params} />
        </div>

        <div style={navButtongroup}>
            <DeleteDeckButton params={this.props.params} />
        </div>

        <div style={navButtongroup}>
            <EditDeckModeButton params={this.props.params} />
            <DeckSelectModeButton  params={this.props.params} />
            <SampleHandsModeButton params={this.props.params} />
        </div>
    </div>
  }
}
