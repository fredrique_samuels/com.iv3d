import Immutable from 'immutable'
import AbstractStyleCache from "../../../../framework/AbstractStyleCache";

export default class StyleCache extends AbstractStyleCache {
  constructor(uicontext) {
    super(uicontext)
    const cardWidth = "51.45"
    const cardHeight = "75"
    const backgroundColor = "rgba(255, 255, 255, .7)"
    const border = "1px solid black"

    // Card ratio = w/h = 0.686
    this.set('card-space',
          {
            width:cardWidth+"px",
            height:cardHeight+"px",
            margin:"1px",
            // border:"1px solid black"
          })
    this.set('nav-bar',
            {
              width:"60px",
              height:"100%",
              padding:"10px"
            })
    this.set('abstract-view-container',
            {
              width: "calc(100% - 65px)",
              height: "100%",
              marginLeft: "5px",
              backgroundColor: "rgba(255, 255, 255, .2)"
            })
    this.set('deck-view-container',
          {
            width:"calc( 100% - 500px)",
            height:"100%",
            padding:"10px",
            border:"1px solid white"
          })
    this.set('adv-search-view-container',
          {
              width:"calc( 100% - 500px)",
              height:"100%",
              padding:"10px",
              border:"1px solid white",
              overflowY:"auto"
          })
    this.set('card-search-container',{
            width:"245px",
            height:"100%",
            padding:"10px",
            marginLeft:"5px",
            border:"1px solid white"
          })

    this.set('card-details-container',{
            width:"245px",
            height:"100%",
            padding:"10px",
            marginRight:"5px",
            border:"1px solid white",
            backgroundColor:backgroundColor
          })
    this.set('card-info-space',
          {
            width:"225px",
            height:"328px",
            margin:"1px",
            border:"none"//"1px solid black"
          })

    this.set('card-lore-block',
          {
            width:"225px",
            height:"calc(100% - 340px)",
            margin:"1px",
            overflowY:"auto"
          })
    this.set('main-deck-container',{
              width:"100%",
              height:"calc( ((100% - 30px) / 6.0) * 4 )",
              backgroundColor:backgroundColor,
              border:"1px solid black",
              marginTop:"2px",
              marginBottom:"2px",
            })
    this.set('extra-deck-container',{
              width:"100%",
              height:"calc( ((100% - 10px) / 6.0) * 1 )",
              backgroundColor:backgroundColor,
              border:"1px solid black",
        marginTop:"2px",
        marginBottom:"2px",
            })
    this.set('side-deck-container',{
              width:"100%",
              height:"calc( ((100% - 10px) / 6.0) * 1 )",
              backgroundColor:backgroundColor,
              border:"1px solid black",
                marginTop:"2px",
                marginBottom:"2px",
            })
    this.set('card-count-label',{
              width:"100%",
              height:"20px",
              display:"flex",
              alignItems: "center",
              fontFamily:'"Open Sans",Arial,sans-serif',
              fontWeight:"bold",
              color:"slategrey"
            })
    this.set('card-list-container',{
              width:"100%",
              height:"calc(100% - 20px)",
              overflowY:"auto"
            })
    this.set('search-term-input-container',{
              width:"100%",
              height:"40px",
              backgroundColor:"rgba(255, 255, 255, .2)",
              marginBottom:"2px",
              paddingTop:"5px",
              paddingLeft:"5px",
              paddingRight:"5px"
            })
    this.set('search-results-container',{
              width:"100%",
              height:"calc(100% - 42px)",
              backgroundColor:backgroundColor,
            })
    this.set('search-result-item-info',{
              width:"calc(100% - "+cardWidth+"px - 5px)",
              overflowX:"auto",
              fontSize: "12px"
            })
    this.set('adv-card-search-panel',{
              backgroundColor:"rgba(0,0,0,.5)",
              border:border
            })
    this.set('adv-search-heading',{
              color:"#eeeeee",
              fontWeight: "bold",
              fontSize: "14px"
            })
    this.set('deck-select-panel',{
                width:"100%",
                height:"100%",
                backgroundColor:backgroundColor
            })
      this.set('recent-deck-edited-panel',{
          width:"100%",
          height:"100%",
          backgroundColor:backgroundColor,
      })


  }

}
