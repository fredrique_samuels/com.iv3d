

export default class SaveDeckForm {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    submitForm(form) {
        const { dataMap } = form
        console.error("Saving Deck")
        console.log(this.ygoEditorModel)
        this.ygoEditorModel.saveDeck(dataMap["deckname"])
        return true
    }
    getView() {
        const formDescription = {
            validation : {
                type:"callback",
                callback : this.validateForm.bind(this)
            },
            submit : {
                type: "callback",
                callback : this.submitForm.bind(this)
            },
            actions : [
                {
                    title : this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.cancel"),
                    type : "form.action.type.cancel"
                },
                {
                    title : this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.submit"),
                    type : "form.action.type.submit"
                }
            ],
            inputs : [
                {
                    type :'form.input.type.text',
                    title : this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.text"),
                    placeholder : this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.placeholder"),
                    exampleText : this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.exampletext"),
                    helpText : this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.nameinput.helptext"),
                    name: "deckname",
                    value:this.ygoEditorModel.getDeckName()
                }
            ]
        }
        return formDescription
    }
    validateForm(form) {
        const { dataArray, data , dataMap} = form
        const errors = []

        const deckname = dataMap["deckname"]
        if(deckname) {
            if(deckname=="") {
                errors.push({
                    field:"deckname",
                    message:this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.validate.deckname.empty")
                })
            }
        }
        else {
            errors.push({
                field:"deckname",
                message:this.ygoEditorModel.getI8nMessage("ygo.edit.savedeck.validate.deckname.notprovided")
            })
        }

        return {
            passed:errors.length==0,
            errors: errors
        }
    }
}

