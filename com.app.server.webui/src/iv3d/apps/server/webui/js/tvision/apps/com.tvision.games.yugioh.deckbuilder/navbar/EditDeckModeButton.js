import React from 'react'
import NavButton from './NavButton'

export default class EditDeckModeButton extends React.Component {
    render() {
        const { ygoEditorModel } = this.props.params
        const isActive = ygoEditorModel.inDeckEditMode()
        const action = isActive?null:ygoEditorModel.setDeckEditMode.bind(ygoEditorModel)
        return <NavButton icon="fa-pencil" isActive={isActive} action={action}/>
    }
}