import React from 'react'
import NavButton from './NavButton'
import FormFactory from "./forms/FormFactory";

export default class DeckSelectModeButton extends React.Component {
    render() {
        const { ygoEditorModel } = this.props.params
        const isActive = ygoEditorModel.inDeckSelectMode()
        return <NavButton icon="fa-navicon" isActive={isActive} action={isActive?null:this.clicked.bind(this)}/>
    }
    clicked() {
        const { ygoEditorModel } = this.props.params
        if(ygoEditorModel.isDeckEmpty() || ygoEditorModel.isDeckSaved()) {
            ygoEditorModel.setDeckSelectMode()
        } else {
            new FormFactory(ygoEditorModel).saveBeforeDeckEditExit()
        }
    }
}
