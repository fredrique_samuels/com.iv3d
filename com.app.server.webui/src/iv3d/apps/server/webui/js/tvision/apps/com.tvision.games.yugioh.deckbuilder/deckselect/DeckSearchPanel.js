import React from 'react'

import DeckSelectProfile from './DeckSelectProfile'
import StyleCache from '../styles/StyleCache'

export default class DeckSearchPanel extends React.Component {
    render() {
        const styleCache = new StyleCache();
        const { uicontext, ygoEditorModel } = this.props.params

        const searchResults = ygoEditorModel.getDeckNames()
        const searchResultsComp = searchResults.map( deckName => <DeckSelectProfile deck={ygoEditorModel.getDeckByName(deckName)} ygoEditorModel={ygoEditorModel} /> )

        return (
            <div class="inline-element" style={styleCache.get("card-search-container")}>
                <div style={styleCache.get('search-term-input-container')}>
                    <div class="inline-element" style={{width:"100%", height:"100%"}}>
                        { uicontext.formComponentManager().createTextInput({noTitle:true, addonRight:{faIcon:"fa-search"}, onChangeAction:this.searchTermUpdated.bind(this)}) }
                    </div>
                </div>
                <div style={styleCache.get('search-results-container')}>
                    <div class="back-panel" style={styleCache.get("card-count-label")} >{ygoEditorModel.getI8nMessage("ygo.edit.search.results") +" : " + searchResults.length}</div>
                    <div style={styleCache.get("card-list-container")} >
                        {searchResultsComp}
                    </div>
                </div>
            </div>)
    }
    searchTermUpdated(event) {
        const { ygoEditorModel } = this.props.params
        ygoEditorModel.updateDeckSearchTerm(event.target.value)
    }
}