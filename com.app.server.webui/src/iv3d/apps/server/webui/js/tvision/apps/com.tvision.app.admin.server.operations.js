import React from 'react'
import TVisionApp from './../TVisionApp'
import AdminServerOperationsAppView from './com.tvision.app.admin.server.operations/AdminServerOperationsAppView'

class ViewFactory {
    createView(uicontext, app) {
        return <AdminServerOperationsAppView uicontext={uicontext}  app={app} />
    }
}

export default class AdminServerOperationsApp extends TVisionApp {
    constructor(uicontext) {
        super(uicontext, {
            title : "Server Operations Queue",
            icon : {
                    type : "icon.type.image",
                    graphic : "/res/images/icons/config.png"
                },
            packageId : "com.tvision.app.admin.server.operations",
            viewFactory : new ViewFactory()
        })
    }
}
