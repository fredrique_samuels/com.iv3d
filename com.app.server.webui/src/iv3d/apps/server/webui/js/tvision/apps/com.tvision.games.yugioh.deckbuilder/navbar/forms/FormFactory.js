import SaveDeckForm from "./SaveDeckForm";
import ClearDeckForm from "./ClearDeckForm";
import SaveBeforeDeckEditExitForm from "./SaveBeforeDeckEditExitForm";
import DeleteDeckForm from "./DeleteDeckForm";

export default class FormFactory {
    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }

    saveDeck(){
        var form = new SaveDeckForm(this.ygoEditorModel);
        this.ygoEditorModel.getUiContext()
            .formComponentManager()
            .createLayeredForm(form.getView())
    }

    clearDeck() {
        var form = new ClearDeckForm(this.ygoEditorModel)
        this.ygoEditorModel.getUiContext()
            .formComponentManager()
            .createLayeredForm(form.getView())
    }

    deleteDeck() {
        var form = new DeleteDeckForm(this.ygoEditorModel)
        this.ygoEditorModel.getUiContext()
            .formComponentManager()
            .createLayeredForm(form.getView())
    }

    saveBeforeDeckEditExit() {
        var form = new SaveBeforeDeckEditExitForm(this.ygoEditorModel)
        this.ygoEditorModel.getUiContext()
            .formComponentManager()
            .createLayeredForm(form.getView())
    }
}