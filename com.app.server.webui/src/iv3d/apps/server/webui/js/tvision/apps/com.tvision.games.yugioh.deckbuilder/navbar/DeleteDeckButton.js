import React from 'react'
import NavButton from './NavButton'
import FormFactory from "./forms/FormFactory";

export default class SaveDeckButton extends React.Component {
    render() {
        const action = this.clicked.bind(this)
        return <NavButton icon="fa-trash" disabled={this.disabled()} action={action}/>
    }
    disabled() {
        const { ygoEditorModel } = this.props.params
        return !ygoEditorModel.inDeckSelectMode()
    }
    clicked() {
        const { ygoEditorModel } = this.props.params
        new FormFactory(ygoEditorModel).deleteDeck()
    }
}