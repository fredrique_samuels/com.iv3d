import React from 'react'

import StyleCache from '../styles/StyleCache'

import AdvancedSearchButton from './AdvancedSearchButton'
import SearchResultItem from './SearchResultItem'
import CardDragData from "../dnd/CardDragData";

export default class CardFilterPanel extends React.Component {
    render() {
        const styleCache = new StyleCache();
        const { uicontext, ygoEditorModel } = this.props.params

        const searchResults = ygoEditorModel.getSearchResultCards()
        const searchResultsComp = searchResults.map( c => <SearchResultItem card={c} ygoEditorModel={ygoEditorModel} /> )

        return (
            <div class="inline-element" style={styleCache.get("card-search-container")}>
                <div style={styleCache.get('search-term-input-container')}>
                    <div class="inline-element" style={{width:"40px", height:"100%"}}>
                        <AdvancedSearchButton params={this.props.params} />
                    </div>
                    <div class="inline-element" style={{width:"calc(100% - 40px)", height:"100%"}}>
                        { uicontext.formComponentManager().createTextInput({noTitle:true, addonRight:{faIcon:"fa-search"}, onChangeAction:this.searchTermUpdated.bind(this)}) }
                    </div>
                </div>
                <div style={styleCache.get('search-results-container')} onDrop={this.cardDropped.bind(this)} onDragOver={this.allowCardDrop.bind(this)}>
                    <div class="back-panel" style={styleCache.get("card-count-label")} >{ygoEditorModel.getI8nMessage("ygo.edit.search.results") +" : " + searchResults.length}</div>
                    <div style={styleCache.get("card-list-container")} >
                        {searchResultsComp}
                    </div>
                </div>
            </div>)
    }
    allowCardDrop(event) {
        const { ygoEditorModel } = this.props.params
        const transferData = new CardDragData().fromEvent(event, ygoEditorModel)
        if(transferData) {
            if(transferData.canDeleteCard()) {
                event.preventDefault();
            }
        }
    }
    cardDropped(event) {
        const { ygoEditorModel } = this.props.params
        const transferData = new CardDragData().fromEvent(event, ygoEditorModel)
        if(transferData) {
            event.preventDefault();
            transferData.deleteCard()
        }
    }
    searchTermUpdated(event) {
        const { ygoEditorModel } = this.props.params
        ygoEditorModel.updateSearchTerm(event.target.value)
    }
}