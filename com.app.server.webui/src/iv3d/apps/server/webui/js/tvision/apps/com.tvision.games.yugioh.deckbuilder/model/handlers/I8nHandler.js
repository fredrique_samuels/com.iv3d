import StateHandler from "./StateHandler";

import messages_en from './../../i8n/messages_en.json'

export default class I8nHandler extends StateHandler {
    constructor(ygoEditorModel) {
        super(ygoEditorModel)
    }

    getMessage(key) {
        const msg = messages_en.messages[key]
        if(msg) {
            return msg
        }
        console.error("I8n message not found for key:", key)
        return key
    }
}