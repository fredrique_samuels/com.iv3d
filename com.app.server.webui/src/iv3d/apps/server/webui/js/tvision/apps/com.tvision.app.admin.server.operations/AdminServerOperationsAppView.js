import React from 'react'
import {IdGenerator} from "../../../common"
import {Timer} from "../../../common";
import LoadingDialogCloser from "../../../framework/form/loading/LoadingDialogCloser";

class DemoLoadingController {
    constructor() {

    }

    start(loadingModel) {
        const loadingDialogCloser = new LoadingDialogCloser(loadingModel);
        new Timer(2500, loadingDialogCloser.close.bind(loadingDialogCloser))
    }
}


export default class AdminServerOperationsAppView extends React.Component {
    constructor() {
        super()
        this.widgetId = IdGenerator.generateGlobalId()
    }

    render() {
        return <div class="fill-parent">
            {this.props.uicontext.layerComponentManager().createLayerComponent(this.widgetId, this.onLayerManagerReady.bind(this))}
        </div>
    }

    onLayerManagerReady() {
        const uicontext = this.props.uicontext.setLayerComponentModelId(this.widgetId)
        uicontext.formComponentManager().createLayeredSpinLoader(new DemoLoadingController())
    }
}