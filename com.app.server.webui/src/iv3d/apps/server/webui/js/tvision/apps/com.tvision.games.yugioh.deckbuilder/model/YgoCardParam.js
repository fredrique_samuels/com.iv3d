


class NameTransformer {get(data){return data.nameEnglish}}

export const CARD_TYPE_SPELL = 'Spell'
export const CARD_TYPE_TRAP = 'Trap'
export const CARD_TYPE_MONSTER = 'Monster'
class CardTypeTransformer {
    get(data){
        return data.cardType
    }
}


export const TYPE_SPELLCASTER = "Spellcaster"
export const TYPE_DRAGON = "Dragon"
export const TYPE_ZOMBIE = "Zombie"
export const TYPE_WARRIOR = "Warrior"
export const TYPE_BEAST_WARRIOR = "Beast-Warrior"
export const TYPE_BEAST = "Beast"
export const TYPE_WINGED_BEAST = "Winged Beast"
export const TYPE_FIEND = "Fiend"
export const TYPE_FAIRY = "Fairy"
export const TYPE_INSECT = "Insect"
export const TYPE_DINOSAUR = "Dinosaur"
export const TYPE_REPTILE = "Reptile"
export const TYPE_FISH = "Fish"
export const TYPE_SEA_SERPENT = "Sea Serpent"
export const TYPE_AQUA = "Aqua"
export const TYPE_PYRO = "Pyro"
export const TYPE_THUNDER = "Thunder"
export const TYPE_ROCK = "Rock"
export const TYPE_PLANT = "Plant"
export const TYPE_MACHINE = "Machine"
export const TYPE_PSYCHIC = "Psychic"
export const TYPE_DIVINE_BEAST = "Divine-Beast"
export const TYPE_WYRM = "Wyrm"
export const TYPE_CYBERSE = "Cyberse"

export const TYPE_NORMAL = "Normal"
export const TYPE_EFFECT = "Effect"
export const TYPE_RITUAL = "Ritual"
export const TYPE_FUSION = "Fusion"
export const TYPE_SYNCHRO = "Synchro"
export const TYPE_XYZ = "Xyz"
export const TYPE_PENDULUM = "Pendulum"
export const TYPE_LINK = "Link"
export const TYPE_TOON = "Toon"
export const TYPE_SPIRIT = "Spirit"
export const TYPE_UNION = "Union"
export const TYPE_GEMINI = "Gemini"
export const TYPE_TUNER = "Tuner"
export const TYPE_FLIP = "Flip"

class TypesTransformer {
    get(data){
        if(data.types) {
            if (data.types.includes("/")) {
                return data.types.split("/").map(s => s.trim())
            }
            return [data.types]
        }
        return []
    }
}

class LoreTransformer {
    get(data){
        if(data.loreEnglish) {
            return data.loreEnglish
        }
        return ""
    }
}

export const PROPERTY_NORMAL="Normal"
export const PROPERTY_COUNTER="Counter"
export const PROPERTY_QUICK_PLAY="Quick-Play"
export const PROPERTY_RITUAL="Ritual"
export const PROPERTY_EQUIP="Equip"
export const PROPERTY_FIELD="Field"
export const PROPERTY_CONTINUOUS="Continuous"

class PropertyTransformer {
    get(data) {
        if(data.property) {
            return data.property
        }
        return ""
    }

}

export const ATTRIBUTE_WIND = 'WIND'
export const ATTRIBUTE_WATER = 'WATER'
export const ATTRIBUTE_FIRE = 'FIRE'
export const ATTRIBUTE_LIGHT = 'LIGHT'
export const ATTRIBUTE_DARK = 'DARK'
export const ATTRIBUTE_EARTH = 'EARTH'
export const ATTRIBUTE_DIVINE = 'DIVINE'
class AttributeTransformer {
    get(data) {
        if(data.attribute) {
            return data.attribute
        }
        return ""
    }
}

class StarsTransformer {
    get(data) {
        if(data.level) {
            return data.level
        }
        if(data.rank) {
            return data.rank
        }
        return null
    }
}

class AtkTransformer {
    get(data) {
        if(data.atkdef) {
            return data.atkdef.split("/").map(s => s.trim())[0]
        }
        if(data.atklink) {
            return data.atklink.split("/").map(s => s.trim())[0]
        }
        return null
    }
}

class DefTransformer {
    get(data) {
        if(data.atkdef) {
            return data.atkdef.split("/").map(s => s.trim())[1]
        }
        return null
    }
}

class ScaleTransformer {
    get(data) {
        if(data.scale) {
            return data.scale
        }
        return null
    }
}

class LinkTransformer {
    get(data) {
        if(data.atklink) {
            return data.atklink.split("/").map(s => s.trim())[1]
        }
        return null
    }
}

class ImageUrlTransformer {
    get(data) {
        if(data.imageUrl) {
            return data.imageUrl
        }
        return null
    }
}

class ImageKeyTransformer {
    get(data) {
        if(data.imageKey) {
            return data.imageKey
        }
        return null
    }
}


class PasscodeTransformer {
    get(data) {
        if(data.passcode) {
            return data.passcode
        }
        return null
    }
}



export const LINK_MARKER_LEFT = "Left"
export const LINK_MARKER_TOP = "Top"
export const LINK_MARKER_BOTTOM = "Bottom"
export const LINK_MARKER_RIGHT = "Right"
export const LINK_MARKER_BOTTOM_RIGHT = "Bottom-Right"
export const LINK_MARKER_BOTTOM_LEFT = "Bottom-Left"
export const LINK_MARKER_TOP_LEFT = "Top-Left"
export const LINK_MARKER_TOP_RIGHT = "Top-Right"


class LinkMarkersTransformer{
    get(data) {
        if(data.linkMarkers) {
            if (data.linkMarkers.includes(",")) {
                return data.linkMarkers.split(",").map(s => s.trim())
            }
            return [data.linkMarkers]
        }
        return []
    }
}

export default class YgoCardParam {
    constructor(cardData) {
        this.cardData = cardData?cardData:{"nameEnglish":"Alexandrite Dragon","nameFrench":"Dragon Alexandrite","nameItalian":"Drago di Alexandrite","nameGerman":"Alexandrit-Drache","nameKorean":"알렉산드라이트 드래곤","namePortuguese":"Dragão de Alexandrita","nameSpanish":"Dragón de Alexandrita","nameJapaneseKana":null,"nameJapaneseBase":null,"nameJapaneseRomaji":"Arekisandoraidoragon","loreEnglish":"Many of the czars' lost jewels can be found in the scales of this priceless dragon. Its creator remains a mystery, along with how they acquired the imperial treasures. But whosoever finds this dragon has hit the jackpot... whether they know it or not.","cardType":"Monster","property":null,"passcode":"43096270","loreFrench":"Nombreux des joyaux disparus du tsar se retrouvent dans les écailles de cet inestimable dragon. Personne ne connaît son créateur ni comment celui-ci a acquis ces trésors impériaux. Mais une chose est sûre, quiconque trouvera ce monstre mettra la main sur le gros lot... qu'il le sache ou non.","loreGerman":"Viele der verschwundenen Edelsteine des Zaren finden sich in den Schuppen dieses unschätzbar wertvollen Drachen wieder. Sein Schöpfer und wie dieser zu den Schätzen des Zaren gekommen ist, bleiben ein Geheimnis. Aber wer auch immer diesen Drachen findet, hat den Jackpot geknackt ... ob er dessen gewahr ist oder auch nicht.","loreItalian":"Molti dei gioielli perduti degli zar si possono trovare nelle scaglie di questo inestimabile drago. Il suo creatore, e come abbia fatto ad entrare in possesso del tesoro imperiale, rimangono un segreto. Ma chiunque trovi questo drago ha vinto la lotteria… che lo sappia o meno.","lorePortuguese":"Muitas das joias perdidas dos czares podem ser encontradas nas escamas desse precioso dragão. Seu criadores continuam um mistério, assim como a forma como eles adquiriram os tesouros imperiais. Mas quem quer que encontre esse dragão terá encontrado o pote de ouro… sabendo disso ou não.","loreSpanish":"Muchas de las joyas perdidas del Zar pueden encontrarse en las escamas de este dragón invaluable. Su creador permanece en el misterio, así como la forma en la que adquirió los tesoros imperiales. Pero quienquiera que encuentra a este dragón, se ha ganado un premio... lo sepa o no.","loreJapanese":"アレキサンドライトのウロコを持った、非常に珍しいドラゴン。その美しいウロコは古の王の名を冠し、神秘の象徴とされる。 ――それを手にした者は大いなる幸運を既につかんでいる事に気づいていない。","loreKorean":"알렉산드라이트의 비늘을 가진 상당히 진귀한 드래곤. 그 아름다운 비늘은 고대 왕의 이름을 받아, 신비의 상징이 된다. - 그것을 손에 넣은 자는 거대한 행운을 이미 거머쥐었어도 그 사실을 깨닫지 못한다.","attribute":"LIGHT","types":null,"rank":null,"level":"4","atkdef":"2000 / 100","materials":null}
    }

    getId(){return this.getName()}

    getStars(){return new StarsTransformer().get(this.cardData)}
    getName(){return new NameTransformer().get(this.cardData)}
    getAttribute(){return new AttributeTransformer().get(this.cardData)}
    getLore(){return new LoreTransformer().get(this.cardData)}
    getAtk(){return new AtkTransformer().get(this.cardData)}
    getDef(){return  new DefTransformer().get(this.cardData)}
    getLink(){return  new LinkTransformer().get(this.cardData)}
    getScale(){return  new ScaleTransformer().get(this.cardData)}
    getTypes(){return new TypesTransformer().get(this.cardData)}
    getProperty(){return new PropertyTransformer().get(this.cardData)}
    getLinkMarkers(){return new LinkMarkersTransformer().get(this.cardData)}
    getPasscode(){return new PasscodeTransformer().get(this.cardData)}

    getImageUrl(){return new ImageUrlTransformer().get(this.cardData)}
    getImageKey(){return new ImageKeyTransformer().get(this.cardData)}

    hasMarker(m){return this.getLinkMarkers().indexOf(m)>-1}
    hasLeftLinkMarker(){return this.hasMarker(LINK_MARKER_LEFT)}
    hasRightLinkMarker(){return this.hasMarker(LINK_MARKER_RIGHT)}
    hasTopLinkMarker(){return this.hasMarker(LINK_MARKER_TOP)}
    hasTopLeftLinkMarker(){return this.hasMarker(LINK_MARKER_TOP_LEFT)}
    hasTopRightLinkMarker(){return this.hasMarker(LINK_MARKER_TOP_RIGHT)}
    hasBottomLinkMarker(){return this.hasMarker(LINK_MARKER_BOTTOM)}
    hasBottomLeftLinkMarker(){return this.hasMarker(LINK_MARKER_BOTTOM_LEFT)}
    hasBottomRightLinkMarker(){return this.hasMarker(LINK_MARKER_BOTTOM_RIGHT)}

    getCardType(){return new CardTypeTransformer().get(this.cardData)}
    isTrap(){return this.getCardType()==CARD_TYPE_TRAP}
    isSpell(){return this.getCardType()==CARD_TYPE_SPELL}
    isMonster(){return this.getCardType()==CARD_TYPE_MONSTER}

    isSynchroMonster(){return this.isType(TYPE_SYNCHRO)}
    isXyzMonster(){return this.isType(TYPE_XYZ)}
    isLinkMonster(){return this.isType(TYPE_LINK)}
    isFusionMonster(){return this.isType(TYPE_FUSION)}
    isRitualMonster(){return this.isType(TYPE_RITUAL)}
    isEffectMonster(){return this.isType(TYPE_EFFECT)}
    isNormalMonster(){return this.isMonster() && !(this.isEffectMonster() || this.isRitualMonster() || this.isFusionMonster() || this.isXyzMonster() || this.isSynchroMonster() || this.isLinkMonster() )}
    isPendulumMonster(){return this.isType(TYPE_PENDULUM)}
    isExtraDeckCard() {return this.isLinkMonster() || this.isFusionMonster() || this.isXyzMonster() || this.isSynchroMonster() }

    isType(t){return this.getTypes().indexOf(t)>-1}

    isDarkAttribute(){return this.getAttribute()==ATTRIBUTE_DARK}
    isLightAttribute(){return this.getAttribute()==ATTRIBUTE_LIGHT}
    isWindAttribute(){return this.getAttribute()==ATTRIBUTE_WIND}
    isWaterAttribute(){return this.getAttribute()==ATTRIBUTE_WATER}
    isFireAttribute(){return this.getAttribute()==ATTRIBUTE_FIRE}
    isEarthAttribute(){return this.getAttribute()==ATTRIBUTE_EARTH}
    isDivineAttribute(){return this.getAttribute()==ATTRIBUTE_DIVINE}


    isDragonType(){return this.isType(TYPE_DRAGON)}
    isSpellcasterType(){return this.isType(TYPE_SPELLCASTER)}
    isZombieType(){return this.isType(TYPE_ZOMBIE)}
    isWarriorType(){return this.isType(TYPE_WARRIOR)}
    isBeastWarriorType(){return this.isType(TYPE_BEAST_WARRIOR)}
    isBeastType(){return this.isType(TYPE_BEAST)}
    isWingedBeastType(){return this.isType(TYPE_WINGED_BEAST)}
    isFiendType(){return this.isType(TYPE_FIEND)}
    isFairyType(){return this.isType(TYPE_FAIRY)}
    isInsectType(){return this.isType(TYPE_INSECT)}
    isDinosaurType(){return this.isType(TYPE_DINOSAUR)}
    isReptileType(){return this.isType(TYPE_REPTILE)}
    isFishType(){return this.isType(TYPE_FISH)}
    isSeaSerpentType(){return this.isType(TYPE_SEA_SERPENT)}
    isAquaType(){return this.isType(TYPE_AQUA)}
    isPyroType(){return this.isType(TYPE_PYRO)}
    isThunderType(){return this.isType(TYPE_THUNDER)}
    isPlantType(){return this.isType(TYPE_PLANT)}
    isRockType(){return this.isType(TYPE_ROCK)}
    isMachineType(){return this.isType(TYPE_MACHINE)}
    isPsychicType(){return this.isType(TYPE_PSYCHIC)}
    isDivineBeastType(){return this.isType(TYPE_DIVINE_BEAST)}
    isWyrmType(){return this.isType(TYPE_WYRM)}
    isCyberseType(){return this.isType(TYPE_CYBERSE)}

    isEquipCard(){return this.getProperty()==PROPERTY_EQUIP}
    isContinuousCard(){return this.getProperty()==PROPERTY_CONTINUOUS}
    isFieldCard(){return this.getProperty()==PROPERTY_FIELD}
    isRitualCard(){return this.getProperty()==PROPERTY_RITUAL}
    isQuickPlayCard(){return this.getProperty()==PROPERTY_QUICK_PLAY}
    isCounterCard(){return this.getProperty()==PROPERTY_COUNTER}
    isNormalCard(){return this.getProperty()==PROPERTY_NORMAL}

    isToonMonster(){return this.isType(TYPE_TOON)}
    isSpiritMonster(){return this.isType(TYPE_SPIRIT)}
    isUnionMonster(){return this.isType(TYPE_UNION)}
    isGeminiMonster(){return this.isType(TYPE_GEMINI)}
    isTunerMonster(){return this.isType(TYPE_TUNER)}
    isFlipMonster(){return this.isType(TYPE_FLIP)}

    //summary getters
    getTypeSummaryString() {
        if(this.isSpell() || this.isTrap()) {
            return this.getCardType() + "/" + this.getProperty()
        }

        const attr = this.getAttribute()
        const types = this.getTypes()
        let typesString = attr
        for (let tIndex in types) {
          typesString+="/"+types[tIndex]
        }
        return typesString
    }

    getAtkDefSummaryString() {
        const atk = this.getAtk()
        if(atk) return atk + "/" + (this.isLinkMonster()?this.getLink():this.getDef())
        return null
    }

}
