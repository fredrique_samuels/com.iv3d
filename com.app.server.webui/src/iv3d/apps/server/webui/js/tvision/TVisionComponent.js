import React from 'react'
import { connect } from 'react-redux'

import UiContext from '../uicontext'
import LayerComponent from '../framework/LayerComponent'

import * as ComponentModels from "./../framework/ComponentModels"
import { TVisionModelFactory } from './TVisionManager'

import AppSection from './AppsView'
import AppMenu from './menu/AppMenu'

const TT_LOGO = "/res/images/tt_logo.svg"

const MAIN_LAYER_COMPONENT = "component.model.layermanager.main"
const APP_LAYER_COMPONENT = "component.model.layermanager.apps"

class LogoBox extends React.Component {
  render() {
    return (
      <div class="inline-element" style={{width:"65px", height:"65px", boxSizing:"borderBox"}}>
        <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <a>
              <image x="0" y="0" height="100%" width="100%" xlinkHref="/res/images/tt_logo.svg" />
            </a>
        </svg>
      </div>
    )
  }
}

class UserProfilePic extends React.Component {
  render() {
    return (
      <div class="inline-element" style={{width:"65px", height:"65px", boxSizing:"borderBox"}}>
        <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <a>
              <image x="0" y="0" height="100%" width="100%" xlinkHref="/res/images/icons/user.png" />
            </a>
        </svg>
      </div>
    )
  }
}

class UserNameContainer extends React.Component {
  render() {

    const style = {minWidth:"125px",
      height:"25px",
      borderRadius:"2px",
      paddingTop:"2px",
      paddingLeft:"4px",
      paddingRight:"4px",
      textAlign:"center"
    }

    return (
      <div class="color-primary" style={style}>
      </div>
    )
  }
}

class UserCard extends React.Component {
  render() {
    return (
      <div class="back-panel inline-element" style={{minWidth:"200px", height:"75px", padding:"5px",borderRadius:"0 0 5px 0", boxSizing:"borderBox", position:"absolute", top:"0px", left:"0px"}} >
        <UserProfilePic />
        <div class="inline-element has-text" style={{minWidth:"125px", height:"65px"}}>
          {/*<UserNameContainer />*/}
        </div>
      </div>
    )
  }
}

class CompanyCard extends React.Component {
  render() {
    return (
      <div class="back-panel inline-element" style={{width:"75px", height:"75px", padding:"5px",borderRadius:"0 0 0 5px", boxSizing:"borderBox", position:"absolute", top:"0px", right:"0px"}} >
        <LogoBox />
      </div>
    )
  }
}

class StatusBar extends React.Component {
  render() {
    return (
      <div class="back-panel inline-element" style={{width:"100%", height:"50px"}} >
      </div>
    )
  }
}

class NavBar extends React.Component {
  render() {
    const style={
      width:"60px",
      height:"100%",
      padding:"10px",
      boxSizing:"border-box",
    }

    return (
      <div class="back-panel inline-element" style={style} >
        {this.createButton(this.setHome.bind(this), this.props.uicontext.tvisionManager().inDisplayModeHome(), "fa-home")}
        {this.createButton(this.setMenu.bind(this), this.props.uicontext.tvisionManager().inDisplayModeMenu(), "fa-th")}
        {this.createButton(this.setAppList.bind(this), this.props.uicontext.tvisionManager().inDisplayModeAppList(), "fa-window-restore")}
      </div>
    )
  }

  createButton(action, active, icon){
    const buttonClassValues = "tvision-nav-menu-button"+ (active ? " color-primary" : " tvision-nav-menu-button-default")
    const iconClassValues = "fa " + icon + " fa-2x"
    return <button onClick={action} class={buttonClassValues} ><i class={iconClassValues}></i></button>
  }

  setHome(){this.props.uicontext.tvisionManager().setDisplayModeHome()}
  setMenu(){this.props.uicontext.tvisionManager().setDisplayModeMenu()}
  setAppList(){this.props.uicontext.tvisionManager().setDisplayModeAppList()}
}



class ContentContainer extends React.Component {
  render() {
    const style={
      width:"calc(100% - 90px)",
      height:"100%",
      boxSizing:"border-box",
      marginLeft:"10px",
      border:"1px solid rgba(0,0,0,.2)"
    }

    const apps = (
      <div class="fill-parent">
        <LayerComponent modelId={MAIN_LAYER_COMPONENT} mountedCallback={this.onLayerMounted.bind(this)}/>
      </div>
    )

    return (
      <div class="inline-element" style={style} >
        {this.props.uicontext.tvisionManager().inDisplayModeMenu()?<AppMenu uicontext={this.props.uicontext}/>:null}
        <AppSection uicontext={this.props.uicontext} />
      </div>
    )
  }

  renderApps() {
    const appMode = this.props.uicontext.tvisionManager().inDisplayModeApp()
    const appListMode = this.props.uicontext.tvisionManager().inDisplayModeApp()
    if (appMode || appListMode )
      return <AppsView uicontext={this.props.uicontext} />
    return null
  }

  onLayerMounted(id){}
}

class MainContent extends React.Component {
  render() {
    const css = {
      padding:"10px",
      boxSizing:"border-box",
      width:"100%",
      height:"calc(100% - 100px)",
      marginTop:"35px"
    }

    return (
      <div style={css}>
        <NavBar uicontext={this.props.uicontext} />
        <ContentContainer uicontext={this.props.uicontext} />
      </div>
    )
  }
}

@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export default class TVisionComponent extends React.Component {
  render() {
    this.uicontext = this.uicontext.setProperties({tvisionModel:this.cmg.getModel(this)})

    return (<div class=" back-panel fill-parent" style={{position:"relative"}}>
      <UserCard />
      <CompanyCard />
      <StatusBar />
      <MainContent uicontext={this.uicontext} />
    </div>)
  }

  constructor(){
      super()
      this.cmg = new ComponentModels.ComponentModelGateway({
          modelFactory:new TVisionModelFactory()
      })
      this.uicontext = new UiContext()
        .setProperties({tvisionCmg:this.cmg})
  }

  componentWillMount() {
      this.cmg.createModel(this)
  }

  componentDidMount() {
  }

}
