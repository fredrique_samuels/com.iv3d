import StateHandler from "./StateHandler";


export default class PreviewCardHandler extends StateHandler {
    constructor(model) {
        super(model)
    }

    setPreviewCard(card){this.updateState({previewCard:card})}
    getPreviewCard(){return this.getState().previewCard?this.getState().previewCard:null}
    getPreviewCardName(){return this.getState().previewCard?this.getState().previewCard.getName():""}
    getPreviewCardLore(){return this.getState().previewCard?this.getState().previewCard.getLore():""}

    getDefaults() {
        return {previewCard:null}
    }
}