import React from 'react'
import StyleCache from './styles/StyleCache'

export default class AbstractViewContainer extends React.Component {
  render() {
    const { hidden } = this.props
    const style=new StyleCache().get('abstract-view-container', hidden?{display:"none"}:{})
    return <div class="inline-element" style={style}>
      {this.props.children}
    </div>
  }
}
