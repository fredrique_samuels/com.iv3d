import React from 'react'

class MenuItemFaIcon extends React.Component {
    render() {
        return (<div class="center-content-vh hover-shadow" style={{position:"relative",width:"100%", height:"100%",  borderRadius:"5px", border:"1px solid black"}}>
            <i onClick={this.props.action} class={"fa "+this.props.graphic+" fa-5x"}></i>
        </div>)
    }
}

class MenuItemImageIcon extends React.Component {
    render() {
        return <div class="center-content-vh hover-shadow" onClick={this.props.action} style={{width:"100%", height:"100%",borderRadius:"5px"}}>
            <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                <a>
                    <image x="0" y="0" height="100%" width="100%" xlinkHref={this.props.graphic} />
                </a>
            </svg>
        </div>
    }
}

export default class MenuItemIcon extends React.Component {
    render() {

        const {icon, action } = this.props
        if (icon) {
            if(icon.type=='icon.type.fontawesome')
                return <MenuItemFaIcon graphic={icon.graphic} action={action} />
            else if(icon.type=='icon.type.image') {
                return <MenuItemImageIcon graphic={icon.graphic} action={action} />
            }
        }
        return null
    }
}