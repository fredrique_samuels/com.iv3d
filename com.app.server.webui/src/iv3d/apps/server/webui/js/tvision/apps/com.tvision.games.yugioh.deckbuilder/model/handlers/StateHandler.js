

export default class StateHandler {
    constructor(model) {
        this.model = model
    }
    updateState(stateChanges) {
        this.model.updateState(stateChanges)
    }

    getState() {return this.model.getState()}
    getUiContext(){return this.model.getUiContext()}
    getModel(){return this.model}

    getDefaults() {return {}}
    loadStartupData(doneCallBack){}

    getCardId(card) {return this.model.getCardId(card)}
    getCardById(cardId){return this.model.getCardById(cardId)}
    sortCards(cards){return this.model.sortCards(cards)}
}