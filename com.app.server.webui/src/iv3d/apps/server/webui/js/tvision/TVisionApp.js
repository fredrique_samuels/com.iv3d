import React from 'react'

export default class TVisionApp {
  constructor(uicontext, params={}) {
    this.uicontext = uicontext
    this.appParams = this.applyDefaults(params)
  }

  createView() {
    const { viewFactory } = this.appParams
    if(viewFactory)
      return viewFactory.createView(this.uicontext, this)

    const { title } = this.appParams
    return <div class="fill-parent" style={{border:"1px solid black", backgroundColor:"slategrey"}}>
        {title}
      </div>
  }

  getPackageId() {return this.appParams.packageId}
  getTitle() {return this.appParams.title}
  getIcon() {return this.appParams.icon}
  singelInstaneOnly(){return this.appParams.singleInstanceOnly}

  getSettings() {
    const { title, icon } = this.appParams
    return {
      title : title,
      icon : icon
    }
  }

  applyDefaults(params) {
    const defaultValues = {
      title:'No Title',
      packageId:null,
      singleInstanceOnly:true,
      icon:null
    }
    return Object.assign({}, defaultValues, params)
  }

  open() {
    this.uicontext.tvisionManager().createAppInstance(this)
  }

    close() {
        this.uicontext.tvisionManager().createAppInstance(this)
    }
}
