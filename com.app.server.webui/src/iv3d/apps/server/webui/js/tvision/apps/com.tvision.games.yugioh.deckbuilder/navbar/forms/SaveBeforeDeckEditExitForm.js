import {FORM_DISPLAY_TYPE_BLOCK_TEXT} from "../../../../../framework/form/FormConstants";

export default class SaveBeforeDeckEditExitForm {

    constructor(ygoEditorModel) {
        this.ygoEditorModel = ygoEditorModel
    }
    getView() {
        const formDescription = {
            actions : [
                {
                    title : this.ygoEditorModel.getI8nMessage('ygo.edit.savebeforeexit.cancel'),
                    type : "form.action.type.cancel"
                },
                {
                    title : this.ygoEditorModel.getI8nMessage('ygo.edit.savebeforeexit.discard'),
                    type : "form.action.type.action",
                    action :this.discardAndContinue.bind(this)
                }
            ],
            inputs : [
                {
                    type : FORM_DISPLAY_TYPE_BLOCK_TEXT,
                    text : this.ygoEditorModel.getI8nMessage('ygo.edit.cleardeck.notice')
                }
            ]
        }

        return formDescription
    }
    discardAndContinue(event, form) {
        form.close()
        this.ygoEditorModel.setDeckSelectMode()
    }
}
