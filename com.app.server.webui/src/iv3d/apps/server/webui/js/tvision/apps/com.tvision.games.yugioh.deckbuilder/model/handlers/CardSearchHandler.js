import Immutable from 'immutable'
import StateHandler from "./StateHandler";
import DemoCardData from '../../demo/DemoCardData'

import * as YgoCardConstants from '../YgoCardParam'
import * as YgoEditorModelConstants from '../YgoEditorModelConstants'

export default class CardSearchHandler extends StateHandler {
    constructor(model) {
        super(model)
    }
    getCardId(card) {return card.getId()}
    getCardById(cardId) {return this.getState().cards.filter(c => c.getName()==cardId)[0]}
    getSearchResultCards(){return this.getState().searchResults}
    toggleFilter(id, params={}){
        const state = this.getState();
        const filter = state.searchFilters.get(id)
        const newFilter = Object.assign({}, filter, {active:!filter.active})
        const newFilterList = state.searchFilters.set(id, newFilter)
        const stateChanges = Object.assign({}, {
            searchResults:this.getSearchResults(Object.assign({}, this.getState(), {searchFilters:newFilterList})),
            searchFilters: newFilterList
        }, params)
        this.updateState(stateChanges)
    }
    updateNumberStateFilter(filterId, value) {
        const state = this.getState();
        const filter = state.searchFilters.get(filterId)

        const newFilter = Object.assign({}, filter, {active:value?true:false})
        const newFilterList = state.searchFilters.set(filterId, newFilter)
        const newResults = this.getSearchResults(Object.assign(this.getState(), filter.stateChange(value), {searchFilters:newFilterList}))
        const stateChanges = Object.assign({}, {
            searchResults:newResults,
            searchFilters: newFilterList
        }, filter.stateChange(value))
        this.updateState(stateChanges)
    }
    getFilterNumberValue(filterId) {
        const state = this.getState();
        const filter = state.searchFilters.get(filterId)
        console.log('getFilterNumberValue',filterId,  filter)
        return filter.getValue(state)
    }
    isFilterActive(id){return this.getState().searchFilters.get(id).active}

    updateSearchTerm(term) {
        this.updateState({
            searchResults:this.getSearchResults(Object.assign({}, this.getState(), {searchTerm:term})),
            searchTerm:term
        })
    }
    filterWithState(cards, filterFunc) {
        let results = []
        const state = this.getState()
        for (let c in cards) {
            if (filterFunc(cards[c], state)) {
                results.push(cards[c])
            }
        }
        return results
    }
    getSearchResults(state) {
        const searchFilters = state.searchFilters;

        if(this.getModel().inAdvancedSearchMode()) {
            let results = state.cards

            const activeFilters = searchFilters.toArray().filter(f => f.active)
            for( let fIndex in activeFilters) {
                const filterInstance = activeFilters[fIndex]
                if(filterInstance.requiresState) {
                    results = this.filterWithState(results, filterInstance.filterFunc)
                } else {
                    results = results.filter(filterInstance.filterFunc)
                }
            }

            if(!(!state.searchTerm || state.searchTerm=="" || state.searchTerm.length<3)) {
                results = results.filter(c => c.getName().toLowerCase().includes(state.searchTerm.toLowerCase()))
            }
            return this.sortCards(results)
        }

        if(!state.searchTerm || state.searchTerm=="" || state.searchTerm.length<3) {
            return []
        }

        const cards = this.getState().cards
        let results = cards.filter(c => c.getName().toLowerCase().includes(state.searchTerm.toLowerCase()))
        return this.sortCards(results)
    }

    loadStartupData(onDone) {
        this.updateState({cards:new DemoCardData().get()})
        if(onDone) {
            onDone({cards:new DemoCardData().get()})
        }
    }

    getDefaults() {
        return {
            cards:[],
            searchResults:[],
            searchFilters: this.getSearchFilters(),
            searchTerm:"",
            minAtk:null,
            maxAtk:null,
            minDef:null,
            maxDef:null,
            minStars:null,
            maxStars:null,
            minLink:null,
            maxLink:null,
            minScale:null,
            maxScale:null
        }
    }

    getSearchFilters() {
        let searchFilters = new Immutable.Map()
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_DARK, {active:false, filterFunc: c => c.isDarkAttribute() })
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_LIGHT, {active:false, filterFunc: c => c.isLightAttribute() })
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_WATER, {active:false, filterFunc: c => c.isWaterAttribute() })
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_FIRE, {active:false, filterFunc: c => c.isFireAttribute() })
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_WIND, {active:false, filterFunc: c => c.isWindAttribute() })
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_EARTH, {active:false, filterFunc: c => c.isEarthAttribute() })
        searchFilters = searchFilters.set(YgoCardConstants.ATTRIBUTE_DIVINE, {active:false, filterFunc: c => c.isDivineAttribute() })


        searchFilters = searchFilters.set(YgoCardConstants.TYPE_DRAGON, {active:false, filterFunc: c => c.isDragonType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_SPELLCASTER, {active:false, filterFunc: c => c.isSpellcasterType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_ZOMBIE, {active:false, filterFunc: c => c.isZombieType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_WARRIOR, {active:false, filterFunc: c => c.isWarriorType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_BEAST_WARRIOR, {active:false, filterFunc: c => c.isBeastWarriorType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_BEAST, {active:false, filterFunc: c => c.isBeastType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_WINGED_BEAST, {active:false, filterFunc: c => c.isWingedBeastType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_FIEND, {active:false, filterFunc: c => c.isFiendType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_FAIRY, {active:false, filterFunc: c => c.isFairyType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_INSECT, {active:false, filterFunc: c => c.isInsectType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_DINOSAUR, {active:false, filterFunc: c => c.isDinosaurType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_REPTILE, {active:false, filterFunc: c => c.isReptileType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_FISH, {active:false, filterFunc: c => c.isFishType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_SEA_SERPENT, {active:false, filterFunc: c => c.isSeaSerpentType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_AQUA, {active:false, filterFunc: c => c.isAquaType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_PYRO, {active:false, filterFunc: c => c.isPyroType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_THUNDER, {active:false, filterFunc: c => c.isThunderType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_PLANT, {active:false, filterFunc: c => c.isPlantType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_ROCK, {active:false, filterFunc: c => c.isRockType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_MACHINE, {active:false, filterFunc: c => c.isMachineType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_PSYCHIC, {active:false, filterFunc: c => c.isPsychicType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_DIVINE_BEAST, {active:false, filterFunc: c => c.isDivineBeastType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_WYRM, {active:false, filterFunc: c => c.isWyrmType() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_CYBERSE, {active:false, filterFunc: c => c.isCyberseType() })

        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_EQUIP, {active:false, filterFunc: c => c.isEquipCard() })
        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_CONTINUOUS, {active:false, filterFunc: c => c.isContinuousCard() })
        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_FIELD, {active:false, filterFunc: c => c.isFieldCard() })
        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_RITUAL+"Spell", {active:false, filterFunc: c => c.isRitualCard() })
        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_QUICK_PLAY, {active:false, filterFunc: c => c.isQuickPlayCard() })
        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_COUNTER, {active:false, filterFunc: c => c.isCounterCard() })
        searchFilters = searchFilters.set(YgoCardConstants.PROPERTY_NORMAL+"Card", {active:false, filterFunc: c => c.isNormalCard() })

        searchFilters = searchFilters.set(YgoCardConstants.TYPE_NORMAL, {active:false, filterFunc: c => c.isNormalMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_EFFECT, {active:false, filterFunc: c => c.isEffectMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_RITUAL, {active:false, filterFunc: c => c.isRitualMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_FUSION, {active:false, filterFunc: c => c.isFusionMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_SYNCHRO, {active:false, filterFunc: c => c.isSynchroMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_XYZ, {active:false, filterFunc: c => c.isXyzMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_PENDULUM, {active:false, filterFunc: c => c.isPendulumMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_LINK, {active:false, filterFunc: c => c.isLinkMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_TOON, {active:false, filterFunc: c => c.isToonMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_SPIRIT, {active:false, filterFunc: c => c.isSpiritMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_UNION, {active:false, filterFunc: c => c.isUnionMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_GEMINI, {active:false, filterFunc: c => c.isGeminiMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_TUNER, {active:false, filterFunc: c => c.isTunerMonster() })
        searchFilters = searchFilters.set(YgoCardConstants.TYPE_FLIP, {active:false, filterFunc: c => c.isFlipMonster() })

        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_TOP, {active:false, filterFunc: c => c.hasTopLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_LEFT, {active:false, filterFunc: c => c.hasLeftLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_RIGHT, {active:false, filterFunc: c => c.hasRightLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_BOTTOM, {active:false, filterFunc: c => c.hasBottomLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_BOTTOM_LEFT, {active:false, filterFunc: c => c.hasBottomLeftLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_BOTTOM_RIGHT, {active:false, filterFunc: c => c.hasBottomRightLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_TOP_RIGHT, {active:false, filterFunc: c => c.hasTopRightLinkMarker() })
        searchFilters = searchFilters.set(YgoCardConstants.LINK_MARKER_TOP_LEFT, {active:false, filterFunc: c => c.hasTopLeftLinkMarker() })

        searchFilters = searchFilters.set(YgoEditorModelConstants.MIN_ATK_FILTER, {active:false, stateChange: v => {return {minAtk:v}}, getValue: s => s.minAtk, requiresState:true, filterFunc: function(c, s){ return c.getAtk() && s.minAtk ? parseInt(c.getAtk()) >= s.minAtk:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MAX_ATK_FILTER, {active:false, stateChange: v => {return {maxAtk:v}}, getValue: s => s.maxAtk, requiresState:true, filterFunc: function(c, s){ return c.getAtk() && s.maxAtk ? parseInt(c.getAtk()) <= s.maxAtk:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MIN_DEF_FILTER, {active:false, stateChange: v => {return {minDef:v}}, getValue: s => s.minDef, requiresState:true,filterFunc: function(c, s) { return  c.getDef() && s.minDef ? parseInt(c.getDef()) >= s.minDef:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MAX_DEF_FILTER, {active:false, stateChange: v => {return {maxDef:v}}, getValue: s => s.maxDef, requiresState:true,filterFunc: function(c, s) { return  c.getDef() && s.maxDef ? parseInt(c.getDef()) <= s.maxDef:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MIN_STARS_FILTER, {active:false, stateChange: v => {return {minStars:v}}, getValue: s => s.minStars, requiresState:true,filterFunc: function(c, s) { return  c.getStars() && s.minStars ? parseInt(c.getStars()) >= s.minStars:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MAX_STARS_FILTER, {active:false, stateChange: v => {return {maxStars:v}}, getValue: s => s.maxStars, requiresState:true,filterFunc: function(c, s) { return  c.getStars() && s.maxStars ? parseInt(c.getStars()) <= s.maxStars:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MIN_SCALE_FILTER, {active:false, stateChange: v => {return {minScale:v}}, getValue: s => s.minScale, requiresState:true,filterFunc: function(c, s) { return  c.getScale() && s.minScale ? parseInt(c.getScale()) >= s.minScale:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MAX_SCALE_FILTER, {active:false, stateChange: v => {return {maxScale:v}}, getValue: s => s.maxScale, requiresState:true,filterFunc: function(c, s) { return  c.getScale() && s.maxScale ? parseInt(c.getScale()) <= s.maxScale:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MIN_LINK_FILTER, {active:false, stateChange: v => {return {minLink:v}}, getValue: s => s.minLink, requiresState:true,filterFunc: function(c, s) { return  c.getLink() && s.minLink ? parseInt(c.getLink()) >= s.minLink:false } })
        searchFilters = searchFilters.set(YgoEditorModelConstants.MAX_LINK_FILTER, {active:false, stateChange: v => {return {maxLink:v}}, getValue: s => s.maxLink, requiresState:true,filterFunc: function(c, s) { return  c.getLink() && s.maxLink ? parseInt(c.getLink()) <= s.maxLink:false } })
        return searchFilters
    }
}