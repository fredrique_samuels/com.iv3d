import React from 'react'
import MenuItemIcon from './MenuItemIcon'

export default class MenuItem extends React.Component {
    render() {
        const { app } = this.props
        return (
            <div class="center-content-h inline-element" style={{width:"180px", height:"160px", margin:"10px"}}>
                <div style={{position:"relative",left:"40px", width:"100px", height:"100px"}}>
                    <MenuItemIcon icon={app.getIcon()} action={this.openApp.bind(this)} />
                </div>
                <div class="center-content-h" style={{color:"white", paddingTop:"3px", height:"60px", width:"100%", textAlign:"center", textShadow:"0px 0px 10px black"}}>
                    {app.getTitle()}
                </div>
            </div>
        )
    }

    openApp() {
        const { app } = this.props
        app.open()
    }
}