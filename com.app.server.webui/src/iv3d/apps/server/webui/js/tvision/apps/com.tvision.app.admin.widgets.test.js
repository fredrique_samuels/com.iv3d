import React from 'react'
import TVisionApp from './../TVisionApp'
import TestAllWidgets from './../../tests/TestAll'

class ViewFactory {
    createView(uicontext, app) {
        return <TestAllWidgets />
    }
}

export default class AdminWidgetTestApp extends TVisionApp {
    constructor(uicontext) {
        super(uicontext, {
            title : "Widgets Test",
            icon : {
                    type : "icon.type.image",
                    graphic : "/res/images/icons/config.png"
                },
            packageId : "com.tvision.app.admin.widgets.test",
            viewFactory : new ViewFactory()
        })
    }
}
