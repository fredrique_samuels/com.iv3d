import $ from 'jquery'
import React from 'react'
import Immutable from 'immutable'

import TVisionApp from './../TVisionApp'

import YgoCardView from './com.tvision.games.yugioh.deckbuilder/YgoCardView'
import YgoDeckEditor from './com.tvision.games.yugioh.deckbuilder/YgoDeckEditor'
import YgoDeckEditorModel from './com.tvision.games.yugioh.deckbuilder/model/YgoDeckEditorModel'

const LOAD_STATE_DONE = 2
const LOAD_STATE_LOADING = 1
const LOAD_STATE_FAILURE = 0

class LoadingScreen extends React.Component {
  render() {
    const { loadPerc, loadState } = this.props.state

    const barStyle = {
      width: loadPerc.toFixed(2)+"%"
    }

    return (<div style={{textShadow:"0px 0px 10px black", color:"white", position:"relative", left:"10%", top:"70%", width:"80%"}} >
      <div class={"progress-meter" + (loadState==LOAD_STATE_FAILURE?" progress-meter-red":"")} style={{width:"100%", height:"30px"}}>
        <span style={barStyle} ></span>
      </div>
      Loading...
    </div>)
  }
}

class YgoDeckBuilderView extends React.Component {
  constructor() {
    super()
    // this.imageResolver = new ImageUrlResolver(this.onSuccess.bind(this), this.onFailure.bind(this))
    // this.cardDataCache = new CardDataCache(this.onSuccess.bind(this), this.onFailure.bind(this))
  }

  componentWillMount() {
    this.state = YgoDeckEditorModel.createState()
    const ygoDeckEditorModel = new YgoDeckEditorModel(this,this.props.uicontext);
    ygoDeckEditorModel.loadStartupData()
    // this.imageResolver.load()
    // this.cardDataCache.load()
  }

  render() {
    const { app } = this.props
    return (<div class="fill-parent" style={{ position:"relative", backgroundSize:"cover", backgroundRepeat: "no-repeat",backgroundImage:'url("/res/images/deckbuilder/ygo-bg.jpeg")', border:"1px solid black", backgroundColor:"slategrey"}}>
      {this.state.loadState==LOAD_STATE_DONE?<YgoDeckEditor ygoEditorModel={new YgoDeckEditorModel(this, this.props.uicontext)} uicontext={this.props.uicontext} />:<LoadingScreen state={this.state} />}
    </div>)
  }

  onSuccess(){
    if(this.state.loadState==LOAD_STATE_FAILURE) {
      return
    }

    const resourcesLoading = this.state.resourcesLoading-1
    this.setState({
      loadPerc:this.state.loadPerc+45.0,
      resourcesLoading:resourcesLoading,
      loadState:resourcesLoading==0?LOAD_STATE_DONE:LOAD_STATE_LOADING
      })
  }
  onFailure(){
    this.setState({loadState:LOAD_STATE_FAILURE})
  }
}

class YgoDeckBuilderFactory {
  createView(uicontext, app) {
    return <YgoDeckBuilderView app={app} uicontext={uicontext}/>
  }
}

export default class YgoDeckBuilderApp extends TVisionApp {
  constructor(uicontext) {
    super(uicontext, {
      title : "YGO Deck Builder",
      icon : {
        type : "icon.type.image",
        graphic : "/res/images/deckbuilder/deck_box.png"
      },
      packageId : "com.tvision.games.yugioh.deckbuilder",
      viewFactory : new YgoDeckBuilderFactory()
    })
  }
}
