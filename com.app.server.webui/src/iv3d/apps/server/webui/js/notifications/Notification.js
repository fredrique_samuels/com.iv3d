import * as common from "../common"

import Dispatcher from './../common'

const notificationIdGenerator = new common.IdGenerator("notification_uid_")

export function dispatchNotification(n){
  const dispatcher = Dispatcher.getGlobal()
  dispatcher(n)
}

class AbstractNotification {
  constructor(type, payload={}) {
    this.id = notificationIdGenerator.generate()
    this.type = type
    this.payload = payload
  }
}

export class News24ItemPayload {
  constructor() {
    this.title="No Title"
    this.tags=[]
    this.locationName=""
    this.location=null
  }
}

export const NEWS24_ITEM = "NEWS24_ITEM_NOTIFICATION"
export class News24Item extends AbstractNotification {
  constructor(payload=new News24ItemPayload()) {
    super(NEWS24_ITEM, payload)
  }
}

export const APP_LAYER_COMPONENT_MOUNTED = "APP_LAYER_COMPONENT_MOUNTED"
export class AppLayerComponentMounted extends AbstractNotification {
  constructor(payload) {
    super(APP_LAYER_COMPONENT_MOUNTED, payload)
  }
}
export function dispatchAppLayerComponentMounted(modelId) {
  dispatchNotification(new AppLayerComponentMounted({layerComponentModelId:modelId}))
}
