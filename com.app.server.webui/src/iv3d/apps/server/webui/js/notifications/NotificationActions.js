
export const NEW_NOTIFICATION = "NOTIFICATION_CACHE_NEW_NOTIFICATION"
export function newNotification(n) {
  return {
    type:NEW_NOTIFICATION,
    payload:n,
  }
}
