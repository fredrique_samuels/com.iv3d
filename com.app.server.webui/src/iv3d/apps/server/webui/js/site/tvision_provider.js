'use strict'

import { Provider } from 'react-redux'

import React from 'react'
import ReactDom from 'react-dom'
import TVision from './TVision'
import store from './../store'


const app = document.getElementById('app')
ReactDom.render(
  <Provider store={store}>
    <TVision />
  </Provider>,
  app)
