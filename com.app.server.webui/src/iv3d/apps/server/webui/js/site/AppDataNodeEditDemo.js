import React from 'react'
import { connect } from 'react-redux'

import DataNodeEditDemoLayerComponent from './../applications/datanode/DataNodeEditDemoLayerComponent'
import Dispatcher from './../common'

const DATANODE_EDIT_DEMO_MODELID = "datanode.edit.demo.modelid"

@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export default class AppDataNodeEditDemo extends React.Component {

  constructor() {
    super()
  }

  componentWillMount() {
    Dispatcher.setGlobal(this.props.dispatch)
  }

  render() {
    const contentArea = {
      position:"relative",
      width:"100%",
      height:"100%",
    }

    return ( <div class="fill-parent" style={contentArea}>
        {<DataNodeEditDemoLayerComponent modelId={DATANODE_EDIT_DEMO_MODELID} />}
    </div> )
  }
}
