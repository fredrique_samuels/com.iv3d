import React from 'react'
import { connect } from 'react-redux'

import Immutable from 'immutable'

import * as App from './../components/ApplicationContentComponent'
import * as common from "../common"
import * as Common from './../api/Common'
import * as ComponentModels from "./../components/ComponentModels"
import * as UiConstants from './../components/ui/UiConstants'
import * as Notification from './../notifications/Notification'
import * as NotificationActions from './../notifications/NotificationActions'


import ApplicationContentComponent from './../components/ApplicationContentComponent'
import DataNodeFactory from './../components/ui/DataNodeFactory'
import Interface from './../api/Interface'
import MainMenu from './../components/MainMenu'
import ServiceManager from './../domain/ServiceManager'
import WorldMapEventsWidget from './../components/worldevents/WorldMapEventsWidget'

import Dispatcher from './../common'

import * as TestData from './../components/TestData'



@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export default class TVision extends React.Component {

  constructor() {
    super()
    this.services = new ServiceManager()
    // console.log(Dispatcher.getGlobal())
  }

  componentWillMount() {
    Dispatcher.setGlobal(this.props.dispatch)
    this.services.startAll()
    ApplicationContentComponent.createDefaultModel(this.props.dispatch)
    TestData.testLayers(this.props.dispatch, App.DEFAULT_APP_CONTENT_COMPONENT)
    // TestData.demoEventData(this.props.dispatch)
  }

  componentWillUnmount() {
    this.services.stopAll()
  }

  componentDidUpdate() {
  }

  render() {

    const g = {
      backgroundColor:"black"
    }

    const img = {
      width:"100%",
      height:"100%",
    }

    const mainAppContentId = App.DEFAULT_APP_CONTENT_COMPONENT
    const worldMap = (<div class="fill-parent-absolute" >
      <div class="fill-parent layer-root">
        <WorldMapEventsWidget />
      </div>
    </div>)

    const contentArea = {
      position:"relative",
      width:"100%",
      height:"100%",
    }

    const contentLayer = <ApplicationContentComponent modelId={mainAppContentId} />

    const menuItems = []
    menuItems.push()

    return ( <div class="fill-parent" style={contentArea}>
        <div class="fill-parent" style={g}>
          <img src="res/images/blue-gradient-background-2.jpg" style={img}/>
        </div>
        {worldMap}
        {contentLayer}
    </div> )
  }
}
