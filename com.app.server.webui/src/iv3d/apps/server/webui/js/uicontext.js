import Dispatcher from './common'
import { LayerComponentManager } from './framework/LayerComponent'
import { FormComponentManager } from './framework/FormComponent'
import { RadialMenuManager } from './framework/RadialMenuComponent'
import { TitledBoxManager } from './framework/TitledBoxComponent'
import {TableComponentManager} from "./framework/TableComponent";
import StatusBoxViewManager from "./framework/StatusBoxViewManager";

import TVisionManager from './tvision/TVisionManager'
import TVisionAppManager from './tvision/TVisionAppManager'
import CookieManager from "./framework/Cookies";
import AccountManager from "./framework/AccountManager";
import I8nManager from "./framework/I8nManager";
import { uiStyleManager } from "./framework/styles/UiStyleSheet";

export default class UiContext {
    constructor(params={}) {
        this.params = params;
    }

    // mutation methods
    setProperties(userparams={}) {return new UiContext(Object.assign({}, this.params, userparams))}
    setLayerComponentModelId(modelId) {return this.setProperties({layer_component_id:modelId})}

    // getters
    getDispatcher(){return Dispatcher.getGlobal()}

    //ui managers
    layerComponentManager(){return new LayerComponentManager(this, this.params.layer_component_id)}
    formComponentManager(){return new FormComponentManager(this)}
    radialMenuManager(){return new RadialMenuManager(this)}
    titledBoxManager(){return new TitledBoxManager(this)}
    tableManager(){return new TableComponentManager(this)}
    statusBoxManager(){return new StatusBoxViewManager(this)}

    // tvision managers
    tvisionManager(){return new TVisionManager(this, this.params.tvisionCmg, this.params.tvisionModel)}
    tvisionAppManager(){return new TVisionAppManager(this)}

    cookieManager(){return new CookieManager()}
    accountManager(){return new AccountManager(this)}

    i8n(){return new I8nManager(this)}
    styleSheet(){return uiStyleManager}

    /**
    TODO

    create ajax manager

    // fetch(this.masterFileUrl,{ method:'get',headers: {'Access-Control-Allow-Origin': "*"}})
    //     .then(result => {
    //             console.log(result.json())
    //     })

    // var xhttp = new XMLHttpRequest();
    // xhttp.onreadystatechange = function() {
    //   console.log('response success');
    //     if (this.readyState == 4 && this.status == 200) {
    //       console.log('response success');
    //       console.log(JSON.parse(this.responseText))
    //    }
    // };
    // xhttp.open("GET", this.masterFileUrl, true);
    // xhttp.setRequestHeader("Content-type", "text/plain");
    // xhttp.send();


    // $.getJSON(this.imageIndexFileUrl, function (obj) {
    //       console.log(obj);
    //     })
    //     .fail(function() {
    //       console.log( "error" );
    //     });
    //
    // $.getJSON(this.masterFileUrl, function (obj) {
    //       console.log(obj);
    //     })
    //     .fail(function() {
    //       console.log( "error" );
    //     });

    // var xhr = new XMLHttpRequest();
    // xhr.open('GET', this.imageIndexFileUrl, true);
    // xhr.responseType = 'text';
    // xhr.onload = function(r){console.log(JSON.parse(r.response))}
    // xhr.send();

    // fetch('URL_GOES_HERE', {
    //    method: 'post',
    //    headers: {
    //      'Authorization': 'Basic '+btoa('username:password'),
    //      'Content-Type': 'application/x-www-form-urlencoded'
    //    },
    //    body: 'A=1&B=2'
    //  });
    */

}
