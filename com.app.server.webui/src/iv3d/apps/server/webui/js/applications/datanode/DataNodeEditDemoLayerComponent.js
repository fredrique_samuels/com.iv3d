import React from 'react'

import * as ApplicationContentComponentModule from './../../components/ApplicationContentComponent'

import ApplicationContentComponent from './../../components/ApplicationContentComponent'
import Dispatcher from './../../common'
import Interface from './../../api/Interface'
import DataNodeEditorWindow from './edit/DataNodeEditorWindow'


class ContentHandler extends React.Component{
  render() {
    const containerStyle = {
      position:"relative",
      width:"80%",
      height:"80%",
      backgroundColor:"rgba(0,0,0,.2)",
      top:"10%",
      left:"10%",
    }
    return <div style={containerStyle}><DataNodeEditorWindow /></div>
  }
}

class ContentHandlerFactory {
  createContentHandler(args) {
    const {contentParams, layerId} = args
    return <ContentHandler />
  }
}

export default class DataNodeEditDemoLayerComponent extends React.Component {
  onMount(id) {

    const contentParams = {
      layerComponentModelId:this.props.modelId,
      contentHandlerFactory:new ContentHandlerFactory()
    }
    const layerParams = {
      canBeCleared:true
    }

    Interface.renderDataToNewLayer(Dispatcher.getGlobal(), contentParams, layerParams)
  }

  render() {
    const params = {
      functionMode : ApplicationContentComponentModule.FUNCTION_MODE_CUSTOM,
      actions:[
        {
          title:<i class="fa fa-home"></i>,
          action:function(){window.location="/"}
        },
      ]
    }
    const contentComp = <ApplicationContentComponent modelId={this.props.modelId} mountedCallback={this.onMount.bind(this)} params={params} />

    const containerStyle = {
      top:"0%",
      width:"100%",
      height:"100%",
      position:"absolute",
      opacity:"1",
      backgroundSize: "100% 100%",
      backgroundImage: "url('/res/images/application-layer-bg.jpg')"
    }

    return (<div style={containerStyle}>
      {contentComp}
    </div>
    )
  }
}
