
import * as App from './../components/ApplicationContentComponent'
import ApplicationContentComponent from './../components/ApplicationContentComponent'
import Dispatcher from './../common'


/**
 *
 * DOCUMENTATION
 *
 * @Dispatcher
 *
 *  This a redux dispatcher stored in common.js. We typically set the in the
 *  entry point JS file and store it globally. No point passing this to every function
 *  in the code base.
 *
 * @ContentParams
 * An object used by the LayerContentFactory to constuct the view objects.
 * {
 *
 *      layerComponentModelId : The layer manager id,
 *      contentParams: Contains the data to be displayed
 *      {
 *          contentHandlerFactory[optional]: An class instance with a obj.createContentHandler(@LayerModel)
 *                                           method that return a React.Component.
 *          dataNodes: {},
 *          formData:{},
 *          // or any custom user data
 *      }
 * }
 *
 *
 * @LayerModel
 * A object contain all the data for the layer.
 *
 * {
 *  layerComponentModelId: see @layerComponentModelId,
 *  layerId: The current layer Id. Used to remove the layer if needed,
 *  contentParams: see @ContentParams,
 *  layerParams: see @LayerParams,
 * }
 *
 * @LayerParams
 * An object containing setting for the layer
 * {
 *      canBeCleared: Boolean indicating if the layer can be cleared using the global clear button.
 *                    If set false it is the responsibility of the layer widgets to have a way to clear the layer.
 * }
 *
 * @layerComponentModelId
 * The model id of the layer manager component. Used to spawn new layers
 *
 *
 * @DataNodes
 *
 */


export class UiController {
    constructor(layerManagerId) {
        this.layerMangerId = layerManagerId;
    }
}

export default class Interface {

  static renderDataToNewLayer(dispatcher, contentParams, layerParams={canBeCleared:true}) {
    const {layerComponentModelId} = contentParams
    const layerId = ApplicationContentComponent
      .addLayeredComponents(dispatcher,
        layerComponentModelId,
        contentParams,
        layerParams)
    return layerId
  }

  static renderNewLayer(contentParams, layerParams={canBeCleared:true}) {
    Interface.renderDataToNewLayer(Dispatcher.getGlobal(), contentParams, layerParams)
  }

 static removeLayeredComponents(dispatcher, layerId, layerComponentModelId) {
   ApplicationContentComponent
     .removeLayeredComponents(dispatcher,
       layerComponentModelId,
       layerId)
 }

 static removeLayeredComponents(layerId, layerComponentModelId) {
    ApplicationContentComponent
        .removeLayeredComponents(Dispatcher.getGlobal(),
            layerComponentModelId,
            layerId)
 }

 static renderFormToNewLayer(formData, layerParams) {
     Interface.renderDataToNewLayer(Dispatcher.getGlobal(), contentParams, layerParams)
 }

}
