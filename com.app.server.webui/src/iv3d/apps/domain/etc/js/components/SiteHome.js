import React from 'react'
import { connect } from 'react-redux'

import * as ComponentModels from "./ComponentModels"

import Immutable from 'immutable'
import Dispatcher from './../common'
import ProductCard from './home/ProductCard'

import ResourcePathResolver from './ResourcePathResolver'


class TopMenuButton extends React.Component {

}

@connect((store) => {
  return {
    environment : store.environment,
    componentModels : store.componentModels,
  }
})
export default class SiteHome extends React.Component {

  constructor() {
    super()
  }

  componentWillMount() {
    Dispatcher.setGlobal(this.props.dispatch)
  }

  render() {

    const g = {
      backgroundColor:"black"
    }

    const img = {
      width:"100%",
      height:"100%",
    }

    const contentArea = {
      position:"relative",
      width:"100%",
      height:"100%",
    }

    const productContent = (
      <div style={{position:"relative", width:"100%", height:"50px", backgroundColor:"rgba(255, 0, 255, .5)"}}></div>
    )

    return ( <div class="fill-parent" style={contentArea}>
        <div class="fill-parent" style={{width:"100%", height:"120px", position:"relative", backgroundColor:"rgba(211,211,211, 1)"}}>
          <svg width="300px" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
               <a>
               <image x="0" y="0" height="100%" width="100%" xlinkHref="/res/images/tt_logo_entry_animated.svg" />
              </a>
          </svg>

          <button class="site-home-menu-button"><i class="fa fa-question fa-2x"></i></button>
          <button class="site-home-menu-button"><i class="fa fa-home fa-2x"></i></button>
        </div>
        <div class="fill-parent" style={{overflow:"auto", backgroundSize: "100% 100%", backgroundImage: "url('/res/images/003.jpeg')", width:"100%", height:"calc(100% - 120px)", position:"relative"}}>
            <ProductCard contentComp={null}/>
            <ProductCard contentComp={null}/>
            <ProductCard contentComp={null}/>
            <ProductCard contentComp={null}/>
            <ProductCard contentComp={null}/>
        </div>
    </div> )
  }
}
