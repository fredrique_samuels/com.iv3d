package org.tect.microtasks;

import java.util.function.Function;

public final class UniqueChannelNameGen extends UniqueNameFinder {
    public UniqueChannelNameGen(Function<String, Boolean> exists) {
        super(exists, (n, i) -> n+"$"+i);
    }
}
