/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;


import org.apache.commons.collections.map.ListOrderedMap;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

final class InputChannels implements ChannelCollection {

    private final Map<String, InputChannel> channels;
    private StatusUpdateConsumer statusUpdateConsumer;

    private InputChannels(Map<String, InputChannel> channels, StatusUpdateConsumer statusUpdateConsumer) {
        this.channels = Collections.unmodifiableMap(channels);
        this.statusUpdateConsumer = statusUpdateConsumer;
        pushWaitingStates();
    }

    private void pushWaitingStates() {
        if(statusUpdateConsumer==null) {
            return;
        }
        this.channels.values()
                .stream()
                .forEach( c -> statusUpdateConsumer.onInputChannelStatusChange(c.getId(), c.getState()));
    }

    @Override
    public void push(String channelId, Payload payload) {
        InputChannel inputChannel = channels.get(channelId);
        if(inputChannel==null) {
            return;
        }
        ChannelStatus state = inputChannel.getState();
        inputChannel.push(payload);
        ChannelStatus newState = inputChannel.getState();
        if(!state.equals(newState) && statusUpdateConsumer!=null) {
            statusUpdateConsumer.onInputChannelStatusChange(channelId, newState);
        }
    }

    @Override
    public void pushValue(String channelId, Object value) {
        assert(!(value instanceof Payload));
        InputChannel channel = channels.get(channelId);
        if(channel==null) {
            return;
        }
        Payload payload = Payload.create(channel.getType(), value);
        this.push(channelId, payload);
    }

    @Override
    public void pushPayloads(PayloadCollection inputs) {
        inputs.forEach((n, p) -> push(n, p));
    }

    @Override
    public void fillEmptyChannels() {
        channels.entrySet().stream().forEach( es -> es.getValue().push(Payload.nullValue()));
    }

    @Override
    public List<Channel> getChannels() {
        return channels.values().stream().collect(Collectors.toList());
    }

    public PayloadCollection join() {
        channels.values().stream().forEach(c -> c.join());
        return getPayload();
    }

    public PayloadCollection getPayload() {
        return new PayloadCollectionImpl(getChannels());
    }

    public static Builder create() {
        return new BuilderImpl(null);
    }

    public static Builder create(StatusUpdateConsumer statusUpdateConsumer) {
        return new BuilderImpl(statusUpdateConsumer);
    }

    public interface Builder {
        InputChannels build();
        Builder addChannel(String channelName, PayloadType type);
    }

    private static class BuilderImpl implements Builder {

        private final Map<String, InputChannel> channels;
        private StatusUpdateConsumer statusUpdateConsumer;

        public BuilderImpl(StatusUpdateConsumer statusUpdateConsumer) {
            this.statusUpdateConsumer = statusUpdateConsumer;
            this.channels = new ListOrderedMap();
        }

        @Override
        public InputChannels build() {
            return new InputChannels(channels, statusUpdateConsumer);
        }

        @Override
        public Builder addChannel(String channelName, PayloadType type) {
            try {
                channels.put(channelName, new InputChannel(channelName, type));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return this;
        }
    }

    private static class InputChannel extends AbstractChannel {
        private final Semaphore semaphore = new Semaphore(1);

        public InputChannel(String name, PayloadType type) throws InterruptedException {
            super(name, type);
            semaphore.acquire();
        }

        public synchronized final void push(Payload payload) {
            semaphore.release();
            super.push(payload);
        }

        public Payload join() {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getPayload();
        }
    }
}
