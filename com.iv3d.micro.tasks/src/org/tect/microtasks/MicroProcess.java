/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;

public final class MicroProcess {

    private final InputChannels inputChannels;
    private final OutputChannels outputChannels;
    private final MicroTask microTask;
    private final StatusUpdateConsumer consumer;
    private Exception error;

    public MicroProcess(InputChannels inputChannels, OutputChannels outputChannels, MicroTask microTask, StatusUpdateConsumer consumer) {
        this.inputChannels = inputChannels;
        this.outputChannels = outputChannels;
        this.microTask = microTask;
        this.consumer = consumer;
    }

    public final ChannelCollection getInputChannels() {
        return inputChannels;
    }

    public final Exception getError() {
        return error;
    }

    public final PayloadCollection execute() {
        try {
            dispatchStatus(TaskStatus.WAITING);
            PayloadCollection inputs = inputChannels.join();
            dispatchStatus(TaskStatus.RUNNING);
            if(!inputs.isUsable()) {
                return getOutputPayloadCollection();
            }

            microTask.execute(inputs, outputChannels);
            dispatchStatus(TaskStatus.OK);
        } catch (Exception e) {
            dispatchStatus(TaskStatus.ERROR);
            error = e;
            e.printStackTrace();
        }

        return getOutputPayloadCollection();
    }

    private void dispatchStatus(TaskStatus status) {
        if(consumer==null) {
            return;
        }
        consumer.onTaskStatusChange(status);
    }

    PayloadCollection getOutputPayloadCollection() {
        outputChannels.fillEmptyChannels();
        return outputChannels.getPayloadCollection();
    }
}
