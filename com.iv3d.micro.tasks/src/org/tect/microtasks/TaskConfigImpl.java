package org.tect.microtasks;

class TaskConfigImpl implements TaskConfig {
    private final long id;
    private final String name;

    TaskConfigImpl(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getId() {
        return id;
    }
}