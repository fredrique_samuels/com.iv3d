package org.tect.microtasks;

public interface MicroTaskFactory {
    MicroTask create();
}
