package org.tect.microtasks;

public interface TaskExecutor {
    TaskExecutor withDoneObserver(TaskDoneObserver doneObserver);
    TaskExecutor withPayloadProvider(PayloadProvider payloadProvider);
    TaskExecutor withStatusConsumer(StatusUpdateConsumer statusObserver);
    void execute();
}