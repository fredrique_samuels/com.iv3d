package org.tect.microtasks;

class ChannelConfigImpl implements ChannelConfig {
    private final Long taskId;
    private final String channel;
    private final String type;

    public ChannelConfigImpl(Long taskId, String channel, String type) {
        this.taskId = taskId;
        this.channel = channel;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public Long getTaskId() {
        return taskId;
    }

    @Override
    public String getChannel() {
        return channel;
    }
}
