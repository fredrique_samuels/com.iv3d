package org.tect.microtasks;

public interface StatusUpdateConsumer {
    void onInputChannelStatusChange(String channelName, ChannelStatus status);
    void onOutputChannelStatusUpdate(String channelName, ChannelStatus status);
    void onTaskStatusChange(TaskStatus status);
}
