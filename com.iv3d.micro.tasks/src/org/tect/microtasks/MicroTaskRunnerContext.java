package org.tect.microtasks;

public interface MicroTaskRunnerContext {
    String getTaskId();
    PayloadCollection runTask();
    void signalDone(PayloadCollection output);
}
