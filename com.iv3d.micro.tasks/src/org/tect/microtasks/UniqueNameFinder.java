package org.tect.microtasks;

import java.util.function.Function;

public class UniqueNameFinder {
    private final Function<String, Boolean> exists;
    private final NameFactory nameFactory;

    public UniqueNameFinder(Function<String, Boolean> exists, NameFactory nameFactory) {
        this.exists = exists;
        this.nameFactory = nameFactory;
    }

    public final String getName(String name) {
        String curName = name;
        int iteration = 0;
        while(this.exists.apply(curName)) {
            curName = nameFactory.create(name, ++iteration);
        }
        return curName;
    }

    public interface NameFactory {
        String create(String baseName, int iteration);
    }
}
