package org.tect.microtasks;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

class ConfigBasedProcessBuilder extends MicroProcessBuilder {
    private static Logger logger = Logger.getLogger(ConfigBasedProcessBuilder.class);

    ConfigBasedProcessBuilder(MicroTaskContext context, String id, TaskExecutorConfig config) {
        super(id, () -> new Task(context, config));

        config.getInputs()
                .entrySet()
                .stream()
                .forEach( es -> addInput(es.getKey(), es.getValue().getType()));

        config.getOutputs()
                .entrySet()
                .stream()
                .forEach( es -> addOutput(es.getKey(), es.getValue().getType()));
    }

    private static class Task implements MicroTask {

        private final MicroTaskContext context;
        private final TaskExecutorConfig config;

        public Task(MicroTaskContext context, TaskExecutorConfig config) {
            this.context = context;
            this.config = config;
        }

        @Override
        public void execute(PayloadCollection inputPayloads, ChannelCollection outputPayload) {

            List<Long> roots = config.getRoots();

            RunContext runContext = new RunContext(context, config, outputPayload , inputPayloads);
            roots.stream().forEach( id -> runContext.execute(id));
            runContext.join();
        }

        class RunContext {
            private final WaitOnDone waitOnDone;
            private PayloadCollection inputPayloads;
            private final MicroTaskContext context;
            private final TaskExecutorConfig config;
            private final ChannelCollection outputPayloads;
            private final Map<Long, TaskInputConsumer> inputConsumerMap;

            public RunContext(MicroTaskContext context, TaskExecutorConfig config, ChannelCollection outputPayloads, PayloadCollection inputPayloads) {
                this.context = context;
                this.config = config;
                this.outputPayloads = outputPayloads;
                this.waitOnDone = new WaitOnDone(config.getLeafs().size());
                this.inputPayloads = inputPayloads;
                this.inputConsumerMap = new HashMap<>();
            }

            public ChannelCollection getOutput() {
                return outputPayloads;
            }

            public void notifyLeafDone() {
                waitOnDone.release();
            }

            public void join() {
                waitOnDone.join();
            }

            public TaskInputConsumer execute(Long workingTaskId) {
                synchronized (inputConsumerMap) {
                    if(inputConsumerMap.containsKey(workingTaskId)) {
                        return inputConsumerMap.get(workingTaskId);
                    }
                    TaskConfig taskConfig = config.get(workingTaskId);

                    TaskInputConsumer inputConsumer = new TaskInputConsumer();
                    inputConsumerMap.put(workingTaskId, inputConsumer);

                    TaskDoneObserver leafDone = o -> {
                        if(config.getLeafs().contains(workingTaskId)) {
                            notifyLeafDone();
                        }
                    };

                    TaskDoneObserver pushConnections = new TaskDoneObserver() {
                        @Override
                        public void done(PayloadCollection output) {
                            config.getConnections()
                                .stream()
                                .filter( conn -> conn.getOutputTask()==workingTaskId)
                                .forEach(conn -> {
                                    Long inputTask = conn.getInputTask();
                                    String inputChannel = conn.getInputChannel();
                                    String outputChannel = conn.getOutputChannel();
                                    Payload payload = output.get(outputChannel);
                                    TaskInputConsumer taskInputConsumer = RunContext.this.execute(inputTask);
                                    taskInputConsumer.push(inputChannel, payload);
                                });
                        }
                    };

                    TaskDoneObserver pushOutputs = new TaskDoneObserver() {
                        @Override
                        public void done(PayloadCollection output) {
                            Map<String, ChannelConfig> configOutputs = config.getOutputs();
                            configOutputs.entrySet()
                                .stream()
                                .filter( es -> es.getValue().getTaskId()==workingTaskId)
                                .forEach(es -> {
                                    String channelId = es.getKey();
                                    Payload payload = output.get(es.getValue().getChannel());
                                    outputPayloads.push(channelId, payload);
                                });
                        }
                    };

                    PayloadProvider payloadProvider = new PayloadProvider() {
                        @Override
                        public void provideFor(ChannelCollection inputChannels) {
                            Map<String, ChannelConfig> inputs = config.getInputs();
                            inputs.entrySet()
                                    .stream()
                                    .filter( es -> es.getValue().getTaskId()==workingTaskId)
                                    .forEach(es -> {
                                        String channel = es.getValue().getChannel();
                                        Payload payload = inputPayloads.get(es.getKey());
                                        inputChannels.push(channel, payload);
                                    });
                            inputConsumer.setInputChannels(inputChannels);
                        }
                    };

                    StatusUpdateConsumer statusConsumer=null;
                    ConfigExecutorStatusConsumer configStatusConsumer = config.getStatusConsumer();
                    if(configStatusConsumer!=null) {
                        statusConsumer = new StatusUpdateConsumer() {
                            @Override
                            public void onInputChannelStatusChange(String channelName, ChannelStatus status) {
                                configStatusConsumer.onInputChannelStatusChange(workingTaskId, channelName, status);
                            }

                            @Override
                            public void onOutputChannelStatusUpdate(String channelName, ChannelStatus status) {
                                configStatusConsumer.onOutputChannelStatusUpdate(workingTaskId, channelName, status);
                            }

                            @Override
                            public void onTaskStatusChange(TaskStatus status) {
                                configStatusConsumer.onTaskStatusChange(workingTaskId, status);
                            }
                        };
                    }

                    String name = taskConfig.getName();
                    context.beginTask(name)
                            .withDoneObserver(pushConnections)
                            .withDoneObserver(pushOutputs)
                            .withDoneObserver(leafDone)
                            .withStatusConsumer(statusConsumer)
                            .withPayloadProvider(payloadProvider)
                            .execute();

                    return inputConsumer;
                }
            }
        }

        class WaitOnDone {

            private final Semaphore s;
            private int size;

            public WaitOnDone(int size) {
                this.size = size;
                this.s = new Semaphore(size);
                this.acquire(size);
            }

            void acquire(int size) {
                try {
                    this.s.acquire(size);
                } catch (InterruptedException e) {
                    logger.trace("", e);
                }
            }

            boolean join() {
                try {
                    s.acquire(size);
                } catch (InterruptedException e) {
                    logger.trace("", e);
                    return false;
                }
                return true;
            }

            public void release() {
                s.release(1);
            }
        }

        private class TaskInputConsumer {
            private ChannelCollection inputChannels;
            private Map<String, Payload> queuedPayload;
            private Semaphore semaphore = new Semaphore(1);

            public TaskInputConsumer() {
                queuedPayload = new HashMap<>();
            }

            public void setInputChannels(ChannelCollection inputChannels) {
                try {
                    semaphore.acquire();
                    this.inputChannels = inputChannels;
                    queuedPayload.entrySet().forEach(es -> inputChannels.push(es.getKey(), es.getValue()));
                    queuedPayload.clear();
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            public void push(String channel, Payload value) {
                try {
                    semaphore.acquire();
                    if(this.inputChannels!=null) {
                        inputChannels.push(channel, value);
                    } else {
                        if(!queuedPayload.containsKey(channel)) {
                            queuedPayload.put(channel, value);
                        }
                    }
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
