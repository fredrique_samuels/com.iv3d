/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class PayloadCollectionImpl extends AbstractPayloadCollection {

    private final Map<String, Payload> payloadMap;
    private boolean usable = true;

    public PayloadCollectionImpl(Collection<Channel> channels) {
        this.payloadMap = new HashMap<>();
        channels.stream().forEach( c -> addPayload(c) );
    }

    public PayloadCollectionImpl(Map<String, Payload> payloads) {
        this.payloadMap = Collections.unmodifiableMap(payloads);
    }

    @Override
    public Payload get(String channelId) {
        if(contains(channelId))  {
            return payloadMap.get(channelId);
        }
        return Payload.nullValue();
    }

    @Override
    public String[] getChannelIds() {
        return payloadMap.keySet().toArray(new String[0]);
    }

    @Override
    public boolean contains(String channelId) {
        return payloadMap.containsKey(channelId);
    }

    @Override
    public boolean isUsable() {
        return usable;
    }

    @Override
    public void forEach(PayloadConsumer payloadConsumer) {
        payloadMap.entrySet().stream().forEach(e -> payloadConsumer.accept(e.getKey(), e.getValue()));
    }

    private void addPayload(Channel c) {
        String channelId = c.getId();
        if(ChannelStatus.WAITING.equals(c.getState())) {
            payloadMap.put(channelId, Payload.nullValue());
            this.usable = false;
            return;
        }

        Payload pl = c.getPayload();
        if(Payload.isNull(pl)) {
            this.usable = false;
        }
        payloadMap.put(channelId, pl);
    }
}