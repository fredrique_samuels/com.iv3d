/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;

import org.apache.commons.collections.map.ListOrderedMap;

import java.util.Collections;
import java.util.Map;

public class MicroProcessBuilder {

    private final Map<String, String> inputs;
    private final Map<String, String> outputs;
    private final String id;
    private final MicroTaskFactory microTaskFactory;

    public MicroProcessBuilder(String id, MicroTaskFactory microTaskFactory) {
        this.id = id;
        this.microTaskFactory = microTaskFactory;
        this.outputs = new ListOrderedMap();
        this.inputs = new ListOrderedMap();
    }

    public final MicroProcessBuilder addInput(String name, String type) {
        this.inputs.put(createKey(inputs, name), type);
        return this;
    }

    public final MicroProcessBuilder addOutput(String name, String type) {
        this.outputs.put(createKey(outputs, name), type);
        return this;
    }

    public final String getId() {
        return id;
    }

    public final Map<String, String> getInputs() {
        return Collections.unmodifiableMap(inputs);
    }

    public final Map<String, String> getOutputs() {
        return Collections.unmodifiableMap(outputs);
    }

    public final MicroProcess build(MicroTaskContext context, StatusUpdateConsumer consumer) {
        InputChannels.Builder inputBuilder = InputChannels.create(consumer);
        getInputs().entrySet().stream().forEachOrdered( c -> inputBuilder.addChannel(c.getKey(), getPayloadType(context, c.getValue())));
        InputChannels inputChannels = inputBuilder.build();


        OutputChannels.Builder outputBuilder = OutputChannels.create(consumer);
        getOutputs().entrySet().stream().forEachOrdered( c -> outputBuilder.addChannel(c.getKey(), getPayloadType(context, c.getValue())));
        OutputChannels outputChannels = outputBuilder.build();

        return new MicroProcess(inputChannels, outputChannels, microTaskFactory.create(), consumer);
    }

    private String createKey(Map<String, String> map, String name) {
        int counter = 0;
        String newName = name;
        while(map.containsKey(newName)) {
            newName = name + "$" + (++counter);
        }
        return newName;
    }

    private PayloadType getPayloadType(MicroTaskContext context, String type) {
        PayloadType payloadType = context.getPayloadTypeByName(type);
        if(payloadType==null) {
            throw new RuntimeException("Unable to find payload type for key '"+type+"'");
        }
        return payloadType;
    }
}
