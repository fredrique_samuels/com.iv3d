package org.tect.microtasks;

public interface PayloadConsumer {
    void accept(String name, Payload payload);
}