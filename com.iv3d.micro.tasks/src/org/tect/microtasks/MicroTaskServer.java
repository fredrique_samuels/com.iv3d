/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class MicroTaskServer implements MicroTaskContext {

    private static Logger logger = Logger.getLogger(MicroTaskServer.class);
    private MicroTaskRunner defaultTaskRunner;
    private MicroTaskRunner fallbackDefaultRunner;
    private Map<String, MicroProcessBuilder> microTaskProcessFactoryRegistry;
    private Map<String, PayloadFactory> payloadFactoryRegistry;
    private Map<String, MicroTaskRunner> runners;
    private long tempIdSeed = 0;

    public MicroTaskServer() {
        this.defaultTaskRunner = new ThreadPoolTaskRunner(20);
        this.fallbackDefaultRunner = defaultTaskRunner;
        this.microTaskProcessFactoryRegistry = new ConcurrentHashMap<>();
        this.payloadFactoryRegistry = new ConcurrentHashMap<>();
        this.runners = new HashedMap();
    }

    public final void setDefaultTaskRunner(MicroTaskRunner taskRunner) {
        if(taskRunner==null) {
            this.defaultTaskRunner = fallbackDefaultRunner;
        } else {
            this.defaultTaskRunner = taskRunner;
        }
    }

    @Override
    public final TaskExecutor beginTask(String task) {
        return new TaskExecutorImpl(this, task);
    }

    @Override
    public TaskExecutor beginTask(TaskExecutorConfig config) {
        String tempId = createTempId();
        registerProcess(new ConfigBasedProcessBuilder(this, tempId, config));
        return new TaskExecutorImpl(this, tempId)
                .withDoneObserver((o) -> deregisterProcess(tempId));
    }

    @Override
    public final void registerProcess(MicroProcessBuilder processBuilder) {
        if(processBuilder==null) {
            return;
        }
        microTaskProcessFactoryRegistry.put(processBuilder.getId(), processBuilder);
    }

    @Override
    public final void deregisterProcess(String taskId) {
        microTaskProcessFactoryRegistry.remove(taskId);
    }

    @Override
    public final void registerPayloadType(String payloadId, PayloadFactory factory) {
        payloadFactoryRegistry.put(payloadId, factory);
    }

    @Override
    public PayloadType getPayloadTypeByName(String type) {
        PayloadFactory factory = payloadFactoryRegistry.get(type);
        if(factory==null) {
            return PayloadType.nullValue();
        }
        return factory.getType();
    }

    @Override
    public Map<String, String> getInputChannels(String taskId) {
        MicroProcessBuilder builder = microTaskProcessFactoryRegistry.get(taskId);
        if(builder==null) {
            return Collections.emptyMap();
        }
        return builder.getInputs();
    }


    @Override
    public Map<String, String> getOutputChannels(String taskId) {
        MicroProcessBuilder builder = microTaskProcessFactoryRegistry.get(taskId);
        if(builder==null) {
            return Collections.emptyMap();
        }
        return builder.getOutputs();
    }

    public final void setCustomRunner(String taskName, MicroTaskRunner runner) {
        this.runners.put(taskName, runner);
    }


    private static class TaskExecutorImpl implements TaskExecutor {

        private final MicroTaskServer taskServer;
        private final String task;
        private final CompositeDoneObserver compositeDoneObserver;
        private PayloadProvider payloadProvider;
        private StatusUpdateConsumer statusUpdateConsumer;

        public TaskExecutorImpl(MicroTaskServer taskServer, String task) {
            this.taskServer = taskServer;
            this.task = task;
            this.compositeDoneObserver = new CompositeDoneObserver();
        }

        @Override
        public TaskExecutor withDoneObserver(TaskDoneObserver doneObserver) {
            compositeDoneObserver.add(doneObserver);
            return this;
        }

        @Override
        public TaskExecutor withPayloadProvider(PayloadProvider payloadProvider) {
            this.payloadProvider = payloadProvider;
            return this;
        }

        @Override
        public TaskExecutor withStatusConsumer(StatusUpdateConsumer statusUpdateConsumer) {
            this.statusUpdateConsumer = statusUpdateConsumer;
            return this;
        }

        @Override
        public void execute() {
            MicroTaskRunner taskRunner = taskServer.runners.get(task);
            if(taskRunner!=null) {
                executeTask(compositeDoneObserver, task, taskRunner);
                return;
            }
            executeTask(compositeDoneObserver, task, taskServer.defaultTaskRunner);
        }

        void executeTask(TaskDoneObserver taskDoneObserver, String task, MicroTaskRunner taskRunner) {
            MicroProcessRegistryImpl processRegistry = new MicroProcessRegistryImpl(this.taskServer);

            MicroTaskRunnerContextImpl context = new MicroTaskRunnerContextImpl(processRegistry, task, taskDoneObserver, payloadProvider, statusUpdateConsumer);
            taskRunner.execute(context);
        }

    }

    private String createTempId() {
        return "micro.task.server.temp.id."+(++tempIdSeed);
    }

    private static class MicroTaskRunnerContextImpl implements MicroTaskRunnerContext {
        private final String taskId;
        private final TaskDoneObserver taskDoneObserver;
        private final PayloadProvider payloadProvider;
        private StatusUpdateConsumer statusUpdateConsumer;
        private MicroProcessRegistry processRegistry;

        public MicroTaskRunnerContextImpl(MicroProcessRegistry processRegistry, String task, TaskDoneObserver taskDoneObserver, PayloadProvider payloadProvider, StatusUpdateConsumer statusUpdateConsumer) {
            this.processRegistry = processRegistry;
            this.taskId = task;
            this.taskDoneObserver = taskDoneObserver;
            this.payloadProvider = payloadProvider;
            this.statusUpdateConsumer = statusUpdateConsumer;
        }

        @Override
        public String getTaskId() {
            return taskId;
        }

        @Override
        public PayloadCollection runTask() {
            MicroProcess process = processRegistry.createProcess(taskId, statusUpdateConsumer);
            if(process==null) {
                PayloadCollection empty = AbstractPayloadCollection.empty();
                this.taskDoneObserver.done(empty);
                return empty;
            }

            ChannelCollection inputChannels = process.getInputChannels();
            if(payloadProvider!=null) {
                payloadProvider.provideFor(inputChannels);
            } else {
                inputChannels.fillEmptyChannels();
            }

            PayloadCollection output = process.execute();
            this.taskDoneObserver.done(output);
            return output;
        }

        @Override
        public void signalDone(PayloadCollection output) {
            if(taskDoneObserver!=null) {
                taskDoneObserver.done(output);
            }
        }
    }

    private static class MicroProcessRegistryImpl implements MicroProcessRegistry {
        private Map<String, MicroProcessBuilder> processFactoryMap;
        private MicroTaskServer taskServer;

        public MicroProcessRegistryImpl(MicroTaskServer taskServer) {
            this.processFactoryMap = taskServer.microTaskProcessFactoryRegistry;
            this.taskServer = taskServer;
        }

        @Override
        public MicroProcess createProcess(String taskId, StatusUpdateConsumer statusUpdateConsumer) {
            MicroProcessBuilder builder = processFactoryMap.get(taskId);
            if(builder==null) {
                logger.warn("No process builder registered for task " + taskId);
                return null;
            }

            try {
                return builder.build(taskServer, statusUpdateConsumer);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error creating process to task " + taskId, e);
            }
            return null;
        }
    }

    private static class CompositeDoneObserver implements TaskDoneObserver {

        private final ArrayList<TaskDoneObserver> observers;

        public CompositeDoneObserver() {
            this.observers = new ArrayList<>();
        }

        void add(TaskDoneObserver observer) {
            this.observers.add(observer);
        }

        @Override
        public void done(PayloadCollection output) {
            observers.stream().forEach( o -> o.done(output));
        }
    }
}
