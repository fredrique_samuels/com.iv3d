package org.tect.microtasks;

/**
 * Created by fred on 2017/11/05.
 */
public abstract class AbstractPayloadCollection implements PayloadCollection{
    /**
     * Return an Empty Collection
     * @return asn empty collection
     */
    public static PayloadCollection empty() {
        return new EmptyCollection();
    }

    private static class EmptyCollection implements PayloadCollection {
        @Override
        public Payload get(String channelId) {
            return Payload.nullValue();
        }

        @Override
        public String[] getChannelIds() {
            return new String[0];
        }

        @Override
        public boolean contains(String channelId) {
            return false;
        }

        @Override
        public boolean isUsable() {
            return false;
        }

        @Override
        public void forEach(PayloadConsumer payloadConsumer) {

        }
    }

}
