package org.tect.microtasks;


public interface ChannelConfig {
    String getType();
    Long getTaskId();
    String getChannel();
}
