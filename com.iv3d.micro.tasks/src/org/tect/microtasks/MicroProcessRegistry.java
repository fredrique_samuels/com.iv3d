package org.tect.microtasks;

public interface MicroProcessRegistry {
    MicroProcess createProcess(String taskId, StatusUpdateConsumer statusUpdateConsumer);
}
