package org.tect.microtasks;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

final class TaskExecutorConfigImpl implements TaskExecutorConfig {
    private final Map<Long, TaskConfig> tasks;
    private final List<ChannelConnectionConfig> connections;
    private final List<Long> roots;
    private final List<Long> leafs;
    private final Map<String, ChannelConfig> inputs;
    private final Map<String, ChannelConfig> outputs;
    private final ConfigExecutorStatusConsumer statusConsumer;

    public TaskExecutorConfigImpl(TaskExecutorConfigBuilder builder) {
        this.tasks = builder.getTaskMap();
        this.connections = builder.getConnections();
        this.roots = builder.getRoots();
        this.leafs = builder.getLeafs();
        this.inputs = builder.getInputs();
        this.outputs = builder.getOutputs();
        this.statusConsumer = builder.statusConsumer;
    }

    @Override
    public int getSize() {
        return tasks.size();
    }

    @Override
    public TaskConfig get(Long id) {
        return tasks.get(id);
    }

    @Override
    public List<Long> getRoots() {
        return Collections.unmodifiableList(roots);
    }

    @Override
    public List<Long> getLeafs() {
        return Collections.unmodifiableList(leafs);
    }

    @Override
    public List<Long> getTasks() {
        return  tasks.keySet().stream().collect(Collectors.toList());
    }

    @Override
    public List<ChannelConnectionConfig> getConnections() {
            return Collections.unmodifiableList(connections);
        }

    @Override
    public Map<String, ChannelConfig> getInputs() {
        return Collections.unmodifiableMap(inputs);
    }

    @Override
    public Map<String, ChannelConfig> getOutputs() {
        return Collections.unmodifiableMap(outputs);
    }

    @Override
    public ConfigExecutorStatusConsumer getStatusConsumer() {
        return statusConsumer;
    }


}