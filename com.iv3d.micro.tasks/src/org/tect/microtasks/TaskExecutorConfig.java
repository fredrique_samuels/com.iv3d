package org.tect.microtasks;

import java.util.List;
import java.util.Map;

public interface TaskExecutorConfig {
    int getSize();
    TaskConfig get(Long id);
    List<Long> getRoots();
    List<Long> getLeafs();
    List<Long> getTasks();
    List<ChannelConnectionConfig> getConnections();
    Map<String,ChannelConfig> getInputs();
    Map<String,ChannelConfig> getOutputs();
    ConfigExecutorStatusConsumer getStatusConsumer();
}
