package org.tect.microtasks;

/**
 * Created by fred on 2017/11/10.
 */
public interface ChannelConnectionConfig {
    Long getOutputTask();

    String getOutputChannel();

    Long getInputTask();

    String getInputChannel();
}
