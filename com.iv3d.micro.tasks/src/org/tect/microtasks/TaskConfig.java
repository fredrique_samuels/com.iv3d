package org.tect.microtasks;

public interface TaskConfig {
    String getName();
    long getId();
}
