package org.tect.microtasks;

public interface PayloadProvider {
    void provideFor(ChannelCollection inputChannels);
}
