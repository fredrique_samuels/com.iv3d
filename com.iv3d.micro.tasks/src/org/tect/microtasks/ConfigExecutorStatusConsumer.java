package org.tect.microtasks;

/**
 * Created by fred on 2017/11/14.
 */
public interface ConfigExecutorStatusConsumer {
    void onInputChannelStatusChange(long id, String channelName, ChannelStatus status);
    void onOutputChannelStatusUpdate(long id, String channelName, ChannelStatus status);
    void onTaskStatusChange(long id, TaskStatus status);
}
