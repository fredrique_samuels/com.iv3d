/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;

import java.io.Serializable;


public abstract class PayloadType implements Serializable {

    /**
     * An implementation of {@link PayloadType} representing a null value.
     */
    public static final PayloadType TYPE_NULL = new NullPayloadType();

    /**
     * The type name.
     * @return The type name.
     */
    public abstract String getName();

    /**
     * Validate if the object given is of the payload type expected.
     *
     * @param data The data to validate.
     * @return <code>true</code> if the data is of the right type.
     */
    public boolean validateData(Object data) {
        return true;
    }

    /**
     * Check if a instance of {@link PayloadType} is the same is a this one.
     * @param p The instance to check for.
     * @return <code>true</code> if the paylaod types match.
     */
    public final boolean isType(PayloadType p) {
        try {
            return this.getName().equals(p.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isNullType(PayloadType p) {
        try {
            return TYPE_NULL.isType(p);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * An implementation of {@link PayloadType} representing a null value.
     *
     * @return {@link PayloadType#TYPE_NULL}
     */
    public static final PayloadType nullValue() {
        return TYPE_NULL;
    }

    private static final String NULL_PAYLOAD_NAME = "com.iv3d.micro.service.nullpayload";

    public static PayloadType createForName(String name) {
        return new PayloadType() {
            @Override
            public String getName() {
                return name;
            }
        };
    }

    private static final class NullPayloadType extends PayloadType {

        @Override
        final public String getName() {
            return NULL_PAYLOAD_NAME;
        }
    }

}
