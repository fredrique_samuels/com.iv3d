package org.tect.microtasks;

/**
 * Created by fred on 2017/11/12.
 */
public enum TaskStatus {
    WAITING,
    RUNNING,
    OK,
    ERROR
}
