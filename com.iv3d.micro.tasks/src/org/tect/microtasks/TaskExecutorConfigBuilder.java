package org.tect.microtasks;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.collections.map.ListOrderedMap;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public final class TaskExecutorConfigBuilder {
    private long idSeed = 0;

    private final List<TaskConfigImpl> tasks;
    private final MicroTaskContext context;
    private final List<ChannelConnectionConfigImpl> connections;
    private final List<ChannelRenameConfig> inputRenames;
    private final List<ChannelRenameConfig> outputRenames;
    private final Map<String, ChannelConfig> forcedOutputs;
    ConfigExecutorStatusConsumer statusConsumer;

    public TaskExecutorConfigBuilder(MicroTaskContext context) {
        this.context = context;
        this.connections = new ArrayList<>();
        this.tasks = new ArrayList<>();
        this.inputRenames = new ArrayList<>();
        this.outputRenames = new ArrayList<>();
        this.forcedOutputs = new HashedMap();
    }

    public Long addTask(String s) {
        TaskConfigImpl e = new TaskConfigImpl(newId(), s);
        this.tasks.add(e);
        return e.getId();
    }

    private TaskConfigImpl get(Long id) {
        return tasks.stream().filter( t -> t.getId()==id).findFirst().orElse(null);
    }

    public List<Long> getRoots() {
        return tasks.stream()
                .map(t -> t.getId())
                .filter( id -> isRoot(id))
                .collect(Collectors.toList());
    }

    private boolean isRoot(Long id) {
        return !hasInputChannels(id) || hasUnconnectedInputChannels(id);
    }

    private boolean isLeaf(Long id)  {
        return !hasOutputChannels(id) || hasUnconnectedOutputChannels(id) || hasForcedOutputChannels(id);
    }

    private boolean hasForcedOutputChannels(Long id) {
        return getForcedOutputs(id).size()>0;
    }

    private Map<String, ChannelConfig> getForcedOutputs(Long id) {
        return forcedOutputs.entrySet()
                .stream()
                .filter( es -> es.getValue().getTaskId()==id)
                .collect(Collectors.toMap(es -> es.getKey(),es -> es.getValue()));
    }

    private boolean hasUnconnectedInputChannels(Long id) {
        return getUnconnectedInputs(id).size() > 0;
    }

    private boolean hasUnconnectedOutputChannels(Long id) {
        return getUnconnectedOutputs(id).size() > 0;
    }

    public List<Long> getLeafs() {
        return tasks.stream()
                .map(t -> t.getId())
                .filter( id -> isLeaf(id))
                .collect(Collectors.toList());
    }

    private Map<String, String> getUnconnectedInputs(Long id) {
        Map<String, String> inputChannels = context.getInputChannels(get(id).getName());
        List<String> channelsConnected = connections.stream()
                .filter(conn -> conn.isInput(id))
                .map(conn -> conn.getInputChannel())
                .collect(Collectors.toList());
        Map<String, String> unconnected = inputChannels.entrySet()
                .stream()
                .filter(es -> !channelsConnected.contains(es.getKey()))
                .collect(Collectors.toMap(es -> es.getKey(), es -> es.getValue()));
        return unconnected;
    }

    private Map<String, String> getUnconnectedOutputs(Long id) {
        Map<String, String> outputChannels = context.getOutputChannels(get(id).getName());
        List<String> channelsConnected = connections.stream()
                .filter(conn -> conn.isOutput(id))
                .map(conn -> conn.getOutputChannel())
                .collect(Collectors.toList());
        Map<String, String> unconnected = outputChannels.entrySet()
                .stream()
                .filter(es -> !channelsConnected.contains(es.getKey()))
                .collect(Collectors.toMap(es -> es.getKey(), es -> es.getValue()));
        return unconnected;
    }

    private long newId() {
        return ++idSeed;
    }

    public void connect(Long outputTask, String outputChannel, Long inputTask, String inputChannel) {
        ChannelConnectionConfigImpl newConn =
                new ChannelConnectionConfigImpl(outputTask, outputChannel, inputTask, inputChannel);
        if(connections.stream()
                .filter( conn -> conn.equals(newConn)).count() == 0 && outputTask != inputTask ) {
            connections.add(newConn);
        }
    }

    public TaskExecutorConfig build() {
        return new TaskExecutorConfigImpl(this);
    }

    public TaskExecutorConfigBuilder renameInput(String newName, Long taskId, String channelName) {
        if(inputRenames.stream()
                .filter( r -> !(r.getTaskId()==taskId && r.getChannelName().equals(channelName)))
                .count()>0) {
            return this;
        }
        inputRenames.add(new ChannelRenameConfig(newName, taskId, channelName));
        return this;
    }

    public TaskExecutorConfigBuilder renameOutput(String newName, Long taskId, String channelName) {
        if(outputRenames.stream()
                .filter( r -> !(r.getTaskId()==taskId && r.getChannelName().equals(channelName)))
                .count()>0) {
            return this;
        }
        outputRenames.add(new ChannelRenameConfig(newName, taskId, channelName));
        return this;
    }

    private String createInputChannelName(Long taskId, UniqueChannelNameGen uniqueChannelNameGen, String channelName) {
        ChannelRenameConfig channelRenameConfig = inputRenames.stream()
                .filter(r -> r.getTaskId() == taskId && r.getChannelName().equals(channelName))
                .findFirst().orElse(null);
        if(channelRenameConfig!=null) {
            return uniqueChannelNameGen.getName(channelRenameConfig.getNewName());
        }

        return uniqueChannelNameGen.getName(channelName);
    }

    private String createOutputChannelName(Long taskId, UniqueChannelNameGen uniqueChannelNameGen, String channelName) {
        ChannelRenameConfig channelRenameConfig = outputRenames.stream()
                .filter(r -> r.getTaskId() == taskId && r.getChannelName().equals(channelName))
                .findFirst().orElse(null);
        if(channelRenameConfig!=null) {
            return uniqueChannelNameGen.getName(channelRenameConfig.getNewName());
        }

        return uniqueChannelNameGen.getName(channelName);
    }

    Map<Long,TaskConfig> getTaskMap() {
        Map<Long, TaskConfigImpl> collect  = new ListOrderedMap();
        tasks.stream().forEach( t -> collect.put(t.getId(), t));
        return Collections.unmodifiableMap(collect);
    }

    private boolean hasOutputChannels(Long id) {
        return connections.stream().filter( conn -> conn.getOutputTask()==id).count() > 0;
    }

    private boolean hasInputChannels(Long id) {
        return context.getInputChannels(get(id).getName()).size()>0;
    }

    Map<String, ChannelConfig> getInputs() {
        Map<String, ChannelConfig> map = new ListOrderedMap();
        UniqueChannelNameGen uniqueChannelNameGen = new UniqueChannelNameGen((n) -> map.containsKey(n));

        Consumer<Long> processRoot = id -> {
            Map<String, String> inputChannelsForId = context.getInputChannels(get(id).getName());
            Set<String> unconnectedChannels = getUnconnectedInputs(id).keySet();
            inputChannelsForId.entrySet().stream()
                    .filter( es -> unconnectedChannels.contains(es.getKey()))
                    .forEach(es -> map.put(createInputChannelName(id, uniqueChannelNameGen, es.getKey()), new ChannelConfigImpl(id, es.getKey(), es.getValue())));
        };

        List<Long> roots = getRoots();
        roots.stream().forEach(processRoot);

        return map;
    }

    Map<String, ChannelConfig> getOutputs() {
        final Map<String, ChannelConfig> map = new ListOrderedMap();
        UniqueChannelNameGen uniqueChannelNameGen = new UniqueChannelNameGen((n) -> map.containsKey(n));

        Consumer<Long> processLeaf = id -> {
            Map<String, String> channelsForIds = context.getOutputChannels(get(id).getName());
            Set<String> unconnectedChannels = getUnconnectedOutputs(id).keySet();
            Map<String, ChannelConfig> forcedOutputs = getForcedOutputs(id);
            channelsForIds.entrySet().stream()
                    .filter( es -> unconnectedChannels.contains(es.getKey()) || hasForcedOutputChannels(id))
                    .forEach(es -> {
                        Map.Entry<String, ChannelConfig> forcedConfig = forcedOutputs.entrySet()
                                .stream()
                                .filter(fes -> fes.getValue().getChannel().equals(es.getKey()))
                                .findFirst()
                                .orElse(null);
                        if(forcedConfig!=null) {
                            ChannelConfigImpl channelConfig = new ChannelConfigImpl(id, es.getKey(), es.getValue());
                            map.put(forcedConfig.getKey(), channelConfig);
                        } else {
                            String outputChannelName = createOutputChannelName(id, uniqueChannelNameGen, es.getKey());
                            ChannelConfigImpl channelConfig = new ChannelConfigImpl(id, es.getKey(), es.getValue());
                            map.put(outputChannelName, channelConfig);
                        }
                    });
        };

        List<Long> leafs = getLeafs();
        leafs.stream().forEach(processLeaf);

        return map;
    }

    List<ChannelConnectionConfig> getConnections() {
        return connections.stream().collect(Collectors.toList());
    }

    public void setOutputChannel(String name, Long taskId, String channel) {
        forcedOutputs.put(name, new ChannelConfigImpl(taskId, channel, null));
    }

    public TaskExecutorConfigBuilder setStatusConsumer(ConfigExecutorStatusConsumer statusConsumer) {
        this.statusConsumer = statusConsumer;
        return this;
    }

    private class ChannelConnectionConfigImpl implements ChannelConnectionConfig {
        private final Long outTask;
        private final String outChannel;
        private final Long inTask;
        private final String inChannel;

        private ChannelConnectionConfigImpl(Long outTask, String outChannel, Long inTask, String inChannel) {
            this.outTask = outTask;
            this.outChannel = outChannel;
            this.inTask = inTask;
            this.inChannel = inChannel;
        }

        @Override
        public Long getOutputTask() {
            return outTask;
        }

        @Override
        public String getOutputChannel() {
            return outChannel;
        }

        @Override
        public Long getInputTask() {
            return inTask;
        }

        @Override
        public String getInputChannel() {
            return inChannel;
        }

        boolean isOutput(Long taskId) {
            return this.outTask==taskId;
        }

        boolean isInput(Long taskId) {
            return this.inTask==taskId;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof ChannelConnectionConfig) {
                ChannelConnectionConfig other = (ChannelConnectionConfig)obj;
                return this.outTask==other.getOutputTask() &&
                        this.outChannel.equals(other.getOutputChannel()) &&
                        this.inTask == other.getInputTask() &&
                        this.inChannel.equals(other.getInputChannel());
            }
            return super.equals(obj);
        }
    }

    private class ChannelRenameConfig {
        private final String newName;
        private final Long taskId;
        private final String channelName;

        public ChannelRenameConfig(String newName, Long taskId, String channelName) {
            this.newName = newName;
            this.taskId = taskId;
            this.channelName = channelName;
        }

        public String getNewName() {
            return newName;
        }

        public Long getTaskId() {
            return taskId;
        }

        public String getChannelName() {
            return channelName;
        }
    }
}
