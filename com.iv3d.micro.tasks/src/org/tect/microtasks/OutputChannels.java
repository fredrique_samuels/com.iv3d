/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;


import org.apache.commons.collections.map.ListOrderedMap;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

final class OutputChannels implements ChannelCollection {

    private final Map<String, OutputChannel> channels;
    private final StatusUpdateConsumer statusUpdateConsumer;

    private OutputChannels(Map<String, OutputChannel> channels, StatusUpdateConsumer statusUpdateConsumer) {
        this.channels = channels;
        this.statusUpdateConsumer = statusUpdateConsumer;
        pushWaitingStates();
    }

    private void pushWaitingStates() {
        if(statusUpdateConsumer==null) {
            return;
        }
        channels.values()
                .stream()
                .forEach( c -> statusUpdateConsumer.onOutputChannelStatusUpdate(c.getId(), c.getState()));
    }

    public void fillEmptyChannels() {
        channels.entrySet().stream().forEach( es -> es.getValue().push(Payload.nullValue()));
    }

    PayloadCollection getPayloadCollection() {
        return new PayloadCollectionImpl(getChannels());
    }


    PayloadCollection join() {
        List<Channel> channels = getChannels();
        channels.stream().forEach( c -> join() );
        return new PayloadCollectionImpl(channels);
    }

    @Override
    public void pushValue(String channelId, Object value) {
        assert(!(value instanceof Payload));
        OutputChannel channel = channels.get(channelId);
        if(channel==null) {
            return;
        }
        Payload payload = Payload.create(channel.getType(), value);
        push(channelId, payload);
    }

    @Override
    public void push(String channelId, Payload payload) {
        OutputChannel outputChannel = channels.get(channelId);
        if(outputChannel!=null) {
            ChannelStatus state = outputChannel.getState();
            outputChannel.push(payload);
            ChannelStatus newState = outputChannel.getState();
            if(!state.equals(newState) && statusUpdateConsumer !=null) {
                statusUpdateConsumer.onOutputChannelStatusUpdate(channelId, newState);
            }
        }
    }

    @Override
    public void pushPayloads(PayloadCollection inputs) {
        inputs.forEach((n, p) -> push(n, p));
    }

    @Override
    public List<Channel> getChannels() {
        return channels.values().stream().collect(Collectors.toList());
    }

    public static Builder create() {
        return new BuilderImpl(null);
    }

    public static Builder create(StatusUpdateConsumer consumer) {
        return new BuilderImpl(consumer);
    }

    public interface Builder {
        Builder addChannel(String channelId, PayloadType payloadType);
        OutputChannels build();
    }

    private static class BuilderImpl implements Builder {

        private Map<String, PayloadType> typeMap;
        private StatusUpdateConsumer consumer;

        public BuilderImpl(StatusUpdateConsumer consumer) {
            this.consumer = consumer;
            this.typeMap = new ListOrderedMap();
        }

        @Override
        public Builder addChannel(String channelId, PayloadType payloadType) {
            typeMap.put(channelId, payloadType);
            return this;
        }

        @Override
        public OutputChannels build() {
            final Map<String, OutputChannel> channelMap = new ListOrderedMap();
            typeMap.entrySet()
                    .stream()
                    .forEach(es -> {
                        OutputChannel channel = null;
                        try {
                            channel = new OutputChannel(es.getKey(), es.getValue());
                            channelMap.put(es.getKey(), channel);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
            return new OutputChannels(channelMap, consumer);
        }
    }

    private static class OutputChannel extends AbstractChannel {

        private final Semaphore semaphore = new Semaphore(1);

        public OutputChannel(String name, PayloadType type) throws InterruptedException {
            super(name, type);
            semaphore.acquire();
        }

        public synchronized final void push(Payload payload) {
            semaphore.release();
            super.push(payload);
        }

        public Payload join() {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getPayload();
        }
    }
}
