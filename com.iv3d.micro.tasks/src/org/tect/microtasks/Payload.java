/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;


import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;

public abstract class Payload implements Serializable {
    private static Logger logger = Logger.getLogger(Payload.class);
    public static final Payload NULL = new NullPayload();

    protected Payload() {
    }

    /**
     * The payload type.
     *
     * @return The payload type.
     * @see Payload
     */
    public abstract PayloadType getType();

    /**
     * The payload data
     *
     * @return The payload data.
     */
    public abstract Object getData();

    public final <T> T getData(Class<T> aClass){
        Object data = getData();
        if(data==null) {
            return null;
        }

        Class<?> valueType = data.getClass();
        if(valueType.isArray()) {
            Object[] objArr = (Object[]) data;
            return objArr.length > 0 ? aClass.cast(objArr[0]) : null;
        }

        return aClass.cast(data);
    }

    public final <T> T[] getDataArray(Class<T> aClass) {
        Object data = getData();
        if(data==null) {
            return null;
        }

        Class<?> valueType = data.getClass();
        if(valueType.isArray()) {
            Object[] objArr = (Object[]) data;
            T[] arr = (T[]) Array.newInstance(aClass, objArr.length);
            System.arraycopy(objArr, 0, arr, 0, objArr.length);
            return arr;
        }

        T[] arr = (T[]) Array.newInstance(aClass, 1);
        arr[0] = aClass.cast(data);
        return arr;
    }

    /**
     * An implementation of {@link PayloadType} representing a null value.
     *
     * @return {@link PayloadType#TYPE_NULL}
     */
    public static final Payload nullValue() {
        return NULL;
    }

    public static final Payload create(PayloadType type, Object value) {
        if(value==null) {
            return createPayload(type, null);
        }
        try {
            if (!type.validateData(value)) {
                return createPayload(type, null);
            }
        } catch (Exception e) {
            logger.warn(ExceptionUtils.getStackTrace(e));
            return createPayload(type, null);
        }

        return createPayload(type, value);
    }

    static Payload createPayload(final PayloadType type, final Object value) {
        return new Payload() {
            @Override
            public PayloadType getType() {
                return type;
            }

            @Override
            public Object getData() {
                return value;
            }
        };
    }

    public static boolean isNull(Payload pl) {
        return PayloadType.isNullType(pl.getType());
    }


}
