
package org.tect.microtasks;

public interface PayloadFactory {
    PayloadType getType();
    Payload createPayload(Object o);
}
