/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;


class AbstractChannel implements Channel {
    private final String name;
    private final PayloadType type;
    private ChannelStatus state;
    private Payload payload;

    public AbstractChannel(String name, PayloadType type) {
        this.name = name;
        this.type = type;
        this.state = ChannelStatus.WAITING;
    }

    @Override
    public final String getId() {
        return name;
    }

    @Override
    public final ChannelStatus getState() {
        return state;
    }

    @Override
    public final PayloadType getType() {
        return type;
    }

    @Override
    public final Payload getPayload() {
        return payload;
    }

    public synchronized void push(Payload payload) {
        if (ChannelStatus.WAITING.equals(state)) {
            PayloadType type = payload.getType();
            if(!this.type.equals(type) && !PayloadType.isNullType(type)) {
                state = ChannelStatus.NULL;
                this.payload = Payload.nullValue();
            } else {
                state = ChannelStatus.OK;
                this.payload = payload == null ? Payload.nullValue() : payload;
            }
        }
    }
}
