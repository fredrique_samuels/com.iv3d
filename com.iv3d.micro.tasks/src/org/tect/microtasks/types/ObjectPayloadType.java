/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package org.tect.microtasks.types;

import org.tect.microtasks.Payload;
import org.tect.microtasks.PayloadFactory;
import org.tect.microtasks.PayloadType;

public final class ObjectPayloadType extends PayloadType {
    public static final String NAME = Object.class.getTypeName();
    public static final PayloadFactory FACTORY = new PayloadFactoryImpl();
    private static final PayloadType TYPE = new ObjectPayloadType();

    private ObjectPayloadType() {
    }

    @Override
    public String getName() {
        return NAME;
    }

    private static class PayloadFactoryImpl implements PayloadFactory {
        @Override
        public PayloadType getType() {
            return TYPE;
        }

        @Override
        public Payload createPayload(Object o) {
            return Payload.create(TYPE, o);
        }
    }

}
