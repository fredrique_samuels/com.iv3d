package org.tect.microtasks;

import com.sun.tools.javac.util.Pair;
import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.apache.commons.collections.map.ListOrderedMap;
import org.tect.microtasks.TestUtils.*;
import org.tect.microtasks.types.StringPayloadType;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by fred on 2017/11/07.
 */
public class TaskExecutorConfigBuilderTest extends TestCase {

    private TaskExecutorConfigBuilder taskExecutorConfigBuilder;
    private @Mocked MicroTaskContext context;

    @Override
    protected void setUp() throws Exception {
        taskExecutorConfigBuilder = new TaskExecutorConfigBuilder(context);
    }

    public void testCreateNew() {
        //when
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        assertNotNull(config);
        assertEquals(0, config.getSize());
        assertEquals(0, config.getRoots().size());
        assertEquals(0, config.getLeafs().size());
    }


    public void testCreateBasicTask() {
        //given
        taskExecutorConfigBuilder.addTask("test.task");

        //when
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        assertNotNull(config);
        assertEquals(1, config.getSize());

        List<Long> roots = config.getRoots();
        assertEquals(1, roots.size());

        List<Long> leafs = config.getLeafs();
        assertEquals(1, leafs.size());

        TaskConfig root = config.get(roots.get(0));
        assertNotNull(root);
        assertEquals(1L, root.getId());
        assertEquals("test.task", root.getName());
    }

    public void testGetTasks() {
        //given
        taskExecutorConfigBuilder.addTask("test.task");
        taskExecutorConfigBuilder.addTask("test.task2");

        //when
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        List<Long> tasks = config.getTasks();
        assertEquals(2, tasks.size());

        {
            Long id = tasks.get(0);
            String expected = "test.task";
            long expectedId = 1L;

            assertEquals(Long.valueOf(expectedId), id);
            TaskConfig taskConfig = config.get(id);
            assertEquals(expected, taskConfig.getName());
        }

        {
            Long id = tasks.get(1);
            assertEquals(Long.valueOf(2L), id);

            TaskConfig taskConfig = config.get(id);
            assertEquals(taskConfig.getName(), "test.task2");
        }
    }

    public void testBasicConnection() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(),
                Arrays.asList(new Pair<>("output", "type1")));
        createTaskOnContext("test.task2",
                Arrays.asList(new Pair<>("input", "type1")),
                Arrays.asList());

        // when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long t2 = taskExecutorConfigBuilder.addTask("test.task2");
        taskExecutorConfigBuilder.connect(t1, "output", t2, "input");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        List<Long> roots = config.getRoots();
        assertEquals(1, roots.size());
        assertEquals(t1, roots.get(0));

        List<Long> leafs = config.getLeafs();
        assertEquals(1, leafs.size());
        assertEquals(t2, leafs.get(0));

        List<ChannelConnectionConfig> connections = config.getConnections();
        assertNotNull(connections);
        assertEquals(1, connections.size());

        ChannelConnectionConfig connection = connections.get(0);
        assertEquals(t1, connection.getOutputTask());
        assertEquals("output", connection.getOutputChannel());
        assertEquals(t2, connection.getInputTask());
        assertEquals("input", connection.getInputChannel());
    }

    public void testConfigInputs_Include_AllRootInputs() {

        //given
        createTaskOnContext("test.task1",
                Arrays.asList(new Pair<>("input1", "type1")),
                Arrays.asList());

        createTaskOnContext("test.task2",
                Arrays.asList(new Pair<>("input2", "type2")),
                Arrays.asList());

        //when
        Long task1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long task2 = taskExecutorConfigBuilder.addTask("test.task2");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> inputs = config.getInputs();
        assertEquals(2, inputs.size());
        {
            ChannelConfig input = inputs.get("input1");
            assertChannelConfig(input, task1, "input1", "type1");
        }


        {
            ChannelConfig input = inputs.get("input2");
            assertChannelConfig(input, task2, "input2", "type2");
        }

    }

    public void testConfigOutputs_Include_AllLeafOutputs() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(),
                Arrays.asList(new Pair<>("output1", "type1")));

        createTaskOnContext("test.task2",
                Arrays.asList(),
                Arrays.asList(new Pair<>("output2", "type2")));

        //when
        Long task1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long task2 = taskExecutorConfigBuilder.addTask("test.task2");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> channels = config.getOutputs();
        assertEquals(2, channels.size());

        assertChannelConfig(channels.get("output1"), task1, "output1", "type1");
        assertChannelConfig(channels.get("output2"), task2, "output2", "type2");
    }

    public void testConfigInputs_Include_AllNonConnectedInputs() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(new Pair<>("input", "string")),
                Arrays.asList(new Pair<>("output", "string")));
        createTaskOnContext("test.task2",
                Arrays.asList(new Pair<>("input", "string"), new Pair<>("not.connected", "string")),
                Arrays.asList());

        // when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long t2 = taskExecutorConfigBuilder.addTask("test.task2");
        taskExecutorConfigBuilder.connect(t1, "output", t2, "input");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> inputs = config.getInputs();
        assertEquals(2, inputs.size());
        assertChannelConfig(inputs.get("not.connected"), t2, "not.connected", "string");

        //and
        List<Long> roots = config.getRoots();
        assertEquals(2, roots.size());
    }

    public void testConfigOutputs_Include_AllNonConnectedOutputs() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(new Pair<>("input", "string")),
                Arrays.asList(new Pair<>("output", "string"), new Pair<>("not.connected", "string")));
        createTaskOnContext("test.task2",
                Arrays.asList(new Pair<>("input", "string")),
                Arrays.asList(new Pair<>("output2", "string")));

        // when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long t2 = taskExecutorConfigBuilder.addTask("test.task2");
        taskExecutorConfigBuilder.connect(t1, "output", t2, "input");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> inputs = config.getOutputs();
        assertEquals(2, inputs.size());
        ChannelConfig c = inputs.get("not.connected");
        assertNotNull(c);
        assertChannelConfig(c, t1, "not.connected", "string");

        //and
        List<Long> leafs = config.getLeafs();
        assertEquals(2, leafs.size());
    }

    public void testConfigInputs_DuplicateNamed() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(new Pair<>("input", "string")),
                Arrays.asList());
        createTaskOnContext("test.task2",
                Arrays.asList(new Pair<>("input", "string")),
                Arrays.asList());

        //when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long t2 = taskExecutorConfigBuilder.addTask("test.task2");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> inputs = config.getInputs();
        assertEquals(2, inputs.size());
        assertChannelConfig(inputs.get("input"), t1, "input", "string");
        assertChannelConfig(inputs.get("input$1"), t2, "input", "string");
    }

    public void testConfigOutputs_DuplicateNamed() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(),
                Arrays.asList(new Pair<>("output", "string")));
        createTaskOnContext("test.task2",
                Arrays.asList(),
                Arrays.asList(new Pair<>("output", "string")));

        //when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        Long t2 = taskExecutorConfigBuilder.addTask("test.task2");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> configMap = config.getOutputs();
        assertEquals(2, configMap.size());
        assertChannelConfig(configMap.get("output"), t1, "output", "string");
        assertChannelConfig(configMap.get("output$1"), t2, "output", "string");
    }

    public void testConfigInputs_RenameChannel() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(new Pair<>("input", "string")),
                Arrays.asList());

        //when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        taskExecutorConfigBuilder.renameInput("new.name", t1, "input");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> configMap = config.getInputs();
        assertChannelConfigTypeString(configMap.get("new.name"), t1, "input");
    }

    public void testConfigOutputs_RenameChannel() {
        //given
        createTaskOnContext("test.task1",
                Arrays.asList(),
                Arrays.asList(new Pair<>("channel", "string")));

        //when
        Long t1 = taskExecutorConfigBuilder.addTask("test.task1");
        taskExecutorConfigBuilder.renameOutput("new.name", t1, "channel");
        TaskExecutorConfig config = taskExecutorConfigBuilder.build();

        //then
        Map<String, ChannelConfig> configMap = config.getOutputs();
        assertChannelConfigTypeString(configMap.get("new.name"), t1, "channel");
    }

    public void testSetChannelAsOutput() throws InterruptedException {
        //given
        MicroTaskServer microTaskServer = createTaskContext();

        //and
        TaskExecutorConfigBuilder builder = new TaskExecutorConfigBuilder(microTaskServer);
        Long toLower = builder.addTask(StringToLowerBuilder.ID);

        Long split = builder.addTask(StringSplitBuilder.ID);
        builder.connect(toLower, StringToLowerBuilder.OUTPUT, split, StringSplitBuilder.INPUT);

        builder.setOutputChannel("customOutput", toLower, StringToLowerBuilder.OUTPUT);
        TaskExecutorConfig config = builder.build();

        assertEquals(2, config.getLeafs().size());
        assertEquals(2, config.getOutputs().size());

        // when
        ThreadWait threadWait = new ThreadWait();
        microTaskServer.beginTask(config)
                .withDoneObserver((pl) -> {
                    String customOutput = pl.get("customOutput").getData(String.class);
                    assertEquals("uppercase", customOutput);
                    threadWait.done();
                })
                .withPayloadProvider((cc) -> cc.pushValue(StringToLowerBuilder.INPUT, "UPPERCASE"))
                .execute();

        // then
        assertTrue(threadWait.waitOnCall(2000));
    }

    MicroTaskServer createTaskContext() {
        MicroTaskServer microTaskServer = new MicroTaskServer();
        TestProcessModules.registerTypes(microTaskServer);
        TestProcessModules.registerProcessBuilders(microTaskServer);
        return microTaskServer;
    }

//    public void testErrorOnFeedbackLoop() {
//        //given
//        //TODO
//        fail();
//    }

    public void testSingleTaskExecution() throws InterruptedException {

        //given
        MicroTaskServer microTaskServer = new MicroTaskServer();
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        microTaskServer.registerProcess(new StringToLowerBuilder());

        //and
        TaskExecutorConfigBuilder builder = new TaskExecutorConfigBuilder(microTaskServer);
        builder.addTask(StringToLowerBuilder.ID);
        TaskExecutorConfig config = builder.build();

        //with
        ThreadWait threadWait = new ThreadWait();

        // when
        microTaskServer.beginTask(config)
                .withDoneObserver((pl) -> {
                    assertEquals("uppercase", pl.get(StringToLowerBuilder.OUTPUT).getData(String.class));
                    threadWait.done();
                })
                .withPayloadProvider((cc) -> cc.pushValue(StringToLowerBuilder.INPUT, "UPPERCASE"))
                .execute();

        // then
        assertTrue(threadWait.waitOnCall(2000));
    }



    public void testComplexConfiguration() throws InterruptedException {

        //given
        MicroTaskServer microTaskServer = createTaskContext();

        //and
        TaskExecutorConfigBuilder builder = new TaskExecutorConfigBuilder(microTaskServer);
        Long toLower = builder.addTask(StringToLowerBuilder.ID);

        Long split = builder.addTask(StringSplitBuilder.ID);
        builder.connect(toLower, StringToLowerBuilder.OUTPUT, split, StringSplitBuilder.INPUT);

        Long reverse = builder.addTask(StringReverseBuilder.ID);
        builder.connect(toLower, StringToLowerBuilder.OUTPUT, reverse, StringReverseBuilder.INPUT);

        Long join = builder.addTask(StringJoinBuilder.ID);
        builder.connect(split, StringSplitBuilder.OUTPUT, join, StringJoinBuilder.INPUT1);
        builder.connect(reverse, StringReverseBuilder.OUTPUT, join, StringJoinBuilder.INPUT2);

        //when
        ThreadWait threadWait = new ThreadWait();
        TaskExecutorConfig config = builder.build();
        microTaskServer.beginTask(config)
                .withPayloadProvider(new PayloadProvider() {
                    @Override
                    public void provideFor(ChannelCollection inputChannels) {
                        inputChannels.pushValue(StringToLowerBuilder.INPUT, "TestValue");
                    }
                })
                .withDoneObserver(output -> {
                            String output1 = output.get(StringJoinBuilder.OUTPUT).getData(String.class);
                            assertNotNull(output1);
                            assertEquals("testvalue@eulavtset", output1);
                            threadWait.done();
                        })
                .execute();

        assertTrue(threadWait.waitOnCall(5000));
    }

    public void testBuild_WithInvalidChannelType() throws InterruptedException {
        //given
        MicroTaskServer microTaskServer = createTaskContext();

        //and
        TaskExecutorConfigBuilder builder = new TaskExecutorConfigBuilder(microTaskServer);
        builder.addTask(TaskWithNonExistingChannelType.ID);
        TaskExecutorConfig config = builder.build();

        //when
        ThreadWait threadWait = new ThreadWait();
        microTaskServer.beginTask(config)
                .withDoneObserver(output -> threadWait.done())
                .execute();

        //then
        assertTrue(threadWait.waitOnCall(2000));
    }

    public void testBuild_WithInvalid_Task() throws InterruptedException {

        //given
        MicroTaskContext microTaskServer = new MicroTaskServer();
        TestProcessModules.registerTypes(microTaskServer);

        //and
        TaskExecutorConfigBuilder builder = new TaskExecutorConfigBuilder(microTaskServer);
        builder.addTask("does not exists");
        TaskExecutorConfig config = builder.build();

        //when
        ThreadWait threadWait = new ThreadWait();
        microTaskServer.beginTask(config)
                .withDoneObserver(output -> threadWait.done())
                .execute();

        //then
        assertTrue(threadWait.waitOnCall(2000));
    }

    private void createTaskOnContext(String taskId, List<Pair<String, String>> inputs, List<Pair<String, String>> outputs) {

        Map<String, String> inputMap = new ListOrderedMap();
        inputs.stream().forEach( i -> inputMap.put(i.fst, i.snd));

        Map<String, String> outputMap = new ListOrderedMap();
        outputs.stream().forEach( i -> outputMap.put(i.fst, i.snd));

        new NonStrictExpectations() {
            {
                context.getInputChannels(taskId);
                result = inputMap;

                context.getOutputChannels(taskId);
                result = outputMap;
            }
        };
    }

    private void assertChannelConfig(ChannelConfig c, Long task1, String expectedChannel, String expectedType) {
        assertNotNull(c);
        assertEquals(task1, c.getTaskId());
        assertEquals(expectedChannel, c.getChannel());
        assertEquals(expectedType, c.getType());
    }

    private void assertChannelConfigTypeString(ChannelConfig c, Long task1, String expectedChannel) {
        assertChannelConfig(c, task1, expectedChannel, "string");
    }
}