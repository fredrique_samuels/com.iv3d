/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;


import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

public class OutputChannelsTest extends TestCase {
    private static final PayloadType DEMO_TYPE_INT = new PayloadType() {
        @Override
        public String getName() {
            return "demo.type.1";
        }

        @Override
        public boolean validateData(Object data) {
            return data instanceof Integer;
        }
    };

    private static final PayloadType DEMO_TYPE_STRING = new PayloadType() {
        @Override
        public String getName() {
            return "demo.type.2";
        }

        @Override
        public boolean validateData(Object data) {
            return data instanceof String;
        }
    };

    private static final String CHANNEL_NAME_1 = "channel.1";
    private static final String CHANNEL_NAME_2 = "channel.2";

    @Test
    public void testCreateNew() {
        //given
        OutputChannels channels = OutputChannels.create()
                .build();

        //then
        assertEquals(0, channels.getChannels().size());
    }

    @Test
    public void testSetupChannels() {
        //given
        OutputChannels outputChannels = OutputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_STRING)
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .addChannel(CHANNEL_NAME_2, DEMO_TYPE_STRING)
                .build();

        //then
        List<Channel> channels = outputChannels.getChannels();
        assertEquals(2, channels.size());

        {
            Channel channel = channels.get(0);
            assertEquals(CHANNEL_NAME_1, channel.getId());
            assertEquals(ChannelStatus.WAITING, channel.getState());
            assertEquals(null, channel.getPayload());
            assertEquals(DEMO_TYPE_INT, channel.getType());
        }

        {
            Channel channel = channels.get(1);
            assertEquals(CHANNEL_NAME_2, channel.getId());
            assertEquals(ChannelStatus.WAITING, channel.getState());
            assertEquals(null, channel.getPayload());
            assertEquals(DEMO_TYPE_STRING, channel.getType());
        }
    }

    @Test
    public void testFillEmptyChannels() throws Exception {
        //given
        OutputChannels outputChannels = OutputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .addChannel(CHANNEL_NAME_2, DEMO_TYPE_STRING)
                .build();

        //when
        outputChannels.push(CHANNEL_NAME_1, Payload.create(DEMO_TYPE_INT, Integer.valueOf(33)));
        outputChannels.fillEmptyChannels();

        //then
        PayloadCollection payloadCollection = outputChannels.getPayloadCollection();
        assertEquals(2, payloadCollection.getChannelIds().length);
        assertEquals(Integer.valueOf(33), payloadCollection.get(CHANNEL_NAME_1).getData());
        assertTrue(Payload.isNull(payloadCollection.get(CHANNEL_NAME_2)));
    }

    @Test
    public void testPush() throws Exception {
        //given
        OutputChannels outputChannels = OutputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_STRING)
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .addChannel(CHANNEL_NAME_2, DEMO_TYPE_STRING)
                .build();

        //when
        outputChannels.push(CHANNEL_NAME_1, Payload.create(DEMO_TYPE_INT, Integer.valueOf(33)));
        outputChannels.push(CHANNEL_NAME_2, Payload.create(DEMO_TYPE_STRING, "value"));

        //then
        PayloadCollection payloadCollection = outputChannels.getPayloadCollection();

        assertEquals(2, payloadCollection.getChannelIds().length);
        assertEquals(Integer.valueOf(33), payloadCollection.get(CHANNEL_NAME_1).getData());
        assertEquals("value", payloadCollection.get(CHANNEL_NAME_2).getData());
    }

}