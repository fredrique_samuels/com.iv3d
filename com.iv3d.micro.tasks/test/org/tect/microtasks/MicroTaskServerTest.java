package org.tect.microtasks;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.Verifications;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.tect.microtasks.types.StringPayloadType;

import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Created by fred on 2017/10/29.
 */
public class MicroTaskServerTest extends TestCase {
    private static Logger logger = Logger.getLogger(MicroTaskServerTest.class);

    private MicroTaskServer microTaskServer;

    private static final String TEST_TASK = "task.1";
    @Mocked private MicroTask testTask;

    private static final String TEST_TASK_WA_STR = "task.withargs.string";
    @Mocked private MicroTask testTaskWaStr;

    private static final String TEST_TASK_WR_STR = "task.withreturn.string";
    @Mocked private MicroTask testTaskWrStr;

    private static final String TEST_TASK_WR_HELLO = "task.withreturn.hello";
    private MicroTask testTaskWrHello = (payloads, output) -> {
        output.pushValue("str", "Hello");
    };

    private static final String EXCEPTION_TASK = "task.exception";
    private MicroTask testTaskException = (payloads, output) -> {
        throw new RuntimeException();
    };

    private static final String TEST_TASK_CLONE_INPUT = "task.cloneinput";
    private MicroTask testTaskCloneInput = (payloads, output) -> {
        Payload str = payloads.get("str");
        output.push("str", str);
    };

    @Before
    public void setUp() {
        microTaskServer = new MicroTaskServer();

        microTaskServer.registerProcess(new MicroProcessBuilder(TEST_TASK, () -> testTask));

        microTaskServer.registerProcess(new MicroProcessBuilder(TEST_TASK_WA_STR, () -> testTaskWaStr).addInput("str", StringPayloadType.NAME));
        microTaskServer.registerProcess(
                new MicroProcessBuilder(TEST_TASK_WR_STR, () -> testTaskWrStr).addOutput("str", StringPayloadType.NAME) );
        microTaskServer.registerProcess(
                new MicroProcessBuilder(TEST_TASK_WR_HELLO, () -> testTaskWrHello).addOutput("str", StringPayloadType.NAME));
        microTaskServer.registerProcess(
                new MicroProcessBuilder(EXCEPTION_TASK, () -> testTaskException).addOutput("str", StringPayloadType.NAME));
        microTaskServer.registerProcess(
                new MicroProcessBuilder(TEST_TASK_CLONE_INPUT, () -> testTaskCloneInput)
                        .addInput("str", StringPayloadType.NAME)
                        .addOutput("str", StringPayloadType.NAME));
    }

    public void testGetChannels() {

        //when
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);

        //then
        Map<String, String> inputChannels = microTaskServer.getInputChannels(TEST_TASK_CLONE_INPUT);
        assertEquals(1, inputChannels.size());
        assertEquals(StringPayloadType.NAME, inputChannels.get("str"));

        //then
        Map<String, String> outputChannel = microTaskServer.getOutputChannels(TEST_TASK_CLONE_INPUT);
        assertEquals(1, inputChannels.size());
        assertEquals(StringPayloadType.NAME, outputChannel.get("str"));
    }

    @Test
    public void testDefaultTaskRunnerNotNull() throws InterruptedException {

        // given
        final Semaphore asyncWait = new Semaphore(1);
        TaskDoneObserver taskDoneObserver = payloadCollection -> asyncWait.release();
        asyncWait.acquire();

        // when
        runTestTask(taskDoneObserver);
        assertTrue(asyncWait.tryAcquire(5000, TimeUnit.MILLISECONDS));

        //then
        checkTaskRun();
    }

    @Test
    public void testUserDefaultTaskRunner(@Mocked MicroTaskRunner runner,
                                          @Mocked TaskDoneObserver taskDoneObserver) {
        // given
        microTaskServer.setDefaultTaskRunner(runner);

        //when
        runTestTask(taskDoneObserver);

        //then
        new Verifications() {
            {
                runner.execute((MicroTaskRunnerContext) any);
                times=1;
            }
        };
    }

    @Test
    public void testRunnerContextValues(@Mocked MicroTaskRunner runner) {
        // given
        microTaskServer.setDefaultTaskRunner(runner);

        //when
        runTestTask();

        //then
        new Verifications() {
            {
                MicroTaskRunnerContext context;
                runner.execute( context = withCapture());
                times=1;

                assertNotNull(context);
                assertEquals(TEST_TASK, context.getTaskId());
            }
        };
    }

    @Test
    public void testUserTaskRunner(@Mocked MicroTaskRunner runner,
                                   @Mocked TaskDoneObserver taskDoneObserver) {
        //given
        microTaskServer.setCustomRunner(TEST_TASK, runner);

        //when
        runTestTask(taskDoneObserver);

        //then
        new Verifications() {
            {
                runner.execute((MicroTaskRunnerContext) any);
                times=1;
            }
        };
    }

    @Test
    public void testMissingInputValues_TaskSkip_DoneObserverCalled(@Mocked TaskDoneObserver taskDoneObserver) throws InterruptedException {

        //given
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        microTaskServer.setDefaultTaskRunner(context -> context.runTask());

        //when
        microTaskServer.beginTask(TEST_TASK_WA_STR)
                .withDoneObserver(taskDoneObserver)
                .execute();

        //then
        new Verifications() {
            {
                testTaskWaStr.execute((PayloadCollection)any, (ChannelCollection) any);
                times=0;

                taskDoneObserver.done((PayloadCollection) any);
                times=1;
            }
        };
    }

    @Test
    public void testTaskException_AlwaysCallsDoneobserver(@Mocked TaskDoneObserver taskDoneObserver) throws InterruptedException {

        //given
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        microTaskServer.setDefaultTaskRunner(context -> context.runTask());

        //when
        microTaskServer.beginTask(EXCEPTION_TASK)
                .withDoneObserver(taskDoneObserver)
                .execute();

        //then
        new Verifications() {
            {
                testTaskWaStr.execute((PayloadCollection)any, (ChannelCollection) any);
                times=0;

                PayloadCollection payload;
                taskDoneObserver.done(payload = withCapture());
                times=1;
                assertNotNull(payload);
                assertTrue(Payload.isNull(payload.get("str")));
            }
        };
    }

    @Test
    public void testInputValues(@Mocked TaskDoneObserver taskDoneObserver) throws InterruptedException {

        //given
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        microTaskServer.setDefaultTaskRunner(context -> context.runTask());

        //when
        microTaskServer.beginTask(TEST_TASK_WA_STR)
                .withDoneObserver(taskDoneObserver)
                .withPayloadProvider(inputChannels -> inputChannels.pushValue("str", "Hello"))
                .execute();

        //then
        new Verifications() {
            {
                PayloadCollection inputs;
                ChannelCollection channelCollection;

                testTaskWaStr.execute(inputs = withCapture(), channelCollection = withCapture());
                times=1;
                assertNotNull(inputs);
                Payload str = inputs.get("str");
                assertNotNull(str);
                assertEquals("Hello", str.getData());
                assertNotNull(channelCollection);

                taskDoneObserver.done((PayloadCollection) any);
                times=1;
            }
        };
    }

    @Test
    public void testOutputValues(@Mocked TaskDoneObserver taskDoneObserver) throws InterruptedException {

        //given
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        microTaskServer.setDefaultTaskRunner(context -> context.runTask());

        //when
        microTaskServer.beginTask(TEST_TASK_WR_HELLO)
                .withDoneObserver(taskDoneObserver)
                .execute();

        //then
        new Verifications() {
            {
                PayloadCollection output;
                taskDoneObserver.done( output = withCapture());
                times=1;
                assertNotNull(output);
                Payload str = output.get("str");
                assertNotNull(str);
                assertEquals("Hello", str.getData());

            }
        };
    }

//    public void testKillProcess() {
//        //TODO
//        fail();
//    }

    @Test
    public void testOutputValues_DefaultsToNull(@Mocked TaskDoneObserver taskDoneObserver) throws InterruptedException {

        //given
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        microTaskServer.setDefaultTaskRunner(context -> context.runTask());

        //when
        microTaskServer.beginTask(TEST_TASK_WR_STR)
                .withDoneObserver(taskDoneObserver)
                .execute();

        //then
        new Verifications() {
            {
                testTaskWrStr.execute((PayloadCollection)any, (ChannelCollection) any);
                times=1;

                PayloadCollection output;
                taskDoneObserver.done( output = withCapture());
                times=1;
                assertNotNull(output);
                Payload str = output.get("str");
                assertNotNull(str);
                assertTrue(Payload.isNull(str));

            }
        };
    }

    @Test
    public void testTaskInputs_ThreadInputs() throws InterruptedException {
        //given
        final String TEST_VALUE = "TestValue";

        //given
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);

        // and
        ThreadWait waitOnChannelInputObject = new ThreadWait();

        class PayloadSetter extends Thread {
            ChannelCollection channelCollection;

            @Override
            public void run() {
                logger.info("Waiting on input channels");
                try {
                    assertTrue(waitOnChannelInputObject.waitOnCall(5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                logger.info("Pushing inputs values");
                channelCollection.pushValue("str", TEST_VALUE);
                channelCollection.fillEmptyChannels();
            }
        }

        final PayloadSetter payloadSetter = new PayloadSetter();
        payloadSetter.start();

        PayloadProvider payloadProvider = inputChannels -> {
            logger.info("Inputs channels received");
            payloadSetter.channelCollection = inputChannels;
            waitOnChannelInputObject.done();
        };

        //when
        class ResultCapture extends AsyncOnDone {

            PayloadCollection payloadCollection;

            public ResultCapture() throws InterruptedException {
                super();
            }

            @Override
            public void done(PayloadCollection payloadCollection) {
                this.payloadCollection = payloadCollection;
                super.done(payloadCollection);
            }
        }
        ResultCapture resultCapture = new ResultCapture();

        microTaskServer.beginTask(TEST_TASK_CLONE_INPUT)
                .withDoneObserver(resultCapture)
                .withPayloadProvider(payloadProvider)
                .execute();

        assertTrue(resultCapture.waitOnCall(2000));
        assertNotNull(resultCapture.payloadCollection);
        Payload str = resultCapture.payloadCollection.get("str");
        assertEquals(TEST_VALUE, str.getData());
    }

    void runTestTask(TaskDoneObserver taskDoneObserver) {
        microTaskServer.beginTask(TEST_TASK)
                .withDoneObserver(taskDoneObserver)
                .execute();
    }

    private void runTestTask() {
        runTestTask(null);
    }

    void checkTaskRun() {
        new Verifications() {
            {
                testTask.execute((PayloadCollection) any, (ChannelCollection) any);
                times=1;
            }
        };
    }


    class AsyncOnDone implements TaskDoneObserver {
        final Semaphore asyncWait = new Semaphore(1);

        public AsyncOnDone() throws InterruptedException {
            asyncWait.acquire();
        }

        @Override
        public void done(PayloadCollection payloadCollection) {
            asyncWait.release();
        }

        public boolean waitOnCall(long millisec) throws InterruptedException {
            return asyncWait.tryAcquire(millisec, TimeUnit.MILLISECONDS);
        }
    }


}