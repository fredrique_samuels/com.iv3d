package org.tect.microtasks;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class ThreadWait {
    final Semaphore asyncWait = new Semaphore(1);

    public ThreadWait() throws InterruptedException {
        asyncWait.acquire();
    }

    public void done() {
        asyncWait.release();
    }

    public boolean waitOnCall(long millisec) throws InterruptedException {
        return asyncWait.tryAcquire(millisec, TimeUnit.MILLISECONDS);
    }
}