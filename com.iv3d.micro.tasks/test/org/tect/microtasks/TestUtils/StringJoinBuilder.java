package org.tect.microtasks.TestUtils;

import org.tect.microtasks.ChannelCollection;
import org.tect.microtasks.MicroProcessBuilder;
import org.tect.microtasks.MicroTask;
import org.tect.microtasks.PayloadCollection;
import org.tect.microtasks.types.StringPayloadType;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by fred on 2017/11/08.
 */
public class StringJoinBuilder extends MicroProcessBuilder {

    public static final String ID = "str.join";
    public static final String INPUT1 = "str.join.input1";
    public static final String INPUT2 = "str.join.input2";
    public static final String OUTPUT = "str.join.output";

    public StringJoinBuilder() {
        super(ID, () -> new Task());
        addInput(INPUT1, StringPayloadType.NAME);
        addInput(INPUT2, StringPayloadType.NAME);
        addOutput(OUTPUT, StringPayloadType.NAME);
    }

    private static class Task implements MicroTask {
        @Override
        public void execute(PayloadCollection payloads, ChannelCollection output) {
            String[] inputs1 = payloads.get(INPUT1).getDataArray(String.class);
            String[] inputs2 = payloads.get(INPUT2).getDataArray(String.class);

            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.addAll(Arrays.asList(inputs1));
            arrayList.addAll(Arrays.asList(inputs2));

            String join = String.join("@", arrayList);
            output.pushValue(OUTPUT, join);
        }
    }
}
