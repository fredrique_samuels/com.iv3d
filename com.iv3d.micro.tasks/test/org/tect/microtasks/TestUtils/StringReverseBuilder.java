package org.tect.microtasks.TestUtils;


import org.apache.log4j.Logger;
import org.tect.microtasks.ChannelCollection;
import org.tect.microtasks.MicroProcessBuilder;
import org.tect.microtasks.MicroTask;
import org.tect.microtasks.PayloadCollection;
import org.tect.microtasks.types.StringPayloadType;

public class StringReverseBuilder extends MicroProcessBuilder {

    static Logger logger = Logger.getLogger(StringReverseBuilder.ID);
    public static final String ID = "str.reverse";
    public static final String INPUT = "str.reverse.input";
    public static final String OUTPUT = "str.reverse.output";

    public StringReverseBuilder() {
        super(ID, () -> new Task());
        addInput(INPUT, StringPayloadType.NAME);
        addOutput(OUTPUT, StringPayloadType.NAME);
    }

    private static class Task implements MicroTask {
        @Override
        public void execute(PayloadCollection payloads, ChannelCollection output) {
            String input = payloads.get(INPUT).getData(String.class);
            output.pushValue(OUTPUT, new StringBuilder(input).reverse().toString());
        }
    }
}
