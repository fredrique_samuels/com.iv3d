package org.tect.microtasks.TestUtils;

import org.tect.microtasks.MicroTaskContext;
import org.tect.microtasks.types.StringPayloadType;

/**
 * Created by fred on 2017/11/08.
 */
public class TestProcessModules {

    public static void registerTypes(MicroTaskContext context) {
        context.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
    }

    static public void registerProcessBuilders(MicroTaskContext context) {
        context.registerProcess(new StringToLowerBuilder());
        context.registerProcess(new StringSplitBuilder());
        context.registerProcess(new StringReverseBuilder());
        context.registerProcess(new StringJoinBuilder());
        context.registerProcess(new TaskWithNonExistingChannelType());
    }
}
