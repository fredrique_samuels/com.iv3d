package org.tect.microtasks.TestUtils;


import org.apache.log4j.Logger;
import org.tect.microtasks.ChannelCollection;
import org.tect.microtasks.MicroProcessBuilder;
import org.tect.microtasks.MicroTask;
import org.tect.microtasks.PayloadCollection;

public class TaskWithNonExistingChannelType extends MicroProcessBuilder {

    static Logger logger = Logger.getLogger(TaskWithNonExistingChannelType.ID);
    public static final String ID = "has.non.existing.channel.type";
    public static final String INPUT = ID + ".input";
    public static final String OUTPUT = ID + ".output";

    public TaskWithNonExistingChannelType() {
        super(ID, () -> new Task());
        addInput(INPUT, "does.not.exists");
        addOutput(OUTPUT, "does.not.exists");
    }

    private static class Task implements MicroTask {
        @Override
        public void execute(PayloadCollection payloads, ChannelCollection output) {
            logger.info("Running " + ID);
        }
    }
}
