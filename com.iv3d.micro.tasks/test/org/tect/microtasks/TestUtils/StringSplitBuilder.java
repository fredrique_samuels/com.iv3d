package org.tect.microtasks.TestUtils;

import org.tect.microtasks.ChannelCollection;
import org.tect.microtasks.MicroProcessBuilder;
import org.tect.microtasks.MicroTask;
import org.tect.microtasks.PayloadCollection;
import org.tect.microtasks.types.StringPayloadType;

/**
 * Created by fred on 2017/11/08.
 */
public class StringSplitBuilder extends MicroProcessBuilder {

    public static final String ID = "str.split";
    public static final String INPUT = "str.split.input";
    public static final String OUTPUT = "str.split.output";

    public StringSplitBuilder() {
        super(ID, () -> new Task());
        addInput(INPUT, StringPayloadType.NAME);
        addOutput(OUTPUT, StringPayloadType.NAME);
    }

    private static class Task implements MicroTask {
        @Override
        public void execute(PayloadCollection payloads, ChannelCollection output) {
            String input = payloads.get(INPUT).getData(String.class);
            output.pushValue(OUTPUT, input.split(" "));
        }
    }
}
