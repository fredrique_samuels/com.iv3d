package org.tect.microtasks.TestUtils;


import org.apache.log4j.Logger;
import org.tect.microtasks.*;
import org.tect.microtasks.types.StringPayloadType;

public class StringToLowerBuilder extends MicroProcessBuilder {

    static Logger logger = Logger.getLogger(StringToLowerBuilder.ID);
    public static final String ID = "str.toLowerCase";
    public static final String INPUT = "str.toLowerCase.input";
    public static final String OUTPUT = "str.toLowerCase.output";

    public StringToLowerBuilder() {
        super(ID, () -> new Task());
        addInput(INPUT, StringPayloadType.NAME);
        addOutput(OUTPUT, StringPayloadType.NAME);
    }

    private static class Task implements MicroTask {
        @Override
        public void execute(PayloadCollection payloads, ChannelCollection output) {
            String input = payloads.get(INPUT).getData(String.class);
            logger.info("Running task with input "+ input);
            output.pushValue(OUTPUT, input.toLowerCase());
        }
    }
}
