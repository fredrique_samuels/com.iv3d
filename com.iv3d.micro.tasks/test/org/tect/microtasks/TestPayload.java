package org.tect.microtasks;

import junit.framework.TestCase;

/**
 * Created by fred on 2017/11/07.
 */
public class TestPayload extends TestCase {

    public void testGetData() {
        Object test = "test";
        Payload payload = Payload.create(new TestType(), test);
        assertEquals("test", payload.getData());
    }

    public void testGetDataAsType() {
        Object test = "test";
        Payload payload = Payload.create(new TestType(), test);
        assertEquals("test", payload.getData(String.class));
    }

    public void testGetDataAsType_CastError() {
        Object test = "test";
        Payload payload = Payload.create(new TestType(), test);
        try {
            payload.getData(Long.class);
            fail("Expected an exception");
        } catch (ClassCastException e) {

        } catch (Exception e) {
            fail();
        }
    }

    public void testGetAsDataType_FromArray_ReturnsFirstItem() {
        Object test = new String[]{"test1", "test2"};
        Payload payload = Payload.create(new TestType(), test);
        assertEquals("test1", payload.getData(String.class));
    }

    public void testGetAsDataType_EmptyFromArray_ReturnsNull() {
        Object test = new String[]{};
        Payload payload = Payload.create(new TestType(), test);
        assertNull(payload.getData(String.class));
    }

    public void testGetArray_String_ReturnsOneItemArray() {
        Payload payload = Payload.create(new TestType(), "test");
        String[] value = payload.getDataArray(String.class);
        assertNotNull(value);
        assertEquals(1, value.length);
        assertEquals("test", value[0]);
    }

    public void testGetArray_FromArray_ReturnsCastedArray() {
        Object test = new String[]{"test1", "test2"};
        Payload payload = Payload.create(new TestType(), test);
        String[] value = payload.getDataArray(String.class);
        assertNotNull(value);
        assertEquals(2, value.length);
        assertEquals("test1", value[0]);
        assertEquals("test2", value[1]);
    }

    private class TestType extends PayloadType {
        @Override
        public String getName() {
            return "test.type";
        }
    }
}
