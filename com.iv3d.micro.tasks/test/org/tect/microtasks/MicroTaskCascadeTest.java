package org.tect.microtasks;


import junit.framework.TestCase;
import org.tect.microtasks.types.ObjectPayloadType;
import org.tect.microtasks.types.StringPayloadType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class MicroTaskCascadeTest extends TestCase {


    public void testToLowerAndSplit() throws InterruptedException {
        /**
         * Testing configuration of two task where the output of one feeds the input of another.
         * Example :
         *      We will perform two operations on the string. First we will
         *      convert it to lower case and then do a split.
         *
         * "TestValue" -> |toLower| -> |split| -> ["test","value"]
         */
        final MicroTaskServer microTaskServer = createMicroTaskServer();
        final String TO_LOWER_TASK = registerStrToLowerTask(microTaskServer);
        final String SPLIT_TASK = registerStrSplitTask(microTaskServer);

        final ThreadWait waitForTestToComplete = new ThreadWait();

        TaskDoneObserver onSplitDone = payloadCollection -> {
            String[] outputs = payloadCollection.get("output").getDataArray(String.class);
            assertNotNull(outputs);
            assertEquals(2, outputs.length);
            assertEquals("test", outputs[0]);
            assertEquals("value", outputs[1]);
            waitForTestToComplete.done();
        };

        TaskDoneObserver runSplit = payloadCollection -> {
            String output = payloadCollection.get("output").getData(String.class);
            assertNotNull(output);
            microTaskServer.beginTask(SPLIT_TASK)
                    .withDoneObserver(onSplitDone)
                    .withPayloadProvider(inputChannels -> inputChannels.pushValue("input", output))
                    .execute();
        };

        microTaskServer.beginTask(TO_LOWER_TASK)
                .withDoneObserver(runSplit)
                .withPayloadProvider(inputChannels -> inputChannels.pushValue("input", "Test Value"))
                .execute();
        assertTrue(waitForTestToComplete.waitOnCall(5000));
    }

    public void testMultiCascadeTasks() throws InterruptedException {

        /**
         * Testing configuration of two task where the output of one feeds the input of another.
         * Example :
         *      We will perform two operations on the string. First we will
         *      convert it to lower case and then do a split.
         *
         * "Test Value" -> |toLower| -> |split| -> ["test","value"] -> |concatAt| -> "test@value@eulav tset"
         * =========================|==============================|
         * =========================-> |reverse| -> ["eulavtset"] -|
         */
        final MicroTaskServer microTaskServer = createMicroTaskServer();
        final String TO_LOWER_TASK = registerStrToLowerTask(microTaskServer);
        final String SPLIT_TASK = registerStrSplitTask(microTaskServer);
        final String REVERSE_TASK = registerStrReverseTask(microTaskServer);
        final String CONCAT_TASK = registerStrConcatTask(microTaskServer);


        final ThreadWait waitForTestToComplete = new ThreadWait();
        class RunConcat {
            private final TaskExecutor taskExecutor;
            private final PlP payloadProvider;

            Boolean isRunning = false;

            public RunConcat() {
                this.payloadProvider = new PlP();
                taskExecutor = microTaskServer.beginTask(CONCAT_TASK)
                        .withPayloadProvider(payloadProvider)
                        .withDoneObserver(output -> {
                            String output1 = output.get("output").getData(String.class);
                            assertNotNull(output1);
                            assertEquals("test@value@eulav tset", output1);
                            waitForTestToComplete.done();
                        });
            }

            public void run() {
                synchronized (isRunning) {
                    if (isRunning) {
                        return;
                    }
                    isRunning = true;
                    taskExecutor.execute();
                }
            }

            public void pushValue(String channel, Object value) {
                payloadProvider.pushValue(channel, value);
            }

            class PlP implements PayloadProvider {
                private ChannelCollection inputChannels;
                private Map<String, Object> queuedPayload;
                private Semaphore semaphore = new Semaphore(1);

                public PlP() {
                    queuedPayload = new HashMap<>();
                }

                @Override
                public void provideFor(ChannelCollection inputChannels) {
                    try {
                        semaphore.acquire();
                        this.inputChannels = inputChannels;
                        queuedPayload.entrySet().forEach(es -> inputChannels.pushValue(es.getKey(), es.getValue()));
                        queuedPayload.clear();
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                public void pushValue(String channel, Object value) {
                    try {
                        semaphore.acquire();
                        if(this.inputChannels!=null) {
                            inputChannels.pushValue(channel, value);
                        } else {
                            if(!queuedPayload.containsKey(channel)) {
                                queuedPayload.put(channel, value);
                            }
                        }
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        RunConcat runConcat = new RunConcat();


        TaskDoneObserver toLowerDone = new TaskDoneObserver() {
            @Override
            public void done(PayloadCollection output) {
                microTaskServer.beginTask(SPLIT_TASK)
                        .withDoneObserver(output12 -> {
                            runConcat.run();
                            runConcat.pushValue("input1", output12.get("output").getData());
                        })
                        .withPayloadProvider(inputChannels -> inputChannels.pushValue("input", output.get("output").getData()))
                        .execute();

                microTaskServer.beginTask(REVERSE_TASK)
                        .withDoneObserver(output1 -> {
                            runConcat.run();
                            runConcat.pushValue("input2", output1.get("output").getData());
                        })
                        .withPayloadProvider(inputChannels -> inputChannels.pushValue("input", output.get("output").getData()))
                        .execute();
            }
        };
        PayloadProvider toLowerPayload = inputChannels -> inputChannels.pushValue("input", "Test Value");
        microTaskServer.beginTask(TO_LOWER_TASK)
            .withDoneObserver(toLowerDone)
            .withPayloadProvider(toLowerPayload)
            .execute();
        assertTrue(waitForTestToComplete.waitOnCall(5000));
    }

    private String registerStrConcatTask(MicroTaskServer microTaskServer) {
        MicroTask task = (payloads, output) -> {
            String[] inputs1 = payloads.get("input1").getDataArray(String.class);
            String[] inputs2 = payloads.get("input2").getDataArray(String.class);
            assertNotNull(inputs1);
            assertNotNull(inputs2);

            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.addAll(Arrays.asList(inputs1));
            arrayList.addAll(Arrays.asList(inputs2));

            String join = String.join("@", arrayList);
            System.out.println(join);
            output.pushValue("output", join);
        };

        final String taskId = "cancat";
        MicroProcessBuilder builder = new MicroProcessBuilder(taskId, () -> task)
                .addInput("input1", StringPayloadType.NAME)
                .addInput("input2", StringPayloadType.NAME)
                .addOutput("output", ObjectPayloadType.NAME);

        microTaskServer.registerProcess(builder);
        return taskId;
    }

    private String registerStrReverseTask(MicroTaskServer microTaskServer) {
        MicroTask splitStringTask = (payloads, output) -> {
            String input = payloads.get("input").getData(String.class);
            assertNotNull(input);
            output.pushValue("output", new StringBuilder(input).reverse().toString());
        };

        final String REVERSE_TASK = "reverse";
        microTaskServer.registerProcess(new MicroProcessBuilder(REVERSE_TASK, () -> splitStringTask)
                .addInput("input", StringPayloadType.NAME)
                .addOutput("output", ObjectPayloadType.NAME));
        return REVERSE_TASK;
    }

    private String registerStrSplitTask(MicroTaskServer microTaskServer) {
        MicroTask splitStringTask = (payloads, output) -> {
            String input = payloads.get("input").getData(String.class);
            assertNotNull(input);
            String[] split = input.split(" ");
            output.pushValue("output", split);
        };

        final String SPLIT_TASK = "split";
        microTaskServer.registerProcess(new MicroProcessBuilder(SPLIT_TASK, () -> splitStringTask)
                .addInput("input", StringPayloadType.NAME)
                .addOutput("output", ObjectPayloadType.NAME));
        return SPLIT_TASK;
    }

    private String registerStrToLowerTask(MicroTaskServer microTaskServer) {
        MicroTask toLowerTask = (payloads, output) -> {
            String input = payloads.get("input").getData(String.class);
            output.pushValue("output", input.toLowerCase());
        };

        final String TO_LOWER_TASK = "tolower";
        microTaskServer.registerProcess(new MicroProcessBuilder(TO_LOWER_TASK, () -> toLowerTask)
                .addInput("input", StringPayloadType.NAME)
                .addOutput("output", StringPayloadType.NAME));
        return TO_LOWER_TASK;
    }

    private MicroTaskServer createMicroTaskServer() {
        MicroTaskServer microTaskServer = new MicroTaskServer();
        microTaskServer.registerPayloadType(ObjectPayloadType.NAME, ObjectPayloadType.FACTORY);
        microTaskServer.registerPayloadType(StringPayloadType.NAME, StringPayloadType.FACTORY);
        return microTaskServer;
    }

}
