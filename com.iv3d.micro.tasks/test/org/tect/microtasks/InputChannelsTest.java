/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.microtasks;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

public class InputChannelsTest extends TestCase {

    private static final PayloadType DEMO_TYPE_INT = new PayloadType() {
        @Override
        public String getName() {
            return "demo.type.1";
        }

        @Override
        public boolean validateData(Object data) {
            return data instanceof Integer;
        }
    };

    private static final PayloadType DEMO_TYPE_2 = new PayloadType() {
        @Override
        public String getName() {
            return "demo.type.2";
        }
    };

    private static final PayloadType EXCEPTION_THROWER_VALIDATE_TYPE = new PayloadType() {
        @Override
        public String getName() {
           return "Some Name";
        }

        @Override
        public boolean validateData(Object data) {
            throw new RuntimeException();
        }
    };

    private static final String CHANNEL_NAME_1 = "channel.1";
    private static final String CHANNEL_NAME_2 = "channel.2";

    @Test
    public void testPush_WithNoChannels() throws Exception {
        //given
        InputChannels inputChannels = InputChannels.create()
                .build();

        //then
        assertNotNull(inputChannels);

        Collection<Channel> channels = inputChannels.getChannels();
        assertNotNull(channels);
        assertEquals(0, channels.size());
    }

    @Test
    public void testAddChannels() throws Exception {
        //given
        InputChannels inputChannels = InputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_2)
                .build();

        //then
        assertNotNull(inputChannels);

        List<Channel> channels = inputChannels.getChannels();
        assertNotNull(channels);
        assertEquals(1, channels.size());


        Channel channel = channels.get(0);

        assertNotNull(channel);
        assertNull(null, channel.getPayload());
        assertEquals(CHANNEL_NAME_1, channel.getId());
        assertEquals(DEMO_TYPE_2, channel.getType());
        assertEquals(ChannelStatus.WAITING, channel.getState());
    }

    @Test
    public void testGetPayload() throws Exception {
        //given
        InputChannels inputChannels = InputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .build();
        {
            PayloadCollection payload = inputChannels.getPayload();
            assertNotNull(payload.get(CHANNEL_NAME_1));
            assertEquals(PayloadType.nullValue(), payload.get(CHANNEL_NAME_1).getType());
        }

        //when
        inputChannels.push(CHANNEL_NAME_1, Payload.create(DEMO_TYPE_INT, Integer.valueOf(30)));

        //then
        Payload payload = inputChannels.getPayload().get(CHANNEL_NAME_1);
        assertNotNull(payload);
        assertEquals(DEMO_TYPE_INT, payload.getType());
    }

    @Test
    public void testPush_InvalidPayload() throws Exception {
        //given
        InputChannels inputChannels = InputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .build();

        //when
        {
            Payload payload = Payload.create(DEMO_TYPE_INT, Long.valueOf(30));
            inputChannels.push(CHANNEL_NAME_1, payload);
        }

        //then
        assertNullValue(inputChannels, CHANNEL_NAME_1);
    }

    @Test
    public void testPush_InvalidPayloadType() throws Exception {
        //given
        InputChannels inputChannels = InputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_2)
                .build();
        //when
        {
            Payload payload = Payload.create(DEMO_TYPE_INT, Integer.valueOf(30));
            inputChannels.push(CHANNEL_NAME_1, payload);
        }

        //then
        assertNullValue(inputChannels, CHANNEL_NAME_1);
    }

    void assertNullValue(InputChannels inputChannels, String channelName1) {
        PayloadCollection payloadCollection = inputChannels.getPayload();
        assertNull(payloadCollection.get(channelName1).getData());
    }

    @Test
    public void testJoin() throws Exception {
        //given
        InputChannels inputChannels = InputChannels.create()
                .addChannel(CHANNEL_NAME_1, DEMO_TYPE_INT)
                .addChannel(CHANNEL_NAME_2, DEMO_TYPE_INT)
                .build();

        inputChannels.push(CHANNEL_NAME_1, Payload.create(DEMO_TYPE_INT, Integer.valueOf(30)));
        inputChannels.push(CHANNEL_NAME_2, Payload.create(DEMO_TYPE_INT, Integer.valueOf(15)));

        //when
        PayloadCollection join = inputChannels.join();

        //then
        assertEquals(30, join.get(CHANNEL_NAME_1).getData());
        assertEquals(15, join.get(CHANNEL_NAME_2).getData());
    }
}