package org.tect.microtasks;

import junit.framework.TestCase;
import mockit.Verifications;
import org.junit.Test;
import org.tect.microtasks.TestUtils.StringSplitBuilder;
import org.tect.microtasks.TestUtils.StringToLowerBuilder;
import org.tect.microtasks.TestUtils.TestProcessModules;

public class TaskStatusMonitorTests extends TestCase {

    private MicroTaskServer microTaskServer;

    public void setUp() {
        microTaskServer = new MicroTaskServer();
        TestProcessModules.registerProcessBuilders(microTaskServer);
        TestProcessModules.registerTypes(microTaskServer);
    }

    @Test
    public void testSingleRun(StatusUpdateConsumer statusConsumer) throws InterruptedException {

        ThreadWait threadWait = new ThreadWait();
        microTaskServer.beginTask(StringToLowerBuilder.ID)
            .withStatusConsumer(statusConsumer)
            .withDoneObserver((pl) -> threadWait.done())
            .withPayloadProvider(pl -> pl.pushValue(StringToLowerBuilder.INPUT, "hello"))
            .execute();

        assertTrue(threadWait.waitOnCall(2000));

        new Verifications() {
            {
                statusConsumer.onInputChannelStatusChange(StringToLowerBuilder.INPUT, ChannelStatus.WAITING);
                times=1;

                statusConsumer.onOutputChannelStatusUpdate(StringToLowerBuilder.OUTPUT, ChannelStatus.WAITING);
                times=1;

                statusConsumer.onInputChannelStatusChange(StringToLowerBuilder.INPUT, ChannelStatus.OK);
                times=1;

                statusConsumer.onOutputChannelStatusUpdate(StringToLowerBuilder.OUTPUT, ChannelStatus.OK);
                times=1;

                statusConsumer.onTaskStatusChange(TaskStatus.WAITING);
                times=1;

                statusConsumer.onTaskStatusChange(TaskStatus.RUNNING);
                times=1;

                statusConsumer.onTaskStatusChange(TaskStatus.OK);
                times=1;
            }
        };
    }

    @Test
    public void testConfigRunMonitor(ConfigExecutorStatusConsumer statusConsumer) throws InterruptedException {
        TaskExecutorConfigBuilder builder = new TaskExecutorConfigBuilder(microTaskServer)
                .setStatusConsumer(statusConsumer);
        Long toLower = builder.addTask(StringToLowerBuilder.ID);
        TaskExecutorConfig config = builder.build();

        ThreadWait threadWait = new ThreadWait();
        microTaskServer.beginTask(config)
                .withDoneObserver((pl) -> threadWait.done())
                .withPayloadProvider(pl -> pl.pushValue(StringToLowerBuilder.INPUT, "hello"))
                .execute();

        assertTrue(threadWait.waitOnCall(2000));

        new Verifications() {
            {
                statusConsumer.onInputChannelStatusChange(toLower, StringToLowerBuilder.INPUT, ChannelStatus.WAITING);
                times=1;

                statusConsumer.onOutputChannelStatusUpdate(toLower, StringToLowerBuilder.OUTPUT, ChannelStatus.WAITING);
                times=1;

                statusConsumer.onInputChannelStatusChange(toLower, StringToLowerBuilder.INPUT, ChannelStatus.OK);
                times=1;

                statusConsumer.onOutputChannelStatusUpdate(toLower, StringToLowerBuilder.OUTPUT, ChannelStatus.OK);
                times=1;

                statusConsumer.onTaskStatusChange(toLower, TaskStatus.WAITING);
                times=1;

                statusConsumer.onTaskStatusChange(toLower, TaskStatus.RUNNING);
                times=1;

                statusConsumer.onTaskStatusChange(toLower, TaskStatus.OK);
                times=1;
            }
        };

    }
}
