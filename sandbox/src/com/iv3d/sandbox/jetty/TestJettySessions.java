//
//  ========================================================================
//  Copyright (c) 1995-2017 Mort Bay Consulting Pty. Ltd.
//  ------------------------------------------------------------------------
//  All rights reserved. This program and the accompanying materials
//  are made available under the terms of the Eclipse Public License v1.0
//  and Apache License v2.0 which accompanies this distribution.
//
//      The Eclipse Public License is available at
//      http://www.eclipse.org/legal/epl-v10.html
//
//      The Apache License v2.0 is available at
//      http://www.opensource.org/licenses/apache2.0.php
//
//  You may elect to redistribute this code under either of these licenses.
//  ========================================================================
//

package com.iv3d.sandbox.jetty;

import com.app.server.base.server.HttpSessionService;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;

public class TestJettySessions
{
    public static void main( String[] args ) throws Exception
    {
        Server server = new Server( 58100 );


        // Specify the Session ID Manager
        HashSessionIdManager idmanager = new HashSessionIdManager();
        server.setSessionIdManager(idmanager);

        SessionManager sessionManager = new HashSessionManager();
        sessionManager.setSessionIdPathParameterName("./sessions");

        HttpSessionService sessionService = new HttpSessionService(sessionManager);
        sessionService.setSessionManager(sessionManager);
        sessionService.setSessionAttributesPopulate((session) -> session.setAttribute("USERID", 300));

        // Add a single handler on context "/hello"

        ContextHandler helloHandler = new ContextHandler();
        {
            helloHandler.setContextPath("/hello");
            helloHandler.setHandler(new HelloHandler(sessionService));
        }

        ContextHandler loginHandler = new ContextHandler();
        {
            loginHandler.setContextPath("/login");
            loginHandler.setHandler(new LoginHandler(sessionService));
        }

        ContextHandler logoutHandler = new ContextHandler();
        {
            logoutHandler.setContextPath("/logout");
            logoutHandler.setHandler(new LogoutHandler(sessionService));
        }


        HandlerList handlerList = new HandlerList();
        handlerList.setHandlers(new Handler[]{
            getSessionHandler(sessionManager, helloHandler),
            getSessionHandler(sessionManager, loginHandler),
            getSessionHandler(sessionManager, logoutHandler)});

        // Can be accessed using http://localhost:8080/hello


        server.setHandler( handlerList );

        // Start the server
        server.start();
        server.join();
    }

    private static SessionHandler getSessionHandler(SessionManager sessionManager, ContextHandler helloHandler) {
        SessionHandler sessionHandler = new SessionHandler(sessionManager);
        sessionHandler.setHandler(helloHandler);
        return sessionHandler;
    }
}