//
//  ========================================================================
//  Copyright (c) 1995-2017 Mort Bay Consulting Pty. Ltd.
//  ------------------------------------------------------------------------
//  All rights reserved. This program and the accompanying materials
//  are made available under the terms of the Eclipse Public License v1.0
//  and Apache License v2.0 which accompanies this distribution.
//
//      The Eclipse Public License is available at
//      http://www.eclipse.org/legal/epl-v10.html
//
//      The Apache License v2.0 is available at
//      http://www.opensource.org/licenses/apache2.0.php
//
//  You may elect to redistribute this code under either of these licenses.
//  ========================================================================
//

package com.iv3d.sandbox.jetty;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.app.server.base.server.HttpSessionService;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class HelloHandler extends AbstractHandler
{
    private final HttpSessionService sessionService;

    public HelloHandler(HttpSessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void handle( String target,
                        Request baseRequest,
                        HttpServletRequest request,
                        HttpServletResponse response ) throws IOException,
                                                      ServletException
    {

        HttpSession session = sessionService.getActiveSession(request);

        if(session==null) {
            returnResult(baseRequest, response, "<h1>You are not logged in </h1>");
            return;
        }

        if (session.isNew()) {
            System.out.printf("New Session: %s%n", session.getId());
        } else {
            System.out.printf("Old Session: %s%n", session.getId());
        }

        returnResult(baseRequest, response, "<h1>Hello from session " + session.getId() + " USERID"+ session.getAttribute("USERID") + "</h1>");
    }

    private void returnResult(Request baseRequest, HttpServletResponse response, String x) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = response.getWriter();
        out.println(x);
        baseRequest.setHandled(true);
    }
}