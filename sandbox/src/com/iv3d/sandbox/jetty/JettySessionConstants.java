package com.iv3d.sandbox.jetty;

import org.eclipse.jetty.server.SessionManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by fred on 2017/09/18.
 */
public class JettySessionConstants {
    public static final String JSESSIONID = "TVD_JSESSIONID";

    public static String getCurrentSessionId(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie c:cookies) {
            if(c.getName().equals(JSESSIONID)) {
                return c.getValue();
            }
        }
        return null;
    }

    public static void setSessionCookie(HttpServletResponse response, HttpSession session) {
        Cookie userCookie = new Cookie(JettySessionConstants.JSESSIONID, session.getId());
        response.addCookie(userCookie);
    }

    public static String closeSession(SessionManager sessionManager, HttpServletRequest request) {
        String jsessionid = JettySessionConstants.getCurrentSessionId(request);
        if(jsessionid != null)
        {
            HttpSession session = sessionManager.getHttpSession(jsessionid);
            if(session!=null) {
                String id = session.getId();
//                System.out.println("Ending Session "+ id);
                session.invalidate();
            }
        }
        return jsessionid;
    }
}
