package com.iv3d.sandbox.jetty;

import com.app.server.base.server.HttpSessionService;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by fred on 2017/07/31.
 */
public class LoginHandler  extends AbstractHandler {

    private HttpSessionService sessionService;

    public LoginHandler(HttpSessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void handle(String target, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        System.err.println("Login .....");
        HttpSession session = sessionService.login(request, httpServletRequest, httpServletResponse);

        httpServletResponse.addHeader("Set-Cookie","lang=en-US; Path=/;");
        httpServletResponse.setContentType("text/html; charset=utf-8");
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = httpServletResponse.getWriter();
        out.println("<h1> started session" + session.getId() + "</h1>");

        request.setHandled(true);
    }
}
