package com.iv3d.sandbox.jetty;

import com.app.server.base.server.HttpSessionService;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by fred on 2017/07/31.
 */
public class LogoutHandler extends AbstractHandler {

    private HttpSessionService sessionService;

    public LogoutHandler(HttpSessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void handle(String target, Request request, HttpServletRequest httpServletRequest, HttpServletResponse response) throws IOException, ServletException {
        String sesssionIfAny = sessionService.logout(httpServletRequest);

        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = response.getWriter();
        out.println("<h1> Closed session" + sesssionIfAny + "</h1>");
        request.setHandled(true);
    }
}
