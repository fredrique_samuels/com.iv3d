package com.iv3d.sandbox;

import com.iv3d.common.rest.HttpRestClient;

/**
 * Created by fred on 2017/07/14.
 */
public class TestHttpClient {
    public static void main(String[] args) {
        HttpRestClient httpRestClient = new HttpRestClient();
        String s = httpRestClient.get("http://localhost:58080/server.admin/jobs/status?jobId=2&runId=1");
        System.out.println(s);
    }
}
