package com.iv3d.sandbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

interface Apple {
    String getDisplay();
}

class CommittableAppleBuilder<T> {
    public enum AppleColor {
        RED,
        GREEN
    }
    private final Function<Apple, T> commitReceiver;
    private AppleColor color;

    public CommittableAppleBuilder(Function<Apple, T> commitReceiver) {
        this.commitReceiver = commitReceiver;
        this.color = AppleColor.GREEN;
    }

    final CommittableAppleBuilder<T> setGreen(){
        this.color = AppleColor.GREEN;
        return this;
    }
    final CommittableAppleBuilder<T> setRed(){
        this.color = AppleColor.RED;
        return this;
    }

    final Apple build(){return new AppleImpl(this);}
    final T commit(){
        if(commitReceiver==null)
            return null;
        return commitReceiver.apply(this.build());
    }

    class AppleImpl implements Apple {
        private final AppleColor color;

        @Override
        public String getDisplay() {
            return new StringBuilder()
                    .append("<Apple ")
                    .append("color=")
                    .append(color.toString())
                    .append(" >")
                    .toString();
        }

        public AppleImpl(CommittableAppleBuilder builder) {
            this.color = builder.color;
        }
    }
}

final class AppleBuilder extends CommittableAppleBuilder {

    public AppleBuilder() {
        super(null);
    }
}

interface AppleBasket {
    String getDisplay();
}

class AppleBasketBuilder {
    private final List<Apple> apples = new ArrayList<>();

    public CommittableAppleBuilder<AppleBasketBuilder> createApple(){
        return new CommittableAppleBuilder<>((apple) -> {addApple(apple);return this;} );
    }

    private AppleBasketBuilder addApple(Apple apple){
        apples.add(apple);
        return this;
    }

    final public AppleBasket build() {
        return new AppleBasketImpl(this);
    }


    class AppleBasketImpl implements AppleBasket {
        private final List<Apple> apples;

        public AppleBasketImpl(AppleBasketBuilder builder) {
            this.apples = Collections.unmodifiableList(builder.apples);
        }

        @Override
        public String getDisplay() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder = stringBuilder
                    .append("<AppleBasket ")
                    .append("size=")
                    .append(apples.size())
                    .append(" ");
            for (Apple apple: apples ) {
                stringBuilder = stringBuilder.append(apple.getDisplay());
            }
            return stringBuilder.append(" >")
                    .toString();
        }
    }
}


public class BuilderDemo {
    public static void main(String[] args) {
        {
            Apple apple = new AppleBuilder()
                    .setRed()
                    .build();
            System.out.println(apple.getDisplay());
        }

        {
            Apple apple = new AppleBuilder()
                    .setGreen()
                    .build();
            System.out.println(apple.getDisplay());
        }

        AppleBasket appleBasket = new AppleBasketBuilder()
                .createApple()
                .setRed()
                .commit()

                .createApple()
                .setGreen()
                .commit()

                .build();

        System.out.println(appleBasket.getDisplay());
    }
}
