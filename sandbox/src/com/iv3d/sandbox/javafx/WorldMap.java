/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.core.MapCoords;
import com.iv3d.common.core.PointDouble;
import com.iv3d.common.rest.HttpRestClient;
import com.iv3d.common.storage.db.SqliteConnection;
import com.iv3d.sandbox.javafx.maps.DefaulMapsSvgReaderDao;
import com.iv3d.sandbox.javafx.maps.MapsSvgReaderDao;
import com.iv3d.sandbox.javafx.maps.common.SphereMercatorCalculator;
import com.iv3d.sandbox.javafx.maps.domain.CountrySvg;
import com.iv3d.sandbox.javafx.maps.domain.WorldSvg;
import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.List;

public class WorldMap extends Application {
	public static void main(String[] args) {
        launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

	    new HttpRestClient();
		
		SqliteConnection db = new SqliteConnection("jdbc:sqlite:./cache/com.iv3d.maps.svg/mapsvg.db");
		MapsSvgReaderDao readerDao = new DefaulMapsSvgReaderDao(db);
		
		WorldSvg world = readerDao.getWorldLowRes();
		List<CountrySvg> countries = readerDao.getCountries(world.getId());
		
		
		primaryStage.setTitle("Drawing Operations Test");

        BorderPane root = new BorderPane();
        
		Canvas canvas = new Canvas(1024, 768);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        
        gc.setFill(Color.LIGHTBLUE);
        gc.setStroke(Color.BLUE);
        
        //latitude:40.3951, longitude:-73.5619
       
        String targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

        double bottomLatitude = world.getBottomLatitude();
        double topLatitude = world.getTopLatitude();
        double leftLongitude = world.getLeftLongitude();
        double rightLongitude = world.getRightLongitude();
        
        BoundingBoxDouble worldBoundingBox = world.getBoundingBox();
		SphereMercatorCalculator smc = new SphereMercatorCalculator(new MapCoords(topLatitude, leftLongitude),
        		new MapCoords(bottomLatitude, rightLongitude),
        		worldBoundingBox.getMaxX(), 
        		worldBoundingBox.getMaxY());
		
        for (CountrySvg countrySvg : countries) {
        	 SVGPath path = new SVGPath();
             path.setContent(countrySvg.getSvgPath());
             path.setStroke(Color.BLACK);
             root.getChildren().add(path);
		}
      
        showLocation(root, targetSVG, smc, new MapCoords(0, 0));
        showLocation(root, targetSVG, smc, new MapCoords(40.3951,-73.5619));
        showLocation(root, targetSVG, smc, new MapCoords(64.1353, -21.8952));
        
  
        SVGPath path = new SVGPath();
        path.setStroke(Color.BLUE);
        String icon= "M21.25,8.375V28h6.5V8.375H21.25zM12.25,28h6.5V4.125h-6.5V28zM3.25,28h6.5V12.625h-6.5V28z";
        path.setContent(icon);
        root.getChildren().add(path);
        
        FadeTransition ft = new FadeTransition(Duration.millis(1000), path);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();
        
//        addFadeRect(root);
        primaryStage.setScene(new Scene(root, 1000, 768));
        primaryStage.show();
	}

	private void showLocation(BorderPane root, String targetSVG, SphereMercatorCalculator smc, MapCoords latLng) {
		PointDouble zeroZero = smc.latLngToPoint(latLng);
        {
        	SVGPath path = new SVGPath();
        	path.setContent(targetSVG);
        	Bounds boundsInLocal = path.getBoundsInLocal();
        	path.setTranslateX(zeroZero.getX()-(boundsInLocal.getWidth()*.5));
        	path.setTranslateY(zeroZero.getY()-(boundsInLocal.getHeight()*.5));
            path.setStroke(Color.YELLOW);
            path.setFill(Color.YELLOW);
            root.getChildren().add(path);
        }
	}

	public void addFadeRect(Group root) {
		final Rectangle rectFade = new Rectangle(10, 150, 100, 100);
        rectFade.setArcHeight(20);
        rectFade.setArcWidth(20);
        rectFade.setFill(Color.RED);
        
        FadeTransition ft = new FadeTransition(Duration.millis(3000), rectFade);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();
        
        root.getChildren().add(rectFade);
	}
}
