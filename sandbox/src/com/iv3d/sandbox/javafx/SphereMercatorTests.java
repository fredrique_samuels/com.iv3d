/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx;

import com.iv3d.common.core.MapCoords;
import com.iv3d.common.core.PointDouble;
import com.iv3d.sandbox.javafx.maps.common.SphereMercatorCalculator;
import com.iv3d.sandbox.javafx.maps.common.SphericalMercatorProjection;

public class SphereMercatorTests {
	
	public static void main(String[] args) {
		
		double width = 1009.4398803710938;
		double height = 651.9571533203125;
		
		double leftLongitude = -169.6;
		double topLatitude = 83.68;
		
		double rightLongitude = 190.25;
		double bottomLatitude = -55.55;
		
		SphericalMercatorProjection sphericalMercatorProjection = new SphericalMercatorProjection(100);
		
		PointDouble topLeftPerc = sphericalMercatorProjection.toPoint(new MapCoords(topLatitude, leftLongitude));
        PointDouble bottomRightPerc = sphericalMercatorProjection.toPoint(new MapCoords(bottomLatitude, rightLongitude));
       
        System.out.println("===========================");
        System.out.println(topLeftPerc);
        System.out.println(bottomRightPerc);
        
        
        System.out.println("=========CALC IMAGE SIZE=========");
        double xPerc = bottomRightPerc.getX()*.01;
//        double totalMapWidth = (width/bottomRightPerc.getX())*100;
//        double totalMapHeight = (height/bottomRightPerc.getY())*100;
        
        double pppx =  (bottomRightPerc.getX()-topLeftPerc.getX());
        double normalizedWidth = (width/pppx)*100;
		System.out.println("nw " +  normalizedWidth);
		
		double pppy =  (bottomRightPerc.getY()-topLeftPerc.getY());
        double normalizedHeight= (height/pppy)*100;
		System.out.println("nh " +  normalizedHeight);
		
		System.out.println("=========OFFSET=========");
		double offsetX = normalizedWidth * (topLeftPerc.getX()*0.01);
		double offsetY = normalizedHeight * (topLeftPerc.getY()*0.01);
		System.out.println("offsetX " + offsetX);
		System.out.println("offsetY " + offsetY);
        
        System.out.println("===Zero Zero==========");
        {
	        PointDouble point = sphericalMercatorProjection.toPoint(new MapCoords(0,0));
	        System.out.println(point);
        }
        
        {
	        PointDouble point2 = sphericalMercatorProjection.toPoint(new MapCoords(bottomLatitude,rightLongitude));
	        System.out.println((point2.getX()*0.01)*normalizedWidth-offsetX);
	        System.out.println((point2.getY()*0.01)*normalizedHeight-offsetY);
        }
        
        PointDouble point3 = sphericalMercatorProjection.toPoint(new MapCoords(topLatitude,leftLongitude));
        System.out.println((point3.getX()*0.01)*normalizedWidth-offsetX);
        System.out.println((point3.getY()*0.01)*normalizedHeight-offsetY);
        
        {
	        PointDouble point4 = sphericalMercatorProjection.toPoint(new MapCoords(0,0));
	        System.out.println((point4.getX()*0.01)*normalizedWidth-offsetX);
	        System.out.println((point4.getY()*0.01)*normalizedHeight-offsetY);
	        
	        SphericalMercatorProjection smp = new SphericalMercatorProjection(normalizedWidth, normalizedHeight);
	        PointDouble point = smp.toPoint(new MapCoords(0,0));
	        System.out.println(point.getX()-offsetX);
	        System.out.println(point.getY()-offsetY);
	        
	        
	        //WH  650.9400024414062
	        //WW  1008.27001953125
	        SphereMercatorCalculator smc = new SphereMercatorCalculator(
	        		new MapCoords(topLatitude,leftLongitude),
	        		new MapCoords(bottomLatitude,rightLongitude),
	        		1008.27001953125, 650.9400024414062);
	        PointDouble latLngToPoint = smc.latLngToPoint(new MapCoords(0,0));
	        System.out.println(smc);
	        System.out.println(latLngToPoint.getX());
	        System.out.println(latLngToPoint.getY());
	        
        }
        
        
	}
}
