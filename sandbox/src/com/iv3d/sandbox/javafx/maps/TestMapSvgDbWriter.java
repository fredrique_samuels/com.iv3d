/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

class TestMapSvgDbWriter {

//	private String jsSvgPath;
////	private MapsSvgWriterDao dao;
//	private MapsSvgWriterDao writerDao;
//	private MapsSvgReaderDao readerDao;
//
//	public TestMapSvgDbWriter(String jsSvgPath,
//			MapsSvgWriterDao writerDao,
//			MapsSvgReaderDao readerDao) {
//		this.jsSvgPath = jsSvgPath;
//		this.writerDao = writerDao;
//		this.readerDao = readerDao;
//	}
//	
//	public void run() {
//		writeContinents();
//		writeCountryAndRegions();
//		System.err.println("Write successfull");
//	}
//
//	private void writeContinents() {
//		SvgMap svg = getSvgFromString(new File(jsSvgPath, "worldLow.js"));
//		AMAP amap = svg.getAmap();
//		writerDao.saveAmapAsWorld(MapsDaoUtils.WORLD_NAME_LOW_RES, amap);
//	}
//
//	private void writeCountryAndRegions() {
//		File root = new File(jsSvgPath);
//		File[] listFiles = root.listFiles();
//		
//		for (File file : listFiles) {
//			if(file.isFile() && !exclude(file))
//				processFileAsCountry(file);
//		}
//	}
//
//	private boolean exclude(File file) {
//
//		String[] excludes = new String[]{
//				"worldHigh.js",
//				"worldIndiaHigh.js",
//				"worldIndiaLow.js",
//				"worldKashmir2Low.js",
//				"worldKashmir2WithAntarcticaLow.js",
//				"worldLow.js",
//				"worldRussiaSplitHigh.js",
//				"worldRussiaSplitLow.js",
//				"worldRussiaSplitWithAntarcticaHigh.js",
//				"worldRussiaSplitWithAntarcticaLow.js",
//				"usa2High.js",
//				"usa2Low.js",
//				"usaHigh.js",
//				"usaLow.js",
//				"usaTerritories2High.js",
//				"usaTerritories2Low.js",
//				"usaTerritoriesHigh.js",
//				"usaTerritoriesLow.js",
//				"continentsHigh.js",
//				"continentsLow.js",
//				"continentsWithAntarcticaHigh.js",
//				"continentsWithAntarcticaLow.js",
//				"cyprusNorthernCyprusHigh.js",
//				"cyprusNorthernCyprusLow.js",
//				"dominicanRepublicHigh.js",
//				"dominicanRepublicLow.js",
//				"france2016High.js",
//				"france2016Low.js",
//				"franceDepartmentsHigh.js",
//				"franceDepartmentsLow.js",
//				"moroccoWesternSaharaHigh.js",
//				"moroccoWesternSaharaLow.js",
//				"portugalRegionsHigh.js",
//				"portugalRegionsLow.js",
//				"serbiaNoKosovoHigh.js",
//				"serbiaNoKosovoLow.js"
//		};
//		
//		String path = file.getPath();
//		for (String p : excludes) {
//			if(path.endsWith(p))
//				return true;
//		}
//		return false;
//	}
//
//	private void processFileAsCountry(File file) {
//		String contentsAsString = FileUtils.contentsAsString(file);
//		String name = getName(contentsAsString);
//		
//		SvgMap svg = getSvgFromString(file);
//		writeToCountryStorage(name, svg);
//	}
//
//	public SvgMap getSvgFromString(File file) {
//		String contentsAsString = FileUtils.contentsAsString(file);
//		int resolution = file.getPath().endsWith("High.js")?1:0;
//		SvgMap svg = getSvgFromJson(contentsAsString);
//		svg.setResolution(resolution);
//		return svg;
//	}
//
//	private SvgMap getSvgFromJson(String jsonString) {
//		String json = jsonString.split("=")[1].replace(" ", "").replace("\n", "").replace("\t", "");
//		SvgMap svg = JsonUtils.readFromString(json, SvgMap.class);
//		return svg;
//	}
//
//	private String getName(String contentsAsString) {
//		int index = 0;
//		String lineOne = contentsAsString.split("\n")[0];
//		while(lineOne.isEmpty()) {
//			index++;
//			lineOne = contentsAsString.split("\n")[index];
//		}
//		System.err.println(lineOne);
//		return lineOne.split("map of ")[1].replace("-", "").replace("High", "").replace("Low", "").trim();
//	}
//
//	private void writeToCountryStorage(String name, SvgMap svg) {
//		String code = svg.getSvg().getCode();
//		PATH[] paths = svg.getPaths();
//		CountrySvg country = readerDao.getCountryByCode(code);
//		
//		System.out.println("name='"+name +"' code:"+ code);
//		
//		if(country==null)
//			country = writerDao.saveSvgAsCountry(name, svg.getSvg());
//		if(country==null) {
//			System.out.println("Err");
//		}
//		for (PATH path : paths) {
//			long id = country.getId();
//			writerDao.savePathAsRegion(id, svg.getResolution(), path);
//		}
//	}
}
