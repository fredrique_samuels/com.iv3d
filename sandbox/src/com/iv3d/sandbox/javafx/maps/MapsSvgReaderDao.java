/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

import com.iv3d.sandbox.javafx.maps.domain.CountrySvg;
import com.iv3d.sandbox.javafx.maps.domain.WorldSvg;

import java.util.List;

public interface MapsSvgReaderDao {
	// world
	public WorldSvg getWorldLowRes();
	public WorldSvg getWorldHighRes();
	public List<CountrySvg> getCountries(long worldId);
	
	// country
//	public CountrySvg getCountryByCode(String code);
	
	// region 
//	public List<RegionSvg> getHighResRegions(String countryCode);
//	public RegionSvg getHighResRegionByCode(String regionCode);
//	public RegionSvg getLowResRegionByCode(String regionCode);
	
}
