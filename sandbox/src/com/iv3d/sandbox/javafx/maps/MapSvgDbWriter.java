/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.storage.db.SqliteConnection;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;
import com.iv3d.sandbox.javafx.maps.domain.WorldSvg;
import com.iv3d.sandbox.javafx.maps.svgmap.AMAP;
import com.iv3d.sandbox.javafx.maps.svgmap.PATH;
import com.iv3d.sandbox.javafx.maps.svgmap.SvgMap;
import javafx.scene.shape.SVGPath;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public final class MapSvgDbWriter {
	private final String fileRoot;
	private final MapsSvgReaderDao readerDao;
	private final MapsSvgWriterDao writerDao;
	
	public MapSvgDbWriter(String fileRoot, MapsSvgReaderDao readerDao, MapsSvgWriterDao writerDao) {
		this.fileRoot = fileRoot;
		this.readerDao = readerDao;
		this.writerDao = writerDao;
	}
	
	public static void main(String[] args) {
		SqliteConnection db = new SqliteConnection("jdbc:sqlite:mapsvg.db");
		db.setupSchemaVersionControl();
		db.updateSchema("./cache/com.iv3d.maps.svg/sql");
		MapsSvgReaderDao readerDao = new DefaulMapsSvgReaderDao(db);
		MapsSvgWriterDao writerDao = new DefaultMapsSvgWriterDao(db);
		
		MapSvgDbWriter svgDbWriter = new MapSvgDbWriter("./cache/com.iv3d.maps.svg/raw/ammap_3.20.7.free/ammap/maps/js", 
				readerDao, writerDao);
		svgDbWriter.run();
	}

	public void run() {
		saveWorlds();
//		saveCities();
	}

	private void saveCities() {
		String csvFile = "./cache/com.iv3d.maps.svg/raw/worldcitiespop.txt";
		Reader in;
		try {
			in = new FileReader(csvFile);
			Iterable<CSVRecord> records = CSVFormat.RFC4180
					.withHeader("Country","City","AccentCity","Region","Population","Latitude","Longitude")
					.parse(in);
			int count = 0;
			List<CSVRecord> writeList = new ArrayList<>();
			for (CSVRecord record : records) {
				count++;
				
				boolean skipHeader = count==1;
				if(skipHeader)continue;
				
				writeList.add(record);
				
				boolean write = writeList.size()==1000;
				if(write) {
					System.err.println("Saved " +count+ " cities ...");
					writeCities(writeList);
				}
			}
			writeCities(writeList);
			System.err.println("Saved a total of " + count + " cities.");
		} catch (IOException e) {
			throw new CsvParserError(e);
		}
	}

	private void writeCities(List<CSVRecord> writeList) {
		if(writeList.size()>0) {
			writerDao.saveCities(writeList);
			writeList.clear();
		}
	}

	private void saveWorlds() {;
		writeLowResWorld();
		writeHighResWorld();
	}

	private void writeHighResWorld() {		
		SvgMap svg = writeWorld(MapsDaoUtils.WORLD_NAME_HIGH_RES, "worldHigh.js");
		WorldSvg world = readerDao.getWorldHighRes();
		BoundingBoxDouble bb = writeContinents(svg, world);
		writerDao.updateWorldBoundingBox(world.getId(), bb);
	}

	private void writeLowResWorld() {
		SvgMap svg = writeWorld(MapsDaoUtils.WORLD_NAME_LOW_RES, "worldLow.js");
		WorldSvg world = readerDao.getWorldLowRes();
		BoundingBoxDouble da = writeContinents(svg, world);
		writerDao.updateWorldBoundingBox(world.getId(), da);
	}

	private BoundingBoxDouble writeContinents(SvgMap svg, WorldSvg world) {
		long id = world.getId();
		BoundingBoxDouble bb = null;
		
		PATH[] paths = svg.getPaths();
		for (PATH path : paths) {
			if(bb==null)bb=path.getBoundingBox();
			else bb = bb.combine(path.getBoundingBox());
			
			SVGPath svgWidget = new SVGPath();
			svgWidget.setContent(path.getD());
			System.out.println(svgWidget.getBoundsInParent());
            
            
			writerDao.saveContinent(id, path);
		}
		
		return bb;
	}

	private SvgMap writeWorld(String name, String svgFile) {
		SvgMap svg = SvgMap.fromFile(new File(fileRoot, svgFile));
		AMAP amap = svg.getAmap();
		writerDao.saveWorld(name, amap);
		return svg;
	}
	
	
	public static final class CsvParserError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public CsvParserError(IOException e) {super(e);}		
	}
	
}
