/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps.svgmap;

import java.io.File;

import com.iv3d.common.utils.FileUtils;
import com.iv3d.common.utils.JsonUtils;

public class SvgMap {
	private int resolution;
	private SVG svg;
	public SVG getSvg() {return svg;}
	public void setSvg(SVG svg) {this.svg = svg;}
	public PATH[] getPaths() {return svg.getPaths();}
	public int getResolution() {return resolution;}
	public void setResolution(int resolution) {this.resolution = resolution;}
	public AMAP getAmap() {return svg.getAmap();}
	
	public static SvgMap fromFile(File file) {
		String contentsAsString = FileUtils.contentsAsString(file);
		int resolution = file.getPath().endsWith("High.js")?1:0;
		SvgMap svg = getSvgFromJson(contentsAsString);
		svg.setResolution(resolution);
		return svg;
	}
	
	private static SvgMap getSvgFromJson(String jsonString) {
		String json = jsonString.split("=")[1].replace(" ", "").replace("\n", "").replace("\t", "");
		SvgMap svg = JsonUtils.readFromString(json, SvgMap.class);
		return svg;
	}

}
