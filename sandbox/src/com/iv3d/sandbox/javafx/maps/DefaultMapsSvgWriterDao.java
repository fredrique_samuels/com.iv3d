/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.storage.db.SqliteConnection;
import com.iv3d.sandbox.javafx.maps.dao.city.SaveCity;
import com.iv3d.sandbox.javafx.maps.dao.country.SaveCountry;
import com.iv3d.sandbox.javafx.maps.dao.world.SaveWorld;
import com.iv3d.sandbox.javafx.maps.dao.world.UpdateWorldDa;
import com.iv3d.sandbox.javafx.maps.svgmap.AMAP;
import com.iv3d.sandbox.javafx.maps.svgmap.PATH;
import org.apache.commons.csv.CSVRecord;

import java.util.List;

public class DefaultMapsSvgWriterDao implements MapsSvgWriterDao {

	private SqliteConnection db;

	public DefaultMapsSvgWriterDao(SqliteConnection db) {
		this.db = db;
	}

	@Override
	public void saveWorld(String name, AMAP amap) {
		db.update(new SaveWorld(name, amap));
	}

	@Override
	public void saveContinent(long id, PATH path) {
		db.update(new SaveCountry(id, path));
	}

	@Override
	public void updateWorldBoundingBox(long id, BoundingBoxDouble bb) {
		db.update(new UpdateWorldDa(id, bb));
	}

	@Override
	public void saveCities(List<CSVRecord> records) {
		db.update(new SaveCity(records));
	}

}
