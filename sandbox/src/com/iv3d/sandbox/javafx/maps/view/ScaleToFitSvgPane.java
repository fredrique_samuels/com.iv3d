package com.iv3d.sandbox.javafx.maps.view;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.SVGPath;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

public class ScaleToFitSvgPane extends StackPane {
    private final Group contentGroup;
    private final Group content;
    private final Pane editPane;

    public ScaleToFitSvgPane() {
        editPane= new Pane();
        contentGroup = new Group();
        content = new Group();
        contentGroup.getChildren().add(content);
        editPane.getChildren().add(contentGroup);
        getChildren().add(editPane);
        editPane.widthProperty().addListener((observable, oldValue, newValue) -> {adjustTransform();});
        editPane.heightProperty().addListener((observable, oldValue, newValue) -> {adjustTransform();});
        Platform.runLater(() -> adjustTransform());
    }

    public final void add(SVGPath path) {
        content.getChildren().add(path);
    }

    public final void remove(SVGPath path) {
        content.getChildren().remove(path);
    }

    public final void refresh() {
        Platform.runLater(() -> adjustTransform());
    }

    private void adjustTransform() {
        contentGroup.getTransforms().clear();

        double cx = content.getBoundsInParent().getMinX();
        double cy = content.getBoundsInParent().getMinY();
        double cw = content.getBoundsInParent().getWidth();
        double ch = content.getBoundsInParent().getHeight();

        double ew = editPane.getWidth();
        double eh = editPane.getHeight();

        if (ew > 0.0 && eh > 0.0) {
            double scale = Math.min(ew / cw, eh / ch);

            // Offset to center content
            double sx = 0.5 * (ew - cw * scale);
            double sy = 0.5 * (eh - ch * scale);

            contentGroup.getTransforms().add(new Translate(sx, sy));
            contentGroup.getTransforms().add(new Translate(-cx, -cy));
            contentGroup.getTransforms().add(new Scale(scale, scale, cx, cy));
        }
    }

}
