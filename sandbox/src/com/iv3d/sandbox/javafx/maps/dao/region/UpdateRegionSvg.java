/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps.dao.region;

import com.iv3d.common.storage.db.DbManager;
import com.iv3d.common.storage.db.DbUpdate;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;

public final class UpdateRegionSvg implements DbUpdate {

	private final String query;

	public UpdateRegionSvg(String code, int resolution, String svgPath) {
		String format = "UPDATE %s SET %s=\"%s\" where code=\"%s\";";
		this.query = String.format(format, MapsDaoUtils.REGION_TABLE, 
				MapsDaoUtils.getSvgColumnNameFromResolution(resolution),
				UriUtils.encode(svgPath),
				MapsDaoUtils.getEncodedCode(code));
	}

	public final String getSql(DbManager connection) {
		return query;
	}

}
