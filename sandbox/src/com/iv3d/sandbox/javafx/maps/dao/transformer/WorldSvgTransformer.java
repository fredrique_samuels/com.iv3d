/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps.dao.transformer;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;
import com.iv3d.sandbox.javafx.maps.domain.WorldSvg;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class WorldSvgTransformer extends AbstractResultSetTransformer<WorldSvg> {

	public WorldSvg get(ResultSet resultSet) throws SQLException {
		Impl impl = new Impl();
		impl.id = resultSet.getLong("id");
		impl.name = UriUtils.decode(resultSet.getString("name"));
		impl.bottomLatitude = resultSet.getDouble("bottom_latitude");
		impl.topLatitude = resultSet.getDouble("top_latitude");
		impl.leftLongitude = resultSet.getDouble("left_longitude");
		impl.rightLongitude = resultSet.getDouble("right_longitude");
		impl.bb = getBB(resultSet);
		
		return impl;
	}

	private BoundingBoxDouble getBB(ResultSet resultSet) throws SQLException {
		String bbs = resultSet.getString("boundingbox_json");
		if(bbs==null) return new BoundingBoxDouble();
		return BoundingBoxDouble.fromJson(MapsDaoUtils.decodeString(bbs));
	}

	
	private static class Impl implements WorldSvg {
		public BoundingBoxDouble bb;
		private long id;
		private String name;
		private double leftLongitude;
		private double rightLongitude;
		private double topLatitude;
		private double bottomLatitude;

		public long getId() {return id;}
		public String getName() {return name;}
		public double getLeftLongitude() {return leftLongitude;}
		public double getTopLatitude() {return topLatitude;}
		public double getRightLongitude() {return rightLongitude;}
		public double getBottomLatitude() {return bottomLatitude;}
		public BoundingBoxDouble getBoundingBox() {return bb;}
	}

}
