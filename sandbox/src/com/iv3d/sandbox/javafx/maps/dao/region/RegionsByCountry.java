/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps.dao.region;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.iv3d.common.storage.db.DbManager;
import com.iv3d.common.storage.db.DbQuery;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;
import com.iv3d.sandbox.javafx.maps.dao.transformer.RegionSvgTransformer;
import com.iv3d.sandbox.javafx.maps.domain.RegionSvg;

public final class RegionsByCountry implements DbQuery<List<RegionSvg>> {

	private final String query;
	private final int resolution;

	public RegionsByCountry(int resolution, String countryCode) {
		this.resolution = resolution;
		String format = "SEECT * from %s WHERE code=\"%s\";";
		this.query = String.format(format, MapsDaoUtils.REGION_TABLE, 
				MapsDaoUtils.getEncodedCode(countryCode));
	}

	public String getSql() {
		return query;
	}

	public List<RegionSvg> parse(ResultSet resultSet, DbManager connection) throws SQLException {
		List<RegionSvg> list = new ArrayList<>();
		while (resultSet.next()) {
			RegionSvg transform = new RegionSvgTransformer(resolution).transform(resultSet);
			list.add(transform);
		}
		return list;
	}

}
