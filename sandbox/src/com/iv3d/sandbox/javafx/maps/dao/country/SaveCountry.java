/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps.dao.country;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;
import com.iv3d.sandbox.javafx.maps.svgmap.PATH;

public class SaveCountry extends AbstractDbUpdate {
	private static final String QS = "INSERT INTO %s (world_id, name, code, svg_path, boundingbox_json)"
			+ " VALUES (%d, \"%s\", \"%s\", \"%s\", \"%s\");";

	private static String qs(long id, PATH path) {
		return String.format(QS, 
				MapsDaoUtils.COUNTRY_TABLE,
				id,
				UriUtils.encode(path.getTitle()),
				MapsDaoUtils.getEncodedCode(path.getId()),
				UriUtils.encode(path.getD()),
				MapsDaoUtils.encodeString(JsonUtils.writeToString(path.getBoundingBox())));
	}

	public SaveCountry(long id, PATH path) {
		super(qs(id, path));
	}

}
