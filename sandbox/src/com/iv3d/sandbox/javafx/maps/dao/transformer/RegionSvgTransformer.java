/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps.dao.transformer;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;
import com.iv3d.sandbox.javafx.maps.domain.RegionSvg;

public final class RegionSvgTransformer extends AbstractResultSetTransformer<RegionSvg>{

	private int resolution;

	public RegionSvgTransformer(int resolution) {
		this.resolution = resolution;
	}

	protected RegionSvg get(ResultSet resultSet) throws SQLException {
		Impl impl = new Impl();
		impl.id = resultSet.getLong("id");
		impl.name = getName(resultSet);
		impl.code = MapsDaoUtils.getDecodedCode(resultSet.getString("code"));
		impl.resolution = resolution;
		impl.countryId = resultSet.getLong("country_id");
		impl.svgPath = getSvgPath(resultSet);
		return impl;
	}

	private String getSvgPath(ResultSet resultSet) throws SQLException {
		String svgCol = MapsDaoUtils.getSvgColumnNameFromResolution(resolution);
		return UriUtils.decode(resultSet.getString(svgCol));                                                                                     
	}

	private String getName(ResultSet resultSet) throws SQLException {
		return UriUtils.decode(resultSet.getString("name"));
	}
	
	private static class Impl implements RegionSvg {
		private long id;
		private String name;
		private String code;
		private long countryId;
		private String svgPath;
		private int resolution;
		public long getId() {return id;}
		public String getName() {return name;}
		public String getCode() {return code;}
		public long getCountryId() {return countryId;}
		public String getSvgPath() {return svgPath;}
		public int getResolution() {return resolution;}
	}

}
