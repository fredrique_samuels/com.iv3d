package com.iv3d.sandbox.javafx.maps.dao.city;

import java.util.List;

import org.apache.commons.csv.CSVRecord;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import com.iv3d.common.utils.UriUtils;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;

public class SaveCity extends AbstractDbUpdate {
	private static final String QS = "INSERT INTO %s (country_code,name,accent_name,region,polulation,latitude,longitude,latlon) "
			+ "VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%d\", %.3f, %.3f, \"%s\");";
	
	public SaveCity(List<CSVRecord> records) {
		super(qs(records));
	}
	
	private static String qs(List<CSVRecord> records) {
		String rs = "";
		for (CSVRecord csvRecord : records) {
			rs+=getRecordsQs(csvRecord);
		}
		return rs;
	}

	private static String getRecordsQs(CSVRecord record) {
		return String.format(QS,
				MapsDaoUtils.CITY_TABLE,
				MapsDaoUtils.getEncodedCode(record.get("Country").toUpperCase()),
				UriUtils.encode(record.get("City")),
				UriUtils.encode(record.get("AccentCity")),
				record.get("Region"),
				getLong(record.get("Population")),
				getReal(record.get("Latitude")),
				getReal(record.get("Longitude")),
				getLatLon(record)
				);
	}
	
	private static long getLong(String string) {
		return string.isEmpty()?0L:Long.valueOf(string);
	}
	
	private static double getReal(String string) {
		return string.isEmpty()?0L:Double.valueOf(string);
	}
	
	private static String getLatLon(CSVRecord record) {
		return String.format("%s,%s", record.get("Latitude"), record.get("Longitude"));
	}

}
