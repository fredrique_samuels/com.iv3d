/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

public final class DefaultMapsSvgDao {
	
//	private final SqliteConnection db;
//
//	public DefaultMapsSvgDao(SqliteConnection db) {
//		this.db = db;
//	}
//	
//	public final CountrySvg getCountryByCode(String code) {
//		return db.query(new CountryByCode(code));
//	}
//	
//	public final RegionSvg getLowResRegionByCode(String code) {
//		RegionSvg r = getRegionByCode(code, MapsDaoUtils.RESOLUTION_LOW);
//		if(r==null)
//			r = getRegionByCode(code, MapsDaoUtils.RESOLUTION_HIGH);
//		return r;
//	}
//	
//	public final RegionSvg getHighResRegionByCode(String code) {
//		RegionSvg r = getRegionByCode(code, MapsDaoUtils.RESOLUTION_HIGH);
//		if(r==null)
//			r = getRegionByCode(code, MapsDaoUtils.RESOLUTION_LOW);
//		return r;
//	}
//
//
//	
//	// END OF PUBLIC DAO
//
//	public WorldSvg saveAmapAsWorld(String name, AMAP amap) {
//		db.update(new SaveWorld(name, amap));
//		return db.query(new WorldByName(name));
//	}
//
//	public final CountrySvg saveSvgAsCountry(String name, SVG svg) {
//		db.update(new SaveSvgAsCountry(name, svg));
//		return getCountryByCode(svg.getCode());
//	}
//
//	public final RegionSvg savePathAsRegion(long id, int resolution, PATH path) {
//		String code = path.getId();
//		if(getRegionByCode(code, resolution)==null) {
//			db.update(new SavePathAsRegion(id, resolution, path));
//		} else {
//			updatePathSvg(resolution, path.getD(), code);
//		}
//		return getRegionByCode(code, resolution);
//	}
//
//	public final void updatePathSvg(int resolution, String svgPath, String code) {
//		db.update(new UpdateRegionSvg(code, resolution, svgPath));
//	}
//	
//	private RegionSvg getRegionByCode(String code, int resolution) {
//		return db.query(new RegionByCode(code, resolution));
//	}
//
//	@Override
//	public WorldSvg getWorldLowRes() {
//		return db.queryUnique(new GetLowResWorld());
//	}
//
//	@Override
//	public List<ContinentSvg> getContinents(long worldId) {
//		return null;
//	}
//	
}
