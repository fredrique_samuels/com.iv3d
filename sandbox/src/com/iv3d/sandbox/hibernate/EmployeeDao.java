package com.iv3d.sandbox.hibernate;

import com.iv3d.common.storage.hibernate.AbstractHibernateDao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Iterator;
import java.util.List;

public class EmployeeDao extends AbstractHibernateDao {

    public EmployeeDao() {
        super("/com/iv3d/sandbox/hibernate/employee.sqlite.hbm.xml",
                new Class[]{Employee.class});
    }

    public final Long addEmployee(String fname, String lname, int salary){
        Employee employee = new Employee();
        employee.setFirstName(fname);
        employee.setLastName(lname);
        employee.setSalary(salary);
        return saveObject(employee);
    }

    public final void listEmployees( ){
        List employees = getObjectList((session)-> session.createQuery("FROM Employee"));
        for (Iterator iterator =
            employees.iterator(); iterator.hasNext();){
            Employee employee = (Employee) iterator.next();
            System.out.print("Id: " + employee.getId());
            System.out.print("First Name: " + employee.getFirstName());
            System.out.print("  Last Name: " + employee.getLastName());
            System.out.println("  Salary: " + employee.getSalary());
        }
    }

    public final void updateEmployee(Long id, int salary ){
        Employee employee = getEmployee(id);
        employee.setSalary( salary );
        updateObject(employee);
    }

    public final Employee getEmployee(Long id) {
        return get(id, Employee.class);
    }

    public final void deleteEmployee(Long id){
        Employee employee = getEmployee(id);
        deleteObject(employee);
    }

}
