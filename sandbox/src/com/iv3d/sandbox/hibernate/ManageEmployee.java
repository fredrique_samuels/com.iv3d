package com.iv3d.sandbox.hibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Date;
import java.util.Iterator;

import com.iv3d.sandbox.hibernate.Employee;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManageEmployee {
    private static SessionFactory factory;
    public static void main(String[] args) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:employee.db");
            System.out.println("Opened database successfully");

            runCreate(c, "CREATE TABLE IF NOT EXISTS EMPLOYEE " +
                    "(id INTEGER PRIMARY KEY," +
                    " first_name           TEXT    NOT NULL, " +
                    " last_name            TEXT    NOT NULL, " +
                    " salary         INT)");

            runCreate(c, "CREATE TABLE IF NOT EXISTS CARS " +
                    "(id INTEGER PRIMARY KEY," +
                    " make           TEXT    NOT NULL, " +
                    " empl_id        INTEGER);");

            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        System.out.println("Table created successfully");

        EmployeeDao dao = new EmployeeDao();
        /* Add few employee records in database */
        Long empID1 = dao.addEmployee("Zara", "Ali", 1000);
        Long empID2 = dao.addEmployee("Daisy", "Das", 5000);
        Long empID3 = dao.addEmployee("John", "Paul", 10000);

      /* List down all the employees */
        dao.listEmployees();

      /* Update employee's records */
        dao.updateEmployee(empID1, 5000);

      /* Delete an employee from the database */
        dao.deleteEmployee(empID2);

      /* List down new list of the employees */
        dao.listEmployees();

//        oldImpl();
    }

    private static void runCreate(Connection c, String sql) throws SQLException {
        Statement stmt;
        stmt = c.createStatement();
        stmt.executeUpdate(sql);
        stmt.close();
    }

    private static void oldImpl() {
        try{
            factory = new AnnotationConfiguration().
                    configure("hibernate.cfg.xml").
                    //addPackage("com.xyz") //add package if used.
                    addAnnotatedClass(Employee.class).
                    buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        ManageEmployee ME = new ManageEmployee();

      /* Add few employee records in database */
        Integer empID1 = ME.addEmployee("Zara", "Ali", 1000);
        Integer empID2 = ME.addEmployee("Daisy", "Das", 5000);
        Integer empID3 = ME.addEmployee("John", "Paul", 10000);

      /* List down all the employees */
        ME.listEmployees();

      /* Update employee's records */
        ME.updateEmployee(empID1, 5000);

      /* Delete an employee from the database */
        ME.deleteEmployee(empID2);

      /* List down new list of the employees */
        ME.listEmployees();
    }

    /* Method to CREATE an employee in the database */
    public Integer addEmployee(String fname, String lname, int salary){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;
        try{
            tx = session.beginTransaction();
            Employee employee = new Employee();
            employee.setFirstName(fname);
            employee.setLastName(lname);
            employee.setSalary(salary);
            employeeID = (Integer) session.save(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return employeeID;
    }
    /* Method to  READ all the employees */
    public void listEmployees( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List employees = session.createQuery("FROM Employee").list();
            for (Iterator iterator =
                 employees.iterator(); iterator.hasNext();){
                Employee employee = (Employee) iterator.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to UPDATE salary for an employee */
    public void updateEmployee(Integer EmployeeID, int salary ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Employee employee =
                    (Employee)session.get(Employee.class, EmployeeID);
            employee.setSalary( salary );
            session.update(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to DELETE an employee from the records */
    public void deleteEmployee(Integer EmployeeID){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Employee employee =
                    (Employee)session.get(Employee.class, EmployeeID);
            session.delete(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}