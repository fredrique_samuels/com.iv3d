package com.iv3d.sandbox.hibernate;

import javax.persistence.*;

/**
 * Created by fred on 2017/05/23.
 */
@Entity
@Table(name = "CARS")
public class Car {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "make")
    private String make;

    @Column
    private int emplId;

    public int getEmplId() {
        return emplId;
    }

    public void setEmplId(int emplId) {
        this.emplId = emplId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }
}
