package com.iv3d.sandbox;

/**
 * Created by fred on 2016/09/12.
 */
public class SmartDecorator {
    enum Direction {
        LEFT,
        STRAIGHT,
        RIGHT
    }

    interface Car {
        void setDirection(Direction direction);
        void draw();
    }

    static class CarImpl implements Car {
        private Direction direction = Direction.STRAIGHT;

        @Override
        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        @Override
        public void draw() {
            System.out.println("Driving " +  direction);
        }
    }


    static class CarDecorator {

    }

    static class CarHandler {
        public void draw(Car car) {
            car.draw();
        }
    }

    public static void main(String[] args) {
        new CarHandler().draw(new CarImpl());
    }
}
