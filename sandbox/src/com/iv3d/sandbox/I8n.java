package com.iv3d.sandbox;

import com.iv3d.common.i8n.I8nMessageResolver;
import com.iv3d.common.i8n.Language;

/**
 * Created by fred on 2017/08/03.
 */
public class I8n {
    public static void main(String[] args) {
        I8nMessageResolver i8NMessageResolver = new I8nMessageResolver();
        i8NMessageResolver.getMessage("message", Language.ENGLISH);
    }
}
