package com.iv3d.sandbox.emails;

import com.iv3d.common.core.Credentials;

/**
 * Created by fred on 2017/08/01.
 */
public class DemoEmailCredentials implements Credentials {

    @Override
    public String getUser() {
        return "hr.tectendencies@gmail.com";
    }

    @Override
    public String getPassword() {
        return "Organ300";
    }
}
