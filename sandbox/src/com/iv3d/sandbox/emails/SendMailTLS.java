package com.iv3d.sandbox.emails;

import com.iv3d.common.core.Credentials;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.email.GmailSettings;
import com.iv3d.common.email.MailSession;

import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailTLS {

    final static String username = "hr.tectendencies@gmail.com";
    final static String password = "Organ300";

	public static void main(String[] args) throws MessagingException {

        Credentials c = new Credentials() {
            @Override
            public String getUser() {
                return username;
            }

            @Override
            public String getPassword() {
                return password;
            }
        };


        MailSession mailSession = new MailSession(c, new GmailSettings());

        String text = ResourceLoader.readAsString("/template.email.account.created.macro");
        mailSession.sendHtmlMessage("fredriquesamuels@gmail.com", "API TEST", text);

//        Session session = createMainSession();
//        sendMessage(session);

//        try {
//            Store store = session.getStore("imaps");
//            store.connect("smtp.gmail.com", username, password);
//
//            Folder emailFolder = store.getFolder("INBOX");
//            emailFolder.open(Folder.READ_WRITE);
//
//            int messageCount = emailFolder.getMessageCount();
//            int unreadMessageCount = emailFolder.getUnreadMessageCount();
//            int newMessageCount = emailFolder.getNewMessageCount();
//
//            System.out.println("messageCount " + messageCount);
//            System.out.println("unreadMessageCount " + unreadMessageCount);
//            System.out.println("newMessageCount " + newMessageCount);
//            Message[] messages = emailFolder.getMessages();
//            System.out.println("messages.length---" + messages.length);
//
//            emailFolder.close(false);
//            store.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private static void sendMessage(Session session) {
        try {

            String text = ResourceLoader.readAsString("/template.email.account.created.macro");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("tt.support@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("fredriquesamuels@gmail.com"));
            message.setSubject("Testing Subject");

            message.setContent(text, "text/html; charset=utf-8");
//            message.setText("<h1 style=\"color:red\"> HEADER </h1>");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private static Session createMainSession() {

//        String host = "pop.gmail.com";// change accordingly
//        String mailStoreType = "pop3";

        Properties props = new Properties();
//        props.put("mail.store.protocol", "pop3");
//        props.put("mail.pop3.host", "pop.gmail.com");
//        props.put("mail.pop3.port", "995");
//        props.put("mail.pop3.starttls.enable", "true");

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        return Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });
    }
}