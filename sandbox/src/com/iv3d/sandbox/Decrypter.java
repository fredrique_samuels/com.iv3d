package com.iv3d.sandbox;

import javax.crypto.Cipher;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.SecretKeyFactory;
import java.security.AlgorithmParameters;
import javax.crypto.spec.IvParameterSpec;

public class Decrypter {
    private final byte[] iv;
    private Cipher dcipher;
    private SecretKey key;

    private Decrypter(String secretKey) throws Exception {
        key = new SecretKeySpec(secretKey.getBytes(), "AES");
        dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        dcipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters params = dcipher.getParameters();
        iv = params.getParameterSpec(IvParameterSpec.class).getIV();
    }

    public static Decrypter createCrypter(String passPhrase) throws Exception {
        byte[] salt = new String("12345678").getBytes();
        int iterationCount = 1024;
        int keyStrength = 256;
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount, keyStrength);

        byte[] tmp = factory.generateSecret(spec).getEncoded();
        return new Decrypter(new String(tmp));
    }

    public String encrypt(String data) throws Exception {
        dcipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] utf8EncryptedData = dcipher.doFinal(data.getBytes());
        String base64EncryptedData = new sun.misc.BASE64Encoder().encodeBuffer(utf8EncryptedData);
        return base64EncryptedData;
    }

    public String decrypt(String base64EncryptedData) throws Exception {
        dcipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        byte[] decryptedData = new sun.misc.BASE64Decoder().decodeBuffer(base64EncryptedData);
        byte[] utf8 = dcipher.doFinal(decryptedData);
        return new String(utf8, "UTF8");
    }

    public static void main(String args[]) throws Exception {
        String data = "the quick brown fox jumps over the lazy dog";

        System.out.println(test(data, "ABCDEFGHIJKL"));
        System.out.println(test(data, "1234567890"));
    }

    static String test(String data, String passPhrase) throws Exception {
        Decrypter decrypter = Decrypter.createCrypter(passPhrase);
        String encrypted = decrypter.encrypt(data);
        System.out.println(encrypted);
        return decrypter.decrypt(encrypted);
    }
}