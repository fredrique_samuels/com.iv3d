package com.iv3d.sandbox;

import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by fred on 2017/07/19.
 */
public class LoggerTest {

    public static void main(String[] args) {

        String s = "{\"operation.status.attribute.logs\":[{\"level\":\"INFO\",\"message\":\"2017-07-20T10:18:19+0000 INFO Starting download for file iv3d.games.ygo.cards.download.wiki.WikiCardListUrl@5e75e46\",\"date\":{\"iso\":\"2017-07-20T10:18:19+0000\",\"milliSinceEpoc\":1500545899850}}],\"operation.status.attribute.file.source\":\"http://yugioh.wikia.com/wiki/Special:Ask?x=-5B-5BClass-201%3A%3AOfficial-5D-5D-20-5B-5BCard-20type%3A%3AMonster-20Card-5D-5D-20-5B-5BCard-20type%3A%3ASynchro-20Monster-5D-5D%2F-3FEnglish-20name%3D%2F-3FJapanese-20name%2F-3FCard-20image%2F-3FPrimary-20type%2F-3FSecondary-20type%2F-3FAttribute%3D-5B-5BAttribute-5D-5D%2F-3FType%3D-5B-5BType-5D-5D%2F-3FStars%3D-5B-5BLevel-5D-5D-2F-5B-5BRank-5D-5D%2F-3FATK%3D-5B-5BATK-5D-5D%2F-3FDEF%3D-5B-5BDEF-5D-5D%2F-3FPasscode%3D-5B-5BPasscode-5D-5D%2F-3FPendulum-20Scale%2F-3FPendulum-20Effect%3D-5B-5BPendulum-20Effect-5D-5D%2F-3FLore%3D-5B-5BLore-5D-5D%2F-3FRitual-20Monster-20required%2F-3FRitual-20Spell-20Card-20required%2F-3FMaterials%2F-3FFusion-20Material&mainlabel=-&limit=500&prettyprint=true&format=json&offset=0\",\"operation.status.attribute.file.createdonwrite\":false,\"operation.status.attribute.job.id\":2,\"operation.status.attribute.run.id\":1,\"operation.status.attribute.file.name\":\"synchro-list-0.json\",\"operation.status.attribute.file.saved\":true,\"operation.status.attribute.job.bean\":\"iv3d.games.ygo.cards.download.wiki.operations.downloadwikicardlist\",\"operation.status.attribute.app.package\":\"cardserver\",\"operation.status.attribute.file.writepolicy\":\"REPLACE\"}";
        Map<String, Object> stringObjectMap = JsonUtils.parseToMap(s);
        System.out.println(stringObjectMap);

    }
}
