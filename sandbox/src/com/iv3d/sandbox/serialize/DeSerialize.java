package com.iv3d.sandbox.serialize;

import com.iv3d.common.utils.FileUtils;
import org.springframework.util.SerializationUtils;

import java.io.File;

/**
 * Created by fred on 2017/11/14.
 */
public class DeSerialize {
    public static void main(String[] args) {
        byte[] contents = FileUtils.contents(new File("./save_object.bin"));
        MyInterface myInterface = (MyInterface) SerializationUtils.deserialize(contents);
        System.out.println(myInterface.work(() -> "This Is  A Test"));
    }
}
