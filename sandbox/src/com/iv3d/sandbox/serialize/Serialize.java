package com.iv3d.sandbox.serialize;

import com.iv3d.common.utils.FileUtils;
import org.python.antlr.ast.Str;
import org.springframework.util.SerializationUtils;

import java.io.File;

/**
 * Created by fred on 2017/11/14.
 */
public class Serialize {
    public static void main(String[] args) {
        MyInterface myInterface = i -> {
            String v = i.get();
            String lowerCase = v.toLowerCase();
            String[] split = lowerCase.split(" ");
            return String.join("@", split);
        };

        byte[] serialize = SerializationUtils.serialize(myInterface);
        FileUtils.writeBytesToFile(new File("./save_object.bin"), serialize);
    }
}
