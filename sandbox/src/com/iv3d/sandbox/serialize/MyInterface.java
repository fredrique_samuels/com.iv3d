package com.iv3d.sandbox.serialize;

import java.io.Serializable;

/**
 * Created by fred on 2017/11/14.
 */
public interface MyInterface extends Serializable {
    String work(Input input);
}
