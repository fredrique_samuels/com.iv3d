package com.iv3d.sandbox.serialize;

/**
 * Created by fred on 2017/11/14.
 */
public interface Input {
    String get();
}
