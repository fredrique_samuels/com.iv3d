CREATE table mapssvg_world (
		   id  	INTEGER PRIMARY KEY,
		   field STRING,
		   left_longitude REAL,
		   top_latitude REAL,
		   right_longitude REAL,
		   bottom_latitude REAL,
		   boundingbox_json STRING);

CREATE table mapssvg_country (
		   id  	INTEGER PRIMARY KEY,
		   world_id INT,
		   code STRING,
           field STRING,
           svg_path STRING,
		   boundingbox_json STRING);
		   
/**		   
CREATE table mapssvg_region (
		   id  	INTEGER PRIMARY KEY,
		   country_id INT,
		   code STRING,
           field STRING,
           svg_lr STRING,
           svg_hr STRING,
		   left_longitude REAL,
		   top_latitude REAL,
		   right_longitude REAL,
		   bottom_latitude REAL);
CREATE UNIQUE INDEX mapssvg_region_code_index ON mapssvg_region (code);

CREATE table mapssvg_city (
		   id  	INTEGER PRIMARY KEY,
		   country_code STRING,
		   field STRING,
		   accent_name STRING,
		   region STRING,
		   polulation INT,
		   latitude REAL,
		   longitude REAL,
		   latlon STRING);
**/
