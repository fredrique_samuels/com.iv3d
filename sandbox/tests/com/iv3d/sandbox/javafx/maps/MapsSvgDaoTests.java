/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

import java.io.File;

import com.iv3d.common.storage.db.SqliteConnection;
import com.iv3d.sandbox.javafx.maps.dao.MapsDaoUtils;

import junit.framework.TestCase;

public class MapsSvgDaoTests extends TestCase {
	private File dbFile = new File("test.db");
	private SqliteConnection db;
	
	protected void setUp() throws Exception {
		super.setUp();
		db = new SqliteConnection("jdbc:sqlite:test.db");
		db.setupSchemaVersionControl();
		db.updateSchema(MapsDaoUtils.SQL_SCRIPT_DIR);
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		db.close();
		if(dbFile.exists()){dbFile.delete();}
	}
	
	public void testSetup() {
		assertTrue(db.tableExists(MapsDaoUtils.WORLD_TABLE));
		assertTrue(db.tableExists(MapsDaoUtils.CONTINENT_TABLE));
	}
}
