/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.sandbox.javafx.maps;

import com.iv3d.sandbox.javafx.maps.domain.DrawingArea;

import junit.framework.TestCase;

public class DrawingAreaTest extends TestCase {
	public void testMerge() {
		DrawingArea a1 = new DrawingArea(1,20,30,15);
		DrawingArea a2 = new DrawingArea(2,10,40,10);
		DrawingArea a3 = a1.merge(a2);
		
		assertEquals(1, a3.getLeft(), 0.001);
		assertEquals(20, a3.getTop(), 0.001);
		assertEquals(40, a3.getRight(), 0.001);
		assertEquals(10, a3.getBottom(), 0.001);
	}
}
