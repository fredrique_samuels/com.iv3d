package com.iv3d.sandbox.javafx.maps;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;

/**
 * Created by fred on 2016/09/21.
 */
public class TestMocket extends TestCase {

    public static class B {

        public void methodB(){
        }
    }


    public static class A {
        B b;

        public A(B b) {
            this.b = b;
        }

        public void callB(){
            b.methodB();
        }

        public int getValue() {
            return 10;
        }
    }


    @Tested
    A a;
    @Mocked
    private B b;

    public void setUp() throws Exception {
        super.setUp();
        a = new A(b);
    }

    public void testCallB() {
        new NonStrictExpectations() {
            {
                a.getValue();
                result = 20;
            }
        };

        assertEquals(20, a.getValue());
    }
}
