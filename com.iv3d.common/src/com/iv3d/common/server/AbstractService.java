package com.iv3d.common.server;

import org.eclipse.jetty.server.Server;

import com.google.inject.Inject;
import com.iv3d.common.server.inject.ServerProvider;

public class AbstractService {
	private final ServerProvider server;
	
	@Inject
	public AbstractService(ServerProvider provider) {
		this.server = provider;
	}

	public final void run() {
		Server instance = server.get();
		if(instance.isRunning()) {
			return;
		}
		
		try {
			instance.start();
		} catch (Exception e) {
			throw new ServerCouldNotStartError(e);
		}
		try {
			instance.join();
		} catch (InterruptedException e) {
			//ignore
		}
	}
	
	public final void stop() {
		try {
			Server instance = server.get();
			if(instance.isRunning()) {
				instance.stop();
			}
		} catch (Exception e) {
			//ignore
		}
	}

	public final class ServerCouldNotStartError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ServerCouldNotStartError(Exception e) {	
			super(e);
		}
	}
}
