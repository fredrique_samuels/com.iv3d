package com.iv3d.common.server;

import com.iv3d.common.utils.JsonUtils;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public abstract class AbstractRequestHandler implements PathHandler {

	public static final String GET = "GET";
	public static final String POST = "POST";

	public final String getParameter(HttpServletRequest request, String id) {
		String parameter = request.getParameter(id);
		if (parameter == null) {
			throw new RequiredParameterNotSpecified(String.format("Required Parameter '%s' not found.", id));
		}
		return parameter;
	}
	
	public final String[] getListParameter(HttpServletRequest request, String id) {
		String[] values = request.getParameterMap().get(id);
		if (values == null) {
			throw new RequiredParameterNotSpecified(String.format("Required Parameter '%s' not found.", id));
		}
		
		if (values.length==0) {
			throw new RequiredParameterNotSpecified(String.format("Required Parameter '%s' has no values.", id));
		}
		return values;
	}
	
	public final String[] getListParameter(MultiPartRequest request, String id) {
		String[] values = request.getParameterMap().get(id);
		if (values == null) {
			throw new RequiredParameterNotSpecified(String.format("Required Parameter '%s' not found.", id));
		}
		
		if (values.length==0) {
			throw new RequiredParameterNotSpecified(String.format("Required Parameter '%s' has no values.", id));
		}
		return values;
	}
	
	public final String getParameter(MultiPartRequest request, String id) {
		String parameter = request.getParameter(id);
		if (parameter == null) {
			throw new RequiredParameterNotSpecified(String.format("Required Parameter '%s' not found.", id));
		}
		return parameter;
	}
	
	public final String getOptionalParameter(MultiPartRequest request, String id, String defaultValue) {
		String value = request.getParameter(id);
		return value==null?defaultValue:value;
	}

	public final void returnHtml(HttpServletResponse response, String value) {
		response.setContentType("text/html; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		try {
            response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().write(value);
		} catch (IOException e) {
			throw new UnableToWriteOutput();
		}
	}

	public final void returnNoContent(HttpServletResponse response) {
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}
	
	public final void returnJsonParam(HttpServletResponse response, Object param) {
		RestResponse<Object> result = new RestResponse<Object>();
		result.setBody(param);
        returnJson(response, result);
	}

    public final void returnJson(HttpServletResponse response, Object param) {
        String json = JsonUtils.writeToString(param);

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentLength(json.length());
        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            throw new UnableToWriteOutput();
        }
    }

	public final void returnOkResponse(HttpServletResponse response) {
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_OK);

		RestResponse<Object> result = new RestResponse<Object>();

		try {
            response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().write(JsonUtils.writeToString(result));
		} catch (IOException e) {
			throw new UnableToWriteOutput();
		}
	}

	public final String getStringBody(HttpServletRequest request) {
		BufferedReader reader;
		try {
			reader = request.getReader();
			return org.apache.commons.io.IOUtils.toString(reader);
		} catch (IOException e) {
			throw new UnableToreadRequestBody();
		}
	}

	public final void returnError(HttpServletResponse response, String message) {
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

		RestResponse<Object> result = new RestResponse<Object>().setFailure(message);

		try {
            response.setHeader("Access-Control-Allow-Origin", "*");
            if(message!=null) {
                response.getWriter().write(JsonUtils.writeToString(result));
            }
		} catch (IOException e) {
			throw new UnableToWriteOutput();
		}
	}

	public void downloadFile(HttpServletRequest request, HttpServletResponse response, File media)
			throws FileNotFoundException {
		FileInputStream inStream = new FileInputStream(media);

		String mimeType = "application/octet-stream";
		response.setContentType(mimeType);
		response.setContentLength((int) media.length());

		// forces download
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", media.getName());
		response.setHeader(headerKey, headerValue);

		OutputStream outStream = null;
		try {
			outStream = response.getOutputStream();
			copyStreams(inStream, outStream);
		} catch (IOException e) {
			throw new UnableAccessResponseStream(e);
		} finally {
			closeStream(inStream);
			closeStream(outStream);
		}
	}

	private void copyStreams(InputStream inStream, OutputStream outStream) throws IOException {
		byte[] buffer = new byte[4096];
		int bytesRead = -1;

		while ((bytesRead = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}
	}

	protected final void closeStream(Closeable closeable) {
		try {
			if(closeable!=null) {closeable.close();}
		} catch (IOException e) {
			//ignore
		}
	}

	public final <T> T getJsonBody(HttpServletRequest request, Class<T> type) {
		return JsonUtils.readFromString(getStringBody(request), type);
	}

	public class UnableToreadRequestBody extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}

	public final class UnableToWriteOutput extends RuntimeException {
		private static final long serialVersionUID = 1L;

	}

	public final class UnableAccessResponseStream extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public UnableAccessResponseStream(IOException e) {
			super(e);
		}
	}

	public final class RequiredParameterNotSpecified extends RequestProcessFailed {
		private static final long serialVersionUID = 1L;

		public RequiredParameterNotSpecified(String id) {
			super(id);
		}
	}

}
