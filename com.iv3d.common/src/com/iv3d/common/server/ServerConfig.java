package com.iv3d.common.server;

import java.io.File;

import com.iv3d.common.utils.JsonUtils;

public class ServerConfig {

	private int port;
    private String host;
	private String dataDir;
	private long maxUploadSize;
	private int maxMemSize;
	private String tempUploadDir;
    private String appsDir;

	public ServerConfig() {
	    port = 58080;
        host = "0.0.0.0";
        tempUploadDir = "./temp_cache";
        maxUploadSize=51200;
        maxMemSize=4096;
        appsDir="cache/apps";
	}

	public static ServerConfig load(String path) {
		return JsonUtils.readFromFile(new File(path), ServerConfig.class);
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDataDir() {
		return dataDir;
	}

	public void setDataDir(String dataDir) {
		this.dataDir = dataDir;
	}

	public long getMaxUploadSize() {
		return maxUploadSize;
	}

	public void setMaxUploadSize(long maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
	}

	public int getMaxMemSize() {
		return maxMemSize;
	}

	public void setMaxMemSize(int maxMemSize) {
		this.maxMemSize = maxMemSize;
	}

	public String getTempUploadDir() {
		return tempUploadDir;
	}

	public void setTempUploadDir(String tempUploadFolder) {
		this.tempUploadDir = tempUploadFolder;
	}

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAppsDir() {
        return appsDir;
    }

    public void setAppsDir(String appsDir) {
        this.appsDir = appsDir;
    }

    public File getAppsDirAsFile() {
        return new File(this.appsDir);
    }
}
