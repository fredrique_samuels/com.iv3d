package com.iv3d.common.server;

public class RestResponse<T> {
	private String status="ok";
	private String message="";
	private T body=null;

	public String getStatus() {
		return status;
	}

	public RestResponse<T> setStatus(String status) {
		this.status = status;
		return this;
	}
	
	public RestResponse<T> setFailure(String message) {
		status="failed";
		setMessage(message);
		return this;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getBody() {
		return body;
	}

	public RestResponse<T> setBody(T body) {
		this.body = body;
		return this;
	}
}
