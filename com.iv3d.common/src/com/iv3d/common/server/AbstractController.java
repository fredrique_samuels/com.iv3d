package com.iv3d.common.server;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;

public abstract class AbstractController {
	private static Logger logger = Logger.getLogger(AbstractController.class.getName());
	
	private HashMap<String, PathHandler> paths;

	public AbstractController() {
		this.paths = new HashMap<String, PathHandler>();
	}

	protected final void set(String path, PathHandler pathHandler) {
		logger.info("Adding path handler "+path);
		paths.put(path, pathHandler);
	}

	protected final void handle(String path, Request baseRequest, HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws IOException, ServletException {
		PathHandler pathHandler = paths.get(path);
		if(pathHandler!=null) {
			pathHandler.handle(baseRequest, servletRequest, servletResponse);
		}
	}

	public abstract void configure();

	final boolean canHandlePath(String path, String method) {
		PathHandler pathHandler = paths.get(path);
		if(pathHandler!=null) {
			return method.equals(pathHandler.getMethod());
		}
		return false;
	}
	
}
