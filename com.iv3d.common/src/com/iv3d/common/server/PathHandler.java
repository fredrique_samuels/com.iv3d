package com.iv3d.common.server;

import org.eclipse.jetty.server.Request;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface PathHandler {
	void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	String getMethod();
}
