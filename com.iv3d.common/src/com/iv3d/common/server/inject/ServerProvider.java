package com.iv3d.common.server.inject;

import com.iv3d.common.server.inject.settings.ServerHostProvider;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.server.inject.settings.ServerPortProvider;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class ServerProvider {
	private final ServerPortProvider port;
    private final ServerHostProvider host;
    private final HandlersProvider handlersProvider;
    private SessionManager sessionManager;
    private Server server;

	@Inject
	public ServerProvider(ServerPortProvider port,
            ServerHostProvider host,
			HandlersProvider handlersProvider,
            SessionManager sessionManager) {
		this.port = port;
        this.host = host;
        this.handlersProvider = handlersProvider;
        this.sessionManager = sessionManager;
    }

	public Server get(){
		if(server==null) {
            QueuedThreadPool threadPool = new QueuedThreadPool();
            threadPool.setMaxThreads(1000);

			server = new Server(threadPool);

            final NetworkTrafficServerConnector connector = new NetworkTrafficServerConnector(server);
            connector.setPort(port.get());
            connector.setHost(host.get());
            server.setConnectors(new Connector[]{connector});

            HashSessionIdManager idmanager = new HashSessionIdManager();
            server.setSessionIdManager(idmanager);

            List<SessionHandler> handlers = Arrays.asList(handlersProvider.get())
                    .stream()
                    .map(h -> getSessionHandler(h))
                    .collect(Collectors.toList());

            HandlerList handlerList = new HandlerList();
            handlerList.setHandlers(handlers.toArray(new Handler[]{}));
            server.setHandler(handlerList);

        }
		return server;
	}

    private SessionHandler getSessionHandler(Handler handler) {
        SessionHandler sessionHandler = new SessionHandler(sessionManager);
        sessionHandler.setHandler(handler);
        return sessionHandler;
    }
}
