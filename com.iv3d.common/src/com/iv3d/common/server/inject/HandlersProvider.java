package com.iv3d.common.server.inject;

import org.eclipse.jetty.server.Handler;

public interface HandlersProvider {
	Handler[] get();
}
