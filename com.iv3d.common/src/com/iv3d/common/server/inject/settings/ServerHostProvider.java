package com.iv3d.common.server.inject.settings;

import com.google.inject.Inject;
import com.iv3d.common.server.ServerConfig;
import com.iv3d.common.server.inject.ServerConfigProvider;

public final class ServerHostProvider {

	private final ServerConfig config;

	@Inject
	public ServerHostProvider(ServerConfigProvider config) {
		this.config = config.get();
	}

	public String get() {
        return System.getProperty("jetty.host",config.getHost());
	}

}
