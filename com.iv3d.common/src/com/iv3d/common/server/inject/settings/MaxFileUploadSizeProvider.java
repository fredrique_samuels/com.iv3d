package com.iv3d.common.server.inject.settings;

import com.google.inject.Inject;
import com.iv3d.common.server.ServerConfig;
import com.iv3d.common.server.inject.ServerConfigProvider;

public class MaxFileUploadSizeProvider {
	private final ServerConfig config;

	@Inject
	public MaxFileUploadSizeProvider(ServerConfigProvider config) {
		this.config = config.get();
	}

	public long get() {
		return config.getMaxUploadSize();
	}
}
