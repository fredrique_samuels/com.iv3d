package com.iv3d.common.server.inject.settings;

import com.google.inject.Inject;
import com.iv3d.common.server.ServerConfig;
import com.iv3d.common.server.inject.ServerConfigProvider;

public final class ServerPortProvider {
	
	private final ServerConfig config;

	@Inject
	public ServerPortProvider(ServerConfigProvider config) {
		this.config = config.get();
	}

	public int get() {
        String port = System.getenv().get("PORT");
        if(port!=null) {
            return Integer.parseInt(port);
        }
        return Integer.valueOf(System.getProperty("jetty.port",String.valueOf(config.getPort())));
	}

}
