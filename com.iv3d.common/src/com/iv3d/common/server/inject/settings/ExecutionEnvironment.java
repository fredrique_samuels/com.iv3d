package com.iv3d.common.server.inject.settings;

/**
 * Created by fred on 2017/05/17.
 */
public final class ExecutionEnvironment {
    private final static String ENV_DEV = "dev";
    private final static String ENV_PROD = "prod";
    private final static String ENV_STAGE = "stage";

    public final boolean isProduction() {
        return ENV_PROD.equals(System.getProperty("exec.env", ENV_DEV));
    }

    public final boolean isStaging() {
        return ENV_STAGE.equals(System.getProperty("exec.env", ENV_DEV));
    }

    public final boolean isDev() {
        return ENV_DEV.equals(System.getProperty("exec.env", ENV_DEV));
    }

    public final String get(){
        if (isProduction())return ENV_PROD;
        if (isStaging())return ENV_STAGE;
        return ENV_DEV;
    }
}
