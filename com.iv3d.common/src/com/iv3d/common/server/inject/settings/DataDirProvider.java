package com.iv3d.common.server.inject.settings;

import com.google.inject.Inject;
import com.iv3d.common.server.ServerConfig;
import com.iv3d.common.server.inject.ServerConfigProvider;

public class DataDirProvider {
	private final ServerConfig config;
	
	@Inject
	public DataDirProvider(ServerConfigProvider config) {
		this.config = config.get();
	}
	
	public final String get() {
		return config.getDataDir();
	}
}
