package com.iv3d.common.server.inject;

import com.google.inject.Provider;
import com.iv3d.common.server.ServerConfig;
import com.iv3d.common.utils.ExceptionUtils;
import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Logger;

public final class ServerConfigProvider implements Provider<ServerConfig> {
    private static Logger logger = Logger.getLogger(ServerConfigProvider.class);
    private ServerConfig config;

	public ServerConfig get() {
		if(config==null) {
		    try {
                config = JsonUtils.readFromFile("/etc/webapp/server.settings.json", ServerConfig.class);
            } catch (Exception e) {
                logger.warn(ExceptionUtils.getStackTrace(e));
                return new ServerConfig();
            }
		}
		return config;
	}
}
