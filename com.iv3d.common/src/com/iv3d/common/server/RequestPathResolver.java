package com.iv3d.common.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.iv3d.common.utils.ExceptionUtils;
import com.iv3d.common.utils.JsonUtils;

public final class RequestPathResolver extends AbstractHandler {

	private final static Logger logger = Logger.getLogger(RequestPathResolver.class);	
	private final List<AbstractController> controllers;
	
	public RequestPathResolver() {
		this.controllers = new ArrayList<AbstractController>();
	}

	@Override
	public final void handle(String path, Request baseRequest, HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws IOException, ServletException {
		for(AbstractController c:controllers) {
			if(c.canHandlePath(path, baseRequest.getMethod())) {
				processRequest(path, baseRequest, servletRequest, servletResponse, c);
				break;
			}
		}
	}

	private void processRequest(String path, Request baseRequest, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, AbstractController c) {
		try {
			c.handle(path, baseRequest, servletRequest, servletResponse);
		} catch (RequestProcessFailed e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
			servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				handleRequestProcessError(servletResponse, e);
			} catch (IOException e1) {
				logAndReturnInternalServerError(servletResponse, e1);
			}
		}catch (Exception e) {
			logAndReturnInternalServerError(servletResponse, e);
		} finally {
			baseRequest.setHandled(true);
		}
	}

	private void logAndReturnInternalServerError(HttpServletResponse servletResponse, Exception e) {
		logger.warn(ExceptionUtils.getStackTrace(e));
		servletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	}

	private void handleRequestProcessError(HttpServletResponse servletResponse, RequestProcessFailed e)
			throws IOException {
		String message = e.getMessage();
		RestResponse<Object> response = new RestResponse<Object>().setFailure(message);
		String json = JsonUtils.writeToString(response);
		servletResponse.getWriter().println(json);
	}

	public void addController(AbstractController controller) {
		if(!controllers.contains(controller) && controller!=null) {
			controller.configure();
			controllers.add(controller);
		}
	}

}
