package com.iv3d.common.storage;
/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */


import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.date.UtcSystemDate;

import java.io.Serializable;

public class AuditedParam implements Serializable {
	public static final long UNSAVED = -1L;
	private long id;
	private UtcDate doe;
	private UtcDate dlu;
	private Long ulu;
    private boolean archived;
	
	public AuditedParam() {
		id = UNSAVED;
        doe = new UtcSystemClock().now();
        archived=false;
	}

	public final long getId() {
		return id;
	}

	public final void setId(long id) {
		this.id = id;
	}

	public final UtcDate getDoe() {
		return doe;
	}

	public final void setDoe(UtcDate createdDate) {
		this.doe = createdDate;
	}

    public final boolean isArchived(){return archived;}

    public final void setArchived(boolean b){archived=b;}

    public UtcDate getDlu() {
        return dlu;
    }

    public void setDlu(UtcDate dlu) {
        this.dlu = dlu;
    }

    public Long getUlu() {
        return ulu;
    }

    public void setUlu(Long ulu) {
        this.ulu = ulu;
    }

    public boolean isPersisted() {
        return id!=UNSAVED;
    }
}
