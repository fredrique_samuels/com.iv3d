package com.iv3d.common.storage.migrate;

import com.eroi.migrate.Configure;
import com.eroi.migrate.Engine;
import com.eroi.migrate.misc.Closer;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.server.inject.settings.ExecutionEnvironment;
import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Created by fred on 2017/05/18.
 */
public class SchemaMigrationRunner {
        private static Logger logger = Logger.getLogger(SchemaMigration.class);
        private String[] schemaFiles;

        public SchemaMigrationRunner(String[] schemaFiles) {
            this.schemaFiles = schemaFiles;
        }

        public final void updateAll() {
            List<SchemaMigrationConfig> migrationConfigList = new ArrayList<>();

            for(String schemaFile:schemaFiles) {
                SchemaMigrationConfig config = loadSchemaMigrationConfig(schemaFile);
                if(config!=null) {
                    migrationConfigList.add(config);
                }
            }

            updateAll(migrationConfigList);

        }

    public static SchemaMigrationConfig loadSchemaMigrationConfig(String schemaFile) {
        ExecutionEnvironment executionEnvironment = new ExecutionEnvironment();
        SchemaMigrationConfig config = null;
        String s = executionEnvironment.get();
        InputStream inputStream = ResourceLoader.read("/etc/config/sql/" + s + "/" + schemaFile);
        if (inputStream != null) {
            Properties properties = new Properties();
            try {
                properties.load(inputStream);
                String url = properties.getProperty("connection.url");
                String driver = properties.getProperty("connection.driver");
                String username = properties.getProperty("connection.username");
                String password = properties.getProperty("connection.password");
                String migrationPackage = properties.getProperty("migration.package.name");
                config = new SchemaMigrationConfig(url, username, password, driver, migrationPackage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return config;
    }

    public static void updateAll(Collection<SchemaMigrationConfig> migrationConfigList) {
        for (SchemaMigrationConfig c: migrationConfigList) {
            Connection connection = createConnection(c);
            if(connection==null) {
                logger.warn("Unable to make DB connection " + connection.toString());
                continue;
            }
            Configure.configure(connection, c.getMigrationPackage());
            createVersionTableIfNotExists(connection);
            validateVersion(connection);
            Engine.migrate();
            closeConnection();
        }
    }

    private static void validateVersion(Connection connection) {
            try {
                Engine.getCurrentVersion(connection);
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(ExceptionUtils.getStackTrace(e));
            } catch (Exception e) {
                createVersionEntry(connection);
            }
        }

        private static void createVersionEntry(Connection connection){
            String query = "INSERT INTO `version`\n" +
                    "(`version`)\n" +
                    "VALUES\n" +
                    "(0);";
            Statement statement = null;
            try {
                statement = connection.createStatement();
                statement.executeUpdate(query);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                Closer.close(statement);
            }
        }

        private static Connection createConnection(SchemaMigrationConfig c) {
            Connection connection = null;
            try {
                Class.forName(c.getDriver());
                connection = DriverManager.getConnection(c.getUrl(), c.getUser(), c.getPassword());
            } catch (ClassNotFoundException e) {
                logger.error("Class" + c.getDriver() + "not found ", new ClassNotFoundException());
                e.printStackTrace();
                logger.error(ExceptionUtils.getStackTrace(e));
            } catch (SQLException e) {
                e.printStackTrace();
                logger.warn(ExceptionUtils.getStackTrace(e));
            }
            return connection;
        }

        private static void createVersionTableIfNotExists(Connection connection) {
            String query = "CREATE TABLE IF NOT EXISTS `version` (\n" +
                    "  `version` int(11) DEFAULT NULL\n" +
                    ");";
            Statement statement = null;
            try {
                statement = connection.createStatement();
                statement.executeUpdate(query);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                Closer.close(statement);
            }
        }

        private static void closeConnection() {
            try {
                Configure.getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

