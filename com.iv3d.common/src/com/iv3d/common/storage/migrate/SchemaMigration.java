package com.iv3d.common.storage.migrate;

import com.eroi.migrate.Migration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by fred on 2017/05/18.
 */
public class SchemaMigration implements Migration {
    private final List<Migration> migrations;

    public SchemaMigration(Migration ... migrations) {
        this.migrations = Arrays.asList(migrations);
    }

    public void up() {
        migrations.stream().forEach(migration -> migration.up());
    }

    public void down() {
        migrations.stream().forEach(migration -> migration.down());
    }
}
