package com.iv3d.common.storage.migrate;

import com.google.inject.Singleton;
import com.iv3d.common.storage.hibernate.DbCredentials;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by fred on 2017/05/19.
 */
public class SchemaConfigManager {

    private final Map<String, SchemaMigrationConfig> configs;

    public SchemaConfigManager() {
        this.configs = new TreeMap<>();
    }

    public void addSchema(String alias, String configFile){
        if(configs.containsKey(alias)) {
            throw new DuplicateSchemaAliasError(alias, configFile);
        }
        this.configs.put(alias, SchemaMigrationRunner.loadSchemaMigrationConfig(configFile));
    }

    public void updateAll() {
        SchemaMigrationRunner.updateAll(configs.values());
    }

    public void clean() {

    }

    public DbCredentials getConfig(String alias) {
        return configs.get(alias);
    }

    private class DuplicateSchemaAliasError extends RuntimeException {
        public DuplicateSchemaAliasError(String alias, String configFile) {
            super("Trying to register a schema config under a duplicate alias : " + alias + " " + configFile);
        }
    }
}
