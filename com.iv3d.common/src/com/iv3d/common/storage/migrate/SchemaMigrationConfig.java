package com.iv3d.common.storage.migrate;

import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.storage.hibernate.DbCredentials;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by fred on 2017/05/17.
 */
public class SchemaMigrationConfig extends DbCredentials {

    private final String url;
    private final String user;
    private final String password;
    private final String driver;
    private final String migrationPackage;

    public SchemaMigrationConfig(String url, String user, String password, String driver, String migrationPackage) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.driver = driver;
        this.migrationPackage = migrationPackage;
    }

    @Override
    public final String getUrl() {
        return url;
    }

    @Override
    public final String getUser() {
        return user;
    }

    @Override
    public final String getPassword() {
        return password;
    }

    @Override
    public final String getDriver() {
        return driver;
    }

    public final String getMigrationPackage() {
        return migrationPackage;
    }

    @Override
    public String toString() {
        return " " + url + " " + driver + " " + migrationPackage;
     }

    public static SchemaMigrationConfig loadSchemaConfig(String filename) {
        InputStream inputStream = ResourceLoader.read(filename);
        if (inputStream != null) {
            Properties properties = new Properties();
            try {
                properties.load(inputStream);
                String url = properties.getProperty("connection.url");
                String driver = properties.getProperty("connection.driver");
                String username = properties.getProperty("connection.username");
                String password = properties.getProperty("connection.password");
                String migrationPackage = properties.getProperty("migration.package.name");
                return new SchemaMigrationConfig(url, username, password, driver, migrationPackage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
