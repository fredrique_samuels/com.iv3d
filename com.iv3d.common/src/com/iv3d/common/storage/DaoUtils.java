package com.iv3d.common.storage;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcDateParam;
import com.iv3d.common.date.UtcSystemDate;
import com.iv3d.common.utils.UriUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by fred on 2016/09/16.
 */
public class DaoUtils {
    public static String persist(String value) {
        if(value==null)
            return "";
        return UriUtils.encode(value);
    }

    public static String readString(String link) {
        return UriUtils.decode(link);
    }

    public static String persist(UtcDate value) {
        if(value==null)
            return "";
        return value.format("yyyy-MM-dd HH:mm:ss");
    }

    public static String persist(UtcDateParam value) {
        if(value==null)
            return "";
        return new UtcSystemDate(value.getMillisecondsSinceEpoc()).format("yyyy-MM-dd HH:mm:ss");
    }


    public static UtcDate readDate(String doe) {
        if(doe==null || doe.isEmpty())
            return null;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(doe);
            DateTime dateTime = new DateTime(date).withZone(DateTimeZone.UTC);
            return new UtcSystemDate(dateTime);
        }
        catch (ParseException ex){
            throw new DateParseError(ex);
        }
    }

    public static String persist(Enum<?> type) {
        return type.name();
    }

    public static  <T extends Enum> T readEnum(String name, Class<T> enumType) {
        Enum obj = Enum.valueOf(enumType, name);
        return enumType.cast(obj);
    }

    public static String dateNowForPersist() {
        return persist(UtcDate.now());
    }

    private static class DateParseError extends RuntimeException {
        public DateParseError(ParseException ex) {
            super(ex);
        }
    }
}
