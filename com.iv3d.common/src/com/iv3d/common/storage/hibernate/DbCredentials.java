package com.iv3d.common.storage.hibernate;

import com.iv3d.common.core.Credentials;

/**
 * Created by fred on 2017/05/17.
 */
public abstract class DbCredentials implements Credentials {
    public static final String DRIVER_MYSQL = "com.mysql.jdbc.Driver";
    public static final String DRIVER_SQLITE = "org.sqlite.JDBC";
    public abstract String getUrl();
    public abstract String getUser();
    public abstract String getPassword();
    public abstract String getDriver();
}
