package com.iv3d.common.storage.hibernate;

/**
 * Created by fred on 2017/05/24.
 */
public class DaoDelegate  {
    protected final AbstractHibernateDao dao;

    public DaoDelegate(AbstractHibernateDao dao) {
        this.dao = dao;
    }
}
