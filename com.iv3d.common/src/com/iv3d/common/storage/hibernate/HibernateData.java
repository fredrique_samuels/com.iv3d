package com.iv3d.common.storage.hibernate;

/**
 * Created by fred on 2017/05/21.
 */
public abstract class HibernateData {
    public abstract Long getId();
    public final boolean isPersisted(){return getId()!=null && getId()>0;}
}
