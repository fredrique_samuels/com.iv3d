/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.storage.db;

import java.util.List;

public interface DbManager {
	public void close();
	public void executeUpdate(String sql);
	public <T> T query(DbQuery<T> query);
	public <T> T queryUnique(DbQuery<List<T>> query);
	public void update(DbUpdate update);
	
	// Table methods
	public boolean tableExists(String tableName);
	public List<String> getTables();
	
	// schema version control
	public void setupSchemaVersionControl();
	public int getSchemaVersion();
	public void updateSchema(String scriptsRoot);
}
