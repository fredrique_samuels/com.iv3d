package com.iv3d.common.storage.db.sv;

/**
 * Created by fred on 2016/10/21.
 */
public interface MigrationRunner {
    void run();
}
