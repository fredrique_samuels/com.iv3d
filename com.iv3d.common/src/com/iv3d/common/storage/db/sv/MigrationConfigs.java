package com.iv3d.common.storage.db.sv;

/**
 * Created by fred on 2016/10/21.
 */
public class MigrationConfigs {
    private String id;
    private String bean;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }
}
