/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.storage.db.sv;

import java.io.File;
import java.util.List;

import com.iv3d.common.beans.BeanConfig;
import com.iv3d.common.beans.BeanLoader;
import com.iv3d.common.storage.db.DbManager;
import com.iv3d.common.utils.FileUtils;
import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Logger;

public final class SchemaUpdater {

    private static Logger logger = Logger.getLogger(SchemaUpdater.class);

	public synchronized final void run(File root, DbManager connection) {
		assertSchemaDirectory(root);
		int version = connection.getSchemaVersion();
		
		File nextVersion = new File(root, "v"+(++version));
		while(nextVersion.exists()) {
			try {
				runUpdate(connection, nextVersion);
			} catch (Exception e) {
				throw new SchemaUpdateError(nextVersion.getPath(), e);
			}
			connection.update(new UpdateSchemaVersion(version));
			nextVersion = new File(root, "v"+(++version));
		}
	}


	private void runUpdate(DbManager connection, File nextVersion) {
	    File migrationFile = null;

		File[] listFiles = nextVersion.listFiles();
		for (File file : listFiles) {
			if(file.isDirectory()) continue;
            if(file.getPath().endsWith("migration.json")) {
                migrationFile = file;
                continue;
            }
            if(!file.getPath().endsWith(".ddl")) continue;
			String sql = FileUtils.contentsAsString(file);
            logger.info(sql);
			connection.executeUpdate(sql);
		}
		runMigrations(migrationFile);
	}

    private void runMigrations(File migrationFile) {
        if(migrationFile==null) {
            return;
        }
        BeanLoader beanLoader = new BeanLoader();

        Migrations migrations = JsonUtils.readFromFile(migrationFile, Migrations.class);
        for(MigrationConfigs c : migrations.getBeans()) {
            BeanConfig beanConfig = new BeanConfig(){
                @Override
                public String getSource() {
                    return null;
                }

                @Override
                public String getLanguage() {
                    return BeanLoader.JAVA_BEAN;
                }

                @Override
                public String getBean() {
                    return c.getBean();
                }

                @Override
                public String getPath() {
                    return null;
                }
            };
            MigrationRunner load = beanLoader.load(beanConfig, MigrationRunner.class);
            load.run();
        }
    }


    private void assertSchemaDirectory(File root) {
		if(!root.exists())
			throw new SchemaScriptUpdatesNotFoundError(root.getPath());
	}

	public static class Migrations {
        private List<MigrationConfigs> beans;

        public List<MigrationConfigs> getBeans() {
            return beans;
        }

        public void setBeans(List<MigrationConfigs> beans) {
            this.beans = beans;
        }
    }
}
