/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.storage.db.sv;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.iv3d.common.storage.db.DbManager;
import com.iv3d.common.storage.db.DbQuery;

public class GetSchemaVersion implements DbQuery<Integer> {

	@Override
	public String getSql() {
		return String.format("SELECT * FROM %s;", DbSchemaVersion.TABLE_NAME);
	}

	@Override
	public Integer parse(ResultSet resultSet, DbManager connection) throws SQLException {
		while(resultSet.next()) {
	        return resultSet.getInt("version");
		}
		return DbSchemaVersion.SCHEMA_VERSION_NOT_CONFIGURED;
	}

}
