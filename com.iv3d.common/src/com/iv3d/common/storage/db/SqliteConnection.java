/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.storage.db;

import com.iv3d.common.storage.db.queries.sqllite.GetDbTables;
import com.iv3d.common.storage.db.sv.*;

import java.io.File;
import java.sql.*;
import java.util.List;

public final class SqliteConnection implements DbManager {

	private Connection connection;

	public final class SqliteError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public SqliteError(Exception e) {
			super(e);
		}
	}
	
	public final class UniqueResultNotFound extends RuntimeException{
		private static final long serialVersionUID = 1L;
	}
	
	public SqliteConnection(String db) {
	    try {
	      Class.forName("org.sqlite.JDBC");
	      this.connection = DriverManager.getConnection(db);
	      this.connection.setAutoCommit(true);
	    } catch ( Exception e ) {
	    	throw new SqliteError(e);
	    }
	}
	
	public synchronized final void executeUpdate(String sql) {
		Statement stmt = createStatement();
	    try {
			stmt.executeUpdate(sql);
            if(!connection.getAutoCommit()) {
                connection.commit();
            }
		} catch (SQLException e) {
			throw new SqliteError(e);
		} finally {			
			closeStatement(stmt);
		}
	}
	
	public final void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new SqliteError(e);
		}	
	}

	public final <T> T query(DbQuery<T> query) {
		Statement stmt = createStatement();
		T value = null;
	    try {
	    	value = getQueryResult(query, stmt);
	    	closeStatement(stmt);
		} catch (Exception e) {
			closeStatement(stmt);
			throw new SqliteError(e);
		} 
		return value;
	}
	
	@Override
	public <T> T queryUnique(DbQuery<List<T>> q) {
		List<T> list = query(q);
		if(list.size()>1)
			throw new UniqueResultNotFound();
		if(list.size()==1)
			return list.get(0);
		return null;
	}

	public <T> T getQueryResult(DbQuery<T> query, Statement stmt) throws SQLException {
		ResultSet result = stmt.executeQuery(query.getSql());
		T value = null;
		try {
			value = query.parse(result, this);
			closeResult(result);
		} catch (Exception e) {
			closeResult(result);
			throw new SqliteError(e);
		} 
		return value;
	}
	
	public final void update(DbUpdate dbUpdate) {
		executeUpdate(dbUpdate.getSql(this));
	}
	
	public final void setupSchemaVersionControl() {
		 if(tableExists(DbSchemaVersion.TABLE_NAME))
			 return;
		 createVersionTableIfNotExists();
		 update(new InsertDefaultSchemaVersion());
	}

	public final boolean tableExists(String tableName) {
		List<String> tables = getTables();
		return tables.contains(tableName);
	}
	
	public final int getSchemaVersion() {
		 if(tableExists(DbSchemaVersion.TABLE_NAME))
			 return query(new GetSchemaVersion());
		 return -1;
	}
	
	@Override
	public final List<String> getTables() {
		return query(new GetDbTables());
	}

	@Override
	public void updateSchema(String scriptsRoot) {
		File root = new File(scriptsRoot);
		new SchemaUpdater().run(root, this);
	}

	private void closeResult(ResultSet result) {
		if(result!=null)
			try {
				result.close();
			} catch (SQLException e) {
				// ignore
			}
	}
	
	private void createVersionTableIfNotExists() {
	    update(new CreateSchemaVersionTable());
	}	

	private void closeStatement(Statement stmt) {
		try {
			stmt.close();
		} catch (SQLException e) {
			// ignore
		}
	}

	private Statement createStatement() {
		try {
			return connection.createStatement();
		} catch (SQLException e) {
			throw new SqliteError(e);
		}
	}

}
