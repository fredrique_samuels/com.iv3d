/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control;

import java.util.List;

public abstract class AbstractFlowViewBuilder<FlowContext, FlowViewType extends FlowView, FormViewType extends FormView, InputViewType extends InputView, DisplayViewType extends DisplayView> 
		implements FlowViewBuilder<FlowContext, FlowViewType, FormViewType, InputViewType, DisplayViewType> {
	
	protected String name;
	protected String hint;
	protected List<FormViewType> forms;
	protected List<InputViewType> inputs;
	protected List<DisplayViewType> displays;

	@Override
	public final void setName(String name) {
		this.name = name;
	}

	@Override
	public final void setHint(String hint) {
		this.hint = hint;
	}

	@Override
	public final void setInputs(List<InputViewType> inputs) {
		this.inputs = inputs;
	}
	
	@Override
	public final void setDisplays(List<DisplayViewType> displays) {
		this.displays = displays;
	}

	@Override
	public final void setForms(List<FormViewType> forms) {
		this.forms = forms;
	}

}
