/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control;

import java.util.List;

public abstract class AbstractFormViewBuilder<FlowContextType, FormViewClass extends FormView, InputViewClass extends InputView, CommandClass extends CommandView> 
		implements FormViewBuilder<FlowContextType, FormViewClass, InputViewClass, CommandClass> {

	protected String id;
	protected String name;
	protected List<InputViewClass> inputs;
	protected List<CommandClass> commands;

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setInputs(List<InputViewClass> inputs) {
		this.inputs = inputs;
	}

	@Override
	public void setCommands(List<CommandClass> commands) {
		this.commands = commands;
	}

}
