/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.Arrays;
import java.util.List;

public class FormConfig {
	private String id;
	private String name;
	private List<InputConfig> inputs;
	private List<CommandConfig> formCommands;

	public FormConfig(DefaultFormConfigBuilder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.inputs = builder.getInputs();
		this.formCommands = builder.getFormCommands();
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<InputConfig> getInputs() {
		return Arrays.asList(inputs.toArray(new InputConfig[0]));
	}

	public List<CommandConfig> getFormCommands() {
		return Arrays.asList(formCommands.toArray(new CommandConfig[0]));
	}
}
