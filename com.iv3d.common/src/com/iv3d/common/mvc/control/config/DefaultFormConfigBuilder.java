/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.iv3d.common.mvc.control.InputType;

final class DefaultFormConfigBuilder implements FormConfigBuilder {

	String id;
	String name;
	private final List<InputConfigBuilder> inputBuilders;
	private final List<CommandConfigBuilder> formCommands;

	public DefaultFormConfigBuilder(String id) {
		this.id = id;
		this.name = "";
		this.inputBuilders = new ArrayList<>(); 
		this.formCommands = new ArrayList<>();
	}

	@Override
	public FormConfig build() {
		return new FormConfig(this);
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public InputConfigBuilder createInput(String id, InputType type) {
		DefaultInputConfigBuilder builder = new DefaultInputConfigBuilder(id, type);
		this.inputBuilders.add(builder);
		return builder;
	}

	List<InputConfig> getInputs() {
		return inputBuilders.stream().map(b->b.build()).collect(Collectors.toList());
	}

	@Override
	public CommandConfigBuilder createFormCommand(String id) {
		DefaultCommandConfigBuilder builder = new DefaultCommandConfigBuilder(id);
		this.formCommands.add(builder);
		return builder;
	}

	List<CommandConfig> getFormCommands() {
		return formCommands.stream().map(b->b.build()).collect(Collectors.toList());
	}

}
