/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class FlowConfig {
	private String name;
	private String hint;
	private List<InputConfig> inputs;
	private List<DisplayConfig> displays;
	private List<FormConfig> forms;
	private Map<String, String> userProperties;

	public FlowConfig() {
	}

	public FlowConfig(DefaultFlowConfigBuilder builder) {
		this.name = builder.name;
		this.hint = builder.hint;
		this.inputs = builder.getInputs();
		this.displays = builder.getDisplays();
		this.forms = builder.getForms();
		this.userProperties = Collections.unmodifiableMap(builder.getUserProperties());
	}

	public String getName() {
		return this.name;
	}

	public String getHint() {
		return this.hint;
	}

	public List<InputConfig> getInputs() {
		return Arrays.asList(inputs.toArray(new InputConfig[0]));
	}

	public List<DisplayConfig> getDisplays() {
		return Arrays.asList(displays.toArray(new DisplayConfig[0]));
	}

	public List<FormConfig> getForms() {
		return Arrays.asList(forms.toArray(new FormConfig[0]));
	}

	public Map<String, String> getUserProperties() {
		return userProperties;
	}
}
