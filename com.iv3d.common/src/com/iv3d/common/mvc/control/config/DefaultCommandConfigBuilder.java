/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.ArrayList;
import java.util.List;

import com.iv3d.common.mvc.control.CommandType;


public class DefaultCommandConfigBuilder implements CommandConfigBuilder {

	final String id;
	final List<CommandParam> params;
	String name;
	CommandType type;

	public DefaultCommandConfigBuilder(String id) {
		this.id = id;
		this.params = new ArrayList<>();
		this.name = "";
		this.type = CommandType.DEFAULT;
	}

	@Override
	public final CommandConfig build() {
		return new CommandConfig(this);
	}

	@Override
	public final void addParam(String name, String value) {
		this.params.add(new CommandParam(name, value));
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void asFormSubmit() {
		type = CommandType.FORM_SUBMIT;
	}

}
