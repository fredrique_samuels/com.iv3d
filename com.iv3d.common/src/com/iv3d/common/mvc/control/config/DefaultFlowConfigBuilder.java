/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.iv3d.common.mvc.control.InputType;

public final class DefaultFlowConfigBuilder implements FlowConfigBuilder {

	String name;
	String hint;
	private final List<InputConfigBuilder> inputBuilders;
	private final List<DisplayConfigBuilder> displayBuilders;
	private final List<FormConfigBuilder> formBuilders;
	private Map<String, String> userProperties;
	
	public DefaultFlowConfigBuilder() {
		this.name = "";
		this.hint = "";
		this.inputBuilders = new ArrayList<>();
		this.displayBuilders = new ArrayList<>();
		this.formBuilders = new ArrayList<>();
		this.userProperties = new HashMap<>();
	}

	public List<FormConfig> getForms() {
		return formBuilders
				.stream()
				.map(b->b.build())
				.collect(Collectors.toList());
	}

	public List<DisplayConfig> getDisplays() {
		return displayBuilders.stream().map(b->b.build()).collect(Collectors.toList());
	}

	public List<InputConfig> getInputs() {
		return inputBuilders
				.stream()
				.map(b->b.build())
				.collect(Collectors.toList());
	}

	@Override
	public FlowConfig build() {
		return new FlowConfig(this);
	}

	public DefaultFlowConfigBuilder setName(String name) {
		this.name = name;return this;
	}

	public DefaultFlowConfigBuilder setHint(String hint) {
		this.hint = hint;
        return this;
	}

	public InputConfigBuilder createInput(String id, InputType type) {
		DefaultInputConfigBuilder builder = new DefaultInputConfigBuilder(id, type);
		this.inputBuilders.add(builder);
		return builder;
	}

	public DisplayConfigBuilder createDisplay() {
		DefaultDisplayConfigBuilder builder = new DefaultDisplayConfigBuilder();
		this.displayBuilders.add(builder);
		return builder;
	}

	public FormConfigBuilder createForm(String id) {
		DefaultFormConfigBuilder builder = new DefaultFormConfigBuilder(id);
		formBuilders.add(builder);
		return builder;
	}

	public void addUserProperty(String key, String value) {userProperties.put(key, value);}
	public Map<String, String> getUserProperties() {return userProperties;}
	
}
