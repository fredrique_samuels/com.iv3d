/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.service;

import java.util.List;

import com.iv3d.common.mvc.control.CommandExecutor;
import com.iv3d.common.mvc.control.CommandView;
import com.iv3d.common.mvc.control.DisplayType;
import com.iv3d.common.mvc.control.DisplayView;
import com.iv3d.common.mvc.control.FlowView;
import com.iv3d.common.mvc.control.FormView;
import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.InputView;
import com.iv3d.common.mvc.control.config.CommandConfig;
import com.iv3d.common.mvc.control.config.DisplayConfig;
import com.iv3d.common.mvc.control.config.FormConfig;
import com.iv3d.common.mvc.control.config.InputConfig;

interface ViewConfigParserService<FlowViewType extends FlowView, FormViewType extends FormView, InputViewType extends InputView, DisplayViewType extends DisplayView, CommandType extends CommandView> {
	String getNotNull(String value);
	InputType getNotNull(InputType type);
	DisplayType getNotNull(DisplayType type);
	List<InputViewType> getInputViews(List<InputConfig> inputs);
	List<DisplayViewType> getDisplayViews(List<DisplayConfig> displays);
	List<FormViewType> getFormViews(List<FormConfig> forms);
	List<CommandType> getFormCommands(List<CommandConfig> formCommands, List<InputViewType> inputs);
	CommandExecutor getCommandExecutor();
}
