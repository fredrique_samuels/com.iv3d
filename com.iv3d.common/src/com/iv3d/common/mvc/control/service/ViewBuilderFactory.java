/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.service;

import com.iv3d.common.mvc.control.CommandView;
import com.iv3d.common.mvc.control.CommandViewBuilder;
import com.iv3d.common.mvc.control.DisplayView;
import com.iv3d.common.mvc.control.DisplayViewBuilder;
import com.iv3d.common.mvc.control.FlowView;
import com.iv3d.common.mvc.control.FlowViewBuilder;
import com.iv3d.common.mvc.control.FormView;
import com.iv3d.common.mvc.control.FormViewBuilder;
import com.iv3d.common.mvc.control.InputView;
import com.iv3d.common.mvc.control.InputViewBuilder;

public interface ViewBuilderFactory<FlowContextType, 
									FlowViewType extends FlowView,
									FormViewType extends FormView,
									InputViewType extends InputView,
									DisplayViewType extends DisplayView,
									CommandType extends CommandView> {
	public FlowContextType createFlowContext();
	public FlowViewBuilder<FlowContextType, FlowViewType, FormViewType, InputViewType, DisplayViewType> createFlowViewBuilder();
	public InputViewBuilder<FlowContextType, InputViewType> createInputViewBuilder();
	public DisplayViewBuilder<FlowContextType, DisplayViewType> createDisplayViewBuilder();
	public FormViewBuilder<FlowContextType, FormViewType, InputViewType, CommandType> createFormViewBuilder();
	public CommandViewBuilder<FlowContextType, CommandType> createCommandBuilder();
}
