/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.iv3d.common.core.NonNullRules;
import com.iv3d.common.mvc.control.AbstractBuilder;
import com.iv3d.common.mvc.control.CommandExecutor;
import com.iv3d.common.mvc.control.CommandType;
import com.iv3d.common.mvc.control.CommandView;
import com.iv3d.common.mvc.control.CommandViewBuilder;
import com.iv3d.common.mvc.control.DisplayType;
import com.iv3d.common.mvc.control.DisplayView;
import com.iv3d.common.mvc.control.DisplayViewBuilder;
import com.iv3d.common.mvc.control.FlowView;
import com.iv3d.common.mvc.control.FlowViewBuilder;
import com.iv3d.common.mvc.control.FormView;
import com.iv3d.common.mvc.control.FormViewBuilder;
import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.InputView;
import com.iv3d.common.mvc.control.InputViewBuilder;
import com.iv3d.common.mvc.control.ViewControlService;
import com.iv3d.common.mvc.control.ViewController;
import com.iv3d.common.mvc.control.config.CommandConfig;
import com.iv3d.common.mvc.control.config.CommandParam;
import com.iv3d.common.mvc.control.config.DisplayConfig;
import com.iv3d.common.mvc.control.config.FlowConfig;
import com.iv3d.common.mvc.control.config.FormConfig;
import com.iv3d.common.mvc.control.config.InputConfig;
import com.iv3d.common.mvc.control.config.ViewConfig;

public final class DefaultViewControlService<FlowContextType, FlowViewClass extends FlowView, FormViewClass extends FormView, InputViewClass extends InputView, DisplayViewClass extends DisplayView, CommandViewClass extends CommandView> implements 
		ViewControlService<FlowContextType, FlowViewClass, FormViewClass, InputViewClass, DisplayViewClass, CommandViewClass> {
	private final NonNullRules nonNullRules;
	private final ViewController controller;
	private ViewBuilderFactory<FlowContextType, FlowViewClass, FormViewClass, InputViewClass, DisplayViewClass, CommandViewClass> viewBuilderFactory;
	
	public DefaultViewControlService(NonNullRules nonNullRules, ViewController controller) {
		super();
		this.nonNullRules = nonNullRules;
		this.controller = controller;
	}
	
	public CommandExecutor getCommandExecutor() {return controller;}
	public String getNotNull(String value) {return nonNullRules.get(value, String.class);}
	public InputType getNotNull(InputType value) {return nonNullRules.get(value, InputType.class);}
	public DisplayType getNotNull(DisplayType value) {return nonNullRules.get(value, DisplayType.class);}

	public final void run(String id) {
		ViewConfig viewConfig = controller.getView(id);
		List<FlowViewClass> flowViews = getFlowViews(viewConfig.getFlows());
		for (FlowViewClass view : flowViews) {
			view.run();
		}
	}
	
	@Override
	public void setViewBuilderFactory(ViewBuilderFactory<FlowContextType, FlowViewClass, FormViewClass, InputViewClass, DisplayViewClass, CommandViewClass> factory) {
		viewBuilderFactory = factory;	
	}
 
	private <ResultType, ConfigType, BuilderType extends AbstractBuilder> List<ResultType> 
			getViews(List<ConfigType> configs, 
					Parser<FlowContextType, ResultType, ConfigType, BuilderType> parser,  
					Provider<BuilderType> builderProvider,
					Provider<FlowContextType> contextProvider) {
		if(configs==null)
			return Arrays.asList();
		ArrayList<ResultType> result = new ArrayList<>();
		if (configs != null) {
			for (ConfigType config : configs) {
				BuilderType builder = builderProvider.get();
				if(builder==null) {
					break;
				}
				
				ResultType view = createView(parser, builder, config,contextProvider.get());
				result.add(view);
			}
		}
		return result;
	}

	private <ResultType, ConfigType, BuilderType> ResultType createView(
			Parser<FlowContextType, ResultType, ConfigType, BuilderType> parser, BuilderType builder,
			ConfigType config, FlowContextType context) {
		ResultType view = parser.build(context, config, builder);
		return view;
	}

	private List<FlowViewClass> getFlowViews(List<FlowConfig> configs) {
		return getViews(configs, new FlowConfigParser(), ()->viewBuilderFactory.createFlowViewBuilder(),
				()->viewBuilderFactory.createFlowContext());
	}
	
	private List<InputViewClass> getInputViews(List<InputConfig> configs, FlowContextType context) {
		return getViews(configs, new InputConfigParser(), ()->viewBuilderFactory.createInputViewBuilder(), ()->context);
	}

	private List<DisplayViewClass> getDisplayViews(List<DisplayConfig> configs, FlowContextType context) {
		return getViews(configs, new DisplayConfigParser(), ()->viewBuilderFactory.createDisplayViewBuilder(), ()->context);
	}

	private List<FormViewClass> getFormViews(List<FormConfig> configs, FlowContextType context) {
		return getViews(configs, new FormConfigParser(), ()->viewBuilderFactory.createFormViewBuilder(), ()->context);
	}

	private List<CommandViewClass> getCommands(List<CommandConfig> configs, List<CommandParam> formParams, FlowContextType context) {
		return getViews(configs, new CommandConfigParser(controller, formParams), ()->viewBuilderFactory.createCommandBuilder(), ()->context);
	}
	
	private interface Provider<Type> {
		Type get();
	}
	
	private interface Parser<FlowContextType,ResultType, ConfigType, BuilderType> {
		ResultType build(FlowContextType context, ConfigType config, BuilderType builder);
	}
	
	private class FlowConfigParser implements Parser<FlowContextType,FlowViewClass, FlowConfig, FlowViewBuilder<FlowContextType, FlowViewClass, FormViewClass, InputViewClass, DisplayViewClass>> {
		public FlowViewClass build(FlowContextType context, FlowConfig config, FlowViewBuilder<FlowContextType, FlowViewClass, FormViewClass, InputViewClass, DisplayViewClass> builder){
			String name = getNotNull(config.getName());
			String hint = getNotNull(config.getHint());
			List<InputViewClass> inputs = getInputViews(config.getInputs(), context);
			List<DisplayViewClass> displays = getDisplayViews(config.getDisplays(), context);
			List<FormViewClass> forms = getFormViews(config.getForms(), context);
			
			builder.setName(name);
			builder.setHint(hint);
			builder.setInputs(Collections.unmodifiableList(inputs));
			builder.setDisplays(Collections.unmodifiableList(displays));
			builder.setForms(Collections.unmodifiableList(forms));
			return builder.build(null);	
		}
	}
	
	private class InputConfigParser implements Parser<FlowContextType, InputViewClass, InputConfig, InputViewBuilder<FlowContextType, InputViewClass>> {
		public InputViewClass build(FlowContextType context, InputConfig config, InputViewBuilder<FlowContextType, InputViewClass> builder){
			String name = getNotNull(config.getName());
			String id = getNotNull(config.getId());
			InputType type = getNotNull(config.getType());
			
			builder.setId(id);
			builder.setName(name);
			
			switch (type) {
			case PARAGRAPH:
				return builder.buildParagraph(context);
			case PASSWORD:
				return builder.buildPassword(context);
			default:
				return builder.buildText(context);	
			}
		}
	}
	
	private class DisplayConfigParser implements Parser<FlowContextType, DisplayViewClass, DisplayConfig, DisplayViewBuilder<FlowContextType, DisplayViewClass>> {
		public DisplayViewClass build(FlowContextType context, DisplayConfig config, DisplayViewBuilder<FlowContextType, DisplayViewClass> builder){
			String name = getNotNull(config.getName());
			String id = getNotNull(config.getId());
			String stringValue = getNotNull(config.getStringValue());
			DisplayType type = getNotNull(config.getType());
			
			builder.setId(id);
			builder.setName(name);
			
			switch (type) {
			case TEXT:
				return builder.buildText(context, stringValue);
			case PARAGRAPH:
				return builder.buildParagraph(context, stringValue);
			default:
				return builder.buildText(context, "");	
			}
		}
	}
	
	private class FormConfigParser implements Parser<FlowContextType, FormViewClass, FormConfig, FormViewBuilder<FlowContextType, FormViewClass, InputViewClass, CommandViewClass>> {
		public FormViewClass build(FlowContextType context, FormConfig config, FormViewBuilder<FlowContextType, FormViewClass, InputViewClass, CommandViewClass> builder){
			String name = getNotNull(config.getName());
			String id = getNotNull(config.getId());
			List<InputViewClass> inputViews = getInputViews(config.getInputs(), context);
			List<CommandParam> params = getInputViewParams(inputViews);
			List<CommandViewClass> commands = getCommands(config.getFormCommands(), params, context);
			
			builder.setId(id);
			builder.setName(name);
			builder.setInputs(Collections.unmodifiableList(inputViews));
			builder.setCommands(Collections.unmodifiableList(commands));
			
			return builder.build(context);	
		}

		private List<CommandParam> getInputViewParams(List<InputViewClass> inputViews) {
			return inputViews.stream().map(v->v.getParam()).collect(Collectors.toList());
		}
	}
	
	private class CommandConfigParser implements Parser<FlowContextType, CommandViewClass, CommandConfig, CommandViewBuilder<FlowContextType, CommandViewClass>> {
		
		private CommandExecutor commandExecutor;
		private List<CommandParam> formParams;

		public CommandConfigParser(CommandExecutor commandExecutor, List<CommandParam> params) {
			this.commandExecutor = commandExecutor;
			this.formParams = params;
		}

		public CommandViewClass build(FlowContextType context, CommandConfig config, CommandViewBuilder<FlowContextType, CommandViewClass> builder){
			String id = getNotNull(config.getId());
			String name = getNotNull(config.getName());
			List<CommandParam> commandParams = getCommandParams(config);

			builder.setId(id);
			builder.setName(name);
			builder.setInputParams(Collections.unmodifiableList(commandParams));
			return builder.build(null, commandExecutor);
		}

		private List<CommandParam> getCommandParams(CommandConfig config) {
			ArrayList<CommandParam> arrayList = new ArrayList<>();
			List<CommandParam> inputParams = config.getParams();
			if(inputParams!=null)
				arrayList.addAll(inputParams);
			if(formParams!=null && config.getType()==CommandType.FORM_SUBMIT)
				arrayList.addAll(formParams);
			return arrayList;
		}
	}
}
