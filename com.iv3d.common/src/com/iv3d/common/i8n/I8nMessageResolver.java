package com.iv3d.common.i8n;

import com.google.inject.Inject;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by fred on 2017/08/03.
 */
public class I8nMessageResolver {
    private static Logger logger = Logger.getLogger(I8nMessageResolver.class);
    private LanguageProvider languageProvider;

    @Inject
    public void setLanguageProvider(LanguageProvider languageProvider) {
        this.languageProvider = languageProvider;
    }

    public String getMessage(String key) {
        Language language = languageProvider!=null?languageProvider.get():Language.ENGLISH;
        return getMessage(key, language);
    }

    public String getMessage(String key, Language language) {
        String languageCodeExt = language.getCodeExt();
        String path = "/etc/config/i8n/" + languageCodeExt + ".locale.json";
        String json = ResourceLoader.readAsString(path);

        if(json==null && language!=Language.ENGLISH) {
            logger.warn("Unable to locate locale messages file "+ path+ ". Reverting to ENGLISH lookup.");
            return getMessage(key, Language.ENGLISH);
        }

        if(json==null) {
            logger.warn("Unable to locate locale messages file "+ path+ ".");
            return key;
        }

        Map<String, Object> messages = JsonUtils.parseToMap(json);
        Object o = messages.get(key);
        if(o==null){
            logger.warn("No message found for key " + key + " in file " + path);
            return key;
        }

        return o.toString();
    }

}



