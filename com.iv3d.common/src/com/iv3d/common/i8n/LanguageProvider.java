package com.iv3d.common.i8n;

/**
 * Created by fred on 2017/08/05.
 */
public interface LanguageProvider {
    Language get();
}
