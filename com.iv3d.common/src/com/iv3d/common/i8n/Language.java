package com.iv3d.common.i8n;

/**
 * Created by fred on 2017/08/03.
 */
public enum Language {
    AFRIKAANS("af", "Afrikaans"),
    ARABIC("ar", "العربية"),
    BULGARIAN("bg", "български"),
    CHINESE("zh", "中文(简体)","CN"),
    CHINESE_TRADITIONAL("zh", "中文(繁體)","TW"),
    CZECH("cs", "čeština"),
    DANISH("da", "Dansk"),
    DUTCH("nl", "Nederlands"),
    ENGLISH("en", "English (United States)","US"),
    ENGLISH_GB("en", "English (United Kingdom)", "GB"),
    ENGLISH_AU("en", "English (Australia)", "AU"),
    ENGLISH_CA("en", "English (Canada)", "CA"),
    ENGLISH_NZ("en", "English (New Zealand)", "NZ"),
    ENGLISH_ZA("en", "English (South Africa)", "ZA"),
    ESTONIAN("et", "eesti"),
    FILIPINO("tl", "Filipino"),
    FINISH("fi", "suomi"),
    FRENCH("fr", "Français"),
    FRENCH_CA("fr", "Français (Canada)", "CA"),
    GERMAN("de", "Deutsch"),
    HEBREW("he", "עברית"),
    HINDI("hi","हिन्दी"),
    INDONESIAN("id", "Bahasa Indonesia"),
    ITALIAN("it", "Italiano"),
    JAPANESE("ja", "日本語"),
    KOREAN("ko", "한국어"),
    LATVIAN("lv", "latviešu"),
    LITHUANIAN("lt", "lietuvių kalba"),
    MALAY("ms", "Malay"),
    MALTESE("mt", "Malti"),
    NORWEGIAN("no", "Norsk"),
    PORTUGUESE("pt", "Português"),
    PORTUGUESE_BR("pt", "Português (Brasil)", "BR"),
    RUSSIAN("ru", "Русский"),
    SLOVENIAN("sl", "slovenščina"),
    SPANISH("es", "Español"),
    SPANISH_MX("es", "Español (México)", "MX"),
    SWEDISH("sv", "Svensk"),
    THAI("th", "ไทย"),
    TURKISH("tr", "Türkçe"),
    VIETNAMESE("vi", "Tiếng Việt");

    private final String code;
    private final String name;
    private String ext;

    Language(String code, String name, String ext) {
        this.code = code;
        this.name = name;
        this.ext = ext;
    }

    Language(String code, String name) {
        this(code, name, null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getCodeExt() {
        return code + (ext!=null?"_"+ext.toLowerCase():"");
    }
}
