package com.iv3d.common.map;

import com.iv3d.common.core.LatLng;
import com.iv3d.common.core.MapCoords;
import com.iv3d.common.core.PointDouble;

public class SphericalMercatorProjection {
    private final double mWorldWidth;
	private final double mWorldHeight;

    public SphericalMercatorProjection(final double worldDim) {
        this(worldDim, worldDim);
    }
    
    public SphericalMercatorProjection(final double w, final double h) {
        mWorldWidth = w;
        mWorldHeight = h;
    }

    public PointDouble toPoint(final LatLng latLng) {
        final double x = latLng.getLongitude() / 360 + .5;
        final double siny = Math.sin(Math.toRadians(latLng.getLatitude()));
        final double y = 0.5 * Math.log((1 + siny) / (1 - siny)) / -(2 * Math.PI) + .5;

        return new PointDouble(x * mWorldWidth, y * mWorldHeight);
    }

    public LatLng toLatLng(PointDouble point) {
        final double x = point.getX() / mWorldWidth - 0.5;
        final double lng = x * 360;

        double y = .5 - (point.getY() / mWorldHeight);
        final double lat = 90 - Math.toDegrees(Math.atan(Math.exp(-y * 2 * Math.PI)) * 2);

        return new MapCoords(lat, lng);
    }
    
    public final String toString() {
    	return String.format("[%s w=%.3f h=%.3f]", 
    			SphericalMercatorProjection.class, mWorldWidth, mWorldHeight);
    }
}