/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iv3d.common.core.ResourceLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

public final class JsonUtils {

    public static Map<String, Object> parseToMap(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, new TypeReference<Map<String, String>>(){});
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JsonNode parseToTreeModel(File file) {
        ObjectMapper mapper = new ObjectMapper();

        /**
         * Read values from conceptual JSON tree
         */
        JsonNode rootNode = null;
        try {
           return mapper.readTree(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

//        JsonNode carNode = rootNode.path("car");
//        System.out.println(carNode.asText());
//
//        JsonNode priceNode = rootNode.path("price");
//        System.out.println(priceNode.asText());
//
//        JsonNode modelNode = rootNode.path("model");
//        System.out.println(modelNode.asText());
//
//        JsonNode colorsNode = rootNode.path("colors");
//        Iterator<JsonNode> colors = colorsNode.elements();
//
//        while(colors.hasNext()){
//            System.out.println(colors.next().asText());
//        }
    }

    public static final class JsonError extends RuntimeException {
		public JsonError(Exception e) {
			super(e);
		}

		private static final long serialVersionUID = 1L;
		
	}

	public static void writeToFile(File file, Object object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(file, object);
		} catch (JsonGenerationException e) {
			throw new JsonError(e);
		} catch (JsonMappingException e) {
			throw new JsonError(e);
		} catch (IOException e) {
			throw new JsonError(e);
		}
	}


	public static <T> T readFromFile(File file, Class<T> type) {
		String s = ResourceLoader.readAsString(file.getPath());
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(s, type);
		} catch (JsonParseException e) {
			throw new JsonError(e);
		} catch (JsonMappingException e) {
			throw new JsonError(e);
		} catch (IOException e) {
			throw new JsonError(e);
		}
	}

    public static <T> T readFromFile(String path, Class<T> type) {
        String s = ResourceLoader.readAsString(path);
        if(s==null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(s, type);
        } catch (JsonParseException e) {
            throw new JsonError(e);
        } catch (JsonMappingException e) {
            throw new JsonError(e);
        } catch (IOException e) {
            throw new JsonError(e);
        }
    }

	public static String writeToString(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new JsonError(e);
		}
	}


	public static <T> T readFromString(String json, Class<T> type) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, type);
		} catch (IOException e) {
			throw new JsonError(e);
		}
	}
	
}
