/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.utils;

import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public final class RmiGateway {
	
	private final Registry registry;
	
	public RmiGateway(int port) {
		registry = create(port);
	}
	
	public RmiGateway(String host, int port) {
		registry = getRegistry(host, port);
	}
	
	public final void bind(String name, Remote remote) {
		try { 
			registry.bind(name, remote);
		} catch (RemoteException e) {
			throw new RmiRemoteError(e);
		} catch (AlreadyBoundException e) {
			throw new RmiAlreadyBoundError(e);
		}
	}
	
	public final void rebind(String name, Remote remote){
		try { 
			registry.bind(name, remote);
		} catch (RemoteException e) {
			throw new RmiRemoteError(e);
		} catch (AlreadyBoundException e) {
			throw new RmiAlreadyBoundError(e);
		}
	}
	
	public final void unbind(String id) {
		try { 
			registry.unbind(id);
		} catch (RemoteException e) {
			throw new RmiRemoteError(e);
		} catch (NotBoundException e) {
		}
	}
	
	public final <T> T getObject(String objectName, Class<T> type) {
		try {
			Remote lookup = registry.lookup(objectName);
			return type.cast(lookup);
		} catch (AccessException e) {
			throw new RmiRemoteError(e);
		} catch (RemoteException e) {
			throw new RmiRemoteError(e);
		} catch (NotBoundException e) {
			throw new RmiRemoteError(e);
		}
	}

	private Registry create(int port) {
		try {
			return LocateRegistry.createRegistry(port);
		} catch (RemoteException e) {
			throw new RmiRemoteError(e);
		}
	}
	
	private Registry getRegistry(String host, int port) {
		try {
			return LocateRegistry.getRegistry(host, port);
		} catch (RemoteException e) {
			throw new RmiRemoteError(e);
		}
	}
	
	public final class RmiRemoteError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public RmiRemoteError(Exception e) {
			super(e);
		}	
	}
	
	public final class RmiAlreadyBoundError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public RmiAlreadyBoundError(AlreadyBoundException e) {
			super(e);
		}	
	}
	

}
