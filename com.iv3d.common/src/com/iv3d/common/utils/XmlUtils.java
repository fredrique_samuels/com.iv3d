/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.utils;

import com.iv3d.common.core.ResourceLoader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;

public class XmlUtils {
	private XmlUtils() {}

	public static <T> T read(File pluginFile, Class<T> type) {
		InputStream stream = ResourceLoader.read(pluginFile.getAbsolutePath());
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(type);
			Unmarshaller u = jc.createUnmarshaller();
			Object unmarshal = u.unmarshal(stream);
			return type.cast(unmarshal);
		} catch (JAXBException e) {
			throw new ConfigParsingError(e);
		}
	}
	
	public final static class ConfigParsingError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public ConfigParsingError(JAXBException e) {super(e);}
	}
}
