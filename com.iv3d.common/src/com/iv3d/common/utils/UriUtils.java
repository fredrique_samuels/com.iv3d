/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.utils;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public final class UriUtils {
	public static final class SyntaxError extends RuntimeException {

		public SyntaxError(URISyntaxException e) {
			super(e);
		}
		private static final long serialVersionUID = 1L;
	}
	
	public static final class EncodingError extends RuntimeException {
		public EncodingError(UnsupportedEncodingException e) {
			super(e);
		}

		private static final long serialVersionUID = 1L;
	}
	
	public static final class DecodingError extends RuntimeException {
		public DecodingError(UnsupportedEncodingException e) {
			super(e);
		}

		private static final long serialVersionUID = 1L;
	}

	private UriUtils(){}
	
	public static URI createForUrl(String url) {
		try {
			return new URI(url);
		} catch (URISyntaxException e) {
			throw new SyntaxError(e);
		}
	}
	
	public static String encode(String uri) {
		try {
			return uri==null?null:URLEncoder.encode(uri, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new EncodingError(e);
		}
	}
	
	public static String decode(String uri) {
		try {
			return uri==null?null:URLDecoder.decode(uri, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new DecodingError(e);
		}
	}
}
