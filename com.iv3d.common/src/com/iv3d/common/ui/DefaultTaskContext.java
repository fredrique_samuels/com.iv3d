/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui;

import com.iv3d.common.core.Callback;

import java.util.HashMap;
import java.util.Map;

public class DefaultTaskContext implements UiTaskContext, UiCancelable {
	private boolean canceled;
	private Callback<UiTaskStatus> updateCallable;
	private final Map<String, Object> params;
	
	public DefaultTaskContext() {
		this.params = new HashMap<String, Object>();
	}

	@Override
	public final void postUpdate(UiTaskStatus status) {
		if(this.updateCallable!=null) {
			this.updateCallable.invoke(status);
		}
	}
	
	@Override
	public final boolean isCanceled() {
		return this.canceled;
	}

	@Override
	public final void cancel() {
		this.canceled = true;
	}
	
	@Override
	public final <T> T getParam(String paramName, Class<T> type) {
		return this.params.containsKey(paramName)?type.cast(params.get(paramName)):null;
	}

	public final void addParam(String key, Object value) {
		this.params.put(key, value);
	}

	public final void setTaskUpdateCallback(Callback<UiTaskStatus> updateCallable) {
		this.updateCallable = updateCallable;
	}
}
