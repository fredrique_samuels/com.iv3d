/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.ui;

import java.io.Serializable;

/**
 * A builder for {@link UiTaskResult} instances. 
 */
public final class UiTaskResultBuilder {
	private String message = "";
	private Exception exception;
	private Object value;

	/**
	 * @param message The result message.
	 * @return The {@link UiTaskResultBuilder}
	 */
	public final UiTaskResultBuilder setMessage(String message) {
		this.message = message==null?"":message;
		return this;
	}
	
	/**
	 * Set the exception for this result.
	 * This will also set the message to the exception message
	 * if {@link #setMessage(String)} has not been invoked yet.
	 * 
	 * @param exception The exception object.
	 * @return The {@link UiTaskResultBuilder}
	 */
	public final UiTaskResultBuilder setException(Exception exception) {
		this.exception = exception;
		if(message.isEmpty()) {
			message = exception.getMessage();
		}
		return this;
	}

	/**
	 * A short hand method to set the exception. Has the same effect as 
	 * {@link #setException(new Exception(message))}
	 * 
	 * @param message The exception message.
	 * @return The {@link UiTaskResultBuilder}
	 */
	public final UiTaskResultBuilder configureAsError(String message) {	
		setMessage(message);
		setException(new Exception(message));
		return this;
	}

	/**
	 * Set the result value.
	 * 
	 * @param value The result value.
	 * @return The {@link UiTaskResultBuilder}
	 */
	public final UiTaskResultBuilder setValue(Object value) {
		this.value = value;
		return this;
	}
	
	/**
	 * Build the result object.
	 * 
	 * @return The result objects.
	 */
	public final UiTaskResult build() {
		return new UiTaskResultImpl(this);
	}

	private class UiTaskResultImpl implements UiTaskResult, Serializable {

		private static final long serialVersionUID = 1L;

		private String message;
		private Exception exception;
		private Object value;

		public UiTaskResultImpl(UiTaskResultBuilder builder) {
			this.message = builder.message;
			this.exception = builder.exception;
			this.value = builder.value;
		}

		@Override
		public Exception getException() {
			return exception;
		}

		@Override
		public String getMessage() {
			return message;
		}

		@Override
		public Object getValue() {
			return value;
		}

		@Override
		public boolean hasError() {
			return exception!=null;
		}
	}
}
