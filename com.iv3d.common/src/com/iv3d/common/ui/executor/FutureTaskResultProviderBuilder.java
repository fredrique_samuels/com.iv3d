/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui.executor;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskResultBuilder;

/**
 * Builder that creates a {@link TaskResultProvider}
 * for {@link Future} tasks results.
 */
public final class FutureTaskResultProviderBuilder {
	
	private Future<UiTaskResult> future;
	private long timeout;

	/**
	 * Default constructor.
	 */
	public FutureTaskResultProviderBuilder() {
		this.timeout = 0;
	}

	/**
	 * Set the future task.
	 * 
	 * @param future The future task.
	 * @return The {@link FutureTaskResultProviderBuilder}
	 */
	public final FutureTaskResultProviderBuilder setFuture(Future<UiTaskResult> future) {
		this.future = future;
		return this;
	}

	/**
	 * Set the task timeout.
	 * 
	 * @param timeout The timeout in milliseconds.
	 * @return The {@link FutureTaskResultProviderBuilder}
	 */
	public final FutureTaskResultProviderBuilder setTimeout(long timeout) {
		this.timeout = timeout;
		return this;
	}

	/**
	 * Build the {@link TaskResultProvider}.
	 * @return The {@link TaskResultProvider}.
	 */
	public final TaskResultProvider build() {
		if(future==null){return new NoFutureImpl();}
		return new TaskFutureImpl(this);
	}

	private class NoFutureImpl implements TaskResultProvider {

		@Override
		public UiTaskResult getResult() {
			return new UiTaskResultBuilder()
				.setMessage("Unable to build TaskFuture")
				.setException(new Exception("No java.util.concurrent.Future was set."))
				.build();
		}
	}
	
	private class TaskFutureImpl implements TaskResultProvider {
		
		private Future<UiTaskResult> future;
		private long timeout;

		public TaskFutureImpl(FutureTaskResultProviderBuilder builder) {
			this.future = builder.future;
			this.timeout = builder.timeout;
		}

		@Override
		public UiTaskResult getResult() {
			UiTaskResult value = null;
	        try {
	        	if(timeout==0)
	        		value = future.get();
	    		else
	    			value = future.get(timeout, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				value = new UiTaskResultBuilder()
							.setMessage("Task failed!")
							.setException(e)
							.build();
			} catch (ExecutionException e) {
				value = new UiTaskResultBuilder()
							.setMessage("Task failed!")
							.setException(e)
							.build();
			} catch (TimeoutException e) {
				value = new UiTaskResultBuilder()
							.setMessage("Task failed!")
							.setException(e)
							.build();
			}
			return value;
		}
	}
}
