/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.rest;

import com.iv3d.common.utils.UriUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class HttpRestClient implements RestClient {

	private final RestTemplate restTemplate;

	HttpRestClient(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public HttpRestClient() {
		this(new RestTemplate());
	}
	
	@Override
	public final String get(String link) {return execute(new SimpleGetRequest(link), String.class).getBody();}

	@Override
	public final <R> ResponseEntity<R> execute(RestRequest request, Class<R> responseType) {
		if(request.getMethod()==HttpMethod.GET) {
			URI url = request.getUrl();
			return restTemplate.exchange(url, 
					HttpMethod.GET, 
					new HttpEntity<String>(request.getHeaders()), 
					responseType);
		} else if(request.getMethod()==HttpMethod.POST) {
			URI url = request.getUrl();
			return restTemplate.exchange(url, 
					HttpMethod.POST, 
					new HttpEntity<String>(request.getPayload(), request.getHeaders()), 
					responseType);
		}
		return null;
	}

	public final RequestRunner buildGet(String url) {
	    return new RequestRunner(this, url, HttpMethod.GET);
    }

    public final RequestRunner buildPost(String url) {
        return new RequestRunner(this, url, HttpMethod.POST);
    }
	
	protected List<HttpMessageConverter<?>> buildMessageConverters() {
        HttpMessageConverter<?> formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter<?> stringHttpMessageConverternew = new StringHttpMessageConverter();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(formHttpMessageConverter);
        messageConverters.add(stringHttpMessageConverternew);
        return messageConverters;
    }

	private static class SimpleGetRequest implements RestRequest {
		final URI uri;
		
		public SimpleGetRequest(String link) {uri = URI.create(link);}
		public URI getUrl() {return uri;}
		public HttpMethod getMethod() {return HttpMethod.GET;}
		public HttpHeaders getHeaders() {return new HttpHeaders();}
		public String getPayload() {return null;}
	}

	public static class RequestRunner {
        private final HttpRestClient client;
        private final String url;
        private final Map<String, String> params;
        private final HttpMethod method;

        private RequestRunner(HttpRestClient client, String url, HttpMethod method) {
            this.client = client;
            this.url = url;
            this.method = method;
            this.params = new HashMap<>();
        }

        public final RequestRunner addParam(String key, String value) {
            this.params.put(key, value);
            return this;
        }

        public final String getStringResult() {
            return getResult().getBody();
        }

        public final ResponseEntity<String> getResult() {
            RestRequest r = getRestRequest();
            return client.execute(r, String.class);
        }

        public RestRequest getRestRequest() {
            return new RestRequest() {
                        @Override
                        public URI getUrl() {
                            try {
                                StringBuilder sb = new StringBuilder(url);

                                int count = 1;

                                for (Map.Entry<String, String> e : params.entrySet()) {
                                    String p = String.format("%s=%s", e.getKey(), UriUtils.encode(e.getValue()));
                                    if(count==0) {
                                        sb.append("?");
                                        sb.append(p);
                                    } else {
                                        sb.append("&");
                                        sb.append(p);
                                    }
                                    count++;
                                }

                                return new URI(sb.toString());
                            } catch (URISyntaxException e) {
                                throw new UriFormatError(e);
                            }
                        }

                        @Override
                        public HttpMethod getMethod() {
                            return method;
                        }

                        @Override
                        public HttpHeaders getHeaders() {
                            return new HttpHeaders();
                        }

                        @Override
                        public String getPayload() {
                            return null;
                        }
                    };
        }

        private class UriFormatError extends RuntimeException {
            public UriFormatError(URISyntaxException e) {
                super(e);
            }
        }
    }
}
