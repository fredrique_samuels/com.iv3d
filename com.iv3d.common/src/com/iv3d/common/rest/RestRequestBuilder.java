package com.iv3d.common.rest;

import com.iv3d.common.utils.UriUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public final class RestRequestBuilder {
	private final String host;
	private final String path;
	private HttpMethod httpMethod;
	private String payload;
	private Map<String, String> params;

	public RestRequestBuilder(String host, String path) {
		this.host = host;
		this.path = path;
		this.httpMethod = HttpMethod.GET;
	}
	
	public final RestRequestBuilder setMethodGet() {
		this.httpMethod = HttpMethod.GET;
		return this;
	}
	
	public final RestRequestBuilder setMethodPost() {
		this.httpMethod = HttpMethod.POST;
		return this;
	}

	public final RestRequestBuilder setParams(Map<String, String> params) {
		this.params = params;
		return this;
	}
	
	public final RestRequest build() {
		return new Impl(this);
	}


	private static class Impl implements RestRequest {

		private final URI uri;
		private final HttpMethod httpMethod;
		private final String payload;

		public Impl(RestRequestBuilder builder) {
			this.uri = createUri(builder.host, builder.path, builder.params);
			this.httpMethod = builder.httpMethod;
			this.payload = builder.payload;

		}

		@Override
		public URI getUrl() {
			return uri;
		}

		@Override
		public HttpMethod getMethod() {
			return httpMethod;
		}

		@Override
		public HttpHeaders getHeaders() {
			return createHeaders();
		}

		@Override
		public String getPayload() {
			return payload;
		}

		private HttpHeaders createHeaders() {
			return new HttpHeaders();
		}
		
	}

	private static URI createUri(String host, String path, Map<String, String> params) {
		StringBuilder sb = new StringBuilder(host);
		if(host.endsWith("/") && path.startsWith("/")) {
			sb.append(path.substring(1));
		} else if(!host.endsWith("/") && !path.startsWith("/")) {
			sb.append("/");
			sb.append(path);
		} else if(!host.endsWith("/") && !path.startsWith("/")) {
			sb.append("/");
			sb.append(path);
		} else if(!host.endsWith("/") && path.startsWith("/")) {
			sb.append(path);
		} else if(host.endsWith("/") && !path.startsWith("/")) {
			sb.append(path);
		}

		if(params!=null) {
			int count = 1;
			for (Map.Entry<String, String> e : params.entrySet()) {
				String p = String.format("%s=%s", e.getKey(), UriUtils.encode(e.getValue()));
				if(count==0) {
					sb.append("?");
					sb.append(p);
				} else {
					sb.append("&");
					sb.append(p);
				}
				count++;
			}
		}
		
		return stringToUri(sb);
	}

	private static URI stringToUri(StringBuilder sb) {
		try {
			return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new UriSyntaxError(e);
		}
	}
	
	public static final class UriSyntaxError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public UriSyntaxError(URISyntaxException e) {super(e);}
	}
	
}
