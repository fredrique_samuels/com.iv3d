/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.pagination;

/**
 * Public interface for paginated result values.
 *
 * @param <T> The page type.
 */
public interface PagedResult<T> {
	
	/**
	 * @return The page contents.
	 */
	T[] data();
	
	/**
	 * @return The total number of pages
	 */
	long totalPages();
	
	/**
	 * @return the current page number.
	 */
	long currentPage();
	
	/**
	 * @return Number of result items on this page.
	 */
	long size();
	
	/**
	 * @return  <code>true</code> if this is not the last page.
	 */
	boolean hasNext();
	
	/**
	 * @return <code>true</code> if this is not the first page.
	 */
	boolean hasPrev();
	
	/**
	 * @return The next page or the current one if it is the last page.
	 */
	PagedResult<T> next();
	
	/**
	 * @return The previous page or the current page if this is the first page.
	 */
	PagedResult<T> prev();
}
