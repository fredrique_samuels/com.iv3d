package com.iv3d.common.pagination;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2017/06/10.
 */
public class PaginationParam<T> implements Serializable {
    private int totalPages = 0;
    private int pageNumber = 0;
    private int size = 0;
    private List<T> results = Collections.EMPTY_LIST;

    public PaginationParam() {
    }

    public PaginationParam(int totalPages, int pageNumber, List<T> results) {
        this.totalPages = totalPages;
        this.pageNumber = pageNumber;
        this.results = results;
        this.size=results.size();
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

}
