/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.pagination;

/**
 * Public interface for all data generating objects.
 */
public interface DataGenerator<T> {	
	
	/**
	 * Get the total number of result in the data source.
	 * This is used along with the pageSize to determine the page count.
	 * 
	 * @return The total number of results in the data source.
	 */
	long totalRecords();
	
	/**
	 * Generate the data from the given index with maximum number of records
	 * not exceeding <tt>max</tt>.
	 * 
	 * @param offset The record offset.
	 * @param max The maximum number of records to return.
	 * @return The records results.
	 */
	T[] generate(long offset, long maxRecords);
}
