/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.core;

import java.util.HashMap;
import java.util.Map;

public final class NonNullRules {

	private final Map<Class<?>, Object> types;
	
	public NonNullRules() {
		types = new HashMap<>();
	}

	@SuppressWarnings("unchecked")
	public final <T> T get(T value, Class<T> type) {
		if(!types.containsKey(type)) {
			throw new TypeNotRegisteredError(type);
		}
		if(value==null) {
			return (T)types.get(type);
		}
		return value;
	}

	public final <T> void add(T alternateValue, Class<T> type) {
		if (alternateValue == null)
			throw new IllegalArgumentException("Cannot provide null as an alternate value");
		types.put(type, alternateValue);
	}

	public final class TypeNotRegisteredError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public TypeNotRegisteredError(Class<? extends Object> type) {
			super(String.format("No non null value for type %s not registered.", type.getName()));
		}
	}
}