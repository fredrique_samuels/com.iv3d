package com.iv3d.common.core;

/**
 * Created by fred on 2016/09/11.
 */
public class RectangleDouble {
    private double x;
    private double y;
    private double width;
    private double height;

    public RectangleDouble(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return String.format("<Rectangle x=%.3f y=%.3f w=%.3f h=%.3f >",
                x, y, width, height);
    }
}
