package com.iv3d.common.core;

/**
 * Created by fred on 2016/09/07.
 */
public final class ThreadUtils {
    private ThreadUtils() {
    }

    public static void delay(long millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException e) {
            throw new InterruptedError(e);
        }
    }

    private static class InterruptedError extends RuntimeException {
        public InterruptedError(InterruptedException e) {
            super(e);
        }
    }
}
