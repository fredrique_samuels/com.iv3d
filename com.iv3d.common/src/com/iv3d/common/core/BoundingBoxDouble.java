/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.core;

import com.iv3d.common.utils.JsonUtils;

import java.util.HashMap;

public final class BoundingBoxDouble {
	final double minX;
	final double minY;
	final double maxX;
	final double maxY;
	final double  width;
	final double  height;
	
	public BoundingBoxDouble() {
		this(0,0,0,0,0,0);
	}
	
	public BoundingBoxDouble(double minX, double minY, double maxX, double maxY, double width, double height) {
		super();
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.width = width;
		this.height = height;
	}

    public BoundingBoxDouble(double minX, double minY, double maxX, double maxY) {
        super();
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;
        this.width = maxX-minX;
        this.height = maxY-minY;
    }

	public final double getMinX() {
		return minX;
	}

	public final double getMinY() {
		return minY;
	}

	public final double getMaxX() {
		return maxX;
	}

	public final double getMaxY() {
		return maxY;
	}

	public final double getWidth() {
		return width;
	}

	public final double getHeight() {
		return height;
	}
	
	public final BoundingBoxDouble combine(BoundingBoxDouble bb) {
		double minX = this.minX < bb.minX ? this.minX : bb.minX;
		double minY = this.minY < bb.minY ? this.minY : bb.minY;
		double maxX = this.maxX > bb.maxX ? this.maxX : bb.maxX;
		double maxY = this.maxY > bb.maxY ? this.maxY : bb.maxY;
		double width = maxX - minX;
		double height = maxY - minY;
		return new BoundingBoxDouble(minX, minY, maxX, maxY, width, height);
	}
	
	public static BoundingBoxDouble fromJson(String json) {
		@SuppressWarnings("unchecked")
		HashMap<String, Object> values = JsonUtils.readFromString(json, HashMap.class);
		double minX = get(values, "minX");
		double minY = get(values, "minY");
		double maxX = get(values, "maxX");
		double maxY = get(values, "maxY");
		double width = get(values, "width");
		double height = get(values, "height");
		return new BoundingBoxDouble(minX, minY, maxX, maxY, width, height);
	}

	private static Double get(HashMap<String, Object> values, String key) {
		return Double.valueOf(values.get(key).toString());
	}

	public final String toJson() {
		return JsonUtils.writeToString(this);
	}

    @Override
    public String toString() {
        return toJson();
    }

    public final BoundingBoxInt asBoundingBoxInt() {
		int minX = Double.valueOf(this.minX).intValue();
		int minY = Double.valueOf(this.minY).intValue();
		int maxX = Double.valueOf(this.maxX).intValue();
		int maxY = Double.valueOf(this.maxY).intValue();
		return new BoundingBoxInt(minX, minY, maxX, maxY);
	}

}
