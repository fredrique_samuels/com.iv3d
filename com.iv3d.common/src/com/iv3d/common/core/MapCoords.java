/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MapCoords extends LatLng {

	public MapCoords(double latitude, double longitude) {
	    super(latitude, longitude);
	}
	
	public final String toString() {
		return String.format("[MapCoords lat=%.3f long=%.3f]", getLatitude(), getLongitude());
	}

    public static String encode(MapCoords mapCoords) {
        return String.format("mapcoords_lat%.3f_lng%.3f", mapCoords.getLatitude(), mapCoords.getLongitude());
    }

    public static MapCoords decode(String s) {
        Pattern compile = Pattern.compile("mapcoords_lat([-+]?[0-9]*\\.?[0-9]+)_lng([-+]?[0-9]*\\.?[0-9]+)");
        Matcher m = compile.matcher(s);
        if (m.find( )) {
            double lat = Double.valueOf(m.group(1));
            double lng = Double.valueOf(m.group(2));
            return new MapCoords(lat, lng);
        }
        return null;
    }
}
