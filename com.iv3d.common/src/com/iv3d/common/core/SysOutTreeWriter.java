package com.iv3d.common.core;

/**
 * Created by fred on 2016/08/12.
 */
public class SysOutTreeWriter implements TreeWriter {

    @Override
    public void write(int level, String value) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0;i<level;++i) {
            stringBuilder.append("\t");
        }
        stringBuilder.append(value);
        System.out.println(stringBuilder);
    }

}
