package com.iv3d.common.core;

/**
 * Created by fred on 2016/08/12.
 */
public interface TreeWriter {
    void write(int level, String value);
}
