package com.iv3d.common.core;

/**
 * Created by fred on 2016/10/20.
 */
public class LatLng {
    private double latitude;
    private double longitude;

    public LatLng() {
        this(0,0);
    }

    public LatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
