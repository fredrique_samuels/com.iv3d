/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.core;

import com.iv3d.common.utils.JsonUtils;

import java.util.HashMap;

public final class BoundingBoxInt {
	final int minX;
	final int minY;
	final int maxX;
	final int maxY;
	final int  width;
	final int  height;

	public BoundingBoxInt() {
		this(0,0,0,0);
	}

	public BoundingBoxInt(int minX, int minY, int maxX, int maxY) {
		super();
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
        this.width = maxX-minX;
        this.height = maxY-minY;
	}

	public final int getMinX() {
		return minX;
	}

	public final int getMinY() {
		return minY;
	}

	public final int getMaxX() {
		return maxX;
	}

	public final int getMaxY() {
		return maxY;
	}

	public final int getWidth() {
		return width;
	}

	public final int getHeight() {
		return height;
	}
	
	public final BoundingBoxInt combine(BoundingBoxInt bb) {
		int minX = this.minX < bb.minX ? this.minX : bb.minX;
		int minY = this.minY < bb.minY ? this.minY : bb.minY;
		int maxX = this.maxX > bb.maxX ? this.maxX : bb.maxX;
		int maxY = this.maxY > bb.maxY ? this.maxY : bb.maxY;
		return new BoundingBoxInt(minX, minY, maxX, maxY);
	}

    public final boolean intersects(BoundingBoxInt other) {
        boolean pointInside = intersects(other, this);
        boolean pointInside1 = intersects(this, other);
        return pointInside || pointInside1;
    }

    private boolean intersects(BoundingBoxInt other, BoundingBoxInt o2) {
        boolean iminx = other.minX >= o2.minX && other.minX <= o2.maxX;
        boolean imaxx = other.maxX >= o2.minX && other.maxX <= o2.maxX;
        boolean iminy = other.minY >= o2.minY && other.minY <= o2.maxY;
        boolean imaxy = other.maxY >= o2.minY && other.maxY <= o2.maxY;
        boolean yInside = iminy || imaxy;
        boolean xInside = iminx || imaxx;
        boolean xEncloses = other.minX <= o2.minX && other.maxX >= o2.maxX;
        return (xInside || xEncloses) && yInside;
    }

    public static BoundingBoxInt fromJson(String json) {
		@SuppressWarnings("unchecked")
		HashMap<String, Object> values = JsonUtils.readFromString(json, HashMap.class);
		int minX = get(values, "minX");
		int minY = get(values, "minY");
		int maxX = get(values, "maxX");
		int maxY = get(values, "maxY");
		return new BoundingBoxInt(minX, minY, maxX, maxY);
	}

	private static int get(HashMap<String, Object> values, String key) {
		return Integer.valueOf(values.get(key).toString());
	}

	public final String toJson() {
		return JsonUtils.writeToString(this);
	}

	public static BoundingBoxInt fromRectangle(Rectangle r) {
        int minX = r.getX();
        int minY = r.getY();
        int maxX = r.getX()+r.getWidth();
        int maxY =r.getY()+r.getHeight();
        return new BoundingBoxInt(minX, minY, maxX, maxY);
    }

    public BoundingBoxInt offset(int x, int y) {
        return new BoundingBoxInt(x+this.getMinX(), y+minY, x+maxX, y+maxY);
    }

    public final BoundingBoxInt setMaxX(int maxX) {
        return new BoundingBoxInt(minX, minY, maxX, maxY);
    }

    public final BoundingBoxInt setMinX(int minX) {
        return new BoundingBoxInt(minX, minY, maxX, maxY);
    }

    public final BoundingBoxInt setMinY(int minY) {
        return new BoundingBoxInt(minX, minY, maxX, maxY);
    }

    public final BoundingBoxInt setMaxY(int maxY) {
        return new BoundingBoxInt(minX, minY, maxX, maxY);
    }

    @Override
    public String toString() {
        return String.format("[%s minX=%d maxX=%d minY=%d maxY=%d]",
            BoundingBoxInt.class,minX, maxX, minY, maxY);
    }
}
