package com.iv3d.common.core;

/**
 * Created by fred on 2017/08/01.
 */
public interface Credentials {
    String getUser();
    String getPassword();
}
