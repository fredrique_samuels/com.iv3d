/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.jython;

import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import java.net.URL;

public final class JythonUtils {
	
	private static PythonInterpreter interpreter;    
	
	private JythonUtils() {
	}
	
	public static void init() {
		get();
	}
	
	public static PythonInterpreter get() {
		if(interpreter==null) {
			interpreter = new PythonInterpreter();      
	        interpreter.exec("import sys, os;print 'Python working dir ' + os.getcwd()");
		}
		return interpreter;
	}
	
	public static void addLibPath(String path) {
		try {
            get().exec(String.format("if not (os.path.isdir('%s') or os.path.isfile('%s')):print 'Unable to find location \"%s\"'", path, path, path));
			get().exec(String.format("if not '%s' in sys.path and (os.path.isdir('%s') or os.path.isfile('%s')):sys.path+=['%s']", path, path, path, path));
		} catch (Exception ex) {
			throw new JythonException("Unable to append path.", ex);
		}
	}
	
	protected static String getClassName(String clazz) {
		if(!clazz.contains(".")) {
			return clazz;
		}
		
		int index = clazz.lastIndexOf(".");
		return index>=clazz.length() ? "" : clazz.substring(index+1);
	}
	
	protected static String getFullClassNameAlias(String clazz) {
		return clazz.replace(".", "_");
	}
	
	/**
	 * Create a new Java object from a Jython class.
	 * 
	 * Given <code>com.jython.test.MyBuilding.MyBuilding</code> will create a new 
	 * instance of com.jython.test.MyBuilding.MyBuilding.
	 * 
	 * @param clazz The class name.
	 * @param javaType The type that this object needs to be in Java.
	 * @return The new java object.
	 * @throws JythonException If the class is not found or an exception occurs. 
	 */
	public static <T> T newBean(String clazz, Class<T> javaType) {
		String fullClassNameAlias = getFullClassNameAlias(clazz);
		String importCode = String.format("from %s import %s as %s", 
				getClassPackage(clazz), getClassName(clazz), fullClassNameAlias);
		
		try {
			interpreter.exec(importCode);			
		} catch (Exception e) {
			String message = String.format("Unable to import class '%s' with code '%s'",clazz, importCode);
			throw new JythonException(message, e); 
		}
		
		
		PyObject clazzType = interpreter.get(fullClassNameAlias);
		if(clazzType==null) {
			throw new JythonException("Unable to locate class type " + fullClassNameAlias); 
		}
		
        
		PyObject buildingObject = null;
		try {
        	buildingObject = clazzType.__call__();
		} catch (Exception e) {
			throw new JythonException("Unable to construct new instance of " + clazz, e);
		}
        
		try {
			return javaType.cast(buildingObject.__tojava__(javaType));			
		} catch (Exception e) {
			String message = "Unable to cast Jython class %s to %s";
			throw new JythonException(String.format(message, clazz, javaType.getName()), e);
		}
	}
	
	private static String getClassPackage(String clazz) {
		assert(clazz.contains("."));
		
		int index = clazz.lastIndexOf(".");
		return index>=clazz.length() ? null : clazz.substring(0, index);
	}

	public static class JythonException extends RuntimeException {
		
		private static final long serialVersionUID = 1L;

		/**
		 * Create a new {@link JythonException}.
		 * 
		 * @param message The error message.
		 * @param e The {@link Exception} being wrapped.
		 */
		public JythonException(String message, Exception e) {
			super(message, e);
		}

		/**
		 * Create a new {@link JythonException}.
		 * 
		 * @param message The error message.
		 */
		public JythonException(String message) {
			super(message);
		}
	}

	public static void exec(String code) {
		get().exec(code);
	}

	public static void addLibPath(URL url) throws JythonException {
		addLibPath(url.getPath());
	}
}
