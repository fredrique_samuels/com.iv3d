/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.beans;

import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.jython.JythonUtils;
import com.iv3d.common.jython.JythonUtils.JythonException;
import org.apache.log4j.Logger;

import java.net.URL;

public class JythonBeanLoaderStrategy implements
		BeanLoaderStrategy {

	private final Logger logger = org.apache.log4j.Logger.getLogger(JythonBeanLoaderStrategy.class);

	@Override
	public <T> T load(BeanConfig beanConfig, Class<T> clazz) {
		String path = beanConfig.getPath();
		if(path==null)
			throw new NoModulePathSetError();
		
		URL url = ResourceLoader.getUrl(path);
		if(url==null)
			throw new CannotResolvePathError(path);
		
		try {
			String file = url.getFile();
			JythonUtils.addLibPath(file);
			logger.debug(String.format("Adding path '%s' to Jython imports.", path));
			return JythonUtils.newBean(beanConfig.getBean(), clazz);
		} catch (JythonException e) {
			throw new JythonError(e);
		}
	}
	
	public final class NoModulePathSetError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public NoModulePathSetError() {
			super();
		}
	}

	public final class CannotResolvePathError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public CannotResolvePathError(String path) {
			super(path);
		}
	}

	public final class JythonError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public JythonError(JythonException e) {
			super(e);
		}
	}
}
