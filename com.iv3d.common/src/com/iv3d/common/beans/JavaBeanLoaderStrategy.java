/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.beans;


public class JavaBeanLoaderStrategy implements BeanLoaderStrategy {

	@Override
	public <T> T load(BeanConfig beanConfig, Class<T> clazz) {
		try {
			Class<?> type = ClassLoader.getSystemClassLoader().loadClass(beanConfig.getBean());
			Object newInstance = type.newInstance();
			return clazz.cast(newInstance);
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundError(e);
		} catch (InstantiationException e) {
			throw new InitError(e);
		} catch (IllegalAccessException e) {
			throw new AccessError(e);
		}
	}
	
	public class AccessError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public AccessError(IllegalAccessException e) {
			super(e);
		}
	}
	
	public class InitError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public InitError(InstantiationException e) {
			super(e);
		}
	}
	
	public class ClassNotFoundError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ClassNotFoundError(ClassNotFoundException e) {
			super(e);
		}
	}

}
