package com.iv3d.common.beans;

/**
 * Created by fred on 2017/10/10.
 */
public class DefaultBeanConfig implements BeanConfig {
    private String source;
    private String language;
    private String bean;
    private String path;

    @Override
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
