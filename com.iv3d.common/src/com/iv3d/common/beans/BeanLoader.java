/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.beans;

import java.util.HashMap;
import java.util.Map;

public final class BeanLoader {
	public static final String JYTHON_BEAN = "jython";
	public static final String JAVA_BEAN = "java";
	
	private final Map<String, BeanLoaderStrategy> sourceParsers;
	
	public BeanLoader() {
		sourceParsers = new HashMap<String, BeanLoaderStrategy>();
		sourceParsers.put(JYTHON_BEAN, new JythonBeanLoaderStrategy());
		sourceParsers.put(JAVA_BEAN, new JavaBeanLoaderStrategy());
	}

	public final void addStategy(String langauge, BeanLoaderStrategy loader) {
		sourceParsers.put(langauge, loader);
	}
	
	public final <T> T load(BeanConfig beanConfig, Class<T> clazz) {
	    if(beanConfig==null) {
	        return null;
        }

		String language = beanConfig.getLanguage();
		if(language==null) {
			language = JAVA_BEAN;
		}
		
		BeanLoaderStrategy loader = sourceParsers.get(language);
		if(loader==null) {
			throw new LanguageNotSupportedError(language);
		}
		
		return loader.load(beanConfig, clazz);
	}

	public final class LanguageNotSupportedError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public LanguageNotSupportedError(String language) {
			super(String.format("The language %s is not suppported!", language));
		} 		
	}
}
