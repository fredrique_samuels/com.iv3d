package com.iv3d.common.email;

import javax.mail.Session;
import javax.mail.Store;
import java.util.Properties;

/**
 * Created by fred on 2017/08/02.
 */
public interface MailSettings {
    Properties getProperties();
    Store createStore(Session session);
    String getHost();
}
