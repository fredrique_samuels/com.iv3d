package com.iv3d.common.email;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import java.util.Properties;

/**
 * Created by fred on 2017/08/02.
 */
public class GmailSettings implements MailSettings {
    @Override
    public Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        return props;
    }

    @Override
    public Store createStore(Session session) {
        try {
            return session.getStore("imaps");
        } catch (NoSuchProviderException e) {
            throw new MailStoreCreationError(e);
        }
    }

    @Override
    public String getHost() {
        return "smtp.gmail.com";
    }

}
