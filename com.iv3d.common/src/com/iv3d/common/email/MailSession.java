package com.iv3d.common.email;

import com.iv3d.common.core.Credentials;
import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.function.Function;

/**
 * Created by fred on 2017/08/02.
 */
public final class MailSession {
    private static Logger logger = Logger.getLogger(MailSession.class);

    public static final String MIME_TYPE_HTML = "text/html; charset=utf-8";

    private final Session session;
    private final Credentials credentials;
    private final MailSettings mailSettings;

    public MailSession(Credentials credentials, MailSettings mailSettings) {
        this.credentials = credentials;
        this.mailSettings = mailSettings;
        session = Session.getInstance(mailSettings.getProperties(),
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(credentials.getUser(), credentials.getPassword());
                    }
                });
    }

    public final void processInboxMessages(Function<MailBoxSummary, Void> processFunc) {

        Store store = null;
        Folder emailFolder = null;
        try {
            store = mailSettings.createStore(session);
            store.connect(mailSettings.getHost(), credentials.getUser(), credentials.getPassword());

            emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            int messageCount = emailFolder.getMessageCount();
            int unreadMessageCount = emailFolder.getUnreadMessageCount();
            int newMessageCount = emailFolder.getNewMessageCount();
            Message[] messages = emailFolder.getMessages();

            MailBoxSummary mailBoxSummary = new MailBoxSummaryImpl(messages, messageCount, unreadMessageCount, newMessageCount);
            processFunc.apply(mailBoxSummary);

            emailFolder.close(true);
            store.close();
        } catch (Exception e) {
            throw new EmailMessageProccessingError(e);
        } finally {
            closeMailFolder(emailFolder);
            closeMailStore(store);
        }
    }

    public final void sendTextMessage(String recipient, String subject, String text, String type) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(credentials.getUser()));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public final void sendHtmlMessage(String recipient, String subject, String text) {
        sendMessage(recipient, subject, text, MIME_TYPE_HTML);
    }

    public final void sendHtmlMessage(MailParam mail) {
        sendMessage(mail.getRecipient(), mail.getSubject(), mail.getContent(), mail.getMimeType());
    }

    public final void sendMessage(String recipient, String subject, String text, String type) {
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(credentials.getUser()));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setContent(text, type);
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeMailStore(Store store) {
        if(store!=null) {
            try {
                store.close();
            } catch (MessagingException e) {
                logger.error(ExceptionUtils.getStackTrace(e));
            }
        }
    }

    private void closeMailFolder(Folder emailFolder){
        if(emailFolder!=null) {
            try {
                emailFolder.close(true);
            } catch (MessagingException e) {
                logger.error(ExceptionUtils.getStackTrace(e));
            }
        }
    }

    class MailBoxSummaryImpl implements MailBoxSummary {

        private Message[] messages;
        private final int messageCount;
        private final int unreadMessageCount;
        private final int newMessageCount;

        public MailBoxSummaryImpl(Message[] messages, int messageCount, int unreadMessageCount, int newMessageCount) {
            this.messages = messages;
            this.messageCount = messageCount;
            this.unreadMessageCount = unreadMessageCount;
            this.newMessageCount = newMessageCount;
        }

        @Override
        public int getMessageCount() {
            return messageCount;
        }

        @Override
        public int getUnreadMessageCount() {
            return unreadMessageCount;
        }

        @Override
        public int getNewMessageCount() {
            return newMessageCount;
        }

        @Override
        public Message[] getMessages() {
            return messages;
        }
    }

    private class EmailMessageProccessingError extends RuntimeException {
        public EmailMessageProccessingError(Exception e) {
            super(e);
        }
    }
}
