package com.iv3d.common.email;

import javax.mail.NoSuchProviderException;

/**
 * Created by fred on 2017/08/02.
 */
public class MailStoreCreationError extends RuntimeException {
    public MailStoreCreationError(NoSuchProviderException e) {
        super(e);
    }
}
