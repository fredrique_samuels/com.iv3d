package com.iv3d.common.email;

import javax.mail.Message;

/**
 * Created by fred on 2017/08/02.
 */
public interface MailBoxSummary {
    int getMessageCount();
    int getUnreadMessageCount();
    int getNewMessageCount();
    Message[] getMessages();
}
