package com.iv3d.common.email;

/**
 * Created by fred on 2017/08/03.
 */
public class MailParam {
    private String recipient;
    private String subject;
    private String content;
    private String mimeType;

    public MailParam(String recipient, String subject, String content, String contentFormat) {
        this.recipient = recipient;
        this.subject = subject;
        this.content = content;
        this.mimeType = contentFormat;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getMimeType() {
        return mimeType;
    }
}
