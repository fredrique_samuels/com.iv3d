package com.iv3d.common.date;

/**
 * Created by fred on 2017/07/20.
 */
public class UtcDateParam {

    private final String iso;
    private final long millisecondsSinceEpoc;

    public UtcDateParam(UtcDate date) {
        this.iso = date.toIso8601String();
        this.millisecondsSinceEpoc = date.getMillisecondsSinceEpoc();
    }

    public String getIso() {
        return iso;
    }

    public long getMillisecondsSinceEpoc() {
        return millisecondsSinceEpoc;
    }
}
