package com.iv3d.common.date;

/**
 * Created by fred on 2017/05/15.
 */
public final class UtcSystemTimer {
    private final UtcSystemClock clock;
    private final UtcDate startTime;
    private UtcDate endTime;

    public UtcSystemTimer() {
        this.clock = new UtcSystemClock();
        this.startTime = clock.now();
    }

    public UtcSystemTimer(ClockMilliSecSource milliSecSource) {
        this.clock = new UtcSystemClock(milliSecSource);
        this.startTime = clock.now();
    }

    public final synchronized UtcDuration stop() {
        if(endTime==null)
            endTime = this.clock.now();
        return getDuration();
    }

    public final UtcDate getStartTime() {
        return startTime;
    }

    public final UtcDuration getDuration() {
        if(endTime==null) {
            throw new TimerNotStoppedError();
        }
        return startTime.getDuration(endTime);
    }

    private class TimerNotStoppedError extends RuntimeException {
        public TimerNotStoppedError() {
            super("Cannot retrieve duation on timer that is still running.");
        }
    }
}
