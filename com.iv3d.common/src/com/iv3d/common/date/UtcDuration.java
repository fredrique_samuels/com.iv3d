package com.iv3d.common.date;

import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 * Created by fred on 2017/05/15.
 */
public final class UtcDuration {

    private final UtcSystemDate start;
    private final Duration duration;

    public UtcDuration(UtcSystemDate start, Duration duration) {
        this.start = start;
        this.duration = duration;
    }

    public final long getMillis() {
        return duration.getMillis();
    }

    public final UtcDate getStart() {
        return start;
    }

    public String prettyPrint() {
        Period period = duration.toPeriod();
        PeriodFormatter minutesAndSeconds = new PeriodFormatterBuilder()
                .printZeroAlways()
                .appendDays()
                .appendSeparator(" ")
                .appendHours()
                .appendSeparator(":")
                .appendMinutes()
                .appendSeparator(":")
                .appendSecondsWithOptionalMillis()
                .toFormatter();
        return minutesAndSeconds.print(period);
    }
}
