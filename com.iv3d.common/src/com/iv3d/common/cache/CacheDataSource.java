package com.iv3d.common.cache;

/**
 * Created by fred on 2016/10/30.
 */
public interface CacheDataSource<S, L> {
    L get(S key);
}
