package com.iv3d.common.cache;

import com.iv3d.common.core.Provider;

/**
 * Created by fred on 2017/06/10.
 */
public final class CacheParam<T> {
    private T value;
    private final Provider<T> provider;

    public CacheParam(Provider<T> provider) {
        this.provider = provider;
    }

    public final T get(){
        if(value==null)
            value = provider.get();
        return value;
    }
}
