package com.iv3d.common.cache;

/**
 * Created by fred on 2016/10/30.
 */
public interface DataValidator<L> {
    boolean isValid(L data);
}
