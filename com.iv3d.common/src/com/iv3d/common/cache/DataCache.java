package com.iv3d.common.cache;


import com.iv3d.common.utils.FileUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fred on 2016/10/20.
 */
public class DataCache<S, L> {
    private final Map<S, L> cache;
    private final CacheDataSource<S, L> dataSource;
    private final DataValidator<L> dataValidator;

    public DataCache(CacheDataSource<S, L> dataSource, DataValidator<L> dataValidator) {
        this.dataSource = dataSource;
        this.dataValidator = dataValidator;
        this.cache = new HashMap<S, L>();
    }

    public final L get(S key) {
        if(!cache.containsKey(key)) {
            L data = dataSource.get(key);
            if(dataValidator.isValid(data)) {
                cache.put(key, data);
            }
            return data;
        }
        return cache.get(key);
    }
}
