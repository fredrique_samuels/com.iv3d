package com.iv3d.common;

import com.iv3d.common.core.MapCoords;
import junit.framework.TestCase;

/**
 * Created by fred on 2016/09/13.
 */
public class MapCoordsTest extends TestCase {
    public void testEncode() {
        MapCoords mapCoords = new MapCoords(-2.33, 3.33);
        String encoded = MapCoords.encode(mapCoords);
        assertEquals("mapcoords_lat-2.330_lng3.330", encoded);
    }

    public void testDecode() {
        MapCoords mapCoords = new MapCoords(-2.33, 3.33);
        String encoded = MapCoords.encode(mapCoords);

        MapCoords actual = MapCoords.decode(encoded);
        assertEquals(-2.330, actual.getLatitude(), 0.001);
        assertEquals(3.330, actual.getLongitude(), 0.001);
    }
}