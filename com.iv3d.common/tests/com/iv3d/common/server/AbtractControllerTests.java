/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.server;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

public class AbtractControllerTests extends TestCase {
	
	private AbstractController instance;
	@Mocked
	private PathHandler pathHandler;

	public void testCannotHandleUnSetPath() {
		createInstance();
		assertFalse(instance.canHandlePath("somePath", "GET"));
	}
	
	public void testCanHandleSetPath() {
		createInstance();
		new NonStrictExpectations() {
			{
				pathHandler.getMethod();result = "GET";
			}
		};
		instance.set("somePath", pathHandler);
		assertTrue(instance.canHandlePath("somePath", "GET"));
	}
	
	
//	public void testAnnotatedPathCanHandlePath() {
//		createInstance();
//		assertTrue(instance.canHandlePath("some.path", "GET"));
//	}

	private void createInstance() {
		instance = new AbstractController() {
			
			@Override
			public void configure() {
			}
			
			@Path(value = "some.path")
			public void funcA() {
				
			}
		};
	}
}
