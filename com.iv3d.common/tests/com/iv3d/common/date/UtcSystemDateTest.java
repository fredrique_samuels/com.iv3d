package com.iv3d.common.date;

import junit.framework.TestCase;

public class UtcSystemDateTest extends TestCase {
	
	private UtcDate instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new UtcSystemDate(1262296800000L);
	}
	
	public void testIso8601() {
		assertEquals("2009-12-31T22:00:00+0000", instance.toIso8601String());
	}
	
	public void testGetYear() {
		assertEquals(2009, instance.getYear());
	}
	
	public void testGetMonth() {
		assertEquals(12, instance.getMonth());
	}
	
	public void testGetDay() {
		assertEquals(31, instance.getDay());
	}
	
	public void testGetHour() {
		assertEquals(22, instance.getHour());
	}
	
	public void testGetMinute() {
		instance = new UtcSystemDate(1262297700000L);
		assertEquals(15, instance.getMinute());
	}
	
	public void testGetSecond() {
		instance = new UtcSystemDate(1262297715000L);
		assertEquals(15, instance.getSecond());
	}
}
