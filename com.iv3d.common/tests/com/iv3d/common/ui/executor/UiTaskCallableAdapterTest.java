/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.ui.executor;

import com.iv3d.common.ui.UiTask;
import com.iv3d.common.ui.UiTaskBootstrap;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.VerificationsInOrder;

public class UiTaskCallableAdapterTest extends TestCase {
	
	@Mocked
	private UiTask task;
	@Mocked
	private UiTaskContext context;
	@Mocked 
	UiTaskResult uiResult;
	@Mocked 
	UiTaskBootstrap<UiTask> bootstrap;

	public void testRun() throws Exception {
		new NonStrictExpectations() {
			{
				task.run(context);
				result = uiResult;
				
				uiResult.getMessage();
				result = "message";
			}
		};
		
		UiTaskCallableAdapter<UiTask> adapter = new UiTaskCallableAdapter<UiTask>(task, context, null);
		UiTaskResult result = adapter.call();
		assertEquals("message", result.getMessage());
	}
	
	public void testBootstrap() throws Exception {
		new NonStrictExpectations() {
			{
				task.run(context);
				result = uiResult;
				
				uiResult.getMessage();
				result = "message";
			}
		};
		
		UiTaskCallableAdapter<UiTask> adapter = new UiTaskCallableAdapter<UiTask>(task, context, bootstrap);
		adapter.call();
		
		new VerificationsInOrder() {
			{
				
				bootstrap.bootstrap(task);
				times=1;
				
				task.run(context);
				times=1;
			}
		};
	}

}
