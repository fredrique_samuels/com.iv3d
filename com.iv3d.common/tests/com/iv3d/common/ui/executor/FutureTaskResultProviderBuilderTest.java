/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui.executor;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.iv3d.common.ui.UiTaskResult;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

public class FutureTaskResultProviderBuilderTest extends TestCase {

	private FutureTaskResultProviderBuilder instance;
	@Mocked
	private Future<UiTaskResult> future;
	@Mocked
	UiTaskResult uiResult;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new FutureTaskResultProviderBuilder();
	}
	
	public void testReturnsNullObjectIfNoFutureSupplied() {
		TaskResultProvider value = instance.build();
		assertNotNull(value);
	}
	
	public void testFutureWithNoTimeout() throws InterruptedException, ExecutionException {
		new NonStrictExpectations() {
			{
				future.get();
				result = uiResult;
				
				uiResult.getMessage();
				result = "get()";
			}
		};
		
		TaskResultProvider value = instance.setFuture(future).build();
		UiTaskResult result = value.getResult();

		assertEquals("get()", result.getMessage());
		new Verifications() {
			{
				future.get();
				times=1;
			}
		};
	}
	
	public void testFutureWithTimeout() throws InterruptedException, ExecutionException, TimeoutException {
		final int timeout = 123;
		new NonStrictExpectations() {
			{
				future.get(timeout, TimeUnit.MILLISECONDS);
				result = uiResult;
				
				uiResult.getMessage();
				result = "get(123)";
			}
		};
		
		TaskResultProvider value = instance.setTimeout(timeout).setFuture(future).build();
		UiTaskResult result = value.getResult();

		assertEquals("get(123)", result.getMessage());
		new Verifications() {
			{
				future.get(timeout, TimeUnit.MILLISECONDS);
				times=1;
			}
		};
	}
}
