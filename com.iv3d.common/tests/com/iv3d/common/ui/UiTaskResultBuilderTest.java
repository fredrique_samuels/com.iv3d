/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui;

import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskResultBuilder;

import junit.framework.TestCase;

public class UiTaskResultBuilderTest extends TestCase {
	
	private UiTaskResultBuilder instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance  = new UiTaskResultBuilder();
	}
	
	public void testSetMessage() {
		UiTaskResult result = instance.setMessage("hello").build();
		
		assertEquals("hello", result.getMessage());
		assertFalse(result.hasError());
		assertNull("", result.getException());
		assertNull("", result.getValue());
	}
	
	public void testSetException() {
		Exception exception = new Exception("Error");
		UiTaskResult result = instance.setException(exception).build();
		
		assertEquals("Error", result.getMessage());
		assertTrue(result.hasError());
		assertNotNull(result.getException());
		assertEquals(exception, result.getException());
		assertNull("", result.getValue());
	}
	
	public void testConfigureAsError() { 
		UiTaskResult result = instance.configureAsError("Error").build();
		
		assertEquals("Error", result.getMessage());
		assertTrue(result.hasError());
		assertNotNull(result.getException());
		assertEquals("Error", result.getException().getMessage());
		assertNull("", result.getValue());
	}
	
	public void testSetValue() {
		UiTaskResult result = instance.setValue(new Integer(0)).build();
		
		assertEquals("", result.getMessage());
		assertFalse(result.hasError());
		assertNull(result.getException());
		assertTrue(result.getValue() instanceof Integer);
	}
	
}
