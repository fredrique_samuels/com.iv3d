/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common;

import com.iv3d.common.core.NonNullRules;
import junit.framework.TestCase;

public class NonNullValueRulesTest extends TestCase {
	public void testNullValueRegistration() {
		NonNullRules provider = new NonNullRules();
		try {
			provider.add(null, String.class);
			fail("Expected an exception!");
		} catch (IllegalArgumentException e) {
			assertEquals("Cannot provide null as an alternate value", e.getMessage());
		}
	}
	
	public void testNonRegisteredType() {
		NonNullRules provider = new NonNullRules();
		try {
			provider.get("value", String.class);
		} catch (NonNullRules.TypeNotRegisteredError e){
			assertEquals("No non null value for type java.lang.String not registered.", e.getMessage());
		}	
	}
	
	public void testGetValue() {
		NonNullRules provider = new NonNullRules();
		provider.add("altValue", String.class);
		
		String value = provider.get("value", String.class);
		assertEquals("value", value);
	}
	
	public void testGetAlternateValue() {
		NonNullRules provider = new NonNullRules();
		provider.add("altValue", String.class);
		String value = provider.get(null, String.class);
		assertEquals("altValue", value);
	}
}
