/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.pagination;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

public class PaginatedDataSourceTest extends TestCase {
	@Mocked
	DataGenerator<Integer> dataGenerator;
	
	public void testPageCountCalculation() {
		new NonStrictExpectations() {
			{
				dataGenerator.totalRecords();
				result = 68L;
			}
		};
		
		int maxPageSize = 10;
		PaginatedDataSource<Integer> dataSource = new PaginatedDataSource<Integer>(maxPageSize, dataGenerator) ;
		
		PagedResult<Integer> pagedResult = dataSource.get(0);
		
		assertEquals(7, pagedResult.totalPages());
		
		new Verifications() {
			{
				dataGenerator.totalRecords();
				times=1;
			}
		};
	}
	
	public void testPageGet() {
		//given
		int totalRecords = 7;
		int maxPageSize = 3;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(1);
			//then
			assertEquals(maxPageSize, pagedResult.totalPages());
			assertEquals(1, pagedResult.currentPage());
			assertFalse(pagedResult.hasPrev());
			assertTrue(pagedResult.hasNext());
		}
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(2);
			//then
			assertEquals(maxPageSize, pagedResult.totalPages());
			assertEquals(2, pagedResult.currentPage());
			assertTrue(pagedResult.hasPrev());
			assertTrue(pagedResult.hasNext());
		}
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(maxPageSize);
			//then
			assertEquals(maxPageSize, pagedResult.totalPages());
			assertEquals(maxPageSize, pagedResult.currentPage());
			assertTrue(pagedResult.hasPrev());
			assertFalse(pagedResult.hasNext());
		}
	}
	
	public void testNegativePageNumber() {
		//given
		int totalRecords = 1;
		int maxPageSize = 1;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(-totalRecords);
			//then
			assertEquals(totalRecords, pagedResult.currentPage());
		}
	}
	
	public void testZeroPageNumber() {
		//given
		int totalRecords = 1;
		int maxPageSize = 1;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(0);
			//then
			assertEquals(1, pagedResult.currentPage());
		}
	}
	
	public void testLargePageNumber() {
		//given
		PaginatedDataSource<Long> dataSource = createDemoDataSource(2, 1);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(6);
			//then
			assertEquals(2, pagedResult.currentPage());
		}
	}
	
	public void testFirstPagePrevReturnsFirstPage() {
		//given
		int totalRecords = 3;
		int maxPageSize = 1;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(maxPageSize).prev();
			//then
			assertEquals(maxPageSize, pagedResult.currentPage());
		}
	}
	
	public void testNextPage() {
		//given
		int maxPageSize = 1;
		int totalRecords = 3;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(maxPageSize).next();
			//then
			assertEquals(2, pagedResult.currentPage());
		}
	}
	
	public void testPrevPage() {
		//given
		int totalRecords = 3;
		int maxPageSize = 1;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(2).prev();
			//then
			assertEquals(maxPageSize, pagedResult.currentPage());
		}
	}
	
	public void testLastPageNextIsLastPage() {
		//given
		PaginatedDataSource<Long> dataSource = createDemoDataSource(3, 1);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(3).next();
			//then
			assertEquals(3, pagedResult.currentPage());
		}
	}
	
	public void testDataGeneratorParams() {
		//given
		int totalRecords = 7;
		int maxPageSize = 3;
		PaginatedDataSource<Long> dataSource = createDemoDataSource(totalRecords, maxPageSize);
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(1);
			Long[] data = pagedResult.data();
			//then
			assertEquals(0L, data[0].longValue());
			assertEquals(3L, data[1].longValue());
			assertEquals(1, pagedResult.currentPage());
		}
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(2);
			Long[] data = pagedResult.data();
			//then
			assertEquals(3L, data[0].longValue());
			assertEquals(3L, data[1].longValue());
			assertEquals(2, pagedResult.currentPage());
		}
		
		{
			//when
			PagedResult<Long> pagedResult = dataSource.get(maxPageSize);
			Long[] data = pagedResult.data();
			//then
			assertEquals(6L, data[0].longValue());
			assertEquals(3L, data[1].longValue());
			assertEquals(maxPageSize, pagedResult.currentPage());
		}
	}

	private PaginatedDataSource<Long> createDemoDataSource(final int totalRecords, 
			final int maxPageSize) {
		DataGenerator<Long> g = new DataGenerator<Long>() {
			
			@Override
			public long totalRecords() {
				return totalRecords;
			}
			
			@Override
			public Long[] generate(long offset, long maxReords) {
				return new Long[]{offset, maxReords};
			}
		};
		
		PaginatedDataSource<Long> dataSource = new PaginatedDataSource<Long>(maxPageSize, g) ;
		return dataSource;
	}
	
}
