/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.mvc.control.service;

import java.util.Arrays;
import java.util.List;

import com.iv3d.common.core.NonNullRules;
import com.iv3d.common.mvc.control.CommandExecutor;
import com.iv3d.common.mvc.control.CommandType;
import com.iv3d.common.mvc.control.CommandView;
import com.iv3d.common.mvc.control.CommandViewBuilder;
import com.iv3d.common.mvc.control.DisplayType;
import com.iv3d.common.mvc.control.DisplayView;
import com.iv3d.common.mvc.control.DisplayViewBuilder;
import com.iv3d.common.mvc.control.FlowView;
import com.iv3d.common.mvc.control.FlowViewBuilder;
import com.iv3d.common.mvc.control.FormView;
import com.iv3d.common.mvc.control.FormViewBuilder;
import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.InputView;
import com.iv3d.common.mvc.control.InputViewBuilder;
import com.iv3d.common.mvc.control.ViewController;
import com.iv3d.common.mvc.control.config.CommandConfig;
import com.iv3d.common.mvc.control.config.CommandParam;
import com.iv3d.common.mvc.control.config.DisplayConfig;
import com.iv3d.common.mvc.control.config.FlowConfig;
import com.iv3d.common.mvc.control.config.FormConfig;
import com.iv3d.common.mvc.control.config.InputConfig;
import com.iv3d.common.mvc.control.config.ViewConfig;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;
import mockit.VerificationsInOrder;


public class DefaultViewControlServiceTests extends TestCase {
	private DefaultViewControlService<Object, FlowView, FormView, InputView, DisplayView, CommandView> service;
	private NonNullRules nonNullRules;
	@Mocked
	ViewConfig viewConfig;
	@Mocked
	private ViewController controller;
	private CommandExecutor commandExecutor;
	@Mocked
	private ViewBuilderFactory<Object, FlowView, FormView, InputView, DisplayView, CommandView> viewBuilderFactory;
	@Mocked
	private FlowView flowView;
	@Mocked
	private FlowConfig flowConfig;
	@Mocked 
	private FlowViewBuilder<Object, FlowView, FormView, InputView, DisplayView> flowViewBuilder;
	@Mocked
	private InputConfig inputConfig;
	@Mocked
	private InputViewBuilder<Object, InputView> inputViewBuilder;
	@Mocked
	private InputView inputView;
	@Mocked
	private DisplayConfig displayConfig;
	@Mocked
	private DisplayViewBuilder<Object, DisplayView> displayViewBuilder;
	@Mocked
	private DisplayView displayView;
	@Mocked
	private FormViewBuilder<Object, FormView, InputView, CommandView> formViewBuilder;
	@Mocked
	private FormView formView;
	@Mocked
	private FormConfig formConfig;
	@Mocked
	private CommandViewBuilder<Object, CommandView> commandBuilder;
	@Mocked
	private CommandView command;
	@Mocked
	private CommandConfig commandConfig;
	@Mocked
	protected CommandParam commandParam;
	@Mocked
	protected CommandParam inputViewParam;
	protected Object context;
	
	public void setUp() {
		nonNullRules = new NonNullRules();
		nonNullRules.add("", String.class);
		nonNullRules.add(InputType.TEXT, InputType.class);
		nonNullRules.add(DisplayType.TEXT, DisplayType.class);
		
		service = new DefaultViewControlService<>(nonNullRules, controller);
		service.setViewBuilderFactory(viewBuilderFactory);
		new NonStrictExpectations() {
			{
				controller.getView(anyString);
				result = viewConfig;
			}
		};
		commandExecutor = controller;
	}
	
	public void testFlowViewBuilder_NameAndHintSet() {
		setupFlowBuilderNameAndHint("flowName", "flowType");
		execute();
		assertFlowBuilderNameAndHint("flowName", "flowType");
	}

	public void execute() {
		service.run("main");
	}
	
	public void testFlowViewBuilder_NameAndHintNeverNull() {
		setupFlowBuilderNameAndHint(null, null);
		execute();
		assertFlowBuilderNameAndHint("", "");
	}
	
	public void testFlowViewBuilder_InputsNeverNull() {
		setupFlowBuilderNullInputs();
		execute();
		assertFlowBuilderInputsEmpty();
	}
	
	public void testFlowViewBuilder_InputsSet() {
		setupFlowBuilderInputs();
		execute();
		assertFlowBuilderInputsSet();
	}
	
	public void testFlowViewBuilder_DisplaysNeverNull() {
		setupFlowBuilderNullDisplays();
		execute();
		assertFlowBuilderDisplaysEmpty();
	}
	
	public void testFlowViewBuilder_DisplaysSet() {
		setupFlowBuilderDisplays();
		execute();
		assertFlowBuilderDisplaysSet();
	}
	
	public void testFlowViewBuilderForms_NeverNull() {
		setupFlowBuilderNullForms();
		execute();
		assertFlowBuilderFormsEmpty();
	}
	
	public void testFlowViewBuilder_FormsSet() {
		setupFlowBuilderForms();
		execute();
		assertFlowBuilderFormsSet();
	}
	
	public void testInputViewBuilder_NameAndIdNeverNull() {
		setupFlowBuilderInputsWithNameAndId(null, null);
		execute();
		assertInputBuilderNameAndId("", "");
	}
	
	public void testInputViewBuilder_NameAndIdSet() {
		setupFlowBuilderInputsWithNameAndId("inputId", "inputName");
		execute();
		assertInputBuilderNameAndId("inputId", "inputName");
	}
	
	public void testInputViewBuilder_DefaultBuildTypeIsText() { 
		setupFlowBuilderInputsWithNullType();
		execute();
		assertInputBuilderTextCreated();
	}
	
	public void testInputViewBuilder_BuildTextType() { 
		setupFlowBuilderInputsWithTextType();
		execute();
		assertInputBuilderTextCreated();
	}
	
	public void testInputViewBuilder_BuildParagraphType() { 
		setupFlowBuilderInputsWithParagraphType();
		execute();
		assertInputBuilderParagraphCreated();
	}

	public void testInputViewBuilder_BuildPasswordType() { 
		setupFlowBuilderInputsWithPasswordType();
		execute();
		assertInputBuilderPasswordCreated();
	}
	
	public void testDisplayViewBuilder_NameAndIdNeverNull() {
		setupFlowBuilderDisplaysWithNameAndId((String)null, (String)null);
		execute();
		assertDisplayBuilderNameAndId("", "");
	}
	
	public void testDisplayViewBuilder_NameAndIdSet() {
		setupFlowBuilderDisplaysWithNameAndId("displayId", "displayName");
		execute();
		assertDisplayBuilderNameAndId("displayId", "displayName");
	}
	
	public void testDisplayViewBuilder_TypeDefaultsToText() {
		setupFlowDisplayWithNullType();
		execute();
		assertDisplayBuilderTextCreated("");
	}
	
	public void testDisplayViewBuilder_BuildTextWithNullValue() {
		setupFlowDisplayWithTextType(null);
		execute();
		assertDisplayBuilderTextCreated("");
	}
	
	public void testDisplayViewBuilder_BuildText() {
		setupFlowDisplayWithTextType("textDisplay");
		execute();
		assertDisplayBuilderTextCreated("textDisplay");
	}
	
	public void testDisplayViewBuilder_BuildParagraphWithNullValue() {
		setupFlowDisplayWithParagraphType(null);
		execute();
		assertDisplayBuilderParagraphCreated("");
	}
	
	public void testDisplayViewBuilder_BuildParagraph() {
		setupFlowDisplayWithParagraphType("paragraphValue");
		execute();
		assertDisplayBuilderParagraphCreated("paragraphValue");
	}
	
	public void testFormViewBuilder_NameAndIdNeverNull() {
		setupFlowFormWithIdAndName((String)null, (String)null);
		execute();
		assertFormIdAndName("", "");
	}
		
	public void testFormViewBuilder_NameAndIdSet() {
		setupFlowFormWithIdAndName("formId", "formName");
		execute();
		assertFormIdAndName("formId", "formName");
	}
	
	public void testFormViewBuilder_InputsNeverNull() {
		setupFlowFormWithNullInputs();
		execute();
		assertFormInputsEmpty();
	}
	
	public void testFormViewBuilder_InputsSet() {
		setupFlowFormWithInputs();
		execute();
		assertFormInputsSet();
	}
	
	public void testFormViewBuilder_CommandsNeverNull() {
		seutpFlowFormWithNullCommands();
		execute();
		assertFormCommandsEmpty();
	}

	public void testFormViewBuilder_CommandsSet() {
		setupFlowFormWithCommands();
		execute();
		assertFormCommandsSet();
	}
	
	public void testCommandBuilder_ExecutorPassed() {
		setupFlowFormWithCommands();
		execute();
		assertCommandExecutor();
	}
	
	public void testCommandBuilder_IdAndNameNeverNull() {
		seutpFlowFormWithCommandsWithIdAndName((String)null, (String)null);
		execute();
		assertCommandBuilderIdAndName("", "");
	}
		
	public void testCommandBuilder_IdAndNameSet() {
		seutpFlowFormWithCommandsWithIdAndName("commandId", "commandName");
		execute();
		assertCommandBuilderIdAndName("commandId", "commandName");
	}
	
	public void testCommandBuilder_InputParamsNeverNull() {
		seutpFlowFormWithCommandsWithNullInputParams();
		execute();
		assertCommandBuilderInputParamsEmpty();
	}
	
	public void testCommandBuilder_InputParamsSet() {
		setupFlowFormWithCommandsWithInputParams();
		execute();
		assertCommandBuilderCommandParams();
	}
	
	public void testCommandBuilder_FormInputParamsSetForFormSubmitType() {
		setupFlowFormWithInputsAndFormCommands();
		execute();
		assertCommandBuilderInputViewCommandParams();
	}
	
	public void testCommandBuilder_FormInputParamsNotSetNonFOrmSubmitCommand() {
		setupFlowFormWithNonFormCommandsAndInput();
		execute();
		assertCommandBuilderInputParamsEmpty();
	}
	
	public void testCommandBuilder_FormInputParamsAndCommandParamsSet() {
		setupFlowFormFormCommandsAndInputParams();
		execute();
		assertCommandBuilderAllParamsAreSet();
	}
	
	private void assertCommandBuilderAllParamsAreSet() {
		new VerificationsInOrder() {
			{
				List<CommandParam> actual;
				
				commandBuilder.setInputParams(actual=withCapture());
				times=1;
				commandBuilder.build(null, commandExecutor);
				times=1;
				
				assertEquals(2, actual.size());
				assertEquals(commandParam, actual.get(0));		
				assertEquals(inputViewParam, actual.get(1));				
			}
		};
	}

	private void setupFlowFormFormCommandsAndInputParams() {
		setupFlowFormWithInputsAndFormCommands();
		new NonStrictExpectations() {
			{
				commandConfig.getParams();
				result = commandParam;
			}
		};
	}

	private void setupFlowFormWithNonFormCommandsAndInput() {
		setupFlowFormWithCommands();
		setupFlowFormWithInputs();
	}

	private void assertCommandBuilderInputViewCommandParams() {
		new VerificationsInOrder() {
			{
				List<CommandParam> actual;
				
				commandBuilder.setInputParams(actual=withCapture());
				times=1;
				commandBuilder.build(null, commandExecutor);
				times=1;
				
				assertEquals(1, actual.size());
				assertEquals(inputViewParam, actual.get(0));				
			}
		};
	}

	private void setupFlowFormWithInputsAndFormCommands() {
		setupFlowFormWithCommands();
		setupFlowFormWithInputs();
		new NonStrictExpectations() {
			{
				inputView.getParam();
				result = inputViewParam;
			}
		};
		new NonStrictExpectations() {
			{
				commandConfig.getType();
				result = CommandType.FORM_SUBMIT;
			}
		};
	}

	private void assertCommandBuilderCommandParams() {
		new VerificationsInOrder() {
			{
				List<CommandParam> actual;
				
				commandBuilder.setInputParams(actual=withCapture());
				times=1;
				commandBuilder.build(null, commandExecutor);
				times=1;
				
				assertEquals(1, actual.size());
				assertEquals(commandParam, actual.get(0));				
			}
		};
	}

	private void setupFlowFormWithCommandsWithInputParams() {
		setupFlowFormWithCommands();
		new NonStrictExpectations() {
			{
				commandConfig.getParams();
				result = Arrays.asList(commandParam);
			}
		};
	}

	private void assertCommandBuilderInputParamsEmpty() {
		new VerificationsInOrder() {
			{
				List<CommandParam> actual;
				
				commandBuilder.setInputParams(actual=withCapture());
				times=1;
				assertTrue(actual.isEmpty());
			}
		};
	}

	private void seutpFlowFormWithCommandsWithNullInputParams() {
		setupFlowFormWithCommands();
		new NonStrictExpectations() {
			{
				commandConfig.getParams();
				result = null;
			}
		};
	}

	private void assertCommandBuilderIdAndName(String id, String name) {
		new VerificationsInOrder() {
			{
				String actualId;
				String actualName;
				
				commandBuilder.setId(actualId=withCapture());
				times=1;
				commandBuilder.setName(actualName=withCapture());
				times=1;
				commandBuilder.build(null, commandExecutor);
				times=1;
				
				assertEquals(name, actualName);
				assertEquals(id, actualId);
			}
		};
	}

	private void seutpFlowFormWithCommandsWithIdAndName(String id, String name) {
		setupFlowFormWithCommands();
		new NonStrictExpectations() {
			{
				commandConfig.getId();
				result = id;
				commandConfig.getName();
				result = name;
			}
		};
	}

	private void assertFormCommandsSet() {
		new VerificationsInOrder() {
			{
				List<CommandView> actual;
				
				formViewBuilder.setCommands(actual=withCapture());
				times=1;
				formViewBuilder.build(context);
				times=1;
				
				assertEquals(command, actual.get(0));
			}
		};
	}

	private void assertCommandExecutor() {
		new VerificationsInOrder() {
			{
				CommandExecutor actual;	
				
				commandBuilder.build(null, actual=withCapture());
				times=1;
				
				assertEquals(actual, commandExecutor);
			}
		};
	}

	private void setupFlowFormWithCommands() {
		setupFlowBuilderForms();
		setupFormCommandBuilder();
		new NonStrictExpectations() {
			{
				formConfig.getFormCommands();
				result = Arrays.asList(commandConfig);
			}
		};
	}

	private void assertFormCommandsEmpty() {
		new VerificationsInOrder() {
			{
				List<CommandView> actual;
				
				formViewBuilder.setCommands(actual=withCapture());
				times=1;
				formViewBuilder.build(context);
				times=1;
				
				assertTrue(actual.isEmpty());
			}
		};
	}

	private void seutpFlowFormWithNullCommands() {
		setupFlowBuilderForms();
		setupFormCommandBuilder();
		new NonStrictExpectations() {
			{
				formConfig.getFormCommands();
				result = null;
			}
		};
	}

	private void setupFormCommandBuilder() {
		new NonStrictExpectations() {
			{
				viewBuilderFactory.createCommandBuilder();
				result = commandBuilder;
				
				commandBuilder.build(null, controller);
				result = command;
			}
		};
	}

	private void assertFormInputsSet() {
		new VerificationsInOrder() {
			{
				List<InputView> actual;
				
				formViewBuilder.setInputs(actual=withCapture());
				times=1;
				formViewBuilder.build(context);
				times=1;
				
				assertEquals(1, actual.size());
				assertEquals(inputView, actual.get(0));
			}
		};
	}

	private void setupFlowFormWithInputs() {
		setupFlowBuilderForms();
		setupInputBuilder();
		new NonStrictExpectations() {
			{
				formConfig.getInputs();
				result = Arrays.asList(inputConfig);
			}
		};
	}

	private void assertFormInputsEmpty() {
		new VerificationsInOrder() {
			{
				List<InputView> actual;
				
				formViewBuilder.setInputs(actual=withCapture());
				times=1;
				formViewBuilder.build(context);
				times=1;
				
				assertTrue(actual.isEmpty());
			}
		};
	}

	private void setupFlowFormWithNullInputs() {
		setupFlowBuilderForms();
		new NonStrictExpectations() {
			{
				formConfig.getInputs();
				result = null;
			}
		};
	}

	private void assertFormIdAndName(String id, String name) {
		new VerificationsInOrder() {
			{
				String actualId;
				String actualName;
				
				formViewBuilder.setId(actualId=withCapture());
				times=1;
				formViewBuilder.setName(actualName=withCapture());
				times=1;
				formViewBuilder.build(context);
				times=1;
				
				assertEquals(name, actualName);
				assertEquals(id, actualId);
			}
		};
	}

	private void setupFlowFormWithIdAndName(String id, String name) {
		setupFlowBuilderForms();
		new NonStrictExpectations() {
			{
				formConfig.getId();
				result = id;
				formConfig.getName();
				result = name;
			}
		};
	}

	private void assertDisplayBuilderParagraphCreated(String value) {
		new VerificationsInOrder() {
			{
				displayViewBuilder.buildParagraph(context, value);
				times=1;
			}
		};
	}

	private void setupFlowDisplayWithParagraphType(String value) {
		setupFlowBuilderDisplays();
		new NonStrictExpectations() {
			{
				displayConfig.getType();
				result = DisplayType.PARAGRAPH;
				displayConfig.getStringValue();
				result = value;
			}
		};
	}

	private void setupFlowDisplayWithTextType(String value) {
		setupFlowBuilderDisplays();
		new NonStrictExpectations() {
			{
				displayConfig.getType();
				result = DisplayType.TEXT;
				displayConfig.getStringValue();
				result = value;
			}
		};
	}

	private void assertDisplayBuilderTextCreated(String value) {
		new VerificationsInOrder() {
			{
				displayViewBuilder.buildText(context, value);
				times=1;
			}
		};
	}

	private void setupFlowDisplayWithNullType() {
		setupFlowBuilderDisplays();
		new NonStrictExpectations() {
			{
				displayConfig.getType();
				result = null;
			}
		};
	}

	private void assertDisplayBuilderNameAndId(String id, String name) {
		new VerificationsInOrder() {
			{
				String actualId;
				String actualName;
				
				displayViewBuilder.setId(actualId=withCapture());
				times=1;
				displayViewBuilder.setName(actualName=withCapture());
				times=1;
				displayViewBuilder.buildText(context, anyString);
				times=1;
				
				assertEquals(name, actualName);
				assertEquals(id, actualId);
			}
		};
	}

	private void setupFlowBuilderDisplaysWithNameAndId(String id, String name) {
		setupFlowBuilderDisplays();
		new NonStrictExpectations() {
			{
				displayConfig.getName();
				result = name;
				displayConfig.getId();
				result = id;
			}
		};
	}

	private void assertInputBuilderPasswordCreated() {
		new Verifications() {
			{
				inputViewBuilder.buildPassword(context);
				times=1;
			}
		};
	}

	private void setupFlowBuilderInputsWithPasswordType() {
		setupFlowBuilderInputs();
		setupInputType(InputType.PASSWORD);
	}

	private void assertInputBuilderParagraphCreated() {
		new Verifications() {
			{
				inputViewBuilder.buildParagraph(context);
				times=1;
			}
		};
	}

	private void setupFlowBuilderInputsWithParagraphType() {
		setupFlowBuilderInputs();
		setupInputType(InputType.PARAGRAPH);
	}

	private void setupFlowBuilderInputsWithTextType() {
		setupFlowBuilderInputs();
		setupInputType(InputType.TEXT);
	}

	private void assertInputBuilderTextCreated() {
		new Verifications() {
			{
				inputViewBuilder.buildText(context);
				times=1;
			}
		};
	}

	private void setupFlowBuilderInputsWithNullType() {
		setupFlowBuilderInputs();
		setupInputType(null);

		new NonStrictExpectations() {
			{
				
				inputViewBuilder.buildText(context);
				result = inputView;
			}
		};
	}

	private void setupInputType(InputType type) {
		new NonStrictExpectations() {
			{
				inputConfig.getType();
				result = type;
			}
		};
	}

	private void assertInputBuilderNameAndId(String id, String name) {
		new VerificationsInOrder() {
			{
				String actualId;
				String actualName;
				
				inputViewBuilder.setId(actualId=withCapture());
				times=1;
				inputViewBuilder.setName(actualName=withCapture());
				times=1;
				inputViewBuilder.buildText(context);
				times=1;
				
				assertEquals(name, actualName);
				assertEquals(id, actualId);
			}
		};
	}

	private void setupFlowBuilderInputsWithNameAndId(String id, String name) {
		setupFlowBuilderInputs();
		new NonStrictExpectations() {
			{
				inputConfig.getName();
				result = name;
				inputConfig.getId();
				result = id;
			}
		};
	}

	private void assertFlowBuilderFormsSet() {
		new VerificationsInOrder() {
			{
				List<FormView> actual;
				
				flowViewBuilder.setForms(actual=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertEquals(1, actual.size());
				assertEquals(formView, actual.get(0));
			}
		};
	}

	private void setupFlowBuilderForms() {
		setupFlowBuilder();
		setupFormBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getForms();
				result = Arrays.asList(formConfig);
			}
		};
	}
	
	private void setupFormBuilder() {
		new NonStrictExpectations() {
			{	
				viewBuilderFactory.createFormViewBuilder();
				result = formViewBuilder;
				
				formViewBuilder.build(context);
				result = formView;
			}
		};
	}

	private void assertFlowBuilderFormsEmpty() {
		new VerificationsInOrder() {
			{
				List<FormView> actual;
				
				flowViewBuilder.setForms(actual=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertTrue(actual.isEmpty());
			}
		};
	}

	private void setupFlowBuilderNullForms() {
		setupFlowBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getForms();
				result = null;
			}
		};
	}

	private void assertFlowBuilderDisplaysSet() {
		new VerificationsInOrder() {
			{
				List<DisplayView> actual;
				
				flowViewBuilder.setDisplays(actual=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertEquals(1, actual.size());
				assertEquals(displayView, actual.get(0));
			}
		};
	}

	private void setupFlowBuilderDisplays() {
		setupFlowBuilder();
		setupDisplayBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getDisplays();
				result = Arrays.asList(displayConfig);
			}
		};
	}

	private void setupDisplayBuilder() {
		new NonStrictExpectations() {
			{	
				viewBuilderFactory.createDisplayViewBuilder();
				result = displayViewBuilder;
				
				displayViewBuilder.buildText(context, anyString);
				result = displayView;
			}
		};
	}

	private void assertFlowBuilderDisplaysEmpty() {
		new VerificationsInOrder() {
			{
				List<DisplayView> actual;
				
				flowViewBuilder.setDisplays(actual=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertTrue(actual.isEmpty());
			}
		};
	}

	private void setupFlowBuilderNullDisplays() {
		setupFlowBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getDisplays();
				result = null;
			}
		};
	}

	private void assertFlowBuilderInputsSet() {
		new VerificationsInOrder() {
			{
				List<InputView> actual;
				
				flowViewBuilder.setInputs(actual=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertEquals(1, actual.size());
				assertEquals(inputView, actual.get(0));
			}
		};
	}

	private void setupFlowBuilderInputs() {
		setupFlowBuilder();
		setupInputBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getInputs();
				result = Arrays.asList(inputConfig);
			}
		};
	}

	private void setupInputBuilder() {
		new NonStrictExpectations() {
			{	
				viewBuilderFactory.createInputViewBuilder();
				result = inputViewBuilder;
				
				inputViewBuilder.buildText(context);
				result = inputView;
			}
		};
	}

	private void assertFlowBuilderInputsEmpty() {
		new VerificationsInOrder() {
			{
				List<InputView> actual;
				
				flowViewBuilder.setInputs(actual=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertTrue(actual.isEmpty());
			}
		};
	}

	private void setupFlowBuilderNullInputs() {
		setupFlowBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getInputs();
				result = null;
			}
		};
	}

	private void assertFlowBuilderNameAndHint(String name, String hint) {
		new VerificationsInOrder() {
			{
				String actualName;
				String actualHint;
				
				flowViewBuilder.setName(actualName=withCapture());
				times=1;
				flowViewBuilder.setHint(actualHint=withCapture());
				times=1;
				flowViewBuilder.build(null);
				times=1;
				
				assertEquals(name,  actualName);
				assertEquals(hint,  actualHint);
			}
		};
	}


	private void setupFlowBuilderNameAndHint(String name, String hint) {
		setupFlowBuilder();
		new NonStrictExpectations() {
			{
				flowConfig.getName();
				result = name;
				flowConfig.getHint();
				result = hint;
			}
		};
	}

	private void setupFlowBuilder() {
		new NonStrictExpectations() {
			{
				viewConfig.getFlows();
				result = Arrays.asList(flowConfig);
				
				viewBuilderFactory.createFlowViewBuilder();
				result = flowViewBuilder;
				
				flowViewBuilder.build(null);
				result = flowView;
			}
		};
	}
}
