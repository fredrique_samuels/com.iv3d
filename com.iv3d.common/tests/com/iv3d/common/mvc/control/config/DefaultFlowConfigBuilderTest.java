/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.config.DefaultFlowConfigBuilder;
import com.iv3d.common.mvc.control.config.DisplayConfigBuilder;
import com.iv3d.common.mvc.control.config.FlowConfig;
import com.iv3d.common.mvc.control.config.FlowConfigBuilder;
import com.iv3d.common.mvc.control.config.FormConfigBuilder;
import com.iv3d.common.mvc.control.config.InputConfigBuilder;

import junit.framework.TestCase;

public class DefaultFlowConfigBuilderTest extends TestCase {
	private FlowConfigBuilder instance;

@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new DefaultFlowConfigBuilder();
	}
	public void testNameDefault() {
		FlowConfig config = instance.build();
		assertEquals("", config.getName());
	}
	
	public void testSetNameDefault() {
		instance.setName("name");
		FlowConfig config = instance.build();
		
		assertEquals("name", config.getName());
	}
	
	public void testHintDefault() {
		FlowConfig config = instance.build();
		assertEquals("", config.getHint());
	}
	
	public void testSetHint() {
		instance.setHint("hint");
		FlowConfig config = instance.build();
		assertEquals("hint", config.getHint());
	}
	
	public void testInputsDefault() {
		FlowConfig config = instance.build();
		assertEquals(0, config.getInputs().size());
	}
	
	public void testCreateInput() {
		InputConfigBuilder inputBuilder = instance.createInput("", InputType.TEXT);
		assertNotNull(inputBuilder);
		
		FlowConfig config = instance.build();
		assertEquals(1, config.getInputs().size());
	}
	
	public void testDisplaysDefault() {
		FlowConfig config = instance.build();
		assertEquals(0, config.getDisplays().size());
	}
	
	public void testCreateDisplay() {
		DisplayConfigBuilder displayBuilder = instance.createDisplay();
		assertNotNull(displayBuilder);
		
		FlowConfig config = instance.build();
		assertEquals(1, config.getDisplays().size());
	}
	
	public void testFormsDefault() {
		FlowConfig config = instance.build();
		assertEquals(0, config.getForms().size());
	}
	
	public void testCreateForm() {
		String id = "";
		FormConfigBuilder builder = instance.createForm(id);
		assertNotNull(builder);
		
		FlowConfig config = instance.build();
		assertEquals(1, config.getForms().size());
	}
	
}
