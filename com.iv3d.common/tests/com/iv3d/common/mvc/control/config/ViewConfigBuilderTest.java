/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.List;

import com.iv3d.common.mvc.control.config.FlowConfig;
import com.iv3d.common.mvc.control.config.FlowConfigBuilder;
import com.iv3d.common.mvc.control.config.ViewConfig;
import com.iv3d.common.mvc.control.config.ViewConfigBuilder;

import junit.framework.TestCase;

public class ViewConfigBuilderTest extends TestCase {
	
	private ViewConfigBuilder instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		instance = new ViewConfigBuilder();
	}
	
	public void testBuild() {
		ViewConfig config = instance.build();
		assertNotNull(config);
	}
	
	public void testDefaultFlows() {
		ViewConfig config = instance.build();
		List<FlowConfig> flows = config.getFlows();
		assertEquals(0, flows.size());
	}
	
	public void testCreateFlow() {
		FlowConfigBuilder builder = instance.createFlow();
		assertNotNull(builder);
		
		ViewConfig config = instance.build();
		List<FlowConfig> flows = config.getFlows();
		assertEquals(1, flows.size());
	}
	
}
