/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.List;

import com.iv3d.common.mvc.control.CommandType;
import com.iv3d.common.mvc.control.config.CommandConfig;
import com.iv3d.common.mvc.control.config.CommandConfigBuilder;
import com.iv3d.common.mvc.control.config.CommandParam;
import com.iv3d.common.mvc.control.config.DefaultCommandConfigBuilder;

import junit.framework.TestCase;


public class DefaultCommandConfigBuilderTest extends TestCase {
	private CommandConfigBuilder instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setupInstance("");
	}
	
	public void testIdSet() {
		setupInstance("commandId");
		CommandConfig config  = instance.build();
		assertEquals("commandId", config.getId());
	}
	
	public void testDefaultName() {
		CommandConfig config  = instance.build();
		assertEquals("", config.getName());
	}
	
	public void testNameSet() {
		instance.setName("commandName");
		CommandConfig config  = instance.build();
		assertEquals("commandName", config.getName());
	}
	
	public void testDefaultInputParams(){
		CommandConfig config = instance.build();
		assertEquals(0, config.getParams().size());
	}
	
	public void testAddInputParams(){
		instance.addParam("paramName", "paramValue");
		CommandConfig config = instance.build();
		assertParamValue(config);
	}
	
	public void testTypeDefaultToType(){
		CommandConfig config  = instance.build();
		assertEquals(CommandType.DEFAULT, config.getType());
	}
	
	public void testFormSubmitTypeSet() {
		instance.asFormSubmit();
		CommandConfig config  = instance.build();
		assertEquals(CommandType.FORM_SUBMIT, config.getType());
	}

	public void assertParamValue(CommandConfig config) {
		List<CommandParam> inputParams = config.getParams();
		assertEquals(1, inputParams.size());
		assertEquals("paramName", inputParams.get(0).id());
		assertEquals("paramValue", inputParams.get(0).value());
	}
	
	private void setupInstance(String id) {
		instance  = new DefaultCommandConfigBuilder(id);
	}
	
}
