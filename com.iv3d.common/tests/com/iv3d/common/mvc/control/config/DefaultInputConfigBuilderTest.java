/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.config.DefaultInputConfigBuilder;
import com.iv3d.common.mvc.control.config.InputConfig;
import com.iv3d.common.mvc.control.config.InputConfigBuilder;

import junit.framework.TestCase;

public class DefaultInputConfigBuilderTest extends TestCase {
	private InputConfigBuilder instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setupInstance(InputType.TEXT, "");
	}
	
	public void testConstructor() {
		setupInstance(InputType.TEXT, "someId");
		
		InputConfig config  = instance.build();
		assertEquals("someId", config.getId());
		assertEquals(InputType.TEXT, config.getType());
	}
	
	public void testConstructorWithMoreValues() {
		setupInstance(InputType.PARAGRAPH, "myId");
		
		InputConfig config  = instance.build();
		assertEquals("myId", config.getId());
		assertEquals(InputType.PARAGRAPH, config.getType());
	}
	
	public void testDefaultName() {
		InputConfig config = instance.build();
		assertEquals("", config.getName());
	}
	
	public void testNameSet() {
		instance.setName("testName");
		
		InputConfig config  = instance.build();
		assertEquals("testName", config.getName());
		
	}
	
	private void setupInstance(InputType type, String id) {
		instance = new DefaultInputConfigBuilder(id, type);
	}
}
