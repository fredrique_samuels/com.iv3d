/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import java.util.List;

import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.config.CommandConfig;
import com.iv3d.common.mvc.control.config.CommandConfigBuilder;
import com.iv3d.common.mvc.control.config.DefaultFormConfigBuilder;
import com.iv3d.common.mvc.control.config.FormConfig;
import com.iv3d.common.mvc.control.config.FormConfigBuilder;
import com.iv3d.common.mvc.control.config.InputConfigBuilder;

import junit.framework.TestCase;

public class DefaultFormBuilderTest extends TestCase {
	private FormConfigBuilder instance;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setupInstance("");
	}

	public void  testIdSet() {
		setupInstance("formId");
		FormConfig config = instance.build();
		assertEquals("formId", config.getId());
	}
	
	public void  testIdSetALLtValue() {
		setupInstance("formId2");
		FormConfig config = instance.build();
		assertEquals("formId2", config.getId());
	}

	private void setupInstance(String id) {
		instance = new DefaultFormConfigBuilder(id);
	}
	
	public void testDefaultName() {
		FormConfig config = instance.build();
		assertEquals("", config.getName());
	}
	
	public void testNameSet() {
		instance.setName("formName");
		FormConfig config = instance.build();
		assertEquals("formName", config.getName());
	}
	
	public void testDefaultInputs() {
		FormConfig config = instance.build();
		assertEquals(0, config.getInputs().size());
	}
	
	public void testCreateInput() {
		InputConfigBuilder builder = instance.createInput("", InputType.TEXT);
		assertNotNull(builder);
		
		FormConfig config = instance.build();
		assertEquals(1, config.getInputs().size());
	}
	
	public void testDefaultFormCammands() {
		FormConfig config = instance.build();
		List<CommandConfig> formCommands = config.getFormCommands();
		assertEquals(0, formCommands.size());
	}
	
	public void testFormCammandsSet() {
		CommandConfigBuilder builder = instance.createFormCommand("");
		assertNotNull(builder);
		
		FormConfig config = instance.build();
		List<CommandConfig> formCommands = config.getFormCommands();
		assertEquals(1, formCommands.size());
		assertNotNull(formCommands.get(0));
	}
	
}
