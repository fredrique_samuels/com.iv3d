/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.mvc.control.config;

import com.iv3d.common.mvc.control.DisplayType;
import com.iv3d.common.mvc.control.config.DisplayConfig;
import com.iv3d.common.mvc.control.config.DisplayConfigBuilder;

import junit.framework.TestCase;

public class DefaultDisplayConfigBuilderTest extends TestCase {
	
	private DisplayConfigBuilder instance;

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setupInstance();
	}

	private void setupInstance() {
		instance = new DefaultDisplayConfigBuilder();
	}
	
	public void testDefaultType() {
		DisplayConfig config = instance.build();
		assertEquals(DisplayType.TEXT, config.getType());
		assertEquals("", config.getStringValue());
	}
	
	public void testTextSet() {
		instance.setText("Text");
		DisplayConfig config = instance.build();
		
		assertEquals(DisplayType.TEXT, config.getType());
		assertEquals("Text", config.getStringValue());
	}
	
	public void testParagraphSet() {
		instance.setParagraph("Paragraph");
		DisplayConfig config = instance.build();
		
		assertEquals(DisplayType.PARAGRAPH, config.getType());
		assertEquals("Paragraph", config.getStringValue());
	}
	
	public void testDefaultId() {
		DisplayConfig config = instance.build();
		assertEquals("", config.getId());
	}
	
	public void testIdSet() {
		instance.setId("testId");
		DisplayConfig config = instance.build();
		assertEquals("testId", config.getId());
	}
	
	public void testDefaultName() throws Exception {
		// When
		DisplayConfig config = instance.build();
		// Then
		assertEquals("", config.getName());
	}
	
	public void testNameSet() throws Exception {
		// Given
		instance.setName("testName");
		// When
		DisplayConfig config = instance.build();
		// Then
		assertEquals("testName", config.getName());
	}
	
}
