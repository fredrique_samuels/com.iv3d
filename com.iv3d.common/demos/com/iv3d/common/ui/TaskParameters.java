package com.iv3d.common.ui;
/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */


import com.iv3d.common.core.Callback;
import com.iv3d.common.ui.executor.UiTaskExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class TaskParameters {
	public static void sleep(long milli) {
		try {
			Thread.sleep(milli);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		/**
		 * Create a waiting semaphore.
		 */
		Semaphore waitOnDone = new Semaphore(1);
		waitOnDone.acquire();

		UiTask task = new UiTask() {

			@Override
			public UiTaskResult run(UiTaskContext context) {
				System.out.println("Autenticating User...");
				String userName = context.getParam("user.name", String.class);
				String password = context.getParam("user.pass", String.class);
				
				if(userName=="root" && password.isEmpty()) {
					return new UiTaskResultBuilder()
						.setMessage("Login complete.")
						.build();
				}
				return new UiTaskResultBuilder().build();
			}
			
		};

		Callback<UiTaskResult> doneCallback = new Callback<UiTaskResult>() {
			
			@Override
			public void invoke(UiTaskResult result) {
				String template = "Result \n\terror=%s\n\tmessage='%s'\n\tvalue=%s\n\texception=%s"; 
				System.out.println(String.format(template, result.hasError(), 
						result.getMessage(),
						result.getValue(),
						result.getException()));
				
				waitOnDone.release();
			}
		};
		
		/**
		 * Create a task context. You must create a new context for each
		 * UiTaskExecutor instance as it's state will not apply to the next run
		 * session.
		 */
		DefaultTaskContext context = new DefaultTaskContext();
		context.addParam("user.name", "root");
		context.addParam("user.pass", "");

		/**
		 * Create a task executor. You must create a new UiTaskExecutor each
		 * time you want to run a task. This object is used to manage callback
		 * and state transitions on the execution thread.
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		UiTaskExecutor<UiTask> executor = new UiTaskExecutor<UiTask>(executorService, context);
		executor.setDoneCallback(doneCallback);
		
		/**
		 * Execute and wait
		 */
		executor.execute(task);
		waitOnDone.acquire();
		
		executorService.shutdown();
	}
}
