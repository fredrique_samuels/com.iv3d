/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.demo1.controller;

import com.iv3d.common.core.Percentage;
import com.iv3d.common.mvc.control.*;
import com.iv3d.common.mvc.control.config.*;

import java.util.*;


public class AuthenicationViewController implements ViewController {
	
	public final static String USER_NAME_FIELD = "userid";
	public final static String PASSWORD_FIELD = "password";

	@Override
	public ViewConfig getView(String id) {
		ViewConfigBuilder viewConfigBuilder = new ViewConfigBuilder();
		
		FlowConfigBuilder authFlow = viewConfigBuilder.createFlow();
		authFlow.setHint(FlowHints.AUTHENTICATION);
		authFlow.setName("User Login");

		FormConfigBuilder authForm = authFlow.createForm("authentication.submit");
		authForm.setName("User Login");
		
		InputConfigBuilder userIdInput = authForm.createInput(USER_NAME_FIELD, InputType.TEXT);
		userIdInput.setName("User Name");
		
		InputConfigBuilder passwordInput = authForm.createInput(PASSWORD_FIELD, InputType.PASSWORD);
		passwordInput.setName("Password");
		
		CommandConfigBuilder checkCommand = authForm.createFormCommand("demos.authenticaton.check");
		checkCommand.setName("Facebook");
		checkCommand.asFormSubmit();
		
		CommandConfigBuilder submitCommand = authForm.createFormCommand("demos.authenticaton.submit");
		submitCommand.setName("Submit");
		submitCommand.asFormSubmit();
		
		return viewConfigBuilder.build();
	}

	@Override
	public CancelCommandRunner execute(String id, List<CommandParam> params, UpdateHandler updateHandler,
			CommandDoneHandler doneHandler) {
		Map<String, List<String>> paramsMap = createParamsMap(params);
		new Thread() {
			@Override
			public void run() {
				runTask(doneHandler, paramsMap);
			}
		}.start();
		updateHandler.postUpdate("Attempting to login...", new Percentage());
		return new CancelCommandRunner() {public void cancel() {}};
	}

	private Map<String, List<String>> createParamsMap(List<CommandParam> params) {
		HashMap<String, List<String>> hashMap = new HashMap<>();
		for (CommandParam c : params) {
			if(!hashMap.containsKey(c.id())) {
				hashMap.put(c.id(), new ArrayList<String>());
			}
			hashMap.get(c.id()).add(c.value());
		}
		return hashMap;
	}

	private void runTask(CommandDoneHandler doneHandler, Map<String, List<String>> paramsMap) {
		delay();
		String user = "";
		String password = "";
		if(paramsMap.containsKey(USER_NAME_FIELD)) {
			user = paramsMap.get(USER_NAME_FIELD).get(0);
		}
		if(paramsMap.containsKey(PASSWORD_FIELD)) {
			password = paramsMap.get(PASSWORD_FIELD).get(0);
		}
		if(user.isEmpty() || password.isEmpty() ) {
			doneHandler.onDone(CommandDoneHandler.FAILURE, "Login Failed", Collections.unmodifiableList(Arrays.asList()));
		} else {
			doneHandler.onDone(CommandDoneHandler.SUCCESS, "Login successfull", Collections.unmodifiableList(Arrays.asList()));			
		}	
	}

	private void delay() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			//ignore
		}
	}

}
