/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.demo1;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AuthDemoMain extends Application {

	@Override
	public void start(Stage stage) {
		GridPane root = new GridPane();
		root.setPadding(new Insets(25, 25, 25, 25));
		// root.setMinSize(300, 300);
		root.setPrefWidth(586);
		root.setVgap(10);
		root.setHgap(10);

		setupColumns(root);
		setupRows(root);

		{
			Text node = new Text("User 'fredriquesamuels@gmail.com' does not exists!");
			root.add(node, 1, 0);
		}
		{
			Text node = new Text("Login");
			root.add(node, 0, 1);
		}
		{
			Label node = new Label("User ID");
			root.add(node, 0, 2);
		}
		{
			Label node = new Label("Password");
			root.add(node, 0, 3);
		}
		{
			TextField node = new TextField();
			node.setPromptText("User Name");
			root.add(node, 1, 2);
		}
		{
			PasswordField node = new PasswordField();
			node.setPromptText("Password");
			root.add(node, 1, 3);
		}
		

		{
			Separator node = new Separator();
			root.add(node, 0, 4, 3, 1);
		}
		
		{
			HBox box = new HBox();
			root.add(box, 1, 5);
			box.setAlignment(Pos.BOTTOM_RIGHT);
			box.setSpacing(10.0);

			{
				Button node = new Button();
				node.setText("Cancel");
				box.getChildren().add(node);
			}
			
			{
				Button node = new Button();
				node.setText("Submit");
				box.getChildren().add(node);
			}
		}

		Scene scene = new Scene(root);
		stage.setTitle("JavaFX Scene Graph Demo");
		stage.setScene(scene);
		stage.show();
	}

	private void setupRows(GridPane root) {
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(22.0);
			row.setPrefHeight(22.0);
			root.getRowConstraints().add(row);
		}
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(46.0);
			row.setPrefHeight(46.0);
			root.getRowConstraints().add(row);
		}
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(22.0);
			row.setPrefHeight(22.0);
			root.getRowConstraints().add(row);
		}
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(22.0);
			row.setPrefHeight(22.0);
			root.getRowConstraints().add(row);
		}
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(0);
			row.setPrefHeight(0);
			root.getRowConstraints().add(row);
		}
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(22.0);
			row.setPrefHeight(22.0);
			root.getRowConstraints().add(row);
		}
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(0);
			row.setPrefHeight(0);
			root.getRowConstraints().add(row);
		}
	}

	private void setupColumns(GridPane root) {
		ColumnConstraints col1 = new ColumnConstraints();
		col1.setHgrow(Priority.SOMETIMES);
		ColumnConstraints col2 = new ColumnConstraints();
		col2.setHgrow(Priority.NEVER);
		col2.setHalignment(HPos.LEFT);
		ColumnConstraints col3 = new ColumnConstraints();
		col3.setHalignment(HPos.LEFT);
		root.getColumnConstraints().addAll(col1, col2, col3);
	}
	public static void main(String[] args) {
		launch(args);
	}
}
	
	