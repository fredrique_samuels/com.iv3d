/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.demo1;

import com.iv3d.coltron.demos.demo1.controller.AuthenicationViewController;
import com.iv3d.coltron.demos.flows.ViewControlFactory;
import com.iv3d.common.mvc.control.FlowRunnerService;
import com.iv3d.common.mvc.control.ViewController;

import javafx.application.Application;
import javafx.stage.Stage;

public class AuthenticationDemo extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(final Stage stage) {
		// create the View control service
		ViewController controller = new AuthenicationViewController();
		FlowRunnerService runnerService = new ViewControlFactory().create(controller);
		
		// run views
		runnerService.run("main");
	}

}
