/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.flows.jfx;

import java.util.List;

import com.iv3d.coltron.demos.flows.jfx.common.DemoView;
import com.iv3d.coltron.demos.flows.jfx.common.FlowRunner;
import com.iv3d.common.mvc.control.FlowView;

import javafx.geometry.Pos;
import javafx.scene.control.Separator;
import javafx.scene.layout.VBox;

public class DemoFlowView extends DemoView<VBox> implements FlowView {
	private FlowRunner runner;
	private String title;

	public DemoFlowView(FlowRunner runner) {
		super(new VBox());
		this.runner = runner;
		setup();
	}
	
	public final void run() {runner.run(node);}
	public final void setTitle(String title) {this.title = title;}
	public final String getTitle() {return title;}

	public final void addForms(List<DemoFormView> forms) {
		if(forms.size()==0)
			return;
		
		node.getChildren().add(forms.get(0).getNode());
		for (int i = 1; i < forms.size(); i++) {
			node.getChildren().add(new Separator());
			node.getChildren().add(forms.get(i).getNode());
		} 
	}
	
	private void setup() {
		node.setAlignment(Pos.TOP_CENTER);
		node.setSpacing(10.0);
		node.setMinWidth(600);
	}
}
