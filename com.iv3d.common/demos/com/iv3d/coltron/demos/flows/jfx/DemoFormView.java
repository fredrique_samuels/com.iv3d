/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.flows.jfx;

import java.util.List;

import com.iv3d.coltron.demos.flows.jfx.common.GridView;
import com.iv3d.common.core.Percentage;
import com.iv3d.common.mvc.control.CancelCommandRunner;
import com.iv3d.common.mvc.control.FlowView;
import com.iv3d.common.mvc.control.FormView;

import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;

public class DemoFormView extends GridView implements FormView {
	private final Button cancelButton;
	private int currentRow;
	private CancelCommandRunner cancelCommandRunner;
	private List<DemoInputView> inputs;
	private List<DemoCommandView> commands;
	private Text statusText;
	
	public DemoFormView(String id, String name) {
		super();
		setId(id);
		setName(name);
		this.cancelButton = new Button("Cancel");
		this.statusText = new Text("info");
		this.currentRow=0;
		setupInfoText();
		setupHeading();
	}

	public final void setInputs(List<DemoInputView> inputs) {
		this.inputs = inputs;
		for(DemoInputView inputView:inputs) {
			addInput(inputView);
		}
	}
	
	public void setCommands(List<DemoCommandView> commands) {
		this.commands = commands;
		RowConstraints row = new RowConstraints();
		row.setMaxHeight(22.0);
		row.setPrefHeight(22.0);
		node.getRowConstraints().add(row);
		
		HBox box = new HBox();
		node.add(box, 1, currentRow);
		box.setAlignment(Pos.BOTTOM_RIGHT);
		box.setSpacing(10.0);
		
		
		box.getChildren().add(cancelButton);
		disableCancelButton();
		for(DemoCommandView view:commands) {
			box.getChildren().add(view.getNode());
			view.addActionCallback(c->actionExecuted(c));
			view.addDoneHandler((s,m,f)->onCommandDone(s,m,f));
			view.addUpdateHandler((m,p)->onUpdate(m,p));
		}
		currentRow++;
	}
	
	private void onUpdate(String message, Percentage percentage) {
		statusText.setText(message);
	}

	private void onCommandDone(String state, String message, List<FlowView> flows) {
		statusText.setText(message);
		disableCancelButton();
		enableAllInputs();
	}

	private void enableAllInputs() {
		if(inputs!=null)
			inputs.stream().forEach(v->v.enable());
		if(commands!=null)
			commands.stream().forEach(v->v.enable());
	}

	private void disableAllInputs() {
		if(inputs!=null)
			inputs.stream().forEach(v->v.disable());
		if(commands!=null)
			commands.stream().forEach(v->v.disable());
	}
	
	private void disableCancelButton() {
		cancelButton.setDisable(true);
	}

	private void actionExecuted(CancelCommandRunner c) {
		this.cancelCommandRunner = c;
		enableCancelButton();
		disableAllInputs();
	}

	private void enableCancelButton() {
		cancelButton.setDisable(false);
	}

	private void addInput(DemoInputView inputView) {
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(22.0);
			row.setPrefHeight(22.0);
			node.getRowConstraints().add(row);
		}
		
		addNode(new Label(inputView.getName()), 0, currentRow, 1, 1);
		addNode(inputView.getNode(), 1, currentRow, 1, 1);
		currentRow++;
	}

	private void setupInfoText() {
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(22.0);
			row.setPrefHeight(22.0);
			node.getRowConstraints().add(row);
		}
		GridPane.setHalignment(statusText, HPos.CENTER);
		addNode(statusText, 0, currentRow, 3, 1);
		currentRow++;
	}
	
	private void setupHeading() {
		{
			RowConstraints row = new RowConstraints();
			row.setMaxHeight(46.0);
			row.setPrefHeight(46.0);
			node.getRowConstraints().add(row);
		}
		addNode(new Text(name), 0, currentRow, 3, 1);
		currentRow++;
	}
}
