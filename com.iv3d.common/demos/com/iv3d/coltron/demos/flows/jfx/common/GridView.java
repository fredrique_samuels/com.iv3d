/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.flows.jfx.common;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;


public class GridView extends DemoView<GridPane> {
	
	public GridView() {
		super(new GridPane());
		node.setPadding(new Insets(25, 25, 25, 25));
		node.setPrefWidth(586);
		node.setVgap(10);
		node.setHgap(10);
		setupColumns();
	}
	
	protected final void addNode(Node child, int columnIndex, int rowIndex) {
		int rowspan = 1;
		int colspan = 1;
		addNode(child, columnIndex, rowIndex, colspan, rowspan);
	}

	protected final void addNode(Node child, int columnIndex, int rowIndex, int colspan, int rowspan) {
		node.add(child, columnIndex, rowIndex, colspan, rowspan);
	}
	
	private void setupColumns() {
		ColumnConstraints col1 = new ColumnConstraints();
		col1.setHgrow(Priority.NEVER);
		node.getColumnConstraints().add(col1);
		
		ColumnConstraints col2 = new ColumnConstraints();
		col2.setHgrow(Priority.SOMETIMES);
		col2.setHalignment(HPos.LEFT);
		node.getColumnConstraints().add(col2);
		
		ColumnConstraints col3 = new ColumnConstraints();
		col3.setHalignment(HPos.LEFT);
		node.getColumnConstraints().add(col3);
	}
}
