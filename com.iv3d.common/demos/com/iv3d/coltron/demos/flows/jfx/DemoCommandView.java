/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.flows.jfx;

import java.util.ArrayList;
import java.util.List;

import com.iv3d.coltron.demos.flows.jfx.common.CommandActionCallback;
import com.iv3d.coltron.demos.flows.jfx.common.DemoView;
import com.iv3d.common.core.Percentage;
import com.iv3d.common.mvc.control.CancelCommandRunner;
import com.iv3d.common.mvc.control.CommandDoneHandler;
import com.iv3d.common.mvc.control.CommandExecutor;
import com.iv3d.common.mvc.control.CommandView;
import com.iv3d.common.mvc.control.FlowView;
import com.iv3d.common.mvc.control.UpdateHandler;
import com.iv3d.common.mvc.control.config.CommandParam;

import javafx.scene.control.Button;

public class DemoCommandView extends DemoView<Button> implements CommandView {
	private final List<UpdateHandler> updateHandlers;
	private final List<CommandDoneHandler> doneHandlers;
	private final List<CommandActionCallback> cancelCommandRunners;

	public DemoCommandView(String id, String name, List<CommandParam> params, CommandExecutor executor) {
		super(new Button());
		setId(id);
		setName(name);
		updateHandlers = new ArrayList<>();
		doneHandlers = new ArrayList<>();
		cancelCommandRunners = new ArrayList<>();
		node.setText(name);
		node.setOnAction((e)->executeCommand(id, params, executor));
	}
	
	public final void addActionCallback(CommandActionCallback callback) {
		synchronized(cancelCommandRunners) {
			this.cancelCommandRunners.add(callback);
		}
	}
	
	public final void addUpdateHandler(UpdateHandler h) {
		synchronized (updateHandlers) {
			updateHandlers.add(h);
		}
	}
	
	public final void addDoneHandler(CommandDoneHandler h) { 
		synchronized (doneHandlers) {
			doneHandlers.add(h);
		}
	}
	
	private void executeCommand(String id, List<CommandParam> params, CommandExecutor executor) {
		CancelCommandRunner cancelRunner = execute(id, params, executor);
		disptachActionEvents(cancelRunner);
	}

	private void disptachActionEvents(CancelCommandRunner cancelRunner) {
		List<CommandActionCallback> callbacks = copyActionCallbacks();
		callbacks.stream().forEach(c->c.onAction(cancelRunner));
	}

	private CancelCommandRunner execute(String id, List<CommandParam> params, CommandExecutor executor) {
		UpdateDisptacher updateHandler = new UpdateDisptacher(copyUpdateHandlers());
		CommandDoneHandler doneDisptacher = new DoneDisptacher(copyDoneHandlers());
		CancelCommandRunner cancelRunner = executor.execute(id, params, updateHandler, doneDisptacher);
		return cancelRunner;
	}

	private List<CommandActionCallback> copyActionCallbacks() {
		List<CommandActionCallback> callbacks;
		synchronized(cancelCommandRunners) {
			callbacks = new ArrayList<>(cancelCommandRunners);
		}
		return callbacks;
	}

	private ArrayList<CommandDoneHandler> copyDoneHandlers() {
		ArrayList<CommandDoneHandler> arrayList = null;
		synchronized (doneHandlers) {			
			arrayList = new ArrayList<>(doneHandlers);
		}
		return arrayList;
	}

	private ArrayList<UpdateHandler> copyUpdateHandlers() {
		ArrayList<UpdateHandler> arrayList = null;
		synchronized(updateHandlers) {
			arrayList = new ArrayList<>(updateHandlers);
		}
		return arrayList;
	}
	
	private static class UpdateDisptacher implements UpdateHandler {
		
		private final List<UpdateHandler> handlers;

		public UpdateDisptacher(List<UpdateHandler> handlers) {
			this.handlers = handlers;
		}

		@Override
		public void postUpdate(String message, Percentage progress) {
			handlers.stream().forEach(h->h.postUpdate(message, progress));
		}
	}
	
	private static class DoneDisptacher implements CommandDoneHandler {
		
		private final List<CommandDoneHandler> handlers;
		
		public DoneDisptacher(List<CommandDoneHandler> handlers) {
			this.handlers = handlers;
		}
		
		@Override
		public void onDone(String state, String message, List<FlowView> flows) {
			handlers.stream().forEach(h->h.onDone(state, message, flows));
		}
		
	}	
}
