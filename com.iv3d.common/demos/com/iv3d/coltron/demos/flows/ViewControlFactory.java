/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.flows;

import com.iv3d.coltron.demos.flows.jfx.DemoCommandView;
import com.iv3d.coltron.demos.flows.jfx.DemoDisplayView;
import com.iv3d.coltron.demos.flows.jfx.DemoFlowView;
import com.iv3d.coltron.demos.flows.jfx.DemoFormView;
import com.iv3d.coltron.demos.flows.jfx.DemoInputView;
import com.iv3d.common.core.NonNullRules;
import com.iv3d.common.mvc.control.DisplayType;
import com.iv3d.common.mvc.control.FlowRunnerService;
import com.iv3d.common.mvc.control.InputType;
import com.iv3d.common.mvc.control.ViewController;
import com.iv3d.common.mvc.control.service.DefaultViewControlService;


public final class ViewControlFactory {

	
	public final FlowRunnerService create(ViewController controller) {
		NonNullRules nonNullRules = getNonNullRules();
		DemoBuiderFactory viewBuilderFactory = new DemoBuiderFactory();
		
		DefaultViewControlService<FlowContext, DemoFlowView, DemoFormView, DemoInputView, DemoDisplayView, DemoCommandView> controlService = 
					new DefaultViewControlService<>(nonNullRules, controller);
		controlService.setViewBuilderFactory(viewBuilderFactory);
		return controlService;
	}

	private NonNullRules getNonNullRules() {
		NonNullRules nonNull = new NonNullRules();
		nonNull.add("", String.class);
		nonNull.add(DisplayType.TEXT, DisplayType.class);
		nonNull.add(InputType.TEXT, InputType.class);
		return nonNull;
	}

}
