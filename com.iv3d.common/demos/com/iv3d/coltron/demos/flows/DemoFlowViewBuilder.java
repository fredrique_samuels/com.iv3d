/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.coltron.demos.flows;

import com.iv3d.coltron.demos.flows.jfx.DemoDisplayView;
import com.iv3d.coltron.demos.flows.jfx.DemoFlowView;
import com.iv3d.coltron.demos.flows.jfx.DemoFormView;
import com.iv3d.coltron.demos.flows.jfx.DemoInputView;
import com.iv3d.common.mvc.control.AbstractFlowViewBuilder;
import com.iv3d.common.mvc.control.FlowView;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class DemoFlowViewBuilder extends AbstractFlowViewBuilder
	<FlowContext, DemoFlowView, DemoFormView, DemoInputView, DemoDisplayView> {

	public DemoFlowViewBuilder() {
	}

	@Override
	public DemoFlowView build(FlowContext flowContext) {
		DemoFlowView flow = new DemoFlowView((f)->runParent(f));
		flow.setTitle(name);
		flow.addForms(forms);
		return flow;
	}
	
	private static void runParent(Parent node) {
		Stage stage = new Stage();
		Scene scene = new Scene(node);
		String title = getTitle(node);
		stage.setTitle(title);
		stage.setScene(scene);
		stage.show();
	}
	
	private static String getTitle(Parent node) {
		if(node instanceof FlowView) {
			return ((FlowView)node).getTitle();
		}
		return "JavaFX Scene Graph Demo";
	}
}
