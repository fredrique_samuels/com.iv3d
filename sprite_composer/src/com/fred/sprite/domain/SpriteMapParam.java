package com.fred.sprite.domain;

import com.iv3d.common.core.Provider;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/09/09.
 */
public class SpriteMapParam {
    private long id=-1;
    private String name = "Untitled";
    private String spriteMapFile = null;
    private List<SpriteParam> sprites;
    private ColorParam alphaColor = new ColorParam();
    private long spriteIdSeed = 0;
    private Provider<Long> spriteIdSeedProvider;


    public SpriteMapParam() {
        this.sprites = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpriteMapFile() {
        return spriteMapFile;
    }

    public void setSpriteMapFile(String spriteMapFile) {
        this.spriteMapFile = spriteMapFile;
    }

    public ColorParam getAlphaColor() {
        return alphaColor;
    }

    public void setAlphaColor(ColorParam alphaColor) {
        this.alphaColor = alphaColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SpriteParam> getSprites() {
        return sprites;
    }

    public void setSprites(List<SpriteParam> sprites) {
        this.sprites = sprites;
    }

    public long createSprite(int cx, int cy) {
        long id = createId();

        SpriteParam e = new SpriteParam();
        e.setCenterX(cx);
        e.setCenterY(cy);
        e.setX(cx-25);
        e.setY(cy-25);
        e.setWidth(50);
        e.setHeight(50);
        e.setId(id);
        this.sprites.add(e);

        return id;
    }

    public long createSprite(SpriteParam newSpriteParam) {
        long id = createId();

        SpriteParam e = new SpriteParam(id, newSpriteParam);
        this.sprites.add(e);
        return id;
    }

    private long createId() {
        return ++spriteIdSeed;
    }

    public long copySprite(long id) {
        SpriteParam sprite = findSprite(id);
        long newId = createId();
        SpriteParam spriteParam = new SpriteParam(newId, sprite);
        return newId;
    }

    public void deleteSprite(long id) {
        SpriteParam p = findSprite(id);
        if(p!=null) {
            sprites.remove(p);
        }
    }

    public SpriteParam findSprite(long id) {
        for (SpriteParam p: sprites) {
            if(p.getId()==id)return p;
        }
        return null;
    }

    public long getSpriteIdSeed() {
        return spriteIdSeed;
    }

    public void setSpriteIdSeed(long spriteIdSeed) {
        this.spriteIdSeed = spriteIdSeed;
    }

    public static Image makeTransparent(Image inputImage, ColorParam color) {
        int W = (int) inputImage.getWidth();
        int H = (int) inputImage.getHeight();
        WritableImage outputImage = new WritableImage(W, H);
        PixelReader reader = inputImage.getPixelReader();
        PixelWriter writer = outputImage.getPixelWriter();
        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                int argb = reader.getArgb(x, y);

                int r = (argb >> 16) & 0xFF;
                int g = (argb >> 8) & 0xFF;
                int b = argb & 0xFF;

                if (r == color.getRed()
                        && g == color.getGreen()
                        && b == color.getBlue()) {
                    argb &= 0x00FFFFFF;
                }

                writer.setArgb(x, y, argb);
            }
        }

        return outputImage;
    }

    public void configureSpriteIdSeedProvider(Provider<Long> spriteIdSeedProvider) {
        this.spriteIdSeedProvider = spriteIdSeedProvider;
    }

}
