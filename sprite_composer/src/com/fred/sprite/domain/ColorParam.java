package com.fred.sprite.domain;

/**
 * Created by fred on 2016/09/09.
 */
public class ColorParam {
    private int red;
    private int green;
    private int blue;

    public ColorParam() {
        this.red = 0;
        this.green = 0;
        this.blue = 0;
    }

    public ColorParam(int r, int g, int b) {
        red = r;
        green = g;
        blue = b;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }
}
