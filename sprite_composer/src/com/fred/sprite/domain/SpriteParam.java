package com.fred.sprite.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by fred on 2016/09/09.
 */
public class SpriteParam {
    private long id;
    private long mapId;
    private String name;
    private int centerX;
    private int centerY;
    private int x;
    private int y;
    private int width;
    private int height;
    private long parentId;

    public SpriteParam() {
        this.name = "";
        this.centerX=0;
        this.centerY=0;
        this.x=0;
        this.y=0;
        this.width=0;
        this.height=0;
        this.id = -1;
    }

    public SpriteParam(long newId, SpriteParam sprite) {
        this.name = sprite.name;
        this.centerX=sprite.centerX;
        this.centerY=sprite.centerY;
        this.x=sprite.x;
        this.y=sprite.y;
        this.width=sprite.width;
        this.height=sprite.height;
        this.id = newId;
    }

    public void moveTo(int cx, int cy) {
        int xDiff = this.x-centerX;
        int yDiff = this.y-centerY;

        this.x = cx+xDiff;
        this.y = cy+yDiff;
        this.centerX = cx;
        this.centerY = cy;
    }

    public long getMapId() {
        return mapId;
    }

    public void setMapId(long mapId) {
        this.mapId = mapId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @JsonIgnore
    public long getParentId() {
        return parentId;
    }

    @JsonIgnore
    public void setParentId(long parentId) {
        this.parentId = parentId;
    }
}
