package com.fred.sprite.domain;

import com.iv3d.common.utils.JsonUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/09/23.
 */
public class SpriteProject {
    private long spriteIdSeed=0;
    private long spriteMapIdSeed=0;
    private Map<Long, SpriteMapParam> spriteMapParams = new HashMap<>();

    public static SpriteProject load(String file) {
        return JsonUtils.readFromFile(file, SpriteProject.class);
    }

    public List<SpriteMapParam> getSpriteMapParams() {
        return spriteMapParams.values().stream().collect(Collectors.toList());
    }

    public void setSpriteMapParams(List<SpriteMapParam> map) {
        spriteMapParams.clear();
        for (SpriteMapParam p: map) {
            spriteMapParams.put(p.getId(), p);
        }
    }

    public long getSpriteIdSeed() {
        return spriteIdSeed;
    }

    public void setSpriteIdSeed(long spriteIdSeed) {
        this.spriteIdSeed = spriteIdSeed;
    }

    public SpriteMapParam findSpriteMap(long id) {
        return spriteMapParams.get(id);
    }

    public long getSpriteMapIdSeed() {
        return spriteMapIdSeed;
    }

    public void setSpriteMapIdSeed(long spriteMapIdSeed) {
        this.spriteMapIdSeed = spriteMapIdSeed;
    }

    public long importSpriteParam(SpriteMapParam spriteMapParam) {
        long smId = ++spriteMapIdSeed;
        spriteMapParam.setId(smId);
        spriteMapParam.configureSpriteIdSeedProvider(()->++spriteIdSeed);
        spriteMapParams.put(smId, spriteMapParam);
        return smId;
    }
}
