package com.fred.sprite;

/**
 * Created by fred on 2016/09/10.
 */

import com.fred.sprite.bleach.BleachArenaScene;
import com.fred.sprite.common.SpriteSceneUpdater;
import com.fred.sprite.scene.SpriteScene;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;

public class SpriteApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Drawing Operations Test");
        Group root = new Group();
        Canvas canvas = new Canvas(800, 600);

        SpriteScene spriteScene = new BleachArenaScene();

        final SpriteSceneUpdater spriteSceneUpdater = new SpriteSceneUpdater(canvas, spriteScene);
        spriteSceneUpdater.start();

        root.getChildren().add(canvas);

        Scene scene = new Scene(root);
        scene.setOnKeyPressed(event -> {
//            System.err.println(event);
            spriteScene.setKey(event.getCode().toString());
        });
        scene.setOnKeyReleased(event ->
            {
//                System.err.println(event);
                spriteScene.removeKey(event.getCode().toString());
            }
        );
        primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
