package com.fred.sprite.editor.views;

/**
 * Created by fred on 2016/09/09.
 */
public class RootNavItem extends NavItem {
    private String name;

    public RootNavItem(String name) {
        super(-1, Type.NONE);
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
