package com.fred.sprite.editor.views;

/**
 * Created by fred on 2016/09/09.
 */
public class NavItem {
    public enum Type {
        NONE,
        SPRITE_PARAM,
        SPRITE_MAP
    }
    private long id;
    private Type type;
    private String name;

    public NavItem(long id, Type type) {
        this.id = id;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return id+"-"+name;
    }

    public NavItem setName(String name) {
        this.name = name;
        return this;
    }
}
