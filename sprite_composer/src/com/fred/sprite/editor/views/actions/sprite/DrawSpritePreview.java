package com.fred.sprite.editor.views.actions.sprite;

import com.fred.sprite.editor.ViewControl;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.iv3d.common.core.Rectangle;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.CheckBox;
import javafx.scene.paint.Color;

/**
 * Created by fred on 2016/09/23.
 */
public class DrawSpritePreview {

    @Inject
    Provider<ViewControl> viewControlProvider;
    @Inject
    SpriteMapEditorManager editorManager;

    public void run() {
//        editorManager.getSelectedSprite
//        if(selectedSpriteParam==null) {
//            return;
//        }
        Canvas canvas = (Canvas) findNodeById("sprite_preview_canvas");
        boolean alignBottom = ((CheckBox) findNodeById("sprite_preview_centered_check")).isSelected();
        boolean alignLeft = ((CheckBox) findNodeById("sprite_preview_align_left_check")).isSelected();
        boolean scaleX2 = ((CheckBox) findNodeById("sprite_preview_scale_check")).isSelected();
        boolean showBb = ((CheckBox) findNodeById("sprite_preview_bb_check")).isSelected();
        boolean showGuide = ((CheckBox) findNodeById("sprite_preview_guide_check")).isSelected();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, 600, 600);

        gc.setFill(Color.LIGHTGRAY);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(0);

        gc.fill();
        int width = 600;
        int height = 600;
        int halfWidth = Double.valueOf(width*.5).intValue();
        int halfHeight = Double.valueOf(height*.5).intValue();
        gc.strokeRect(
                0,              //x of the upper left corner
                0,              //y of the upper left corner
                width,    //width of the rectangle
                height);  //height of the rectangle

        gc.setLineWidth(1);



        int x = alignLeft?100:halfWidth;
        int y = alignBottom?width-25:halfWidth;

        if(showGuide) {
            gc.strokeLine(0, y, width, y);
            gc.strokeLine(x, 0, x, height);
        }

        Rectangle d = null;
//        spriteRenderer.drawOnCanvas(gc, spriteMapImage, selectedSpriteParam,
//                SpriteRenderer.createDrawSettings(new Point(x, y)).setScale(scaleX2 ? 2 : 1));

        if(showBb) {
            gc.strokeRect(
                    d.getX(),              //x of the upper left corner
                    d.getY(),              //y of the upper left corner
                    d.getWidth(),    //width of the rectangle
                    d.getHeight());  //height of the rectangle
        }
    }

    private Object findNodeById(String s) {
        return viewControlProvider.get().findNodeById(s);
    }
}
