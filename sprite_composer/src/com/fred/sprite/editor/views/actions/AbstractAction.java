package com.fred.sprite.editor.views.actions;

import com.fred.sprite.editor.ViewControl;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by fred on 2016/09/09.
 */
public class AbstractAction {
    @Inject
    Provider<ViewControl> viewControlProvider;
    @Inject
    protected SpriteMapEditorManager editorManager;

    protected final ViewControl getViewControl(){return viewControlProvider.get();}
}
