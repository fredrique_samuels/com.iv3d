package com.fred.sprite.editor.views.actions.edit;

import com.fred.sprite.editor.ViewControl;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.io.File;

/**
 * Created by fred on 2016/09/09.
 */
public class LoadSpriteMapImage {
    @Inject
    Provider<ViewControl> viewControlProvider;
    @Inject
    SpriteMapEditorManager editorManager;

    public void run() {
        ViewControl viewControl = viewControlProvider.get();

        File file = viewControl.selectFileForLoading("Select Sprite Map Image");
        if(file!=null) {
            editorManager.setMapFile(file);
            viewControl.updateSpriteMapImage();
            viewControl.saveProject();
        }
    }
}
