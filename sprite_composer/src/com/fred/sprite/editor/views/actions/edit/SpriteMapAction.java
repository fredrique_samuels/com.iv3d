package com.fred.sprite.editor.views.actions.edit;

import com.fred.sprite.editor.views.actions.AbstractAction;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Created by fred on 2016/09/09.
 */
public class SpriteMapAction extends AbstractAction {
    public void run(MouseEvent event) {
        Double x = event.getX();
        Double y = event.getY();

        boolean createSprite = ((RadioButton) getViewControl().findNodeById("create_sprite_check")).isSelected();
        if(createSprite) {
//            long id = editorManager.createSprite(x.intValue(), y.intValue());
//            getViewControl().addSpriteToNav(id);

            boolean newSpriteEdgeCenter = ((RadioButton) getViewControl().findNodeById("newSpriteEdgeCenter")).isSelected();
            if(newSpriteEdgeCenter) {
                getViewControl().setNewSpriteCenter(x.intValue(), y.intValue());
            }

            boolean newSpriteEdgeLeft = ((RadioButton) getViewControl().findNodeById("newSpriteEdgeLeft")).isSelected();
            if(newSpriteEdgeLeft) {
                getViewControl().setNewSpriteX(x.intValue());
            }

            boolean newSpriteEdgeTop = ((RadioButton) getViewControl().findNodeById("newSpriteEdgeTop")).isSelected();
            if(newSpriteEdgeTop) {
                getViewControl().setNewSpriteY(y.intValue());
            }

            boolean newSpriteEdgeRight = ((RadioButton) getViewControl().findNodeById("newSpriteEdgeRight")).isSelected();
            if(newSpriteEdgeRight) {
                getViewControl().setNewSpriteWidth(x.intValue());
            }

            boolean newSpriteEdgeBottom = ((RadioButton) getViewControl().findNodeById("newSpriteEdgeBottom")).isSelected();
            if(newSpriteEdgeBottom) {
                getViewControl().setNewSpriteHeight(y.intValue());
            }
        } else {
            getViewControl().setAlphaColor(x.intValue(), y.intValue());
        }
    }
}
