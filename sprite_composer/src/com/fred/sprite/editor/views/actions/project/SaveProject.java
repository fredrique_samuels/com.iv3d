package com.fred.sprite.editor.views.actions.project;

import com.fred.sprite.editor.ViewControl;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.io.File;

/**
 * Created by fred on 2016/09/09.
 */
public class SaveProject {

    @Inject
    Provider<ViewControl> viewControlProvider;
    @Inject
    SpriteMapEditorManager editorManager;

    public void run() {
        if(editorManager.isSaved()) {
            editorManager.save();
        } else {
            File file = viewControlProvider.get().selectFileForSaving("Select project file");
            if(file!=null) {
                editorManager.save(file);
            }
        }
    }
}
