// Project
function newProject(){controller.newProject();}
function saveProject(){controller.saveProject();}
function loadProject(){controller.loadProject();}

//Edit
function loadSpriteMapImage(){controller.loadSpriteMapImage();}
function imageMapClicked(event){controller.imageMapClicked(event);}
function createNewSprite(event){controller.createNewSprite(event);}
function setSpriteName(){controller.setSpriteName();}
function deleteSprite(){controller.deleteSprite();}
function toggleSpritePreviewScale(){controller.toggleSpritePreviewScale();}
function toggleSpritePreviewCentered(){controller.toggleSpritePreviewCentered();}
function toggleSpritePreviewAlignLeft(){controller.toggleSpritePreviewAlignLeft();}
function toggleSpritePreviewGuide(){controller.toggleSpritePreviewGuide();}
function toggleSpritePreviewBB(){controller.toggleSpritePreviewBB();}

function exportSprites(){controller.exportSprites();}