package com.fred.sprite.editor.views;

import com.fred.sprite.domain.SpriteMapParam;
import com.fred.sprite.domain.SpriteParam;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.util.List;

/**
 * Created by fred on 2016/09/23.
 */
public class SpriteMapRootTreeItem extends TreeItem<NavItem> {

    public SpriteMapRootTreeItem() {
        super(new RootNavItem("Maps"));
    }

    public void refresh(SpriteMapEditorManager editorManager) {
        ObservableList<TreeItem<NavItem>> spriteMapNavItemChildren = getChildren();
        spriteMapNavItemChildren.clear();

        List<SpriteMapParam> spriteMapParams = editorManager.getSpriteMapParams();
        for(SpriteMapParam p : spriteMapParams) {
            SpriteMapTreeItem spriteMapTreeItem = addSpriteMapToNav(p);
            spriteMapTreeItem.refreshSprites(editorManager);
        }
    }

    public SpriteMapTreeItem addSpriteMapToNav(SpriteMapParam p) {
        SpriteMapTreeItem e = new SpriteMapTreeItem(p.getId(), p.getName());
        getChildren().add(e);
        return e;
    }

    public void addSpriteParamToNav(SpriteMapEditorManager editorManager, SpriteParam p) {
        long activeSpriteMapParamId = editorManager.getActiveSpriteMapParamId();

        ObservableList<TreeItem<NavItem>> children = getChildren();
        for(TreeItem<NavItem> t : children) {
            NavItem value = t.getValue();
            if(value.getId()==activeSpriteMapParamId && value.getType()== NavItem.Type.SPRITE_MAP) {
                SpriteMapTreeItem smti = (SpriteMapTreeItem) t;
                smti.addSprite(p);
            }
        }

    }

    public void refreshSpriteItem(long activeSpriteMapParamId, SpriteParam spriteParam) {
        ObservableList<TreeItem<NavItem>> children = getChildren();
        for (TreeItem<NavItem> p:children) {
            if(p.getValue().getId()==activeSpriteMapParamId) {
                SpriteMapTreeItem smti = (SpriteMapTreeItem) p;
                smti.updateSprite(spriteParam);
            }
        }
     }
}
