package com.fred.sprite.editor.views;

/**
 * Created by fred on 2016/09/23.
 */
public class SpriteMapParamNavItem extends NavItem {
    private String name;

    public SpriteMapParamNavItem(long id, String name) {
        super(id, Type.SPRITE_MAP);
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + "-"+name;
    }

}
