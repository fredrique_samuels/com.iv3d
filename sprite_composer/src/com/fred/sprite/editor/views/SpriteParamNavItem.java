package com.fred.sprite.editor.views;

/**
 * Created by fred on 2016/09/09.
 */
public class SpriteParamNavItem extends NavItem {
    private String name;

    public SpriteParamNavItem(long id, String name) {
        super(id, Type.SPRITE_PARAM);
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + "-"+name;
    }
}
