package com.fred.sprite.editor.views;

import com.fred.sprite.domain.SpriteMapParam;
import com.fred.sprite.domain.SpriteParam;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.util.List;

/**
 * Created by fred on 2016/09/23.
 */
public class SpriteMapTreeItem extends TreeItem<NavItem> {
    private final TreeItem<NavItem> spriteParamRoot;

    public SpriteMapTreeItem(long id, String name) {
        super(new NavItem(id, NavItem.Type.SPRITE_MAP).setName(name));

        spriteParamRoot = new TreeItem<>(new RootNavItem("Sprites"));
        getChildren().add(spriteParamRoot);
    }

    public void refreshSprites(SpriteMapEditorManager editorManager) {
        ObservableList<TreeItem<NavItem>> children = spriteParamRoot.getChildren();
        children.clear();

        SpriteMapParam spriteMapParam = editorManager.findSpriteMapParam(getValue().getId());
        List<SpriteParam> sprites = spriteMapParam.getSprites();
        for (SpriteParam p:sprites) {
            addSprite(p);
        }
    }

    public void addSprite(SpriteParam p) {
        ObservableList<TreeItem<NavItem>> children = spriteParamRoot.getChildren();
        p.setParentId(getValue().getId());
        children.add(new TreeItem<>(new NavItem(p.getId(), NavItem.Type.SPRITE_PARAM).setName(p.getName())));
    }

    public void updateSprite(SpriteParam p) {
        ObservableList<TreeItem<NavItem>> children = spriteParamRoot.getChildren();
        for (TreeItem<NavItem> i:children) {
            if(i.getValue().getId()==p.getId()) {
                i.setValue(new NavItem(p.getId(), NavItem.Type.SPRITE_PARAM).setName(p.getName()));
            }
        }
    }
}
