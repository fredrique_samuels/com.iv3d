package com.fred.sprite.editor.domain;

import com.fred.sprite.domain.ColorParam;
import com.fred.sprite.domain.SpriteMapParam;
import com.fred.sprite.domain.SpriteParam;
import com.fred.sprite.domain.SpriteProject;
import com.google.inject.Singleton;
import com.iv3d.common.utils.JsonUtils;

import java.io.File;
import java.util.List;

/**
 * Created by fred on 2016/09/09.
 */
@Singleton
public class SpriteMapEditorManager {
    private static File SETTINGS_FILE = new File("editor.settings.json");

    private SpriteProject spriteProject;
    private long activeSpriteMapParamId = -1;

    private EditorSettings settings;
    private File currentFile;

    public SpriteMapEditorManager() {
        this.spriteProject = new SpriteProject();
        this.activeSpriteMapParamId = this.spriteProject.importSpriteParam(new SpriteMapParam());

        this.settings = new EditorSettings();
        if(SETTINGS_FILE.exists()) {
            loadSettings();
        } else {
            saveSettings();
        }
    }

    public void saveSettings() {
        JsonUtils.writeToFile(SETTINGS_FILE, settings);
    }

    private void loadSettings() {
        settings = JsonUtils.readFromFile(SETTINGS_FILE, EditorSettings.class);
    }

    public void loadProject(File file) {
        if(file.exists()) {
            try {
                SpriteMapParam spriteMapParam = JsonUtils.readFromFile(file, SpriteMapParam.class);
                activeSpriteMapParamId = spriteProject.importSpriteParam(spriteMapParam);
            } catch (Exception e) {
                spriteProject = JsonUtils.readFromFile(file, SpriteProject.class);
                activeSpriteMapParamId = spriteProject.getSpriteMapParams().get(0).getId();
            }
            currentFile = file;

            settings.setCurrentWorkingDir(file.getParentFile().getPath());
            settings.setCurrentProjectFile(file.getName());
            saveSettings();
        }
    }

    public void newProject(File file) {
        currentFile = file;
//        spriteMapParam = new SpriteMapParam();
        spriteProject = new SpriteProject();
        this.activeSpriteMapParamId = this.spriteProject.importSpriteParam(new SpriteMapParam());
        save(file);

        settings.setCurrentWorkingDir(file.getParentFile().getPath());
        settings.setCurrentProjectFile(file.getName());
        saveSettings();
    }

    public void setMapFile(File file) {
        String relative = getCwd().toURI().relativize(file.toURI()).getPath();
        SpriteMapParam p = spriteProject.findSpriteMap(activeSpriteMapParamId);
        p.setSpriteMapFile(relative);
    }

    public void save(File file) {
        JsonUtils.writeToFile(file, spriteProject);
    }

    public File getCwd() {
        return new File(settings.getCurrentWorkingDir());
    }

    public void save() {
        save(currentFile);
    }

    public File getSpriteMapFile() {
        String spriteMapFile = getActiveSpriteMapParam().getSpriteMapFile();
        return spriteMapFile!=null?new File(getCwd(), spriteMapFile):null;
    }

    private SpriteMapParam getActiveSpriteMapParam() {
        return spriteProject.findSpriteMap(activeSpriteMapParamId);
    }

    public boolean isSaved() {
        return settings.getCurrentProjectFile()!=null;
    }

    public List<SpriteParam> getSpriteParams() {
        return getActiveSpriteMapParam().getSprites();
    }

    public long createSprite(int x, int y) {
        System.out.println(x + " "  + y);
        return getActiveSpriteMapParam().createSprite(x, y);
    }

    public long createSprite(SpriteParam newSpriteParam) {
        return getActiveSpriteMapParam().createSprite(newSpriteParam);
    }

    public SpriteParam findSpriteParam(long id) {
        return getActiveSpriteMapParam().findSprite(id);
    }

    public void setAlphaColor(ColorParam colorParam) {
        getActiveSpriteMapParam().setAlphaColor(colorParam);
    }

    public ColorParam getAlphaColor() {
        return getActiveSpriteMapParam().getAlphaColor();
    }

    public void deleteSprite(long id) {
        getActiveSpriteMapParam().deleteSprite(id);
    }

    public void exportSpriteMap(File file) {
        JsonUtils.writeToFile(file, getActiveSpriteMapParam());
    }

    public void setActiveSpriteMapParamId(long activeSpriteMapParamId) {
        this.activeSpriteMapParamId = activeSpriteMapParamId;
    }

    public long getActiveSpriteMapParamId() {
        return activeSpriteMapParamId;
    }

    public List<SpriteMapParam> getSpriteMapParams() {
        return spriteProject.getSpriteMapParams();
    }

    public SpriteMapParam findSpriteMapParam(long id) {
        return spriteProject.findSpriteMap(id);
    }

}
