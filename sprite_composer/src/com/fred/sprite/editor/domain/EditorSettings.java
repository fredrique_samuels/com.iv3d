package com.fred.sprite.editor.domain;

/**
 * Created by fred on 2016/09/09.
 */
public class EditorSettings {
    private String currentProjectFile;
    private String currentWorkingDir;

    public EditorSettings() {
        this.currentProjectFile = null;
        this.currentWorkingDir = System.getProperty("user.dir");
    }

    public String getCurrentProjectFile() {
        return currentProjectFile;
    }

    public void setCurrentProjectFile(String currentProjectFile) {
        this.currentProjectFile = currentProjectFile;
    }

    public String getCurrentWorkingDir() {
        return currentWorkingDir;
    }

    public void setCurrentWorkingDir(String currentWorkingDir) {
        this.currentWorkingDir = currentWorkingDir;
    }
}
