package com.fred.sprite.editor;

import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import javafx.stage.FileChooser;

import java.io.File;

/**
 * Created by fred on 2016/09/09.
 */
@Singleton
public class FilePickerManager {
    private FileChooser fileChooser;
    @Inject private SpriteMapEditorManager editorManager;

    public FilePickerManager() {
        this.fileChooser = new FileChooser();
    }

    private void configureFilePicker(String title){
        fileChooser.setTitle(title);
        fileChooser.setInitialDirectory(editorManager.getCwd());
    }

    public File selectFileForSaving(String title) {
        configureFilePicker(title);
        return fileChooser.showSaveDialog(null);
    }

    public File selectFileForLoading(String title) {
        configureFilePicker(title);
        return fileChooser.showOpenDialog(null);
    }

}
