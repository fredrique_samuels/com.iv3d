package com.fred.sprite.editor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.jfx.control.FxRunner;
import com.jfx.profile.JavaFxUiProfile;
import com.jfx.profile.JavaFxUiProfileBuilder;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.concurrent.Executors;

/**
 * Created by fred on 2016/09/09.
 */
public class SpriteMapEditorMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        //Load .fxml using the Profile builder
        ViewControlProvider viewControlProvider = new ViewControlProvider();

        Injector injector = Guice.createInjector(new CommonInjectorModule(viewControlProvider));
        viewControlProvider.controller = injector.getInstance(MainViewController.class);

        JavaFxUiProfile fxUiProfile = new JavaFxUiProfileBuilder(Executors.newFixedThreadPool(5),
                "/com/fred/sprite/editor/views/main_view.fxml")
                .setJsFile("main_view.js")
                .setController(viewControlProvider.controller)
                .build();
        Parent parent = fxUiProfile.getParent();

        new FxRunner() {

            @Override
            public void run() {

            }
        };

        // Stage and show :D
        stage.setTitle("FXML Welcome");
        stage.setScene(new Scene(parent));
        stage.show();
    }


    private static class ViewControlProvider implements Provider<ViewControl> {

        private MainViewController controller;

        @Override
        public ViewControl get() {
            return controller;
        }
    }

    public static class CommonInjectorModule extends AbstractModule {

        private ViewControlProvider viewControlProvider;

        public CommonInjectorModule(ViewControlProvider viewControlProvider) {
            this.viewControlProvider = viewControlProvider;
        }

        /* (non-Javadoc)
                 * @see com.google.inject.AbstractModule#configure()
                 */
        @Override
        protected void configure() {
            bind(ViewControl.class).toProvider(viewControlProvider);
        }

    }
}
