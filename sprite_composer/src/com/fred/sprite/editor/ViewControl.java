package com.fred.sprite.editor;

import javafx.scene.Node;

import java.io.File;

/**
 * Created by fred on 2016/09/09.
 */
public interface ViewControl {
    void saveProject();

    File selectFileForSaving(String title);
    File selectFileForLoading(String title);
    Node findNodeById(String fxId);
    void setAlphaColor(int x, int y);

    //update view
    void updateSpriteMapImage();
    void refreshView();
    void addSpriteToNav(long id);

    void updateToSelectedSprite(long id);
    void updateSpritePreview();

    void setNewSpriteCenter(int x, int y);

    void setNewSpriteX(int i);

    void setNewSpriteY(int i);

    void setNewSpriteWidth(int i);

    void setNewSpriteHeight(int i);
}
