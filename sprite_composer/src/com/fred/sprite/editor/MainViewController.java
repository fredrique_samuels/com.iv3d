package com.fred.sprite.editor;

import com.fred.sprite.common.SpriteRenderer;
import com.fred.sprite.domain.ColorParam;
import com.fred.sprite.domain.SpriteMapParam;
import com.fred.sprite.domain.SpriteParam;
import com.fred.sprite.editor.domain.SpriteMapEditorManager;
import com.fred.sprite.editor.views.NavItem;
import com.fred.sprite.editor.views.RootNavItem;
import com.fred.sprite.editor.views.SpriteMapRootTreeItem;
import com.fred.sprite.editor.views.SpriteParamNavItem;
import com.fred.sprite.editor.views.actions.edit.LoadSpriteMapImage;
import com.fred.sprite.editor.views.actions.edit.SpriteMapAction;
import com.fred.sprite.editor.views.actions.project.LoadProject;
import com.fred.sprite.editor.views.actions.project.NewProject;
import com.fred.sprite.editor.views.actions.project.SaveProject;
import com.fred.sprite.editor.views.actions.sprite.DrawSpritePreview;
import com.google.inject.Inject;
import com.iv3d.common.core.Point;
import com.iv3d.common.core.Rectangle;
import com.iv3d.common.core.ResourceLoader;
import com.jfx.profile.JavaFxController;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by fred on 2016/09/09.
 */

public class MainViewController extends JavaFxController implements ViewControl {

    public static final String SPRITE_MAP_IMAGE_NODE = "sprite_map_image_pane";
    public static final String SPRITE_MAP_CANVAS_NODE = "sprite_map_canvas_pane";
    private static Logger logger = Logger.getLogger(MainViewController.class);

    private Image spriteMapImage;
    private SpriteParam selectedSpriteParam;

    private SpriteParam newSpriteParam;
    private BufferedImage overlayImage;

    private SpriteMapRootTreeItem spriteMapNavItem;
    private TreeItem<NavItem> spriteNavItem;

    private Spinner<Integer> spriteCxSpinner;
    private Spinner<Integer> spriteCySpinner;
    private Spinner<Integer> spriteXSpinner;
    private Spinner<Integer> spriteYSpinner;
    private Spinner<Integer> spriteWidthSpinner;
    private Spinner<Integer> spriteHeightSpinner;



    @Inject SpriteRenderer spriteRenderer;
    @Inject private FilePickerManager filePickerManager;
    @Inject SpriteMapEditorManager editorManager;

    @Inject private NewProject newProject;
    @Inject private SaveProject saveProject;
    @Inject private LoadProject loadProject;

    @Inject private LoadSpriteMapImage loadSpriteMapImage;
    @Inject private DrawSpritePreview drawSpritePreview;
    @Inject private SpriteMapAction spriteMapAction;

    // project
    public void newProject() {newProject.run();}
    public void saveProject() {saveProject.run();}
    public void loadProject() {loadProject.run();}

    //edit
    public void loadSpriteMapImage() {loadSpriteMapImage.run();}
    public void imageMapClicked(MouseEvent event){
        spriteMapAction.run(event);
    }
    public void createNewSprite(MouseEvent event){
        long id = editorManager.createSprite(newSpriteParam);
        addSpriteToNav(id);
    }




    // View Control
    @Override
    public File selectFileForSaving(String title) {return filePickerManager.selectFileForSaving(title);}
    @Override
    public File selectFileForLoading(String title) {return filePickerManager.selectFileForLoading(title);}

    @Override
    public void setAlphaColor(int x, int y) {

        PixelReader reader = spriteMapImage.getPixelReader();
        int argb = reader.getArgb(x, y);
        int r = (argb >> 16) & 0xFF;
        int g = (argb >> 8) & 0xFF;
        int b = argb & 0xFF;

        editorManager.setAlphaColor(new ColorParam(r, g, b));
        updateViewImageKeepParams();
    }

    @Override
    public void updateSpriteMapImage() {

        newSpriteParam = new SpriteParam();
        newSpriteParam.setCenterX(100);
        newSpriteParam.setCenterY(100);
        newSpriteParam.setX(100-25);
        newSpriteParam.setY(100-25);
        newSpriteParam.setWidth(50);
        newSpriteParam.setHeight(50);

        updateViewImageKeepParams();
    }

    void updateViewImageKeepParams() {
        File spriteMapFile = editorManager.getSpriteMapFile();
        ImageView imageView = (ImageView) findNodeById(SPRITE_MAP_IMAGE_NODE);
        ImageView overlayView = (ImageView) findNodeById(SPRITE_MAP_CANVAS_NODE);
        if(spriteMapFile!=null) {


            String path = spriteMapFile.getPath();
            logger.info("Updating Sprite Map Image " + path);

            Image inputImage = new Image(ResourceLoader.read(path));
            int w = Double.valueOf(inputImage.getWidth()).intValue();
            int h = Double.valueOf(inputImage.getHeight()).intValue();

            ColorParam color = editorManager.getAlphaColor();
            this.spriteMapImage = SpriteMapParam.makeTransparent(inputImage, color);
            imageView.setImage(spriteMapImage);

            overlayImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            overlayView.setImage(SwingFXUtils.toFXImage(overlayImage, null));

        } else {
            imageView.setImage(null);
            overlayImage = null;
            overlayView.setImage(null);
        }
        updateSpriteMapImageOverlay();
        updateSpritePreview();
    }

    @Override
    public void refreshView() {
        updateSpriteMapImage();
        refreshTreeView();
    }

    @Override
    public void addSpriteToNav(long id) {
        SpriteParam p = editorManager.findSpriteParam(id);
        spriteMapNavItem.addSpriteParamToNav(editorManager, p);
    }

    private void refreshTreeView() {
        spriteMapNavItem.refresh(editorManager);
    }

    private void refreshSpriteMapTree(long id, TreeItem<NavItem> spriteMapNavItem) {
        SpriteMapParam smp = editorManager.findSpriteMapParam(id);

        ObservableList<TreeItem<NavItem>> spriteMapNavItemChildren = spriteMapNavItem.getChildren();
        spriteMapNavItemChildren.clear();
        for(TreeItem<NavItem> p : spriteMapNavItem.getChildren()) {
            long itemId = p.getValue().getId();
            if(itemId == id) {
                //do refresh
            }
        }
    }


    @Override
    public void initNode() {
        spriteMapNavItem = new SpriteMapRootTreeItem();//new TreeItem<>(new RootNavItem("Maps"));
        spriteNavItem = new TreeItem<>(new RootNavItem("Sprites"));

        TreeView<NavItem> project_nav = (TreeView<NavItem>) this.findNodeById("project_nav");
        project_nav.setRoot(spriteMapNavItem);
        project_nav.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            TreeItem<NavItem> selectedItem = newValue;
            NavItem value = selectedItem.getValue();
            if(value!=null && value.getType()== NavItem.Type.SPRITE_PARAM) {
                SpriteParam spriteParam = editorManager.findSpriteParam(value.getId());
                if(spriteParam==null) {
                    logger.error("Unable to locate sprite " + value.getId());
                }
                updateToSelectedSpriteMap(spriteParam.getParentId());
                updateToSelectedSprite(value.getId());
            } else if(value!=null && value.getType()== NavItem.Type.SPRITE_MAP) {
                updateToSelectedSpriteMap(value.getId());
            }
        });

        GridPane gridPane = (GridPane) findNodeById("sprite_edit_container");

        this.spriteCxSpinner = createSpinnerNode((observable, oldValue, newValue) -> {setSpriteCx(newValue);}, 1);
        this.spriteCySpinner = createSpinnerNode((observable, oldValue, newValue) -> {setSpriteCy(newValue);}, 1);
        this.spriteXSpinner = createSpinnerNode((observable, oldValue, newValue) -> {setSpriteX(newValue);}, 3);
        this.spriteYSpinner = createSpinnerNode((observable, oldValue, newValue) -> {setSpriteY(newValue);}, 3);
        this.spriteWidthSpinner = createSpinnerNode((observable, oldValue, newValue) -> {setSpriteWidth(newValue);}, 3);
        this.spriteHeightSpinner = createSpinnerNode((observable, oldValue, newValue) -> {setSpriteHeight(newValue);}, 3);

        gridPane.add(spriteCxSpinner, 1, 2);
        gridPane.add(spriteCySpinner, 1, 3);
        gridPane.add(spriteWidthSpinner, 1, 4);
        gridPane.add(spriteHeightSpinner, 1, 5);
        gridPane.add(spriteXSpinner, 1, 6);
        gridPane.add(spriteYSpinner, 1, 7);

    }

    private void updateToSelectedSpriteMap(long id) {
        if(editorManager.getActiveSpriteMapParamId()==id) {
            return;
        }
        editorManager.setActiveSpriteMapParamId(id);
        updateSpriteMapImage();
    }

    private void setSpriteCx(Integer newValue) {
        selectedSpriteParam.setCenterX(newValue);
        updateSpritePreview();
    }

    private void setSpriteCy(Integer newValue) {
        selectedSpriteParam.setCenterY(newValue);
        updateSpritePreview();
    }

    private void setSpriteX(Integer newValue) {
        selectedSpriteParam.setX(newValue);
        updateSpritePreview();
    }

    private void setSpriteY(Integer newValue) {
        selectedSpriteParam.setY(newValue);
        updateSpritePreview();
    }

    private void setSpriteWidth(Integer newValue) {
        selectedSpriteParam.setWidth(newValue);
        updateSpritePreview();
    }

    private void setSpriteHeight(Integer newValue) {
        selectedSpriteParam.setHeight(newValue);
        updateSpritePreview();
    }

    private Spinner<Integer> createSpinnerNode(ChangeListener<Integer> integerChangeListener, int amountToStepBy) {
        Spinner<Integer> cxSpinner = new Spinner<>();
        cxSpinner.setEditable(true);
        cxSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 999999, 50, amountToStepBy));
        cxSpinner.valueProperty().addListener(integerChangeListener);
        return cxSpinner;
    }

    @Override
    public void updateToSelectedSprite(long id) {
        this.selectedSpriteParam = editorManager.findSpriteParam(id);

        if(selectedSpriteParam!=null) {
            ((Label) findNodeById("sprite_id_view")).setText(String.valueOf(selectedSpriteParam.getId()));
            ((TextField) findNodeById("sprite_name_input")).setText(selectedSpriteParam.getName());

            spriteCxSpinner.getValueFactory().setValue(selectedSpriteParam.getCenterX());
            spriteCySpinner.getValueFactory().setValue(selectedSpriteParam.getCenterY());
            spriteXSpinner.getValueFactory().setValue(selectedSpriteParam.getX());
            spriteYSpinner.getValueFactory().setValue(selectedSpriteParam.getY());
            spriteWidthSpinner.getValueFactory().setValue(selectedSpriteParam.getWidth());
            spriteHeightSpinner.getValueFactory().setValue(selectedSpriteParam.getHeight());

            updateSpritePreview();
        }
    }

    public void setSpriteName() {
        selectedSpriteParam.setName(((TextField) findNodeById("sprite_name_input")).getText());
        spriteMapNavItem.refreshSpriteItem(editorManager.getActiveSpriteMapParamId(), selectedSpriteParam);
    }

    public void updateSpriteMapImageOverlay() {
        if(overlayImage==null) return;

        ImageView overlayView = (ImageView) findNodeById(SPRITE_MAP_CANVAS_NODE);

        Graphics2D gc = overlayImage.createGraphics();
//        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setColor(new java.awt.Color(0,0,0,0));
        gc.clearRect(0,0,overlayImage.getWidth(), overlayImage.getHeight());

        //clear
        gc.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR));
        gc.fillRect(0,0,overlayImage.getWidth(), overlayImage.getHeight());

//reset composite
        gc.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));

        gc.setColor(java.awt.Color.RED);
        gc.drawRect(newSpriteParam.getX(),
                newSpriteParam.getY(),
                newSpriteParam.getWidth(),
                newSpriteParam.getHeight());

        gc.setColor(java.awt.Color.MAGENTA);
        gc.drawLine(newSpriteParam.getX(),
                newSpriteParam.getCenterY(),
                newSpriteParam.getX() + newSpriteParam.getWidth(),
                newSpriteParam.getCenterY());
        gc.drawLine(newSpriteParam.getCenterX(),
                newSpriteParam.getY(),
                newSpriteParam.getCenterX(),
                newSpriteParam.getY() + newSpriteParam.getHeight());

        overlayView.setImage(SwingFXUtils.toFXImage(overlayImage, null));
    }

    @Override
    public void updateSpritePreview() {
        drawSpritePreview.run();

        if(selectedSpriteParam==null) {
            return;
        }
        Canvas canvas = (Canvas) findNodeById("sprite_preview_canvas");
        boolean alignBottom = ((CheckBox) findNodeById("sprite_preview_centered_check")).isSelected();
        boolean alignLeft = ((CheckBox) findNodeById("sprite_preview_align_left_check")).isSelected();
        boolean scaleX2 = ((CheckBox) findNodeById("sprite_preview_scale_check")).isSelected();
        boolean showBb = ((CheckBox) findNodeById("sprite_preview_bb_check")).isSelected();
        boolean showGuide = ((CheckBox) findNodeById("sprite_preview_guide_check")).isSelected();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, 600, 600);

        gc.setFill(Color.LIGHTGRAY);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(0);

        gc.fill();
        int width = 600;
        int height = 600;
        int halfWidth = Double.valueOf(width*.5).intValue();
        int halfHeight = Double.valueOf(height*.5).intValue();
        gc.strokeRect(
                0,              //x of the upper left corner
                0,              //y of the upper left corner
                width,    //width of the rectangle
                height);  //height of the rectangle

        gc.setLineWidth(1);



        int x = alignLeft?100:halfWidth;
        int y = alignBottom?width-25:halfWidth;

        if(showGuide) {
            gc.strokeLine(0, y, width, y);
            gc.strokeLine(x, 0, x, height);
        }

        Rectangle d = spriteRenderer.drawOnCanvas(gc, spriteMapImage, selectedSpriteParam,
                SpriteRenderer.createDrawSettings(new Point(x, y)).setScale(scaleX2 ? 2 : 1));

        if(showBb) {
            gc.strokeRect(
                    d.getX(),              //x of the upper left corner
                    d.getY(),              //y of the upper left corner
                    d.getWidth(),    //width of the rectangle
                    d.getHeight());  //height of the rectangle
        }
    }

    @Override
    public void setNewSpriteCenter(int x, int y) {
        newSpriteParam.moveTo(x, y);
        updateSpriteMapImageOverlay();
    }

    @Override
    public void setNewSpriteX(int nx) {
        if(nx>=newSpriteParam.getCenterX()) {
            return;
        }

        int ox = newSpriteParam.getX();
        int ow = newSpriteParam.getWidth();

        int nw = ow + (ox-nx);

        // ox=10 ow=10 nx=5 nw=15
        newSpriteParam.setWidth(nw);
        newSpriteParam.setX(nx);
        updateSpriteMapImageOverlay();
    }

    @Override
    public void setNewSpriteY(int ny) {
        if(ny>=newSpriteParam.getCenterY()) {
            return;
        }

        int oy = newSpriteParam.getY();
        int oh = newSpriteParam.getHeight();
        int nh = oh + (oy-ny);

        newSpriteParam.setHeight(nh);
        newSpriteParam.setY(ny);
        updateSpriteMapImageOverlay();
    }

    @Override
    public void setNewSpriteWidth(int i) {
        int nw = i - newSpriteParam.getX();
        if(nw>0) {
            newSpriteParam.setWidth(nw);
            updateSpriteMapImageOverlay();
        }
    }

    @Override
    public void setNewSpriteHeight(int i) {
        int nw = i - newSpriteParam.getY();
        if(nw>0) {
            newSpriteParam.setHeight(nw);
            updateSpriteMapImageOverlay();
        }
    }

    public void deleteSprite() {
        if(selectedSpriteParam==null) {
            return;
        }

        editorManager.deleteSprite(selectedSpriteParam.getId());
        ObservableList<TreeItem<NavItem>> children = spriteNavItem.getChildren();
        for(TreeItem<NavItem> item : children) {
            if(item.getValue().getId()==selectedSpriteParam.getId()) {
                item.setValue(new SpriteParamNavItem(selectedSpriteParam.getId(), selectedSpriteParam.getName()));
                spriteNavItem.getChildren().remove(item);
                break;
            }
        }
    }

    public void toggleSpritePreviewScale() {
        updateSpritePreview();
    }

    public void toggleSpritePreviewCentered() {
        updateSpritePreview();
    }
    public void toggleSpritePreviewAlignLeft() {
        updateSpritePreview();
    }
    public void toggleSpritePreviewBB() {
        updateSpritePreview();
    }
    public void toggleSpritePreviewGuide() {
        updateSpritePreview();
    }

    public void exportSprites() {
        File file = selectFileForSaving("Select sprite export file");
        if(file!=null) {
            editorManager.exportSpriteMap(file);
        }
    }

}