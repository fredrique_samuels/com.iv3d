package com.fred.sprite.bleach.factories.modifiers;

import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteScene;
import com.iv3d.common.core.Callback2Arg;

/**
 * Created by fred on 2017/08/24.
 */
public class FallMove implements Callback2Arg<Sprite, SpriteScene> {


    @Override
    public void invoke(Sprite s, SpriteScene ss) {
        int y = Math.max(s.getOrientation().getLocation().getY() -2 , 0);

        s.setHeight(y<0?0:y);

        if(y<=0) {
            s.getState().markDone();
        }
    }
}
