package com.fred.sprite.bleach.factories.modifiers;

import com.fred.sprite.scene.Direction;
import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteScene;
import com.iv3d.common.core.Callback2Arg;

/**
 * Created by fred on 2017/08/24.
 */
public class AccelMove implements Callback2Arg<Sprite, SpriteScene> {

    private final long frameTime;
    private final int endSpeed;

    public AccelMove(long frameTime, int endSpeed) {
        this.frameTime = frameTime;
        this.endSpeed = endSpeed;
    }

    @Override
    public void invoke(Sprite s, SpriteScene ss) {
        int frame =  Double.valueOf( (s.getTimeInState() * 1.0) / frameTime ).intValue();
        Direction direction = s.getDirection();
        if(frame >= endSpeed) return;

        int x = s.getLocation().getX() + frame*direction.getDirectionSign();
        s.setLocationX(x);
    }
}
