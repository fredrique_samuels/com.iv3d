package com.fred.sprite.bleach.factories.modifiers;

import com.fred.sprite.bleach.sprites.DamageSprite;
import com.fred.sprite.common.SpriteFactory;
import com.fred.sprite.scene.SpriteType;

/**
 * Created by fred on 2016/09/13.
 */
public class DamageFactory implements SpriteFactory {

    @Override
    public DamageSprite create() {
        DamageSprite sprite = new DamageSprite();
        sprite.setType(SpriteType.DAMAGE);
        return sprite;
    }

}
