package com.fred.sprite.bleach.factories.modifiers;

import com.fred.sprite.scene.Direction;
import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteScene;
import com.iv3d.common.core.Callback2Arg;

/**
 * Created by fred on 2017/08/24.
 */
public class DampMove implements Callback2Arg<Sprite, SpriteScene> {

    private final long frameTime;
    private final int startSpeed;

    public DampMove(long frameTime, int startSpeed) {
        this.frameTime = frameTime;
        this.startSpeed = startSpeed;
    }

    @Override
    public void invoke(Sprite s, SpriteScene ss) {
        int frame =  Double.valueOf( (s.getTimeInState() * 1.0) / frameTime ).intValue();
        Direction direction = s.getDirection();
        int diff = startSpeed - frame;
        if(diff <=0 ) return;

        int x = s.getLocation().getX() + diff *direction.getDirectionSign();
        s.setLocationX(x);
    }
}
