package com.fred.sprite.bleach.factories.states;

import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.scene.SpriteScene;
import com.fred.sprite.scene.StateType;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;

import java.util.List;

/**
 * Created by fred on 2017/08/22.
 */
public class BaseDeathState extends BaseSpriteState {
    private long fadeTime = 1000;

    public BaseDeathState(List<StateFrame> frames, SpriteMapConfig smc) {
        super(StateType.DETACHED, frames, smc);
        setLoop(false);
    }

    @Override
    public void update(Sprite sprite, SpriteScene spriteScene) {
        super.update(sprite, spriteScene);
        long timeInState = sprite.getTimeInState();
        System.out.println(timeInState + " " + fadeTime);
        if(timeInState>=fadeTime) {
            spriteScene.removeSprite(sprite);
        } else {
            double opacity = 1.0 - (Double.valueOf(timeInState) / Double.valueOf(fadeTime));
            sprite.setOpacity(opacity);
        }
    }
}
