package com.fred.sprite.bleach.factories;

/**
 * Created by fred on 2016/09/24.
 */
public class FactoryConstants {
    public static final int WALK_SPEED_V=3;
    public static final int WALK_SPEED_H=3;
    public static final int RUN_SPEED_V=7;
    public static final int RUN_SPEED_H=7;
    public static final int WEAK_ATTACK_SPEED=3;

    public static final long DEFAULT_FRAME_TIME = 80;
    public static final long DEATH_FRAME_TIME = 5000;
}
