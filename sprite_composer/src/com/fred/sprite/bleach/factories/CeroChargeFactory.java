package com.fred.sprite.bleach.factories;

import com.fred.sprite.common.SpriteFactory;
import com.fred.sprite.scene.*;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;

import java.util.Arrays;
import java.util.List;

/**
 * Created by fred on 2016/09/11.
 */
public class CeroChargeFactory implements SpriteFactory {
    private SpriteMapConfig smc;

    public CeroChargeFactory(SpriteMapConfig spriteMapConfig) {
        this.smc = spriteMapConfig;
    }

    @Override
    public Sprite create() {
        Sprite sprite = new Sprite();

        List<StateFrame> frames = StateFrame.createForIds(Arrays.asList(1L, 2L, 3L, 4L), 100);
        BaseSpriteState abstractSpriteState = new BaseSpriteState(StateType.NOOP, frames, smc);
        sprite.setState(abstractSpriteState);
        sprite.setOpacity(.7);
        sprite.setType(SpriteType.EFFECT);
        return sprite;
    }
}
