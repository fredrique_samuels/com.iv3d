package com.fred.sprite.bleach.factories;

import com.fred.sprite.common.SpriteFactory;
import com.fred.sprite.scene.*;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;
import com.iv3d.common.core.PointDouble;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/09/11.
 */
public class CeroLevelXFactory implements SpriteFactory {
    private final SpriteMapConfig smc;

    public CeroLevelXFactory(SpriteMapConfig spriteMapConfig) {
        this.smc = spriteMapConfig;
    }

    @Override
    public Sprite create() {
        Sprite sprite = new Sprite();


        List<Long> ids = new ArrayList<>();

        for (long i=1L;i<20L;i++) {
            ids.add(i);
        }

        List<StateFrame> frames = StateFrame.createForIds(ids, 50);
        BaseSpriteState state = new BaseSpriteState(StateType.NOOP, frames, smc);
        state.setLoop(false);
        state.setOnDone((s, ss) -> ss.removeSprite(sprite));
        sprite.setState(state);
        sprite.setOpacity(.8);
        sprite.setScale(new PointDouble(1.4, 1.0));
        sprite.setType(SpriteType.EFFECT);
        return sprite;
    }
}
