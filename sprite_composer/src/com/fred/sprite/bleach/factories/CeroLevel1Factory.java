package com.fred.sprite.bleach.factories;

import com.fred.sprite.bleach.factories.modifiers.DamageFactory;
import com.fred.sprite.bleach.sprites.DamageSprite;
import com.fred.sprite.common.SpriteFactory;
import com.fred.sprite.scene.*;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;
import com.iv3d.common.core.PointDouble;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/09/11.
 */
public class CeroLevel1Factory implements SpriteFactory {
    private final SpriteMapConfig smc;

    public CeroLevel1Factory(SpriteMapConfig spriteMapConfig) {
        this.smc = spriteMapConfig;
    }

    @Override
    public Sprite create() {
        Sprite sprite = new Sprite();
        sprite.setType(SpriteType.EFFECT);
        sprite.setBoundingBox(new SpriteBoundingBox(0,-10,500,10, -11, 11));
        List<StateFrame> frames = new ArrayList<>();
        int timeInMilli = 50;

        frames.add(new StateFrame(15L, timeInMilli)
                .setOnShow((s, ss) -> {
                    SpriteOrientation orientation = s.getOrientation();
                    DamageSprite damageSprite = new DamageFactory().create();
                    damageSprite.setOrientation(orientation);
                    damageSprite.setDamage(0);
                    damageSprite.setSourceUnit(s.getSourceUnit());
                    damageSprite.setLifeTimeInMilliSeconds(400);
                    SpriteBoundingBox boundingBox = s.getBoundingBox()
                            .setMaxX(250)
                            .setMinX(-250)
                            .setMaxY(10)
                            .setMinY(-10)
                            .offset(s.getDirection().getDirectionSign() * (250), 0, 0)
                            .padDepth(40);
                    damageSprite.setBoundingBox(boundingBox
                    );
                    ss.addSprite(damageSprite);
                })
        );
        frames.add(new StateFrame(16L, timeInMilli)
        );
        frames.add(new StateFrame(17L, timeInMilli));
        frames.add(new StateFrame(18L, timeInMilli));
        frames.add(new StateFrame(19L, timeInMilli));



        BaseSpriteState state = new BaseSpriteState(StateType.NOOP, frames, smc);
        state.setLoop(false);
        state.setOnDone((s, ss) -> ss.removeSprite(sprite));
        sprite.setState(state);

        sprite.setScale(new PointDouble(1.4, 1.0));
        sprite.setOpacity(.6);
        sprite.setType(SpriteType.EFFECT);

        return sprite;
    }
}
