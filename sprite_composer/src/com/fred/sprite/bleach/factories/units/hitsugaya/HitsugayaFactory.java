package com.fred.sprite.bleach.factories.units.hitsugaya;

import com.fred.sprite.bleach.factories.modifiers.AccelMove;
import com.fred.sprite.bleach.factories.modifiers.DampMove;
import com.fred.sprite.bleach.factories.modifiers.FallMove;
import com.fred.sprite.bleach.factories.states.BaseDeathState;
import com.fred.sprite.bleach.states.*;
import com.fred.sprite.common.SpriteFactory;
import com.fred.sprite.scene.*;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;
import com.iv3d.common.core.Provider;

import java.util.Arrays;
import java.util.List;

import static com.fred.sprite.bleach.factories.FactoryConstants.DEATH_FRAME_TIME;
import static com.fred.sprite.bleach.factories.FactoryConstants.DEFAULT_FRAME_TIME;

/**
 * Created by fred on 2016/09/24.
 */
public class HitsugayaFactory implements SpriteFactory {
    private final SpriteMapConfig smc;


    private static Provider<List<StateFrame>> introStateFramesProvider = () -> StateFrame.createForIds(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L), DEFAULT_FRAME_TIME);
    private static Provider<List<StateFrame>> idleStateFramesProvider = () -> StateFrame.createForIds(Arrays.asList(9L, 10L, 11L, 12L, 13L, 14L), DEFAULT_FRAME_TIME);

    public HitsugayaFactory(SpriteMapConfig smc) {
        this.smc = smc;
    }

    @Override
    public Sprite create() {
        Sprite sprite = new Sprite();
        sprite.setBoundingBox(new SpriteBoundingBox(-20,-65,20,0, -1, 1));
        sprite.setHealthPoints(50);

        sprite.setIntroStateProvider(()->new IntroState(smc));
        sprite.setIdleStateProvider(()-> new IdleState(smc));
        sprite.setFallStateProvider(() -> new IdleAir(smc));
        sprite.setWalkStateProvider(()->new WalkState(smc));
        sprite.setRunStateProvider(()->new RunState(smc));
        sprite.setJumpStateProvider(()->new JumpState(smc));
        sprite.setJumpSmallStateProvider(()->new JumpSmallState(smc));
        sprite.setJumpLargeStateProvider(()->new JumpLargeState(smc));

        sprite.setAttackWeakStateProvider(()->new AttackWeakState(smc));
        sprite.setAttackStrongStateProvider(()->new AttackStrongState(smc));
        sprite.setSpecialAttackAStateProvider(()->new SpecialMaleeAttack(smc));

        sprite.setAirAttackWeakStateProvider(() -> new AirAttackWeakState(smc));
        sprite.setAirAttackStrongStateProvider(() -> new AirAttackStrongState(smc));

        sprite.setDashAttackProvider(()-> new DashAttackState(smc));
        sprite.setDamageStateProvider(()-> {
            if(sprite.inAir()) {
                return new AirDamageState(smc);
            }
            return new BleachDamageState(36, smc);
        });
        sprite.setDeathStateProvider(() -> new BaseDeathState(StateFrame.createForIds(Arrays.asList(45L), 2000), smc));

        sprite.intro();
        return sprite;
    }

    private static class IdleState extends BaseSpriteState {

        public IdleState(SpriteMapConfig smc) {
            super(StateType.IDLE, idleStateFramesProvider.get(),smc);
        }
    }

    private static class IntroState extends BaseSpriteState {

        public IntroState(SpriteMapConfig smc) {
            super(StateType.DETACHED, introStateFramesProvider.get(),smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());
        }
    }

    private static class WalkState extends BleachWalkState {
        public WalkState(SpriteMapConfig smc) {
            super(frames(), smc);
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(17L, 18L, 19L, 20L, 21L, 22L), DEFAULT_FRAME_TIME);
        }
    }

    private static class RunState extends BleachRunState {
        public RunState(SpriteMapConfig smc) {
            super(frames(), smc);
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(23L, 24L, 25L, 26L, 27L, 28L), DEFAULT_FRAME_TIME);
        }
    }

    private static class AttackWeakState extends BaseSpriteState {



        public AttackWeakState(SpriteMapConfig smc) {
            super(StateType.ATTACK, frames(),smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());;
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(54L, 55L, 56L, 57L, 58L, 59L, 60L), DEFAULT_FRAME_TIME);
        }
    }

    private static class AirAttackWeakState extends BaseSpriteState {

        private FallMove fall = new FallMove();

        public AirAttackWeakState(SpriteMapConfig smc) {
            super(StateType.ATTACK, frames(),smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());
            setOnUpdate((s, ss) -> fall.invoke(s, ss));
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(67L, 68L, 69L, 69L, 69L), DEFAULT_FRAME_TIME);
        }
    }

    private static class AirAttackStrongState extends BaseSpriteState {
        private AccelMove accelMove = new AccelMove(DEFAULT_FRAME_TIME, 3);
        private FallMove fall = new FallMove();

        public AirAttackStrongState(SpriteMapConfig smc) {
            super(StateType.ATTACK, frames(),smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());
            setOnUpdate((s, ss) -> {
                accelMove.invoke(s, ss);
                fall.invoke(s, ss);
            });
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(89L, 90L, 91L, 92L, 92L, 92L), DEFAULT_FRAME_TIME);
        }
    }

    private static class AttackStrongState extends BaseSpriteState {

        private AccelMove accelMove = new AccelMove(DEFAULT_FRAME_TIME, 3);

        public AttackStrongState(SpriteMapConfig smc) {
            super(StateType.ATTACK, frames(),smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());
            setOnUpdate((s, ss) -> accelMove.invoke(s, ss));
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForRange(71L, 80L, DEFAULT_FRAME_TIME);
        }
    }

    private static class DashAttackState extends BaseSpriteState {


        private DampMove dampMove;

        public DashAttackState(SpriteMapConfig smc) {
            super(StateType.ATTACK, StateFrame.createForIds(Arrays.asList(93L, 93L, 93L, 93L, 93L, 93L, 93L), DEFAULT_FRAME_TIME),smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());

            dampMove = new DampMove(DEFAULT_FRAME_TIME, 7);
            setOnUpdate((s, ss) -> {
                dampMove.invoke(s, ss);
            });
        }
    }

    private static class SpecialMaleeAttack extends BaseSpriteState {
        private AccelMove accelMove = new AccelMove(DEFAULT_FRAME_TIME, 6);

        public SpecialMaleeAttack(SpriteMapConfig smc) {
            super(StateType.ATTACK, StateFrame.createForRange(80L, 88L, DEFAULT_FRAME_TIME) ,smc);
            setLoop(false);
            setOnDone((s, ss) -> s.idle());
            setOnUpdate((s, ss) -> {
                accelMove.invoke(s, ss);
            });
        }
    }

    private static class AirDamageState extends BaseSpriteState {

        int step = 3;

        public AirDamageState(SpriteMapConfig smc) {
            super(StateType.DETACHED, fallingFrames(), smc);
            this.setOnUpdate((s, ss) -> {
                int height = Math.max(s.getHeight()-step, 0);
                s.setHeight(height);
                step+=1;
                if(height==0) {
                    AirDamageState.this.markDone();
                }
            });

            BaseSpriteState hittingGroundState = new BaseSpriteState(StateType.DETACHED, gettingUpFrames(), smc);
            hittingGroundState.setLoop(false);
            this.setOnDone((s, ss) -> s.setState(hittingGroundState));
        }

        private static List<StateFrame> fallingFrames() {
            return StateFrame.createForIds(Arrays.asList(47L), DEFAULT_FRAME_TIME);
        }

        public List<StateFrame> gettingUpFrames() {
            return StateFrame.createForIds(Arrays.asList(50L, 51L, 52L, 53L), DEFAULT_FRAME_TIME);
        }
    }

    private static class IdleAir extends BaseSpriteState {

        int step = 3;

        public IdleAir(SpriteMapConfig smc) {
            super(StateType.FALL, StateFrame.createForIds(Arrays.asList(30L, 31L, 32L, 33L, 34L), DEFAULT_FRAME_TIME), smc);
            this.setOnUpdate((s, ss) -> {
                int height = Math.max(s.getHeight()-step, 0);
                s.setHeight(height);
                step+=1;
                if(height==0) {
                    IdleAir.this.markDone();
                }
            });

            this.setOnDone((s, ss) -> s.idle());
        }
    }

    private static class JumpState extends BleachJumpState {
        public JumpState(SpriteMapConfig smc) {
            super(smc, 0, startFrames(),
                    airFrames(),
                    landFrames());
        }
    }

    private static class JumpSmallState extends BleachSmallJumpState {
        public JumpSmallState(SpriteMapConfig smc) {
            super(smc, startFrames(),
                    airFrames(),
                    landFrames());
        }
    }

    private static class JumpLargeState extends BleachLargeJumpState {
        public JumpLargeState(SpriteMapConfig smc) {
            super(smc, startFrames(),
                    airFrames(),
                    landFrames());
        }
    }

    private static class FallState extends BleachFallState {
        public FallState(SpriteMapConfig smc, Direction dir) {
            super(smc, airFrames(),
                    landFrames());
        }
    }


    private static class DeathState extends BaseDeathState {
        public DeathState(SpriteMapConfig smc) {
            super(frames(), smc);
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(25L), DEATH_FRAME_TIME);
        }
    }

    private static List<StateFrame> landFrames() {
        return StateFrame.createForIds(Arrays.asList(35L), DEFAULT_FRAME_TIME);
    }

    private static List<StateFrame> airFrames() {
        return StateFrame.createForIds(Arrays.asList(30L, 31L, 32L, 33L, 34L), DEFAULT_FRAME_TIME);
    }

    private static List<StateFrame> startFrames() {
        return StateFrame.createForIds(Arrays.asList(29L), DEFAULT_FRAME_TIME);
    }

}
