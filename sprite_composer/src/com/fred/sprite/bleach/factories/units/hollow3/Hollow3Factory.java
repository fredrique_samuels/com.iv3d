package com.fred.sprite.bleach.factories.units.hollow3;

import com.fred.sprite.bleach.factories.modifiers.DamageFactory;
import com.fred.sprite.bleach.factories.states.BaseDeathState;
import com.fred.sprite.bleach.sprites.DamageSprite;
import com.fred.sprite.bleach.sprites.SpriteFactory;
import com.fred.sprite.bleach.states.BleachDamageState;
import com.fred.sprite.bleach.states.BleachWalkState;
import com.fred.sprite.scene.*;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;
import com.iv3d.common.core.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.fred.sprite.bleach.factories.FactoryConstants.DEFAULT_FRAME_TIME;

/**
 * Created by fred on 2016/09/10.
 */
public class Hollow3Factory implements com.fred.sprite.common.SpriteFactory {
    private SpriteMapConfig smc;

    public Hollow3Factory(SpriteMapConfig smc) {
        this.smc = smc;
    }

    @Override
    public Sprite create() {
        Sprite sprite = new Sprite()
            .setBoundingBox(new SpriteBoundingBox(-25,-50,25,0, -1, 1))
            .setHealthPoints(50)
            .setIdleStateProvider(()->new IdleState(smc))
            .setDeathStateProvider(()->new DeathState(smc))
            .setWalkStateProvider(()->new WalkState(smc))
            .setAttackWeakStateProvider(()->new AttackAState(smc))
//            .setSpecialAttackAStateProvider(()->new SpecialAttackAState(smc))
            .setSpecialAttackAStateProvider(()->new SpecialAttackBState(smc))
            .setDamageStateProvider(()->new DamageState(smc))
            .setDeathStateProvider(()->new DeathState(smc));

        sprite.idle();
        return sprite;
    }

    private static class AttackAState extends BaseSpriteState {
        public AttackAState(SpriteMapConfig smc) {
            super(StateType.ATTACK,frames(),smc);
            setLoop(false);
            setOnDone((s, ss)->s.setState(new IdleState(smc)));
        }

        private static List<StateFrame> frames() {
            long timeInMilli=100;

            List<StateFrame> frames = new ArrayList<>();
            frames.add(new StateFrame(14L, timeInMilli));
            frames.add(new StateFrame(15L, timeInMilli));
            frames.add(new StateFrame(16L, timeInMilli));
            frames.add(new StateFrame(17L, timeInMilli));
            frames.add(new StateFrame(18L, timeInMilli).setOnShow((s, ss) -> {
                DamageSprite damageSprite = new DamageFactory().create();
                damageSprite.setOrientation(s.getOrientation());
                damageSprite.setDamage(25);
                damageSprite.setSourceUnit(s);
                damageSprite.setLifeTimeInMilliSeconds(500);
                damageSprite.setBoundingBox(s.getBoundingBox()
                        .offset(s.getDirection().getDirectionSign()*20, 0, 0)
                        .padDepth(10));
                ss.addSprite(damageSprite);
            }));
            frames.add(new StateFrame(19L, timeInMilli));
            frames.add(new StateFrame(20L, timeInMilli));
            frames.add(new StateFrame(21L, timeInMilli));
            frames.add(new StateFrame(22L, timeInMilli));
            return frames;
        }
    }

    private static class WalkState extends BleachWalkState {
        public WalkState(SpriteMapConfig smc) {
            super(frames(), smc);
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForIds(Arrays.asList(8L, 9L, 10L, 11L, 12L, 13L), 100);
        }
    }

    private static class DamageState extends BleachDamageState {
        public DamageState(SpriteMapConfig smc) {
            super(25L, smc);
        }
    }


    private static class IdleState extends BaseSpriteState {
        public IdleState(SpriteMapConfig smc) {
            super(StateType.IDLE,frames(),smc);
        }

        private static List<StateFrame> frames() {
            return StateFrame.createForRange(1L, 7L, DEFAULT_FRAME_TIME);
        }
    }

    private static class DeathState extends BaseDeathState {
        public DeathState(SpriteMapConfig smc) {
            super(StateFrame.createForIds(Arrays.asList(25L), 2000), smc);
        }
    }

    private static class SpecialAttackAState extends BaseSpriteState {
        private static Sprite ceroChargeSprite;

        public SpecialAttackAState(SpriteMapConfig smc) {
            super(StateType.ATTACK, frames(), smc);

            setLoop(false);
            setOnDone((s, ss)->s.setState(new IdleState(smc)));
            setOnExit((s)->{});
        }

        private static List<StateFrame> frames() {
            int timeInMilli = 100;
            List<StateFrame> frames = new ArrayList<>();

            frames.add(new StateFrame(14L, timeInMilli));
            frames.add(new StateFrame(15L, timeInMilli));
            frames.add(new StateFrame(16L, timeInMilli));
            frames.add(new StateFrame(17L, 600).setOnShow((s, ss)->{
                Point l = s.getLocation();
                int x = l.getX() + 12 * s.getDirection().getDirectionSign();
                int y = 48;
                SpriteOrientation orientation = s.getOrientation().createFrom().setLocation(new Point(x, y)).build();

                ceroChargeSprite = SpriteFactory.CERO_CHARGE.create();
                ceroChargeSprite.setOrientation(orientation);
                ss.addSprite(ceroChargeSprite);
            }));
            frames.add(new StateFrame(21L, 400)
                    .setOnShow((s, ss)->{
                        ss.removeSprite(ceroChargeSprite);

                        int dir = s.getDirection().getDirectionSign();
                        Point l = s.getLocation();

                        int x = l.getX()+24*dir;
                        int y = 24;

                        SpriteOrientation oldOrien = s.getOrientation();
                        SpriteOrientation orientation = oldOrien.createFrom()
                                .setLocation(new Point(x, y))
                                .build();

                        ceroChargeSprite = SpriteFactory.CERO_LEVEL_1_SMALL.create();
                        ceroChargeSprite.setOrientation(orientation);
                        ceroChargeSprite.setSourceUnit(s);
                        ss.addSprite(ceroChargeSprite);
                    })
            );
            frames.add(new StateFrame(22L, timeInMilli));
            return frames;
        }
    }

    private static class SpecialAttackBState extends BaseSpriteState {
        private static Sprite ceroChargeSprite;

        public SpecialAttackBState(SpriteMapConfig smc) {
            super(StateType.ATTACK, frames(), smc);

            setLoop(false);
            setOnDone((s, ss)->s.setState(new IdleState(smc)));
            setOnExit((s)->{});
        }

        private static List<StateFrame> frames() {
            int timeInMilli = 100;
            List<StateFrame> frames = new ArrayList<>();

            frames.add(new StateFrame(14L, timeInMilli));
            frames.add(new StateFrame(15L, timeInMilli));
            frames.add(new StateFrame(16L, timeInMilli));
            frames.add(new StateFrame(17L, 600).setOnShow((s, ss)->{
                Point l = s.getLocation();
                Point p = new Point(l.getX() + 12 * s.getDirection().getDirectionSign(), 48);
                SpriteOrientation orientation = s.getOrientation().createFrom().setLocation(p).build();

                ceroChargeSprite = SpriteFactory.CERO_CHARGE.create();
                ceroChargeSprite.setOrientation(orientation);
                ss.addSprite(ceroChargeSprite);
            }));
            frames.add(new StateFrame(21L, 400)
                    .setOnShow((s, ss)->{
                        ss.removeSprite(ceroChargeSprite);
                        Point l = s.getLocation();
                        int dir = s.getDirection().getDirectionSign();
                        Point p = new Point(l.getX() + 24 * dir, 24);

                        SpriteOrientation origOr = s.getOrientation();
                        SpriteOrientation orientation = origOr
                                .createFrom()
                                .setLocation(p)
                                .setDepth(origOr.getDepth()+5)
                                .build();
                        ceroChargeSprite = SpriteFactory.BEAM_X.create();
                        ceroChargeSprite.setOrientation(orientation);
                        ss.addSprite(ceroChargeSprite);
                    })
            );
            frames.add(new StateFrame(22L, timeInMilli)
                    .setOnShow((s, ss)->{
                        //TODO create damage
                    })
            );
            return frames;
        }
    }
}
