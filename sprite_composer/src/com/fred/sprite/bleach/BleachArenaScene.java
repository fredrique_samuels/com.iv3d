package com.fred.sprite.bleach;

import com.fred.sprite.bleach.control.BleachAIJump;
import com.fred.sprite.bleach.control.BleachPlayerControl;
import com.fred.sprite.bleach.sprites.DamageSprite;
import com.fred.sprite.bleach.sprites.DamageSpriteCollision;
import com.fred.sprite.bleach.sprites.SpriteCollision;
import com.fred.sprite.bleach.sprites.SpriteFactory;
import com.fred.sprite.common.SpriteRenderer;
import com.fred.sprite.scene.*;
import com.iv3d.common.core.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/09/11.
 */
public class BleachArenaScene extends SpriteScene {

    public BleachArenaScene() {
        super();
        setDrawSettingsFactory(new BleachDrawSettingsFactory());
        addTestObjects();
    }

    @Override
    public void update() {
        super.update();
        List<SpriteCollision> collisions = getDamageCollisions();
        collisions.stream().forEach(spriteCollision -> spriteCollision.process());
    }

    private List<SpriteCollision> getDamageCollisions() {
        List<SpriteCollision> collisions = new ArrayList<>();
        forEachSprite(SpriteType.DAMAGE, damageSprite -> {
            SpriteBoundingBox damageBB = getSpriteDrawingBoundingBox(damageSprite);
            forEachSprite(SpriteType.CHARACTER, unit -> {
                SpriteBoundingBox unitBB = getSpriteDrawingBoundingBox(unit);
                boolean collidesWith = damageBB.collidesWith(unitBB);
                boolean notSourceUnit = damageSprite.getSourceUnit() != unit;
                if(collidesWith && notSourceUnit) {
                    collisions.add(new DamageSpriteCollision((DamageSprite) damageSprite, unit));
                }
            });
        });
        return collisions;
    }

    @Override
    public void draw(GraphicsContext gc, double width, double height) {

        gc.save();

        {
            Stop[] stops = new Stop[]{
                    new Stop(0, Color.LIGHTBLUE),
                    new Stop(1, Color.DARKBLUE)
            };
            LinearGradient lg1 = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
            gc.setFill(lg1);
            gc.fillRect(0, 0, width, height);
        }
        {
            Stop[] stops = new Stop[]{
                    new Stop(0, Color.LIGHTBLUE),
                    new Stop(1, Color.BLACK)
            };
            LinearGradient lg1 = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
            gc.setFill(lg1);
            gc.fillRect(0, BleachSceneUtils.DEPTH_MIN- BleachSceneUtils.FLOOR_BORDER, width, BleachSceneUtils.DEPTH_MAX);
        }
        gc.restore();

        {
//            Ellipse ellipse = new Ellipse(100, 100, 50, 100);
//            RadialGradient gradient1 = RadialGradientBuilder.create()
//                    .focusAngle(0)
//                    .focusDistance(.1)
//                    .centerX(80)
//                    .centerY(45)
//                    .radius(120)
//                    .proportional(false)
//                    .cycleMethod(CycleMethod.NO_CYCLE)
//                    .stops(new Stop(0, Color.GRAY), new Stop(1, Color.BLACK))
//                    .build();
//            gc.setFill(gradient1);
        }

//        drawShadows(gc);
        super.draw(gc, width, height);
        drawPowerBars(gc);
        drawDebugInfo(gc);
    }

    private void drawPowerBars(GraphicsContext gc) {
        gc.save();
        gc.setFill(Color.GREEN);
        gc.setGlobalAlpha(.2);
        forEachSprite(sprite -> {
            if(sprite.getType()==SpriteType.CHARACTER) {
                SpriteBoundingBox bb = sprite.getBoundingBox();
                if(bb==null) {
                    return;
                }

                SpriteBoundingBox offset = getSpriteDrawingBoundingBox(sprite);

                int x = offset.getMinX();
                int y = offset.getMinY()-10;
                int w = sprite.getHealthPoints();
                int h = 10;
                gc.fillRect(x, y, w, h);
            }
        });

        gc.restore();
    }

    @Override
    protected void drawView(GraphicsContext gc, SpriteView p, SpriteRenderer.SpriteDrawSettings drawSettings) {
        super.drawView(gc, p, drawSettings);
    }

    private void drawDebugInfo(GraphicsContext gc) {

        gc.save();
        forEachSprite(sprite -> {
            SpriteBoundingBox bb = sprite.getBoundingBox();
            if(bb==null) {
                return;
            }

            if(sprite.getType()==SpriteType.CHARACTER) {
                gc.setFill(Color.LIGHTCYAN);
            } else if(sprite.getType()==SpriteType.DAMAGE) {
                gc.setFill(Color.RED);
            } else {
                return;
            }

            gc.setGlobalAlpha(.3);

            SpriteBoundingBox offset = getSpriteDrawingBoundingBox(sprite);

            int x = offset.getMinX();
            int y = offset.getMinY();
            int w = offset.getWidth();
            int h = offset.getHeight();
            gc.fillRect(x, y, w, h);

        });
        gc.restore();
    }

    private SpriteBoundingBox getSpriteDrawingBoundingBox(Sprite sprite) {
        SpriteBoundingBox bb = sprite.getBoundingBox();
        SpriteRenderer.SpriteDrawSettings spriteDrawSettings = drawSettingsFactory.create(sprite);
        Point l = spriteDrawSettings.getLocation();
        return bb.offset(l.getX(), l.getY(), sprite.getDepth());
    }

    private void drawShadows(GraphicsContext gc) {
        gc.save();
        gc.setFill(Color.GRAY);
        forEachSprite(sprite -> {
            if(sprite.getType()== SpriteType.CHARACTER) {
                gc.fillOval(sprite.getLocation().getX(), sprite.getLocation().getY(), 50, 20);
            }
        });
        gc.restore();
    }

    private void addTestObjects() {

        {
//            Sprite sprite = SpriteFactory.HOLLOW_THREE_GOLD.create();
            Sprite sprite = SpriteFactory.HITSUGAYA.create();
            sprite.setDepth(BleachSceneUtils.DEPTH_MIN);
            sprite.setLocation(new Point(50, 0));

            addSprite(new BleachPlayerControl(sprite));
        }

        {
            Sprite sprite = SpriteFactory.HITSUGAYA.create();
//            Sprite sprite = SpriteFactory.HOLLOW_THREE.create();
//            addSprite(new BleachAIControl(sprite));
            addSprite(new BleachAIJump(sprite));
//            addSprite(sprite);
//
            sprite.setLocation(new Point(500, 0));
            sprite.setDepth(BleachSceneUtils.DEPTH_MIN+100);
            sprite.setDirection(Direction.LEFT);

        }


        {
            Sprite sprite = SpriteFactory.HOLLOW_THREE.create();
//            Sprite sprite = SpriteFactory.HOLLOW_THREE.create();
//            addSprite(new BleachAIControl(sprite));
//            addSprite(new BleachAIJump(sprite));
            addSprite(sprite);

            sprite.setLocation(new Point(200, 0));
            sprite.setDepth(BleachSceneUtils.DEPTH_MIN+100);
            sprite.setDirection(Direction.LEFT);

        }
    }
}
