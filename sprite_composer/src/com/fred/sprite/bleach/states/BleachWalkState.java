package com.fred.sprite.bleach.states;

import com.fred.sprite.bleach.factories.FactoryConstants;
import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.scene.StateType;
import com.fred.sprite.states.StateFrame;

import java.util.List;

/**
 * Created by fred on 2016/09/24.
 */
public class BleachWalkState extends BleachMoveState {
    public BleachWalkState(List<StateFrame> frames, SpriteMapConfig smc) {
        super(frames, smc, StateType.WALK, FactoryConstants.WALK_SPEED_V, FactoryConstants.WALK_SPEED_H);
    }
}
