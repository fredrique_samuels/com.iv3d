package com.fred.sprite.bleach.states;

import com.fred.sprite.scene.*;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;
import com.iv3d.common.core.Point;

import java.util.List;

/**
 * Created by fred on 2016/09/12.
 */
public class BleachMoveState extends BaseSpriteState {
    private int leftRightSpeed;
    private final int topBottomSpeed;

    public BleachMoveState(List<StateFrame> frames, SpriteMapConfig smc,
                           StateType stateType,
                           int leftRightSpeed,
                           int topBottomSpeed) {
        super(stateType, frames, smc);
        this.leftRightSpeed = leftRightSpeed;
        this.topBottomSpeed = topBottomSpeed;
    }

    @Override
    public void update(Sprite sprite, SpriteScene spriteScene) {
        super.update(sprite, spriteScene);

        Direction direction = sprite.getVector();
        Point location = sprite.getLocation();
        int depth = sprite.getDepth();

        int x = location.getX() + leftRightSpeed*direction.getDirectionSign();
        int y = location.getY();
        int d = depth + topBottomSpeed*direction.getTopBottom();

        x = spriteScene.clipX(x);
        y = spriteScene.clipY(y);
        d = spriteScene.clipDepth(d);

        sprite.setLocation(new Point(x, y));
        sprite.setDepth(d);
    }
}
