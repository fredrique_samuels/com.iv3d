package com.fred.sprite.bleach.states;

import com.fred.sprite.bleach.factories.modifiers.FallMove;
import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.scene.StateType;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;

import java.util.Arrays;
import java.util.List;

/**
 * Created by fred on 2016/09/25.
 */
public class BleachFallState extends BaseSpriteState {

    private FallMove fall = new FallMove();

    public BleachFallState(SpriteMapConfig smc,
                           List<StateFrame> airFrames,
                           List<StateFrame> landFrames) {
        super(StateType.JUMP, Arrays.asList(airFrames.get(0)), smc);

        BaseSpriteState landState = new BaseSpriteState(StateType.FALL, landFrames, smc);
        landState.setLoop(false);

        BaseSpriteState airState = new BaseSpriteState(StateType.FALL, airFrames, smc);
        airState.setOnDone((s, ss)->s.setState(landState));
        airState.setOnUpdate((s, ss) -> fall.invoke(s, ss));

        setLoop(false);
        setOnDone((s, ss) -> {
            s.setState(airState);
        });
    }

}
