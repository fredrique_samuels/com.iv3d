package com.fred.sprite.bleach.states;

import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.scene.StateType;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;

import java.util.Arrays;

/**
 * Created by fred on 2016/09/25.
 */
public class BleachDamageState  extends BaseSpriteState {
    public BleachDamageState(long frameId, SpriteMapConfig smc) {
        super(StateType.DETACHED, StateFrame.createForIds(Arrays.asList(frameId), 500), smc);
        setLoop(false);
        setOnDone((s, ss) -> {s.idle();});
    }
}
