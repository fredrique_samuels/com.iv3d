package com.fred.sprite.bleach.states;

import com.fred.sprite.bleach.factories.FactoryConstants;
import com.fred.sprite.scene.Direction;
import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.states.StateFrame;

import java.util.List;

/**
 * Created by fred on 2016/09/25.
 */
public class BleachSmallJumpState extends BleachJumpState {
    public BleachSmallJumpState(SpriteMapConfig smc, List<StateFrame> startFrames, List<StateFrame> airFrames, List<StateFrame> landFrames) {
        super(smc, FactoryConstants.WALK_SPEED_H, startFrames, airFrames, landFrames);
    }
}
