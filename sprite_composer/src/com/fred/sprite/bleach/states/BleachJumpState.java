package com.fred.sprite.bleach.states;

import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.scene.StateType;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.StateFrame;

import java.util.List;

/**
 * Created by fred on 2016/09/25.
 */
public class BleachJumpState extends BaseSpriteState {
    private int speedX =0;
    private int stepY = 13;

    public BleachJumpState(SpriteMapConfig smc,
                           int speed,
                           List<StateFrame> startFrames,
                           List<StateFrame> airFrames,
                           List<StateFrame> landFrames) {
        super(StateType.JUMP, startFrames, smc);
        this.speedX = speed;

        BaseSpriteState landState = new BaseSpriteState(StateType.JUMP, landFrames, smc);
        landState.setLoop(false);
        landState.setOnUpdate((s, ss) -> {
            updateX(s);
        });
        landState.setOnDone((s, ss) -> s.idle());

        BaseSpriteState airState = new BaseSpriteState(StateType.JUMP, airFrames, smc);
        airState.setOnDone((s, ss)-> s.setState(landState));
        airState.setOnUpdate((s, ss) -> {
            updateX(s);

            int y = Math.max(s.getOrientation().getLocation().getY() + stepY, 0);

            s.setHeight(y);

            if(y==0) {
                airState.markDone();
                this.speedX =0;
            }
            stepY -=1;
        });

        setLoop(false);
        setOnUpdate((s, ss) -> {
            updateX(s);
        });
        setOnDone((s, ss) -> {
            s.setState(airState);
        });
    }

    private void updateX(Sprite s) {
        int directionSign = s.getDirection().getDirectionSign();
        int x = s.getOrientation().getLocation().getX()+ (speedX* directionSign);
        s.setLocationX(x);
    }
}
