package com.fred.sprite.bleach.states;

import com.fred.sprite.bleach.factories.FactoryConstants;
import com.fred.sprite.scene.SpriteMapConfig;
import com.fred.sprite.scene.StateType;
import com.fred.sprite.states.StateFrame;

import java.util.List;

/**
 * Created by fred on 2016/09/24.
 */
public class BleachRunState extends BleachMoveState {
    public BleachRunState(List<StateFrame> frames, SpriteMapConfig smc) {
        super(frames, smc, StateType.RUN, FactoryConstants.RUN_SPEED_V, FactoryConstants.RUN_SPEED_H);
    }
}
