package com.fred.sprite.bleach;

import com.fred.sprite.scene.Sprite;
import com.iv3d.common.core.Point;

/**
 * Created by fred on 2016/09/11.
 */
public class BleachSceneUtils {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final int FLOOR_DEPTH = 280;
    public static final int FLOOR_BORDER = 30;
    public static final int DEPTH_MIN = HEIGHT-FLOOR_DEPTH+FLOOR_BORDER;
    public static final int DEPTH_MAX = HEIGHT-FLOOR_BORDER;

    public static Point getSpriteScreenLocation(Sprite s) {
        Point location = s.getLocation();
        int y = s.getDepth() - location.getY();
        return new Point(location.getX(), y);
    }

}
