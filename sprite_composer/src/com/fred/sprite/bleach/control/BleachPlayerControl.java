package com.fred.sprite.bleach.control;

import com.fred.sprite.scene.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/09/10.
 */
public class BleachPlayerControl extends Sprite {

    private final Sprite controlledSprite;
    private KeyCache keyCache;
    private String commandLog = "";

    public BleachPlayerControl(Sprite controlledSprite) {
        super();
        this.controlledSprite = controlledSprite;
        keyCache = new KeyCache();
    }

    @Override
    public Sprite getSprite() {
        return controlledSprite.getSprite();
    }

    @Override
    public void update(SpriteScene spriteScene) {
        super.update(spriteScene);
        Sprite sprite = getSprite();
        StateType stateType = sprite.getStateType();

        String RIGHT_COMMAND = "D";
        String LEFT_COMMAND = "A";
        String UP_COMMAND = "W";
        String DOWN_COMMAND = "S";

        String RUN_COMMAND = "O";

        String JUMP_COMMAND = "SPACE";
        String ATTACK_WEAK_COMMAND = "J";
        String ATTACK_STRONG_COMMAND = "K";
        String ATTACK_SP_COMMAND = "L";

        String TOGGLE_SPECIAL_COMMAND = "U";

        Command moveCommand = Command.STOP;
        if(spriteScene.isKeyActive(RIGHT_COMMAND)) {
            moveCommand = Command.MOVE_RIGHT;
        } else if(spriteScene.isKeyActive(LEFT_COMMAND)) {
            moveCommand = Command.MOVE_LEFT;
        }

        if(spriteScene.isKeyActive(UP_COMMAND)) {
            if(moveCommand==Command.MOVE_RIGHT) {
                moveCommand=Command.MOVE_RIGHT_UP;
            } else if(moveCommand==Command.MOVE_LEFT) {
                moveCommand=Command.MOVE_LEFT_UP;
            } else {
                moveCommand = Command.MOVE_UP;
            }
        } else if(spriteScene.isKeyActive(DOWN_COMMAND)) {
            if(moveCommand==Command.MOVE_RIGHT) {
                moveCommand=Command.MOVE_RIGHT_DOWN;
            } else if(moveCommand==Command.MOVE_LEFT) {
                moveCommand=Command.MOVE_LEFT_DOWN;
            } else {
                moveCommand = Command.MOVE_DOWN;
            }
        }

        List<Command> commandsToProcess = new ArrayList<>();
        commandsToProcess.add(moveCommand);

        if(spriteScene.isKeyActive(RUN_COMMAND)) {
            commandsToProcess.add(Command.RUN);
        } else if(moveCommand!=Command.STOP) {
            commandsToProcess.add(Command.WALK);
        }

        if(spriteScene.isPressed(TOGGLE_SPECIAL_COMMAND)) {
            commandsToProcess.add(Command.TOGGLE_SPECIAL);
        }

        if(spriteScene.isPressed(JUMP_COMMAND)) {
            commandsToProcess.add(Command.JUMP);
        } else if(spriteScene.isPressed(ATTACK_WEAK_COMMAND)) {
            commandsToProcess.add(Command.ATTACK_WEAK);
        } else if(spriteScene.isPressed(ATTACK_STRONG_COMMAND)) {
            commandsToProcess.add(Command.ATTACK_STRONG);
        } else if(spriteScene.isPressed(ATTACK_SP_COMMAND)) {
            commandsToProcess.add(Command.SPECIAL_ATTACK);
        }

        String s = commandsToProcess.toString();
        if(!s.equals(commandLog)) {
            commandLog = s;
            System.out.println(commandLog + " " + stateType);
        }
        stateType.processCommands(sprite, commandsToProcess.toArray(new Command[0]));

    }

    private void fall(Sprite sprite) {
        sprite.fall();
    }

    private void run(Sprite sprite, Direction direction) {
        sprite.setVector(direction);
        sprite.run();
    }

    private void specialAttackA(Sprite sprite) {
        sprite.specialAttackA();
    }

    private void idle(Sprite sprite) {
        sprite.idle();
    }

    private void noOp(Sprite sprite) {
        sprite.noOp();
    }

    private void walk(Sprite sprite, Direction direction) {
        sprite.setVector(direction);
        sprite.walk();
    }

    private void attackA(Sprite sprite) {
        sprite.attackWeak();
    }

    private void attackB(Sprite sprite) {
        sprite.attackStrong();
    }

    private void attackSpecial(Sprite sprite) {
        sprite.specialAttackA();
    }
}

