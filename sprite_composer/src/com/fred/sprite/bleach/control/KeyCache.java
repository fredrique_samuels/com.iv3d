package com.fred.sprite.bleach.control;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/09/25.
 */
public class KeyCache {
    private int timePerKey = 400;
    private List<KeyEntry> cache = new ArrayList<>();
    private int maxSize = 5;

    public void add(String key) {
        cache.add(new KeyEntry(System.currentTimeMillis(), key));
        if(cache.size()>maxSize) {
            cache.remove(0);
        }
    }

    @Override
    public String toString() {
        return cache.toString();
    }

    public boolean hasSequence(String s) {

        int comboLength = s.length();
        if(cache.size() <= 1 || cache.size()<comboLength) {
            return false;
        }

        List<String> strings = cache.stream().map(k -> k.key).collect(Collectors.toList());
        boolean sequence = String.join("", strings).endsWith(s);
        if(!sequence) return false;

        long time = System.currentTimeMillis() - cache.get(cache.size()-s.length()).time;
        boolean comboEntryTimeCorrect = time <= (comboLength-1) * timePerKey;
        return comboEntryTimeCorrect;
    }

    public void clear() {
        cache.clear();
    }

    private class KeyEntry {
        long time;
        String key;

        public KeyEntry(long time, String key) {
            this.time = time;
            this.key = key;
        }
    }
}
