package com.fred.sprite.bleach.control;

import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteScene;
import com.fred.sprite.scene.SpriteState;
import com.fred.sprite.scene.StateType;

/**
 * Created by fred on 2016/09/16.
 */
public class BleachAIJump extends Sprite {


    private final Sprite controlledSprite;

    public BleachAIJump(Sprite controlledSprite) {
        super();
        this.controlledSprite = controlledSprite;
    }

    @Override
    public Sprite getSprite() {
        return controlledSprite.getSprite();
    }

    @Override
    public void update(SpriteScene spriteScene) {
        super.update(spriteScene);
        Sprite sprite = getSprite();
        StateType stateType = sprite.getStateType();

        if(stateType == StateType.DETACHED)
            return;
        if(stateType == StateType.NOOP) {
            getSprite().idle();
        } else if(stateType == StateType.IDLE) {
            getSprite().jump();
        }
    }
}
