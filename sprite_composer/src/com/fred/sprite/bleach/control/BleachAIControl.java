package com.fred.sprite.bleach.control;

import com.fred.sprite.scene.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by fred on 2016/09/16.
 */
public class BleachAIControl extends Sprite {

    private long wanderTime = 1000;

    private final Sprite controlledSprite;

    public BleachAIControl(Sprite controlledSprite) {
        super();
        this.controlledSprite = controlledSprite;
    }

    @Override
    public Sprite getSprite() {
        return controlledSprite.getSprite();
    }

    @Override
    public void update(SpriteScene spriteScene) {
        super.update(spriteScene);
        Sprite sprite = getSprite();
        StateType stateType = sprite.getStateType();

        List<StateType> openStateTypes = Arrays.asList(StateType.IDLE, StateType.WALK);
        if(stateType == StateType.NOOP) {
            idle();
        } else if(stateType == StateType.IDLE) {
            setVector(Direction.random());
            walk();
        } else if(stateType == StateType.WALK && sprite.getTimeInState()>wanderTime) {
            idle();
        }

    }
}
