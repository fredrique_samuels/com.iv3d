package com.fred.sprite.bleach.sprites;

import com.fred.sprite.scene.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/09/14.
 */
public class DamageSprite extends Sprite {
    private final long timeStart;
    private int damage=0;
    private long lifeTimeInMilliSeconds = 0;
    private List<Long> damagedInits = new ArrayList<>();

    public DamageSprite() {
        super();
        setType(SpriteType.DAMAGE);
        this.timeStart = System.currentTimeMillis();
    }

    @Override
    public void update(SpriteScene spriteScene) {
        super.update(spriteScene);
        if(System.currentTimeMillis()-timeStart>lifeTimeInMilliSeconds) {
            spriteScene.removeSprite(this);
        }
    }

    public void damageUnit(Sprite unit) {
        StateType stateStateType = unit.getStateType();
        int healthPoints = unit.getHealthPoints();
        if(stateStateType == StateType.DETACHED || healthPoints<=0) {
            return;
        }

        unit.getStateType().processCommands(unit, Command.DAMAGE);

        boolean preventDoubleDamage = this.damagedInits.contains(unit.getId());
        if(preventDoubleDamage) {
            return;
        }

        if(healthPoints<=damage) {
            healthPoints=0;
            unit.death();
        } else {
            healthPoints-=damage;
            unit.damage();
        }
        unit.setHealthPoints(healthPoints);
        this.damagedInits.add(unit.getId());
    }

    public void setLifeTimeInMilliSeconds(long lifeTimeInMilliSeconds) {
        this.lifeTimeInMilliSeconds = lifeTimeInMilliSeconds;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
