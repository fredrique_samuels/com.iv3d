package com.fred.sprite.bleach.sprites;


import com.fred.sprite.bleach.factories.CeroChargeFactory;
import com.fred.sprite.bleach.factories.CeroLevel1Factory;
import com.fred.sprite.bleach.factories.CeroLevelXFactory;
import com.fred.sprite.bleach.factories.units.hitsugaya.HitsugayaFactory;
import com.fred.sprite.bleach.factories.units.hollow3.Hollow3Factory;
import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteMapConfig;

public enum SpriteFactory {
        HOLLOW_THREE(new Hollow3Factory(new SpriteMapConfig("bleach/hollow3.sprite.json"))),
        HOLLOW_THREE_GOLD(new Hollow3Factory(new SpriteMapConfig("bleach/hollow3_gold.sprite.json"))),
        HITSUGAYA(new HitsugayaFactory(new SpriteProjectConfig("bleach/hitsugaya.sprite.json").findSpriteMap(2))),

        CERO_CHARGE(new CeroChargeFactory(new SpriteMapConfig("bleach/cero_charge.sprite.json"))),
        CERO_LEVEL_1_SMALL(new CeroLevel1Factory(new SpriteMapConfig("bleach/beam_solid_red.sprite.json"))),
//        CERO_X(new CeroLevelXFactory(new SpriteMapConfig("bleach/beam_solid_red.sprite.json"))),
        BEAM_X(new CeroLevelXFactory(new SpriteMapConfig("bleach/beam1.sprite.json")))
        ;

        private com.fred.sprite.common.SpriteFactory spriteFactory;

        SpriteFactory(com.fred.sprite.common.SpriteFactory spriteFactory) {
            this.spriteFactory = spriteFactory;
        }

        public Sprite create() {
            return spriteFactory.create();
        }
    }