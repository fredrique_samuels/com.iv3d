package com.fred.sprite.bleach.sprites;

/**
 * Created by fred on 2016/09/14.
 */
public interface SpriteCollision {
    void process();
}
