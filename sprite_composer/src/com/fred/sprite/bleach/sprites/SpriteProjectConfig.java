package com.fred.sprite.bleach.sprites;

import com.fred.sprite.domain.SpriteMapParam;
import com.fred.sprite.domain.SpriteProject;
import com.fred.sprite.scene.SpriteMapConfig;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fred on 2016/09/24.
 */
public class SpriteProjectConfig {
    private final SpriteProject project;
    private final File parentFolder;
    private Map<Long, SpriteMapConfig> configs = new HashMap<>();

    public SpriteProjectConfig(String file) {
        this.parentFolder = new File(file).getParentFile();
        this.project = SpriteProject.load(file);
    }

    public SpriteMapConfig findSpriteMap(long i) {
        if(configs.containsKey(i)) {
            return configs.get(i);
        }
        return populateCache(i);
    }

    private SpriteMapConfig populateCache(long i) {
        SpriteMapParam spriteMap = project.findSpriteMap(i);
        if(spriteMap==null)
            return null;

        SpriteMapConfig config = new SpriteMapConfig(spriteMap, parentFolder);
        configs.put(i, config);
        return config;
    }
}
