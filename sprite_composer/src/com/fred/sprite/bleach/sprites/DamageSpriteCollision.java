package com.fred.sprite.bleach.sprites;

import com.fred.sprite.scene.Sprite;

/**
 * Created by fred on 2016/09/14.
 */
public class DamageSpriteCollision implements SpriteCollision {
    private final DamageSprite damageSprite;
    private final Sprite unit;

    public DamageSpriteCollision(DamageSprite damageSprite, Sprite unit) {
        super();
        this.damageSprite = damageSprite;
        this.unit = unit;
    }

    public void process() {
        damageSprite.damageUnit(unit);
    }
}
