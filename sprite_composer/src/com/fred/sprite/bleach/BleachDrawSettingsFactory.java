package com.fred.sprite.bleach;

import com.fred.sprite.common.SpriteRenderer;
import com.fred.sprite.scene.DrawSettingsFactory;
import com.fred.sprite.scene.Sprite;
import com.iv3d.common.core.Point;

/**
 * Created by fred on 2016/09/12.
 */
public class BleachDrawSettingsFactory extends DrawSettingsFactory {
    public BleachDrawSettingsFactory() {
        super();
    }

    @Override
    public SpriteRenderer.SpriteDrawSettings create(Sprite s) {
        SpriteRenderer.SpriteDrawSettings ds = super.create(s);
        Point pv = BleachSceneUtils.getSpriteScreenLocation(s);
        ds.setLocation(pv);
        return ds;
    }
}
