package com.fred.sprite.common;

import com.fred.sprite.scene.SpriteScene;
import com.iv3d.common.core.ThreadUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class SpriteSceneUpdater extends Thread {
    private final Canvas canvas;
    private final SpriteScene spriteScene;
    private boolean terminateReq = false;

    public SpriteSceneUpdater(Canvas canvas, SpriteScene spriteScene) {
        this.canvas = canvas;
        this.spriteScene = spriteScene;
    }

    public final void terminate() {
        terminateReq = true;
    }

    @Override
    public void run() {
        while (true) {

            GraphicsContext gc = canvas.getGraphicsContext2D();

            spriteScene.update();
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            spriteScene.draw(gc, canvas.getWidth(), canvas.getHeight());
            if(terminateReq)
                break;

            ThreadUtils.delay(33);
        }
    }
}