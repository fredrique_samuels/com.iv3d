package com.fred.sprite.common;

import com.fred.sprite.scene.Sprite;

/**
 * Created by fred on 2016/09/11.
 */
public interface SpriteFactory {
    Sprite create();
}
