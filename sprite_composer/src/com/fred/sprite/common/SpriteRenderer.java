package com.fred.sprite.common;

import com.fred.sprite.domain.SpriteParam;
import com.iv3d.common.core.Point;
import com.iv3d.common.core.PointDouble;
import com.iv3d.common.core.Rectangle;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Created by fred on 2016/09/10.
 */
public class SpriteRenderer {

    public static SpriteDrawSettings createDrawSettings(Point l) {
        return new SpriteDrawSettings(l);
    }

    public Rectangle drawOnCanvas(GraphicsContext gc, Image spriteMap, SpriteParam selectedSpriteParam, SpriteDrawSettings drawSettings) {
        int cx = selectedSpriteParam.getCenterX();
        int cy = selectedSpriteParam.getCenterY();
        int sx = selectedSpriteParam.getX();
        int sy = selectedSpriteParam.getY();
        int sw = selectedSpriteParam.getWidth();
        int sh = selectedSpriteParam.getHeight();

        double scaleX = drawSettings.scale.getX();
        double scaleY = drawSettings.scale.getY();

        boolean flipOnY = drawSettings.flipOnY();
        int centerOffset = Double.valueOf((sx - cx) * scaleX).intValue();
        if (flipOnY) {
            centerOffset = -Double.valueOf((sx - cx) * scaleX).intValue();
        }

        Point location = drawSettings.getLocation();
        int dx = location.getX() + centerOffset;
        int dy = Double.valueOf(location.getY() + (sy - cy) * scaleY).intValue();
        int dw = Double.valueOf((sw * scaleX) * (flipOnY ? -1 : 1)).intValue();
        int dh = Double.valueOf(sh * scaleY).intValue();

        gc.save();
        gc.setGlobalAlpha(drawSettings.getOpacity());
        gc.drawImage(spriteMap, sx, sy, sw, sh,
                dx, dy, dw, dh);
        gc.restore();

        return new Rectangle(dx, dy, dw, dh);
    }

    public static class SpriteDrawSettings {
        private PointDouble scale = new PointDouble(1,1);
        private boolean flipOnY = false;
        private double opacity=1;
        private Point location;

        private SpriteDrawSettings(Point location) {
            this.location = location;
        }

        public void setLocation(Point location) {
            this.location = location;
        }

        public Point getLocation() {
            return location;
        }

        public SpriteDrawSettings setScale(int scale) {
            this.scale = new PointDouble(scale, scale);
            return this;
        }

        public SpriteDrawSettings setScaleX(double scaleX) {
            this.scale = new PointDouble(scaleX, scale.getY());
            return this;
        }

        public SpriteDrawSettings setScaleY(double scaleY) {
            this.scale = new PointDouble(scale.getX(), scaleY);
            return this;
        }

        public void setFlipOnY(boolean flipOnY) {
            this.flipOnY = flipOnY;
        }

        public boolean flipOnY() {
            return flipOnY;
        }

        public double getOpacity() {
            return opacity;
        }

        public SpriteDrawSettings setOpacity(double opacity) {
            this.opacity = opacity;
            return this;
        }

        public void setScale(PointDouble scale) {
            this.scale = scale;
        }
    }
}
