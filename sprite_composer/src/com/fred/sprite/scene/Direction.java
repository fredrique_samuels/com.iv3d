package com.fred.sprite.scene;

import java.util.concurrent.ThreadLocalRandom;

public enum Direction {
        NONE(0,0, false, false, false, false),
        LEFT(-1,0, true, false, false, false),
        RIGHT(1,0, false, true, false, false),
        UP(0,-1, false, false, true, false),
        DOWN(0,1, false, false, false, true),
        LEFT_UP(-1,-1, true, false, true, false),
        LEFT_DOWN(-1,1, true, false, false, true),
        RIGHT_UP(1,-1, false, true, true, false),
        RIGHT_DOWN(1,1, false, true, false, true);

        private final int lr;
        private final int td;
        private final boolean left;
        private final boolean right;
        private final boolean up;
        private final boolean down;

        Direction(int lr, int td,
                  boolean left, boolean right,
                  boolean up, boolean down) {
            this.lr = lr;
            this.td = td;

            this.left = left;
            this.right = right;
            this.up = up;
            this.down = down;
        }

        public int getDirectionSign(){
            return lr;
        }
        public int getTopBottom(){
            return td;
        }

        public boolean isRight() {
            return right;
        }

        public boolean isLeft() {
            return left;
        }

        public boolean isUp() {
            return up;
        }

        public boolean isDown() {
            return down;
        }


    public static Direction random() {
        int i = ThreadLocalRandom.current().nextInt(0, 8);
        return Direction.values()[i];
    }
}