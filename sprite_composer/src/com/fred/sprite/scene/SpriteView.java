package com.fred.sprite.scene;

import com.fred.sprite.domain.SpriteParam;
import javafx.scene.image.Image;

/**
 * Created by fred on 2016/09/10.
 */
public class SpriteView {
    private Image image;
    private SpriteParam params;

    public SpriteView(Image image, SpriteParam params) {
        this.image = image;
        this.params = params;
    }

    public Image getImage() {
        return image;
    }

    public SpriteParam getParams() {
        return params;
    }
}
