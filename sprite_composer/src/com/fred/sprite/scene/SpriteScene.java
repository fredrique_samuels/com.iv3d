package com.fred.sprite.scene;

import com.fred.sprite.bleach.BleachSceneUtils;
import com.fred.sprite.common.SpriteRenderer;
import com.iv3d.common.core.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by fred on 2016/09/10.
 */
public class SpriteScene {
    private long idSeed = 0;
    private List<Sprite> sprites;

    private SortedMap<Integer, RenderPass> renderPass = new TreeMap<>();
    private List<String> pressedKeys = new ArrayList<>();
    private SpriteRenderer spriteRenderer;
    private HashMap<String, Boolean> currentlyActiveKeys = new HashMap<>();
    protected DrawSettingsFactory drawSettingsFactory;
    private long frameId=0;

    public SpriteScene() {
        this.sprites = new ArrayList<>();
        setSpriteRenderer(new SpriteRenderer());
        setDrawSettingsFactory(new DrawSettingsFactory());
    }

    public void setDrawSettingsFactory(DrawSettingsFactory drawSettingsFactory) {
        this.drawSettingsFactory = drawSettingsFactory;
    }

    public void setSpriteRenderer(SpriteRenderer spriteRenderer) {
        this.spriteRenderer = spriteRenderer;
    }

    public void update() {
        frameId++;
        forEachSprite(s -> {
            s.update(this);
            clipSprite(s);
        });
        pressedKeys.clear();
    }

    private void clipSprite(Sprite s) {
        int x = clipX(s.getLocation().getX());
        int y = clipY(s.getLocation().getY());
        int d = clipDepth(s.getDepth());

        s.setLocation(new Point(x, y));
        s.setDepth(d);
    }


    public long getFrameId() {
        return frameId;
    }

    public void draw(GraphicsContext gc, double width, double height) {
        sortSpritesByDepth();
        drawSpritesFromBackToFront(gc);
        renderPass.clear();
    }

    private void drawSpritesFromBackToFront(GraphicsContext gc) {
        renderPass.values().stream().forEach(rp->{
            rp.units.stream().forEach(s -> drawSprite(s.getSprite(), gc));
            rp.effects.stream().forEach(s -> drawSprite(s.getSprite(), gc));
        });
    }

    private void sortSpritesByDepth() {
        forEachSprite(sprite -> {
            int depth = sprite.getDepth();
            if(renderPass.get(depth)==null) {
                renderPass.put(depth, new RenderPass());
            }
            renderPass.get(depth).add(sprite);
        });
    }

    protected final void forEachSprite(SpriteType type, Consumer<Sprite> spriteConsumer) {
        getSpriteStream().filter(sprite -> sprite.getType()==type).forEach(spriteConsumer);
    }

    protected void forEachSprite(Consumer<Sprite> spriteConsumer) {
        getSpriteStream().forEach(spriteConsumer);
    }

    private Stream<Sprite> getSpriteStream() {
        ArrayList<Sprite> localSprites = new ArrayList<>(this.sprites);
        return localSprites.stream();
    }

    private void drawSprite(Sprite s, GraphicsContext gc) {
        List<SpriteView> params = s.getViews();
        SpriteRenderer.SpriteDrawSettings drawSettings = drawSettingsFactory.create(s);
        params.forEach(p->drawView(gc, p, drawSettings));
    }

    protected void drawView(GraphicsContext gc, SpriteView p, SpriteRenderer.SpriteDrawSettings drawSettings) {
        Image image = p.getImage();
        spriteRenderer.drawOnCanvas(gc, image, p.getParams(), drawSettings);
    }

    public void addSprite(Sprite sprite) {
        sprites.add(sprite);
    }

    public void removeKey(String code) {
        currentlyActiveKeys.remove(code);
    }

    public void setKey(String codeString) {
        if (!currentlyActiveKeys.containsKey(codeString)) {
            currentlyActiveKeys.put(codeString, true);
            pressedKeys.add(codeString);
        }
    }

    public void removeSprite(Sprite sprite) {
        sprites.remove(sprite);
    }

    public boolean isKeyActive(String d) {
        return currentlyActiveKeys.containsKey(d);
    }
    public boolean isPressed(String d) {
        return pressedKeys.contains(d);
    }

    public int clipX(int x) {
        int xUpperLimit = BleachSceneUtils.WIDTH - BleachSceneUtils.FLOOR_BORDER;
        int xLowerLimit = BleachSceneUtils.FLOOR_BORDER;

        if(x>xUpperLimit) {
            return xUpperLimit;
        } else if(x< xLowerLimit) {
            return xLowerLimit;
        }
        return x;
    }

    public int clipDepth(int d) {
        int dLowerLimit = BleachSceneUtils.DEPTH_MIN;
        int dUpperLimit = BleachSceneUtils.DEPTH_MAX;
        if(d>dUpperLimit) {
            return dUpperLimit;
        } else if(d< dLowerLimit) {
            return dLowerLimit;
        }
        return d;
    }

    public int clipY(int y) {
        return y <= 0 ? 0 : y;
    }

    private static class RenderPass {
        final List<Sprite> units = new ArrayList<>();
        final List<Sprite> effects = new ArrayList<>();

        public void add(Sprite s) {
            if(s.getType()==SpriteType.CHARACTER) {
                units.add(s);
            } else if(s.getType()==SpriteType.EFFECT) {
                effects.add(s);
            }
        }
    }
}
