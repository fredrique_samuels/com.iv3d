package com.fred.sprite.scene;

import com.fred.sprite.domain.SpriteMapParam;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.utils.JsonUtils;
import javafx.scene.image.Image;

import java.io.File;

/**
 * Created by fred on 2016/09/10.
 */
public class SpriteMapConfig {
    private final SpriteMapParam spriteMapParam;
    private final Image image;

    public SpriteMapConfig(String fileName) {
        File file = new File(fileName);
        this.spriteMapParam = JsonUtils.readFromFile(file, SpriteMapParam.class);
        File parentFile = file.getParentFile();
        Image plainImage = new Image(ResourceLoader.read(new File(parentFile, spriteMapParam.getSpriteMapFile()).getPath()));
        this.image = SpriteMapParam.makeTransparent(plainImage, spriteMapParam.getAlphaColor());
    }

    public SpriteMapConfig(SpriteMapParam spriteMap, File parentFolder) {
        this.spriteMapParam = spriteMap;
        Image plainImage = new Image(ResourceLoader.read(new File(parentFolder, spriteMapParam.getSpriteMapFile()).getPath()));
        this.image = SpriteMapParam.makeTransparent(plainImage, spriteMapParam.getAlphaColor());
    }

    public SpriteMapParam getSpriteMapParam() {
        return spriteMapParam;
    }

    public Image getImage() {
        return image;
    }
}
