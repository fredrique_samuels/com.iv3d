package com.fred.sprite.scene;

/**
 * Created by fred on 2016/09/10.
 */
public class Frame {
    int id;
    long duration;

    public Frame(int id, long duration) {
        this.id = id;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public long getDuration() {
        return duration;
    }
}
