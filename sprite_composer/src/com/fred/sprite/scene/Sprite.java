package com.fred.sprite.scene;

import com.fred.sprite.bleach.factories.states.BaseDeathState;
import com.fred.sprite.states.BaseSpriteState;
import com.fred.sprite.states.NoOpState;
import com.google.inject.Provider;
import com.iv3d.common.core.Point;
import com.iv3d.common.core.PointDouble;

import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2016/09/10.
 */
public class Sprite {
    // base sprite
    private static long idSeed = 0;
    private final long id = ++idSeed;
    private double opacity=1;
    private SpriteType type;
    private SpriteOrientation orientation = new SpriteOrientation();
    private PointDouble scale = new PointDouble(1, 1);
    private SpriteBoundingBox boundingBox = null;
    private Provider<List<SpriteView>> viewProvider;

    //controllable sprite
    private long timeStateSet = 0;
    private Direction vector = Direction.RIGHT;
    private Provider<BaseSpriteState> introStateProvider;
    private Provider<BaseSpriteState> idleStateProvider;
    private Provider<BaseSpriteState> fallStateProvider;
    private Provider<BaseSpriteState> walkStateProvider;
    private Provider<BaseSpriteState> runStateProvider;
    private Provider<BaseSpriteState> jumpStateProvider;
    private Provider<BaseSpriteState> jumpSmallStateProvider;
    private Provider<BaseSpriteState> jumpLargeStateProvider;

    private Provider<BaseSpriteState> attackWeakStateProvider;
    private Provider<BaseSpriteState> attackStrongStateProvider;
    private Provider<BaseSpriteState> specialAttackAStateProvider;

    private Provider<BaseSpriteState> airAttackWeakStateProvider;
    private Provider<BaseSpriteState> airAttackStrongStateProvider;
    private Provider<BaseSpriteState> airSpecialAttackAStateProvider;

    private Provider<BaseSpriteState> damageStateProvider;
    private Provider<BaseDeathState> deathStateProvider;
    private Provider<BaseSpriteState> dashAttackProvider;

    // units sprites
    private int healthPoints=0;
    private int energyPoints=0;
    private Point shadowOffset = new Point(0,0);
    private SpriteState state = new NoOpState();

    //damage sprite
    private Sprite sourceUnit;

    public Sprite() {
        this.type = SpriteType.CHARACTER;
        this.state = new NoOpState();
        this.viewProvider = ()->this.getSprite().state.getViews();
    }

    public SpriteType getType() {
        return getSprite().type;
    }

    public void setType(SpriteType type) {
        getSprite().type = type;
    }

    public StateType getStateType() {
        return getSprite().state.getStateType();
    }

    public double getOpacity() {
        return this.getSprite().opacity;
    }

    public void setOpacity(double opacity) {
        this.getSprite().opacity = opacity;
    }

    public void setScale(PointDouble scale) {
        this.getSprite().scale = scale;
    }

    public final void setLocation(Point p) {
        getSprite().orientation = this.getSprite().orientation.createFrom().setLocation(p).build();
    }

    public Point getLocation() {
        return getSprite().orientation.getLocation();
    }

    public Direction getDirection() {
        return this.getSprite().orientation.getDirection();
    }

    public void update(SpriteScene spriteScene) {
        this.getSprite().state.update(this, spriteScene);
    }

    public void setDirection(Direction direction) {
        getSprite().orientation = this.getSprite().orientation.createFrom().setDirection(direction).build();
    }

    public Direction getVector() {
        return getSprite().vector;
    }

    public void setVector(Direction vector) {
        if(vector.isLeft()) {
            getSprite().setDirection(Direction.LEFT);
        } else if(vector.isRight()) {
            getSprite().setDirection(Direction.RIGHT);
        }
        getSprite().vector = vector;
    }

    public Sprite getSprite() {
        return this;
    }

    public long getId() {
        return this.getSprite().id;
    }

    public List<SpriteView> getViews() {
        if(viewProvider!=null)
            return viewProvider.get();
        else
            return Collections.emptyList();
    }

    public void setState(SpriteState state) {
        Sprite sprite = this.getSprite();
        sprite.timeStateSet = System.currentTimeMillis();
        if(sprite.state!=null) {
            sprite.state.dispatchExit(sprite);
        }
        sprite.state = state;
    }

    public SpriteState getState() {
        return getSprite().state;
    }

    public PointDouble getScale() {
        return getSprite().scale;
    }

    public long getTimeInState() {
        return System.currentTimeMillis()-this.getSprite().timeStateSet;
    }

    public Point getShadowOffset() {
        return getSprite().shadowOffset;
    }

    public Sprite setShadowOffset(Point shadowOffset) {
        this.getSprite().shadowOffset = shadowOffset;
        return this;
    }

    public int getDepth() {
        return getSprite().orientation.getDepth();
    }

    public Sprite setDepth(int depth) {
        getSprite().orientation = this.getSprite().orientation.createFrom().setDepth(depth).build();
        return this;
    }

    public SpriteOrientation getOrientation() {
        return getSprite().orientation;
    }

    public Sprite setOrientation(SpriteOrientation orientation) {
        getSprite().orientation = orientation;
        return this;
    }

    public SpriteBoundingBox getBoundingBox() {
        return getSprite().boundingBox;
    }

    public Sprite setBoundingBox(SpriteBoundingBox boundingBox) {
        getSprite().boundingBox = boundingBox;
        return this;
    }

    public int getHealthPoints() {
        return getSprite().healthPoints;
    }

    public Sprite setHealthPoints(int healthPoints) {
        this.getSprite().healthPoints = healthPoints;
        return this;
    }

    public int getEnergyPoints() {
        return getSprite().energyPoints;
    }

    public Sprite setEnergyPoints(int energyPoints) {
        getSprite().energyPoints = energyPoints;
        return this;
    }

    public Sprite setSourceUnit(Sprite sourceUnit) {
        getSprite().sourceUnit = sourceUnit;
        return this;
    }

    public Sprite getSourceUnit() {
        return getSprite().sourceUnit;
    }

    public boolean damage() {
        return setState(this.getSprite().damageStateProvider);
    }
    public boolean intro() {
        Sprite sprite = this.getSprite();
        if(!sprite.setState(sprite.introStateProvider)) {
            return idle();
        }
        return false;
    }
    public boolean idle() {
        if(inAir()) {
            boolean b = this.getSprite().setState(this.getSprite().fallStateProvider);
            if(!b) return this.getSprite().setState(this.getSprite().idleStateProvider);
            return b;
        } else {
            return this.getSprite().setState(this.getSprite().idleStateProvider);
        }
    }
    public boolean walk() {
        if(this.getStateType()!=StateType.WALK) {
            return setState(this.getSprite().walkStateProvider);
        }
        return true;
    }
    public boolean walk(Direction direction) {
        this.setVector(direction);

        return true;
    }
    public boolean run() {
        if(this.getStateType()!=StateType.RUN) {
            return setState(this.getSprite().runStateProvider);
        }
        return true;
    }
    public boolean run(Direction direction) {
        this.setVector(direction);
        run();
        return true;
    }
    public boolean jump() { return setState(this.getSprite().jumpStateProvider);}
    public boolean jumpSmall() { return setState(this.getSprite().jumpSmallStateProvider);}
    public boolean jumpLarge() { return setState(this.getSprite().jumpLargeStateProvider);}

    public boolean attackWeak() {
        return setState(this.getSprite().attackWeakStateProvider);
    }
    public boolean attackStrong() {
        return setState(this.getSprite().attackStrongStateProvider);
    }
    public boolean specialAttackA() {
        return setState(this.getSprite().specialAttackAStateProvider);
    }

    public boolean dashAttack() {
        return setState(this.getSprite().dashAttackProvider);
    }

    public boolean airAttackWeak() {
        return setState(this.getSprite().airAttackWeakStateProvider);
    }
    public boolean airAttackStrong() {
        return setState(this.getSprite().airAttackStrongStateProvider);
    }
    public boolean airSpecialAttackA() {
        return setState(this.getSprite().airSpecialAttackAStateProvider);
    }


//    public boolean specialAttackB() {
//        return setState(this.getSprite().specialAttackBStateProvider);
//    }
    public boolean death() {
        return setState(getSprite().deathStateProvider);
    }

    private <T extends BaseSpriteState> boolean setState(Provider<T> stateProvider) {
        if(stateProvider !=null) {
            T t = stateProvider.get();
            if(t==null)return false;
            this.getSprite().setState(stateProvider.get());
            return true;
        }

        return false;
    }

    public Sprite setDamageStateProvider(Provider<BaseSpriteState> damageState) {
        this.getSprite().damageStateProvider = damageState;
        return this;
    }

    public Sprite setViewProvider(Provider<List<SpriteView>> viewProvider) {
        this.viewProvider = viewProvider;
        return this;
    }


    public Sprite setIdleStateProvider(Provider<BaseSpriteState> idleStateProvider) {
        this.idleStateProvider = idleStateProvider;
        return this;
    }

    public Sprite setWalkStateProvider(Provider<BaseSpriteState> walkStateProvider) {
        this.walkStateProvider = walkStateProvider;
        return this;
    }

    public Sprite setRunStateProvider(Provider<BaseSpriteState> runStateProvider) {
        this.runStateProvider = runStateProvider;
        return this;
    }

    public Sprite setJumpStateProvider(Provider<BaseSpriteState> jumpStateProvider) {
        this.jumpStateProvider = jumpStateProvider;
        return this;
    }

    public Sprite setJumpSmallStateProvider(Provider<BaseSpriteState> jumpSmallStateProvider) {
        this.jumpSmallStateProvider = jumpSmallStateProvider;
        return this;
    }

    public Sprite setJumpLargeStateProvider(Provider<BaseSpriteState> jumpLargeStateProvider) {
        this.jumpLargeStateProvider = jumpLargeStateProvider;
        return this;
    }

    public Sprite setAirAttackWeakStateProvider(Provider<BaseSpriteState> airAttackWeakStateProvider) {
        this.airAttackWeakStateProvider = airAttackWeakStateProvider;
        return this;
    }

    public Sprite setAirAttackStrongStateProvider(Provider<BaseSpriteState> airAttackStrongStateProvider) {
        this.airAttackStrongStateProvider = airAttackStrongStateProvider;
        return this;
    }

    public Sprite setAirSpecialAttackAStateProvider(Provider<BaseSpriteState> airSpecialAttackAStateProvider) {
        this.airSpecialAttackAStateProvider = airSpecialAttackAStateProvider;
        return this;
    }

    public Sprite setAttackWeakStateProvider(Provider<BaseSpriteState> attackWeakStateProvider) {
        this.attackWeakStateProvider = attackWeakStateProvider;
        return this;
    }

    public Sprite setAttackStrongStateProvider(Provider<BaseSpriteState> attackStrongStateProvider) {
        this.attackStrongStateProvider = attackStrongStateProvider;
        return this;
    }

    public Sprite setSpecialAttackAStateProvider(Provider<BaseSpriteState> specialAttackAStateProvider) {
        this.specialAttackAStateProvider = specialAttackAStateProvider;
        return this;
    }

    public Sprite setDeathStateProvider(Provider<BaseDeathState> deathStateProvider) {
        this.deathStateProvider = deathStateProvider;
        return this;
    }

    public Sprite setIntroStateProvider(Provider<BaseSpriteState> introStateProvider) {
        this.introStateProvider = introStateProvider;
        return this;
    }

    public void setDashAttackProvider(Provider<BaseSpriteState> dashAttackProvider) {
        this.dashAttackProvider = dashAttackProvider;
    }

    public Sprite setFallStateProvider(Provider<BaseSpriteState> fallStateProvider) {
        this.fallStateProvider = fallStateProvider;
        return this;
    }

    public boolean noOp() {
        if(state!=null) {
            List<SpriteView> frame = state.getViews();
            this.setState(new NoOpState(frame));
        } else {
            this.setState(new NoOpState());
        }
        return true;
    }

    public Sprite setHeight(int height) {
        getSprite().orientation = getSprite().orientation.createFrom().setHeight(height).build();
        return this;
    }

    public Sprite setLocationX(int x) {
        getSprite().orientation = getSprite().orientation.createFrom().setLocationX(x).build();
        return this;
    }

    public int getHeight() {
        return getSprite().getOrientation().getLocation().getY();
    }

    public boolean inAir() {
        return getHeight()>0;
    }

    public boolean fall() {
        return false;
    }

    public boolean toggleSpecial() {
        return false;
    }
}
