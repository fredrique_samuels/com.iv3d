package com.fred.sprite.scene;

import java.util.Arrays;

public enum Command {
    MOVE_LEFT((s) -> {
        s.setVector(Direction.LEFT);
        return false;
    }),
    MOVE_RIGHT((s) -> {
        s.setVector(Direction.RIGHT);
        return false;
    }),
    MOVE_UP((s) -> {
        s.setVector(Direction.UP);
        return false;
    }),
    MOVE_DOWN((s) -> {
        s.setVector(Direction.DOWN);
        return false;
    }),
    MOVE_LEFT_UP((s) -> {
        s.setVector(Direction.LEFT_UP);
        return false;
    }),
    MOVE_LEFT_DOWN((s) -> {
        s.setVector(Direction.LEFT_DOWN);
        return false;
    }),
    MOVE_RIGHT_UP((s) -> {
        s.setVector(Direction.RIGHT_UP);
        return false;
    }),
    MOVE_RIGHT_DOWN((s) -> {
        s.setVector(Direction.RIGHT_DOWN);
        return false;
    }),
    RUN(null),
    WALK(null),
    STOP((s) -> {
        return s.idle();
    }),

    ATTACK_WEAK((s) -> s.attackWeak()),
    ATTACK_STRONG((s) -> s.attackStrong()),
    SPECIAL_ATTACK((s) -> s.specialAttackA()),
    BLOCK(null),
    DASH((s) ->  s.dashAttack() ),
    JUMP((s) -> s.jump()),
    DAMAGE((s) ->  s.damage()),

    TOGGLE_SPECIAL((s) -> s.toggleSpecial());

    private Identity identity;

    Command(Identity identity) {
        this.identity = identity;
    }

    boolean identity(Sprite sprite) {
        if(this.identity!=null) {
            return identity.invoke(sprite);
        }
        return false;
    }

    static final Command[] DIRECTIONS = {MOVE_LEFT,
            MOVE_RIGHT,
            MOVE_UP,
            MOVE_DOWN,
            MOVE_LEFT_UP,
            MOVE_LEFT_DOWN,
            MOVE_RIGHT_UP,
            MOVE_RIGHT_DOWN
    };

    static boolean isCommand(Command c, Command ... list) {
        return Arrays.asList(list).contains(c);
    }


    static boolean isDirection(Command c) {
        return isCommand(c, DIRECTIONS);
    }

    interface Identity {
        boolean invoke(Sprite sprite);
    }
}