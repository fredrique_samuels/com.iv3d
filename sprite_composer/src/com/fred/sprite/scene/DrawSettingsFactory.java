package com.fred.sprite.scene;

import com.fred.sprite.common.SpriteRenderer;

/**
 * Created by fred on 2016/09/12.
 */
public class DrawSettingsFactory {
    public SpriteRenderer.SpriteDrawSettings create(Sprite s) {
        SpriteRenderer.SpriteDrawSettings drawSettings = SpriteRenderer.createDrawSettings(s.getLocation());
        if(s.getDirection().isLeft()) {
            drawSettings.setFlipOnY(true);
        }
        drawSettings.setScale(s.getScale());
        drawSettings.setOpacity(s.getOpacity());
        return drawSettings;
    }
}
