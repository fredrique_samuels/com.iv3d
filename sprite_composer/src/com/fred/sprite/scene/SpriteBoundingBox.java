package com.fred.sprite.scene;

import com.iv3d.common.core.BoundingBoxInt;

/**
 * Created by fred on 2016/09/13.
 */
public class SpriteBoundingBox {
    private final BoundingBoxInt bb;
    private final int minDepth;
    private final int maxDepth;

    public SpriteBoundingBox(int minX, int minY, int maxX, int maxY, int minDepth, int maxDepth) {
        this.bb = new BoundingBoxInt(minX, minY, maxX, maxY);
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }

    private SpriteBoundingBox(BoundingBoxInt bb, int minDepth, int maxDepth) {
        this.bb = bb;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }

    public final int getMinX() {
        return bb.getMinX();
    }

    public final int getMinY() {
        return bb.getMinY();
    }

    public final int getMaxX() {
        return bb.getMaxX();
    }

    public final int getMaxY() {
        return bb.getMaxY();
    }

    public final int getWidth() {
        return bb.getWidth();
    }

    public final int getHeight() {
        return bb.getHeight();
    }

    public final int getMinDepth() {
        return minDepth;
    }

    public final int getMaxDepth() {
        return maxDepth;
    }

    public final SpriteBoundingBox offset(int x, int y, int depth) {
        return new SpriteBoundingBox(bb.offset(x, y), depth+minDepth, depth+maxDepth);
    }

    public final SpriteBoundingBox setMinDepth(int d) {
        return new SpriteBoundingBox(bb, d, maxDepth);
    }

    public final SpriteBoundingBox setMaxDepth(int d) {
        return new SpriteBoundingBox(bb, minDepth, d);
    }

    public boolean collidesWith(SpriteBoundingBox o) {
        boolean bbCollides = bb.intersects(o.bb);
        boolean imind = o.minDepth >= this.minDepth && o.minDepth <= this.maxDepth;
        boolean imaxd = o.maxDepth >= this.minDepth && o.maxDepth <= this.maxDepth;
        return bbCollides && (imaxd || imind);
    }

    public SpriteBoundingBox padDepth(int depth) {
        return new SpriteBoundingBox(bb, minDepth-depth, maxDepth+depth);
    }

    public SpriteBoundingBox setMaxX(int maxX) {
        return new SpriteBoundingBox(bb.setMaxX(maxX), minDepth, maxDepth);
    }

    public SpriteBoundingBox setMinX(int minX) {
        return new SpriteBoundingBox(bb.setMinX(minX), minDepth, maxDepth);
    }

    public SpriteBoundingBox setMaxY(int maxY) {
        return new SpriteBoundingBox(bb.setMaxY(maxY), minDepth, maxDepth);
    }

    public SpriteBoundingBox setMinY(int minY) {
        return new SpriteBoundingBox(bb.setMinY(minY), minDepth, maxDepth);
    }

    @Override
    public String toString() {
        return String.format("[%s minDepth=%d maxDepth=%d bb=%s]",
                SpriteBoundingBox.class, minDepth,maxDepth, bb);
    }
}
