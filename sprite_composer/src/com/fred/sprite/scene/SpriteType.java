package com.fred.sprite.scene;

/**
 * Created by fred on 2016/09/11.
 */
public enum SpriteType {
    CHARACTER,
    EFFECT,
    DAMAGE,
    OVERLAY
}
