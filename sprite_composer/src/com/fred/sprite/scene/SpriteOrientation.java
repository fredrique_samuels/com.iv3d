package com.fred.sprite.scene;

import com.iv3d.common.core.Point;

/**
 * Created by fred on 2016/09/12.
 */
public final class SpriteOrientation {

    private Point location = new Point(0,0);
    private Direction direction = Direction.RIGHT;
    private int depth=0;

    public SpriteOrientation() {
    }

    private SpriteOrientation(SpriteOrientation o) {
        this.location = o.location;
        this.direction = o.direction;
        this.depth = o.depth;
    }

    public final Point getLocation() {
        return location;
    }

    public final Direction getDirection() {
        return direction;
    }

    public final int getDepth() {
        return depth;
    }

    public final Builder createFrom() {
        return new Builder(this);
    }

    public static Builder create() {
        return new SpriteOrientation().createFrom();
    }

    public static class Builder {
        private final SpriteOrientation o;

        public Builder(SpriteOrientation o) {
            this.o = new SpriteOrientation(o);
        }

        public Builder setLocation(Point p) {
            o.location = p;
            return this;
        }

        public Builder setDirection(Direction d) {
            o.direction = d;
            return this;
        }

        public Builder setDepth(int depth) {
            o.depth = depth;
            return this;
        }

        public Builder setHeight(int height) {
            this.o.location = new Point(o.location.getX(), height);
            return this;
        }

        public SpriteOrientation build() {
            return new SpriteOrientation(o);
        }

        public Builder setLocationX(int x) {
            this.o.location = new Point(x, o.location.getY());
            return this;
        }
    }
}
