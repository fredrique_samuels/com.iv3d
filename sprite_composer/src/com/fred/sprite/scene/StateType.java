package com.fred.sprite.scene;

public enum StateType {
    NOOP(new NoOpTransition()),
    IDLE(new IdleTransition()),
    DETACHED(new DetachedTransition()),
    WALK(new WalkTransition()),
    RUN(new RunTransition()),
    JUMP(new JumpTransition()),
    ATTACK(new AttackTransition() ),
    FALL(new FallTransition());


    private Transition stateTransition;

    StateType(Transition stateTransition) {
        this.stateTransition = stateTransition;
    }

    interface Transition {
        boolean invoke(Command command, Sprite sprite);
    }

    public void processCommands(Sprite sprite, Command ... commands){
        for(Command c: commands) {
            if(stateTransition.invoke(c, sprite)) {
                break;
            }
        }
    }

    private static class NoOpTransition implements Transition {

        @Override
        public boolean invoke(Command command, Sprite sprite) {
            boolean skip = Command.isCommand(command, Command.RUN, Command.DASH, Command.STOP);
            if(skip)return false;

            if(Command.isDirection(command)) {
                return sprite.walk();
            }

            return command.identity(sprite);
        }
    }

    private static class IdleTransition extends NoOpTransition {

    }

    private static class WalkTransition implements Transition {
        @Override
        public boolean invoke(Command command, Sprite sprite) {
            boolean skip = Command.isCommand(command, Command.DASH);
            if(skip)return false;

            if(command==Command.JUMP) {
                return sprite.jumpSmall();
            }

            if(command==Command.RUN) {
                return sprite.run();
            }

            if(command==Command.STOP) {
                return sprite.idle();
            }

            return command.identity(sprite);
        }
    }



    private static class RunTransition implements Transition {
        @Override
        public boolean invoke(Command command, Sprite sprite) {
            boolean skip = Command.isCommand(command, Command.DASH);
            if(skip)return false;

            if(Command.isCommand(command, Command.ATTACK_WEAK, Command.ATTACK_STRONG)) {
                return sprite.dashAttack();
            }

            if(command==Command.JUMP) {
                return sprite.jumpLarge();
            }

            if(command==Command.STOP) {
                return sprite.idle();
            }

            if(command==Command.WALK) {
                return sprite.walk();
            }

            return command.identity(sprite);
        }
    }


    private static class DetachedTransition implements Transition {
        @Override
        public boolean invoke(Command command, Sprite sprite) {
            if(command==Command.TOGGLE_SPECIAL) {
                return command.identity(sprite);
            }
            return false;
        }
    }

    private static class JumpTransition implements Transition {
        @Override
        public boolean invoke(Command command, Sprite sprite) {
            if(Command.isCommand(command, Command.TOGGLE_SPECIAL, Command.DAMAGE)) {
                return command.identity(sprite);
            }

            if(command==Command.ATTACK_WEAK) {
                return sprite.airAttackWeak();
            }

            if(command==Command.ATTACK_STRONG) {
                return sprite.airAttackStrong();
            }

            if(command==Command.SPECIAL_ATTACK) {
                return sprite.airSpecialAttackA();
            }

            return false;
        }
    }

    private static class FallTransition extends JumpTransition {

    }

    private static class AttackTransition implements Transition {
        @Override
        public boolean invoke(Command command, Sprite sprite) {
            if(Command.isCommand(command, Command.TOGGLE_SPECIAL, Command.DAMAGE)) {
                return command.identity(sprite);
            }
            return false;
        }
    }
}