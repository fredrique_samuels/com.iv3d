package com.fred.sprite.scene;

import com.iv3d.common.core.Callback;
import com.iv3d.common.core.Callback2Arg;

import java.util.List;

/**
 * Created by fred on 2016/09/10.
 */
public interface SpriteState {

    void update(Sprite sprite, SpriteScene spriteScene);
    List<SpriteView> getViews();
    void setOnExit(Callback<Sprite> onExit);
    void setOnDone(Callback2Arg<Sprite, SpriteScene> onDone);
    void dispatchExit(Sprite sprite);
    StateType getStateType();
    void markDone();

}
