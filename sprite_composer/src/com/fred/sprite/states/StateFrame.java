package com.fred.sprite.states;

import com.fred.sprite.scene.Sprite;
import com.fred.sprite.scene.SpriteScene;
import com.iv3d.common.core.Callback2Arg;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/09/10.
 */
public class StateFrame {
    final long id;
    final long timeInMilli;
    Callback2Arg<Sprite, SpriteScene> onShow;

    public StateFrame(long id, long timeInMilli) {
        this.id = id;
        this.timeInMilli = timeInMilli;
    }

    public final long getId() {
        return id;
    }

    public final long getTimeInMilli() {
        return timeInMilli;
    }

    public static List<StateFrame> createForIds(List<Long> ids, long timeInMilli) {
        return ids.stream()
            .map(id->new StateFrame(id, timeInMilli))
            .collect(Collectors.toList());
    }

    public static List<StateFrame> createForRange(Long start, Long end, long timeInMilli, Callback2Arg<Long, StateFrame> ... frameHandlers) {
        ArrayList s = new ArrayList();
        for(long i=start;i<=end;i++) {
            StateFrame frame = new StateFrame(i, timeInMilli);
            for(Callback2Arg<Long, StateFrame> cb : frameHandlers) {
                cb.invoke(i, frame);
            }
            s.add(frame);
        }
        return s;
    }

    public static List<StateFrame> createRepeat(Long frameId, Long count, long timeInMilli) {
        ArrayList s = new ArrayList();
        for(long i=0;i<=count;i++) {
            s.add(new StateFrame(frameId, timeInMilli));
        }
        return s;
    }


    public final void dispatchShown(Sprite s, SpriteScene ss) {
        if(onShow!=null) {
            this.onShow.invoke(s, ss);
        }
    }

    public final StateFrame setOnShow(Callback2Arg<Sprite, SpriteScene> onShow) {
        this.onShow = onShow;
        return this;
    }
}
