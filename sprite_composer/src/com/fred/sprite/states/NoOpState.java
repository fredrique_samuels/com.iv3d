package com.fred.sprite.states;

import com.fred.sprite.scene.*;
import com.iv3d.common.core.Callback;
import com.iv3d.common.core.Callback2Arg;

import java.util.Collections;
import java.util.List;

public class NoOpState implements SpriteState {
    private final List<SpriteView> views;

    public NoOpState(List<SpriteView> views) {
        this.views = views;
    }

    public NoOpState() {
        this.views = Collections.emptyList();
    }

    @Override
    public void update(Sprite sprite, SpriteScene spriteScene) {

    }

    @Override
    public List<SpriteView> getViews() {
        return views;
    }

    @Override
    public void setOnExit(Callback<Sprite> onExit) {

    }

    @Override
    public void setOnDone(Callback2Arg<Sprite, SpriteScene> onDone) {

    }

    @Override
    public void dispatchExit(Sprite sprite) {

    }

    public StateType getStateType() {
        return StateType.NOOP;
    }

    @Override
    public void markDone() {

    }

}