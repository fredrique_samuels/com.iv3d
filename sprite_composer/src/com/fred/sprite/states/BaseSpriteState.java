package com.fred.sprite.states;

import com.fred.sprite.scene.*;
import com.iv3d.common.core.Callback;
import com.iv3d.common.core.Callback2Arg;

import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2016/09/10.
 */
public class BaseSpriteState implements SpriteState {
    private final StateType stateType;
    private boolean firstIteration;

    private StateFrame currentFrame;
    private final StateFrame lastFrame;

    private List<StateFrame> frames;
    private boolean loop = true;
    protected final SpriteMapConfig smc;
    private Callback2Arg<Sprite, SpriteScene> onDone;
    private Callback2Arg<Sprite, SpriteScene> onUpdate;
    private Callback<Sprite> onExit;
    private boolean done = false;

    public BaseSpriteState(StateType stateType, List<StateFrame> frames, SpriteMapConfig smc) {
        this.stateType = stateType;
        this.frames = frames;
        this.smc = smc;
        this.lastFrame = frames.get(frames.size()-1);
        this.currentFrame = frames.get(0);
        this.firstIteration = true;
        this.setOnDone((s, ss)-> s.noOp());
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    @Override
    public void update(Sprite sprite, SpriteScene spriteScene) {
        dispatchUpdate(sprite, spriteScene);

        if(done) {
            dispatchDone(sprite, spriteScene);
            return;
        }

        long total = 0;
        for (StateFrame f:frames) {
            total += f.getTimeInMilli();
        }

        long timeInState = sprite.getTimeInState();

        if(frames.size()==1 && !loop && timeInState>=total) {
            dispatchDone(sprite, spriteScene);
            done = true;
            return;
        }

        if(timeInState>total) {
            timeInState = timeInState%total;
        }

        for (StateFrame f : frames) {
            timeInState -= f.getTimeInMilli();
            if (timeInState < 0) {
                if (frames.size()>1 && currentFrame == lastFrame && !loop) {
                    dispatchDone(sprite, spriteScene);
                } else if (currentFrame != f) {
                    currentFrame = f;
                    currentFrame.dispatchShown(sprite, spriteScene);
                } else if(firstIteration) {
                    firstIteration = false;
                    currentFrame.dispatchShown(sprite, spriteScene);
                }
                break;
            }
        }
    }

    public void markDone() {
        done = true;
    }

    private void dispatchUpdate(Sprite sprite, SpriteScene spriteScene) {
        if(onUpdate!=null) {
            onUpdate.invoke(sprite, spriteScene);
        }
    }

    private void dispatchDone(Sprite sprite, SpriteScene spriteScene) {
        if (onDone != null) {
            onDone.invoke(sprite, spriteScene);
            onDone=null;
        }
    }

    public long getFrameId() {
        if(currentFrame==null) {
            return 0;
        }
        return currentFrame.getId();
    }

    @Override
    public List<SpriteView> getViews() {;
        return Collections.singletonList(new SpriteView(smc.getImage(), smc.getSpriteMapParam().findSprite(currentFrame.getId())));
    }

    @Override
    public final void setOnExit(Callback<Sprite> onExit) {
        this.onExit = onExit;
    }

    public final void setOnDone(Callback2Arg<Sprite, SpriteScene> onDone) {
        this.onDone = onDone;
    }
    public final void setOnUpdate(Callback2Arg<Sprite, SpriteScene> onUpdate) {
        this.onUpdate = onUpdate;
    }

    @Override
    public void dispatchExit(Sprite sprite) {
        if(onExit!=null) {
            onExit.invoke(sprite);
        }
    }

    public StateType getStateType() {
        return stateType;
    }

}
