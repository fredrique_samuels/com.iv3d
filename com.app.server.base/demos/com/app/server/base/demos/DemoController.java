package com.app.server.base.demos;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.google.inject.Inject;

/**
 * Created by fred on 2016/11/03.
 */
public class DemoController extends AppWebController {

    @Inject
    private HomeHandler homePageHandler;

    @Inject
    public DemoController(LocalAppSettingsManager appSettingsManager) {
        super(appSettingsManager.get("home"));
    }

    @Override
    public void configure() {
        bindPathHandler("/", homePageHandler);
    }
}
