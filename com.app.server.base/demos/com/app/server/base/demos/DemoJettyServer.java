package com.app.server.base.demos;

import com.app.server.base.server.AppServerLauncher;

/**
 * Created by fred on 2016/11/03.
 */

/**
 * VM ARGS
 * Optional : -Djetty.port=58080 -Djetty.host=0.0.0.0
 */
public class DemoJettyServer {
    public static void main(String[] args) throws Exception {
        new AppServerLauncher().run();
    }
}
