package com.app.server.base.demos;

import com.app.server.base.server.AppTask;

/**
 * Created by fred on 2017/02/13.
 */
public class Task1 implements AppTask {
    @Override
    public void execute() {
        System.out.println("Running Application task 1");
    }
}
