package com.app.server.base.demos;

import com.app.server.base.server.web.RequestHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by fred on 2016/11/03.
 */
public class HomeHandler extends RequestHandler {

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        returnOkResponse(response);
    }
}
