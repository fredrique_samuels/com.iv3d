package etc.config.sql.schema.tt_server.tt_app_job_run_ids_seed;

import com.eroi.migrate.Migration;
import etc.config.sql.schema.tt_server.tt_app_jobs.AppJobsSchema;

import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Define.DataTypes.INTEGER;
import static com.eroi.migrate.Define.DataTypes.TIMESTAMP;
import static com.eroi.migrate.Define.DataTypes.VARCHAR;
import static com.eroi.migrate.Define.notnull;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/25.
 */
public class AppJobRunIdSeedSchema_CreateTable  implements Migration {

    @Override
    public void up() {
        createTable(
            table(AppJobRunIdSeedSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("doe", TIMESTAMP, notnull()),
                column("tt_app_job_id", INTEGER),
                column("seed", INTEGER))
        );
    }

    @Override
    public void down() {
        dropTable(AppJobRunIdSeedSchema.TABLE_NAME);
    }
}
