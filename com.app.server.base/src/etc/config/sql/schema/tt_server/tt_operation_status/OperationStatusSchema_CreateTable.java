package etc.config.sql.schema.tt_server.tt_operation_status;

import com.eroi.migrate.Migration;

import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/25.
 */
public class OperationStatusSchema_CreateTable implements Migration {

    @Override
    public void up() {
        createTable(
            table(OperationStatusSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("doe", TIMESTAMP, notnull()),
                column("name", VARCHAR, length(1024)),
                column("start_date", TIMESTAMP, notnull()),
                column("parent_id", INTEGER),
                column("duration_in_ms", INTEGER),
                column("error_message", VARCHAR, length(1024)),
                column("error_trace", VARCHAR, length(2048)),
                column("error", BOOLEAN),
                column("type", VARCHAR, length(50)),
                column("status", VARCHAR, length(50)),
                column("attributes", VARCHAR, length(4096)))
        );
    }

    @Override
    public void down() {
        dropTable(OperationStatusSchema.TABLE_NAME);
    }
}
