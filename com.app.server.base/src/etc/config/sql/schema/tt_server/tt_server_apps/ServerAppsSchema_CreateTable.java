package etc.config.sql.schema.tt_server.tt_server_apps;

import com.eroi.migrate.Migration;
import com.eroi.migrate.schema.Column;
import com.eroi.migrate.schema.Table;
import etc.config.sql.schema.tt_server.tt_server.ServerSchema;

import static com.eroi.migrate.Define.DataTypes.INTEGER;
import static com.eroi.migrate.Define.DataTypes.VARCHAR;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class ServerAppsSchema_CreateTable implements Migration {

    @Override
    public void up() {
        Table table = table(ServerAppsSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("server_run_id", INTEGER, notnull()),
                column("app_id", INTEGER, notnull())
                );
        createTable(table);
    }

    @Override
    public void down() {
        dropTable(ServerAppsSchema.TABLE_NAME);
    }
}
