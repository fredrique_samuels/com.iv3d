package etc.config.sql.schema.tt_server;

import com.iv3d.common.storage.migrate.SchemaMigration;
import etc.config.sql.schema.tt_server.tt_app_job_run_entry.AppJobRunEntrySchema_CreateTable;
import etc.config.sql.schema.tt_server.tt_app_job_run_ids_seed.AppJobRunIdSeedSchema_CreateTable;
import etc.config.sql.schema.tt_server.tt_app_jobs.AppJobSchema_CreateTable;
import etc.config.sql.schema.tt_server.tt_apps.AppsSchema_CreateTable;
import etc.config.sql.schema.tt_server.tt_operation_status.OperationStatusSchema_CreateTable;
import etc.config.sql.schema.tt_server.tt_server.ServerSchema_CreateTable;
import etc.config.sql.schema.tt_server.tt_server_apps.ServerAppsSchema_CreateTable;

public class Migration_1 extends SchemaMigration {
    public Migration_1() {
        super(new AppsSchema_CreateTable(),
              new AppJobSchema_CreateTable(),
              new AppJobRunEntrySchema_CreateTable(),
              new OperationStatusSchema_CreateTable(),
              new AppJobRunIdSeedSchema_CreateTable(),
              new ServerSchema_CreateTable(),
              new ServerAppsSchema_CreateTable());
    }
} 