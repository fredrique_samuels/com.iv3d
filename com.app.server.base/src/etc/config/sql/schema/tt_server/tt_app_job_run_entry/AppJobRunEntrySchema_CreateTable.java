package etc.config.sql.schema.tt_server.tt_app_job_run_entry;

import com.eroi.migrate.Migration;

import javax.persistence.Column;

import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class AppJobRunEntrySchema_CreateTable implements Migration {

    @Override
    public void up() {
        createTable(
            table(AppJobRunEntrySchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("doe", TIMESTAMP, notnull()),
                column("tt_app_job_id", INTEGER),
                column("tt_app_job_run_id", INTEGER),
                column("tt_operation_status_id", INTEGER))
        );
    }

    @Override
    public void down() {
        dropTable(AppJobRunEntrySchema.TABLE_NAME);
    }
}
