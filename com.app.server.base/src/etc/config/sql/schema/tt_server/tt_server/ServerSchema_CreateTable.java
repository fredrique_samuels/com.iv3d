package etc.config.sql.schema.tt_server.tt_server;

import com.eroi.migrate.Configure;
import com.eroi.migrate.Migration;
import com.eroi.migrate.generators.Generator;
import com.eroi.migrate.generators.GeneratorFactory;
import com.eroi.migrate.misc.SchemaMigrationException;
import com.eroi.migrate.schema.Column;
import com.eroi.migrate.schema.Table;
import etc.config.sql.schema.tt_server.tt_apps.AppsSchema;

import java.sql.Connection;
import java.sql.SQLException;

import static com.eroi.migrate.Define.DataTypes.INTEGER;
import static com.eroi.migrate.Define.DataTypes.TIMESTAMP;
import static com.eroi.migrate.Define.DataTypes.VARCHAR;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class ServerSchema_CreateTable implements Migration {

    @Override
    public void up() {
        Column id = column("id", INTEGER, primarykey());
        Column doe = column("doe", TIMESTAMP);
        Table table = table(ServerSchema.TABLE_NAME, id, doe);

        try {
            Connection e = Configure.getConnection();
            Generator generator = GeneratorFactory.getGenerator(e);
            String query = generator.createTableStatement(table);
            System.out.println(query);
        } catch (SQLException var5) {
        }

        createTable(table);
    }

    @Override
    public void down() {
        dropTable(ServerSchema.TABLE_NAME);
    }
}
