package etc.config.sql.schema.tt_server.tt_apps;

import com.eroi.migrate.Migration;
import com.eroi.migrate.schema.Column;

import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Define.DataTypes.INTEGER;
import static com.eroi.migrate.Define.DataTypes.TIMESTAMP;
import static com.eroi.migrate.Define.DataTypes.VARCHAR;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class AppsSchema_CreateTable implements Migration {

    @Override
    public void up() {
        createTable(
            table(AppsSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("doe", TIMESTAMP, notnull()),
                column("package_id", VARCHAR, length(255), notnull()),
                column("name", VARCHAR, length(255), notnull())));
    }

    @Override
    public void down() {
        dropTable(AppsSchema.TABLE_NAME);
    }
}
