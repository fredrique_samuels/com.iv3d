package etc.config.sql.schema.tt_server.tt_app_jobs;

import com.eroi.migrate.Migration;

import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class AppJobSchema_CreateTable implements Migration {

    @Override
    public void up() {
        createTable(
            table(AppJobsSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("tt_app_id", INTEGER),
                column("doe", TIMESTAMP, notnull()),
                column("dlu", TIMESTAMP, notnull()),
                column("bean_id", VARCHAR, length(255), notnull()),
                column("name", VARCHAR, length(255), notnull()),
                column("description", VARCHAR, length(1024), notnull()))
        );
    }

    @Override
    public void down() {
        dropTable(AppJobsSchema.TABLE_NAME);
    }
}
