package com.app.server.base.macros;

import freemarker.cache.TemplateLoader;

public interface HtmlMacroService {
    String parseTemplate(String templateFile, Object model);
    String parseTemplateFromFile(String template, Object model);
    String parseTemplate(String template, Object model, TemplateLoader templateLoader);
}
