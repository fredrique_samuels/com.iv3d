package com.app.server.base.macros;

import com.iv3d.common.core.ResourceLoader;
import freemarker.cache.TemplateLoader;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * Created by fred on 2017/08/02.
 */
public class ResourceMacroLoader implements TemplateLoader {
    @Override
    public Object findTemplateSource(String s) throws IOException {
        return ResourceLoader.getUrl(s);
    }

    @Override
    public long getLastModified(Object o) {
        return 0;
    }

    @Override
    public Reader getReader(Object o, String s) throws IOException {
        String content = ResourceLoader.readAsString(s);
        return new StringReader(content);
    }

    @Override
    public void closeTemplateSource(Object o) throws IOException {

    }
}
