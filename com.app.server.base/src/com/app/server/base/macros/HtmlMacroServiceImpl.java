package com.app.server.base.macros;

import com.google.inject.Singleton;
import com.iv3d.common.core.ResourceLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.apache.log4j.Logger;

import java.io.*;

@Singleton
public class HtmlMacroServiceImpl implements HtmlMacroService {
    private Logger log = Logger.getLogger(HtmlMacroServiceImpl.class);
    private final Configuration cfg;
    private final TemplateLoader defaultTemplateLoader;

    public HtmlMacroServiceImpl() {
        this.cfg = new Configuration(Configuration.VERSION_2_3_25);
        setTemplateDir();
        cfg.setDefaultEncoding("UTF-8");
        cfg.setNumberFormat("0.######");
        cfg.setLocalizedLookup(false);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(true);
        this.defaultTemplateLoader = cfg.getTemplateLoader();
    }

    private void setTemplateDir() {
        try {
            File file = ResourceLoader.getFile("/etc/view/macros");
            if(file.exists()) {
                cfg.setDirectoryForTemplateLoading(file);
            }
            else {
                log.warn("Html macros location not found "+file.getPath());
            }
        } catch (IOException e) {
            throw new TemplateDirError(e);
        }
    }

    @Override
    public String parseTemplate(String templateFile, Object model) {
        return parseTemplate(templateFile, model, defaultTemplateLoader);
    }

    @Override
    public String parseTemplateFromFile(String template, Object model) {
        return parseTemplate(template, model, new ResourceMacroLoader());
    }

    public String parseTemplate(String template, Object model, TemplateLoader templateLoader) {
        try {
            cfg.setTemplateLoader(templateLoader);
            Template temp = cfg.getTemplate(template);
            StringWriter writer = new StringWriter();
            temp.process(model, writer);
            return writer.toString();
        } catch (IOException e) {
            throw new TemplateError(e);
        } catch (TemplateException e) {
            throw new TemplateError(e);
        }
    }

    public static class  TemplateDirError extends RuntimeException {
        public TemplateDirError(IOException e) {
            super(e);
        }
    }

    public static class  TemplateError extends RuntimeException {
        public TemplateError(Exception e) {
            super(e);
        }
    }


    public static void main(String[] args) {
        HtmlMacroServiceImpl htmlMacroService = new HtmlMacroServiceImpl();
//        htmlMacroService.parseTemplate()
    }

}
