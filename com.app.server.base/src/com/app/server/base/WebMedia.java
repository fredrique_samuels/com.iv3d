package com.app.server.base;


public final class WebMedia {

    private String link;
    private String summary;
    private MimeType mimetype;
    private long id;

    public WebMedia() {
        id=-1;
        summary="";
    }

    public final String getLink() {
        return link;
    }

    public final String getSummary() {
        return summary;
    }

    public final MimeType getMimetype() {
        return mimetype;
    }

    public static WebMedia create(String imageLink, String summary, MimeType type) {
        WebMedia webMedia = new WebMedia();
        webMedia.mimetype =type;
        webMedia.link = imageLink;
        webMedia.summary = summary;
        return webMedia;
    }

    public static WebMedia image(String imageLink, String summary) {
        return create(imageLink, summary, MimeType.IMAGE);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setMimetype(MimeType mimetype) {
        this.mimetype = mimetype;
    }
}
