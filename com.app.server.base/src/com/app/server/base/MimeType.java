package com.app.server.base;

public enum MimeType {
    IMAGE("image"),
    VIDEO_YOUTUBE("video/youtube"),
    TWITTER_TWEET("twitter/tweet");

    private final String type;

    MimeType(String type) {
        this.type = type;
    }

    public String value() {
        return type;
    }
}
