/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.common.inject;

import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.name.Names;

public class InjectorProvider implements Provider<BeanInjector>{
	private BeanInjector bi; 

	@Override
	public BeanInjector get() {
		return bi;
	}
	
	public void setInjector(final Injector injector) {
		bi = new BeanInjector() {
			@Override
			public <T> T getInstanceByName(String name, Class<T> type) {
				return injector.getInstance(Key.get(type, Names.named(name)));
			}
			
			@Override
			public <T> T createInstance(Class<T> type) {
				return injector.getInstance(type);
			}
		};
	}
	
}