package com.app.server.base.server;

import com.app.server.base.server.api.AppJobRunEntryParam;
import com.app.server.base.server.api.OperationStatusParam;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;

import java.util.List;

/**
 * Created by fred on 2017/05/15.
 */
public interface AppJobManager {
    OperationStatusParam runOperationTask(String appPackageId, String beanId);
    OperationStatusParam runOperationTask(long appId, String beanId);
    List<OperationStatusParam> getQueueStatus();

    OperationStatusParam getLastJobStatusFromStorage(long jobId);
    OperationStatusParam getJobStatusFromQueueOrStorage(long jobId, long runId);
    List<AppJobRunEntryParam> getJobRunEntryList(Long jobId, Long maxRunId);
    OperationStatusParam getLastJobStatus(long jobId);
}
