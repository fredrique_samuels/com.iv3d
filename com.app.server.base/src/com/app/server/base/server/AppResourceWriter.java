package com.app.server.base.server;

import com.app.server.base.server.operations.results.ImageDownloadStatus;
import com.app.server.base.server.operations.results.WritePolicy;
import com.app.server.base.server.operations.results.ResourceWriteStatus;
import com.iv3d.common.core.ResourceLoader;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

/**
 * Created by fred on 2017/05/15.
 */
public class AppResourceWriter {

    private static Logger logger = Logger.getLogger(AppResourceWriter.class);

    private AppContext appContext;

    public AppResourceWriter(AppContext appContext) {
        this.appContext = appContext;
    }

    public ResourceWriteStatus writePrivateResource(String content, String resource, WritePolicy writePolicy, String source) {
        AppContext appContext = this.appContext;
        File privateResourceFile = appContext.getPrivateResourceFile(resource);
        return writePrivateResource(content, privateResourceFile, writePolicy, source);
    }

    public ResourceWriteStatus writePrivateResource(String content, String resource, WritePolicy writePolicy) {
        return writePrivateResource(content, resource, writePolicy, null);
    }

    private ResourceWriteStatus writePrivateResource(String content, File file, WritePolicy writePolicy, String source) {
        boolean exists = file.exists();
        if(WritePolicy.SKIP_IF_EXISTS.equals(writePolicy) && exists) {
            return ResourceWriteStatus.createBuilder(file)
                    .setSkipped(writePolicy)
                    .setSource(source)
                    .build();
        }
        logger.info("Writing resource... "+file.getPath());

        try {
            file.getParentFile().mkdirs();
            ResourceLoader.writeString(content, file);
        } catch (Exception e) {
            return ResourceWriteStatus.createBuilder(file)
                    .setError(e)
                    .setSource(source)
                    .build();
        }

        return ResourceWriteStatus.createBuilder(file)
                .setName(file.getName())
                .setSource(source)
                .setSuccess(writePolicy, !exists)
                .build();
    }

    public File getPrivateResource(String pathname) {
        return appContext.getPrivateResourceFile(pathname);
    }

    public ImageDownloadStatus writePrivateImageResource(WritePolicy writePolicy, String imageUrl, String pathname) {
        File imageFile = getPrivateResource(pathname);
        boolean exists = imageFile.exists();
        if(writePolicy.equals(WritePolicy.SKIP_IF_EXISTS) && exists) {
            return ImageDownloadStatus.createBuilder(imageFile, imageUrl).setSkipped(writePolicy).build();
        }

        BufferedImage image = null;
        try {
            URL url = new URL(imageUrl);
            image = ImageIO.read(url);

            String fileExt = getFileExtension(pathname);
            ImageIO.write(image, fileExt, imageFile);
            return ImageDownloadStatus.createBuilder(imageFile, imageUrl).setSuccess(writePolicy, !exists).build();
        } catch (Exception error) {
            return ImageDownloadStatus.createBuilder(imageFile, imageUrl).setError(error).build();
        }
    }

    private String getFileExtension(String pathname) {
        String[] split = pathname.split("\\.(?=[^\\.]+$)");
        if(split.length!=2) {
            throw new FileExtensionCouldNotBeFound(pathname);
        }
        return split[1];
    }

    private class FileExtensionCouldNotBeFound extends RuntimeException {
        public FileExtensionCouldNotBeFound(String pathname) {
            super("Unable to find the extension for file : " + pathname);
        }
    }
}
