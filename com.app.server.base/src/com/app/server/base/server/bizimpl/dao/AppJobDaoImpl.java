package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.OperationStatusDao;
import com.app.server.base.server.api.*;
import com.iv3d.common.pagination.PaginationParam;
import com.iv3d.common.storage.hibernate.AbstractHibernateDao;
import com.iv3d.common.storage.hibernate.DaoDelegate;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AppJobDaoImpl extends DaoDelegate implements AppJobDao {
    private final OperationStatusDao operationStatusDao;

    public AppJobDaoImpl(AbstractHibernateDao dao, OperationStatusDao operationStatusDao) {
        super(dao);
        this.operationStatusDao = operationStatusDao;
    }

    @Override
    public List<AppJobHibernate> getJobsForApp(AppContextHibernate app) {
        List<AppJobHibernate> dataList = dao.getObjectList((session)-> session.createQuery("FROM AppJobHibernate where tt_app_id="+app.getId()));
        return dataList==null? Collections.EMPTY_LIST:dataList;
    }

    @Override
    public PaginationParam<AppJobParam> getJobsForApp(AppContextParam app) {
        List<AppJobHibernate> dataList = dao.getObjectList((session)-> session.createQuery("FROM AppJobHibernate where tt_app_id="+app.getId()));
        if(dataList==null)
            return new PaginationParam<>();
        List<AppJobParam> collect = dataList.stream()
                .map(AppJobHibernate::getParam)
                .collect(Collectors.toList());
        return new PaginationParam<>(1,1,collect);
    }

    @Override
    public PaginationParam<AppJobParam> getJobsForAppList(List<AppContextParam> apps) {
        List<Long> appIdsList = apps.stream().map(AppContextParam::getId).collect(Collectors.toList());

        List<AppJobHibernate> hibernateList = dao.getObjectList((session)->
                session.createQuery("FROM AppJobHibernate WHERE tt_app_id IN (:appIdsList)")
                    .setParameterList("appIdsList", appIdsList)
        );

        if(hibernateList==null)
            return new PaginationParam<>();

        List<AppJobParam> collect = hibernateList.stream()
                .map(AppJobHibernate::getParam)
                .collect(Collectors.toList());
        return new PaginationParam<>(1,1,collect);
    }

    @Override
    public void upsert(AppJobHibernate appJobHibernate) {
            dao.upsert(appJobHibernate);
        }

    @Override
    public void saveRunEntry(AppJobRunEntryParam jobRunEntryParam) {
        OperationStatusParam statusParam = jobRunEntryParam.getTaskStatus();
        long statusId = operationStatusDao.save(statusParam);
        AppJobRunEntryHibernate hibernate = AppJobRunEntryHibernate.create(jobRunEntryParam.getAppJobId(), jobRunEntryParam.getRunId(), statusId);
        dao.saveObject(hibernate);
    }

    @Override
    public long getNextJobRunId(long jobId) {
        Object result = dao.getUniqueResult((session) -> {
            String hbl = "FROM AppJobRunIdSeedHibernate a WHERE a.appJobId = :appJobId";
            return session.createQuery(hbl).setLong("appJobId", jobId);
        });


        AppJobRunIdSeedHibernate hibernate = null;
        if(result==null) {
            hibernate = AppJobRunIdSeedHibernate.create(jobId);
        } else {
            hibernate = (AppJobRunIdSeedHibernate)result;
        }

        AppJobRunIdSeedParam param = hibernate.getParam();
        long nextId = param.nextId();
        hibernate.update(param);
        dao.upsert(hibernate);

        return nextId;
    }

    @Override
    public AppJobParam getJobById(long jobId) {
        AppJobHibernate jobHibernate = dao.get(jobId, AppJobHibernate.class);
        if(jobHibernate==null)return null;
        return jobHibernate.getParam();
    }

    @Override
    public OperationStatusParam getJobRunStatus(long jobId, long runId) {
        Object result = dao.getUniqueResult((session) -> {
            String hbl = "FROM AppJobRunEntryHibernate a WHERE a.appJobId = :appJobId AND a.runId = :runId";
            return session.createQuery(hbl)
                    .setLong("appJobId", jobId)
                    .setLong("runId", runId);
        });
        if(result==null)return null;

        AppJobRunEntryHibernate hibernate = ((AppJobRunEntryHibernate)result);
        hibernate.loadOperationStatus(operationStatusDao);
        return hibernate.getParam().getTaskStatus();
    }

    @Override
    public AppJobRunEntryParam getLastJobRunEntry(long jobId) {
        Object result = dao.getCriteriaUniqueResult((session) -> {
            DetachedCriteria maxId = DetachedCriteria.forClass(AppJobRunEntryHibernate.class)
                    .setProjection( Projections.max("id") );
            return session.createCriteria(AppJobRunEntryHibernate.class)
                    .add( Property.forName("id").eq(maxId) )
                    .add( Property.forName("appJobId").eq(jobId) );
        });
        if(result==null)return null;
        return ((AppJobRunEntryHibernate) result).getParam();
    }

    @Override
    public List<AppJobRunEntryParam> getJobRunEntryList(long jobId, Long maxRunId) {
        Object result = null;
        if (maxRunId == null) {
            result = dao.getObjectList((session) -> {
                String hbl = "FROM AppJobRunEntryHibernate a WHERE a.appJobId = :appJobId";
                return session.createQuery(hbl).setLong("appJobId", jobId);
            });
        } else {
            result = dao.getObjectList((session) -> {
                String hbl = "FROM AppJobRunEntryHibernate a WHERE a.appJobId = :appJobId AND a.runId <= :maxRunId";
                return session.createQuery(hbl)
                        .setLong("appJobId", jobId)
                        .setLong("maxRunId", maxRunId);
            });
        }

        if(result!=null) {
            List<AppJobRunEntryHibernate> hibernateList = (List<AppJobRunEntryHibernate>) result;
            hibernateList.forEach( hibernate -> hibernate.loadOperationStatus(operationStatusDao));
            return hibernateList.stream()
                    .map( h -> h.getParam() )
                    .collect(Collectors.toList());
        }
        return Collections.EMPTY_LIST;
    }

}