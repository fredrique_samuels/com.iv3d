package com.app.server.base.server.bizimpl;

import com.app.server.base.common.inject.BeanInjector;
import com.app.server.base.server.*;
import com.app.server.base.server.api.AppContextParam;
import com.app.server.base.server.api.AppJobParam;
import com.app.server.base.server.bizimpl.dao.AppJobDaoImpl;
import com.app.server.base.server.operations.OperationRunner;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;
import com.google.inject.Provider;
import com.iv3d.common.utils.JsonUtils;

/**
 * Created by fred on 2017/05/22.
 */
public class OperationTaskProviderImpl implements OperationTaskProvider {
    private Provider<BeanInjector> beanInjector;
    private AppJobDaoImpl appJobsDao;
    private DefaultAppContextManager appManager;

    public OperationTaskProviderImpl(Provider<BeanInjector> beanInjector, AppJobDaoImpl appJobsDao) {
        this.beanInjector = beanInjector;
        this.appJobsDao = appJobsDao;
    }

    @Override
    public OperationTask get(String appPackageId, String beanId) {
        final AppContextParam appContextParam = appManager.getAppByPackageId(appPackageId);
        if(appContextParam==null) {
            throw new AppCouldNotBeLocatedError(appPackageId);
        }

        AppJobParam jobParam = appContextParam.getJobs()
                .stream()
                .filter(appJobParam -> appJobParam.getBeanId().equals(beanId))
                .findFirst()
                .orElseThrow(() -> new AppJobCouldNotBeLocatedError(appPackageId, beanId));

        try {
            long runId = appJobsDao.getNextJobRunId(jobParam.getId());
            JobProcessInfo appJobProcessInfo = new JobProcessInfo(appPackageId, beanId,  jobParam.getId(), runId);
            OperationRunner operationRunner = (OperationRunner) this.beanInjector.get().getInstanceByName(beanId, AppInjectable.class);

            return new OperationTask() {
                boolean started = false;
                @Override
                public String getProcessId() {
                    return JsonUtils.writeToString(appJobProcessInfo);
                }

                @Override
                public JobProcessInfo getProcessInfo() {
                    return appJobProcessInfo;
                }

                @Override
                public OperationStatus execute() {
                    started = true;
                    operationRunner.start(appManager.getBeanParent(beanId), appJobProcessInfo);
                    return operationRunner.getResult(appJobProcessInfo);
                }

                @Override
                public OperationStatus getResult() {
                    return operationRunner.getResult(appJobProcessInfo);
                }

                @Override
                public boolean hasStarted() {
                    return started;
                }
            };
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setAppManager(DefaultAppContextManager appManager) {
        this.appManager = appManager;
    }

    public static final class AppCouldNotBeLocatedError extends RuntimeException {
        public AppCouldNotBeLocatedError(String appPackageId) {
            super("Unable to locate application with packageId " + appPackageId);
        }
    }

    public static final class AppJobCouldNotBeLocatedError extends RuntimeException {
        public AppJobCouldNotBeLocatedError(String appPackageId, String beanId) {
            super("Unable to locate application job with id '" + beanId + "' for app " + appPackageId);
        }
    }


}
