package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.OperationStatusDao;
import com.app.server.base.server.api.AttributeParam;
import com.app.server.base.server.api.OperationStatusParam;
import com.app.server.base.server.api.OperationState;
import com.iv3d.common.date.UtcDateParam;
import com.iv3d.common.storage.AuditedParam;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;
import com.iv3d.common.utils.JsonUtils;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Table(name = "tt_operation_status")
public class OperationStatusHibernate extends HibernateData {
    @Id
    @GeneratedValue
    @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "parent_id") private long parentId=-1;
    @Column(name = "start_date") private String startDate;
    @Column(name = "duration_in_ms") private long durationInMs;
    @Column(name = "error_message") private String errorMessage;
    @Column(name = "error_trace") private String errorTrace;
    @Column(name = "error") private boolean error;
    @Column(name = "type") private String type;
    @Column(name = "attributes") private String attributes = null;
    @Column(name = "name") private String name="";
    @Column(name="status")
    @Enumerated(EnumType.STRING)
    OperationState status;
    @Transient private List<OperationStatusParam> children = Collections.EMPTY_LIST;

    public static OperationStatusHibernate createForParam(OperationStatusParam param) {
        OperationStatusHibernate hibernate = new OperationStatusHibernate();
        hibernate.doe = DaoUtils.persist(param.getDoe());
        hibernate.parentId = param.getParentId();
        hibernate.durationInMs = param.getDurationInMs();
        hibernate.errorMessage = param.getErrorMessage();
        hibernate.errorTrace = param.getErrorTrace();
        hibernate.error= param.isError();
        hibernate.type = param.getType();
        hibernate.name = param.getName();
        hibernate.startDate = DaoUtils.persist(param.getStartDate());
        hibernate.status = param.getStatus();
        List<AttributeParam> paramAttributes = param.getAttributes();
        Map<String, Object> objectMap = paramAttributes.stream()
                .filter(i -> i.getValue()!=null)
                .collect(Collectors.toMap(i -> i.getName(), i -> i.getValue()));
        hibernate.attributes = JsonUtils.writeToString(objectMap);
        return hibernate;
    }

    public OperationStatusParam getParam() {
        OperationStatusParam param = new OperationStatusParam();
        param.setId(getId());
        param.setDoe(new UtcDateParam(DaoUtils.readDate(doe)));
        param.setParentId(parentId);
        param.setDurationInMs(durationInMs);
        param.setErrorMessage(errorMessage);
        param.setErrorTrace(errorTrace);
        param.setError(error);
        param.setType(type);
        param.setStatus(status);
        param.setName(name);
        param.setStartDate(new UtcDateParam(DaoUtils.readDate(startDate)));
        Map<String, Object> stringObjectMap = JsonUtils.parseToMap(attributes);
        List<AttributeParam> attributeParamList = stringObjectMap.entrySet()
                .stream()
                .filter(i -> i.getValue()!=null)
                .map(i -> new AttributeParam(i.getKey(), i.getValue()))
                .collect(Collectors.toList());
        param.setAttributes(attributeParamList);
        param.setChildren(children);
        return param;
    }

    public void loadChildren(OperationStatusDao dao) {
        if(id==null) return;
        children = dao.getChildrenByParentId(id);
    }

    @Override
    public Long getId() {
        return id==null?AuditedParam.UNSAVED:id;
    }
}
