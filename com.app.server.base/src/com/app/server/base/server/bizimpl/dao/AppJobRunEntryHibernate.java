package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.OperationStatusDao;
import com.app.server.base.server.api.AppJobRunEntryParam;
import com.app.server.base.server.api.OperationStatusParam;
import com.iv3d.common.storage.AuditedParam;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;

/**
 * Created by fred on 2017/05/25.
 */
@Entity
@Table(name="tt_app_job_run_entry")
public class AppJobRunEntryHibernate extends HibernateData {

    @Id
    @GeneratedValue
    @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "tt_app_job_id") private long appJobId;
    @Column(name = "tt_app_job_run_id") private long runId;
    @Column(name = "tt_operation_status_id") private long operationStatusId;
    @Transient private OperationStatusParam operationStatus;

    public AppJobRunEntryHibernate() {
        doe = DaoUtils.dateNowForPersist();
    }

    public static AppJobRunEntryHibernate create(long appJobId, long runId, long operationStatusId) {
        AppJobRunEntryHibernate hibernate = new AppJobRunEntryHibernate();
        hibernate.appJobId = appJobId;
        hibernate.runId = runId;
        hibernate.operationStatusId = operationStatusId;
        return hibernate;
    }

    @Override
    public Long getId() {
        return id;
    }

    public AppJobRunEntryParam getParam() {
        AppJobRunEntryParam param = new AppJobRunEntryParam();
        param.setId(id==null?AuditedParam.UNSAVED:id);
        param.setDoe(DaoUtils.readDate(doe));
        param.setAppJobId(appJobId);
        param.setRunId(runId);
        param.setTaskStatus(operationStatus);
        return param;
    }

    public void loadOperationStatus(OperationStatusDao dao) {
        operationStatus = dao.getOperationStatusById(operationStatusId);
    }
}
