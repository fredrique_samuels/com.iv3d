package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.api.AppJobParam;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/23.
 */
@Entity
@Table(name = "tt_app_jobs")
public class AppJobHibernate extends HibernateData {

    @Id @GeneratedValue @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "dlu") private String dlu;
    @Column(name = "bean_id") private String beanId;
    @Column(name = "description") private String description;
    @Column(name = "name") private String name;
    @Column(name = "tt_app_id") private Long appId;

    public AppJobHibernate() {
        dlu = doe = DaoUtils.dateNowForPersist();
    }

    public static AppJobHibernate createForBean(Long appId, String beanId) {
        AppJobHibernate appJobStored = new AppJobHibernate();
        appJobStored.beanId = beanId;
        appJobStored.appId = appId;
        return appJobStored;
    }

    @Override
    public Long getId() {
        return id;
    }

    public AppJobHibernate update(AppJobParam appJobParam) {
        boolean updated = false;
        if(updateDescription(appJobParam.getDescription())) {
            updated = true;
        }
        if(updateName(appJobParam.getName())) {
            updated = true;
        }

        if(updated) {
            dlu = DaoUtils.dateNowForPersist();
        }

        return this;
    }

    public AppJobParam getParam() {
        AppJobParam appJobParam = new AppJobParam();
        appJobParam.setId(id);
        appJobParam.setBeanId(beanId);
        appJobParam.setAppId(appId);
        appJobParam.setDoe(DaoUtils.readDate(doe));
        appJobParam.setDlu(DaoUtils.readDate(dlu));
        appJobParam.setName(name);
        appJobParam.setDescription(description);
        return appJobParam;
    }

    private boolean updateName(String newName) {
        if((this.name!=null && !description.equals(newName)) || newName!=null && !newName.equals(name)) {
            this.name = newName;
            return true;
        }
        return false;
    }

    private boolean updateDescription(String newDescription) {

        if(this.description!=null && !description.equals(newDescription)) {
            this.description = newDescription;
            return true;
        }
        if(newDescription!=null && !newDescription.equals(description)) {
            this.description = newDescription;
            return true;
        }
        return false;
    }

    public static Map<String, AppJobHibernate> toMap(List<AppJobHibernate> jobsForApp) {
        Function<AppJobHibernate, String> stringFunction = app -> app.beanId;
        return jobsForApp.stream().collect(Collectors.toMap(stringFunction, Function.identity()));
    }
}
