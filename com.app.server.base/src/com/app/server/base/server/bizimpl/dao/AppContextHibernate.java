package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.api.AppContextParam;
import com.app.server.base.server.api.AppJobParam;
import com.iv3d.common.core.Transformer;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.storage.AuditedParam;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/19.
 */
@javax.persistence.Entity
@Table(name = "tt_apps")
public class AppContextHibernate extends HibernateData {

    @Id @GeneratedValue @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "name") private String name;
    @Column(name = "package_Id") private String packageId;
    @Transient private Map<String, AppJobHibernate> jobs = new HashMap<>();

    public AppContextHibernate() {
        this.doe = DaoUtils.persist(new UtcSystemClock().now());
    }

    public AppContextHibernate update(AppContextParam param) {
        name=param.getName();
        packageId=param.getPackageId();
        return this;
    }

    public AppContextParam getParam() {
        return transformer.transform(this);
    }

    public void update(AppContext s) {
        this.name = s.getPackageId();
        this.packageId = s.getPackageId();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void loadJobs(AppJobDao dao) {
        jobs = AppJobHibernate.toMap(dao.getJobsForApp(this));
    }

    private static Transformer<AppContextHibernate, AppContextParam> transformer = from -> {
        AppContextParam param = new AppContextParam();
        param.setId(from.id==null? AuditedParam.UNSAVED:from.id);
        param.setDoe(DaoUtils.readDate(from.doe));
        param.setName(from.name);
        param.setPackageId(from.packageId);
        List<AppJobParam> jobParams = from.jobs.values()
                .stream()
                .map(appJobHibernate -> appJobHibernate.getParam())
                .collect(Collectors.toList());
        param.setJobs(jobParams);
        return param;
    };


    public void updateJobs(AppContextParam appContextParam) {
        for (AppJobParam jobParam : appContextParam.getJobs()) {
            String beanId = jobParam.getBeanId();
            AppJobHibernate jobHibernate = jobs.get(beanId);
            if (jobHibernate==null) {
                jobs.put(beanId, AppJobHibernate.createForBean(id, beanId).update(jobParam));
            } else {
                jobHibernate.update(jobParam);
            }
        }
    }

    public void saveJobs(AppJobDao appJobsDao) {
        jobs.values().stream().forEach(appJobHibernate -> appJobsDao.upsert(appJobHibernate));
    }
}
