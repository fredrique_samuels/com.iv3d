package com.app.server.base.server.bizimpl;

import com.iv3d.common.utils.JsonUtils;

import java.io.Serializable;

public class JobProcessInfo implements Serializable {
    private Long runId;
    private String appPackageId;
    private String beanId;
    private Long jobId;

    //Json
    public JobProcessInfo() {
    }

    public JobProcessInfo(String appPackageId, String beanId, Long jobId, Long runId) {
        this.appPackageId = appPackageId;
        this.beanId = beanId;
        this.jobId = jobId;
        this.runId = runId;
    }

    public String getAppPackageId() {
        return appPackageId;
    }

    public String getBeanId() {
        return beanId;
    }

    public Long getRunId() {
        return runId;
    }

    public Long getJobId() {
        return jobId;
    }

    public boolean isSameJob(JobProcessInfo other) {
        return other.appPackageId.equals(this.appPackageId) && other.beanId.equals(beanId) && other.jobId==jobId;
    }

    public static JobProcessInfo fromJson(String json) {
        return JsonUtils.readFromString(json, JobProcessInfo.class);
    }

}