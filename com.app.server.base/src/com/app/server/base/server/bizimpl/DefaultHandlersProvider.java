/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.server.bizimpl;

import com.app.server.base.common.inject.BeanInjector;
import com.app.server.base.server.*;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.server.AbstractController;
import com.iv3d.common.server.RequestPathResolver;
import com.iv3d.common.server.inject.HandlersProvider;
import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class DefaultHandlersProvider implements HandlersProvider {
	private static final Logger logger = Logger.getLogger(DefaultHandlersProvider.class);
	
	private final BeanInjector beanLoader;
	private final RequestPathResolver resolver;
    private AppTaskManager appTaskManager;
    private final LocalAppSettingsManager appSettingsManager;
	
	@Inject
	public DefaultHandlersProvider(
            LocalAppSettingsManager appSettingsManager,
            BeanInjector beanLoader,
            RequestPathResolver resolver,
            AppTaskManager AppTaskManager) {
		this.appSettingsManager = appSettingsManager;
		this.beanLoader = beanLoader;
		this.resolver = resolver;
        appTaskManager = AppTaskManager;
    }

	@Override
	public Handler[] get() {

		List<Handler> handlerList = new ArrayList<>();
        appSettingsManager.iterateAppContext(as -> {
            if(as.hasPublicCache()) {
                handlerList.add(getAppPublicCacheHandler(as));
            }
            loadControllers(as);
            loadServices(as);
            return null;
        });

		handlerList.add(resolver);
		handlerList.add(getCssResourceHandler());
		handlerList.add(getJsResourceHandler());
		handlerList.add(getImageResourceHandler());

        SessionManager sessionManager = new HashSessionManager();
        sessionManager.setSessionIdPathParameterName("none");
        handlerList.add(new SessionHandler(sessionManager));

		return handlerList.toArray(new Handler[]{});
	}

    private void loadServices(AppContext as) {
        List<AppTaskConfigImpl> tasks = as.getTasks();
        for (AppTaskConfig task : tasks) {
            try {
                AppTask bean = (AppTask) beanLoader.getInstanceByName(task.getBeanId(), AppInjectable.class);
                appTaskManager.add(as, task.getPeriod(), bean);
            } catch (Exception e) {
                logger.error("Error loading app task bean id='"+task.getBeanId()+"'");
                logger.error(ExceptionUtils.getStackTrace(e));
            }
        }
    }


    private Handler getImageResourceHandler() {
        String imagesFolder = ResourceLoader.getUrl("/etc/images/").toExternalForm();
        logger.info("Binding folder for images files->"+imagesFolder);
        return createResourceHandler(imagesFolder, "/res/images");
    }

	private Handler getJsResourceHandler() {
		String jsFolder = ResourceLoader.getUrl("/etc/js/").toExternalForm();
		logger.info("Binding folder for js files->"+jsFolder);
		return createResourceHandler(jsFolder, "/res/js");
	}

	private Handler getCssResourceHandler() {
		String cssFolder = ResourceLoader.getUrl("/etc/css/").toExternalForm();
		logger.info("Binding folder for css files->"+cssFolder);
		return createResourceHandler(cssFolder, "/res/css");
	}

	private void loadControllers(AppContext settings) {
		String[] controllerNames = settings.getControllers();
		for (String name : controllerNames) {
			try {
				AbstractController bean = (AbstractController) beanLoader.getInstanceByName(name, AppInjectable.class);
				resolver.addController(bean);			
			} catch (Exception e) {
				logger.error("Error loading bean id='"+name+"'");
				logger.error(ExceptionUtils.getStackTrace(e));
			}
		}
	}

	private Handler getAppPublicCacheHandler(AppContext settings) {
		String pubCacheDir = settings.getPublicCacheDir();
		String path = settings.getPublicCacheUrlPath();
		return createResourceHandler(pubCacheDir, path);
	}

	private Handler createResourceHandler(String baseDir, String urlPath) {
		ResourceHandler resourceHandler= new ResourceHandler();
		resourceHandler.setResourceBase(baseDir);
		resourceHandler.setDirectoriesListed(true);
		ContextHandler contextHandler= new ContextHandler(urlPath);
		contextHandler.setHandler(resourceHandler);
		return contextHandler;
	}

}
