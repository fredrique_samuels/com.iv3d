package com.app.server.base.server.bizimpl;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.AppTask;
import com.app.server.base.server.AppTaskManager;
import com.app.server.base.server.TimeDelay;
import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/08/05.
 */
public class AppTaskManagerImpl implements AppTaskManager {

    private Logger logger = Logger.getLogger(AppTaskManagerImpl.class);
    private final List<Thread> threads;

    public AppTaskManagerImpl() {
        this.threads = new ArrayList<>();
    }

    @Override
    public void add(AppContext appContext, TimeDelay delay, AppTask task) {
        TaskRunner taskRunner = new TaskRunner(appContext, delay, task);
        Thread thread = new Thread(taskRunner);
        thread.start();
        threads.add(thread);
    }

    private class TaskRunner implements Runnable {
        private final AppContext appContext;
        private final TimeDelay delay;
        private final AppTask task;

        public TaskRunner(AppContext appContext, TimeDelay delay, AppTask task) {
            this.appContext = appContext;
            this.delay = delay;
            this.task = task;
        }

        @Override
        public void run() {
            try {
                mainLoop();
            } catch (Exception e) {
                String msg = String.format("AppTask for bean %s stopped by an error", task.getClass());
                logger.warn(ExceptionUtils.getStackTrace(new AppTaskStoppedError(msg, e)));
            }
        }

        private void mainLoop() {
            while (true) {
                task.execute();
                delay.run();
            }
        }
    }

    private class AppTaskStoppedError extends Throwable {
        public AppTaskStoppedError(String message, Exception e) {
            super(message,e);
        }
    }
}
