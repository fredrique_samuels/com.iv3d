/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.server.bizimpl;

import com.app.server.base.server.*;
import com.app.server.base.server.api.AppContextParam;
import com.app.server.base.server.api.ServerDao;
import com.app.server.base.server.inject.InjectorProperties;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.server.inject.ServerConfigProvider;
import com.iv3d.common.storage.migrate.SchemaConfigManager;
import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
public class DefaultAppContextManager implements LocalAppSettingsManager {
	private static final Logger logger = Logger.getLogger(DefaultHandlersProvider.class);

	private final Map<String, DefaultAppContext> settingsMap= new HashMap<>();
    private final ServerConfigProvider serverConfigProvider;
    private final ServerDao serverDao;

    @Inject
	public DefaultAppContextManager(ServerConfigProvider serverConfigProvider, ServerDao serverDao) {
        this.serverConfigProvider = serverConfigProvider;
        this.serverDao = serverDao;
    }


	public void loadConfigs(AppJobManager appOperationManager, ServerDao serverDao, SchemaConfigManager schemaConfigManager) {
		validateAppDirExists();
		
		File[] appDirs = serverConfigProvider.get().getAppsDirAsFile().listFiles();
		for (File file : appDirs) {
			File appConfFile = new File(file, "app.json");
			if(!validateConfigFile(appConfFile)) {
				continue;
			}
			
			DefaultAppContext settings = JsonUtils.readFromFile(appConfFile, DefaultAppContext.class);
            settings.setAppOperationManager(()->appOperationManager);
			settings.setRootDir(file.getAbsolutePath());
			
			String packageId = settings.getPackageId();
			if(settingsMap.containsKey(packageId)) {
                throw new DuplicatePackageIdInUseError(String.format("Not binding package '%s' as it is already in use!", packageId));
			}
            settingsMap.put(packageId, settings);
		}
        schemaConfigManager.updateAll();
        List<AppContextParam> appContextParamList = settingsMap.values()
                .stream()
                .map(appContext -> appContext.getParams())
                .collect(Collectors.toList());
        serverDao.addServerRunEntry(appContextParamList);
    }

	private boolean validateConfigFile(File appConfFile) {
		boolean exists = appConfFile.exists();
		if(!exists) {
			String message = String.format("Application Config file %s does not exists.", appConfFile.getPath());
			logger.warn(message);
			return false;
		}
		return true;
	}

	private void validateAppDirExists() {
        File appsDir = serverConfigProvider.get().getAppsDirAsFile();
        if(!appsDir.exists()) {
			String message = String.format("The application directory '%s' does not exists.",
                    appsDir.getPath());
			logger.fatal(message);
			throw new RuntimeException(message);
		}

        if(!appsDir.isDirectory()) {
            String message = String.format("Path '%s' does not a directory.",
                    appsDir.getPath());
            logger.fatal(message);
            throw new RuntimeException(message);
        }
	}

	@Override
	public AppContext get(String packageId) {
		return settingsMap.get(packageId);
	}

    public AppContext getBeanParent(String taskId) {
        for(DefaultAppContext c : settingsMap.values()) {
            InjectorProperties injectorProperties = c.getInjectionBeanById(taskId);
            if (injectorProperties!=null) {
                return c;
            }
        }
        return null;
    }

    @Override
    public AppContextParam getAppByPackageId(String packageId) {
        return serverDao.getApplicationByPackageId(packageId);
    }

    public void iterateAppContext(Function<AppContext, Void> function) {
        settingsMap.values().stream().forEach(defaultAppContext -> function.apply(defaultAppContext));
    }

    private class DuplicatePackageIdInUseError extends RuntimeException {
        public DuplicatePackageIdInUseError(String msg) {
            super(msg);
        }
    }
}
