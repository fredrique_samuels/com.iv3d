package com.app.server.base.server.bizimpl;

import com.app.server.base.server.AppTaskConfig;
import com.app.server.base.server.DelayImpl;

/**
 * Created by fred on 2016/08/05.
 */
public class AppTaskConfigImpl implements AppTaskConfig {
    private DelayImpl delay;
    private String beanId;

    public void setDelay(DelayImpl delay) {
        this.delay = delay;
    }
    @Override
    public DelayImpl getPeriod() {
        return delay;
    }

    public String getBeanId() {
        return beanId;
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }
}
