package com.app.server.base.server.bizimpl;

import com.app.server.base.server.AppJobConfig;

/**
 * Created by fred on 2017/05/22.
 */
public class AppJobConfigImpl implements AppJobConfig {

    private String beanId;
    private String name;
    private String description;

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    @Override
    public String getBeanId() {
        return beanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
