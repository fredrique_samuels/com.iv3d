/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.server.bizimpl;

import com.app.server.base.server.*;
import com.app.server.base.server.api.AppContextParam;
import com.app.server.base.server.api.AppJobParam;
import com.app.server.base.server.inject.InjectorProperties;
import com.iv3d.common.core.Provider;
import com.iv3d.common.core.ResourceLoader;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultAppContext implements AppContext {
	private String packageId;
	private String[] controllers;
    private List<AppTaskConfigImpl> tasks = Collections.EMPTY_LIST;
    private List<AppJobConfigImpl> jobs = Collections.EMPTY_LIST;
	private File publicCacheDir;
    private File privateCacheDir;
	private List<InjectorProperties> injectionBeans;

    private Provider<AppJobManager> operationManagerProvider;

    public DefaultAppContext() {
		controllers = new String[]{};
        injectionBeans = Collections.EMPTY_LIST;
	}

	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String[] getControllers() {
		return controllers;
	}
	public void setControllers(String[] controllers) {
		this.controllers = controllers;
	}
	public String getPublicCacheUrlPath() {
		return String.format("/%s/st/pc", packageId);
	}
	public List<InjectorProperties> getInjectionBeans() {
		return injectionBeans!=null?Collections.unmodifiableList(injectionBeans):Collections.emptyList();
	}

    @Override
    public String getPrivateResource(String resource) {
        return getPrivateResourceFile(resource).getPath();
    }

    @Override
    public File getPrivateResourceFile(String resource) {
        String pd = ResourceLoader.getUrl(privateCacheDir.getPath()).getPath();
        return new File(pd, resource);
    }

    public void setInjectionBeans(List<InjectorProperties> injections) {
		this.injectionBeans = injections;
	}
	public void setRootDir(String rootDir) {
        this.publicCacheDir = new File(rootDir, "public_cache");
        this.privateCacheDir = new File(rootDir, "private_cache");
	}
	public String getPublicCacheDir() {
        return ResourceLoader.getUrl(publicCacheDir.getPath()).toExternalForm();
	}
	public boolean hasPublicCache() {
		return publicCacheDir!=null && publicCacheDir.exists();
	}
    public List<AppTaskConfigImpl> getTasks() {return Collections.unmodifiableList(tasks);}
    public void setTasks(List<AppTaskConfigImpl> tasks) {this.tasks = Collections.unmodifiableList(tasks);}

    public void setAppOperationManager(Provider<AppJobManager> operationManagerProvider) {
        this.operationManagerProvider = operationManagerProvider;
    }

    public void setJobs(List<AppJobConfigImpl> jobs) {
        this.jobs = jobs;
    }

    @Override
    public List<AppJobConfigImpl> getJobs() {
        return jobs;
    }

    public InjectorProperties getInjectionBeanById(String id) {
        for (InjectorProperties ip : injectionBeans) {
            if(id.equals(ip.getId())) {
                return ip;
            }
        }
        return null;
    }

    public AppContextParam getParams() {
        AppContextParam param = new AppContextParam();
        param.setName(getPackageId());
        param.setPackageId(getPackageId());
        param.setJobs(jobs.stream().map(job ->  {
            AppJobParam appJobParam = new AppJobParam();
            appJobParam.setBeanId(job.getBeanId());
            appJobParam.setName(job.getName());
            appJobParam.setDescription(job.getDescription());
            return appJobParam;
        }).collect(Collectors.toList()));
        return param;
    }

}
