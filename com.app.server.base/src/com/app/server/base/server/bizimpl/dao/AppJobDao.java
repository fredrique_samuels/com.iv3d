package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.api.AppContextParam;
import com.app.server.base.server.api.AppJobParam;
import com.app.server.base.server.api.AppJobRunEntryParam;
import com.app.server.base.server.api.OperationStatusParam;
import com.iv3d.common.pagination.PaginationParam;

import java.util.List;

/**
 * Created by fred on 2017/05/24.
 */
public interface AppJobDao {
    List<AppJobHibernate> getJobsForApp(AppContextHibernate app);
    PaginationParam<AppJobParam> getJobsForApp(AppContextParam app);
    PaginationParam<AppJobParam> getJobsForAppList(List<AppContextParam> app);
    void upsert(AppJobHibernate appJobHibernate);
    void saveRunEntry(AppJobRunEntryParam jobRunEntryParam);
    long getNextJobRunId(long jobId);

    AppJobParam getJobById(long jobId);
    OperationStatusParam getJobRunStatus(long jobId, long runId);

    AppJobRunEntryParam getLastJobRunEntry(long jobId);
    List<AppJobRunEntryParam> getJobRunEntryList(long jobId, Long maxRunId);
}
