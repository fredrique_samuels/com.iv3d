package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.api.ServerRunningAppParam;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by fred on 2017/05/21.
 */
@javax.persistence.Entity
@Table(name = "tt_server_run_apps")
public class ServerRunningAppHibernate extends HibernateData {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "server_run_id")
    private long serverRunId;

    @Column(name = "app_id")
    private long appId;

    public ServerRunningAppHibernate() {
    }

    public ServerRunningAppHibernate(long serverRunId, long appId) {
        this.serverRunId = serverRunId;
        this.appId = appId;
    }

    @Override
    public Long getId() {
        return id;
    }

    ServerRunningAppParam getParam(){
        ServerRunningAppParam param = new ServerRunningAppParam();
        param.setId(id);
        param.setAppId(appId);
        param.setServerRunId(serverRunId);
        return param;
    }
}
