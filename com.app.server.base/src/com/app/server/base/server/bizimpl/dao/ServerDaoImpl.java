package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.api.*;
import com.app.server.base.server.bizimpl.dao.serverdao.SaveServerRunHelper;
import com.google.inject.Inject;
import com.iv3d.common.pagination.PaginationParam;
import com.iv3d.common.storage.hibernate.AbstractHibernateDao;
import com.iv3d.common.storage.migrate.SchemaConfigManager;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/19.
 */
public class ServerDaoImpl extends AbstractHibernateDao implements ServerDao {

    @Inject
    public ServerDaoImpl(SchemaConfigManager schemaConfigManager) {
        super(schemaConfigManager.getConfig("tt_server"),
                ServerHibernate.class,
                ServerRunningAppHibernate.class,
                AppContextHibernate.class,
                OperationStatusHibernate.class,
                AppJobHibernate.class,
                AppJobRunEntryHibernate.class,
                AppJobRunIdSeedHibernate.class);
    }

    @Override
    public ServerParam addServerRunEntry(List<AppContextParam> values) {
        SaveServerRunHelper saveServerRunHelper = new SaveServerRunHelper(this);
        return saveServerRunHelper.addServerRunEntry(values);
    }

    public ServerParam getServer() {
        Object result = getCriteriaUniqueResult((session) -> {
            DetachedCriteria maxId = DetachedCriteria.forClass(ServerHibernate.class)
                    .setProjection( Projections.max("id") );
            return session.createCriteria(ServerHibernate.class)
                    .add( Property.forName("id").eq(maxId) );
        });

        if(result!=null) {
            ServerHibernate server = (ServerHibernate) result;
            return server.getParam();
        }
        return null;
    }

    @Override
    public AppContextParam getApplicationByPackageId(String packageId) {
        Object result = getUniqueResult((session) -> {
            String hbl = "FROM AppContextHibernate app WHERE app.packageId = :packageId";
            return session.createQuery(hbl).setString("packageId", packageId);
        });
        if(result!=null) {
            AppContextHibernate app = (AppContextHibernate) result;
            app.loadJobs(new AppJobDaoImpl(this, new OperationStatusDaoImpl(this)));
            return app.getParam();
        }
        return null;
    }

    @Override
    public AppContextParam getAppById(long appId) {
        AppContextHibernate hibernate = get(appId, AppContextHibernate.class);
        if(hibernate==null)return null;
        hibernate.loadJobs(new AppJobDaoImpl(this, new OperationStatusDaoImpl(this)));
        return hibernate.getParam();
    }

    @Override
    public PaginationParam<AppContextParam> getApplicationListForServer(ServerParam server) {
        List<ServerRunningAppHibernate> sraList = getObjectList((session) -> {
            String hbl = "FROM ServerRunningAppHibernate app WHERE app.serverRunId = :serverRunId";
            return session.createQuery(hbl).setLong("serverRunId", server.getId());
        });

        List<Long> appIds = sraList.stream().map(sra -> sra.getParam().getAppId()).collect(Collectors.toList());
        List<AppContextHibernate> appList = getObjectList((session) -> {
            String hbl = "FROM AppContextHibernate app WHERE app.id IN (:appIds)";
            return session.createQuery(hbl).setParameterList("appIds", appIds);
        });

        if(sraList.size()>0) {
            List<AppContextParam> params = appList.stream().map(app -> {
                app.loadJobs(new AppJobDaoImpl(this, new OperationStatusDaoImpl(this)));
                return app.getParam();
            }).collect(Collectors.toList());
            return new PaginationParam<>(1,1,params);
        }

        return new PaginationParam<>();
    }

}
