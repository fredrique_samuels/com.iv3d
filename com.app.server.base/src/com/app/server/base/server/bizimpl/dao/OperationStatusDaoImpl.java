package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.OperationStatusDao;
import com.app.server.base.server.api.OperationStatusParam;
import com.app.server.base.server.operations.results.OperationStatus;
import com.iv3d.common.storage.hibernate.AbstractHibernateDao;
import com.iv3d.common.storage.hibernate.DaoDelegate;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/25.
 */
public class OperationStatusDaoImpl extends DaoDelegate implements OperationStatusDao {
    public OperationStatusDaoImpl(AbstractHibernateDao dao) {
        super(dao);
    }

    @Override
    public long save(OperationStatusParam param) {
        OperationStatusHibernate hibernate = OperationStatusHibernate.createForParam(param);
        dao.saveObject(hibernate);

        param.getChildren().forEach(statusParam -> {
                statusParam.setParentId(hibernate.getId());
                save(statusParam);
            }
        );
        return hibernate.getId();
    }

    @Override
    public OperationStatusParam getOperationStatusById(long id) {
        OperationStatusHibernate hibernate = dao.get(id, OperationStatusHibernate.class);
        if(hibernate==null) return null;
        hibernate.loadChildren(this);
        return hibernate.getParam();
    }

    @Override
    public List<OperationStatusParam> getChildrenByParentId(long id) {
        List<OperationStatusHibernate> hibernateList = dao.getObjectList((session) ->
                session.createQuery("FROM OperationStatusHibernate a WHERE a.parentId = (:parentId)")
                        .setParameter("parentId", id)
        );
        hibernateList.stream().forEach( h -> h.loadChildren(this));
        return hibernateList.stream()
                .map( h -> h.getParam())
                .collect(Collectors.toList());
    }
}
