package com.app.server.base.server.bizimpl;

import com.app.server.base.server.*;
import com.app.server.base.server.api.*;
import com.app.server.base.server.bizimpl.dao.AppJobDao;
import com.app.server.base.server.bizimpl.dao.ServerDaoImpl;
import com.app.server.base.server.operations.OperationTaskDoneCallback;
import com.app.server.base.server.operations.OperationTaskManager;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/17.
 */
public class AppJobManagerImpl implements AppJobManager {

    private final OperationTaskProvider taskProvider;
    private final AppJobDao appJobsDao;
    private final ServerDao serverDao;
    private final OperationTaskManager operationTaskManager;

    public AppJobManagerImpl(OperationTaskProvider taskProvider, AppJobDao appJobsDao, ServerDaoImpl serverDao) {
        this.taskProvider = taskProvider;
        this.appJobsDao = appJobsDao;
        this.serverDao = serverDao;
        this.operationTaskManager = new OperationTaskManager(1, new OperationTaskDoneCallbackImpl());
    }

    @Override
    public OperationStatusParam runOperationTask(String appPackageId, String beanId) {
        try {
            OperationTask operationTask = taskProvider.get(appPackageId, beanId);
            return operationTaskManager.runTask(operationTask).getParams();
        } catch (Exception e) {
            return OperationTaskStatus.createBuilder(new JobProcessInfo(appPackageId, beanId, null, null), OperationState.ERROR)
                    .setError(e)
                    .build().getParams();
        }
    }


    @Override
    public OperationStatusParam runOperationTask(long appId, String beanId) {
        AppContextParam appById = serverDao.getAppById(appId);
        return runOperationTask(appById.getPackageId(), beanId);
    }

    @Override
    public List<OperationStatusParam> getQueueStatus() {
        return operationTaskManager.getQueueStatus()
                .stream()
                .map(p -> p.getParams())
                .collect(Collectors.toList());
    }

    @Override
    public OperationStatusParam getLastJobStatusFromStorage(long jobId) {
        AppJobRunEntryParam run = appJobsDao.getLastJobRunEntry(jobId);
        if(run==null)return OperationStatus.NULL.getParams();

        OperationStatusParam jobRunStatus = appJobsDao.getJobRunStatus(jobId, run.getRunId());
        if(jobRunStatus!=null)return jobRunStatus;

        return OperationStatus.NULL.getParams();
    }

    @Override
    public OperationStatusParam getJobStatusFromQueueOrStorage(long jobId, long runId) {
        AppJobParam appJobParam = appJobsDao.getJobById(jobId);
        if(appJobParam==null)return OperationStatus.NULL.getParams();

        AppContextParam appParam = serverDao.getAppById(appJobParam.getAppId());

        OperationStatus operationTaskStatus = operationTaskManager.queuedItemStatus(appParam.getPackageId(), appJobParam.getBeanId(), jobId, runId);
        if(operationTaskStatus!=null)return operationTaskStatus.getParams();

        OperationStatusParam statusParam = appJobsDao.getJobRunStatus(jobId, runId);
        if(statusParam!=null) return statusParam;

        return OperationStatus.NULL.getParams();
    }

    private class OperationTaskDoneCallbackImpl implements OperationTaskDoneCallback {

        @Override
        public void taskDone(String taskId, OperationStatusParam params) {
            JobProcessInfo appJobProcessInfo = JobProcessInfo.fromJson(taskId);
            AppJobRunEntryParam jobRunEntryParam = new AppJobRunEntryParam();
            jobRunEntryParam.setRunId(appJobProcessInfo.getRunId());
            jobRunEntryParam.setAppJobId(appJobProcessInfo.getJobId());
            jobRunEntryParam.setTaskStatus(params);

            appJobsDao.saveRunEntry(jobRunEntryParam);

        }
    }

    public List<AppJobRunEntryParam> getJobRunEntryList(Long jobId, Long maxRunId) {
        return appJobsDao.getJobRunEntryList(jobId, maxRunId);
    }

    @Override
    public OperationStatusParam getLastJobStatus(long jobId) {

        AppJobParam appJobParam = appJobsDao.getJobById(jobId);
        if(appJobParam==null)return OperationStatus.NULL.getParams();

        AppContextParam appParam = serverDao.getAppById(appJobParam.getAppId());

        List<OperationStatus> operationTaskStatuses = operationTaskManager.queuedJobStatusList(appParam.getPackageId(), appJobParam.getBeanId(), jobId);
        if(operationTaskStatuses.size()>0) {
            return operationTaskStatuses.get(operationTaskStatuses.size()-1).getParams();
        }

        return getLastJobStatusFromStorage(jobId);
    }
}
