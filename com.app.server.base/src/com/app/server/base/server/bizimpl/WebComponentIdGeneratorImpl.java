package com.app.server.base.server.bizimpl;

import com.app.server.base.server.WebComponentIdGenerator;
import com.google.inject.Singleton;

@Singleton
public class WebComponentIdGeneratorImpl implements WebComponentIdGenerator {
    private long seed;

    public WebComponentIdGeneratorImpl() {
        this.seed = 0;
    }

    @Override
    public String createId() {
        return "_ui_widget_"+seed+"_";
    }
}
