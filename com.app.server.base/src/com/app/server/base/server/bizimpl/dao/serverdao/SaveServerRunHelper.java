package com.app.server.base.server.bizimpl.dao.serverdao;

import com.app.server.base.server.api.AppContextParam;
import com.app.server.base.server.api.ServerParam;
import com.app.server.base.server.bizimpl.dao.*;
import com.iv3d.common.storage.hibernate.AbstractHibernateDao;
import com.iv3d.common.storage.hibernate.DaoDelegate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/25.
 */
public class SaveServerRunHelper extends DaoDelegate {
    public SaveServerRunHelper(AbstractHibernateDao dao) {
        super(dao);
    }

    public ServerParam addServerRunEntry(List<AppContextParam> values) {
        ServerHibernate persistedData = new ServerHibernate();
        dao.saveObject(persistedData);
        ServerParam param = persistedData.getParam();
        saveApps(param, values);
        return param;
    }

    private void saveApps(ServerParam serverParam, List<AppContextParam> currentAppsParams) {
        Map<String, AppContextHibernate> runInstanceApps = getPersistedAppData(currentAppsParams);
        upsertCurrentApps(runInstanceApps);
        saveInstanceAppLinks(serverParam, runInstanceApps);
        updateAppJobs(currentAppsParams, runInstanceApps);
        upsertAppJobs(runInstanceApps);
    }

    private Map<String, AppContextHibernate> getPersistedAppData(List<AppContextParam> currentAppsParams) {
        Map<String, AppContextHibernate> runInstanceApps = new HashMap<>();
        Map<String, AppContextHibernate> persistedApps = getAllPersistedApps();

        for (AppContextParam ac: currentAppsParams ) {
            String packageId = ac.getPackageId();
            AppContextHibernate stored = persistedApps.get(packageId);
            if(stored==null) {
                AppContextHibernate appContextStored = new AppContextHibernate();
                appContextStored.update(ac);
                runInstanceApps.put(packageId, appContextStored);
            } else {
                stored.update(ac);
                runInstanceApps.put(packageId, stored);
            }
        }
        return runInstanceApps;
    }

    private void upsertAppJobs(Map<String, AppContextHibernate> runInstanceApps) {
        runInstanceApps.values().stream().forEach((contextHibernate) -> contextHibernate.saveJobs(getAppJobsDao()));
    }

    private void updateAppJobs(List<AppContextParam> currentAppsParams, Map<String, AppContextHibernate> runInstanceApps) {
        Map<String, AppContextParam> appParamMap = currentAppsParams.stream().collect(Collectors.toMap(AppContextParam::getPackageId, Function.identity()));
        runInstanceApps.entrySet().stream().forEach((entry) -> entry.getValue().updateJobs(appParamMap.get(entry.getKey())));
    }

    private void saveInstanceAppLinks(ServerParam serverParam, Map<String, AppContextHibernate> runInstanceApps) {
        runInstanceApps.values().stream().forEach((appContextStored) -> dao.saveObject(new ServerRunningAppHibernate(serverParam.getId(), appContextStored.getId())));
    }

    private void upsertCurrentApps(Map<String, AppContextHibernate> runInstanceApps) {
        runInstanceApps.values().forEach((persistedData) -> dao.upsert(persistedData));
    }

    private Map<String, AppContextHibernate> getAllPersistedApps() {
        List<AppContextHibernate> dataList = dao.getObjectList((session)-> session.createQuery("from AppContextHibernate"));
        if(dataList==null) {
            return new HashMap<>();
        }

        dataList.stream().forEach(appContextHibernate -> appContextHibernate.loadJobs(getAppJobsDao()));

        Function<AppContextHibernate, String> keyTransformer = persistedData -> persistedData.getParam().getPackageId();
        Map<String, AppContextHibernate> result =
                dataList.stream().collect(Collectors.toMap(keyTransformer, Function.identity()));
        return result;
    }

    private AppJobDaoImpl getAppJobsDao() {
        return new AppJobDaoImpl(dao, new OperationStatusDaoImpl(dao));
    }
}
