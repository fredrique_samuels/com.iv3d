package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.api.ServerParam;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.storage.AuditedParam;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "tt_server_run")
public class ServerHibernate extends HibernateData {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "doe")
    private String doe;

    public ServerHibernate() {
        this.doe = DaoUtils.persist(new UtcSystemClock().now());
    }

    public void update(AuditedParam param) {
        id = param.getId();
        doe = DaoUtils.persist(param.getDoe());
    }

    public void populate(AuditedParam param) {
        param.setId(id==null? AuditedParam.UNSAVED:id);
        param.setDoe(DaoUtils.readDate(doe));
    }

    public ServerParam getParam() {
        ServerParam serverParam = new ServerParam();
        populate(serverParam);
        return serverParam;
    }

    @Override
    public Long getId() {
        return id;
    }
}
