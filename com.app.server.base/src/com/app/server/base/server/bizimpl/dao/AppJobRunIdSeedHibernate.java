package com.app.server.base.server.bizimpl.dao;

import com.app.server.base.server.api.AppJobRunIdSeedParam;
import com.iv3d.common.storage.AuditedParam;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;

/**
 * Created by fred on 2017/05/25.
 */
@Entity
@Table(name="tt_app_job_run_ids_seed")
public class AppJobRunIdSeedHibernate  extends HibernateData {
    @Id
    @GeneratedValue
    @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "tt_app_job_id") private long appJobId;
    @Column(name = "seed") private long seed;

    public AppJobRunIdSeedHibernate() {
        doe = DaoUtils.dateNowForPersist();
    }

    @Override
    public Long getId() {
        return id;
    }

    public static AppJobRunIdSeedHibernate create(long appJobId) {
        AppJobRunIdSeedHibernate hibernate = new AppJobRunIdSeedHibernate();
        hibernate.appJobId = appJobId;
        hibernate.seed = 0;
        return hibernate;
    }

    public final void update(AppJobRunIdSeedParam param) {
        this.seed = param.getSeed();
    }

    public final AppJobRunIdSeedParam getParam() {
        AppJobRunIdSeedParam param = new AppJobRunIdSeedParam();
        param.setId(id==null? AuditedParam.UNSAVED:id);
        param.setDoe(DaoUtils.readDate(doe));
        param.setAppJobId(appJobId);
        param.setSeed(seed);
        return param;
    }

}
