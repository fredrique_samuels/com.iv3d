package com.app.server.base.server;

import com.app.server.base.server.web.RequestHandler;
import com.iv3d.common.server.AbstractController;

public abstract class AppWebController extends AbstractController implements AppInjectable {
	protected final AppContext appContext;
	
	public AppWebController(AppContext settings) {
		super();
		this.appContext = settings;
	}
	
	protected final void bindPathHandler(String path, RequestHandler pathHandler) {
		if(path==null)return;
        pathHandler.setAppContext(appContext);
		set(buildPath(path), pathHandler);
	}

	private String buildPath(String path) {
		if(path.startsWith("/")) 
			return String.format("/%s%s", appContext.getPackageId(), path);
		return String.format("/%s/%s", appContext.getPackageId(), path);
	}
}
