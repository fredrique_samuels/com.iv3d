package com.app.server.base.server;

import com.app.server.base.server.api.OperationStatusParam;

import java.util.List;

/**
 * Created by fred on 2017/05/16.
 */
public interface OperationStatusDao {
    long save(OperationStatusParam statusParam);
    OperationStatusParam getOperationStatusById(long id);
    List<OperationStatusParam> getChildrenByParentId(long id);
}
