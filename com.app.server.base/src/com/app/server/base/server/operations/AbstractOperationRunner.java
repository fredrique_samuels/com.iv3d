package com.app.server.base.server.operations;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.api.AttributeParam;
import com.app.server.base.server.api.OperationState;
import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;
import com.iv3d.common.date.*;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/15.
 */
public abstract class AbstractOperationRunner<Context extends OperationContext> implements OperationRunner {
    private OperationStatus result;
    private RunnerStatus runnerStatus = new RunnerStatus();
    private List<AbstractOperationRunner<Context>> children = new ArrayList<>();
    private Context context;
    private static Logger logger = Logger.getLogger(AbstractOperationRunner.class);
    private boolean running = false;
    private UtcDate startDate = null;
    private UtcDate endDate = null;

    public final OperationStatus getResult(JobProcessInfo appJobProcessInfo) {
        if(result==null) {
            return buildStatus(running?OperationState.RUNNING:OperationState.WAITING, appJobProcessInfo);
        }
        return result;
    }

    protected abstract void run(Context context, JobProcessInfo appJobProcessInfo, OperationLogger operationLogger);

    public final void addChildOperationRunner(AbstractOperationRunner<Context> abstractOperationRunner){
        if(abstractOperationRunner ==this)return;
        this.children.add(abstractOperationRunner);
    }

    protected final void setContext(Context context) {
        this.context = context;
    }

    public final void start(AppContext appContext, JobProcessInfo appJobProcessInfo) {
        this.startDate = new UtcSystemClock().now();

        running = true;
        Context activeContext = getUsableContext(appContext);
        for (AbstractOperationRunner operation : children) {
            operation.setContext(activeContext);
            operation.start(appContext, appJobProcessInfo);
        }
        try {
            run(activeContext, appJobProcessInfo,runnerStatus.operationLogger);
        } catch (Exception e) {
            setErrorState(e);
        }

        endDate = new UtcSystemClock().now();
        this.result = buildStatus(null, appJobProcessInfo);
    }

    private OperationStatus buildStatus(OperationState operationState, JobProcessInfo appJobProcessInfo) {
        UtcDuration duration = new UtcSystemTimer().stop();

        if(startDate!=null) {
            if(endDate!=null) {
                duration = startDate.getDuration(endDate);
            } else {
                duration = startDate.getDuration(UtcDate.now());
            }
        }

        OperationTaskStatus.Builder builder = new OperationTaskStatus.Builder(duration, appJobProcessInfo, OperationState.RUNNING)
                .setName(runnerStatus.name);
        if(operationState==OperationState.RUNNING) {
            builder.setStatus(OperationState.RUNNING);
        }else if(operationState==OperationState.WAITING) {
            builder.setStatus(OperationState.WAITING);
        } else if(runnerStatus.state==OperationState.ERROR) {
            builder.setError(runnerStatus.error);
            builder.setStatus(OperationState.ERROR);
        } else if(runnerStatus.state==OperationState.WARNING) {
            builder.setStatus(OperationState.WARNING);
        } else {
            builder.setStatus(OperationState.SUCCESS);
        }
        builder.setChildren(children.stream().map( c -> {
            return c.getResult(appJobProcessInfo);
        })
            .collect(Collectors.toList()));
        ArrayList<AttributeParam> attributeParams = new ArrayList<>(runnerStatus.attributes);
        builder.setAttributes(attributeParams.stream()
                .filter(i -> i.getValue()!=null)
                .collect(Collectors.toMap(i -> i.getName(), i -> i.getValue())));
        builder.setLogs(runnerStatus.operationLogger.getAll());

        return builder.build();
    }

    private Context getUsableContext(AppContext appContext) {
        Context activeContext = this.context;
        if(activeContext==null) {
            activeContext = createContext(appContext);
        }
        return activeContext;
    }

    protected Context createContext(AppContext appContext){return null;}


    private class RunnerStatus {
        private OperationState state = OperationState.RUNNING;
        private OperationLoggerImpl operationLogger = new OperationLoggerImpl();
        private Exception error;
        public String name;
        public List<AttributeParam> attributes = new ArrayList<>();

        public void setError(Exception error) {
            this.error = error;
        }

        public Exception getError() {
            return error;
        }
    }

    protected void setSuccessState() {
        runnerStatus.state = OperationState.SUCCESS;
    }

    protected void setWarningState() {
        runnerStatus.state = OperationState.WARNING;
    }

    protected void setErrorState(Exception e) {
        runnerStatus.state = OperationState.ERROR;
        runnerStatus.error = e;
    }

    protected void setOperationName(String name) {
        runnerStatus.name = name;
    }

    protected void setOperationAttribute(String key, Object value) {
        runnerStatus.attributes.add(new AttributeParam(key,value));
    }

    protected void setOperationStatus(OperationStatus status) {
        setOperationName(status.getName());
        if(status.hasError() || status.getStatus()==OperationState.ERROR) {
            setErrorState(status.getError());
        } else {
            if(status.getStatus()==OperationState.WARNING) {
                setWarningState();
            }
        }
        List<AttributeParam> oldAttr = runnerStatus.attributes;
        runnerStatus.attributes = new ArrayList(status.getParams().getAttributes());
        runnerStatus.attributes.addAll(oldAttr);
    }

    private class OperationLoggerImpl implements OperationLogger, Serializable {
        List<LogEntry> logs = new ArrayList<>();

        public void info(String s) {
            logger.info(s);
            synchronized (logs) {
                appendLog(LogEntry.Level.INFO, s);
            }
        }

        public void warning(String s) {
            logger.warn(s);
            synchronized (logs) {
                appendLog(LogEntry.Level.WARNING, s);
            }
        }

        public void error(String s) {
            logger.error(s);
            synchronized (logs) {
                appendLog(LogEntry.Level.ERROR, s);
            }
        }

        public List<LogEntry> getAll() {
            synchronized (logs) {
                return Collections.unmodifiableList(logs);
            }
        }

        private void appendLog(LogEntry.Level level, String s) {
            UtcDate now = new UtcSystemClock().now();
            String entry = String.format("%s %s %s", now.toIso8601String(), level, s);
            logs.add(new LogEntry(level, entry, new UtcDateParam(now)));
        }
    }
}
