package com.app.server.base.server.operations.results;

public enum WritePolicy {
    NONE,
    REPLACE,
    SKIP_IF_EXISTS
}
