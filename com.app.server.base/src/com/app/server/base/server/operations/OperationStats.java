package com.app.server.base.server.operations;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fred on 2017/05/15.
 */
public final class OperationStats {
    private Map<String, String> values = new HashMap<>();
    public final void set(String key, String value){
        this.values.put(key, value);
    }
    public final Map<String, String> getAll(){return Collections.unmodifiableMap(values);}
    public final Object get(String key){return values.get(key);}

    public boolean hasKey(String key) {
        return values.containsKey(key);
    }
}
