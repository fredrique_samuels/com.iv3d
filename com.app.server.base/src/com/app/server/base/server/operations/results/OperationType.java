package com.app.server.base.server.operations.results;

/**
 * Created by fred on 2017/05/15.
 */
public interface OperationType {
    String getId();
}
