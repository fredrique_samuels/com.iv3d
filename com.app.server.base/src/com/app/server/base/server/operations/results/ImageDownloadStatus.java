package com.app.server.base.server.operations.results;

import com.iv3d.common.date.UtcDuration;
import com.iv3d.common.date.UtcSystemTimer;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2017/05/15.
 */
public class ImageDownloadStatus extends ResourceWriteStatus {
    public static final OperationType OPERATION_TYPE = () -> "IMAGE_DOWNLOAD";

    private final String url;

    public ImageDownloadStatus(String name, UtcDuration duration, String url, WritePolicy writePolicy, boolean createdOnWrite, boolean saved, File file, Exception error) {
        super(name, url, writePolicy, createdOnWrite, saved, file, error, Collections.EMPTY_LIST, duration);
        this.url = url;
    }

    public final String getUrl() {
        return url;
    }

    @Override
    public OperationType getType() {
        return OPERATION_TYPE;
    }

    public static Builder createBuilder(File file, String imageUrl, UtcDuration duration) {
        return new Builder(file, imageUrl, duration);
    }

    public static Builder createBuilder(File file, String imageUrl) {
        return new Builder(file, imageUrl, new UtcSystemTimer().stop());
    }

    public static final class Builder {
        private WritePolicy writePolicy = WritePolicy.NONE;
        private boolean createdOnWrite = false;
        private boolean saved = false;
        private Exception error;
        private final File file;
        private String url;
        private String name;
        private UtcDuration duration;
        private List<OperationStatus> children = Collections.emptyList();

        public Builder(File file, String url, UtcDuration duration) {
            this.file = file;
            this.url = url;
            this.duration = duration;
        }

        public final Builder setError(Exception error) {
            this.createdOnWrite = false;
            this.saved = false;
            this.writePolicy = WritePolicy.NONE;
            this.error = error;
            return this;
        }

        public final Builder setChildren(List<OperationStatus> children) {
            this.children = children;
            return this;
        }

        public final Builder setName(String name) {
            this.name = name;
            return this;
        }

        public final Builder setSkipped(WritePolicy writePolicy) {
            this.writePolicy = writePolicy;
            this.createdOnWrite = false;
            this.saved = false;
            this.error= null;
            return this;
        }

        public final Builder setSuccess(WritePolicy policy, boolean createdOnWrite) {
            this.saved = true;
            this.writePolicy = policy;
            this.createdOnWrite = createdOnWrite;
            this.error = null;
            return this;
        }

        public final ImageDownloadStatus build() {
            return new ImageDownloadStatus(name, duration, url, writePolicy, createdOnWrite, saved, file, error);
        }
    }

}
