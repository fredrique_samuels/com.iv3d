package com.app.server.base.server.operations.results;

import com.app.server.base.server.api.OperationStatusParam;
import com.iv3d.common.date.UtcDuration;
import com.iv3d.common.date.UtcSystemTimer;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class ResourceWriteStatus extends OperationStatus {
    public static final OperationType OPERATION_TYPE = () -> "RESOURCE_WRITE";

    public static final String ATTRIBUTE_FILE_NAME = "operation.status.attribute.file.name";
    public static final String ATTRIBUTE_FILE_WRITE_POLICY = "operation.status.attribute.file.writepolicy";
    public static final String ATTRIBUTE_FILE_SAVED = "operation.status.attribute.file.saved";
    public static final String ATTRIBUTE_FILE_CREATED = "operation.status.attribute.file.createdonwrite";
    public static final String ATTRIBUTE_FILE_SOURCE = "operation.status.attribute.file.source";

    private final String source;
    private final WritePolicy writePolicy;
    private final boolean createdOnWrite;
    private final boolean saved;
    private final File file;

    public ResourceWriteStatus(String name, String source, WritePolicy writePolicy, boolean createdOnWrite,
                               boolean saved, File file, Exception error,
                               List<OperationStatus> children,
                               UtcDuration duration) {
        super(name, error, children, duration, null);
        this.source = source;
        this.writePolicy = writePolicy;
        this.createdOnWrite = createdOnWrite;
        this.saved = saved;
        this.file = file;
    }

    public WritePolicy getWritePolicy() {
        return writePolicy;
    }

    public boolean isCreatedOnWrite() {
        return createdOnWrite;
    }

    public boolean isSaved() {
        return saved;
    }

    public File getFile() {
        return file;
    }

    public String getSource() {
        return source;
    }

    @Override
    public OperationType getType() {
        return OPERATION_TYPE;
    }

    public static long newFilesCount(List<OperationStatus> operationSummaries) {
        return operationSummaries.stream()
                .filter(operationSummary -> {
                    if(operationSummary.hasError())return false;
                    if(operationSummary.getType()!= OPERATION_TYPE)return false;
                    return ((ResourceWriteStatus)operationSummary).isCreatedOnWrite();
                }).count();
    }

    public void populate(OperationStatusParam statusParam) {
        super.populate(statusParam);
        statusParam.addAttribute(ATTRIBUTE_FILE_NAME, file.getName());
        statusParam.addAttribute(ATTRIBUTE_FILE_CREATED, createdOnWrite);
        statusParam.addAttribute(ATTRIBUTE_FILE_SAVED, saved);
        statusParam.addAttribute(ATTRIBUTE_FILE_SOURCE, source);
        statusParam.addAttribute(ATTRIBUTE_FILE_WRITE_POLICY, writePolicy.name());
    }

    public static Builder createBuilder(File file, UtcDuration duration) {
        return new Builder(file, duration);
    }

    public static Builder createBuilder(File file) {
        return new Builder(file, new UtcSystemTimer().stop());
    }

    public static final class Builder {
        private WritePolicy writePolicy = WritePolicy.NONE;
        private boolean createdOnWrite = false;
        private boolean saved = false;
        private Exception error;
        private final File file;
        private String source="";
        private String name="";
        private UtcDuration duration;
        private List<OperationStatus> children = Collections.emptyList();

        public Builder(File file, UtcDuration duration) {
            this.file = file;
            this.duration = duration;
        }

        public final Builder setError(Exception error) {
            this.createdOnWrite = false;
            this.saved = false;
            this.writePolicy = WritePolicy.NONE;
            this.error = error;
            return this;
        }

        public final Builder setChildren(List<OperationStatus> children) {
            this.children = children;
            return this;
        }

        public final Builder setSource(String source) {
            this.source = source!=null?source:"";
            return this;
        }

        public final Builder setName(String name) {
            this.name = name!=null?name:"";
            return this;
        }

        public final Builder setSkipped(WritePolicy writePolicy) {
            this.writePolicy = writePolicy;
            this.createdOnWrite = false;
            this.saved = false;
            this.error= null;
            return this;
        }

        public final Builder setSuccess(WritePolicy policy, boolean createdOnWrite) {
            this.saved = true;
            this.writePolicy = policy;
            this.createdOnWrite = createdOnWrite;
            this.error = null;
            return this;
        }

        public final ResourceWriteStatus build() {
            return new ResourceWriteStatus(name, source, writePolicy, createdOnWrite, saved, file, error, children, duration);
        }
    }
}
