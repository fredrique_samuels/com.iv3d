package com.app.server.base.server.operations;

import com.app.server.base.server.OperationTask;
import com.app.server.base.server.api.OperationState;
import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;
import com.google.inject.Inject;
import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;


public class OperationTaskManager {
    public static Logger logger = Logger.getLogger(OperationTaskManager.class);

    private final ExecutorService taskPool;
    private final ExecutorService taskCompletionPool;
    private OperationTaskDoneCallback taskDoneCallback;
    private SortedMap<String, TaskEntry> taskQueue = new TreeMap<>();

    @Inject
    public OperationTaskManager(int threadPoolSize,
                                OperationTaskDoneCallback taskDoneCallback) {
        this.taskPool = Executors.newFixedThreadPool(threadPoolSize);
        this.taskCompletionPool = Executors.newFixedThreadPool(threadPoolSize);
        this.taskDoneCallback = taskDoneCallback;
    }

    public OperationStatus runTask(OperationTask operationTask) {
        String processId = operationTask.getProcessId();
        TaskEntry entry = getEntry(processId);
        if(entry!=null) {
            return entry.getStatus();
        }
        CallableTask task = new CallableTask(processId, operationTask);
        Future<OperationTaskStatus> future = taskPool.submit(task);
        TaskEntry taskEntry = addTaskToQueue(new TaskEntry(future, task));
        return taskEntry.getStatus();
    }

    public OperationStatus queuedItemStatus(String packageId, String beanId, long jobId, long runId) {
        JobProcessInfo appJobProcessInfo = new JobProcessInfo(packageId, beanId, jobId, runId);
        TaskEntry entry = getEntry(JsonUtils.writeToString(appJobProcessInfo));
        if(entry!=null) {
            return entry.getStatus();
        }
        return null;
    }

    public List<OperationStatus> queuedJobStatusList(String packageId, String beanId, long jobId) {
        JobProcessInfo appJobProcessInfo = new JobProcessInfo(packageId, beanId, jobId, null);
        List<TaskEntry> entries = getEntries(appJobProcessInfo);
        return entries.stream().map( e -> e.getStatus()).collect(Collectors.toList());
    }


    private TaskEntry addTaskToQueue(TaskEntry taskEntry) {
        synchronized (taskQueue) {
            String id = taskEntry.callableTask.getId();
            boolean containsKey = taskQueue.containsKey(id);
            if(containsKey) {
                return taskQueue.get(id);
            }
            logger.info("Adding task to queue " + id);
            taskQueue.put(id, taskEntry);
        }
        return taskEntry;
    }

    private void removeTaskFromQueue(TaskEntry taskEntry) {
        synchronized (taskQueue) {
            String id = taskEntry.callableTask.getId();
            boolean containsKey = taskQueue.containsKey(id);
            if(containsKey) {
                logger.info("removing task from queue " + id);
                taskQueue.remove(id);
            }
        }
    }

    private TaskEntry getEntry(String id) {
        synchronized (taskQueue) {
            return taskQueue.get(id);
        }
    }


    private List<TaskEntry> getEntries(JobProcessInfo appJobProcessInfo) {
        synchronized (taskQueue) {
            return taskQueue.values().stream()
                    .filter( v -> v.getJobInfo().isSameJob(appJobProcessInfo))
                    .collect(Collectors.toList());
        }
    }

    public List<OperationStatus> getStatusList() {
        synchronized (taskQueue) {
            return taskQueue.values().stream()
                    .map(taskEntry -> taskEntry.getStatus())
                    .collect(Collectors.toList());
        }
    }

    public List<OperationStatus> getQueueStatus() {
        synchronized (taskQueue) {
            return taskQueue.values().stream()
                    .map(taskEntry -> taskEntry.getStatus())
                    .collect(Collectors.toList());
        }
    }

    private class CallableTask implements Callable<OperationTaskStatus> {
        private Boolean isRunning = false;
        private final String id;
        private final OperationTask operationTask;

        private CallableTask(String id, OperationTask operationTask) {
            this.id = id;
            this.operationTask = operationTask;
        }

        @Override
        public OperationTaskStatus call() throws Exception {
            setRunning();
            try {
                OperationStatus status = operationTask.execute();
                return OperationTaskStatus.createBuilder(operationTask.getProcessInfo(), OperationState.SUCCESS)
                        .setChildren(Arrays.asList(status))
                        .build();
            } catch (Exception e) {
                return OperationTaskStatus.createBuilder(operationTask.getProcessInfo(), OperationState.ERROR)
                        .setError(e)
                        .build();
            }
        }

        public void setRunning() {
            synchronized (isRunning) {
                isRunning = true;
                taskCompletionPool.submit(() -> {
                    try {
                        TaskEntry taskEntry = getEntry(id);
                        OperationTaskStatus futureTaskStatus = taskEntry.getFutureTaskStatus();
                        removeTaskFromQueue(taskEntry);
                        taskDoneCallback.taskDone(id, futureTaskStatus.getParams());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                });
            }
        }

        boolean isRunning () {
            synchronized (isRunning) {
                return isRunning;
            }
        }

        public String getId() {
            return id;
        }
    }

    private class TaskEntry {
        private final Future<OperationTaskStatus> future;
        private final CallableTask callableTask;

        public TaskEntry(Future<OperationTaskStatus> future, CallableTask callableTask) {
            this.future = future;
            this.callableTask = callableTask;
        }

        public JobProcessInfo getJobInfo() {
            return callableTask.operationTask.getProcessInfo();
        }

        OperationStatus getStatus() {
            if (future.isDone()) {
                return getFutureTaskStatus();
            }
            if(future.isCancelled()) {
                return OperationTaskStatus.createBuilder(callableTask.operationTask.getProcessInfo(), OperationState.STOPPED)
                    .build();
            }

            if(callableTask.operationTask.hasStarted()) {
                return callableTask.operationTask.getResult();
            }

            return OperationTaskStatus.createBuilder(callableTask.operationTask.getProcessInfo(), OperationState.WAITING)
                    .build();
        }

        private OperationTaskStatus getFutureTaskStatus() {
            try {
                return future.get();
            } catch (Exception e) {
                return OperationTaskStatus.createBuilder(callableTask.operationTask.getProcessInfo(), OperationState.ERROR)
                        .setError(e)
                        .build();
            }
        }
    }
}
