package com.app.server.base.server.operations;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcDateParam;

import java.io.Serializable;

/**
 * Created by fred on 2017/07/20.
 */
public class LogEntry implements Serializable {
   public enum Level {
        INFO,
        WARNING,
        ERROR
    }

    private final Level level;
    private final String message;
    private final UtcDateParam date;

    public LogEntry(Level level, String message, UtcDateParam date) {
        this.level = level;
        this.message = message;
        this.date = date;
    }

    public Level getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }

    public UtcDateParam getDate() {
        return date;
    }
}
