package com.app.server.base.server.operations.results;

import com.iv3d.common.date.UtcDuration;

/**
 * Created by fred on 2017/05/15.
 */
public class ParseStatus extends OperationStatus {
    public static final OperationType OPERATION_TYPE = () -> "DATA PARSE";

    public final String parseId;
    public final String source;

    public ParseStatus(UtcDuration duration, String parseId, String source, Exception error) {
        super("", error, duration);
        this.parseId = parseId;
        this.source = source;
    }

    public String getParseId() {
        return parseId;
    }

    public String getSource() {
        return source;
    }

    @Override
    public OperationType getType() {
        return OPERATION_TYPE;
    }
}
