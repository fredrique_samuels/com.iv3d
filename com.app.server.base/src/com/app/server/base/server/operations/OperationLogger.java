package com.app.server.base.server.operations;

/**
 * Created by fred on 2017/07/19.
 */
public interface OperationLogger {
    void info(String msg);
    void warning(String msg);
    void error(String msg);
}
