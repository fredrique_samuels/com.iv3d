package com.app.server.base.server.operations;

import com.app.server.base.server.AppContext;

/**
 * Created by fred on 2017/05/22.
 */
public interface OperationContextFactory {
    OperationContext create(AppContext appContext);
}
