package com.app.server.base.server.operations.results;

import com.app.server.base.server.api.OperationState;
import com.app.server.base.server.api.OperationStatusParam;
import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.LogEntry;
import com.iv3d.common.date.UtcDuration;
import com.iv3d.common.date.UtcSystemTimer;
import com.iv3d.common.utils.JsonUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by fred on 2017/05/16.
 */
public class OperationTaskStatus extends OperationStatus {

    public final static String ATTRIBUTE_TASK_RUN_ID = "operation.status.attribute.run.id";
    public final static String ATTRIBUTE_TASK_APP_PACKAGE = "operation.status.attribute.app.package";
    public final static String ATTRIBUTE_TASK_JOB_BEAN = "operation.status.attribute.job.bean";
    public final static String ATTRIBUTE_TASK_JOB_ID = "operation.status.attribute.job.id";
    public final static String ATTRIBUTE_TASK_LOGS= "operation.status.attribute.logs";

    private final JobProcessInfo processInfo;
    private final List<LogEntry> logs;
    private Map<String, Object> attributes;

    private OperationTaskStatus(String name, Exception error, List<OperationStatus> children, UtcDuration utcDuration, JobProcessInfo processInfo, OperationState status, List<LogEntry> logs, Map<String, Object> attributes) {
        super(name, error, children, utcDuration, status);
        this.processInfo = processInfo;
        this.logs = logs;
        this.attributes = attributes;
    }

    public void populate(OperationStatusParam statusParam) {
        super.populate(statusParam);
        statusParam.addAttribute(ATTRIBUTE_TASK_RUN_ID, processInfo.getRunId());
        statusParam.addAttribute(ATTRIBUTE_TASK_APP_PACKAGE, processInfo.getAppPackageId());
        statusParam.addAttribute(ATTRIBUTE_TASK_JOB_BEAN, processInfo.getBeanId());
        statusParam.addAttribute(ATTRIBUTE_TASK_JOB_ID, processInfo.getJobId());
        statusParam.addAttribute(ATTRIBUTE_TASK_LOGS, JsonUtils.writeToString(logs));
        attributes.entrySet().stream().forEach(
                e -> statusParam.addAttribute(e.getKey(), e.getValue())
        );
    }

    @Override
    public OperationType getType() {
        return () -> "TASK";
    }

    public static Builder createBuilder(JobProcessInfo processInfo, OperationState taskStatus) {
        return new Builder(new UtcSystemTimer().stop(), processInfo, taskStatus);
    }

    public static class Builder {
        private Exception error;
        private List<OperationStatus> children = Collections.EMPTY_LIST;
        private final UtcDuration utcDuration;
        private final JobProcessInfo processInfo;
        private OperationState taskStatus;
        private String name="";
        private List<LogEntry> logs = Collections.EMPTY_LIST;
        private Map<String, Object> attributes = Collections.EMPTY_MAP;

        public Builder(UtcDuration utcDuration, JobProcessInfo processInfo, OperationState taskStatus) {
            this.utcDuration = utcDuration;
            this.processInfo = processInfo;
            this.taskStatus = taskStatus;
        }

        public Builder setError(Exception error) {
            this.error = error;
            return this;
        }

        public Builder setChildren(List<OperationStatus> children) {
            this.children = children;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setLogs(List<LogEntry> logs) {
            this.logs = logs;
            return this;
        }

        public OperationTaskStatus build() {
            return new OperationTaskStatus(name, error, children, utcDuration, processInfo, taskStatus, logs, attributes);
        }

        public void setStatus(OperationState status) {
            this.taskStatus = status;
        }

        public void setAttributes(Map<String, Object> attributes) {
            this.attributes = attributes;
        }
    }

}
