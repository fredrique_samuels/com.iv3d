package com.app.server.base.server.operations.results;

import com.app.server.base.server.api.OperationState;
import com.app.server.base.server.operations.OperationStats;
import com.app.server.base.server.api.OperationStatusParam;
import com.iv3d.common.date.UtcDateParam;
import com.iv3d.common.date.UtcDuration;
import com.iv3d.common.date.UtcSystemTimer;
import com.iv3d.common.storage.AuditedParam;
import com.iv3d.common.utils.ExceptionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/15.
 */
public abstract class OperationStatus extends AuditedParam {

    public static final OperationStatus NULL = new OperationStatus("", null, new UtcSystemTimer().stop()) {
        @Override
        public OperationType getType() {
            return () -> "";
        }
    };

    private final String STATS_STATUS = "Status";
    private final String STATS_DURATION = "Duration";
    private final String STATS_ERROR_MSG = "Error Message";
    private final String STATS_ERROR_OP_TYPE = "Error Operation Type";
    private final String STATS_CLDRN_OPERATIONS = "Children Operations Count";
    private final String STATS_CLDRN_FAILED = "Children Operations Failed";
    private final String STATS_CLDRN_SUCCEEDED = "Children Operations Succeeded";
    private final String STATS_CLDRN_OPERATIONS_SWE = "Children Operations Succeeded With Errors";

    private final UtcDuration utcDuration;
    private final Exception error;
    private final String errorMessage;
    private final String errorTrace;
    private final boolean hasError;
    private final List<OperationStatus> children ;
    private final OperationState status;
    private final String name;

    public OperationStatus(String name, Exception error, List<OperationStatus> children, UtcDuration utcDuration, OperationState status) {
        this.name = name;
        this.errorMessage = error!=null?error.getMessage():"";
        this.errorTrace = error!=null? ExceptionUtils.getStackTrace(error):"";
        this.hasError = error!=null;
        this.error = error;
        this.utcDuration = utcDuration;
        this.children = Collections.unmodifiableList(children);
        this.status = status;
    }

    public OperationStatus(String name, Exception error, UtcDuration duration) {
        this(name, error, Collections.EMPTY_LIST, duration, OperationState.ERROR);
    }

    public UtcDuration getUtcDuration() {
        return utcDuration;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorTrace() {
        return errorTrace;
    }

    public Exception getError() {
        return error;
    }

    public boolean hasError() {
        return hasError;
    }

    public String getName() {
        return name;
    }

    public OperationState getStatus() {
        if (status!=null) {
            if(status!=OperationState.WARNING && status!=OperationState.SUCCESS) {
                return status;
            }
        }

        if(hasError) {
            return OperationState.ERROR;
        }

        if(errorCount(this.children) > 0 || sweCount(this.children) > 0) {
            return OperationState.WARNING;
        }


        return OperationState.SUCCESS;
    }

    public List<OperationStatus> getChildren() {
        return children;
    }

    public abstract OperationType getType();

    public static long errorCount(List<OperationStatus> operationStatus) {
        return operationStatus.stream().filter(result -> result.hasError()).count();
    }

    public static long sweCount(List<OperationStatus> operationStatus) {
        return operationStatus.stream().filter(result -> OperationState.WARNING.equals(result.getStatus())).count();
    }

    public static long successCount(List<OperationStatus> operationStatuses) {
        return operationStatuses.stream().filter(operationResult -> !operationResult.hasError()).count();
    }

    public static long errorCount(List<OperationStatus> operationStatuses, OperationType operationType) {
        return operationStatuses.stream()
                .filter(result -> {
                    if(!result.hasError())return false;
                    if(result.getType() != operationType)return false;
                    return true;
                }).count();
    }

    public static Builder createBuilder(String id) {
        return new Builder(id, new UtcSystemTimer().stop());
    }
    public static Builder createBuilder(String id, UtcDuration duration) {
        return new Builder(id, duration);
    }


    public final OperationStats getStats() {
        OperationStats operationStats = new OperationStats();
        populateStats(operationStats, this);
        return operationStats;
    }

    public void populateStats(OperationStats stats, OperationStatus operationStatus) {
        if(!stats.hasKey(STATS_STATUS)) {
            stats.set(STATS_STATUS, operationStatus.getStatus().name());
        }

        if(!stats.hasKey(STATS_DURATION)) {
            stats.set(STATS_DURATION, operationStatus.getUtcDuration().prettyPrint());
        }

        if(operationStatus.hasError()) {
            if(!stats.hasKey(STATS_ERROR_OP_TYPE)) {
                stats.set(STATS_STATUS, operationStatus.getType().getId());
            }

            if(!stats.hasKey(STATS_ERROR_MSG)) {
                stats.set(STATS_ERROR_MSG, operationStatus.getErrorMessage());
            }
        }

        List<OperationStatus> children = operationStatus.getChildren();
        if(children.size()>0) {
            if (!stats.hasKey(STATS_CLDRN_OPERATIONS)) {
                stats.set(STATS_CLDRN_OPERATIONS, String.valueOf(children.size()));
            }

            if (!stats.hasKey(STATS_CLDRN_FAILED)) {
                stats.set(STATS_CLDRN_FAILED, String.valueOf(errorCount(children)));
            }

            if (!stats.hasKey(STATS_CLDRN_OPERATIONS_SWE)) {
                stats.set(STATS_CLDRN_OPERATIONS_SWE, String.valueOf(sweCount(children)));
            }

            if (!stats.hasKey(STATS_CLDRN_SUCCEEDED)) {
                stats.set(STATS_CLDRN_SUCCEEDED, String.valueOf(successCount(children)));
            }
        }
    }

    public void populate(OperationStatusParam statusParam) {
        statusParam.setDurationInMs(this.utcDuration.getMillis());
        statusParam.setStartDate(new UtcDateParam(this.utcDuration.getStart()));
        statusParam.setChildren(this.children.stream().map( c -> c.getParams() ).collect(Collectors.toList()));
        statusParam.setError(hasError);
        statusParam.setErrorMessage(errorMessage);
        statusParam.setErrorTrace(errorTrace);
        statusParam.setType(getType().getId());
        statusParam.setId(getId());
        statusParam.setDoe(new UtcDateParam(getDoe()));
        statusParam.setStatus(getStatus());
        statusParam.setName(getName());
    }

    public final OperationStatusParam getParams() {
        OperationStatusParam operationStatusParam = new OperationStatusParam();
        this.populate(operationStatusParam);
        return operationStatusParam;
    }

    public static class Builder {

        private final String id;
        private UtcDuration duration;
        private List<OperationStatus> children = Collections.EMPTY_LIST;
        private Exception error;
        private OperationState status;
        private String name;
        private Map<String, Object> attributes = Collections.EMPTY_MAP;


        public Builder(String id, UtcDuration duration) {
            this.id = id;
            this.duration = duration;
        }

        public Builder setAttributes(Map<String, Object> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder setChildren(List<OperationStatus> children) {
            this.children = children;
            return this;
        }

        public Builder setError(Exception error) {
            this.error = error;
            return this;
        }

        public Builder setStatus(OperationState status) {
            this.status = status;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public OperationStatus build() {
            if(name==null)
                name=id;
            return new OperationStatus(name, error, children, duration, status) {
                @Override
                public OperationType getType() {
                    return () -> id;
                }

                @Override
                public void populate(OperationStatusParam statusParam) {
                    super.populate(statusParam);
                    attributes.entrySet()
                            .stream()
                            .forEach( e -> statusParam.addAttribute(e.getKey(), e.getValue()));
                }
            };
        }
    }

}
