package com.app.server.base.server.operations.results;

import com.iv3d.common.date.UtcDuration;
import com.iv3d.common.date.UtcSystemTimer;
import com.iv3d.common.rest.RestRequest;
import org.springframework.http.HttpMethod;

/**
 * Created by fred on 2017/05/15.
 */
public final class HttpRequestStatus extends OperationStatus {
    public static final OperationType OPERATION_TYPE = () -> "HTTP REQUEST";

    enum Method {
        GET,
        POST
    }

    private final String url;
    private final Method method;

    public HttpRequestStatus(String name, UtcDuration duration, String url, Method method, Exception error) {
        super(name, error, duration);
        this.url = url;
        this.method = method;
    }

    public final String getUrl() {
        return url;
    }

    public final Method getMethod() {
        return method;
    }

    @Override
    public OperationType getType() {
        return OPERATION_TYPE;
    }

    public static Builder createBuilder(RestRequest restRequest) {
        return new Builder(new UtcSystemTimer().stop(),
                restRequest.getUrl().getPath(),
                restRequest.getMethod()== HttpMethod.GET?Method.GET:Method.POST);
    }

    public static class Builder  {
        private UtcDuration duration;
        private final String url;
        private final Method method;
        private Exception error;
        private String name;

        public Builder(UtcDuration duration, String url, Method method) {
            this.duration = duration;
            this.url = url;
            this.method = method;
        }

        public Builder setError(Exception error) {
            this.error = error;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public HttpRequestStatus build() {
            return new HttpRequestStatus(name, duration, url, method, error);
        }
    }
}
