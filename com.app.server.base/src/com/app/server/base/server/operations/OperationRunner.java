package com.app.server.base.server.operations;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.AppInjectable;
import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.results.OperationStatus;

import java.io.Serializable;

/**
 * Created by fred on 2017/05/22.
 */
public interface OperationRunner extends AppInjectable {
    OperationStatus getResult(JobProcessInfo appJobProcessInfo);
    void start(AppContext appContext, JobProcessInfo appJobProcessInfo);
}
