package com.app.server.base.server.operations;

import com.app.server.base.server.AppJobManager;
import com.app.server.base.server.AppWebController;
import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.api.OperationStatusParam;
import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by fred on 2017/05/16.
 */
public class OperationTasksController extends AppWebController {

    @Inject
    private LocalAppSettingsManager settingsManager;

    @Inject
    private AppJobManager appJobManager;

    @Inject
    public OperationTasksController(LocalAppSettingsManager appSettingsManager) {
        super(appSettingsManager.get("server.admin"));
    }

    @Override
    public void configure() {
//        bindPathHandler("/", new AppPathHandler() {
//            @Override
//            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
//                OperationTaskStatus operationTaskStatus = appJobManager.runOperationTask("server.admin", "iv3d.apps.server.operations.demojob");
//                returnJsonParam(response, operationTaskStatus.getParams());
//            }
//
//            @Override
//            public String getMethod() {
//                return GET;
//            }
//        });

        bindPathHandler("/apps", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                OperationStatusParam params = appJobManager.runOperationTask("server.admin", "iv3d.apps.server.operations.demojob");
                returnJsonParam(response, params);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });
    }
}
