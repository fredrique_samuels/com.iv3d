package com.app.server.base.server.operations;

import com.app.server.base.server.api.OperationStatusParam;

/**
 * Created by fred on 2017/05/25.
 */
public interface OperationTaskDoneCallback {
    void taskDone(String taskId, OperationStatusParam params);
}
