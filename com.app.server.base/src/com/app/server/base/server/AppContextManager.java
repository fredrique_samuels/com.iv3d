package com.app.server.base.server;

import com.app.server.base.server.api.AppContextParam;

/**
 * Created by fred on 2017/05/24.
 */
public interface AppContextManager {
    AppContextParam getAppByPackageId(String packageId);
}
