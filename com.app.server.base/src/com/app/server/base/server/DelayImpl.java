package com.app.server.base.server;

import java.util.concurrent.TimeUnit;

/**
 * Created by fred on 2016/08/05.
 */
public final class DelayImpl implements TimeDelay {
    private long value;
    private String unit;

    public void setValue(long value) {
        this.value = value;
    }

    public void setUnit(String unitString) {
        this.unit = unitString;
    }

    public long getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }

    private TimeUnit getUnitFromString(String unitString) {
        if("milliseconds".equals(unitString))return TimeUnit.MILLISECONDS;
        if("seconds".equals(unitString))return TimeUnit.SECONDS;
        if("minutes".equals(unitString))return TimeUnit.MINUTES;
        if("hour".equals(unitString))return TimeUnit.HOURS;
        if("days".equals(unitString))return TimeUnit.DAYS;
        throw new UnknowTimeUnitError(unitString);
    }

    @Override
    public void run() {
        try {
            TimeUnit unitFromString = getUnitFromString(unit);
            Thread.sleep(unitFromString.toMillis(value));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private class UnknowTimeUnitError extends RuntimeException {
        public UnknowTimeUnitError(String unitString) {
            super("Unknown time unit =" + unitString);
        }
    }
}
