package com.app.server.base.server;

/**
 * Created by fred on 2016/08/05.
 */
public interface AppTask extends AppInjectable {
    void execute();
}
