/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.server.inject;

import com.app.server.base.common.inject.BeanInjector;
import com.app.server.base.macros.HtmlMacroService;
import com.app.server.base.macros.HtmlMacroServiceImpl;
import com.app.server.base.server.*;
import com.app.server.base.server.bizimpl.*;
import com.app.server.base.server.api.ServerDao;
import com.app.server.base.server.bizimpl.dao.AppJobDao;
import com.app.server.base.server.bizimpl.dao.AppJobDaoImpl;
import com.app.server.base.server.bizimpl.dao.OperationStatusDaoImpl;
import com.app.server.base.server.bizimpl.dao.ServerDaoImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.name.Names;
import com.iv3d.common.server.inject.HandlersProvider;
import com.iv3d.common.server.inject.ServerConfigProvider;
import com.iv3d.common.storage.migrate.SchemaConfigManager;
import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;

import java.util.List;

public class ServerInjectorModule extends AbstractModule {
	private static final Logger logger = Logger.getLogger(ServerInjectorModule.class);

	private Provider<BeanInjector> beanInjector;
    private SchemaConfigManager schemaConfigManager;
    private ServerConfigProvider serverConfigProvider = new ServerConfigProvider();

    public ServerInjectorModule(Provider<BeanInjector> beanInjector, SchemaConfigManager schemaConfigManager) {
		this.beanInjector = beanInjector;
        this.schemaConfigManager = schemaConfigManager;
    }

	@Override
	protected void configure() {

        bind(SchemaConfigManager.class).toInstance(schemaConfigManager);

        ServerDaoImpl serverDao = new ServerDaoImpl(schemaConfigManager);
        OperationStatusDaoImpl operationStatusDao = new OperationStatusDaoImpl(serverDao);
        AppJobDaoImpl appJobsDao = new AppJobDaoImpl(serverDao, operationStatusDao);


        OperationTaskProviderImpl operationTaskProvider = new OperationTaskProviderImpl(beanInjector, appJobsDao);
        AppJobManager appOperationManager = new AppJobManagerImpl(operationTaskProvider, appJobsDao,serverDao);
        bind(AppJobManager.class).toInstance(appOperationManager);

        bind(ServerDao.class).toInstance(serverDao);
        bind(OperationStatusDao.class).toInstance(operationStatusDao);
        bind(AppJobDao.class).toInstance(appJobsDao);

        DefaultAppContextManager appSettingsManager = new DefaultAppContextManager(serverConfigProvider, serverDao);
        appSettingsManager.loadConfigs(appOperationManager, serverDao, schemaConfigManager);

        operationTaskProvider.setAppManager(appSettingsManager);

        bind(ServerConfigProvider.class).toInstance(serverConfigProvider);
        bindBeanLoader();

        bindAppConfigCache(appSettingsManager);
        bindApplicationBeans(appSettingsManager);

        SessionManager sessionManager = new HashSessionManager();
        sessionManager.setSessionIdPathParameterName("./sessions");
        bind(SessionManager.class).toInstance(sessionManager);

        bind(HandlersProvider.class).to(DefaultHandlersProvider.class);

        bind(HandlersProvider.class).to(DefaultHandlersProvider.class);
        bind(HtmlMacroService.class).to(HtmlMacroServiceImpl.class);
        bind(WebComponentIdGenerator.class).to(WebComponentIdGeneratorImpl.class);
        bind(AppTaskManager.class).to(AppTaskManagerImpl.class);
        bind(SessionIdentifierGenerator.class);
	}

	private void bindApplicationBeans(DefaultAppContextManager appSettingsManager) {
        appSettingsManager.iterateAppContext(appContext -> {
            List<InjectorProperties> ijp = appContext.getInjectionBeans();
            for (InjectorProperties ijpp : ijp) {
                bindBeanOrLogError(ijpp);
            }
            return null;
        });
	}

	private void bindBeanOrLogError(InjectorProperties ijpp) {
		try {
			bindInjectionBean(ijpp);
		} catch (ClassNotFoundException e) {
			logger.error("Error binding bean for injection '"+ijpp.getId()+"'");
			logger.error(ExceptionUtils.getStackTrace(e));
		}
	}

	private void bindInjectionBean(InjectorProperties ijpp) throws ClassNotFoundException {
		@SuppressWarnings("unchecked")
		Class<AppInjectable> beanClass = (Class<AppInjectable>) Class.forName(ijpp.getBean());
		bind(AppInjectable.class)
			.annotatedWith(Names.named(ijpp.getId()))
			.to(beanClass);
	}

	private void bindAppConfigCache(final LocalAppSettingsManager appSettingsManager) {
		bind(LocalAppSettingsManager.class).toProvider(()-> appSettingsManager);
	}

	private void bindBeanLoader() {
		bind(BeanInjector.class).toProvider(()->beanInjector.get());
	}

}
