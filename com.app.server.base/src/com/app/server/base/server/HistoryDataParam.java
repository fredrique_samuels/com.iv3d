package com.app.server.base.server;

import java.io.Serializable;

/**
 * Created by fred on 2017/06/27.
 */
public class HistoryDataParam<T extends Serializable> implements Serializable {
    T data;
}
