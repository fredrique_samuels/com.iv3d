package com.app.server.base.server.admin;

import com.app.server.base.server.AppJobManager;
import com.app.server.base.server.api.*;
import com.app.server.base.server.bizimpl.dao.AppJobDao;
import com.app.server.base.server.operations.results.OperationTaskStatus;
import com.google.inject.Singleton;
import com.iv3d.common.cache.CacheParam;
import com.iv3d.common.pagination.PaginationParam;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/06/10.
 */
@Singleton
public class AdminManager {
    @Inject private ServerDao serverDao;
    @Inject private AppJobDao appJobDao;
    @Inject private AppJobManager appJobManager;

    // cached values
    private final CacheParam<ServerParam> serverParam;

    public AdminManager() {
        serverParam = new CacheParam<>(() -> serverDao.getServer());
    }

    public PaginationParam<AppContextParam> getAppList() {
        ServerParam server = getServer();
        return serverDao.getApplicationListForServer(server);
    }

    public PaginationParam<AppJobParam> getJobList() {
        PaginationParam<AppContextParam> appList = getAppList();
        return appJobDao.getJobsForAppList(appList.getResults());
    }

    public OperationStatusParam getAppOperationStatus(){
        return null;
    }

    public ServerParam getServer() {
        return serverParam.get();
    }

    public OperationStatusParam runOperationTask(String appPackage, String beanId) {
        return appJobManager.runOperationTask(appPackage, beanId);
    }

    public OperationStatusParam runOperationTask(long appId, String beanId) {
        return appJobManager.runOperationTask(appId, beanId);
    }

    public OperationStatusParam getJobStatus(long jobId) {
        return appJobManager.getLastJobStatus(jobId);
    }

    public List<JobRunEntrySummary> getJobRunStatusHistory(Long jobId, Long maxRunId) {
        List<AppJobRunEntryParam> runHistory = appJobManager.getJobRunEntryList(jobId, maxRunId);
        return runHistory.stream()
            .map( r -> {
                JobRunEntrySummary summary = new JobRunEntrySummary();
                    summary.setJobId(r.getAppJobId());
                    summary.setRunId(r.getRunId());
                    summary.setStatus(r.getTaskStatus().getStatus());
                    return summary;
                })
            .collect(Collectors.toList());
    }

    public AppContextParam getAppByPackageId(String appPackageId) {
        return serverDao.getApplicationByPackageId(appPackageId);
    }

    public OperationStatusParam getJobStatusForRun(Integer jobId, Integer runId) {
        return appJobManager.getJobStatusFromQueueOrStorage(jobId, runId);
    }
}
