package com.app.server.base.server.admin.web;

import com.app.server.base.server.AppJobManager;
import com.app.server.base.server.AppWebController;
import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.admin.AdminManager;
import com.app.server.base.server.api.*;
import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;
import com.iv3d.common.pagination.PaginationParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by fred on 2017/05/16.
 */
public class AdminController extends AppWebController {

    @Inject
    private LocalAppSettingsManager settingsManager;

    @Inject
    private AdminManager adminManager;


    @Inject
    private AppJobManager appJobManager;

    @Inject
    public AdminController(LocalAppSettingsManager appSettingsManager) {
        super(appSettingsManager.get("server.admin"));
    }

    @Override
    public void configure() {

        RequestHandler demoJobScheduler = new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                String appPackage = "server.admin";
                String jobId = "iv3d.apps.server.operations.demojob";
                OperationStatusParam statusParam = adminManager.runOperationTask(appPackage, jobId);
                returnJsonParam(response, statusParam);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        };
        bindPathHandler("/jobs/demo", demoJobScheduler);

        bindPathHandler("/jobs/run", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                String appPackage = getParameter("appPackageId");
                Integer appId = getParameterInteger("appId");
                String beanId = getRequiredParameter("jobBeanId");
                if(appPackage!=null) {
                    OperationStatusParam statusParam = adminManager.runOperationTask(appPackage, beanId);
                    returnJsonParam(response, statusParam);
                } else {
                    OperationStatusParam statusParam = adminManager.runOperationTask(appId, beanId);
                    returnJsonParam(response, statusParam);
                }
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });

        bindPathHandler("/apps", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                PaginationParam<AppContextParam> appList = adminManager.getAppList();
                returnJsonParam(response, appList);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });

        bindPathHandler("/app", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                String appPackageId = getRequiredParameter("packageId");
                AppContextParam app = adminManager.getAppByPackageId(appPackageId);
                returnJsonParam(response, app);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });

        bindPathHandler("/jobs", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                PaginationParam<AppJobParam> jobList = adminManager.getJobList();
                returnJsonParam(response, jobList);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });
        bindPathHandler("/jobs/status", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                Integer jobId = getParameterIntegerRequired("jobId");
                Integer runId = getParameterInteger("runId");
                if(runId==null) {
                    OperationStatusParam statusParam = adminManager.getJobStatus(jobId);
                    returnJsonParam(response, statusParam);
                } else {
                    OperationStatusParam statusParam = adminManager.getJobStatusForRun(jobId, runId);
                    returnJsonParam(response, statusParam);
                }
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });
        bindPathHandler("/jobs/run/history", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                Long jobId = getParameterLongRequired("jobId");
                Long runId = getParameterLong("runId");
                List<JobRunEntrySummary> jobRunEntryParams = adminManager.getJobRunStatusHistory(jobId, runId);
                returnJsonParam(response, jobRunEntryParams);
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });

        bindPathHandler("/server", new RequestHandler() {
            @Override
            protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
                returnJsonParam(response, adminManager.getServer());
            }

            @Override
            public String getMethod() {
                return GET;
            }
        });
    }
}
