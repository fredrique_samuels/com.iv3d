/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.server;

import com.app.server.base.server.bizimpl.AppJobConfigImpl;
import com.app.server.base.server.bizimpl.AppTaskConfigImpl;
import com.app.server.base.server.inject.InjectorProperties;
import com.app.server.base.server.operations.OperationContext;
import com.app.server.base.server.operations.results.OperationStatus;

import java.io.File;
import java.util.List;

public interface AppContext extends OperationContext {
	String getPackageId();
	String getPublicCacheDir();
	String getPublicCacheUrlPath();
	boolean hasPublicCache();
	String[] getControllers();
    List<AppTaskConfigImpl> getTasks();
    List<AppJobConfigImpl> getJobs();
	List<InjectorProperties> getInjectionBeans();
    String getPrivateResource(String resource);
    File getPrivateResourceFile(String resource);
    InjectorProperties getInjectionBeanById(String taskId);
}
