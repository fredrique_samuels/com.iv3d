/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.app.server.base.server;

import com.app.server.base.common.inject.CommonInjectorModule;
import com.app.server.base.common.inject.InjectorProvider;
import com.app.server.base.server.inject.ServerInjectorModule;
import com.app.server.base.server.schema.DefaultSchemaConfigManager;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.iv3d.common.storage.migrate.SchemaConfigManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class AppServerLauncher {

    static class Module extends  AbstractModule {
        @Override
        protected void configure() {
            bind(Name.class).to(NameJohn.class);
        }
    }

    public final void run(AbstractModule ... modules) {
        this.run(new DefaultSchemaConfigManager(), modules);
    }

    public final void run(SchemaConfigManager schemaConfigManager, AbstractModule ... modules) {

        InjectorProvider injectorProvider = new InjectorProvider();
        ServerInjectorModule serverInjectorModule = new ServerInjectorModule(injectorProvider, schemaConfigManager);

        List<AbstractModule> defaultModules = Arrays.asList(new CommonInjectorModule(), serverInjectorModule);
        ArrayList<AbstractModule> arrayList = new ArrayList();
        arrayList.addAll(defaultModules);
        arrayList.addAll(Arrays.asList(modules));

        Injector injector = Guice.createInjector(arrayList);
        injectorProvider.setInjector(injector);

        ProjectManagerService server = injector.getInstance(ProjectManagerService.class);
        server.run();
    }

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new Module());
        EntryPoint instance = injector.getInstance(EntryPoint.class);
        instance.run();
    }

    static class Name {
        String get(){return "NoOne";}
    }

    static class NameFred extends Name {
        public String get() {
            return "Fred";
        }
    }

    static class NameJohn extends Name {
        public String get() {
            return "John";
        }
    }

    static class EntryPoint {

        @Inject
        Name name;

        public EntryPoint() {
        }

        void run(){
            System.out.println("runnning " + name.get());
        }
    }

}
