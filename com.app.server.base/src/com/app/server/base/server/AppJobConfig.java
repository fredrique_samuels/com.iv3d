package com.app.server.base.server;

/**
 * Created by fred on 2017/05/22.
 */
public interface AppJobConfig {
    String getBeanId();
    String getName();
    String getDescription();
}
