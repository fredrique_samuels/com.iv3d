package com.app.server.base.server;

import com.google.inject.Inject;
import com.iv3d.common.core.Callback;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.SessionManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by fred on 2017/09/19.
 */
public class HttpSessionService {
    private static Logger logger = Logger.getLogger(HttpSessionService.class);

    private Callback<HttpSession> sessionAttributesPopulate;
    private Callback<HttpSession> sessionAttributesClear;
    private String sessionIdAttribute="TVD_JSESSIONID";
    private long sessionExpirePeriod = 3600L * 1000L;

    private SessionManager sessionManager;

    @Inject
    public HttpSessionService(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public final HttpSession login(Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        HttpSession session = createAndPopulateNewSession(request, httpServletRequest);
        setSessionCookie(httpServletResponse, session);
        return session;
    }

    public final HttpSession getActiveSession(HttpServletRequest request) {
        String currentSessionId = getCurrentSessionId(request);
        if(currentSessionId==null) {
            return null;
        }

        HttpSession session = sessionManager.getHttpSession(currentSessionId);
        if(session==null) {
            return null;
        }

        if(session.getId()==null) {
            return null;
        }

        return session;
    }

    public final String logout(HttpServletRequest httpServletRequest) {
        return closeSession(httpServletRequest);
    }

    public final void setSessionIdAttribute(String sessionIdAttribute) {
        this.sessionIdAttribute = sessionIdAttribute;
    }

    public final void setSessionExpirePeriod(long sessionExpirePeriod) {
        this.sessionExpirePeriod = sessionExpirePeriod;
    }

    @Inject
    public final void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public void setSessionAttributesPopulate(Callback<HttpSession> sessionAttributesPopulate) {
        this.sessionAttributesPopulate = sessionAttributesPopulate;
    }

    public void setSessionAttributesClear(Callback<HttpSession> sessionAttributesClear) {
        this.sessionAttributesClear = sessionAttributesClear;
    }

    private void setSessionCookie(HttpServletResponse response, HttpSession session) {
        Date expdate= new Date();
        expdate.setTime (expdate.getTime() + sessionExpirePeriod);
        DateFormat df = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss zzz");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String cookieExpire = " expires=" + df.format(expdate);
        response.addHeader("Set-Cookie",sessionIdAttribute+"="+session.getId()+"; Path=/;" + cookieExpire);
    }

    private HttpSession createAndPopulateNewSession(Request request, HttpServletRequest httpServletRequest) {
        closeSession(httpServletRequest);
        HttpSession session = httpServletRequest.getSession(true);
        request.setSession(session);
        logger.info("Starting Session "+ session.getId());

        if(sessionAttributesPopulate!=null) {
            sessionAttributesPopulate.invoke(session);
        }
        return session;
    }


    private String getCurrentSessionId(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if(cookies==null)
            return null;

        for (Cookie c:cookies) {
            if(c.getName().equals(sessionIdAttribute)) {
                return c.getValue();
            }
        }
        return null;
    }

    private String closeSession(HttpServletRequest request) {
        String jsessionid = getCurrentSessionId(request);
        if(jsessionid != null)
        {
            HttpSession session = sessionManager.getHttpSession(jsessionid);
            if(session!=null) {
                String id = session.getId();
                logger.info("Ending Session "+ id);
                session.invalidate();
            }
        }
        return jsessionid;
    }

}
