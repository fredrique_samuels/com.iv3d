package com.app.server.base.server;

import com.app.server.base.server.bizimpl.JobProcessInfo;
import com.app.server.base.server.operations.results.OperationStatus;
import com.app.server.base.server.operations.results.OperationTaskStatus;

/**
 * Created by fred on 2017/05/16.
 */
public interface OperationTask {
    String getProcessId();
    JobProcessInfo getProcessInfo();
    OperationStatus execute();
    OperationStatus getResult();
    boolean hasStarted();
}
