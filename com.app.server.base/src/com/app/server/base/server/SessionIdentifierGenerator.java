package com.app.server.base.server;

import com.google.inject.Singleton;

import java.security.SecureRandom;
import java.math.BigInteger;

@Singleton
public final class SessionIdentifierGenerator {
  private SecureRandom random = new SecureRandom();

  public String nextSessionId() {
    return new BigInteger(130, random).toString(32);
  }
}