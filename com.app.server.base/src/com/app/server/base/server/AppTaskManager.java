package com.app.server.base.server;

/**
 * Created by fred on 2016/08/05.
 */
public interface AppTaskManager {
    void add(AppContext as, TimeDelay period, AppTask bean);
}
