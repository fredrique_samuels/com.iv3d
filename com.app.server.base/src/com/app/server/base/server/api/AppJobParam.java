package com.app.server.base.server.api;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2017/05/23.
 */
public class AppJobParam extends AuditedParam {
    private UtcDate dlu;
    private String beanId;
    private String name;
    private String description;
    private Long appId;

    public String getBeanId() {
        return beanId;
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UtcDate getDlu() {
        return dlu;
    }

    public void setDlu(UtcDate dlu) {
        this.dlu = dlu;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getAppId() {
        return appId;
    }
}
