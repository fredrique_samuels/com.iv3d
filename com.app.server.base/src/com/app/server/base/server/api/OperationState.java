package com.app.server.base.server.api;

import java.io.Serializable;

/**
 * Created by fred on 2017/05/15.
 */
public enum OperationState implements Serializable {
    WAITING,
    RUNNING,
    ERROR,
    WARNING,
    SUCCESS,
    STOPPED
}
