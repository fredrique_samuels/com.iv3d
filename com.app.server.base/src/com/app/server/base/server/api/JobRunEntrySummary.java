package com.app.server.base.server.api;

/**
 * Created by fred on 2017/06/28.
 */
public class JobRunEntrySummary {
    private long jobId;
    private long runId;
    private OperationState status;

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public long getRunId() {
        return runId;
    }

    public void setRunId(long runId) {
        this.runId = runId;
    }

    public OperationState getStatus() {
        return status;
    }

    public void setStatus(OperationState status) {
        this.status = status;
    }
}
