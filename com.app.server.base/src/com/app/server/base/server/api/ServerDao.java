package com.app.server.base.server.api;

import com.iv3d.common.pagination.PaginationParam;

import java.util.List;

/**
 * Created by fred on 2017/05/19.
 */
public interface ServerDao {
    ServerParam addServerRunEntry(List<AppContextParam> values);
    ServerParam getServer();
    AppContextParam getApplicationByPackageId(String packageId);
    AppContextParam getAppById(long appId);
    PaginationParam<AppContextParam> getApplicationListForServer(ServerParam server);
}
