package com.app.server.base.server.api;

import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2017/05/25.
 */
public class AppJobRunEntryParam extends AuditedParam {
    private long appJobId;
    private long runId;
    private OperationStatusParam taskStatus;

    public long getAppJobId() {
        return appJobId;
    }

    public void setAppJobId(long appJobId) {
        this.appJobId = appJobId;
    }

    public long getRunId() {
        return runId;
    }

    public void setRunId(long runId) {
        this.runId = runId;
    }

    public OperationStatusParam getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(OperationStatusParam taskStatus) {
        this.taskStatus = taskStatus;
    }
}
