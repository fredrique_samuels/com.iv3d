package com.app.server.base.server.api;

import java.io.Serializable;

/**
 * Created by fred on 2017/06/14.
 */
public class AttributeParam implements Serializable {
    private String name;
    private Object value;

    public AttributeParam() {
    }

    public AttributeParam(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
