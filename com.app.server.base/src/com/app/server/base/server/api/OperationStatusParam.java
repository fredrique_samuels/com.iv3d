package com.app.server.base.server.api;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcDateParam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/05/16.
 */
public class OperationStatusParam implements Serializable {
    private long id=-1;
    private UtcDateParam doe;
    private Long parentId;
    private UtcDateParam startDate;
    private long durationInMs;
    private String errorMessage;
    private String errorTrace;
    private boolean error;
    private String type;
    private List<OperationStatusParam> children = Collections.EMPTY_LIST;
    private List<AttributeParam> attributes = new ArrayList<>();
    private OperationState status;
    private String name;


    public UtcDateParam getStartDate() {
        return startDate;
    }

    public void setStartDate(UtcDateParam startDate) {
        this.startDate = startDate;
    }

    public long getDurationInMs() {
        return durationInMs;
    }

    public void setDurationInMs(long durationInMs) {
        this.durationInMs = durationInMs;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorTrace() {
        return errorTrace;
    }

    public void setErrorTrace(String errorTrace) {
        this.errorTrace = errorTrace;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<OperationStatusParam> getChildren() {
        return children;
    }

    public void setChildren(List<OperationStatusParam> children) {
        this.children = children;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public List<AttributeParam> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeParam> attributes) {
        this.attributes = attributes;
    }

    public <T> T readAttribute(String key, Class<T> aClass) {
        AttributeParam attribute = attributebyName(key);
        if(attribute==null) return null;
        Object o = attribute.getValue();
        if(o==null)return null;
        return aClass.cast(o);
    }

    public void setDoe(UtcDateParam doe) {
        this.doe = doe;
    }

    public UtcDateParam getDoe() {
        return doe;
    }

    public void addAttribute(String key, Object value) {

        AttributeParam attributeParam = attributebyName(key);
        if(attributeParam!=null) {
            attributeParam.setValue(value);
        } else {
            AttributeParam param = new AttributeParam();
            param.setName(key);
            param.setValue(value);
            attributes.add(param);
        }

    }

    public OperationState getStatus() {
        return status;
    }

    public void setStatus(OperationState status) {
        this.status = status;
    }

    private AttributeParam attributebyName(String key) {
        List<AttributeParam> collect = attributes.stream().filter(e -> e.getName().equals(key)).collect(Collectors.toList());
        return collect.size()>0 ? collect.get(0) : null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
