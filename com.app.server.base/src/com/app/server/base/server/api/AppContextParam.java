package com.app.server.base.server.api;

import com.iv3d.common.storage.AuditedParam;

import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2017/05/20.
 */
public class AppContextParam extends AuditedParam {
    private String name;
    private String packageId;
    private List<AppJobParam> jobs = Collections.EMPTY_LIST;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public List<AppJobParam> getJobs() {
        return jobs;
    }

    public void setJobs(List<AppJobParam> jobs) {
        this.jobs = jobs;
    }
}
