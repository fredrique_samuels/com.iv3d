package com.app.server.base.server.api;

/**
 * Created by fred on 2017/06/10.
 */
public class ServerRunningAppParam {
    private long id;
    private long serverRunId;
    private long appId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getServerRunId() {
        return serverRunId;
    }

    public void setServerRunId(long serverRunId) {
        this.serverRunId = serverRunId;
    }

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }
}
