package com.app.server.base.server.api;

import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2017/05/25.
 */
public class AppJobRunIdSeedParam extends AuditedParam {
    private long appJobId;
    private long seed;

    public long getAppJobId() {
        return appJobId;
    }

    public void setAppJobId(long appJobId) {
        this.appJobId = appJobId;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public long nextId() {
        return ++seed;
    }
}
