package com.app.server.base.server.schema;

import com.iv3d.common.storage.migrate.SchemaConfigManager;

/**
 * Created by fred on 2017/08/17.
 */
public class DefaultSchemaConfigManager extends SchemaConfigManager {

    public DefaultSchemaConfigManager() {
        super();
        addSchema("tt_server", "tt_server.schema.properties");
    }
}
