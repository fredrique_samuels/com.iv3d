package com.app.server.base.server.web;

import com.app.server.base.server.AppResourceWriter;
import com.app.server.base.server.AppContext;
import com.app.server.base.macros.HtmlMacroService;
import com.app.server.base.server.WebComponentIdGenerator;
import com.google.inject.Inject;
import com.iv3d.common.core.Transformer;
import com.iv3d.common.server.AbstractRequestHandler;
import com.iv3d.common.utils.ExceptionUtils;
import com.iv3d.common.utils.JsonUtils;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;


public abstract class RequestHandler extends AbstractRequestHandler {
    private static Logger logger = Logger.getLogger(RequestHandler.class);
    private HtmlMacroService htmlMacroService;
    private WebComponentIdGenerator webComponentIdGenerator;
    private Request request;
    private HttpServletRequest httpServletRequest;
    private HttpServletResponse httpServletResponse;
    private AppContext appContext;

    private static final Transformer<String, Long> longTransformer = from -> Long.valueOf(from);
    private static final Transformer<String, Double> doubleTransformer = from -> Double.valueOf(from);
    private static final Transformer<String, Integer> intTransformer = from -> Integer.valueOf(from);

    protected boolean preProcess() {
        return false;
    }

    @Override
    public final void handle(Request requestBase, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        this.request = requestBase;
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;
        try {
            if (!this.preProcess()) {
                this.handle(httpServletRequest, httpServletResponse);
            }
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            returnError(httpServletResponse, null);
        }
    }

    protected final Request getRequest() {
        return request;
    }

    protected final HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

    protected final HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    protected abstract void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception;

    @Inject
    public void inject(HtmlMacroService htmlMacroService) {
        this.htmlMacroService = htmlMacroService;
    }
    @Inject
    public void inject(WebComponentIdGenerator webComponentIdGenerator) {
        this.webComponentIdGenerator = webComponentIdGenerator;
    }

    protected String getNewWebId() {return webComponentIdGenerator.createId();}

    protected final void returnNoContent(){returnNoContent(httpServletResponse);}
    protected final void returnHtml(String html){returnHtml(httpServletResponse, html);}
    protected final void returnJson(String json){returnJson(httpServletResponse, json);}

    protected final String getParameter(String name){return httpServletRequest.getParameter(name);}
    protected final <T> T getParameter(String name, T defaultValue, Transformer<String, T> transformer){
        String parameter = httpServletRequest.getParameter(name);
        return parameter==null?defaultValue: getTransformedValue(transformer, parameter, defaultValue);}
    protected final <T> T getParameterRequired(String name, T defaultValue, Transformer<String, T> transformer){
        String parameter = getRequiredParameter(name);
        return parameter==null?defaultValue: getTransformedValue(transformer, parameter, defaultValue);}

    private <T> T getTransformedValue(Transformer<String, T> transformer, String parameter, T defaultValue) {
        try {
            return transformer.transform(parameter);
        } catch (Exception e) {
            logger.info(ExceptionUtils.getStackTrace(e));
            return defaultValue;
        }
    }

    protected final String getRequiredParameter(String name){return getParameter(httpServletRequest, name);}

    protected final String getParameterString(String name, String defaultValue){
        String parameter = httpServletRequest.getParameter(name);
        return parameter==null?defaultValue:parameter;}
    protected final Long getParameterLong(String name, Long defaultValue) {
        return getParameter(name, defaultValue, longTransformer);
    }
    protected final Integer getParameterInteger(String name, Integer defaultValue) {
        return getParameter(name, defaultValue, intTransformer);
    }
    protected final Double getParameterDouble(String name, Double defaultValue) {
        return getParameter(name, defaultValue, doubleTransformer);
    }
    protected final Long getParameterLong(String name) {
        return getParameter(name, null, longTransformer);
    }
    protected final Long getParameterLongRequired(String name) {
        return getParameterRequired(name, null, longTransformer);
    }
    protected final Integer getParameterInteger(String name) {
        return getParameter(name, null, intTransformer);
    }
    protected final Integer getParameterIntegerRequired(String name) {
        return getParameterRequired(name, null, intTransformer);
    }
    protected final Double getParameterDouble(String name) {
        return getParameter(name, null, doubleTransformer);
    }

    protected final void returnMav(ModelAndView mav) {
        String viewName = mav.getViewName();
        if(viewName==null) returnNoContent(httpServletResponse);
        if(viewName.isEmpty()) returnNoContent(httpServletResponse);

        Map<String, Object> model = mav.getModel();
        String html = htmlMacroService.parseTemplate(viewName, model);
        returnHtml(html);
    }

    protected final void returnMavJson(ModelAndView mav) {
        String viewName = mav.getViewName();
        if(viewName==null) returnNoContent(httpServletResponse);
        if(viewName.isEmpty()) returnNoContent(httpServletResponse);

        Map<String, Object> model = mav.getModel();
        String html = htmlMacroService.parseTemplate(viewName, model);
        mav.setHtml(html);
        returnJson(JsonUtils.writeToString(mav));
    }


    protected final String getRawBody() {
        try {
            // Read from request
            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            return buffer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public final void setAppContext(AppContext appContext) {
        this.appContext = appContext;
    }
    protected final AppContext getAppContext(){return appContext;}
    protected final AppResourceWriter getResourceWriter() {return new AppResourceWriter(appContext);}
}
