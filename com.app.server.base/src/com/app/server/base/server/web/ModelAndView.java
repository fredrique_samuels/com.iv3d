package com.app.server.base.server.web;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class ModelAndView {
    final Map<String, Object> model;
    String viewName;
    private String html;

    public ModelAndView(String viewMacro) {
        this.model = new HashMap<>();
        this.viewName = viewMacro;
    }

    public final ModelAndView addObject(String key, Object value) {
        model.put(key, value);
        return this;
    }

    public final String getViewName(){return viewName;}
    public final Map<String, Object> getModel(){return Collections.unmodifiableMap(model);}

    public void setHtml(String html) {
        this.html = html;
    }

    public String getHtml() {
        return html;
    }
}
