package com.app.server.base.server;

/**
 * Created by fred on 2017/05/16.
 */
public interface OperationTaskProvider {
    OperationTask get(String appPackageId, String beanId);
}
