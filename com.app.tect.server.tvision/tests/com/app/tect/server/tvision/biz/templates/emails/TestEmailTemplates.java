package com.app.tect.server.tvision.biz.templates.emails;

import com.app.server.base.macros.HtmlMacroServiceImpl;
import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.api.email.EmailTemplate;
import com.app.tect.server.tvision.bizimpl.email.EmailTemplateManager;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.email.MailParam;
import com.iv3d.common.email.MailSession;
import com.iv3d.common.i8n.I8nMessageResolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by fred on 2017/08/03.
 */
public class TestEmailTemplates {

    private static final String EXPECTED_FOLDER = "/com/app/tect/server/tvision/biz/templates/emails/expected/";

    private EmailTemplateManager emailTemplateManager;

    @Before
    public void setup() {
        HtmlMacroServiceImpl htmlMacroService = new HtmlMacroServiceImpl();
        emailTemplateManager = new EmailTemplateManager(htmlMacroService, new I8nMessageResolver());
    }

    @Test
    public void testAccountCreatedTemplate() {
        UserParam userParam = new UserParam();
        userParam.setFirstName("user101");
        userParam.setEmail("user101@gmail");
        AccountParam accountParam = new AccountParam();
        accountParam.setValidationToken("1234-56789");
        MailParam mail = emailTemplateManager.getAccountCreatedMail(userParam, accountParam);

        EmailTemplate accountCreated = EmailTemplate.ACCOUNT_CREATED;
        Assert.assertEquals("user101@gmail", mail.getRecipient());
        Assert.assertEquals(MailSession.MIME_TYPE_HTML, mail.getMimeType());
        assertTemplateEmail(accountCreated, mail.getContent());
    }

    void assertTemplateEmail(EmailTemplate accountCreated, String actualHtml) {
        String path = EXPECTED_FOLDER + accountCreated.getId() + ".html";
        String expectedHtml = ResourceLoader.readAsString(path);
        Assert.assertTrue("It looks like "+path+" does not exists", path!=null);
        Assert.assertNotNull(expectedHtml);
        Assert.assertEquals(expectedHtml, actualHtml);
    }
}
