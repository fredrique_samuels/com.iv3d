package com.app.tect.server.tvision.biz.accounts;

import com.app.tect.server.tvision.api.email.EmailManager;
import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.EmailAlreadyInUseError;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.api.validation.ValidationResult;
import com.app.tect.server.tvision.bizimpl.accounts.UserAccountManagerImpl;
import com.app.tect.server.tvision.bizimpl.accounts.UserAccountsDaoImpl;
import com.app.tect.server.tvision.bizimpl.session.SessionThreadContext;
import com.app.tect.server.tvision.security.SecurityToken;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.i8n.I8nMessageResolver;
import com.iv3d.common.storage.migrate.SchemaMigrationConfig;
import com.iv3d.common.storage.migrate.SchemaMigrationRunner;
import mockit.Mocked;
import mockit.Verifications;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * Created by fred on 2017/07/29.
 */
public class TestUserAccountManager {

    private static final String FIRST_NAME = "Jon";
    private static final String LAST_NAME = "snow";
    private static final String EMAIL = "jon.snow@host.com";

    private static final String PASSWORD = "p@ssword300";
    private static final String VALIDATION_TOKEN = "123456789";
    private static final String INVALID_VALIDATION_TOKEN = "---------";

    private UserAccountManagerImpl userAccountManager;

    @Mocked
    private EmailManager emailManager;

    @Before
    public void setup() {
        SchemaMigrationConfig amDbCredentials = SchemaMigrationConfig.loadSchemaConfig("/tect_am.schema.test.properties");
        SchemaMigrationRunner.updateAll(Arrays.asList(amDbCredentials));

        I8nMessageResolver i8nMessageResolver = new I8nMessageResolver();
        i8nMessageResolver.setLanguageProvider(() -> SessionThreadContext.getSessionContext().getLanguage());

        userAccountManager = new UserAccountManagerImpl();
        userAccountManager.setI8nMessageResolver(i8nMessageResolver);
        userAccountManager.setUserAccountsDao(new UserAccountsDaoImpl(amDbCredentials));
        userAccountManager.setTokenGenerator(
                    () -> {
                    SecurityToken securityToken = new SecurityToken();
                    securityToken.setSecret(ResourceLoader.readAsString("/test-secret"));
                    securityToken.setParams(ResourceLoader.readAsString("/test-secret-params"));
                    return securityToken;
                }
            );
        userAccountManager.setEmailManager(emailManager);
        userAccountManager.setValidationTokenGenerator(() -> VALIDATION_TOKEN);
    }

    @After
    public void tearDown() {
        File file = new File("test_tect_am.db");
        if(file.exists()) {
            file.delete();
        }
    }

    @Test
    public void testCreateNewUser() throws Exception {
        UserParam user = createUser();

        Assert.assertEquals(1, user.getId());
        Assert.assertEquals(EMAIL, user.getEmail());
        Assert.assertEquals("cEBzc3dvcmQzMDA=", user.getCredentials().trim());
        Assert.assertEquals(EMAIL, user.getEmail());
    }



    @Test
    public void testError_WhenUserNameAlreadyInUse() {
        createUser();

        try {
            createUser();
            Assert.fail("Expected " + EmailAlreadyInUseError.class.getCanonicalName());
        } catch (EmailAlreadyInUseError e) {
            Assert.assertEquals(EMAIL, e.getMessage());
        }

    }

    @Test
    public void testGetUserByName() {
        createUser();
        createUser2();

        UserParam user = userAccountManager.getUser(EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(FIRST_NAME, user.getFirstName());
        Assert.assertEquals(LAST_NAME, user.getLastName());
        Assert.assertEquals(EMAIL, user.getEmail());
    }

    @Test
    public void testGetUserByName_ReturnsNull() {
        UserParam user = userAccountManager.getUser(EMAIL);
        Assert.assertNull(user);
    }

    @Test
    public void testExistingCredentialValidate() {
        createUser();

        assertFalse(userAccountManager.validateExistingUserCredentials("user1", "password").isSuccess());
        assertFalse(userAccountManager.validateExistingUserCredentials("wrong_name", "p@ssword300").isSuccess());
        Assert.assertTrue(userAccountManager.validateExistingUserCredentials(EMAIL, PASSWORD).isSuccess());
    }

    @Test
    public void testNewCredentialsValidate() {
        createUser();

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("", "user30", "password", "password");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            assertEquals("Please provide a name.", messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", "", "password", "password");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            assertEquals("Please provide an email address.", messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", null, "password", "password");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            assertEquals("Please provide an email address.", messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", "user30", null, null);
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            String expected = "Password must be minimum 6 characters and can include numbers and special characters _,@,.,,$,& .";
            assertEquals(expected, messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", "user30", "", "");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            String expected = "Password must be minimum 6 characters and can include numbers and special characters _,@,.,,$,& .";
            assertEquals(expected, messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", EMAIL, "password", "password");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            assertEquals("The email you provided is already in use.", messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", "user30", "password", "");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            assertEquals("Your confirmation password and password does not match.", messages.get(0).getMessage());
        }

        {
            ValidationResult validationResult = userAccountManager.validateNewUserCredentials("jason", "user30", "passd", "");
            List<ValidationResult.NamedMessage> messages = validationResult.getNamedMessages();
            assertFalse(validationResult.isSuccess());
            assertEquals(1, messages.size());
            assertEquals("Password must be minimum 6 characters and can include numbers and special characters _,@,.,,$,& .", messages.get(0).getMessage());
        }

    }

    @Test
    public void testCreateNewAccount() {
        UserParam user = createUser();
        AccountParam account = userAccountManager.createNewAccount(user);

        Assert.assertNotNull(account);
        Assert.assertEquals(1, account.getId());
        assertFalse(account.isValidated());
        Assert.assertEquals(VALIDATION_TOKEN, account.getValidationToken());
    }

    @Test
    public void testNewAccountLinkedToUser() {
        UserParam user = createUser();
        AccountParam account = userAccountManager.createNewAccount(user);

        List<UserParam> accountUsers = userAccountManager.getAccountUsers(account);
        Assert.assertNotNull(accountUsers);
        Assert.assertEquals(1, accountUsers.size());
        Assert.assertEquals(user.getId(), accountUsers.get(0).getId());

        List<AccountParam> userAccounts = userAccountManager.getUsersAccounts(user);
        Assert.assertNotNull(userAccounts);
        Assert.assertEquals(1, userAccounts.size());
        Assert.assertEquals(account.getId(), userAccounts.get(0).getId());
    }

    @Test
    public void testValidateAccount() {

        UserParam user = createUser();
        AccountParam account = userAccountManager.createNewAccount(user);


        {
            AccountParam validatedAccount = userAccountManager.validateUserAccount(user.getId(), -1, INVALID_VALIDATION_TOKEN);
            Assert.assertNull(validatedAccount);
        }

        {
            AccountParam validatedAccount = userAccountManager.validateUserAccount(0, account.getId(), INVALID_VALIDATION_TOKEN);
            Assert.assertNull(validatedAccount);
        }

        {
            AccountParam validatedAccount = userAccountManager.validateUserAccount(user.getId(), account.getId(), INVALID_VALIDATION_TOKEN);
            Assert.assertNull(validatedAccount);
        }

        {
            AccountParam validatedAccount = userAccountManager.validateUserAccount(user.getId(), account.getId(), VALIDATION_TOKEN);
            Assert.assertTrue(validatedAccount.isValidated());
        }
    }

    @Test
    public void testEmailSentToNewlyCreatedAccountOwner() {
        UserParam user = createUser();
        AccountParam account = userAccountManager.createNewAccount(user);

        new Verifications() {
            {
                emailManager.sendAccountCreatedEmail(user, account);
                times=1;
            }
        };
    }

    UserParam createUser() {
        return userAccountManager.createNewUser(FIRST_NAME, LAST_NAME, EMAIL, PASSWORD);
    }
    UserParam createUser2() {
        return userAccountManager.createNewUser("a", "b", "jon.snow2@host.com", PASSWORD);
    }
}
