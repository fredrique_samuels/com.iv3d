package etc.config.sql.schema.tect_am;

import com.iv3d.common.storage.migrate.SchemaMigration;
import etc.config.sql.schema.tect_am.tect_account.TectAccountSchema_CreateTable;
import etc.config.sql.schema.tect_am.tect_user.TectUserSchema_CreateTable;
import etc.config.sql.schema.tect_am.tect_useraccounts.TectUserAccountSchema_CreateTable;

public class Migration_1 extends SchemaMigration {
    public Migration_1() {
        super(
                new TectUserSchema_CreateTable(),
                new TectAccountSchema_CreateTable(),
                new TectUserAccountSchema_CreateTable()
        );
    }
} 