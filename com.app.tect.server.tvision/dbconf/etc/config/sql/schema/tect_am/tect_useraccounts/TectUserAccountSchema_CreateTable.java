package etc.config.sql.schema.tect_am.tect_useraccounts;

import com.eroi.migrate.Migration;
import com.eroi.migrate.schema.Table;

import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class TectUserAccountSchema_CreateTable implements Migration {

    @Override
    public void up() {
        Table table = table(TectUserAccountSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("user_id", INTEGER),
                column("account_id", INTEGER),

                column("doe", TIMESTAMP, notnull()),
                column("dlu", TIMESTAMP, notnull()),
                column("ulu", TIMESTAMP),
                column("archived", BOOLEAN, notnull())
                );
        createTable(table);
    }

    @Override
    public void down() {
        dropTable(TectUserAccountSchema.TABLE_NAME);
    }
}
