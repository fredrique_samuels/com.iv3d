package etc.config.sql.schema.tect_am.tect_user;

import com.eroi.migrate.Migration;
import com.eroi.migrate.schema.Table;

import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class TectUserSchema_CreateTable implements Migration {

    @Override
    public void up() {
        Table table = table(TectUserSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("firstName", VARCHAR, length(128), notnull()),
                column("lastName", VARCHAR, length(128), notnull()),
                column("credentials", VARCHAR, length(1024)),
                column("email", VARCHAR, length(1024)),
                column("token", VARCHAR, length(25)),

                column("doe", TIMESTAMP, notnull()),
                column("dlu", TIMESTAMP, notnull()),
                column("archived", BOOLEAN, notnull())
                );
        createTable(table);
    }

    @Override
    public void down() {
        dropTable(TectUserSchema.TABLE_NAME);
    }
}
