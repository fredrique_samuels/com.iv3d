package etc.config.sql.schema.tect_am.tect_account;

import com.eroi.migrate.Migration;
import com.eroi.migrate.schema.Table;

import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

/**
 * Created by fred on 2017/05/18.
 */
public class TectAccountSchema_CreateTable implements Migration {

    @Override
    public void up() {
        Table table = table(TectAccountSchema.TABLE_NAME,
                column("id", INTEGER, primarykey()),
                column("name", VARCHAR, length(1024)),
                column("validated", BOOLEAN),
                column("validation_token", VARCHAR, length(15)),
                column("owner_user_id", INTEGER),

                column("doe", TIMESTAMP, notnull()),
                column("dlu", TIMESTAMP, notnull()),
                column("ulu", INTEGER),
                column("archived", BOOLEAN, notnull())
                );
        createTable(table);
    }

    @Override
    public void down() {
        dropTable(TectAccountSchema.TABLE_NAME);
    }
}
