package com.app.tect.server.tvision.api.email;

import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserParam;

/**
 * Created by fred on 2017/08/02.
 */
public interface EmailManager {
    void sendAccountCreatedEmail(UserParam userParam, AccountParam accountParam);
}
