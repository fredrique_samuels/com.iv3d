package com.app.tect.server.tvision.api.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2017/08/04.
 */
public final class ValidationResult {
    private final List<String> messages;
    private final boolean success;
    private final List<NamedMessage> namedMessages;

    private ValidationResult(Builder builder) {
        this.messages = Collections.unmodifiableList(builder.messages);
        this.namedMessages = builder.namedMessages;
        this.success = builder.status==Status.SUCCESS;
    }

    public final List<String> getMessages() {
        return this.messages;
    }

    public final String getMessage() {
        List<String> messages = getMessages();
        if(messages.size()>0) {
            return messages.get(0);
        }
        return null;
    }

    public final List<NamedMessage> getNamedMessages() {
        return this.namedMessages;
    }

    public final boolean isSuccess() {
        return this.success;
    }
    
    private enum Status {
        SUCCESS,
        FAILED
    }

    public static final class Builder {
        private List<String> messages= new ArrayList<>();
        private List<NamedMessage> namedMessages = new ArrayList<>();
        private Status status = Status.FAILED;

        public final Builder setStatusSuccess() {
            this.status = Status.SUCCESS;
            return this;
        }

        public final Builder addMessage(String message) {
            if(message!=null && !message.isEmpty() ) {
                this.messages.add(message);
            }
            return this;
        }

        public final Builder addNamedMessage(String name, String message) {
            this.namedMessages.add(new NamedMessage(name, message));
            return this;
        }

        public final ValidationResult build() {
            return new ValidationResult(this);
        }
    }

    public static final class NamedMessage {
        private String name;
        private String message;

        public NamedMessage(String name, String message) {
            this.name = name;
            this.message = message;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
