package com.app.tect.server.tvision.api.email;

/**
 * Created by fred on 2017/08/02.
 */
public enum EmailTemplate {
    ACCOUNT_CREATED("template.email.account.created");

    private String id;

    EmailTemplate(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


}
