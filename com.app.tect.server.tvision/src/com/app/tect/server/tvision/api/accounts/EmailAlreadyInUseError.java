package com.app.tect.server.tvision.api.accounts;

public final class EmailAlreadyInUseError extends RuntimeException {
    public EmailAlreadyInUseError(String userName) {
        super(userName);
    }
}