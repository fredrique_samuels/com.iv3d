package com.app.tect.server.tvision.api.accounts;

import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2017/07/29.
 */
public class AccountParam extends AuditedParam {
    private long ownerUserId;
    private boolean validated;
    private String validationToken;

    public AccountParam setOwnerUserId(long ownerUserId) {
        this.ownerUserId = ownerUserId;
        return this;
    }

    public long getOwnerUserId() {
        return ownerUserId;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public String getValidationToken() {
        return validationToken;
    }

    public void setValidationToken(String validationToken) {
        this.validationToken = validationToken;
    }
}
