package com.app.tect.server.tvision.api.accounts;

import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2017/07/29.
 */
public class UserAccountParam extends AuditedParam {

    private long userId;
    private long accountId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
}
