package com.app.tect.server.tvision.api.accounts;

import com.app.tect.server.tvision.api.validation.ValidationResult;

import java.util.List;

/**
 * Created by fred on 2017/07/29.
 */
public interface UserAccountManager {
    UserParam createNewUser(String firstName, String lastName, String email, String password);
    UserParam getUser(String email);
    ValidationResult validateExistingUserCredentials(String email, String password);
    ValidationResult validateNewUserCredentials(String firstName, String email, String password, String verifyPassword);

    AccountParam createNewAccount(UserParam user);
    List<UserParam> getAccountUsers(AccountParam account);

    List<AccountParam> getUsersAccounts(UserParam user);

    AccountParam validateUserAccount(long userId, long accountId, String validationToken);
}
