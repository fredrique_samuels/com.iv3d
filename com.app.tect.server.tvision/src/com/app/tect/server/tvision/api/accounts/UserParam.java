package com.app.tect.server.tvision.api.accounts;

import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2017/07/29.
 */
public class UserParam extends AuditedParam {

    private String firstName;
    private String lastName;
    private String credentials;
    private String token;
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public UserParam setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserParam setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getCredentials() {
        return credentials;
    }

    public UserParam setCredentials(String credentials) {
        this.credentials = credentials;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserParam setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getToken() {
        return token;
    }

    public UserParam setToken(String token) {
        this.token = token;
        return this;
    }
}
