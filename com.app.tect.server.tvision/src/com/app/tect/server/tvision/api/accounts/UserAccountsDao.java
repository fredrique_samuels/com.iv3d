package com.app.tect.server.tvision.api.accounts;

import java.util.List;

/**
 * Created by fred on 2017/07/29.
 */
public interface UserAccountsDao {
    UserParam saveUser(UserParam params);
    UserParam getUser(String email);

    AccountParam saveAccount(AccountParam accountParam);
    AccountParam getAccountToValidate(long userId, long accountId, String validationToken);

    UserAccountParam saveUserAccount(UserAccountParam userAccountParam);
    List<UserParam> getAccountUsers(long id);
    List<AccountParam> getUserAccounts(long userId);
}
