package com.app.tect.server.tvision.domain;

import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.bizimpl.session.SessionThreadContext;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;

import static etc.config.sql.schema.tect_am.tect_account.TectAccountSchema.TABLE_NAME;

/**
 * Created by fred on 2017/07/29.
 */
@Entity
@Table(name = TABLE_NAME)
public class AccountHibernate extends HibernateData {

    @Id
    @GeneratedValue
    @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "dlu") private String dlu;
    @Column(name = "ulu") private Long ulu;
    @Column(name = "archived") private boolean archived;

    @Column(name = "validated") private boolean validated;
    @Column(name = "validation_token") private String validationToken;
    @Column(name = "owner_user_id") private long ownerUserId;

    @Override
    public Long getId() {
        return id==null ? -1 : id;
    }

    public AccountParam getParams() {
        AccountParam params = new AccountParam();

        params.setId(id);
        params.setDoe(DaoUtils.readDate(doe));
        params.setDlu(DaoUtils.readDate(dlu));
        params.setUlu(ulu);
        params.setArchived(this.archived);
        params.setValidated(this.validated);
        params.setValidationToken(this.validationToken);

        return params;
    }

    public void update(AccountParam params) {
        this.id = params.getId();
        this.archived = params.isArchived();
        this.doe = DaoUtils.persist(params.getDoe());
        this.dlu = DaoUtils.persist(new UtcSystemClock().now());
        this.ulu = SessionThreadContext.getSessionContext().getUserId();

        this.ownerUserId = params.getOwnerUserId();
        this.validated = params.isValidated();
        this.validationToken = params.getValidationToken();
    }
}
