package com.app.tect.server.tvision.domain;

import com.app.tect.server.tvision.api.accounts.UserParam;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;

import static etc.config.sql.schema.tect_am.tect_user.TectUserSchema.TABLE_NAME;

/**
 * Created by fred on 2017/07/29.
 */
@Entity
@Table(name = TABLE_NAME)
public class UserHibernate extends HibernateData {

    @Id
    @GeneratedValue
    @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "dlu") private String dlu;
    @Column(name = "archived") private boolean archived;

    @Column(name = "firstName") private String firstName;
    @Column(name = "lastName") private String lastName;
    @Column(name = "email") private String email;
    @Column(name = "credentials") private String credentials;
    @Column(name = "token") private String token;

    @Override
    public Long getId() {
        return id==null ? -1 : id;
    }

    public UserParam getParams() {
        UserParam params = new UserParam();

        params.setId(id);
        params.setDoe(DaoUtils.readDate(doe));
        params.setDlu(DaoUtils.readDate(dlu));
        params.setArchived(this.archived);

        params.setFirstName(firstName);
        params.setLastName(lastName);
        params.setEmail(email);
        params.setCredentials(credentials);
        params.setToken(token);
        return params;
    }

    public void update(UserParam params) {
        this.id = params.getId();
        this.archived = params.isArchived();
        this.doe = DaoUtils.persist(params.getDoe());
        this.dlu = DaoUtils.persist(new UtcSystemClock().now());

        this.firstName = params.getFirstName();
        this.lastName = params.getLastName();
        this.credentials = params.getCredentials();
        this.email = params.getEmail();
        this.token = params.getToken();
    }
}
