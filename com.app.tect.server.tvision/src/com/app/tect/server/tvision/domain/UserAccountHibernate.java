package com.app.tect.server.tvision.domain;

import com.app.tect.server.tvision.api.accounts.UserAccountParam;
import com.app.tect.server.tvision.bizimpl.session.SessionThreadContext;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.storage.DaoUtils;
import com.iv3d.common.storage.hibernate.HibernateData;

import javax.persistence.*;

import static etc.config.sql.schema.tect_am.tect_useraccounts.TectUserAccountSchema.TABLE_NAME;

/**
 * Created by fred on 2017/07/29.
 */
@Entity
@Table(name = TABLE_NAME)
public class UserAccountHibernate extends HibernateData {

    @Id
    @GeneratedValue
    @Column(name = "id") private Long id;
    @Column(name = "doe") private String doe;
    @Column(name = "dlu") private String dlu;
    @Column(name = "ulu") private Long ulu;
    @Column(name = "archived") private boolean archived;

    @Column(name = "user_id") private long userId;
    @Column(name = "account_id") private long accountId;

    @Override
    public Long getId() {
        return id==null ? -1 : id;
    }

    public UserAccountParam getParams() {
        UserAccountParam params = new UserAccountParam();

        params.setId(id);
        params.setDoe(DaoUtils.readDate(doe));
        params.setDlu(DaoUtils.readDate(dlu));
        params.setUlu(ulu);
        params.setArchived(this.archived);

        params.setUserId(userId);
        params.setAccountId(accountId);

        return params;
    }

    public void update(UserAccountParam params) {
        this.id = params.getId();
        this.archived = params.isArchived();
        this.doe = DaoUtils.persist(params.getDoe());
        this.dlu = DaoUtils.persist(new UtcSystemClock().now());
        this.ulu = SessionThreadContext.getSessionContext().getUserId();

        this.userId = params.getUserId();
        this.accountId = params.getAccountId();
    }
}
