package com.app.tect.server.tvision.security;

import com.iv3d.common.utils.JsonUtils;

/**
 * Created by fred on 2017/07/31.
 */
public final class SecurityToken {
    private String secret;
    private String params;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String toJson(){
        return JsonUtils.writeToString(this);
    }

    public static SecurityToken fromJson(String json) {
        return JsonUtils.readFromString(json, SecurityToken.class);
    }
}
