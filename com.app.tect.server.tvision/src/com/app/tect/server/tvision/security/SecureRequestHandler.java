package com.app.tect.server.tvision.security;

import com.app.server.base.server.HttpSessionService;
import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by fred on 2017/09/22.
 */
public abstract class SecureRequestHandler extends RequestHandler {

    @Inject
    private HttpSessionService httpSessionService;
    private HttpSession session;

    @Override
    protected final void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession activeSession = httpSessionService.getActiveSession(request);
        if(activeSession==null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        this.session = activeSession;
        this.handle();
    }

    protected final HttpSession getSession() {
        return session;
    }

    abstract protected void handle();
}
