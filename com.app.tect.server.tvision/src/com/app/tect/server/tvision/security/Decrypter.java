package com.app.tect.server.tvision.security;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcSystemTimer;
import org.springframework.util.SerializationUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class Decrypter {
    Cipher dcipher;

    static byte[] salt = new String("12345678").getBytes();
    int iterationCount = 1024;
    int keyStrength = 256;
    SecretKey key;
    byte[] iv;

    public Decrypter(String passPhrase) throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount, keyStrength);
        SecretKey tmp = factory.generateSecret(spec);
        byte[] encoded = tmp.getEncoded();
        key = new SecretKeySpec(encoded, "AES");
        dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    }

    public String encrypt(String data) throws Exception {
        dcipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters params = dcipher.getParameters();
        iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        byte[] utf8EncryptedData = dcipher.doFinal(data.getBytes());
        String base64EncryptedData = new sun.misc.BASE64Encoder().encodeBuffer(utf8EncryptedData);

//        System.out.println("IV " + new sun.misc.BASE64Encoder().encodeBuffer(iv));
//        System.out.println("Encrypted Data " + base64EncryptedData);
        return base64EncryptedData;
    }

    public String decrypt(String base64EncryptedData) throws Exception {
        dcipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        byte[] decryptedData = new sun.misc.BASE64Decoder().decodeBuffer(base64EncryptedData);
        byte[] utf8 = dcipher.doFinal(decryptedData);
        return new String(utf8, "UTF8");
    }

    public static String generateSecret()  {
        SecretKeyFactory factory = null;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            KeySpec spec = new PBEKeySpec(UtcDate.now().toIso8601String().toCharArray(), salt, 1, 256);
            SecretKey tmp = factory.generateSecret(spec);
            return new String(tmp.getEncoded(), "ISO-8859-1");
        } catch (NoSuchAlgorithmException e) {
            throw new SecretKeyGenError(e);
        } catch (InvalidKeySpecException e) {
            throw new SecretKeyGenError(e);
        } catch (UnsupportedEncodingException e) {
            throw new SecretKeyGenError(e);
        }
    }



    private static class SecretKeyGenError extends RuntimeException {
        public SecretKeyGenError(Exception e) {
            super(e);
        }
    }

    public static void main(String args[]) throws Exception {

        String passPhrase = "jsapt";

        byte[] encoded = createSecretKeyEncoded(passPhrase);
        SecretKey key = new SecretKeySpec(encoded, "AES");
        Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        byte[] iv;
        String input = "Fredrique";
        String encrypted;
        String decrypted;

        {
            dcipher.init(Cipher.ENCRYPT_MODE, key);
            AlgorithmParameters params = dcipher.getParameters();
            iv = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] utf8EncryptedData = dcipher.doFinal(input.getBytes());
            encrypted = new sun.misc.BASE64Encoder().encodeBuffer(utf8EncryptedData);
        }

        System.out.println(new String(iv));
        {
            dcipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            byte[] decryptedData = new sun.misc.BASE64Decoder().decodeBuffer(encrypted);
            byte[] utf8 = dcipher.doFinal(decryptedData);
            decrypted = new String(utf8, "UTF8");
        }

        System.out.println(input);
        System.out.println(encrypted);
        System.out.println(decrypted);


//        String x = Decrypter.generateSecret();
//        System.out.println(x);
//        String data = "the quick brown fox jumps over the lazy dog";
//        System.out.println(test(data, x));
//        System.out.println(test(data, "ABCDEFGHIJKL"));
//        System.out.println(test(data, "1234567890"));
    }

    static byte[] createSecretKeyEncoded(String passPhrase) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] salt = new String("SALT").getBytes();
        int iterationCount = 1024;
        int keyStrength = 256;
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount, keyStrength);
        SecretKey tmp = factory.generateSecret(spec);
        return tmp.getEncoded();
    }

    static String test(String data, String passPhrase) throws Exception {
        Decrypter decrypter = new Decrypter(passPhrase);
        String encrypted = decrypter.encrypt(data);
        System.out.println(encrypted);
        return decrypter.decrypt(encrypted);
    }

    public static class CryptParams {

    }
}