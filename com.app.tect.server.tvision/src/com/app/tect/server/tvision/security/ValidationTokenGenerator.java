package com.app.tect.server.tvision.security;

/**
 * Created by fred on 2017/08/01.
 */
public interface ValidationTokenGenerator {
    String generateToken();
}
