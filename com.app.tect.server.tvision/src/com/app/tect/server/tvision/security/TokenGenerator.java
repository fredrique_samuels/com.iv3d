package com.app.tect.server.tvision.security;

public interface TokenGenerator {
    SecurityToken generate();
}
