package com.app.tect.server.tvision;

import com.app.server.base.server.AppServerLauncher;
import com.app.server.base.server.schema.DefaultSchemaConfigManager;
import com.app.tect.server.tvision.api.accounts.UserAccountManager;
import com.app.tect.server.tvision.api.accounts.UserAccountsDao;
import com.app.tect.server.tvision.api.email.EmailManager;
import com.app.tect.server.tvision.bizimpl.accounts.UserAccountManagerImpl;
import com.app.tect.server.tvision.bizimpl.accounts.UserAccountsDaoImpl;
import com.app.tect.server.tvision.bizimpl.email.EmailManagerImpl;
import com.app.tect.server.tvision.bizimpl.security.TokenGeneratorImpl;
import com.app.tect.server.tvision.bizimpl.security.ValidationTokenGeneratorImpl;
import com.app.tect.server.tvision.security.TokenGenerator;
import com.app.tect.server.tvision.security.ValidationTokenGenerator;
import com.google.inject.AbstractModule;
import com.iv3d.common.core.Credentials;
import com.iv3d.common.email.GmailSettings;
import com.iv3d.common.email.MailSession;
import com.iv3d.common.i8n.Language;
import com.iv3d.common.i8n.LanguageProvider;
import com.iv3d.common.storage.migrate.SchemaConfigManager;

/**
 * Created by fred on 2017/04/30.
 */
public class TVision {

    private static String DB_TECT_AM = "tect_am";

    public static void main(String[] args) throws Exception {
        SchemaConfigManager schemaConfigManager = new SchemaUpdater();
        new AppServerLauncher().run(schemaConfigManager, new ServerInjectorModule(schemaConfigManager));
    }


    public static class SchemaUpdater extends DefaultSchemaConfigManager {
        public SchemaUpdater() {
            super();
            addSchema(DB_TECT_AM, "tect_am.schema.properties");
        }
    }


    public static class ServerInjectorModule extends AbstractModule {

        private SchemaConfigManager schemaConfigManager;

        public ServerInjectorModule(SchemaConfigManager schemaConfigManager) {
            this.schemaConfigManager = schemaConfigManager;
        }

        @Override
        protected void configure() {
            Credentials c = new Credentials() {
                @Override
                public String getUser() {
                    return "";
                }

                @Override
                public String getPassword() {
                    return "";
                }
            };

            bind(UserAccountsDao.class).toInstance(new UserAccountsDaoImpl(schemaConfigManager.getConfig(DB_TECT_AM)));
            bind(MailSession.class).toInstance(new MailSession(c, new GmailSettings()));
            bind(EmailManager.class).to(EmailManagerImpl.class);
            bind(TokenGenerator.class).to(TokenGeneratorImpl.class);
            bind(ValidationTokenGenerator.class).to(ValidationTokenGeneratorImpl.class);
            bind(LanguageProvider.class).toInstance(() -> Language.ENGLISH);
            bind(UserAccountManager.class).to(UserAccountManagerImpl.class);
        }
    }
}
