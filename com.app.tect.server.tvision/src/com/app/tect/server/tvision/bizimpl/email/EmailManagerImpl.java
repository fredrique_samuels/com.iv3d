package com.app.tect.server.tvision.bizimpl.email;

import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.api.email.EmailManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.email.MailParam;
import com.iv3d.common.email.MailSession;
import com.iv3d.common.utils.ExceptionUtils;
import org.apache.log4j.Logger;

/**
 * Created by fred on 2017/08/02.
 */
@Singleton
public class EmailManagerImpl implements EmailManager {

    private static final Logger logger = Logger.getLogger(EmailManagerImpl.class);

    private EmailTemplateManager templateManager;
    private MailSession mailSession;

    @Override
    public void sendAccountCreatedEmail(UserParam userParam, AccountParam accountParam) {
        try {
            MailParam mail = templateManager.getAccountCreatedMail(userParam, accountParam);
            mailSession.sendHtmlMessage(mail);
        } catch (Exception e) {
            e.printStackTrace();
            RuntimeException exception = new RuntimeException("Email not sent to " + userParam.getEmail());
            logger.warn(ExceptionUtils.getStackTrace(exception));
        }
    }

    @Inject
    public void setTemplateManager(EmailTemplateManager templateManager) {
        this.templateManager = templateManager;
    }

    @Inject
    public void setMailSession(MailSession mailSession) {
        this.mailSession = mailSession;
    }

}
