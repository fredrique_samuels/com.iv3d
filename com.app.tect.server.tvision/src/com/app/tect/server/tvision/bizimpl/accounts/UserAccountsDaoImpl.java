package com.app.tect.server.tvision.bizimpl.accounts;

import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserAccountParam;
import com.app.tect.server.tvision.api.accounts.UserAccountsDao;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.bizimpl.session.SessionThreadContext;
import com.app.tect.server.tvision.domain.AccountHibernate;
import com.app.tect.server.tvision.domain.UserAccountHibernate;
import com.app.tect.server.tvision.security.ValidationTokenGenerator;
import com.app.tect.server.tvision.domain.UserHibernate;
import com.google.inject.Singleton;
import com.iv3d.common.storage.hibernate.AbstractHibernateDao;
import com.iv3d.common.storage.hibernate.DbCredentials;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2017/07/29.
 */
@Singleton
public class UserAccountsDaoImpl extends AbstractHibernateDao implements UserAccountsDao {

    public UserAccountsDaoImpl(DbCredentials dbCredentials) {
        super(dbCredentials,
                UserHibernate.class,
                AccountHibernate.class,
                UserAccountHibernate.class);
    }

    @Override
    public UserParam getUser(String email) {
        Object hibernate = getUniqueResult((session) -> {
            String hbl = "FROM  UserHibernate a WHERE a.email = :email AND a.archived=false";
            return session.createQuery(hbl).setString("email", email);
        });

        return hibernate!=null ? ((UserHibernate) hibernate).getParams() : null;
    }

    @Override
    public UserParam saveUser(UserParam user) {
        if(user.isPersisted()) {
            UserHibernate hibernate = get(user.getId(), UserHibernate.class);
            hibernate.update(user);
            saveObject(hibernate);
            return hibernate.getParams();
        }

        UserHibernate hibernate = new UserHibernate();
        hibernate.update(user);
        saveObject(hibernate);
        return hibernate.getParams();
    }

    @Override
    public List<UserParam> getAccountUsers(long accountId) {

        List<UserAccountHibernate> userAccounts = getObjectList((session)->
                session.createQuery("FROM UserAccountHibernate WHERE accountId = :id ")
                        .setLong("id", accountId)
        );

        if(userAccounts==null) {
            return Collections.EMPTY_LIST;
        }

        if(userAccounts.size()==0) {
            return Collections.EMPTY_LIST;
        }

        List<UserHibernate> usersHibernate = getObjectList((session) -> {
            List<Long> userIds = userAccounts.stream().map(i -> i.getParams().getUserId()).collect(Collectors.toList());
                    return session.createQuery("FROM UserHibernate WHERE id IN (:userIds) ")
                            .setParameterList("userIds", userIds);
                }
        );

        return usersHibernate.stream().map( u -> u.getParams()).collect(Collectors.toList());
    }

    @Override
    public List<AccountParam> getUserAccounts(long userId) {

        List<UserAccountHibernate> userAccounts = getObjectList((session)->
                session.createQuery("FROM UserAccountHibernate WHERE userId = :id ")
                        .setLong("id", userId)
        );

        if(userAccounts==null) {
            return Collections.EMPTY_LIST;
        }

        if(userAccounts.size()==0) {
            return Collections.EMPTY_LIST;
        }

        List<AccountHibernate> accountsHibernate = getObjectList((session) -> {
                    List<Long> accountIds = userAccounts.stream().map(i -> i.getParams().getAccountId()).collect(Collectors.toList());
                    return session.createQuery("FROM AccountHibernate WHERE id IN (:accountIds) ")
                            .setParameterList("accountIds", accountIds);
                }
        );

        return accountsHibernate.stream().map( u -> u.getParams()).collect(Collectors.toList());
    }

    @Override
    public AccountParam getAccountToValidate(long userId, long accountId, String validationToken) {
        Object hibernate = getUniqueResult((session) -> {
            String hbl = "FROM  AccountHibernate a WHERE a.id = :accountId AND a.ownerUserId = :userId AND validationToken= :validationToken";
            return session.createQuery(hbl)
                    .setLong("userId", userId)
                    .setLong("accountId", accountId)
                    .setString("validationToken", validationToken);
        });

        if(hibernate!=null) return ((AccountHibernate)hibernate).getParams();
        return null;
    }

    @Override
    public UserAccountParam saveUserAccount(UserAccountParam userAccountParam) {
        if(userAccountParam.isPersisted()) {
            UserAccountHibernate hibernate = get(userAccountParam.getId(), UserAccountHibernate.class);
            hibernate.update(userAccountParam);
            saveObject(hibernate);
            return hibernate.getParams();
        }

        UserAccountHibernate hibernate = new UserAccountHibernate();
        hibernate.update(userAccountParam);
        saveObject(hibernate);
        return hibernate.getParams();
    }

    @Override
    public AccountParam saveAccount(AccountParam account) {
        if(account.isPersisted()) {
            AccountHibernate hibernate = get(account.getId(), AccountHibernate.class);
            hibernate.update(account);
            saveObject(hibernate);
            return hibernate.getParams();
        }

        AccountHibernate hibernate = new AccountHibernate();
        hibernate.update(account);
        saveObject(hibernate);
        return hibernate.getParams();
    }


}
