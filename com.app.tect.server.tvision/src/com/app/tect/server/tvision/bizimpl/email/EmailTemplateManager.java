package com.app.tect.server.tvision.bizimpl.email;

import com.app.server.base.macros.HtmlMacroService;
import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.api.email.EmailTemplate;
import com.app.tect.server.tvision.bizimpl.session.SessionThreadContext;
import com.iv3d.common.core.ResourceLoader;
import com.iv3d.common.email.MailParam;
import com.iv3d.common.email.MailSession;
import com.iv3d.common.i8n.I8nMessageResolver;
import com.iv3d.common.i8n.Language;
import freemarker.cache.TemplateLoader;
import org.apache.commons.collections.map.HashedMap;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Map;

/**
 * Created by fred on 2017/08/03.
 */
public class EmailTemplateManager {

    private static final String TEMPLATE_PATH = "/etc/config/templates/emails/";
    private static final String TEMPLATE_EXT = ".macro";

    private HtmlMacroService macroService;
    private I8nMessageResolver i8nMessageResolver;

    @Inject
    public EmailTemplateManager(HtmlMacroService macroService, I8nMessageResolver i8nMessageResolver) {
        this.macroService = macroService;
        this.i8nMessageResolver = i8nMessageResolver;
    }

    public MailParam getAccountCreatedMail(UserParam user, AccountParam account) {


        Map<String, Object> model = new HashedMap();

        model.put("title", "Welcome to the Vision Program");
        model.put("username", user.getFirstName());
        model.put("supportUrl", "http://tectendencies.com/tvision/support");
        model.put("activationCodeLabel", "Activation Code");
        model.put("copyRight", "© Tec-Tendencies (Pty), 2017");
        model.put("address", "Cape Town <br/> South Africa");
        model.put("needHelp", "If you need help or have any questions, please visit");
        model.put("token", account.getValidationToken());
        model.put("thankYou", "Thank you for joining the Vision Program.");
        model.put("accountCreated", "Your account has been created and you can log in when ever you wish.");
        model.put("requiredValidation", "We require you to active your account to get access to all the features available.");
        model.put("activationInstruction", "ONCE LOGGED IN, COPY AND PASTE THE TOKEN ABOVE IN THE ACTIVATION PROMPT");

        String path = TEMPLATE_PATH + EmailTemplate.ACCOUNT_CREATED.getId() + TEMPLATE_EXT;
        String s = ResourceLoader.readAsString(path);

        SessionThreadContext sessionContext = SessionThreadContext.getSessionContext();
        Language language = sessionContext.getLanguage();

        String mailContent = macroService.parseTemplate(s, model, new StringTemplateLoader());
        String subject = i8nMessageResolver.getMessage(EmailTemplate.ACCOUNT_CREATED.getId() + ".subject", language);

        return new MailParam(user.getEmail(), subject, mailContent, MailSession.MIME_TYPE_HTML);
    }


    class StringTemplateLoader implements TemplateLoader {


        @Override
        public Object findTemplateSource(String s) throws IOException {
            return s;
        }

        @Override
        public long getLastModified(Object o) {
            return 0;
        }

        @Override
        public Reader getReader(Object o, String s) throws IOException {
            return new StringReader(o.toString());
        }

        @Override
        public void closeTemplateSource(Object o) throws IOException {

        }
    }
}
