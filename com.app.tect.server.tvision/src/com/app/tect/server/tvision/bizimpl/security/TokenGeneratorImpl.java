package com.app.tect.server.tvision.bizimpl.security;

import com.app.tect.server.tvision.security.Decrypter;
import com.app.tect.server.tvision.security.SecurityToken;
import com.app.tect.server.tvision.security.TokenGenerator;
import com.google.inject.Singleton;

/**
 * Created by fred on 2017/07/30.
 */
@Singleton
public final class TokenGeneratorImpl implements TokenGenerator {

    public final SecurityToken generate() {
        SecurityToken securityToken = new SecurityToken();
        securityToken.setSecret(Decrypter.generateSecret());
        return securityToken;
    }
}
