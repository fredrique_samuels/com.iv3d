package com.app.tect.server.tvision.bizimpl.accounts;

import com.app.tect.server.tvision.api.email.EmailManager;
import com.app.tect.server.tvision.api.accounts.*;
import com.app.tect.server.tvision.api.validation.ValidationResult;
import com.app.tect.server.tvision.bizimpl.session.SessionThreadContext;
import com.app.tect.server.tvision.security.TokenGenerator;
import com.app.tect.server.tvision.security.ValidationTokenGenerator;
import com.google.inject.Singleton;
import com.iv3d.common.i8n.I8nMessageResolver;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by fred on 2017/07/29.
 */
@Singleton
public class UserAccountManagerImpl implements UserAccountManager {

    private static Logger logger = Logger.getLogger(UserAccountManager.class);
    @Inject
    private UserAccountsDao userAccountsDao;
    @Inject
    private TokenGenerator tokenGenerator;
    @Inject
    private ValidationTokenGenerator validationTokenGenerator;
    @Inject
    private EmailManager emailManager;
    @Inject
    private I8nMessageResolver i8nMessageResolver;

    @Override
    public UserParam createNewUser(String firstName, String lastName, String email, String password) {
        UserParam user = userAccountsDao.getUser(email);
        if(user!=null) {
            throw new EmailAlreadyInUseError(email);
        }

        String credentials = null;
        try {
            credentials = encodeCredentials(password);
        } catch (Exception e) {
            logger.error(e);
            throw new RuntimeException(e);
        }

        UserParam newUser = new UserParam()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setToken("")
                .setCredentials(credentials)
                .setEmail(email);

        UserParam saveUser = userAccountsDao.saveUser(newUser);

        return saveUser;
    }


    @Override
    public UserParam getUser(String email) {
        return userAccountsDao.getUser(email);
    }

    @Override
    public ValidationResult validateExistingUserCredentials(String userName, String password) {
        UserParam user = getUser(userName);
        if(user==null) {
            return new ValidationResult.Builder()
                    .addMessage(i8nMessageResolver.getMessage("useraccounts.credentials.validation.notvalid"))
                    .build();
        }
        try {
            String credentials = encodeCredentials(password);
            boolean isValid = user.getCredentials().equals(credentials);
            if(isValid) {
                return new ValidationResult.Builder()
                        .setStatusSuccess()
                        .build();
            }
            return new ValidationResult.Builder()
                    .addMessage(i8nMessageResolver.getMessage("useraccounts.credentials.validation.notvalid"))
                    .build();
        } catch (Exception e) {
            logger.error(e);
            return new ValidationResult.Builder()
                    .addMessage(i8nMessageResolver.getMessage("useraccounts.internal.error"))
                    .build();
        }
    }

    @Override
    public ValidationResult validateNewUserCredentials(String firstName, String email, String password, String verifyPassword) {
        if(firstName==null || firstName.isEmpty()) {
            return new ValidationResult.Builder()
                    .addNamedMessage("firstName", i8nMessageResolver.getMessage("useraccounts.validation.firstname.empty"))
                    .build();
        }

        if(email==null || email.isEmpty()) {
            return new ValidationResult.Builder()
                    .addNamedMessage("email", i8nMessageResolver.getMessage("useraccounts.validation.email.empty"))
                    .build();
        }

        UserParam user = getUser(email);
        if(user !=null) {
            return new ValidationResult.Builder()
                    .addNamedMessage("email", i8nMessageResolver.getMessage("useraccounts.validation.email.inuse"))
                    .build();
        }

        if(password==null || password.isEmpty()) {
            return new ValidationResult.Builder()
                    .addNamedMessage("password", i8nMessageResolver.getMessage("useraccounts.validation.password.chars"))
                    .build();
        }

        String inputRegex = "[A-Za-z0-9_@.$&]{6,}";
        if(!password.matches(inputRegex)) {
            return new ValidationResult.Builder()
                    .addNamedMessage("password", i8nMessageResolver.getMessage("useraccounts.validation.password.chars"))
                    .build();
        }

        if(verifyPassword==null) {
            return new ValidationResult.Builder()
                    .addNamedMessage("verifyPassword", i8nMessageResolver.getMessage("useraccounts.validation.password.match"))
                    .build();
        }

        if(!password.equals(verifyPassword)) {
            return new ValidationResult.Builder()
                    .addNamedMessage("verifyPassword", i8nMessageResolver.getMessage("useraccounts.validation.password.match"))
                    .build();
        }

        return new ValidationResult.Builder()
                .setStatusSuccess()
                .build();
    }

    @Override
    public AccountParam createNewAccount(UserParam user) {
        AccountParam newAccount = new AccountParam()
                .setOwnerUserId(user.getId());
        newAccount.setValidated(false);
        newAccount.setValidationToken(validationTokenGenerator.generateToken());
        newAccount.setUlu(SessionThreadContext.getSessionContext().getUserId());
        newAccount = userAccountsDao.saveAccount(newAccount);

        UserAccountParam userAccountParam = new UserAccountParam();
        userAccountParam.setUserId(user.getId());
        userAccountParam.setAccountId(newAccount.getId());
        userAccountsDao.saveUserAccount(userAccountParam);

        try {
            emailManager.sendAccountCreatedEmail(user, newAccount);
        } catch (Exception e) {
            logger.error(e);
        }


        return newAccount;
    }

    @Override
    public List<UserParam> getAccountUsers(AccountParam account) {
        return userAccountsDao.getAccountUsers(account.getId());
    }

    @Override
    public List<AccountParam> getUsersAccounts(UserParam user) {
        return userAccountsDao.getUserAccounts(user.getId());
    }

    @Override
    public AccountParam validateUserAccount(long userId, long accountId, String validationToken) {
        AccountParam account = userAccountsDao.getAccountToValidate(userId, accountId, validationToken);
        if(account!=null) {
            if(account.isValidated())return account;
            account.setValidated(true);
            return userAccountsDao.saveAccount(account);
        }
        return null;
    }

    @Inject
    public void setUserAccountsDao(UserAccountsDao userAccountsDao) {
        this.userAccountsDao = userAccountsDao;
    }

    @Inject
    public void setTokenGenerator(TokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;
    }

    @Inject
    public void setValidationTokenGenerator(ValidationTokenGenerator validationTokenGenerator) {
        this.validationTokenGenerator = validationTokenGenerator;
    }

    @Inject
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }


    @Inject
    public void setI8nMessageResolver(I8nMessageResolver i8nMessageResolver) {
        this.i8nMessageResolver = i8nMessageResolver;
    }

    private String encodeCredentials(String password) throws Exception {
        return new sun.misc.BASE64Encoder().encodeBuffer(password.getBytes());
    }

}
