package com.app.tect.server.tvision.bizimpl.security;

import com.app.tect.server.tvision.security.Decrypter;
import com.app.tect.server.tvision.security.ValidationTokenGenerator;
import com.google.inject.Singleton;

/**
 * Created by fred on 2017/09/23.
 */
@Singleton
public class ValidationTokenGeneratorImpl implements ValidationTokenGenerator {
    @Override
    public String generateToken() {
        return (1000+(int) (Math.random()*8999)) + "-" + (10000+(int) (Math.random()*89999));
    }
}
