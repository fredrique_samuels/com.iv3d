package com.app.tect.server.tvision.bizimpl.session;

import com.iv3d.common.core.Transformer;
import com.iv3d.common.i8n.Language;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fred on 2017/08/01.
 */
public final class SessionThreadContext {
    public static final String ATTRIBUTE_USER_ID = "thread.session.attribute.userid";
    public static final String ATTRIBUTE_LANGUAGE = "thread.session.attribute.language";


    private static Map<Long, SessionThreadContext> threadSessionMap = new HashMap<>();
    private HttpSession session;

    public static SessionThreadContext getSessionContext() {
        long id = Thread.currentThread().getId();
        if(!threadSessionMap.containsKey(id)) {
            threadSessionMap.put(id, new SessionThreadContext());
        }
        return threadSessionMap.get(id);
    }

    public static void initThreadSession(HttpSession session) {
        SessionThreadContext sessionContext = getSessionContext();
        sessionContext.setSession(session);
    }

    public Long getUserId() {return getAttribute(ATTRIBUTE_USER_ID, Long.class, null);}
    public Language getLanguage() {
        return getAttribute(ATTRIBUTE_LANGUAGE, (l) -> Language.valueOf(l.toString().toUpperCase()), Language.ENGLISH);
    }

    private <T> T getAttribute(String name, Class<T> type, T defaultValue) {
        if(session==null) return defaultValue;
        Object attribute = session.getAttribute(name);
        if(attribute==null)return defaultValue;
        return type.cast(attribute);
    }

    private <T> T getAttribute(String name, Transformer<Object, T> transformer, T defaultValue) {
        if(session==null) return defaultValue;
        Object attribute = session.getAttribute(name);
        if(attribute==null)return defaultValue;
        return transformer.transform(attribute);
    }

    private <T> T getAttribute(String name, Class<T> type) {
        return getAttribute(name, type, null);
    }

    private SessionThreadContext() {
    }

    private final void setSession(HttpSession session) {
        this.session = session;
    }
}
