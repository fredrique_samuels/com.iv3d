package com.app.tect.server.tvision.apps;

import com.app.tect.server.tvision.api.validation.ValidationResult;
import com.iv3d.common.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2017/09/23.
 */
public class ApiResponseBuilder {

    private boolean success = false;
    private Object data = null;
    private String message = "";
    private List<FieldMessage> fieldMessages = new ArrayList<>();

    public ApiResponseBuilder setSuccess() {
        this.success=true;
        return this;
    }

    public ApiResponseBuilder setValidationError(ValidationResult result) {
        List<String> messages = result.getMessages();
        if(messages.size()>0) {
            this.setMessage(messages.get(0));
        }
        result.getNamedMessages().stream().forEach(nm -> this.addFieldMessage(nm.getName(), nm.getMessage()));

        if(result.isSuccess()){
            setSuccess();
        }
        return this;
    }

    public ApiResponseBuilder setMessage(String s){this.message = s;return this;}
    public ApiResponseBuilder setData(Object d){this.data=d;return this;}
    public ApiResponseBuilder addFieldMessage(String name, String msg){
        this.fieldMessages.add(new FieldMessage(name, msg));
        return this;
    }

    public ApiResponse build() {
        ApiResponse response = new ApiResponse();
        response.setSuccess(this.success);
        response.setMessage(this.message);
        response.setData(this.data);
        response.setFieldErrors(new ArrayList<>(fieldMessages));
        return response;
    }

    public String buildJson() {
        return JsonUtils.writeToString(this.build());
    }
}
