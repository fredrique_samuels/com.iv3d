package com.app.tect.server.tvision.apps.secure.web.common;

public final  class SessionResult {
        String sId;
        Long uId;
        Long aId;

        public String getsId() {
            return sId;
        }

        public Long getuId() {
            return uId;
        }

        public Long getaId() {
            return aId;
        }

        public SessionResult(String sId, Long uId, Long aId) {
            this.sId = sId;
            this.uId = uId;
            this.aId = aId;
        }
    }