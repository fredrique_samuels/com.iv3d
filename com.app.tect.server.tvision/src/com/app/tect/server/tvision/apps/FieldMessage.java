package com.app.tect.server.tvision.apps;

/**
 * Created by fred on 2017/09/23.
 */
public class FieldMessage {
    String field;
    String message;

    public FieldMessage(String field, String msg) {
        this.field = field;
        this.message = msg;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
