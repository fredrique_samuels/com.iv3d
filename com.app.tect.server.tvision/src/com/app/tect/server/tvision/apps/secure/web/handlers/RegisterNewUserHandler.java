package com.app.tect.server.tvision.apps.secure.web.handlers;

import com.app.server.base.server.HttpSessionService;
import com.app.server.base.server.web.RequestHandler;
import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserAccountManager;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.api.validation.ValidationResult;
import com.app.tect.server.tvision.apps.ApiResponseBuilder;
import com.app.tect.server.tvision.apps.secure.web.common.SessionResult;
import com.google.inject.Inject;
import com.iv3d.common.utils.JsonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by fred on 2017/09/23.
 */

public class RegisterNewUserHandler extends RequestHandler {

    @Inject
    private UserAccountManager userAccountManager;

    @Inject
    HttpSessionService httpSessionService;

    @Override
    public String getMethod() {
        return POST;
    }

    @Override
    protected void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        Form form = getJsonBody(Form.class);
        String firstName = form.getFirstName();
        String email = form.getEmail();
        String password = form.getPassword();
        String verifyPassword = form.getVerifyPassword();

        ValidationResult validationResult = userAccountManager.validateNewUserCredentials(firstName, email, password, verifyPassword);

        if(validationResult.isSuccess()) {
            String lastName = form.getLastName();

            UserParam newUser = userAccountManager.createNewUser(firstName, lastName, email, password);
            AccountParam newAccount = userAccountManager.createNewAccount(newUser);

            httpSessionService.logout(getRequest());
            HttpSession session = httpSessionService.login(getRequest(), httpServletRequest, httpServletResponse);

            returnJson(new ApiResponseBuilder()
                    .setData(new SessionResult(session.getId(), newUser.getId(), newAccount.getId()))
                    .setSuccess()
                    .buildJson());
        } else {
            ApiResponseBuilder builder = new ApiResponseBuilder();
            builder.setValidationError(validationResult);
            returnJson(builder.buildJson());
        }

    }

    private Form getJsonBody(Class<Form> formClass) {
        String rawBody = getRawBody();
        return JsonUtils.readFromString(rawBody, formClass);
    }

    public static class Form {
        String email;
        String password;
        String verifyPassword;
        String firstName;
        String lastName;

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String getVerifyPassword() {
            return verifyPassword;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
    }
}
