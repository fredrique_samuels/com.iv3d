package com.app.tect.server.tvision.apps;

/**
 * Created by fred on 2017/09/24.
 */
public interface ApiParameter<T> {
    String getName();
    T getValue();
}
