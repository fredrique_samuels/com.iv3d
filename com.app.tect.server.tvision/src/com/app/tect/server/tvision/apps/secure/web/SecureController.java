package com.app.tect.server.tvision.apps.secure.web;

import com.app.server.base.server.AppWebController;
import com.app.server.base.server.HttpSessionService;
import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.web.RequestHandler;
import com.app.tect.server.tvision.apps.secure.web.handlers.LoginHandler;
import com.app.tect.server.tvision.apps.secure.web.handlers.LogoutHandler;
import com.app.tect.server.tvision.apps.secure.web.handlers.RegisterNewUserHandler;
import com.app.tect.server.tvision.security.SecureRequestHandler;
import com.google.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by fred on 2017/09/22.
 */
public class SecureController extends AppWebController {

    @Inject
    private HttpSessionService httpSessionService;
    @Inject
    private RegisterNewUserHandler registerHandler;
    @Inject
    private LoginHandler loginHandler;
    @Inject
    private LogoutHandler logoutHandler;

    @Inject
    public SecureController(LocalAppSettingsManager appSettingsManager) {
        super(appSettingsManager.get("tvision.server.secure"));
    }

    @Override
    public void configure() {

        bindPathHandler("/register", registerHandler);
        bindPathHandler("/login", loginHandler);
        bindPathHandler("/logout", logoutHandler);

        {
            RequestHandler handler = new SecureRequestHandler() {

                @Override
                protected void handle() {
                    HttpSession session = getSession();
                    returnHtml("<h1>Hello from session " + session.getId() + " USERID"+ session.getAttribute("USERID") + "</h1>");
                }

                @Override
                public String getMethod() {
                    return GET;
                }
            };
            bindPathHandler("/test", handler);
        }

    }
}
