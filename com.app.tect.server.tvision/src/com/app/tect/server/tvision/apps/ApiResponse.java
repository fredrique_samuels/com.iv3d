package com.app.tect.server.tvision.apps;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fred on 2017/09/23.
 */
public class ApiResponse implements Serializable {
    private boolean success;
    private Object data;
    private String message;
    private List<FieldMessage> fieldErrors;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FieldMessage> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldMessage> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }


}
