package com.app.tect.server.tvision.apps.secure.web.handlers;

import com.app.server.base.server.HttpSessionService;
import com.app.server.base.server.web.RequestHandler;
import com.app.tect.server.tvision.apps.ApiResponseBuilder;
import com.app.tect.server.tvision.apps.secure.web.common.SessionResult;
import com.google.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by fred on 2017/09/26.
 */
public class LogoutHandler extends RequestHandler {

    @Inject
    HttpSessionService httpSessionService;

    @Override
    public String getMethod() {
        return POST;
    }

    @Override
    protected void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpSessionService.logout(getRequest());
        returnJson(new ApiResponseBuilder()
                .setData(new SessionResult(null, 0L, 0L))
                .setSuccess()
                .buildJson());
    }
}
