package com.app.tect.server.tvision.apps.secure.web.handlers;

import com.app.tect.server.tvision.apps.ApiResponseBuilder;
import com.app.tect.server.tvision.security.SecureRequestHandler;

/**
 * Created by fred on 2017/09/24.
 */
public class GetAccountSettingsHandler extends SecureRequestHandler {

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle() {
        Long uId = getParameterLong("uId");
        Long aId = getParameterLong("aId");

        ApiResponseBuilder builder = new ApiResponseBuilder();
//        builder.setSuccess().setData()
        returnJson(builder.buildJson());
    }

    public static class Response {

    }
}
