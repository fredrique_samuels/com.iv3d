package com.app.tect.server.tvision.apps.secure.web.handlers;

import com.app.server.base.server.HttpSessionService;
import com.app.server.base.server.web.RequestHandler;
import com.app.tect.server.tvision.api.accounts.AccountParam;
import com.app.tect.server.tvision.api.accounts.UserAccountManager;
import com.app.tect.server.tvision.api.accounts.UserParam;
import com.app.tect.server.tvision.api.validation.ValidationResult;
import com.app.tect.server.tvision.apps.ApiResponseBuilder;
import com.app.tect.server.tvision.apps.secure.web.common.SessionResult;
import com.google.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by fred on 2017/09/26.
 */
public class LoginHandler extends RequestHandler {

    @Inject
    private UserAccountManager userAccountManager;

    @Inject
    HttpSessionService httpSessionService;

    @Override
    public String getMethod() {
        return POST;
    }

    @Override
    protected void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        Form form = getJsonBody(httpServletRequest, Form.class);
        String email = form.getEmail();
        String password = form.getPassword();
        ValidationResult validationResult = userAccountManager.validateExistingUserCredentials(email, password);

        if(validationResult.isSuccess()) {
            UserParam user = userAccountManager.getUser(email);
            AccountParam accountParam = userAccountManager.getUsersAccounts(user).get(0);

            httpSessionService.logout(getRequest());
            HttpSession session = httpSessionService.login(getRequest(), httpServletRequest, httpServletResponse);

            returnJson(new ApiResponseBuilder()
                    .setData(new SessionResult(session.getId(), user.getId(), accountParam.getId()))
                    .setSuccess()
                    .buildJson());
        } else {
            ApiResponseBuilder builder = new ApiResponseBuilder();
            builder.setMessage(validationResult.getMessage());
            returnJson(builder.buildJson());
        }
    }

    public static class Form {
        String email;
        String password;

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
