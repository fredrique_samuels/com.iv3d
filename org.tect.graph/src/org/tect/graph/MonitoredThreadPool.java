/**
 * Copyright (C) 2017 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.graph;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MonitoredThreadPool {
    private final ExecutorService pool;
    private final String name;
    private final String prefix;

    private long threadIdSeed;
    private Map<String, ThreadInfo> threadInfoMap;

    public MonitoredThreadPool(String name, int poolSize) {
        this.pool = Executors.newFixedThreadPool(poolSize);
        this.name = name;
        this.prefix = name == null ? "Thread-" : name + "-";
        this.threadInfoMap = new HashMap<>();
    }

    public void execute(Runnable command) {
        String threadId = newThreadId();

        ThreadInfo threadInfo = new ThreadInfo(threadId);
        MonitoredRunnable task = new MonitoredRunnable(command, threadInfo);
        this.threadInfoMap.put(threadId, threadInfo);

        Future<?> future = pool.submit(task);
        threadInfo.setFuture(future);
    }

    private synchronized String newThreadId() {
        return prefix + (++threadIdSeed);
    }

    private final class MonitoredRunnable implements Runnable {
        private final Runnable command;
        private ThreadInfo threadInfo;

        public MonitoredRunnable(Runnable command, ThreadInfo threadInfo) {
            this.command = command;
            this.threadInfo = threadInfo;
        }

        @Override
        public void run() {
//            try {
//                threadInfo.setRunning();
//                this.command.run();
//            } catch (Exception e) {
//                threadInfo.setError(e);
//            } finally {
//                threadInfo.done();
//            }
        }
    }

    private class ThreadInfo {
        private final String threadId;
        private Future<?> future;

        public ThreadInfo(String threadId) {
            this.threadId = threadId;
        }

        public void setFuture(Future<?> future) {
            this.future = future;
        }
    }
}
