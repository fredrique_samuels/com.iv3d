package org.tect.graph;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class StatusGraphModelTest {

    public enum TestStates implements GraphStatus {
        STATE_A(1, "state.a"),
        STATE_B(2, "state.b");

        private final int i;
        private final String s;

        TestStates(int i, String s) {
            this.i = i;
            this.s = s;
        }

        @Override
        public int getValue() {
            return i;
        }

        @Override
        public String getDisplayName() {
            return s;
        }
    }

    @org.junit.Test
    public void testGraphModelInit() {

        List<GraphStatus> collect = Stream.of(TestStates.values()).collect(Collectors.toList());
        StatusGraphModel statusGraphModel = new StatusGraphModel(collect);
        GraphDataPointCollection pointCollection = statusGraphModel.getCurrentValue();

        assertNotNull(pointCollection);
        assertEquals(2, pointCollection.size());
        assertEquals(0, pointCollection.getTime());
        assertEquals(0, pointCollection.getVersion());

        GraphDataPoint[] data = pointCollection.getData();
        assertNotNull(data);
        assertEquals(2, data.length);

        assertEquals(TestStates.STATE_A.getDisplayName(), data[0].getKey());
        assertEquals(0, data[0].getValue());

        assertEquals(TestStates.STATE_B.getDisplayName(), data[1].getKey());
        assertEquals(0, data[1].getValue());
    }
}