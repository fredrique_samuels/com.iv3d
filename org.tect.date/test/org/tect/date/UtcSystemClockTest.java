package org.tect.date;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import static org.junit.Assert.assertEquals;

public class UtcSystemClockTest {
	
	@Mocked
	ClockMilliSecSource millSource;

    @org.junit.Test
	public void testNow() {
		new NonStrictExpectations() {
			{
				millSource.get();
				result = 1262296800000L;
			}
		};
		
		UtcDate now = new UtcSystemClock(millSource).now();
		assertEquals("2009-12-31T22:00:00+0000", now.toIso8601String());
	}
}
