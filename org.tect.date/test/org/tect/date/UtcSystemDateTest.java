package org.tect.date;

import junit.framework.TestCase;
import org.junit.Test;

public class UtcSystemDateTest extends TestCase {
	
	private UtcDate instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new UtcSystemDate(1262296800000L);
	}

	@Test
	public void testIso8601() {
		assertEquals("2009-12-31T22:00:00+0000", instance.toIso8601String());
	}

    @Test
	public void testGetYear() {
		assertEquals(2009, instance.getYear());
	}

    @Test
	public void testGetMonth() {
		assertEquals(12, instance.getMonth());
	}

    @Test
	public void testGetDay() {
		assertEquals(31, instance.getDay());
	}

    @Test
	public void testGetHour() {
		assertEquals(22, instance.getHour());
	}

    @Test
	public void testGetMinute() {
		instance = new UtcSystemDate(1262297700000L);
		assertEquals(15, instance.getMinute());
	}

    @Test
	public void testGetSecond() {
		instance = new UtcSystemDate(1262297715000L);
		assertEquals(15, instance.getSecond());
	}
}
