/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package org.tect.date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;

public class UtcSystemDate extends UtcDate {

	private DateTime time;

	public UtcSystemDate(long currentTimeMillis) {
		time = new DateTime(currentTimeMillis).withZone(DateTimeZone.UTC);
	}
    public UtcSystemDate(DateTime dateTime) {
        time = dateTime.withZone(DateTimeZone.UTC);
    }

	@Override
	public String toIso8601String() {
		return time.toDateTimeISO().toString("yyyy-MM-dd'T'HH:mm:ssZ");
	}

    @Override
    public String format(String format) {
        return time.toDateTimeISO().toString(format);
    }

    @Override
    public long getMillisecondsSinceEpoc() {
        return time.getMillis();
    }

    @Override
    public String toString() {
        return toIso8601String();
    }

    @Override
    public UtcDuration getDuration(UtcDate end) {
        Duration duration = new Duration(this.time, end instanceof UtcSystemDate ? ((UtcSystemDate) end).time : time);
        return new UtcDuration(this, duration);
    }

    @Override
	public int getYear() {
		return time.getYear();
	}

	@Override
	public int getMonth() {
		return time.getMonthOfYear();
	}

	@Override
	public int getDay() {
		return time.getDayOfMonth();
	}

	@Override
	public int getHour() {
		return time.getHourOfDay();
	}

	@Override
	public int getMinute() {
		return time.getMinuteOfHour();
	}

	@Override
	public int getSecond() {
		return time.getSecondOfMinute();
	}

}
