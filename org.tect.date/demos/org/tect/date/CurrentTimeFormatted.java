package org.tect.date;

/**
 * Created by fred on 2017/11/01.
 */
public class CurrentTimeFormatted {
    public static void main(String[] args) {
        UtcSystemClock utcSystemClock = new UtcSystemClock();
        String now = utcSystemClock.now().format("yyyy-MM-dd HH:mm:ss");
        System.out.println(now);
    }
}
