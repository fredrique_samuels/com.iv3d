package org.tect.date;

import org.joda.time.DateTime;

/**
 * Created by fred on 2017/11/01.
 */
public class JodaDateConversion {
    public static void main(String[] args) {
        DateTime joda = new DateTime().withDate(2016, 10, 14).withTime(8,45,0,0);

        UtcSystemDate utc = new UtcSystemDate(joda);

        System.out.println("Joda to utc " + utc);
        System.out.println("utc to joda " + new DateTime(utc.getMillisecondsSinceEpoc()));
        System.out.println("milliseconds to utc " + new UtcSystemDate(joda.getMillis()));
    }
}
