package iv3d.apps.domain.news24.tasks.bizimpl;

import com.app.server.base.MimeType;
import com.app.server.base.WebMedia;
import com.iv3d.common.core.ResourceLoader;
import iv3d.apps.domain.news24.JsoupService;
import iv3d.apps.domain.news24.News24ArticleManager;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagParam;
import iv3d.apps.domain.news24.tasks.Topic;
import iv3d.apps.domain.worldmap.WorldMapManager;
import iv3d.apps.domain.worldmap.domain.CityData;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by fred on 2016/10/25.
 */
public class TopicToArticleWorkerTest {
    private static final String PLAIN_TEXT_TOPIC_FILE = "tests/news24/PlainTextTopicPage.html";
    private static final String TOPIC_FILE = "tests/news24/TopicPage.html";
    private static final String PLAIN_ARTICLE_FILE = "tests/news24/ArticlePlainText.html";
    private static final String VIDEO_ARTICLE_FILE = "tests/news24/VideoArticle.html";
    private static final String VIDEO_ARTICLE_EMBED_IMAGE_FILE = "tests/news24/VideoWithEmbedImage.html";
    private static final String ARTICLE_EMBED_VIDEO_FILE = "tests/news24/ArticleWithVideo.html";
    private static final String VIDEO_ARTICLE_EMBED_TWITTER = "tests/news24/VideoArticleWithTwitterImage.html";
    private static final String PARENT24_ARTICLE = "tests/news24/ParentNews24Article.html";

    private static final String TOPIC_LINK = "test_link";
    private Topic TOPIC = new Topic(234L, TOPIC_LINK);

    private TopicToArticleWorker instance;
    @Mocked
    JsoupService jsoupService;
    @Mocked
    private News24ArticleManager articleManager;
    @Mocked
    private WorldMapManager worldMapManager;
    @Mocked
    private CityData cityData;

    @Before
    public void setup() {
        instance = new TopicToArticleWorker(jsoupService, articleManager, worldMapManager);
        setupTopicPage(PLAIN_TEXT_TOPIC_FILE);
    }

    private void setupTopicPage(String file) {
        new NonStrictExpectations() {
            {
                jsoupService.getDocumentForLink(TOPIC_LINK,5);
                result = Jsoup.parse(ResourceLoader.readAsString(file));
            }
        };
    }

    @Test
    public void testPlainArticle() {

        new NonStrictExpectations() {
            {
                worldMapManager.searchCityByName(anyString);
                result = Collections.singletonList(cityData);
            }
        };

        String expectedSource = "http://www.news24.com/SouthAfrica/News/panayiotou-2-co-accused-back-in-dock-for-wifes-murder-20161028";
        String expectedTitle = "Panayiotou, 2 co-accused back in dock for wife's murder";
        String expectedSummary = "Alleged murder mastermind Christopher Panayiotou and his two co-accused are expected back in the Eastern Cape High Court.";
        String expectedPubDate = "2016-10-28T03:56:00+0000";


        setupPageSource(expectedSource, PLAIN_ARTICLE_FILE);
        setupNoArticlesPersisted();

        List<News24ArticleParam> articleParams = instance.run(TOPIC);

        News24ArticleParam param = articleParams.get(0);
        assertEquals(expectedTitle, param.getTitle());
        assertEquals(expectedSummary, param.getSummary());
        assertEquals(expectedSource, param.getSourceLink());
        assertEquals(expectedPubDate, param.getPublicationDate().toIso8601String());
        assertEquals(4, param.getTags().size());

        assertTagValue(param, "donald trump", "/Tags/People/donald_trump", 0);
        assertTagValue(param, "hillary clinton", "/Tags/People/hillary_clinton", 1);
        assertTagValue(param, "us", "/Tags/Places/us", 2);
        assertTagValue(param, "us 2016 elections", "/Tags/Topics/us_2016_elections", 3);

        assertEquals(1, param.getMedia().size());

        assertMedia(param.getMedia().get(0), MimeType.IMAGE, "http://cdn.24.co.za/files/Cms/General/d/4616/deb76143f68b4899b0fe22267e548f74.jpg",
                "Republican presidential candidate Donald Trump speaks during a campaign rally in Delaware. (Evan Vucci, AP)"
        );

        assertEquals("St. Augustine", param.getLocation());
        assertTrue(param.getContent().startsWith("St. Augustine - A defiant Donald Trump b"));
        assertNotNull(param.getHtmlPage());
    }

    @Test
    public void testVideoArticle() {

        String expectedSource = "http://www.news24.com/Video/SouthAfrica/News/watch-jhb-resident-chats-to-visitor-outside-home-gets-robbed-20160914";
        String expectedSummary = "A woman in Kensington, Johannesburg, was saying her goodbyes to a visitor outside her residence last Friday evening. A Ford Focus sedan soon pulled up to the driveway and three armed perpetrators stepped out and proceeded to rob them. Watch.";
        String expectedPubDate = "2016-09-14T08:37:00+0000";

        setupTopicPage(TOPIC_FILE);
        setupPageSource(expectedSource, VIDEO_ARTICLE_FILE);
        setupNoArticlesPersisted();

        List<News24ArticleParam> articleParams = instance.run(TOPIC);

        News24ArticleParam param = articleParams.get(0);
        assertEquals("WATCH: Jhb resident chats to visitor outside home, gets robbed", param.getTitle());
        assertEquals(expectedSummary, param.getSummary());
        assertEquals(expectedSource, param.getSourceLink());
        assertEquals(expectedPubDate, param.getPublicationDate().toIso8601String());
        assertEquals(0, param.getTags().size());

        assertEquals(1, param.getMedia().size());

        assertMedia(param.getMedia().get(0),
                MimeType.VIDEO_YOUTUBE, "https://youtu.be/t0ytdngw9lY",
                "");

        assertNotNull(param.getHtmlPage());
    }

    @Test
    public void testVideoArticleWithEmbededImage() {
        String expectedSource = "http://www.news24.com/Video/SouthAfrica/News/watch-jhb-resident-chats-to-visitor-outside-home-gets-robbed-20160914";

        setupTopicPage(TOPIC_FILE);
        setupPageSource(expectedSource, VIDEO_ARTICLE_EMBED_IMAGE_FILE);
        setupNoArticlesPersisted();

        List<News24ArticleParam> articleParams = instance.run(TOPIC);

        News24ArticleParam param = articleParams.get(0);
        assertEquals(2, param.getMedia().size());

        assertMedia(param.getMedia().get(0),
                MimeType.VIDEO_YOUTUBE,
                "https://www.youtube.com/watch?v=y222UmqGAkA",
                "");

        assertMedia(param.getMedia().get(1),
                MimeType.IMAGE,
                "http://cdn.24.co.za/files/Cms/General/d/4250/adaa0b26efe44bcf89c18273b15168ce.jpg",
                "");

        assertNotNull(param.getHtmlPage());
    }

    @Test
    public void testArticleWithEmbeddedVideo() {
        String expectedSource = "http://www.news24.com/Video/SouthAfrica/News/watch-jhb-resident-chats-to-visitor-outside-home-gets-robbed-20160914";

        setupTopicPage(TOPIC_FILE);
        setupPageSource(expectedSource, ARTICLE_EMBED_VIDEO_FILE);
        setupNoArticlesPersisted();

        List<News24ArticleParam> articleParams = instance.run(TOPIC);

        News24ArticleParam param = articleParams.get(0);
        assertEquals(2, param.getMedia().size());

        assertMedia(param.getMedia().get(0),
                MimeType.IMAGE,
                "http://cdn.24.co.za/files/Cms/General/d/4512/782608e90a614d29a370c17988ba9f8d.jpg",
                "Home Affairs and the Hawks arrest officials at Maseru Bridge port of entry. (Jeanette Chabalala, News24)");

        assertMedia(param.getMedia().get(1),
                MimeType.VIDEO_YOUTUBE,
                "https://www.youtube.com/embed/1OX-QJgbGWA",
                "");

        assertNotNull(param.getHtmlPage());
    }

    @Test
    public void testVideoWithEmbeddedTwitter() {
        // use https://publish.twitter.com/oembed?url=https://twitter.com/Interior/status/463440424141459456
        // to get the twitter embedded link

        String expectedSource = "http://www.news24.com/Video/SouthAfrica/News/watch-jhb-resident-chats-to-visitor-outside-home-gets-robbed-20160914";

        setupTopicPage(TOPIC_FILE);
        setupPageSource(expectedSource, VIDEO_ARTICLE_EMBED_TWITTER);
        setupNoArticlesPersisted();

        List<News24ArticleParam> articleParams = instance.run(TOPIC);

        News24ArticleParam param = articleParams.get(0);
        assertEquals(2, param.getMedia().size());

        assertMedia(param.getMedia().get(0),
                MimeType.VIDEO_YOUTUBE,
                "https://www.youtube.com/watch?v=ZPpyFl0EpIM",
                "");

        assertMedia(param.getMedia().get(1),
                MimeType.TWITTER_TWEET,
                "https://twitter.com/Netcare911_sa/status/726025361314746369",
                "");

        assertNotNull(param.getHtmlPage());
    }

    @Test
    public void testParent24Article() {

        String expectedSource = "http://www.news24.com/Video/SouthAfrica/News/watch-jhb-resident-chats-to-visitor-outside-home-gets-robbed-20160914";

        setupTopicPage(TOPIC_FILE);
        setupPageSource(expectedSource, PARENT24_ARTICLE);
        setupNoArticlesPersisted();

        List<News24ArticleParam> articleParams = instance.run(TOPIC);

        News24ArticleParam param = articleParams.get(0);
        List<WebMedia> media = param.getMedia();
        assertEquals(5, media.size());

        assertMedia(media.get(0),
                MimeType.IMAGE,
                "http://cdn.24.co.za/files/Cms/General/d/3699/db17d6d49e4b42e5856e7e037853f848.jpg",
                "(iStock)");

        assertMedia(media.get(1),
                MimeType.TWITTER_TWEET,
                "https://twitter.com/cathjenkin/status/699879699615449088",
                "");

        assertMedia(media.get(2),
                MimeType.TWITTER_TWEET,
                "https://twitter.com/TiffanyBarb/status/699880557963059204",
                "");

        assertMedia(media.get(3),
                MimeType.TWITTER_TWEET,
                "https://twitter.com/thestilettomum/status/699889462801756160",
                "");

        assertMedia(media.get(4),
                MimeType.TWITTER_TWEET,
                "https://twitter.com/EmyDiesel/status/699924078443028481",
                "");

        assertNotNull(param.getHtmlPage());
    }

    private void assertMedia(WebMedia media, MimeType mediaMimeType, String expectedMediaLink, String expectedMediaSummmary) {
        assertEquals(expectedMediaLink, media.getLink());
        assertEquals(expectedMediaSummmary, media.getSummary());
        assertEquals(mediaMimeType, media.getMimetype());
    }

    private void assertTagValue(News24ArticleParam param, String expectedTagName, String expectedTagLink, int index) {
        News24TagParam tag = param.getTags().get(index);
        assertEquals(expectedTagName, tag.getName());
        assertEquals(expectedTagLink, tag.getLink());
    }

    private void setupPageSource(final String expectedSource, final String articleSourceFile) {
        new NonStrictExpectations() {
            {
                jsoupService.getDocumentForLink(expectedSource, 5);
                result = Jsoup.parse(ResourceLoader.readAsString(articleSourceFile));
            }
        };
    }

    private void setupNoArticlesPersisted() {
        new NonStrictExpectations() {
            {
                articleManager.getArticleBySource(anyString);
                result = null;
            }
        };
    }
}