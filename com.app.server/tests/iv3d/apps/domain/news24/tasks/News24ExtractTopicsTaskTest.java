package iv3d.apps.domain.news24.tasks;

import com.iv3d.common.core.ResourceLoader;
import iv3d.apps.domain.news24.JsoupService;
import iv3d.apps.domain.news24.News24ArticleManager;
import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.jsoup.Jsoup;

/**
 * Created by fred on 2016/08/12.
 */
public class News24ExtractTopicsTaskTest extends TestCase {
    private static final  String TEST_TOPIC = "TEST_TOPIC";

    private News24ExtractTopicsTask instance;

    @Mocked
    private News24ArticleManager articleManager;
    @Mocked
    private JsoupService jsoupService;
    @Mocked
    private News24TopicProvider topicProvider;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        instance = new News24ExtractTopicsTask();
        instance.inject(articleManager);
        instance.setJSoupService(jsoupService);
        instance.setTopicProvider(topicProvider);


        new NonStrictExpectations() {
            {
                topicProvider.getTopic();
                result = TEST_TOPIC;

                jsoupService.getDocumentForLink(TEST_TOPIC, anyInt);
                result = Jsoup.parse(ResourceLoader.readAsString("tests/news24item.html"));

                jsoupService.getDocumentForLink(anyString, anyInt);
                result = Jsoup.parse(ResourceLoader.readAsString("tests/news24Article.html"));
            }
        };
    }

    public void testExecute() throws Exception {
        instance.execute();
    }

}