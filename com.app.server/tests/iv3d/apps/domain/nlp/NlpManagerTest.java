package iv3d.apps.domain.nlp;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.LocalAppSettingsManager;
import com.iv3d.common.core.SysOutTreeWriter;
import iv3d.apps.domain.nlp.domain.NlpNode;
import iv3d.apps.domain.nlp.domain.NlpType;
import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import java.util.List;

/**
 * Created by fred on 2016/08/11.
 */
public class NlpManagerTest extends TestCase {

    private NlpManager instance;
    @Mocked
    private LocalAppSettingsManager appSettingsManager;
    @Mocked
    AppContext appContext;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setupAppSettingsManager();
        instance = new NlpManager(appSettingsManager);
    }

    public void testParser() {
        List<NlpNode> parse = instance.parse("Patience has a dog.");
        SysOutTreeWriter sysOutTreeWriter = new SysOutTreeWriter();
        for(NlpNode p : parse) {
            p.printTree(sysOutTreeWriter);
            List<NlpNode> byType = p.getByType(NlpType.NP);
            System.out.println(byType);
        }
    }

    private void setupAppSettingsManager() {
        new NonStrictExpectations() {
            {
                appSettingsManager.get(NlpContents.APP_ID);
                result = appContext;

                appContext.getPackageId();
                result = NlpContents.APP_ID;

                appContext.getPrivateResource(NlpContents.TOKEN_BIN);
                result =  getFileLocation(NlpContents.TOKEN_BIN);

                appContext.getPrivateResource(NlpContents.PERSON_BIN);
                result =  getFileLocation(NlpContents.PERSON_BIN);

                appContext.getPrivateResource(NlpContents.PARSER_BIN);
                result =  getFileLocation(NlpContents.PARSER_BIN);

                appContext.getPrivateResource(NlpContents.LOCATION_BIN);
                result =  getFileLocation(NlpContents.LOCATION_BIN);
            }
        };
    }

    private String getFileLocation(String binFile) {
//        File file = new File(AppSettingUtils.CACHE_APPS_DIR, NlpContents.APP_ID);
//        return new File(file, "private_cache/"+binFile).getPath();
        return "";
    }
}