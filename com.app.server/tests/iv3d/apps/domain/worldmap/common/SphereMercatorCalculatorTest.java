package iv3d.apps.domain.worldmap.common;

import com.iv3d.common.core.LatLng;
import com.iv3d.common.core.MapCoords;
import com.iv3d.common.core.PointDouble;
import com.iv3d.common.map.SphereMercatorCalculator;
import junit.framework.TestCase;

/**
 * Created by fred on 2016/08/03.
 */
public class SphereMercatorCalculatorTest extends TestCase {

    private SphereMercatorCalculator smc;
    private double delta = 0.001;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MapCoords topLeftLatLng = new MapCoords(85, -180);
        MapCoords bottomRightLatLng = new MapCoords(-85 ,180);
        smc =  new SphereMercatorCalculator(topLeftLatLng,
                bottomRightLatLng, 500, 500);
    }

    public void testLatLngToPointZeroZero() throws Exception {
        PointDouble point = smc.latLngToPoint(new MapCoords(0, 0));
        assertEquals(250.0, point.getX(), delta);
        assertEquals(250.0, point.getY(), delta);
    }

    public void testLatLngToPoint() throws Exception {
        PointDouble point = smc.latLngToPoint(new MapCoords(30, 40));
        assertEquals(305.5555555555556, point.getX(), delta);
        assertEquals(206.14394095410384, point.getY(), delta);
    }

    public void testPointToLatLngZeroZero() throws Exception {
        LatLng latLng = smc.pointToLatLng(new PointDouble(250, 250));
        assertEquals(0.0, latLng.getLatitude(), delta);
        assertEquals(0.0, latLng.getLongitude(), delta);
    }

    public void testPointToLatLng() throws Exception {
        LatLng latLng = smc.pointToLatLng(new PointDouble(305.5555555555556, 206.14394095410384));
        assertEquals(30.0, latLng.getLatitude(), delta);
        assertEquals(40.0, latLng.getLongitude(), delta);
    }
}