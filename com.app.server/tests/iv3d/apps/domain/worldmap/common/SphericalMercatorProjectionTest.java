package iv3d.apps.domain.worldmap.common;

import com.iv3d.common.core.LatLng;
import com.iv3d.common.core.MapCoords;
import com.iv3d.common.core.PointDouble;
import com.iv3d.common.map.SphericalMercatorProjection;
import junit.framework.TestCase;


public class SphericalMercatorProjectionTest extends TestCase {

    public void testToPointZeroZero() throws Exception {
        SphericalMercatorProjection smp = new SphericalMercatorProjection(100);
        PointDouble point = smp.toPoint(new MapCoords(0, 0));
        assertEquals(50.0, point.getX());
        assertEquals(50.0, point.getY());
    }

    public void testToLatLngZeroZero() throws Exception {
        SphericalMercatorProjection smp = new SphericalMercatorProjection(100);
        LatLng latLng = smp.toLatLng(new PointDouble(50, 50));
        assertEquals(0.0, latLng.getLatitude());
        assertEquals(0.0, latLng.getLongitude());
    }

    public void testToLatLngTopLeft() throws Exception {
        SphericalMercatorProjection smp = new SphericalMercatorProjection(100);
        LatLng latLng = smp.toLatLng(new PointDouble(0, 0));
        assertEquals(85.05112877980659, latLng.getLatitude(), 0.001);
        assertEquals(-180.0, latLng.getLongitude(), 0.001);
    }

    public void testToLatLngBottomRight() throws Exception {
        SphericalMercatorProjection smp = new SphericalMercatorProjection(100);
        LatLng latLng = smp.toLatLng(new PointDouble(100, 100));
        assertEquals(-85.05112877980659, latLng.getLatitude(), 0.001);
        assertEquals(180.0, latLng.getLongitude(), 0.001);
    }

}