/**
 * Created by fred on 2016/07/21.
 */

// Converts from degrees to radians.
Math.toRadians = function(degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.toDegrees = function(radians) {
    return radians * 180 / Math.PI;
};

Iv3dCore =
{
    __CallbackDispatcher : function()
    {
        var p = {};
        p.dict = {};
        p.dispatch = function(event){Iv3dCore.__dispatch_callback(this, event);};
        p.remove = function (ownerId){Iv3dCore.__remove_callback(this, ownerId);};
        p.add = function (ownerId, callback){Iv3dCore.__add_callback(this, ownerId, callback);};
        return p;
    }
    ,WindowResizeCallbackDispatcher:function() {
        var p = {};
        p.timeout = undefined;
        p.event = undefined;
        p.clear = function(){console.log(this);this.event=undefined;this.task=undefined;}
        p.onresize = function(event, callback){
            this.event = event;
            if(this.timeout){
                this.timeout.reset();
            }
            else {
                this.timeout = Iv3d.ext.Timeout(callback, 300);
            }
        }
        return p;
    }
    ,__add_callback: function(cache, ownerId, callback){if(cache.dict[ownerId]){return;}else{cache.dict[ownerId]=callback;}}
    ,__remove_callback: function(cache, ownerId){if(cache.dict[ownerId]){cache.dict[ownerId];}}
    ,__dispatch_callback: function(cache, event){for(var key in cache.dict){cache.dict[key](event);}}
    ,__uid_seed:0
    ,createNewWidgetId:function(){Iv3dCore.__uid_seed+=1;return Iv3dCore.__uid_seed;}
}

Iv3dEventsCore =
{
    __resize_callbacks : Iv3dCore.__CallbackDispatcher()
    ,__resize_delay_dispatcher : Iv3dCore.WindowResizeCallbackDispatcher()
    ,__on_resize : function(event) {Iv3dEventsCore.__resize_delay_dispatcher.onresize(event, Iv3dEventsCore.__dispatch_resize_event);}
    ,__dispatch_resize_event:function(){Iv3dEventsCore.__resize_delay_dispatcher.clear();Iv3dEventsCore.__resize_callbacks.dispatch(Iv3dEventsCore.__resize_delay_dispatcher.event);}
    ,__init_events: function()
    {
        $(document).ready(function(){
            //console.log($(window).width());
            //console.log($(window).height());
            $(window).on("resize",Iv3dEventsCore.__on_resize);
        });
    }
}

Iv3d =
{
    events :
    {
        onWindowResize : function(ownerId, callback) {Iv3dEventsCore.__resize_callbacks.add(ownerId, callback);}
    }
    ,
    ext :
    {
        Color :
        {
            fromHex : function(hex) {return Iv3d.ext._Color(hex, 1);}
            ,fromHexAndAlpha : function(hex, opacity) {return Iv3d.ext._Color(hex, opacity);}
        }
        ,
        _Color : function(hex, opacity)
        {
            var c = {};
            c.hex = hex;
            c.opacity = opacity;
            c.asJsRgbaStr = function(){return Iv3d.common.hexa2rgbaf(this.hex, this.opacity);}
            c.colorLuminance = function(lum){var luminanceHex = Iv3d.common.colorLuminance(this.hex,lum);return Iv3d.ext.Color.fromHexAndAlpha(luminanceHex, this.opacity);}
            c.lighter = function(lum){return this.colorLuminance(lum);}
            c.darker = function(lum){return this.colorLuminance(-lum);}
            c.withOpacity = function(opacity){return Iv3d.ext.Color.fromHexAndAlpha(this.hex, opacity);}
            return c;
        }
        ,TimerTask:function(callback, intervalMs){
            var t = {};
            t.task = setInterval(callback, intervalMs);
            t.stop = function(){clearInterval(this.task);this.task=undefined;}
            return t;
        }
        ,Timeout:function(callback, intervalMs){
            var t = {};
            t.task = setTimeout(callback, intervalMs);
            t.cb = callback;
            t.interval = intervalMs;
            t.__stop = function(){clearTimeout(this.task);this.task=undefined;};
            t.stop = function(){clearTimeout(this.task);this.task=undefined;this.cb=undefined;};
            t.reset = function(){if(this.cb){this.__stop();this.task = setTimeout(this.cb, this.interval)}};
            return t;
        }
        ,LatLng : function(latitude, longitude) {
            return {"latitude":latitude, "longitude":longitude};
        }
        ,Point2D : function(x, y) {
            return {"x":x, "y":y};
        }
        ,SphericalMercatorProjection : function (worldWidth, worldHeight) {
            var smp = {};
            smp.mWorldWidth = worldWidth;
            smp.mWorldHeight = worldHeight;
            smp.toPoint = function (latLng){
                var x = (latLng.longitude/360) + .5;
                var siny = Math.sin(Math.toRadians(latLng.latitude));
                var y = 0.5 * Math.log((1 + siny) / (1 - siny)) / -(2 * Math.PI) + .5;
                return Iv3d.ext.Point2D(x * this.mWorldWidth, y * this.mWorldHeight);
            };
            smp.toLatLng = function (point) {
                var x = point.x / this.mWorldWidth - 0.5;
                var lng = x * 360;
                var y = .5 - (point.y / this.mWorldHeight);
                var lat = 90 - Math.toDegrees(Math.atan(Math.exp(-y * 2 * Math.PI)) * 2);
                return Iv3d.ext.LatLng(lat, lng);
            };
            return smp;
        }
        ,SphereMercatorCalculator : function(topLeftLatLng, bottomRightLatLng, width, height) {
            var sphericalMercatorProjection = Iv3d.ext.SphericalMercatorProjection(100.0,100.0);
            var topLeftPerc = sphericalMercatorProjection.toPoint(topLeftLatLng);
            var bottomRightPerc = sphericalMercatorProjection.toPoint(bottomRightLatLng);

            var pppx =  (bottomRightPerc.x-topLeftPerc.x);
            var normalizedWidth = (width/pppx)*100;

            var pppy =  (bottomRightPerc.y-topLeftPerc.y);
            var normalizedHeight = (height/pppy)*100;

            var smc = {};
            smc.offsetX = normalizedWidth * (topLeftPerc.x*0.01);
            smc.offsetY = normalizedHeight * (topLeftPerc.y*0.01);
            smc.smp = Iv3d.ext.SphericalMercatorProjection(normalizedWidth, normalizedHeight);
            smc.latLngToPoint = function(latLng) {
                var point = this.smp.toPoint(latLng);
                return Iv3d.ext.Point2D(point.x-this.offsetX, point.y-this.offsetY);
            };
            return smc;
        }
    }
    ,
    common :
    {
        hexa2rgbaf : function(hex, opacity){return 'rgba('+Iv3d.common.hex2rgb(hex).join(',')+','+opacity+')'}
        ,hex2rgb : function(hex)
        {
            var c;
            if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
                c= hex.substring(1).split('');
                if(c.length== 3){
                    c= [c[0], c[0], c[1], c[1], c[2], c[2]];
                }
                c= '0x'+c.join('');
                return [(c>>16)&255, (c>>8)&255, c&255];
            }
            throw new Error('Bad Hex');
        }
        ,colorLuminance : function (hex, lum)
        {
            // https://www.sitepoint.com/javascript-generate-lighter-darker-color/
            //colorLuminance("#69c", 0);		// returns "#6699cc"
            //colorLuminance("6699CC", 0.2);	// "#7ab8f5" - 20% lighter
            //colorLuminance("69C", -0.5);	// "#334d66" - 50% darker
            //colorLuminance("000", 1);		// "#000000" - true black cannot be made lighter!
            // validate hex string
            hex = String(hex).replace(/[^0-9a-f]/gi, '');
            if (hex.length < 6) {
                hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
            }
            lum = lum || 0;

            // convert to decimal and change luminosity
            var rgb = "#", c, i;
            for (i = 0; i < 3; i++) {
                c = parseInt(hex.substr(i*2,2), 16);
                c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                rgb += ("00"+c).substr(c.length);
            }

            return rgb;
        }
        ,polarToCartesian: function (centerX, centerY, radius, angleInDegrees) {
            var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

            return {
                x: centerX + (radius * Math.cos(angleInRadians)),
                y: centerY + (radius * Math.sin(angleInRadians))
            };
        }
        ,describeArc :function(x, y, radius, startAngle, endAngle){

            var start = Iv3d.common.polarToCartesian(x, y, radius, endAngle);
            var end = Iv3d.common.polarToCartesian(x, y, radius, startAngle);

            var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

            var d = [
                "M", start.x, start.y,
                "A", radius, radius, 0, arcSweep, 0, end.x, end.y
            ].join(" ");

            return d;
        }
        ,clone:function(obj) {
            var copy;

            // Handle the 3 simple types, and null or undefined
            if (null == obj || "object" != typeof obj) return obj;

            // Handle Date
            if (obj instanceof Date) {
                copy = new Date();
                copy.setTime(obj.getTime());
                return copy;
            }

            // Handle Array
            if (obj instanceof Array) {
                copy = [];
                for (var i = 0, len = obj.length; i < len; i++) {
                    copy[i] = clone(obj[i]);
                }
                return copy;
            }

            // Handle Object
            if (obj instanceof Object) {
                copy = {};
                for (var attr in obj) {
                    if (obj.hasOwnProperty(attr)) copy[attr] = Iv3d.common.clone(obj[attr]);
                }
                return copy;
            }

            throw new Error("Unable to copy obj! Its type isn't supported.");
        }
    },
    widgets :
    {
        __cache:{}
        ,get:function(widgetId){return Iv3d.widgets.__cache[widgetId];}
        ,exists:function(widgetId){return Iv3d.widgets.__cache[widgetId]?true:false;}
        ,__remove:function(widgetId){if(Iv3d.widgets.exists(widgetId)){var w=Iv3d.widgets.__cache[widgetId];delete Iv3d.widgets.__cache[widgetId];}}
        ,BaseWidget:function(context){
            var widgetId = "widget"+Iv3dCore.createNewWidgetId();
            var w = {};
            w.widgetId = widgetId;
            Iv3d.widgets.__cache[w.widgetId] = w;

            w.el = undefined;
            w.viewUrl = context.viewUrl;
            w.viewParams = context.viewParams;
            w.viewParams["widgetId"] = widgetId;
            w.viewParams["zIndex"] = context.zIndex;
            w.zIndex = context.zIndex;
            w.loadFunc = context.loadFunc;
            w.destroyFunc = context.destroyFunc;
            eval("w.viewLoaded = function(data, status){var w = Iv3d.widgets.get('"+widgetId+"');w.viewParams=undefined;w.el=$(data);w.loadFunc(w);};");
            w.load = function(){if(this.viewUrl) {$.get(this.viewUrl, this.viewParams, this.viewLoaded);}else {this.loadFunc(w);}}
            w.destroy = function(){if(this.destroyFunc){this.destroyFunc(this);}this.cleanup();}
            w.cleanup =function() {if(this.el) {this.el.remove();}Iv3d.widgets.__remove(this.widgetId);}

            w.load();
            return w;
        }
        ,WidgetContext:function(){
            var c = {};
            c.loadFunc = function(w){};
            c.destroyFunc = function(w){};
            c.viewUrl = undefined;
            c.viewParams = {};
            c.zIndex = 1;
            return c;
        }
        ,WorldMapEventIndicatorWidget : function(context)
        {
            // for filtering use
            //context.viewUrl = "/iv3d.worldmap/view/map.ihtml?location_type=country&property_type=code&property_value=ZA"l
            context.viewUrl = "/iv3d.worldmap/view/map.ihtml";
            context.viewParams = {};
            context.viewParams.location_type = "country";
            context.viewParams.property_type = "code";
            context.viewParams.property_value = "ZA";
            return Iv3d.widgets.BaseWidget(context);
        }
    }

};

function initIv3d() {
    Iv3dEventsCore.__init_events();
}

initIv3d();