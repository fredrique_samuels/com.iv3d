package iv3d.apps.domain.opengl.web.handlers;


import com.app.server.base.server.web.RequestHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

public class OglSample1 extends RequestHandler {

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String publicCacheDir = getAppContext().getPublicCacheDir();
        String publicCacheUrlPath = getAppContext().getPublicCacheUrlPath();

        File filePath = new File(publicCacheDir, "sample1/index.html");

        returnOkResponse(response);
    }

    @Override
    public String getMethod() {
        return GET;
    }
}
