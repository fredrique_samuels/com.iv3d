package iv3d.apps.domain.opengl.web;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.google.inject.Inject;
import iv3d.apps.domain.opengl.OpenGlAppConstants;

/**
 * Created by fred on 2016/07/27.
 */
public class OpenglController extends AppWebController {

    @Inject
    public OpenglController(LocalAppSettingsManager cache) {
        super(cache.get(OpenGlAppConstants.PACKAGE_ID));
    }

    @Override
    public void configure() {

    }

}
