package iv3d.apps.domain.opengl;


public class OpenGlAppConstants {
    private OpenGlAppConstants() {
    }

    public static final String PACKAGE_ID = "iv3d.opengl";
}
