package iv3d.apps.domain.dao;

import com.iv3d.common.storage.db.AbstractQuery;
import com.iv3d.common.storage.db.AbstractResultSetTransformer;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by fred on 2016/09/16.
 */
public class GetMaxId extends AbstractQuery<Long> {
    private static final String QS = "select max(id) from '%s'";

    public GetMaxId(String table) {
        super(qs(table), new IdTransformer());
    }

    private static String qs(String table) {
        return String.format(QS, table);
    }

    private static class IdTransformer extends AbstractResultSetTransformer<Long>{
        @Override
        protected Long get(ResultSet rs) throws SQLException {
            return rs.getLong("max(id)");
        }
    }
}
