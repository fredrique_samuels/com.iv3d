package iv3d.apps.domain.nlp;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.LocalAppSettingsManager;
import com.google.inject.Inject;
import com.iv3d.common.core.ResourceLoader;
import iv3d.apps.domain.nlp.domain.NlpNode;
import iv3d.apps.domain.nlp.domain.NlpType;
import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.Parser;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2016/08/07.
 */
public class NlpManager {
    private final TokenizerME tokenizer;
    private final NameFinderME nameFinder;
    private final NameFinderME locationFinder;
    private final Parser parser;

    @Inject
    public NlpManager(LocalAppSettingsManager settingsManager) {
        AppContext appContext = settingsManager.get(NlpContents.APP_ID);
        this.tokenizer = createTokenizer(appContext);
        this.nameFinder = createNameFinder(appContext);
        this.locationFinder = createLocationFinder(appContext);
        this.parser = createParser(appContext);
    }


    public List<String> tokenize(String text) {
        return Arrays.asList(tokenizer.tokenize(text));
    }

    public List<String[]> findNames(String text) {
        String[] tokens = tokenizer.tokenize(text);
        Span nameSpans[] = nameFinder.find(tokens);

        List<String[]> names = new ArrayList<>();
        List<String> tokenList = Arrays.asList(tokens);
        for(Span s: nameSpans) {
            names.add(tokenList.subList(s.getStart(), s.getEnd()).toArray(new String[]{}));
        }
        return names;
    }

    public List<String[]> findLocations(String text) {
        String[] tokens = tokenizer.tokenize(text);
        Span nameSpans[] = locationFinder.find(tokens);

        List<String[]> names = new ArrayList<>();
        List<String> tokenList = Arrays.asList(tokens);
        for(Span s: nameSpans) {
            names.add(tokenList.subList(s.getStart(), s.getEnd()).toArray(new String[]{}));
        }
        return names;
    }

    public List<NlpNode> parse(String text) {
        Parse topParses[] = ParserTool.parseLine(text, this.parser, 1);
        ArrayList<NlpNode> nlpNodes = new ArrayList<>();
        for(Parse p : topParses) {
            nlpNodes.add(createNode(p));
        }
        return Collections.unmodifiableList(nlpNodes);
    }

    private NlpNode createNode(Parse p) {
        String coveredText = p.getCoveredText();
        String type = p.getType();
        List<NlpNode> children = new ArrayList<>();
        for(Parse c:p.getChildren())children.add(createNode(c));

        return new NlpNode(coveredText,
                NlpType.valueOf(type),
                children);
    }

    private Parser createParser(AppContext appContext) {

        ParserModel model = null;
        try {
            String enToken = appContext.getPrivateResource(NlpContents.PARSER_BIN);
            InputStream enTokenIs = ResourceLoader.read(enToken);
            model = new ParserModel(enTokenIs);
            enTokenIs.close();
            return ParserFactory.create(model);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private NameFinderME createNameFinder(AppContext appContext) {
        try {
            String enToken = appContext.getPrivateResource(NlpContents.PERSON_BIN);
            InputStream enTokenIs = ResourceLoader.read(enToken);
            TokenNameFinderModel model = null;
                model = new TokenNameFinderModel(enTokenIs);
            enTokenIs.close();
            return new NameFinderME(model);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private NameFinderME createLocationFinder(AppContext appContext) {
        try {
            String enToken = appContext.getPrivateResource(NlpContents.LOCATION_BIN);
            InputStream enTokenIs = ResourceLoader.read(enToken);
            TokenNameFinderModel model = null;
            model = new TokenNameFinderModel(enTokenIs);
            enTokenIs.close();
            return new NameFinderME(model);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private TokenizerME createTokenizer(AppContext appContext) {
        try {
            String enToken = appContext.getPrivateResource(NlpContents.TOKEN_BIN);
            InputStream enTokenIs = ResourceLoader.read(enToken);
            TokenizerModel tokenizerModel = new TokenizerModel(enTokenIs);
            TokenizerME tokenizerME = new TokenizerME(tokenizerModel);
            enTokenIs.close();
            return tokenizerME;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
