package iv3d.apps.domain.nlp.domain;

import com.iv3d.common.core.TreeWriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by fred on 2016/08/12.
 */
public final class NlpNode {
    private String text;
    private NlpType type;
    private List<NlpNode> children;

    public NlpNode(String text, NlpType type, List<NlpNode> children) {
        this.text = text;
        this.type = type;
        this.children = children;
    }

    public final String getText() {
        return text;
    }

    public final NlpType getType() {
        return type;
    }

    public final List<NlpNode> getChildren() {
        return children;
    }

    @Override
    public final String toString() {
        return "("+type + " " + text+")";
    }

    public final List<NlpNode> getByType(final NlpType filterType) {
        ArrayList<NlpNode> list = new ArrayList<>();
        if(this.type==filterType) {
            list.add(this);
        }
        for(NlpNode n:children) {list.addAll(n.getByType(filterType));};
        return Collections.unmodifiableList(list);
    }

    public final void printTree(TreeWriter printer) {
        printTree(0, printer);
    }

    private void printTree(int level, TreeWriter printer) {
        printer.write(level, toString());
        for(NlpNode n:children)n.printTree(level+1, printer);
    }

}
