package iv3d.apps.domain.nlp;

/**
 * Created by fred on 2016/08/07.
 */
public class NlpContents {
    public static final String APP_ID = "iv3d.nlp";
    public static final String PARSER_BIN = "en-parser-chunking.bin";
    public static final String PERSON_BIN = "en-ner-person.bin";
    public static final String LOCATION_BIN = "en-ner-location.bin";
    public static final String TOKEN_BIN = "en-token.bin";

    private NlpContents() {
    }
}
