package iv3d.apps.domain.news24;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by fred on 2016/09/13.
 */
public class JsoupService {
    public Document getDocumentForLink(String link) {
        try {
            return Jsoup.connect(link).get();
        } catch (IOException e) {
            return null;
        }
    }

    public Document getDocumentForLink(String link, long retryCount) {
        if (retryCount==0)
            return getDocumentForLink(link);

        for (int i = 0; i < retryCount; i++) {
            Document documentForLink = getDocumentForLink(link);
            if(documentForLink!=null)
                return documentForLink;
        }
        return null;
    }
}
