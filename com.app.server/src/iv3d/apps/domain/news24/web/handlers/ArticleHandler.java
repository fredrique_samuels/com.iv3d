package iv3d.apps.domain.news24.web.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;
import com.iv3d.common.core.LatLng;
import com.iv3d.common.core.Transformer;
import iv3d.apps.domain.Location;
import iv3d.apps.domain.NewsItem;
import iv3d.apps.domain.news24.News24ArticleManager;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagToNewsTagTransformer;
import iv3d.apps.domain.worldmap.WorldMapManager;
import iv3d.apps.domain.worldmap.domain.CityData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/18.
 */
public class ArticleHandler extends RequestHandler {

    private static final String LAST_24_HOURS = "LAST_24_HOURS";

    @Inject
    WorldMapManager worldMapManager;

    private final Transformer<News24ArticleParam, NewsItem> transformer = new Transformer<News24ArticleParam, NewsItem>() {
        @Override
        public NewsItem transform(News24ArticleParam from) {
            NewsItem to = new NewsItem();

            to.setId(String.valueOf(from.getId()));
            to.setTitle(from.getTitle());

            String location = from.getLocation();
            List<CityData> cities = worldMapManager.searchCityByName(location);
            List<Location> locations = cities.stream()
                    .map(c -> {
                        Location l = new Location();
                        l.setName(c.getAccentName());
                        l.setCountryCode(c.getCountryCode());
                        l.setType(Location.CITY);

                        LatLng latLng = new LatLng();
                        latLng.setLatitude(c.getLatitude());
                        latLng.setLongitude(c.getLongitude());
                        l.setLatLng(latLng);
                        return l;
                    })
                    .collect(Collectors.toList());

            to.setLocations(locations);
            to.setPublicationDate(from.getPublicationDate());
            to.setSourceLink(from.getSourceLink());
            to.setSummary(from.getSummary());
            to.setContent(from.getContent());
            to.setMedia(from.getMedia());
            to.setTags(from.getTags().stream().map(t -> new News24TagToNewsTagTransformer().transform(t)).collect(Collectors.toList()));
            to.setDataSource("NEWS24");

            return to;
        }
    };

    @Inject
    News24ArticleManager manager;

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String timeRange = getParameter("timeRange");
        String[] categories = getListParameter(request, "category");

        List<News24ArticleParam> article = Collections.EMPTY_LIST;

        if(LAST_24_HOURS.equals(timeRange)) {
            article = manager.getLast24Hours(Arrays.asList(categories));
        }

        returnJsonParam(response, transform(article));
    }

    private List<NewsItem> transform(List<News24ArticleParam> article) {
        return article.stream().map(n -> transformer.transform(n)).collect(Collectors.toList());
    }
}
