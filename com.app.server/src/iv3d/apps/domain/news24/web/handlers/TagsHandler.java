package iv3d.apps.domain.news24.web.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;
import com.iv3d.common.core.Transformer;
import iv3d.apps.domain.NewsTag;
import iv3d.apps.domain.news24.News24ArticleManager;
import iv3d.apps.domain.news24.domain.News24TagParam;
import iv3d.apps.domain.news24.domain.News24TagToNewsTagTransformer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/17.
 */
public class TagsHandler extends RequestHandler {

    private static final String SEARCH = "search";

    private final Transformer<News24TagParam, NewsTag> tagTransformer = new News24TagToNewsTagTransformer();

    private final Transformer<List<News24TagParam>, List<NewsTag>> tagListTransformer =
            from -> from.stream().map(p -> tagTransformer.transform(p)).collect(Collectors.toList());

    @Inject
    News24ArticleManager manager;

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<News24TagParam> tags = Collections.emptyList();

        String searchTerm = getParameter(SEARCH);
        if(searchTerm!=null) {
            tags = manager.searchTags(searchTerm);
        } else {
            tags = manager.getTags();
        }

        returnJsonParam(response, tagListTransformer.transform(tags));
    }
}
