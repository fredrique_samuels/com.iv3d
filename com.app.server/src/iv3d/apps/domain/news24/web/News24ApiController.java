package iv3d.apps.domain.news24.web;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.google.inject.Inject;
import iv3d.apps.domain.news24.News24App;
import iv3d.apps.domain.news24.web.handlers.ArticleHandler;
import iv3d.apps.domain.news24.web.handlers.TagsHandler;

/**
 * Created by fred on 2016/08/05.
 */
public class News24ApiController extends AppWebController {
    @Inject
    private TagsHandler tagsHandler;
    @Inject
    private ArticleHandler articleHandler;

    @Inject
    public News24ApiController(LocalAppSettingsManager cache) {
        super(cache.get(News24App.APP_ID));
    }

    @Override
    public void configure() {
        bindPathHandler("/data/tags.ihtml", tagsHandler);
        bindPathHandler("/data/articles.ihtml", articleHandler);
    }
}
