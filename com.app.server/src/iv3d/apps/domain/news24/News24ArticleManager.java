package iv3d.apps.domain.news24;

import com.app.server.base.WebMedia;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import iv3d.apps.domain.news24.dao.ArticleMetaDataParam;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24Article;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagParam;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by fred on 2016/08/06.
 */
@Singleton
public class News24ArticleManager implements NewsArticleQueryManager {

    @Inject
    private News24Dao dao;

    @Inject
    public News24ArticleManager() {

    }

    public News24Article getArticleBySource(String sourceLink) {
        return null;
    }

    public synchronized long save(News24ArticleParam article) {
        News24ArticleParam articleByLink = dao.getArticleByLink(article.getSourceLink());
        if(articleByLink==null) {
            dao.saveArticle(article);
            return dao.getMaxTableId(News24Dao.ARTICLE_TABLE);
        }
        return articleByLink.getId();
    }

    public synchronized long saveTags(News24TagParam t) {
         News24TagParam persisted = dao.getTag(t.getName());
         if(persisted==null) {
             dao.saveTag(t);
             return dao.getMaxTableId(News24Dao.TAGS_TABLE);
         } else {
             return persisted.getId();
         }
    }

    public boolean isArticleLinkPersisted(String sourceLink) {
        return dao.getArticleByLink(sourceLink) !=null;
    }

    public synchronized long saveMedia(WebMedia wm) {
        WebMedia mediaByLink = dao.getMediaByLink(wm.getLink());
        if(mediaByLink==null) {
            dao.saveMedia(wm);
            return dao.getMaxTableId(News24Dao.MEDIA_TABLE);
        }
        return mediaByLink.getId();
    }

    public synchronized void linkArticleTag(long articleId, Long tagId) {
        dao.saveArticleLink(articleId, tagId);
    }

    public synchronized void linkArticleMedia(long articleId, Long mediaId) {
        dao.saveArticleMedia(articleId, mediaId);
    }

    @Override
    public List<News24ArticleParam> getLast24Hours(List<String> tags) {
        final List<News24ArticleParam> articlesLast24Hours = dao.getArticlesLast24Hours(tags);
//        final List<Long> articleIds = articlesLast24Hours.stream().map(a -> a.getId()).collect(Collectors.toList());
//        final Map<Long, News24ArticleParam> articleMap =  articlesLast24Hours
//                .stream()
//                .collect(Collectors.toMap(News24ArticleParam::getId, Function.identity()));
//
//        List<ArticleMetaDataParam<WebMedia>> mediaList = dao.getMediaByArticleId(articleIds);
//        mediaList.stream().forEach(media -> {
//            News24ArticleParam news24ArticleParam = articleMap.get(media.getArticleId());
//            news24ArticleParam.addMediaUniqueId(media.getValue());
//        });
//
//        List<ArticleMetaDataParam<News24TagParam>> tagList = dao.getTagsByArticleId(articleIds);
//        tagList.stream().forEach(tag -> {
//            News24ArticleParam news24ArticleParam = articleMap.get(tag.getArticleId());
//            news24ArticleParam.addTagUniqueId(tag.getValue());
//        });

        return articlesLast24Hours;
    }

    @Override
    public List<News24TagParam> getTags() {
        return dao.getTags();
    }

    @Override
    public List<News24TagParam> searchTags(String term) {
        return dao.searchTags(term);
    }

}
