package iv3d.apps.domain.news24.domain;

/**
 * Created by fred on 2016/09/13.
 */
public interface News24Tag {
    String getName();
    String getLink();
}
