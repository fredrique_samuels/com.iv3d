package iv3d.apps.domain.news24.domain;

import com.iv3d.common.storage.AuditedParam;

/**
 * Created by fred on 2016/09/13.
 */
public class News24TagParam extends AuditedParam implements News24Tag {
    private String name;
    private String link;

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }
}
