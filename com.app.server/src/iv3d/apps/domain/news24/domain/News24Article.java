package iv3d.apps.domain.news24.domain;

import com.app.server.base.WebMedia;
import com.iv3d.common.date.UtcDate;

import java.util.List;

/**
 * Created by fred on 2016/08/06.
 */
public interface News24Article {
    String getTitle();
    String getLocation();
    UtcDate getPublicationDate();
    String getSourceLink();
    String getSummary();
    List<WebMedia> getMedia();
    String getContent();
    List<News24TagParam> getTags();
    String getHtmlPage();
}
