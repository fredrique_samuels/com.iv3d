package iv3d.apps.domain.news24.domain;

import com.iv3d.common.core.Transformer;
import iv3d.apps.domain.NewsTag;

/**
 * Created by fred on 2016/10/20.
 */
public class News24TagToNewsTagTransformer implements Transformer<News24TagParam, NewsTag> {
    @Override
    public NewsTag transform(News24TagParam from) {
        NewsTag to = new NewsTag();
        to.setId(String.valueOf(from.getId()));
        to.setName(String.valueOf(from.getName()));

        String link = from.getLink();
        if(link!=null && link.startsWith("http")) {
            to.setLink(from.getLink());
        } else {
            to.setLink("http://www.news24.com" + from.getLink());
        }
        return to;
    }
}
