package iv3d.apps.domain.news24.domain;

import com.app.server.base.WebMedia;
import com.iv3d.common.date.UtcDate;
import com.iv3d.common.storage.AuditedParam;

import java.util.ArrayList;
import java.util.List;

public class News24ArticleParam extends AuditedParam implements News24Article {
    private String title;
    private String location;
    private UtcDate publicationDate;
    private String sourceLink;
    private String summary;
    private String content;
    private List<WebMedia> media = new ArrayList<>();
    private List<News24TagParam> tags = new ArrayList<>();
    private String htmlPage;

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public UtcDate getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(UtcDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public String getSourceLink() {
        return sourceLink;
    }

    public void setSourceLink(String sourceLink) {
        this.sourceLink = sourceLink;
    }

    @Override
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public List<WebMedia> getMedia() {
        return media;
    }

    public void addMedia(WebMedia m) {
        media.add(m);
    }

    public void addMediaUniqueId(WebMedia m) {
        boolean present = media.stream().filter(item -> item.getId() == m.getId()).findFirst().isPresent();
        if(!present) {
            media.add(m);
        }
    }

    public void setMedia(List<WebMedia> media) {
        this.media = media;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public List<News24TagParam> getTags() {
        return tags;
    }

    public void setTags(List<News24TagParam> tags) {
        this.tags = tags;
    }

    public void addTag(News24TagParam m) {
        tags.add(m);
    }

    public void addTagUniqueId(News24TagParam m) {
        boolean present = tags.stream().filter(item -> item.getId() == m.getId()).findFirst().isPresent();
        if(!present) {
            tags.add(m);
        }
    }

    public String getHtmlPage() {
        return htmlPage;
    }

    public void setHtmlPage(String htmlPage) {
        this.htmlPage = htmlPage;
    }
}
