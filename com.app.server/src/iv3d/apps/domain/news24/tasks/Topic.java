package iv3d.apps.domain.news24.tasks;

/**
 * Created by fred on 2016/10/17.
 */
public class Topic {
    private final long id;
    private final String link;

    public Topic(long id, String link) {
        this.id = id;
        this.link = link;
    }

    public long getId() {
        return id;
    }

    public String getLink() {
        return link;
    }
}
