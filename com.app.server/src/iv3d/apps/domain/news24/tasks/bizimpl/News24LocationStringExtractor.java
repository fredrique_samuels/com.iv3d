package iv3d.apps.domain.news24.tasks.bizimpl;

import com.iv3d.common.utils.UriUtils;

import java.util.List;

/**
 * Created by fred on 2016/10/17.
 */
public class News24LocationStringExtractor {
    private final static String[] splitMethods = new String[]{
            UriUtils.decode("+%E2%80%93+"),
            UriUtils.decode(" -" + Character.toString ((char) 160)),
            " - "};

    private static final String[] specailTrimCharaters = new String[]{
            UriUtils.decode("%0A"),
    };

    public String extract(List<String> paragraphs) {
        for(String p: paragraphs) {
            for(String splitString : splitMethods) {
                String split = getFirst(p, splitString);
                if (split != null) return split;
            }
        }
        return null;
    }

    private String getFirst(String p, String s) {
        String[] split = p.split(s);
        if(split.length>1) {
            return split[0];
        }
        return null;
    }
}
