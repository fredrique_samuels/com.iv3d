package iv3d.apps.domain.news24.tasks.bizimpl;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.date.UtcSystemDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by fred on 2016/10/26.
 */
public class ArticleItemElement {
    private Element element;

    public ArticleItemElement(Element element) {
        this.element = element;
    }

    public final String getSummary() {
        Elements p = element.select("p");
        if(p.size()>0) {
            return p.get(0).text();
        }
        return null;
    }

    public UtcDate getPublishedDate() {
        Elements select = element.select("span");
        String text = select.get(0).text();

        DateTime dt = null;

        String minutesAgo = " minutes ago";
        String minuteAgo = " minute ago";
        if(text.contains(minutesAgo)) {
            int m = Integer.valueOf(text.replace(minutesAgo, ""));
            dt = new DateTime().minusMinutes(m);
        } else if(text.contains(minuteAgo)) {
            int m = Integer.valueOf(text.replace(minuteAgo, ""));
            dt = new DateTime().minusMinutes(m);
        } else {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
            dt = formatter.parseDateTime(text);
        }

        DateTime dateTime = dt.toDateTime(DateTimeZone.UTC);

        return new UtcSystemDate(dateTime);
    }

    public final String getTitle() {
        Elements select = element.select("a");
        String text = select.get(1).text();
        if(text.isEmpty()) {
            return parseVideoTitle();
        }
        return text;
    }

    public final String getSource() {
        Elements select = element.select("a");
        return select.get(1).attr("href");
    }

    private String parseVideoTitle() {
        Elements titleSectionDiv = element.select("div.title_section");
        Elements aEl = titleSectionDiv.select("a");
        return aEl.get(0).text();
    }
}
