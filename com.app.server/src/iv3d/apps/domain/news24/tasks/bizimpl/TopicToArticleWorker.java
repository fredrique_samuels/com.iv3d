package iv3d.apps.domain.news24.tasks.bizimpl;

import com.app.server.base.MimeType;
import com.app.server.base.WebMedia;
import com.iv3d.common.utils.ExceptionUtils;
import com.iv3d.common.utils.UriUtils;
import iv3d.apps.domain.news24.JsoupService;
import iv3d.apps.domain.news24.News24ArticleManager;
import iv3d.apps.domain.news24.domain.News24Article;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagParam;
import iv3d.apps.domain.news24.tasks.Topic;
import iv3d.apps.domain.worldmap.WorldMapManager;
import iv3d.apps.domain.worldmap.domain.CityData;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/25.
 */
public class TopicToArticleWorker {

    private static Logger logger = Logger.getLogger(TopicToArticleWorker.class);

    private final JsoupService jsoupService;
    private News24ArticleManager articleManager;
    private WorldMapManager worldMapManager;

    public TopicToArticleWorker(JsoupService jsoupService, News24ArticleManager articleManager, WorldMapManager worldMapManager) {
        this.jsoupService = jsoupService;
        this.articleManager = articleManager;
        this.worldMapManager = worldMapManager;
    }

    public List<News24ArticleParam> run(Topic topic) {
        Document doc = getDocumentForLink(topic.getLink());
        if(doc==null) {
            logger.warn("Unable to get web document for " + topic.getLink());
            return Collections.emptyList();
        }

        Elements newsHeadlines = doc.select("div.news_item");
        if(newsHeadlines.size()>0) {
            logger.info("Running extractor for " + topic.getId() +" -> " + topic.getLink());
        }

        ArrayList<News24ArticleParam> params = new ArrayList<>();
        for (Element articleItemElement : newsHeadlines) {
            News24ArticleParam to = getNews24ArticleParam(articleItemElement);
            if (to == null) continue;
            params.add(to);
        }

        return params;
    }

    private News24ArticleParam getNews24ArticleParam(Element articleItemElement) {
        ArticleItemElement itemElement = new ArticleItemElement(articleItemElement);
        News24Article persistedArticle = articleManager.getArticleBySource(itemElement.getSource());
        if(persistedArticle!=null) {
            return null;
        }

        Document articleDocument = getDocumentForLink(itemElement.getSource());
        if(articleDocument==null) {
            logger.warn("Unable to get article at " + itemElement.getSource());
            return null;
        }

        News24ArticleParam to = new News24ArticleParam();
        to.setTitle(itemElement.getTitle());
        to.setSourceLink(itemElement.getSource());
        to.setPublicationDate(itemElement.getPublishedDate());
        to.setSummary(itemElement.getSummary());

        Element articleBodyElement = getArticleElement(articleDocument);
        if(articleBodyElement==null) {
            logger.warn(String.format("Body article for %s not found", to.getSourceLink()));
            return null;
        }

        try {
            List<String> paragraphs = parseParagraphs(articleBodyElement);
            to.setLocation(parserLocation(paragraphs));
            to.setMedia(parseAllMedia(articleDocument));
            to.setContent("".join("\n", paragraphs));
            to.setTags(parserTags(articleDocument));
        } catch (Exception e) {
            logger.warn(String.format("Error parsing %s", to.getSourceLink()));
            logger.warn(ExceptionUtils.getStackTrace(e));
            return null;
        }

        to.setHtmlPage(articleDocument.html());
        return to;
    }

    private String parserLocation(List<String> paragraphs) {
        String extract = new News24LocationStringExtractor().extract(paragraphs);
        List<CityData> cityDatas = worldMapManager.searchCityByName(extract);
        if(cityDatas.size()==0) {
            return null;
        }
        return extract;
    }

    private Element getArticleElement(Document articleDocument) {
        String html = articleDocument.html();
        Elements article = articleDocument.select("article");
        if(article.isEmpty()) {
            article = articleDocument.select("div#divArticleContainer");
        }

        if(article.isEmpty()) {
            return null;
        }
        return article.get(0);
    }

    private Document getDocumentForLink(String link) {
        return jsoupService.getDocumentForLink(link, 5);
    }

    private List<WebMedia> parseAllMedia(Document document) {
        List<WebMedia> media = new ArrayList<>();
        media.addAll(parseFeaturedImage(document));
        media.addAll(parseYoutubeVideos(document));
        media.addAll(parseParant24FeatureImage(document));
        media.addAll(parseEmbeddedImages(document));
        media.addAll(parseEmbeddedYoutubeVideo(document));
        media.addAll(parseEmbeddedTwitterTweets(document));
        return media;
    }

    private List<WebMedia> parseParant24FeatureImage(Document document) {
        Element articleElement = getArticleElement(document);
        Elements select = articleElement.select("span#spnArticleImage");
        return select.stream()
                .map(div ->  parseParent24FeatureImageWebMedia(div))
                .collect(Collectors.toList());
    }

    private WebMedia parseParent24FeatureImageWebMedia(Element div) {
        String link = div.select("img").first().attr("src");
        Element select = div.select("div.caption").first();
        String summary = select.text().trim();
        WebMedia webMedia = createWebMedia(link, MimeType.IMAGE);
        webMedia.setSummary(summary);
        return webMedia;
    }

    private List<WebMedia> parseEmbeddedTwitterTweets(Document document) {
        Element articleElement = getArticleElement(document);
        Elements tweets = articleElement.select("blockquote.twitter-tweet");
        return tweets.stream()
                .map(el -> el.select("a").stream()
                        .map(a->a.attr("href"))
                        .filter(s->s.contains("twitter.com"))
                        .filter(s->s.contains("status"))
                        .findFirst()
                        .get())
                .map(src -> createWebMedia(src, MimeType.TWITTER_TWEET))
                .collect(Collectors.toList());
    }

    private List<WebMedia> parseEmbeddedYoutubeVideo(Document document) {
        Element articleElement = getArticleElement(document);
        Elements imageDiv = articleElement.select("div.youtube");
        return imageDiv.stream()
                .map(div -> div.select("iframe").get(0))
                .map(img -> img.attr("src"))
                .map(src -> createWebMedia(src, MimeType.VIDEO_YOUTUBE))
                .collect(Collectors.toList());
    }

    private List<WebMedia> parseEmbeddedImages(Document document) {
        Element articleElement = getArticleElement(document);
        Elements imageDiv = articleElement.select("div.image");
        return imageDiv.stream()
            .map(div -> div.select("img").get(0))
            .map(img -> img.attr("src"))
            .map(src -> createWebMedia(src, MimeType.IMAGE))
            .collect(Collectors.toList());
    }

    private WebMedia createWebMedia(String src, MimeType image) {
        WebMedia media = new WebMedia();
        media.setLink(src);
        media.setSummary("");
        media.setMimetype(image);
        return media;
    }

    private List<WebMedia> parseYoutubeVideos(Document document) {
        Element articleElement = getArticleElement(document);
        Elements videoParents = articleElement.select("div.video_block");
        return videoParents.stream()
                .map(p -> extractYouVideoUrl(p))
                .map(src -> createWebMedia(src, MimeType.VIDEO_YOUTUBE))
                .collect(Collectors.toList());
    }

    private String extractYouVideoUrl(Element p) {
        String html = p.html();
        Pattern pattern = Pattern.compile("src=\"(.*)\"");
        Matcher matcher = pattern.matcher(html);
        while (matcher.find()) {
            String group = matcher.group(1);
            if(!group.contains("youtu")) {
                continue;
            }
            String s = UriUtils.decode(group.split("vurl=")[1].split("&amp")[0]);
            return s;
        }
        return "";
    }

    private List<News24TagParam> parserTags(Document document) {
        List<News24TagParam> tags = new ArrayList<>();

        Elements select = document.select("div#divKeywordsListing");
        if(select.size()==0) {
            return tags;
        }
        Element tagsDiv = select.get(0);
        Elements aElems = tagsDiv.select("a");
        for (Element a: aElems) {
            String href = a.attr("href");
            String name = a.text();
            News24TagParam tagParam = new News24TagParam();
            tagParam.setName(name);
            tagParam.setLink(href);
            tags.add(tagParam);
        }
        return tags;
    }

    private List<WebMedia> parseFeaturedImage(Document articleBody) {
        Elements select = articleBody.select("div#article_feature");
        String imageLink = null;
        String title = "";
        if(select.size()>0) {
            Element element = select.get(0);
            Elements img = element.select("img");
            imageLink = img.attr("src");
            title = img.attr("title");
        }

        if(imageLink!=null) {
            return Collections.singletonList(WebMedia.image(imageLink, title));
        }
        return Collections.emptyList();
    }

    private List<String> parseParagraphs(Element element) {
        List<String> paragraphs = new ArrayList<>();
        Elements pElements = element.select("p");
        for (Element e: pElements) {
            paragraphs.add(e.text());
        }
        return paragraphs;
    }

}
