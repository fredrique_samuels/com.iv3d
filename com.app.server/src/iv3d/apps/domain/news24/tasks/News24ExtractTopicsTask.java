package iv3d.apps.domain.news24.tasks;

import com.app.server.base.WebMedia;
import com.app.server.base.server.AppTask;
import com.google.inject.Inject;
import com.iv3d.common.utils.ExceptionUtils;
import iv3d.apps.domain.news24.JsoupService;
import iv3d.apps.domain.news24.News24ArticleManager;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagParam;
import iv3d.apps.domain.news24.tasks.bizimpl.TopicToArticleWorker;
import iv3d.apps.domain.worldmap.WorldMapManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fred on 2016/08/05.
 */
public class News24ExtractTopicsTask implements AppTask {
    private Logger logger = Logger.getLogger(News24ExtractTopicsTask.class);

    private News24ArticleManager articleManager;
    private WorldMapManager worldMapManager;
    private News24TopicProvider topicProvider;
    private JsoupService jsoupService;


    public News24ExtractTopicsTask() {

    }

    @Override
    public void execute() {
        List<Topic> links = topicProvider.getTopic();
        links.stream().forEach(l->extractTopic(l));
    }

    private void extractTopic(Topic topic) {
        TopicToArticleWorker topicToArticleWorker = new TopicToArticleWorker(jsoupService,
                articleManager, worldMapManager);

        try {
            List<News24ArticleParam> articleParams = topicToArticleWorker.run(topic);
            articleParams.stream().forEach(a -> saveArticle(a));
        } catch (Exception e) {
            logger.warn(ExceptionUtils.getStackTrace(e));
        }
    }

    private void saveArticle(News24ArticleParam article) {
        long articleId = articleManager.save(article);

        List<Long> tagIds = saveTags(article);
        linkTags(articleId, tagIds);

        List<Long> mediaIds = saveMedia(article.getMedia());
        saveArticleMedia(articleId, mediaIds);
    }

    private void saveArticleMedia(long articleId, List<Long> mediaIds) {
        for(Long id : mediaIds) {
            articleManager.linkArticleMedia(articleId, id);
        }
    }

    private void linkTags(long articleId, List<Long> tagIds) {
        for(Long tagId : tagIds) {
            articleManager.linkArticleTag(articleId, tagId);
        }
    }

    private List<Long> saveMedia(List<WebMedia> media) {
        List<Long> ids = new ArrayList<>();
        for(WebMedia wm : media) {
            ids.add(articleManager.saveMedia(wm));
        }
        return ids;
    }

    private List<Long> saveTags(News24ArticleParam article) {
        List<Long> ids = new ArrayList<>();
        List<News24TagParam> tags = article.getTags();
        for (News24TagParam tag : tags) {
            long id = articleManager.saveTags(tag);
            ids.add(id);
        }
        return ids;
    }

    @Inject
    public void inject(News24ArticleManager articleManager) {
        this.articleManager = articleManager;
    }

    @Inject
    public void setTopicProvider(News24TopicProvider topicProvider) {
        this.topicProvider = topicProvider;
    }

    @Inject
    public void setJSoupService(JsoupService jSoupService) {
        this.jsoupService = jSoupService;
    }

    @Inject
    public void setWorldMapManager(WorldMapManager worldMapManager) {
        this.worldMapManager = worldMapManager;
    }
}
