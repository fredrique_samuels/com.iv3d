package iv3d.apps.domain.news24.tasks;

import com.google.inject.Inject;
import iv3d.apps.domain.news24.News24ArticleManager;
import iv3d.apps.domain.news24.domain.News24TagParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/09/13.
 */
public class News24TopicProvider {
    @Inject
    private News24ArticleManager manager;

    public List<Topic> getTopic() {

//        List<News24TagParam> tags = manager.getTags();
        List<News24TagParam> tags = getSelectedTags();
        return tags.stream().map(t -> new Topic(t.getId(), getLink(t))).collect(Collectors.toList());
    }

    private List<News24TagParam> getSelectedTags() {
        List<News24TagParam> tags = new ArrayList<>();

        tags.addAll(manager.searchTags("crime"));
        tags.addAll(manager.searchTags("theft"));
        tags.addAll(manager.searchTags("robbery"));
        tags.addAll(manager.searchTags("heist"));
        tags.addAll(manager.searchTags("accidents"));
        tags.addAll(manager.searchTags("car accident"));
        tags.addAll(manager.searchTags("murder"));

        if(tags.isEmpty()) {
            createInitialTags(tags);
        }
        return tags;
    }

    private void createInitialTags(List<News24TagParam> tags) {
        {
            News24TagParam e = new News24TagParam();
            e.setLink("/Tags/Topics/crime");
            e.setName("crime");
            tags.add(e);
        }
        {
            News24TagParam e = new News24TagParam();
            e.setLink("/Tags/Topics/robbery");
            e.setName("robbery");
            tags.add(e);
        }
        {
            News24TagParam e = new News24TagParam();
            e.setLink("/Tags/Topics/theft");
            e.setName("robbery");
            tags.add(e);
        }
        {
            News24TagParam e = new News24TagParam();
            e.setLink("/Tags/Topics/heist");
            e.setName("robbery");
            tags.add(e);
        }
    }

    private String getLink(News24TagParam t) {
        if(t.getLink().startsWith("http"))
            return t.getLink();
        return "http://www.news24.com"+t.getLink();
    }
}
