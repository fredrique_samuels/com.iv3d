package iv3d.apps.domain.news24.dao;


public class ArticleMetaDataParam<T>{
    private Long articleId;
    private T value;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
