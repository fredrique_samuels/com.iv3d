package iv3d.apps.domain.news24.dao.migrations;

import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.common.rest.HttpRestClient;
import com.iv3d.common.rest.RestClient;
import com.iv3d.common.storage.db.sv.MigrationRunner;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.dao.article.SaveArticleMissingHtml;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/21.
 */
public class InsertMissingHtml implements MigrationRunner {

    private final News24Dao dao;
    private final HttpRestClient restClient;

    public InsertMissingHtml() {
        this.dao = new News24Dao(new UtcSystemClock());
        this.restClient = new HttpRestClient();
    }

    @Override
    public void run() {
        int offset = 0;
        final int limit = 100;

        List<News24ArticleParam> articles = dao.getHtmlLessArticles(limit, offset);

        while (articles.size()>0) {
            List<String> htmlEntries = articles.stream()
                    .map(a -> restClient.get(a.getSourceLink()))
                    .collect(Collectors.toList());
            dao.saveArticleMissingHtml(articles, htmlEntries);

            offset+=articles.size();
            articles = dao.getHtmlLessArticles(limit, offset);
        }

    }

}
