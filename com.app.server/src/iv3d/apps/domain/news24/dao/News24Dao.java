package iv3d.apps.domain.news24.dao;

import com.app.server.base.WebMedia;
import com.app.server.base.server.AppContext;
import com.app.server.base.server.LocalAppSettingsManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.date.UtcClock;
import com.iv3d.common.storage.db.SqliteConnection;
import iv3d.apps.domain.dao.GetMaxId;
import iv3d.apps.domain.news24.News24App;
import iv3d.apps.domain.news24.dao.article.*;
import iv3d.apps.domain.news24.dao.media.GetMediaByLink;
import iv3d.apps.domain.news24.dao.media.SaveMedia;
import iv3d.apps.domain.news24.dao.tags.GetTags;
import iv3d.apps.domain.news24.dao.tags.SaveTag;
import iv3d.apps.domain.news24.dao.tags.SearchTags;
import iv3d.apps.domain.news24.dao.tags.TagByName;
import iv3d.apps.domain.news24.domain.News24Article;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagParam;

import java.util.List;

/**
 * Created by fred on 2016/08/06.
 */
@Singleton
public class News24Dao {

    public static final String ARTICLE_TABLE = "iv3d_news24_article";
    public static final String TAGS_TABLE = "iv3d_news24_tag";
    public static final String MEDIA_TABLE = "iv3d_news24_media";
    public static final String ARTICLE_TAG_TABLE = "iv3d_news24_article_tag";
    public static final String ARTICLE_MEDIA_TABLE = "iv3d_news24_article_media";

    private final SqliteConnection db;
    private final UtcClock clock;

    @Inject
    public News24Dao(LocalAppSettingsManager appSettingsManager,
                     UtcClock clock) {
        this(clock);
        setupDb(db, appSettingsManager.get(News24App.APP_ID));
    }

    public News24Dao(UtcClock clock) {
        this.clock = clock;
        this.db = new SqliteConnection("jdbc:sqlite:news24.db");
    }

    private void setupDb(SqliteConnection db, AppContext appContext) {
        String sqlRoot = appContext.getPrivateResource("sql");
        db.setupSchemaVersionControl();
        db.updateSchema(sqlRoot);
    }

    public News24Article get(){return null;}

    public News24ArticleParam getArticleByLink(String sourceLink) {
        return db.queryUnique(new GetArticleByLink(sourceLink));
    }

    public void saveTag(News24TagParam t) {
        db.update(new SaveTag(t));
    }

    public News24TagParam getTag(String name) {
        return db.queryUnique(new TagByName(name));
    }

    public Long getMaxTableId(String table) {
        return db.queryUnique(new GetMaxId(table));
    }

    public void saveArticle(News24ArticleParam article) {
        db.update(new SaveArticle(article, clock.now()));
    }

    public void saveMedia(WebMedia wm) {
        db.update(new SaveMedia(wm));
    }

    public WebMedia getMediaByLink(String link) {
        return db.queryUnique(new GetMediaByLink(link));
    }

    public void saveArticleLink(long articleId, Long tagId) {
        db.update(new SaveArticleTag(articleId, tagId));
    }

    public void saveArticleMedia(long articleId, Long mediaId) {
        db.update(new SaveArticleMedia(articleId, mediaId));
    }

    public List<News24TagParam> getTags() {
        return db.query(new GetTags());
    }

    public List<News24TagParam> searchTags(String term) {
        return db.query(new SearchTags(term));
    }

    public List<News24ArticleParam> getArticlesLast24Hours(List<String> tags) {
        return db.query(new GetArticlesLast24Hours(tags));
    }

    public List<ArticleMetaDataParam<WebMedia>> getMediaByArticleId(List<Long> articleIds) {
        return db.query(new GetArticleMedia(articleIds));
    }

    public List<ArticleMetaDataParam<News24TagParam>> getTagsByArticleId(List<Long> articleIds) {
        return db.query(new GetArticleTags(articleIds));
    }

    public List<News24ArticleParam> getHtmlLessArticles(int limit, int offset) {
        return db.query(new GetArticleWithNoHtml(limit, offset));
    }

    public void saveArticleMissingHtml(List<News24ArticleParam> articles, List<String> htmlEntries) {
        db.update(new SaveArticleMissingHtml(articles, htmlEntries));
    }
}
