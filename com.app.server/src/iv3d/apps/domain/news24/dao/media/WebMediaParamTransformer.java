package iv3d.apps.domain.news24.dao.media;

import com.app.server.base.MimeType;
import com.app.server.base.WebMedia;
import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import iv3d.apps.domain.dao.DaoUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by fred on 2016/09/16.
 */
public class WebMediaParamTransformer extends AbstractResultSetTransformer<WebMedia> {

    @Override
    protected WebMedia get(ResultSet rs) throws SQLException {
        WebMedia param = new WebMedia();
        param.setId(rs.getLong(columnName("id")));
        param.setLink(DaoUtils.readString(rs.getString(columnName("link"))));
        param.setSummary(DaoUtils.readString(rs.getString(columnName("summary"))));
        param.setMimetype(DaoUtils.readEnum(rs.getString(columnName("mimetype")), MimeType.class));
        return param;
    }
}
