package iv3d.apps.domain.news24.dao.media;

import com.app.server.base.WebMedia;
import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;

/**
 * Created by fred on 2016/09/16.
 */
public class GetMediaByLink extends AbstractQuery<WebMedia> {


    private static final String QS = "SELECT * FROM %s WHERE link='%s';";

    public GetMediaByLink(String link) {
        super(qs(link), new WebMediaParamTransformer());
    }

    private static String qs(String link) {
        return String.format(QS, News24Dao.MEDIA_TABLE, DaoUtils.persist(link));
    }
}
