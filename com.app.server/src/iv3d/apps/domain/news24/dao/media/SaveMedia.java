package iv3d.apps.domain.news24.dao.media;

import com.app.server.base.WebMedia;
import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;

/**
 * Created by fred on 2016/09/16.
 */
public class SaveMedia extends AbstractDbUpdate {
    private static final String QS = "INSERT INTO %s (link, summary, mimetype) VALUES ('%s', '%s', '%s');";

    public SaveMedia(WebMedia wm) {
        super(qs(wm));
    }

    private static String qs(WebMedia wm) {
        return String.format(QS,
                News24Dao.MEDIA_TABLE,
                DaoUtils.persist(wm.getLink()),
                DaoUtils.persist(wm.getSummary()),
                DaoUtils.persist(wm.getMimetype())
        );
    }
}
