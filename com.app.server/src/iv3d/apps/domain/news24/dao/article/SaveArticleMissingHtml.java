package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.storage.db.AbstractDbUpdate;
import com.iv3d.common.storage.db.DbUpdate;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by fred on 2016/10/21.
 */
public class SaveArticleMissingHtml extends AbstractDbUpdate {

    private static final String QS = "UPDATE %s\n" +
            "    SET html_page = CASE id\n %s" +
            "    ELSE id\n" +
            "    END\n" +
            "    WHERE id IN (%s);";

    public SaveArticleMissingHtml(List<News24ArticleParam> articles, final List<String> htmlEntries) {
        super(qs(articles, htmlEntries));
    }

    private static String qs(List<News24ArticleParam> article, List<String> htmlEntries) {

        List<String> idChecks = IntStream.range(0, article.size())
                .mapToObj(i -> String.format("WHEN %d THEN '%s' ",article.get(i).getId(), DaoUtils.persist(htmlEntries.get(i))))
                .collect(Collectors.toList());

        List<String> idList = article.stream().map(a -> String.valueOf(a.getId())).collect(Collectors.toList());

        return String.format(QS,
                News24Dao.ARTICLE_TABLE,
                String.join("\n", idChecks),
                String.join(" , ", idList));
    }

}
