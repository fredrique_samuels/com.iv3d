package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

/**
 * Created by fred on 2016/09/13.
 */
public class GetArticleByLink extends AbstractQuery<News24ArticleParam> {
    private static final String QS = "select * from %s where link='%s';";

    public GetArticleByLink(String sourceLink) {
        super(qs(sourceLink), new News24ArticleQueryTransformer());
    }

    private static String qs(String sourceLink) {
        return String.format(QS,
                News24Dao.ARTICLE_TABLE,
                DaoUtils.persist(sourceLink));
    }
}
