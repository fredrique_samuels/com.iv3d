package iv3d.apps.domain.news24.dao.article;

import com.app.server.base.WebMedia;
import com.iv3d.common.core.Transformer;
import com.iv3d.common.storage.db.AbstractQuery;
import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import iv3d.apps.domain.news24.dao.ArticleMetaDataParam;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.dao.media.WebMediaParamTransformer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/18.
 */
public class GetArticleMedia  extends AbstractQuery<ArticleMetaDataParam<WebMedia>> {
    private static final String QS = "SELECT am.article_id article_id, m.id media_id, m.link media_link, m.summary media_summary, m.mimetype media_mimetype " +
            "FROM %s m INNER JOIN %s am on am.media_id = m.id " +
            "WHERE am.article_id IN  (%s) " +
            "ORDER BY article_id ASC";

    private static final Transformer<ResultSet, ArticleMetaDataParam<WebMedia>> transformer = new AbstractResultSetTransformer<ArticleMetaDataParam<WebMedia>>() {
        @Override
        protected ArticleMetaDataParam<WebMedia> get(ResultSet rs) throws SQLException {

            WebMediaParamTransformer webMediaParamTransformer = new WebMediaParamTransformer();
            webMediaParamTransformer.setColPrefix("media_");
            WebMedia webMedia = webMediaParamTransformer.transform(rs);

            ArticleMetaDataParam<WebMedia> to = new ArticleMetaDataParam<>();
            to.setArticleId(rs.getLong("article_id"));
            to.setValue(webMedia);
            return to;
        }
    };

    public GetArticleMedia(List<Long> articleIds) {
        super(qs(articleIds), transformer);
    }

    private static String qs(List<Long> articleIds) {
        String ids = String.join(",", articleIds.stream().map(l -> String.valueOf(l)).collect(Collectors.toList()));
        return String.format(QS, News24Dao.MEDIA_TABLE, News24Dao.ARTICLE_MEDIA_TABLE, ids);
    }
}
