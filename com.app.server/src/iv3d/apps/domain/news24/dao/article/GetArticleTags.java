package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.core.Transformer;
import com.iv3d.common.storage.db.AbstractQuery;
import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import iv3d.apps.domain.news24.dao.ArticleMetaDataParam;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.dao.tags.News24TagTransformer;
import iv3d.apps.domain.news24.domain.News24TagParam;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/19.
 */
public class GetArticleTags extends AbstractQuery<ArticleMetaDataParam<News24TagParam>> {
    private static final String QS = "SELECT at.article_id article_id, t.id tag_id, t.link tag_link, t.name tag_name " +
            "FROM %s t INNER JOIN %s at on at.tag_id = t.id " +
            "WHERE at.article_id IN  (%s) " +
            "ORDER BY article_id ASC";

    private static final Transformer<ResultSet, ArticleMetaDataParam<News24TagParam>> transformer = new AbstractResultSetTransformer<ArticleMetaDataParam<News24TagParam>>() {
        @Override
        protected ArticleMetaDataParam<News24TagParam> get(ResultSet rs) throws SQLException {

            News24TagTransformer tagTransformer = new News24TagTransformer();
            tagTransformer.setColPrefix("tag_");
            News24TagParam tag = tagTransformer.transform(rs);

            ArticleMetaDataParam<News24TagParam> to = new ArticleMetaDataParam<>();
            to.setArticleId(rs.getLong("article_id"));
            to.setValue(tag);
            return to;
        }
    };

    public GetArticleTags(List<Long> articleIds) {
        super(qs(articleIds), transformer);
    }

    private static String qs(List<Long> articleIds) {
        String ids = String.join(",", articleIds.stream().map(l -> String.valueOf(l)).collect(Collectors.toList()));
        return String.format(QS, News24Dao.TAGS_TABLE, News24Dao.ARTICLE_TAG_TABLE, ids);
    }
}