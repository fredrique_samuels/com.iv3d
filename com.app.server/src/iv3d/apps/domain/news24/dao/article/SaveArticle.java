package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.date.UtcDate;
import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

/**
 * Created by fred on 2016/09/16.
 */
public class SaveArticle extends AbstractDbUpdate {

    private static final String QS = "INSERT INTO '%s' (title, link, summary, publication_date, location, content, doe, html_page)" +
            " VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');";

    public SaveArticle(News24ArticleParam article, UtcDate doe) {
        super(qs(article, doe));
    }

    private static String qs(News24ArticleParam article, UtcDate doe) {
        return String.format(QS, News24Dao.ARTICLE_TABLE,
                DaoUtils.persist(article.getTitle()),
                DaoUtils.persist(article.getSourceLink()),
                DaoUtils.persist(article.getSummary()),
                DaoUtils.persist(article.getPublicationDate()),
                DaoUtils.persist(article.getLocation()),
                DaoUtils.persist(article.getContent()),
                DaoUtils.persist(doe),
                DaoUtils.persist(article.getHtmlPage()));
    }
}
