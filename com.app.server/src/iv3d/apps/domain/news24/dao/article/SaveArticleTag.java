package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.news24.dao.News24Dao;

/**
 * Created by fred on 2016/09/16.
 */
public class SaveArticleTag extends AbstractDbUpdate {
    private static final String QS = "INSERT INTO %s (article_id, tag_id) VALUES (%d, %d);";

    public SaveArticleTag(long articleId, long tagId) {
        super(qs(articleId, tagId));
    }

    private static String qs(long articleId, long tagId) {
        return String.format(QS, News24Dao.ARTICLE_TAG_TABLE, articleId, tagId);
    }
}
