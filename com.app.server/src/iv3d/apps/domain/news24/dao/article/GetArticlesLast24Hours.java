package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fred on 2016/10/18.
 */
public class GetArticlesLast24Hours extends AbstractQuery<News24ArticleParam> {
    private static final String QS = "SELECT * FROM %s WHERE strftime('%s','now')-STRFTIME('%s',publication_date)<86400 %s ORDER BY STRFTIME('%s',publication_date) DESC;";

    public GetArticlesLast24Hours(List<String> tags) {
        super(qs(tags), new News24ArticleQueryTransformer());
    }

    private static String qs(List<String> tags) {
        return String.format(QS, News24Dao.ARTICLE_TABLE, "%s", "%s", filterSQL(tags), "%s");
    }

    private static String filterSQL(List<String> tags) {
        List<String> joinList = tags.stream().map(t -> "'"+DaoUtils.persist(t)+"'" ).collect(Collectors.toList());
        String sqlTags = String.join(",", joinList);
        return "AND id IN ( select distinct article_id from iv3d_news24_article_tag where tag_id in (select id from iv3d_news24_tag where name IN ("+sqlTags+")) )";
    }

}
