package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

/**
 * Created by fred on 2016/10/21.
 */
public class GetArticleWithNoHtml extends AbstractQuery<News24ArticleParam> {
    private static final String QS = "SELECT * FROM %s WHERE html_page='' LIMIT %d OFFSET %d;";

    public GetArticleWithNoHtml(int limit, int offset) {
        super(qs(limit, offset), new News24ArticleQueryTransformer());
    }

    private static String qs(int limit, int offset) {
        return String.format(QS,
                News24Dao.ARTICLE_TABLE, limit, offset);
    }
}
