package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.domain.News24ArticleParam;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by fred on 2016/09/13.
 */
public class News24ArticleQueryTransformer extends AbstractResultSetTransformer<News24ArticleParam>{

    @Override
    protected News24ArticleParam get(ResultSet rs) throws SQLException {
        News24ArticleParam p = new News24ArticleParam();

//        p.setId(rs.getLong("id"));
        p.setTitle(DaoUtils.readString(rs.getString("title")));
        p.setSourceLink(DaoUtils.readString(rs.getString("link")));
        p.setSummary(DaoUtils.readString(rs.getString("summary")));
        p.setLocation(DaoUtils.readString(rs.getString("location")));
        p.setDoe(DaoUtils.readDate(rs.getString("doe")));
        p.setContent(DaoUtils.readString(rs.getString("content")).replace("\n", ""));
        p.setHtmlPage(DaoUtils.readString(rs.getString("html_page")));
        p.setPublicationDate(DaoUtils.readDate(rs.getString("publication_date")));

        return p;
    }
}
