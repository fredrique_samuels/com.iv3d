package iv3d.apps.domain.news24.dao.article;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.news24.dao.News24Dao;

/**
 * Created by fred on 2016/09/16.
 */
public class SaveArticleMedia extends AbstractDbUpdate {
    private static final String QS = "INSERT INTO %s (article_id, media_id) VALUES (%d, %d);";

    public SaveArticleMedia(long articleId, long mediaId) {
        super(qs(articleId, mediaId));
    }

    private static String qs(long articleId, long mediaId) {
        return String.format(QS, News24Dao.ARTICLE_MEDIA_TABLE, articleId, mediaId);
    }
}
