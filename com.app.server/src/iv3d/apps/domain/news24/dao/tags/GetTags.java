package iv3d.apps.domain.news24.dao.tags;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24TagParam;

public class GetTags extends AbstractQuery<News24TagParam> {
    private static final String QS = "SELECT * from %s;";

    public GetTags() {
        super(qs(), new News24TagTransformer());
    }

    private static String qs() {
        return String.format(QS, News24Dao.TAGS_TABLE);
    }
}
