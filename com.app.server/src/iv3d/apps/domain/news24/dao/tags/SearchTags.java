package iv3d.apps.domain.news24.dao.tags;

import com.iv3d.common.storage.db.AbstractQuery;
import com.iv3d.common.storage.db.DbQuery;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24TagParam;

/**
 * Created by fred on 2016/10/17.
 */
public class SearchTags extends AbstractQuery<News24TagParam> {
    private static final String QS = "SELECT * from %s where name like '%s%s%s';";

    public SearchTags(String term) {
        super(qs(term), new News24TagTransformer());
    }

    private static String qs(String term) {
        return String.format(QS, News24Dao.TAGS_TABLE,
                "%",
                DaoUtils.persist(term),
                "%");
    }
}
