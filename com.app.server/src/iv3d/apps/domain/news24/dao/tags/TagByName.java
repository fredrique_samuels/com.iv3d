package iv3d.apps.domain.news24.dao.tags;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24TagParam;

/**
 * Created by fred on 2016/09/16.
 */
public class TagByName extends AbstractQuery<News24TagParam> {
    private static final String QS = "SELECT * from '%s' where name='%s';";

    public TagByName(String name) {
        super(qs(name), new News24TagTransformer());
    }

    private static String qs(String name) {
        return String.format(QS, News24Dao.TAGS_TABLE, DaoUtils.persist(name));
    }
}
