package iv3d.apps.domain.news24.dao.tags;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.dao.News24Dao;
import iv3d.apps.domain.news24.domain.News24TagParam;

/**
 * Created by fred on 2016/09/16.
 */
public class SaveTag extends AbstractDbUpdate {
    private static final String QS = "INSERT INTO %s (name, link) VALUES ('%s', '%s');";

    public SaveTag(News24TagParam t) {
        super(qs(t));
    }

    private static String qs(News24TagParam t) {
        return String.format(QS,
                News24Dao.TAGS_TABLE,
                DaoUtils.persist(t.getName()),
                DaoUtils.persist(t.getLink()));
    }
}
