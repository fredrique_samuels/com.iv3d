package iv3d.apps.domain.news24.dao.tags;

import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import iv3d.apps.domain.dao.DaoUtils;
import iv3d.apps.domain.news24.domain.News24TagParam;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by fred on 2016/09/16.
 */
public class News24TagTransformer extends AbstractResultSetTransformer<News24TagParam> {

    @Override
    protected News24TagParam get(ResultSet rs) throws SQLException {
        News24TagParam to = new News24TagParam();
        to.setId(rs.getInt(columnName("id")));
        to.setName(DaoUtils.readString(rs.getString(columnName("name"))));
        to.setLink(DaoUtils.readString(rs.getString(columnName("link"))));
        return to;
    }
}
