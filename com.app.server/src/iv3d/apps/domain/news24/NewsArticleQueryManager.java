package iv3d.apps.domain.news24;

import iv3d.apps.domain.NewsItem;
import iv3d.apps.domain.NewsTag;
import iv3d.apps.domain.news24.domain.News24ArticleParam;
import iv3d.apps.domain.news24.domain.News24TagParam;

import java.util.List;

public interface NewsArticleQueryManager {
    List<News24ArticleParam> getLast24Hours(List<String> tags);
    List<News24TagParam> getTags();
    List<News24TagParam> searchTags(String term);
}
