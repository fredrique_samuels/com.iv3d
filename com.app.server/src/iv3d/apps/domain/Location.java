package iv3d.apps.domain;

import com.iv3d.common.core.LatLng;

/**
 * Created by fred on 2016/10/20.
 */
public class Location {
    public static final String CITY = "CITY";
    public static final String COUNTRY = "COUNTRY";

    private String type;
    private String name;
    private LatLng latLng;
    private String countryCode;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
