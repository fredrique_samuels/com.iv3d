/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.domain;

public class DrawingArea {
	private double left;
	private double top;
	private double right;
	private double bottom;
	private double width;
	private double height;

	public DrawingArea(double left, double top, double right, double bottom) {
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.width = right-left;
		this.height = top-bottom;
	}
	
	public double getLeft() {return left;}
	public double getRight() {return right;}
	public double getTop() {return top;}
	public double getBottom() {return bottom;}
	public double getWidth(){return width;}
	public double getHeight(){return height;}
	
	public DrawingArea merge(DrawingArea a) {
		double l = left < a.left ? left : a.left;
		double r = right > a.right ? right : a.right;
		double t = top > a.top ? top : a.top;
		double b = bottom < a.bottom ? bottom : a.bottom;
		return new DrawingArea(l, t, r, b);
	}
}