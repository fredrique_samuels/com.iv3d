package iv3d.apps.domain.worldmap.domain;

/**
 * Created by fred on 2016/08/03.
 */
public interface CityData {
    long getId();
    String getCountryCode();
    String getName();
    String getAccentName();
    String getRegion();
    long getPopulation();
    double getLatitude();
    double getLongitude();
    String getLatLng();
}
