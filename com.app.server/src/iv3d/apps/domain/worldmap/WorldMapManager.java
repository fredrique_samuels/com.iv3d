package iv3d.apps.domain.worldmap;

import com.google.inject.Singleton;
import com.iv3d.common.cache.DataCache;
import com.iv3d.common.storage.db.SqliteConnection;
import iv3d.apps.domain.worldmap.dao.bizimpl.DefaultMapsSvgReaderDao;
import iv3d.apps.domain.worldmap.domain.CityData;
import iv3d.apps.domain.worldmap.domain.CountrySvg;
import iv3d.apps.domain.worldmap.domain.RegionSvg;
import iv3d.apps.domain.worldmap.domain.WorldSvg;

import java.util.Collections;
import java.util.List;

@Singleton
public class WorldMapManager {
    private final static String DB = "jdbc:sqlite:mapsvg.db";
    private final DefaultMapsSvgReaderDao readerDao;
    private final DataCache<String, CityData> cityCache;
    private final DataCache<String, List<CityData>> citySearchCache;

    public WorldMapManager() {
        SqliteConnection db = new SqliteConnection(DB);
        this.readerDao = new DefaultMapsSvgReaderDao(db);
        this.cityCache = new DataCache<>(
                name -> readerDao.getCityByName(name),
                data -> data!=null);
        this.citySearchCache = new DataCache<>(
                key -> readerDao.searchCityByName(key),
                data -> data!=null && !data.isEmpty());
    }

    // SVG methods
    public final WorldSvg getWorldSvg() {return readerDao.getWorldLowRes();}
    public final WorldSvg getWorldSvgHr() {return readerDao.getWorldHighRes();}
    public final List<CountrySvg> getCountySvgs(long worldId) {return readerDao.getCountries(worldId);}

    public final CountrySvg getCountryByName(String name){return readerDao.getCountryByNameLowRes(name);}
    public final CountrySvg getCountryByCode(String code){return readerDao.getCountryByCodeLowRes(code);}

    public List<RegionSvg> getRegionsSvgs(long countryId) {
        return readerDao.getRegions(countryId);
    }

    public CountrySvg getCountyByCode(long worldId, String code) {
        return code==null?null:readerDao.getCountryByCodeInWorld(worldId, code);
    }
    public CountrySvg getCountyByName(long worldId, String code) {
        return code==null?null:readerDao.getCountryByNameInWorld(worldId, code);
    }

    public List<CityData> searchCityByName(String name) {
        if(name==null) {
            return Collections.emptyList();
        }
        if(name.trim().isEmpty()) {
            return Collections.emptyList();
        }
        return citySearchCache.get(name);
    }

    public CityData getCityByName(String name) {
        return name==null?null:cityCache.get(name);
    }
}
