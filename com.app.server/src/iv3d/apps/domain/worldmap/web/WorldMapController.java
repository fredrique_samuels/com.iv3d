/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.web;

import com.app.server.base.server.AppContext;
import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import iv3d.apps.domain.worldmap.WorldMapConstants;
import iv3d.apps.domain.worldmap.web.handlers.CityDataHandler;
import iv3d.apps.domain.worldmap.web.handlers.FullMapHandler;
import iv3d.apps.domain.worldmap.web.handlers.MapPingHandler;
import iv3d.apps.domain.worldmap.web.handlers.MapWidgetPathHandler;

@Singleton
public class WorldMapController extends AppWebController {



    public WorldMapController(AppContext settings) {
        super(settings);
    }

    private FullMapHandler fullMapHandler;
    private MapWidgetPathHandler mapWidgetPathHandler;
    private MapPingHandler pingHandler;

    @Inject
    private CityDataHandler cityDataHandler;

	@Inject
	public WorldMapController(LocalAppSettingsManager settingsCache) {
		super(settingsCache.get(WorldMapConstants.APP_ID));
	}

	@Override
	public void configure() {
        bindPathHandler("/view/ping.ihtml", pingHandler);
        bindPathHandler("/view/map.ihtml", mapWidgetPathHandler);
        bindPathHandler("/view/fullmap.ihtml", fullMapHandler);
        bindPathHandler("/data/city.ihtml", cityDataHandler);
	}

    @Inject
    public void inject(MapWidgetPathHandler handler) {this.mapWidgetPathHandler = handler;}
    @Inject
    public void inject(MapPingHandler handler) {this.pingHandler = handler;}
    @Inject
    public void setFullMapHandler(FullMapHandler fullMapHandler) {
        this.fullMapHandler = fullMapHandler;
    }
}
