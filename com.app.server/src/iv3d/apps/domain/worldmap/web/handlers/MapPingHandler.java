package iv3d.apps.domain.worldmap.web.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.app.server.base.server.web.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by fred on 2016/07/31.
 */
public class MapPingHandler extends RequestHandler {
    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("worldmap/worldmap_ping.macro")
                .addObject("pingId", getParameter("pingId"))
                .addObject("zIndex", getParameter("zIndex"))
                .addObject("x", getParameterDouble("x"))
                .addObject("y", getParameterDouble("y"))
                .addObject("latitude", getParameterDouble("latitude"))
                .addObject("longitude", getParameterDouble("longitude"));
        returnMavJson(mav);
    }

    @Override
    public String getMethod() {
        return GET;
    }
}
