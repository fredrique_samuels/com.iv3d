package iv3d.apps.domain.worldmap.web.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;
import iv3d.apps.domain.worldmap.WorldMapManager;
import iv3d.apps.domain.worldmap.domain.CityData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by fred on 2016/10/30.
 */
public class CityDataHandler extends RequestHandler {

    @Inject
    private WorldMapManager worldMapManager;

    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String search = getParameter("search");
        if(search!=null) {
            List<CityData> cityDatas = worldMapManager.searchCityByName(search);
            returnJsonParam(response, cityDatas);
            return;
        }

        String name = getParameter("name");
        CityData city = worldMapManager.getCityByName(name);
        returnJsonParam(response, city);
    }
}
