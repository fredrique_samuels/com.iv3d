package iv3d.apps.domain.worldmap.web.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.app.server.base.server.web.ModelAndView;
import com.google.inject.Inject;
import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.core.LatLng;
import com.iv3d.common.core.MapCoords;
import com.iv3d.common.core.PointDouble;
import com.iv3d.common.map.SphereMercatorCalculator;
import iv3d.apps.domain.worldmap.WorldMapManager;
import iv3d.apps.domain.worldmap.domain.CountrySvg;
import iv3d.apps.domain.worldmap.domain.WorldSvg;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

public class MapWidgetPathHandler extends RequestHandler {
    enum LocationFilter {
        ALL,
        COUNTRY
    }

    enum PropertyFilter {
        NONE,
        NAME,
        CODE
    }

    private WorldMapManager worldMapManager;

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LocationFilter locationFilter = getParameter("location_type", LocationFilter.ALL, from -> LocationFilter.valueOf(from.toUpperCase()));
        PropertyFilter propertyFilter = getParameter("property_type", PropertyFilter.NONE, from -> PropertyFilter.valueOf(from.toUpperCase()));
        String property = getParameterString("property_value", null);
        String widgetId = getParameter("widgetId");
        String zIndex = getParameter("zIndex");

        WorldSvg world = worldMapManager.getWorldSvg();
        BoundingBoxDouble worldBoundingBox = world.getBoundingBox();
        BoundingBoxDouble targetBB = worldBoundingBox;

        double bottomLatitude = world.getBottomLatitude();
        double topLatitude = world.getTopLatitude();
        double leftLongitude = world.getLeftLongitude();
        double rightLongitude = world.getRightLongitude();

        Object svgs = null;

        if(locationFilter==LocationFilter.ALL) {
            svgs = worldMapManager.getCountySvgs(world.getId());
            targetBB = worldBoundingBox;
        } else if (locationFilter==LocationFilter.COUNTRY) {
            CountrySvg country=null;
            if(propertyFilter==PropertyFilter.CODE) {
                country = worldMapManager.getCountyByCode(world.getId(), property);
            } else if(propertyFilter==PropertyFilter.NAME){
                country = worldMapManager.getCountyByName(world.getId(), property);
            }
            if(country!=null) {
                targetBB = country.getBoundingBox();
                System.out.println(targetBB);
                svgs = Collections.singletonList(country);

                MapCoords latLngTl = new MapCoords(topLatitude, leftLongitude);
                MapCoords latLngBr = new MapCoords(bottomLatitude, rightLongitude);
                SphereMercatorCalculator smc = new SphereMercatorCalculator(latLngTl,
                        latLngBr, worldBoundingBox.getWidth(), worldBoundingBox.getHeight());

                LatLng targetLatLngTl = smc.pointToLatLng(new PointDouble(targetBB.getMinX(), targetBB.getMinY()));
                LatLng targetLatLngBr = smc.pointToLatLng(new PointDouble(targetBB.getMaxX(), targetBB.getMaxY()));

                topLatitude = targetLatLngTl.getLatitude();
                leftLongitude = targetLatLngTl.getLongitude();
                bottomLatitude = targetLatLngBr.getLatitude();
                rightLongitude = targetLatLngBr.getLongitude();
            }
        }

        if(svgs==null) {
            svgs = worldMapManager.getCountySvgs(world.getId());
            targetBB = worldBoundingBox;
        }

        double scaleX = worldBoundingBox.getWidth() / targetBB.getWidth();
        double scaleY = worldBoundingBox.getHeight() / targetBB.getHeight();
        double offsetX = -targetBB.getMinX();
        double offsetY = -targetBB.getMinY();


        ModelAndView mav = new ModelAndView("worldmap/worldmap_widget.macro")
                .addObject("widgetId", widgetId)
                .addObject("zIndex", zIndex)
                .addObject("boundingBox", worldBoundingBox)
                .addObject("svgs", svgs)
                .addObject("scaleX", scaleX)
                .addObject("scaleY", scaleY)
                .addObject("offsetX", offsetX)
                .addObject("offsetY", offsetY)
                .addObject("bottomLatitude", bottomLatitude)
                .addObject("topLatitude", topLatitude)
                .addObject("leftLongitude", leftLongitude)
                .addObject("rightLongitude", rightLongitude);
        returnMav(mav);
    }

    @Override
    public String getMethod() {
        return GET;
    }

    @Inject
    public void inject(WorldMapManager worldMapManager) {
        this.worldMapManager = worldMapManager;
    }
}
