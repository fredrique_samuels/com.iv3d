package iv3d.apps.domain.worldmap.web.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.google.inject.Inject;
import iv3d.apps.domain.worldmap.WorldMapManager;
import iv3d.apps.domain.worldmap.domain.CountrySvg;
import iv3d.apps.domain.worldmap.domain.WorldSvg;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by fred on 2016/09/08.
 */
public class FullMapHandler  extends RequestHandler {
    private WorldMapManager worldMapManager;
    @Override
    public String getMethod() {
        return GET;
    }

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        WorldSvg worldSvg = worldMapManager.getWorldSvg();
        List<CountrySvg> countySvgs = worldMapManager.getCountySvgs(worldSvg.getId());
        returnJsonParam(response, countySvgs);
    }

    @Inject
    public void setWorldMapManager(WorldMapManager worldMapManager) {
        this.worldMapManager = worldMapManager;
    }
}
