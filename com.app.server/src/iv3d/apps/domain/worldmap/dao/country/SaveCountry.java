/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao.country;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.storage.db.AbstractDbUpdate;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.common.utils.UriUtils;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.svgmap.AMAP;
import iv3d.apps.domain.worldmap.svgmap.PATH;

public class SaveCountry extends AbstractDbUpdate {
	private static final String QS = "INSERT INTO %s (world_id, resolution, name, code, svg_path, left_longitude, top_latitude, right_longitude, bottom_latitude, boundingbox_json)"
			+ " VALUES (%d, %d, \"%s\", \"%s\", \"%s\", %.3f, %.3f, %.3f, %.3f, \"%s\");";

    public SaveCountry(String code, String name, int resolution, AMAP amap) {
        super(qs(code, name, resolution, amap));
    }

    private static String qs(String code, String name, int resolution, AMAP amap) {
        return String.format(QS,
                MapsDaoUtils.COUNTRY_TABLE,
                -1,
                resolution,
                UriUtils.encode(name),
                MapsDaoUtils.getEncodedCode(code),
                "",
                Double.valueOf(amap.getLeftLongitude()),
                Double.valueOf(amap.getTopLatitude()),
                Double.valueOf(amap.getRightLongitude()),
                Double.valueOf(amap.getBottomLatitude()),
                MapsDaoUtils.encodeString(JsonUtils.writeToString(new BoundingBoxDouble())));
    }

    private static String qs(long worldId, int resolution, PATH path) {
        String fixed_title = path.getTitle().replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
        return String.format(QS,
				MapsDaoUtils.COUNTRY_TABLE,
                worldId,
                resolution,
				UriUtils.encode(fixed_title),
				MapsDaoUtils.getEncodedCode(path.getId()),
				UriUtils.encode(path.getD()),
                0.0,0.0,0.0,0.0,
				MapsDaoUtils.encodeString(JsonUtils.writeToString(path.getBoundingBox())));
	}

	public SaveCountry(long worldId, PATH path, int resolution) {
		super(qs(worldId, resolution, path));
	}
}
