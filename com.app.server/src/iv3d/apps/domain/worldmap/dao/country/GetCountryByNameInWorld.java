package iv3d.apps.domain.worldmap.dao.country;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.transformer.CountrySvgTransformer;
import iv3d.apps.domain.worldmap.domain.CountrySvg;

/**
 * Created by fred on 2016/07/26.
 */
public class GetCountryByNameInWorld extends AbstractQuery<CountrySvg> {

    private static final String QS = "SELECT * FROM %s WHERE world_id=%d AND name=\"%s\";";

    private static String qs(long worldId, String name) {
        return String.format(QS, MapsDaoUtils.COUNTRY_TABLE, worldId, MapsDaoUtils.encodeString(name));
    }

    public GetCountryByNameInWorld(long worldId, String name) {
        super(qs(worldId, name), new CountrySvgTransformer());
    }
}
