package iv3d.apps.domain.worldmap.dao.country;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.transformer.CountrySvgTransformer;
import iv3d.apps.domain.worldmap.domain.CountrySvg;

/**
 * Created by fred on 2016/07/25.
 */
public class GetCountryByName extends AbstractQuery<CountrySvg> {

    private static final String QS = "SELECT * FROM %s WHERE world_id=-1 AND name='%s' AND resolution=%d;";

    private static String qs(String name, int resolution) {
        return String.format(QS, MapsDaoUtils.COUNTRY_TABLE, MapsDaoUtils.getEncodedCode(name), resolution);
    }

    public GetCountryByName(String name, int resolution) {
        super(qs(name, resolution), new CountrySvgTransformer());
    }
}
