package iv3d.apps.domain.worldmap.dao.country;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;

/**
 * Created by fred on 2016/07/25.
 */
public class UpdateCountryBoundingBox extends AbstractDbUpdate {
    private static final String QS =
            "UPDATE %s SET boundingbox_json=\"%s\" WHERE id=%d;";

    private static String qs(long id, BoundingBoxDouble bb) {
        return String.format(QS, MapsDaoUtils.COUNTRY_TABLE,
                MapsDaoUtils.encodeString(bb.toJson()),
                id);
    }

    public UpdateCountryBoundingBox(long countryId, BoundingBoxDouble bb) {
        super(qs(countryId, bb));
    }

}
