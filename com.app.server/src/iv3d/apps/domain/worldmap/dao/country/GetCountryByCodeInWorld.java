package iv3d.apps.domain.worldmap.dao.country;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.transformer.CountrySvgTransformer;
import iv3d.apps.domain.worldmap.domain.CountrySvg;

/**
 * Created by fred on 2016/07/25.
 */
public class GetCountryByCodeInWorld extends AbstractQuery<CountrySvg>{

    private static final String QS = "SELECT * FROM %s WHERE world_id=%d AND code=\"%s\";";

    private static String qs(long worldId, String code) {
        return String.format(QS, MapsDaoUtils.COUNTRY_TABLE, worldId, MapsDaoUtils.getEncodedCode(code));
    }

    public GetCountryByCodeInWorld(long worldId, String code) {
        super(qs(worldId, code), new CountrySvgTransformer());
    }
}
