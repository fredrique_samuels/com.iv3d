package iv3d.apps.domain.worldmap.dao.country;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.transformer.CountrySvgTransformer;
import iv3d.apps.domain.worldmap.domain.CountrySvg;


public class GetCountryByCode extends AbstractQuery<CountrySvg> {

    private static final String QS = "SELECT * FROM %s WHERE world_id=-1 AND code='%s' AND resolution=%d;";

    private static String qs(String code, int resolution) {
        return String.format(QS, MapsDaoUtils.COUNTRY_TABLE, MapsDaoUtils.getEncodedCode(code), resolution);
    }

    public GetCountryByCode(String code, int resolution) {
        super(qs(code, resolution), new CountrySvgTransformer());
    }
}