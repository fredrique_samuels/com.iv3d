/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao.bizimpl;

import com.iv3d.common.storage.db.SqliteConnection;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.MapsSvgReaderDao;
import iv3d.apps.domain.worldmap.dao.city.GetCityByName;
import iv3d.apps.domain.worldmap.dao.city.SearchCityByName;
import iv3d.apps.domain.worldmap.dao.country.*;
import iv3d.apps.domain.worldmap.dao.region.GetRegionsByCountry;
import iv3d.apps.domain.worldmap.dao.world.GetHighResWorld;
import iv3d.apps.domain.worldmap.dao.world.GetLowResWorld;
import iv3d.apps.domain.worldmap.domain.CityData;
import iv3d.apps.domain.worldmap.domain.CountrySvg;
import iv3d.apps.domain.worldmap.domain.RegionSvg;
import iv3d.apps.domain.worldmap.domain.WorldSvg;

import java.util.List;

public class DefaultMapsSvgReaderDao implements MapsSvgReaderDao {

	private final SqliteConnection db;

	public DefaultMapsSvgReaderDao(SqliteConnection db) {
		this.db = db;
	}

	@Override
	public WorldSvg getWorldLowRes() {
		return db.queryUnique(new GetLowResWorld());
	}

	@Override
	public WorldSvg getWorldHighRes() {
		return db.queryUnique(new GetHighResWorld());
	}

	@Override
	public List<CountrySvg> getCountries(long worldId) {
		return db.query(new GetCountryByWorld(worldId));
	}

    @Override
    public CountrySvg getCountryByCodeLowRes(String code) {return getCountryByCode(code, MapsDaoUtils.RESOLUTION_LOW);}

    @Override
    public CountrySvg getCountryByNameLowRes(String name) {
        return db.queryUnique(new GetCountryByName(name, MapsDaoUtils.RESOLUTION_LOW));
    }

    @Override
    public List<RegionSvg> getRegions(long countryId) {
        return db.query(new GetRegionsByCountry(countryId));
    }

    @Override
    public CountrySvg getCountryByCode(String code, int resolution) {
        return db.queryUnique(new GetCountryByCode(code, resolution));
    }

    @Override
    public CountrySvg getCountryByCodeInWorld(long worldId, String code) {
        return db.queryUnique(new GetCountryByCodeInWorld(worldId, code));
    }

    @Override
    public CountrySvg getCountryByNameInWorld(long worldId, String name) {
        return db.queryUnique(new GetCountryByNameInWorld(worldId, name));
    }

    @Override
    public List<CityData> searchCityByName(String name) {
        return db.query(new SearchCityByName(name));
    }

    @Override
    public CityData getCityByName(String name) {
        return db.queryUnique(new GetCityByName(name));
    }

}
