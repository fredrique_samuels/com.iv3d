/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao.bizimpl;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.storage.db.SqliteConnection;
import iv3d.apps.domain.worldmap.dao.MapsSvgWriterDao;
import iv3d.apps.domain.worldmap.dao.city.SaveCity;
import iv3d.apps.domain.worldmap.dao.country.SaveCountry;
import iv3d.apps.domain.worldmap.dao.country.UpdateCountryBoundingBox;
import iv3d.apps.domain.worldmap.dao.region.SaveRegion;
import iv3d.apps.domain.worldmap.dao.world.SaveWorld;
import iv3d.apps.domain.worldmap.dao.world.UpdateWorldBoundingBox;
import iv3d.apps.domain.worldmap.svgmap.AMAP;
import iv3d.apps.domain.worldmap.svgmap.PATH;
import org.apache.commons.csv.CSVRecord;

import java.util.List;

public class DefaultMapsSvgWriterDao implements MapsSvgWriterDao {

	private SqliteConnection db;

	public DefaultMapsSvgWriterDao(SqliteConnection db) {
		this.db = db;
	}

	@Override
	public void saveWorld(String name, AMAP amap) {
		db.update(new SaveWorld(name, amap));
	}

	@Override
	public void saveCountry(long worldId, PATH path, int resolution) {
		db.update(new SaveCountry(worldId, path, resolution));
	}

    @Override
	public void updateWorldBoundingBox(long id, BoundingBoxDouble bb) {
		db.update(new UpdateWorldBoundingBox(id, bb));
	}

	@Override
	public void saveCities(List<CSVRecord> records) {
		db.update(new SaveCity(records));
	}

    @Override
    public void saveCountry(String code, String name, int resolution, AMAP amap) {
        db.update(new SaveCountry(code, name, resolution, amap));
    }

    @Override
    public void updateCountryBoundingBox(long countryId, BoundingBoxDouble bb) {
        db.update(new UpdateCountryBoundingBox(countryId, bb));
    }

    @Override
    public void saveRegion(long countryId, PATH path) {
        db.update(new SaveRegion(countryId, path));
    }

}
