package iv3d.apps.domain.worldmap.dao.city;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.transformer.CityDataTransformer;
import iv3d.apps.domain.worldmap.domain.CityData;

/**
 * Created by fred on 2016/08/03.
 */
public class GetCityByName extends AbstractQuery<CityData> {
    private static final String QS = "Select * from %s where name='%s';";

    public GetCityByName(String name) {
        super(qs(name), new CityDataTransformer());
    }

    private static String qs(String name) {
        String qName = MapsDaoUtils.encodeString(name.toLowerCase());
        return String.format(QS, MapsDaoUtils.CITY_TABLE, qName);
    }
}
