/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao.world;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.svgmap.AMAP;

public class SaveWorld extends AbstractDbUpdate {
	
	private static final String QS = 
			"INSERT INTO %s (name, left_longitude, top_latitude, right_longitude, bottom_latitude) "
			+ "VALUES (\"%s\", %.3f, %.3f, %.3f, %.3f);";
	
	private static String qs(String name, AMAP amap) {
		return String.format(QS, MapsDaoUtils.WORLD_TABLE,
				name, 
				Double.valueOf(amap.getLeftLongitude()),
				Double.valueOf(amap.getTopLatitude()),
				Double.valueOf(amap.getRightLongitude()),
				Double.valueOf(amap.getBottomLatitude()));
	}
	
	public SaveWorld(String name, AMAP amap) {
		super(qs(name, amap));
	}

}
