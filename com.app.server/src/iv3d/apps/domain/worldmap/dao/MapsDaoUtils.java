/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao;

import com.iv3d.common.utils.UriUtils;

public final class MapsDaoUtils {	
	public static final int RESOLUTION_LOW = 0;
	public static final int RESOLUTION_HIGH = 1;
	
	public static final String SQL_SCRIPT_DIR = "./cache/com.iv3d.maps.svg/sql";

	public static final String CITY_TABLE = "iv3d_mapssvg_city";
	public static final String REGION_TABLE = "iv3d_mapssvg_region";
	public static final String COUNTRY_TABLE = "iv3d_mapssvg_country";
	public static final String CONTINENT_TABLE = "iv3d_mapssvg_continent";
	public static final String WORLD_TABLE = "iv3d_mapssvg_world";
	public static final String WORLD_NAME_LOW_RES = "world_low";
	public static final String WORLD_NAME_HIGH_RES = "world_high";
	
	public static String getSvgColumnNameFromResolution(int res) {
		return res==RESOLUTION_LOW?"svg_lr":"svg_hr";
	}
	
	public static String getEncodedCode(String code) {
		return "_" + code;
	}
	
	public static String getDecodedCode(String code) {
		return code.replace("_", "");
	}
	
	public static String encodeString(String s) {
		return UriUtils.encode(s);
	}
	
	public static String decodeString(String s) {
		return UriUtils.decode(s);
	}
	
	private MapsDaoUtils(){}
	
	
}
