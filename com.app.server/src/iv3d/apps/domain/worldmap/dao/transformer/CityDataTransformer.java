package iv3d.apps.domain.worldmap.dao.transformer;

import com.iv3d.common.storage.db.AbstractResultSetTransformer;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.domain.CityData;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by fred on 2016/08/03.
 */
public class CityDataTransformer extends AbstractResultSetTransformer<CityData> {

    @Override
    protected CityData get(ResultSet resultSet) throws SQLException {
        Impl impl = new Impl();
        impl.id =  resultSet.getLong("id");
        impl.countryCode = MapsDaoUtils.getDecodedCode(resultSet.getString("country_code"));
        impl.name =  MapsDaoUtils.decodeString(resultSet.getString("name"));
        impl.accentName =  MapsDaoUtils.decodeString(resultSet.getString("accent_name"));
        impl.region =  resultSet.getString("region");
        impl.population =  resultSet.getLong("population");
        impl.latitude =  resultSet.getDouble("latitude");
        impl.longitude =  resultSet.getDouble("longitude");
        impl.latLng =  resultSet.getString("longitude");
        return impl;
    }

    private static class Impl implements CityData {
        private long id;
        private String countryCode;
        private String name;
        private String accentName;
        private String region;
        private long population;
        private double latitude;
        private double longitude;
        private String latLng;

        @Override
        public long getId() {
            return id;
        }

        @Override
        public String getCountryCode() {
            return countryCode;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getAccentName() {
            return accentName;
        }

        @Override
        public String getRegion() {
            return region;
        }

        @Override
        public long getPopulation() {
            return population;
        }

        @Override
        public double getLatitude() {
            return latitude;
        }

        @Override
        public double getLongitude() {
            return longitude;
        }

        @Override
        public String getLatLng() {
            return latLng;
        }
    }

}
