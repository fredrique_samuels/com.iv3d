/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao;

import com.iv3d.common.core.BoundingBoxDouble;
import iv3d.apps.domain.worldmap.svgmap.AMAP;
import iv3d.apps.domain.worldmap.svgmap.PATH;
import org.apache.commons.csv.CSVRecord;

import java.util.List;

public interface MapsSvgWriterDao {
	void saveWorld(String name, AMAP amap);
	void updateWorldBoundingBox(long id, BoundingBoxDouble bb);

	void saveCountry(long id, PATH path, int resolution);
    void saveCountry(String code, String name, int resolution, AMAP amap);
    void updateCountryBoundingBox(long id, BoundingBoxDouble bb);

    void saveRegion(long countryId, PATH path);
    void saveCities(List<CSVRecord> records);
}
