package iv3d.apps.domain.worldmap.dao.region;

import com.iv3d.common.storage.db.AbstractQuery;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.transformer.RegionSvgTransformer;
import iv3d.apps.domain.worldmap.domain.RegionSvg;


/**
 * Created by fred on 2016/07/25.
 */
public class GetRegionsByCountry  extends AbstractQuery<RegionSvg> {

    private static final String QS = "SELECT * FROM %s WHERE country_id=%d;";

    private static String qs(long countryId) {
        return String.format(QS, MapsDaoUtils.REGION_TABLE, countryId);
    }

    public GetRegionsByCountry(long countryId) {
        super(qs(countryId), new RegionSvgTransformer());
    }
}
