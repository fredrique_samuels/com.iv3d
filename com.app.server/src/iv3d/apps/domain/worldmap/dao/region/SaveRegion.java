package iv3d.apps.domain.worldmap.dao.region;

import com.iv3d.common.storage.db.AbstractDbUpdate;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.common.utils.UriUtils;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.svgmap.PATH;

public class SaveRegion extends AbstractDbUpdate {
    private static final String QS = "INSERT INTO %s (country_id, name, code, svg_path, boundingbox_json)"
            + " VALUES (%d, \"%s\", \"%s\", \"%s\", \"%s\");";

    private static String qs(long countryId, PATH path) {
        String fixed_title = path.getTitle().replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
        System.out.println(fixed_title);
        return String.format(QS,
                MapsDaoUtils.REGION_TABLE,
                countryId,
                UriUtils.encode(fixed_title),
                MapsDaoUtils.getEncodedCode(path.getId()),
                UriUtils.encode(path.getD()),
                MapsDaoUtils.encodeString(JsonUtils.writeToString(path.getBoundingBox())));
    }

    public SaveRegion(long countryId, PATH path) {
        super(qs(countryId, path));
    }
}
