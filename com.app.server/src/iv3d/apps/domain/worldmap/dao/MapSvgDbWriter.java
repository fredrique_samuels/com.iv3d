/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.dao;

import com.iv3d.common.storage.db.SqliteConnection;
import iv3d.apps.domain.worldmap.dao.bizimpl.DefaultMapsSvgReaderDao;
import iv3d.apps.domain.worldmap.dao.bizimpl.DefaultMapsSvgWriterDao;
import iv3d.apps.domain.worldmap.dao.writers.CityDataWriter;
import iv3d.apps.domain.worldmap.dao.writers.CountryDataWriter;
import iv3d.apps.domain.worldmap.dao.writers.WorldDataWriter;

import java.io.File;

public final class MapSvgDbWriter {
	private final String fileRoot;
	private final MapsSvgReaderDao readerDao;
	private final MapsSvgWriterDao writerDao;
	
	public MapSvgDbWriter(String fileRoot, MapsSvgReaderDao readerDao, MapsSvgWriterDao writerDao) {
		this.fileRoot = fileRoot;
		this.readerDao = readerDao;
		this.writerDao = writerDao;
	}
	
	public static void main(String[] args) {
        new File("mapsvg.db").delete();
		SqliteConnection db = new SqliteConnection("jdbc:sqlite:mapsvg.db");
		db.setupSchemaVersionControl();
		db.updateSchema("./cache/apps/iv3d.worldmap/private_cache/sql");
		MapsSvgReaderDao readerDao = new DefaultMapsSvgReaderDao(db);
		MapsSvgWriterDao writerDao = new DefaultMapsSvgWriterDao(db);
		
		MapSvgDbWriter svgDbWriter = new MapSvgDbWriter("./cache/apps/iv3d.worldmap/private_cache",
				readerDao, writerDao);
		svgDbWriter.run();
	}

	public void run() {
        new WorldDataWriter(readerDao, writerDao, fileRoot).run();
        new CountryDataWriter(readerDao, writerDao, fileRoot).run();
        new CityDataWriter(writerDao, fileRoot).run();
	}

}
