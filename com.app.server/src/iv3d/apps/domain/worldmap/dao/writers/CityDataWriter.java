package iv3d.apps.domain.worldmap.dao.writers;

import iv3d.apps.domain.worldmap.dao.MapsSvgWriterDao;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by fred on 2016/08/03.
 */
public class CityDataWriter {
    private Logger logger = Logger.getLogger(CityDataWriter.class);
    private final MapsSvgWriterDao writerDao;
    private final String fileRoot;

    public CityDataWriter(MapsSvgWriterDao writerDao, String fileRoot) {
        this.writerDao = writerDao;
        this.fileRoot = fileRoot;
    }

    public void run() {
        saveCities();
    }

    private void saveCities() {
        File[] files = new File(fileRoot, "cities").listFiles();
        Arrays.asList(files).stream().forEach(f -> saveCitiesFile(f));
    }

    private void saveCitiesFile(File file) {
        String csvFile = file.getPath();//new File(fileRoot, file).getPath();
        Reader in;
        logger.info(csvFile);
        try {
            in = new FileReader(csvFile);
            Iterable<CSVRecord> records = CSVFormat.RFC4180
                    .withHeader("Country","City","AccentCity","Region","Population","Latitude","Longitude")
                    .parse(in);
            int count = 0;
            List<CSVRecord> writeList = new ArrayList<>();
            for (CSVRecord record : records) {
                count++;

                boolean skipHeader = count==1;
                if(skipHeader)continue;

                writeList.add(record);

                boolean write = writeList.size()==1000;
                if(write) {
                    logger.debug("Saved " +count+ " cities ...");
                    writeCities(writeList);
                }
            }
            writeCities(writeList);
            logger.debug("Saved a total of " + count + " cities.");
        } catch (IOException e) {
            throw new CsvParserError(e);
        }
    }

    private void writeCities(List<CSVRecord> writeList) {
        if(writeList.size()>0) {
            writerDao.saveCities(writeList);
            writeList.clear();
        }
    }

    public static final class CsvParserError extends RuntimeException {
        private static final long serialVersionUID = 1L;
        public CsvParserError(IOException e) {super(e);}
    }
}
