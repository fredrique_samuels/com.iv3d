package iv3d.apps.domain.worldmap.dao.writers;

import com.iv3d.common.core.BoundingBoxDouble;
import iv3d.apps.domain.worldmap.dao.MapsDaoUtils;
import iv3d.apps.domain.worldmap.dao.MapsSvgReaderDao;
import iv3d.apps.domain.worldmap.dao.MapsSvgWriterDao;
import iv3d.apps.domain.worldmap.domain.WorldSvg;
import iv3d.apps.domain.worldmap.svgmap.AMAP;
import iv3d.apps.domain.worldmap.svgmap.PATH;
import iv3d.apps.domain.worldmap.svgmap.SvgMap;

import java.io.File;

public class WorldDataWriter {
    private final MapsSvgReaderDao readerDao;
    private final MapsSvgWriterDao writerDao;
    private final String fileRoot;

    public WorldDataWriter(MapsSvgReaderDao readerDao, MapsSvgWriterDao writerDao, String fileRoot) {
        this.readerDao = readerDao;
        this.writerDao = writerDao;
        this.fileRoot = new File(fileRoot, "js").getPath();
    }

    public void run() {
        writeLowResWorld();
        writeHighResWorld();
    }

    private void writeHighResWorld() {
        SvgMap svg = writeWorld(MapsDaoUtils.WORLD_NAME_HIGH_RES, "worldHigh.js");
        WorldSvg world = readerDao.getWorldHighRes();
        BoundingBoxDouble bb = writeContinents(svg, world, MapsDaoUtils.RESOLUTION_HIGH);
        writerDao.updateWorldBoundingBox(world.getId(), bb);
    }

    private void writeLowResWorld() {
        SvgMap svg = writeWorld(MapsDaoUtils.WORLD_NAME_LOW_RES, "worldLow.js");
        WorldSvg world = readerDao.getWorldLowRes();
        BoundingBoxDouble da = writeContinents(svg, world, MapsDaoUtils.RESOLUTION_LOW);
        writerDao.updateWorldBoundingBox(world.getId(), da);
    }

    private BoundingBoxDouble writeContinents(SvgMap svg, WorldSvg world, int resolution) {
        long id = world.getId();
        BoundingBoxDouble bb = null;

        PATH[] paths = svg.getPaths();
        for (PATH path : paths) {
            if(bb==null)bb=path.getBoundingBox();
            else bb = bb.combine(path.getBoundingBox());

            writerDao.saveCountry(id, path, resolution);
        }

        return bb;
    }

    private SvgMap writeWorld(String name, String svgFile) {
        SvgMap svg = SvgMap.fromFile(new File(fileRoot, svgFile));
        AMAP amap = svg.getAmap();
        writerDao.saveWorld(name, amap);
        return svg;
    }

}
