package iv3d.apps.domain.worldmap.dao.writers;

import com.iv3d.common.core.BoundingBoxDouble;
import com.iv3d.common.utils.FileUtils;
import com.iv3d.common.utils.JsonUtils;
import iv3d.apps.domain.worldmap.dao.MapsSvgReaderDao;
import iv3d.apps.domain.worldmap.dao.MapsSvgWriterDao;
import iv3d.apps.domain.worldmap.domain.CountrySvg;
import iv3d.apps.domain.worldmap.svgmap.AMAP;
import iv3d.apps.domain.worldmap.svgmap.PATH;
import iv3d.apps.domain.worldmap.svgmap.SvgMap;

import java.io.File;

/**
 * Created by fred on 2016/08/03.
 */
public class CountryDataWriter {
    private final MapsSvgReaderDao readerDao;
    private final MapsSvgWriterDao writerDao;
    private final String fileRoot;

    public CountryDataWriter(MapsSvgReaderDao readerDao, MapsSvgWriterDao writerDao, String fileRoot) {
        this.readerDao = readerDao;
        this.writerDao = writerDao;
        this.fileRoot = new File(fileRoot, "js").getPath();
    }

    public void run() {
        saveCountryRegions();
    }

    private void saveCountryRegions() {
        File root = new File(fileRoot);
        File[] listFiles = root.listFiles();

        for (File file : listFiles) {
            if(file.isFile() && !exclude(file))
                saveFileAsCountry(file);
        }
    }

    private boolean exclude(File file) {
        String[] excludes = new String[]{
                "worldHigh.js",
                "worldIndiaHigh.js",
                "worldIndiaLow.js",
                "worldKashmir2Low.js",
                "worldKashmir2WithAntarcticaLow.js",
                "worldLow.js",
                "worldRussiaSplitHigh.js",
                "worldRussiaSplitLow.js",
                "worldRussiaSplitWithAntarcticaHigh.js",
                "worldRussiaSplitWithAntarcticaLow.js",
                "usa2High.js",
                "usa2Low.js",
                "usaHigh.js",
                "usaLow.js",
                "usaTerritories2High.js",
                "usaTerritories2Low.js",
                "usaTerritoriesHigh.js",
                "usaTerritoriesLow.js",
                "continentsHigh.js",
                "continentsLow.js",
                "continentsWithAntarcticaHigh.js",
                "continentsWithAntarcticaLow.js",
                "cyprusNorthernCyprusHigh.js",
                "cyprusNorthernCyprusLow.js",
                "dominicanRepublicHigh.js",
                "dominicanRepublicLow.js",
                "france2016High.js",
                "france2016Low.js",
                "franceDepartmentsHigh.js",
                "franceDepartmentsLow.js",
                "moroccoWesternSaharaHigh.js",
                "moroccoWesternSaharaLow.js",
                "portugalRegionsHigh.js",
                "portugalRegionsLow.js",
                "serbiaNoKosovoHigh.js",
                "serbiaNoKosovoLow.js",
                "spain2Low.js",
                "spain2High.js",
                "spainProvincesLow.js",
                "spainProvincesHigh.js"

        };

        String path = file.getPath();
        for (String p : excludes) {
            if(path.endsWith(p))
                return true;
        }
        return false;
    }

    private void saveFileAsCountry(File file) {
        String contentsAsString = FileUtils.contentsAsString(file);
        String name = getCountryNameFromJsContent(contentsAsString);

        SvgMap svg = getSvgFromString(file);
        writeToCountryStorage(name, svg);
    }

    private void writeToCountryStorage(String name, SvgMap svg) {
        String code = svg.getSvg().getCode();
        PATH[] paths = svg.getPaths();
        int resolution = svg.getResolution();
        AMAP amap = svg.getAmap();

        CountrySvg country = readerDao.getCountryByCode(code, resolution);

        System.out.println("Country name='"+name +"' code:"+ code + " res="+resolution);

        if(country==null) {
            writerDao.saveCountry(code, name, resolution, amap);
            country = readerDao.getCountryByCode(code, resolution);
        }

        if(country==null) {
            throw new RuntimeException("Unable to save country " + name);
        }


        BoundingBoxDouble bb = null;
        for (PATH path : paths) {
            if(bb==null)bb=path.getBoundingBox();
            else bb = bb.combine(path.getBoundingBox());

            long countryId = country.getId();
            writerDao.saveRegion(countryId, path);
        }

        writerDao.updateCountryBoundingBox(country.getId(), bb);
    }

    public SvgMap getSvgFromString(File file) {
        String contentsAsString = FileUtils.contentsAsString(file);
        int resolution = file.getPath().endsWith("High.js")?1:0;
        SvgMap svg = getSvgFromJson(contentsAsString);
        svg.setResolution(resolution);
        return svg;
    }

    private SvgMap getSvgFromJson(String jsonString) {
        String json = jsonString.split("=")[1].replace(" ", "").replace("\n", "").replace("\t", "");
        SvgMap svg = JsonUtils.readFromString(json, SvgMap.class);
        return svg;
    }

    private String getCountryNameFromJsContent(String contentsAsString) {
        int index = 0;
        String lineOne = contentsAsString.split("\n")[0];
        while(lineOne.isEmpty()) {
            index++;
            lineOne = contentsAsString.split("\n")[index];
        }
        return lineOne.split("map of ")[1].replace("-", "").replace("High", "").replace("Low", "").trim();
    }

}
