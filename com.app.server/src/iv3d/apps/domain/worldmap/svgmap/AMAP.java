/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.domain.worldmap.svgmap;

public  final class AMAP {
	public AMAP() {}
	private String projection;
	private String leftLongitude;
	private String topLatitude;
	private String rightLongitude;
	private String bottomLatitude;
	public String getProjection() {return projection;}
	public void setProjection(String projection) {this.projection = projection;}
	public String getLeftLongitude() {return leftLongitude;}
	public void setLeftLongitude(String leftLongitude) {this.leftLongitude = leftLongitude;}
	public String getTopLatitude() {return topLatitude;}
	public void setTopLatitude(String topLatitude) {this.topLatitude = topLatitude;}
	public String getRightLongitude() {return rightLongitude;}
	public void setRightLongitude(String rightLongitude) {this.rightLongitude = rightLongitude;}
	public String getBottomLatitude() {return bottomLatitude;}
	public void setBottomLatitude(String bottomLatitude) {this.bottomLatitude = bottomLatitude;}
}