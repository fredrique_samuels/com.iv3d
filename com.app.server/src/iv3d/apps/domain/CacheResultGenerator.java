package iv3d.apps.domain;

/**
 * Created by fred on 2016/10/20.
 */
public interface CacheResultGenerator<T> {
    T generate();
}
