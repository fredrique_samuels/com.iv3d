package iv3d.apps.domain.home.handlers;

import com.app.server.base.server.web.RequestHandler;
import com.app.server.base.server.web.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class IndexWidgetHandler extends RequestHandler {

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        returnMav(new ModelAndView("home/index.macro"));
    }

    @Override
    public String getMethod() {
        return GET;
    }
}