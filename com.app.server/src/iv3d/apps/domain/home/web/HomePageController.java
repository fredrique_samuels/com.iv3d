package iv3d.apps.domain.home.web;

import com.app.server.base.server.LocalAppSettingsManager;
import com.app.server.base.server.AppWebController;
import com.app.server.base.server.web.RequestHandler;
import iv3d.apps.domain.home.handlers.HomePageHandler;
import iv3d.apps.domain.home.handlers.IndexWidgetHandler;
import iv3d.apps.domain.news24.News24App;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HomePageController extends AppWebController {

    private IndexWidgetHandler indexHandler;
    @Inject
    private HomePageHandler homePageHandler;

    @Inject
    public HomePageController(LocalAppSettingsManager cache) {
        super(cache.get(News24App.APP_ID));
    }

    @Override
    public void configure() {
        set("/", homePageHandler);
    }

    private static class InlinedHomePageHandler extends RequestHandler {

        @Override
        protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {

            response.sendRedirect("index.html");
        }

        @Override
        public String getMethod() {
            return "GET";
        }
    }

    @Inject
    public void inject(IndexWidgetHandler handler) {this.indexHandler = handler;}
}
