package iv3d.apps.domain;

/**
 * Created by fred on 2016/10/18.
 */
public class NewsTag {
    private String id;
    private String name;
    private String link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
