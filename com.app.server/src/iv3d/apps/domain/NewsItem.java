package iv3d.apps.domain;

import com.app.server.base.WebMedia;
import com.iv3d.common.date.UtcDate;

import java.util.ArrayList;
import java.util.List;

public final class NewsItem {
    private String id;
    private String title;
    private UtcDate publicationDate;
    private String sourceLink;
    private String summary;
    private String content;
    private String dataSource;
    private List<WebMedia> media = new ArrayList<>();
    private List<NewsTag> tags = new ArrayList<>();
    private List<Location> locations = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UtcDate getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(UtcDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getSourceLink() {
        return sourceLink;
    }

    public void setSourceLink(String sourceLink) {
        this.sourceLink = sourceLink;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<WebMedia> getMedia() {
        return media;
    }

    public void setMedia(List<WebMedia> media) {
        this.media = media;
    }

    public List<NewsTag> getTags() {
        return tags;
    }

    public void setTags(List<NewsTag> tags) {
        this.tags = tags;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
