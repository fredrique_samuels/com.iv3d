package iv3d.apps.server;

import com.app.server.base.server.AppServerLauncher;

public class AppServerMain {
	public static void main(String[] args) throws Exception {
		new AppServerLauncher().run();
	}
}
