CREATE table iv3d_news24_article (
		   id  	INTEGER PRIMARY KEY,
		   title STRING,
		   link STRING,
		   summary STRING,
		   publication_date TIMESTAMP,
       location STRING,
       content STRING,
       html_page STRING,
		   doe TIMESTAMP
);

CREATE table iv3d_news24_tag (
		   id  	INTEGER PRIMARY KEY,
		   name STRING,
		   link STRING
);

CREATE table iv3d_news24_article_tag (
		   tag_id INTEGER,
		   article_id INTEGER
);

CREATE table iv3d_news24_media (
		   id  	INTEGER PRIMARY KEY,
		   link STRING,
		   summary STRING,
		   mimetype STRING
);

CREATE table iv3d_news24_article_media (
		   media_id INTEGER,
		   article_id INTEGER
);