fh = open("worldcitiespop.txt")
raw = fh.read()
fh.close()


rawLines = raw.split("\n")

heading = rawLines[0]
print heading
print len(rawLines)


currentCode = None
currentEntryCache = []

loopArray = rawLines[1:]

def saveFile():
	if currentCode and len(currentEntryCache):
		print currentCode, len(currentEntryCache)
		fh = open("cities/" + currentCode+".txt", "w")
		fh.write(heading)
		fh.write("\n")
		fh.write("\n".join(currentEntryCache))

for entry in loopArray:
	code = entry.split(',')[0]
	if code != currentCode:
		saveFile()
		currentCode = code
		currentEntryCache = []
	currentEntryCache.append(entry)
