CREATE table iv3d_mapssvg_world (
		   id  	INTEGER PRIMARY KEY,
		   field STRING,
		   left_longitude REAL,
		   top_latitude REAL,
		   right_longitude REAL,
		   bottom_latitude REAL,
		   boundingbox_json STRING);

CREATE table iv3d_mapssvg_country (
		   id  	INTEGER PRIMARY KEY,
		   world_id INT,
		   resolution INT,
		   code STRING,
       field STRING,
       svg_path STRING,
		   left_longitude REAL,
		   top_latitude REAL,
		   right_longitude REAL,
		   bottom_latitude REAL,
		   boundingbox_json STRING);
		   

CREATE table iv3d_mapssvg_region (
		   id  	INTEGER PRIMARY KEY,
		   country_id INT,
		   code STRING,
       field STRING,
       svg_path STRING,
		   boundingbox_json STRING);

CREATE table iv3d_mapssvg_city (
		   id  	INTEGER PRIMARY KEY,
		   country_code STRING,
		   field STRING,
		   accent_name STRING,
		   region STRING,
		   population INT,
		   latitude REAL,
		   longitude REAL,
		   latlng STRING);
