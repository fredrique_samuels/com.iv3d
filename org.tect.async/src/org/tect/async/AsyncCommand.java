package org.tect.async;

import com.iv3d.common.core.Percentage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by fred on 2017/10/07.
 */
public abstract class AsyncCommand<T> {
    public abstract AsyncMonitor<T> execute();

    public static <T> AsyncCommand<T> createForRunner(AsyncRunnable<T> asyncRunnable) {
        return new AsyncCommandImpl<T>(asyncRunnable);
    }

    private static class AsyncCommandImpl<T> extends AsyncCommand<T> {

        private final List<AsyncUpdateHandler<T>> updateHandlers = new ArrayList<>();
        private final AsyncRunnable<T> asyncRunnable;
        private AsyncUpdate lastUpdate;

        public AsyncCommandImpl(AsyncRunnable<T> runnable) {
            super();
            this.asyncRunnable = runnable;
        }

        void dispatchUpdate(AsyncUpdate update) {
            synchronized (updateHandlers) {
                dispatchUpdate(updateHandlers, update);

                if(lastUpdate!=null && lastUpdate.isDone()) {
                    return;
                }

                lastUpdate = update;
            }

        }

        private void dispatchUpdate(List<AsyncUpdateHandler<T>> updateHandlers, AsyncUpdate<T> update) {


            if(update.isDone()) {
                updateHandlers.stream().forEach(uh -> {
                    try {
                        uh.onDone(update.getData());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            } else {
                updateHandlers.stream().forEach(uh -> {
                    try {
                        uh.onUpdate(update.getMessage(), update.getPercentage(), update.getData());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }

        @Override
        public AsyncMonitor<T> execute() {
            Updater<T> updater = new Updater<>(this);
            Monitor<T> monitor = new Monitor<>(this);
            try {
                asyncRunnable.run(updater);
            } catch (Exception e) {
                updater.newUpdate().setDone().execute();
            }
            return monitor;
        }

        public void withUpdateHandler(AsyncUpdateHandler<T> handler) {
            synchronized (updateHandlers) {
                if(lastUpdate!=null) {
                    dispatchUpdate(Arrays.asList(handler), lastUpdate);
                }
                updateHandlers.add(handler);
            }
        }

        public void terminate() {
            try {
                asyncRunnable.terminate();
            }catch (Exception e) {

            }
        }
    }

    private static class Monitor<T> extends AsyncMonitor<T> {

        private AsyncCommandImpl<T> asyncCommand;

        public Monitor(AsyncCommandImpl<T> asyncCommand) {
            this.asyncCommand = asyncCommand;
        }

        @Override
        public void terminate() {
            this.asyncCommand.terminate();
        }

        @Override
        public AsyncMonitor<T> withUpdateHandler(AsyncUpdateHandler<T> handler) {
            asyncCommand.withUpdateHandler(handler);
            return this;
        }

        @Override
        public T join() {
            try {
                final Semaphore semaphore = new Semaphore(1);
                semaphore.acquire();
                class FinalResult {
                    T value;
                }
                final FinalResult  finalResult = new FinalResult();
                asyncCommand.withUpdateHandler(new AsyncUpdateHandler<T>() {
                    @Override
                    public void onUpdate(String message, Percentage percentage, T data) {

                    }

                    @Override
                    public void onDone(T result) {
                        semaphore.release();
                        finalResult.value = result;
                    }
                });
                semaphore.acquire();
                return finalResult.value;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static class Updater<T> implements AsyncUpdater {

        private AsyncCommandImpl<T> asyncCommand;

        public Updater(AsyncCommandImpl<T> asyncCommand) {
            this.asyncCommand = asyncCommand;
        }

        @Override
        public AsyncUpdateBuilder<T> newUpdate() {
            return new UpdateBuilder<T>(asyncCommand);
        }
    }

    private static class UpdateBuilder<T> implements AsyncUpdateBuilder<T> {
        private AsyncCommandImpl<T> asyncCommand;
        private Boolean hasFired = false;
        private Percentage percentage = new Percentage();
        private String message = "";
        private boolean isDone = false;
        private T data;

        public UpdateBuilder(AsyncCommandImpl<T> asyncCommand) {
            this.asyncCommand = asyncCommand;
        }

        @Override
        public synchronized AsyncUpdateBuilder setMessage(String message) {
            synchronized (hasFired) {
                if (hasFired) {
                    return this;
                }
            }
            this.message = message;
            return this;
        }

        @Override
        public AsyncUpdateBuilder setPercentage(Percentage percentage) {
            synchronized (hasFired) {
                if (hasFired) {
                    return this;
                }
            }
            this.percentage = percentage;
            return this;
        }

        @Override
        public AsyncUpdateBuilder setData(T data) {
            synchronized (hasFired) {
                if (hasFired) {
                    return this;
                }
            }
            this.data = data;
            return this;
        }

        @Override
        public synchronized AsyncUpdateBuilder setDone() {
            synchronized (hasFired) {
                if (hasFired) {
                    return this;
                }
            }
            this.isDone = true;
            return this;
        }

        @Override
        public synchronized void execute() {
            synchronized (hasFired) {
                if(hasFired) {
                    return;
                }
                hasFired = true;
            }

            this.asyncCommand.dispatchUpdate(new Update(isDone, message, percentage, data));
            this.asyncCommand = null;
        }
    }

    private static class Update implements AsyncUpdate {

        private final boolean isDone;
        private final String message;
        private final Percentage percentage;
        private final Object data;

        public Update(boolean isDone, String message, Percentage percentage, Object data) {
            this.isDone = isDone;
            this.message = message;
            this.percentage = percentage;
            this.data = data;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public Percentage getPercentage() {
            return percentage;
        }

        @Override
        public boolean isDone() {
            return isDone;
        }

        @Override
        public Object getData() {
            return data;
        }
    }
}
