package org.tect.async;

import com.iv3d.common.core.Percentage;

public interface AsyncUpdateHandler<T> {
    void onUpdate(String message, Percentage percentage, T data);
    void onDone(T result);
}