package org.tect.async;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by fred on 2017/10/09.
 */
public final class DefaultAsyncMonitor<T> extends AsyncMonitor<T> {

    private Semaphore semaphore = new Semaphore(1);
    private Boolean done = false;
    private T result = null;
    private List<AsyncUpdateHandler> handlerList = new ArrayList<>();

    public DefaultAsyncMonitor() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public final DefaultAsyncMonitor<T> release() {
        return this.release(null);
    }

    public final DefaultAsyncMonitor<T> release(T result) {
        semaphore.release();
        synchronized (done) {
            if(done) {
                return this;
            }
            this.result = result;
            done = true;
            for(AsyncUpdateHandler h : handlerList) {
                try {
                    h.onDone(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            handlerList.clear();
        }
        return this;
    }

    @Override
    public final void terminate() {

    }

    @Override
    public final AsyncMonitor<T> withUpdateHandler(AsyncUpdateHandler handler) {
        synchronized (done) {
            if(done) {
                try {
                    handler.onDone(this.result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                handlerList.add(handler);
            }
        }
        return this;
    }

    @Override
    public final synchronized T join() {
        try {
            semaphore.acquire();
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.result;
    }
}
