package org.tect.async;

import com.iv3d.common.core.Percentage;

public interface AsyncUpdateBuilder<T> {
    AsyncUpdateBuilder setMessage(String message);
    AsyncUpdateBuilder setPercentage(Percentage percentage);
    AsyncUpdateBuilder setData(T data);
    AsyncUpdateBuilder setDone();
    void execute();
}