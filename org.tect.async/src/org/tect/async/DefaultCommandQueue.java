package org.tect.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by fred on 2017/10/07.
 */
public class DefaultCommandQueue implements CommandQueue {
    private ExecutorService executorService;

    public final <T> AsyncMonitor<T> execute(final AsyncRunnable<T> asyncRunnable) {
        AsyncRunnable<T> runnable = new AsyncRunnable<T>() {
            @Override
            public void run(AsyncUpdater<T> updater) {
                executeTask(() -> {
                    try {
                        asyncRunnable.run(updater);
                    } catch (Exception e) {
                        e.printStackTrace();
                        updater.newUpdate().setDone().execute();
                    }
                });
            }

            @Override
            public void terminate() {
                asyncRunnable.terminate();
            }
        };

        return AsyncCommand.createForRunner(runnable).execute();
    }



    private void executeTask(Runnable task) {
        getExecutor().submit(task);
    }

    private ExecutorService getExecutor() {
        if(executorService==null) {
            executorService = Executors.newFixedThreadPool(getThreadPoolSize());
        }

        return executorService;
    }

    protected int getThreadPoolSize() {
        return 20;
    }
}
