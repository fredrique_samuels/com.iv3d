package org.tect.async;

/**
 * Created by fred on 2017/10/07.
 */
public abstract class AsyncMonitor<T> {
    public abstract void terminate();
    public abstract AsyncMonitor<T> withUpdateHandler(AsyncUpdateHandler<T> handler);
    public abstract T join();

    public static AsyncMonitor done() {
        return new DefaultAsyncMonitor().release();
    }

    public static <T> AsyncMonitor<T> done(T o) {
        return new DefaultAsyncMonitor<T>().release(o);
    }
}
