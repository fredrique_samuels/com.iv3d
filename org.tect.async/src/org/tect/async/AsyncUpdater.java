package org.tect.async;

/**
 * Created by fred on 2017/10/07.
 */
public interface AsyncUpdater<T> {
    AsyncUpdateBuilder<T> newUpdate();
}
