package org.tect.async;

public interface AsyncRunnable<T> {
    void run(AsyncUpdater<T> updater);
    void terminate();
}
