package org.tect.async;

import com.iv3d.common.core.Percentage;

/**
 * Created by fred on 2017/10/07.
 */
public interface AsyncUpdate<T> {
    String getMessage();
    Percentage getPercentage();
    boolean isDone();
    T getData();
}
