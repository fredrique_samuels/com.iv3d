import org.junit.Assert;
import org.junit.Test;

/**
 * Created by fred on 2017/07/28.
 */

public class TestMessage {

    @Test
    public void test_Message() {
        Assert.assertEquals("e", "f");
    }

    @Test
    public void test_Message2() {
        Assert.assertEquals("e", "e");
    }
}
