package com.j9.module.base;

public class ModuleBaseUtils {
    public static String calculateSum(int a, int b) {
        return String.format(" The sum of %d + %d = %d", a, b, a + b);
    }
}
