import com.j9.module.base.ModuleBaseUtils;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by fred on 2017/10/23.
 */
public class TestModuleBaseUtils extends TestCase {

    @Test
    public void testSum() {
        String s = ModuleBaseUtils.calculateSum(1, 2);
        assertEquals("", s);
    }
}
