package iv3d.apps;

import iv3d.apps.view.DesktopViewLauncher;
import javafx.application.Application;
import javafx.stage.Stage;

public final class DesktopViewMain extends Application {
	
	public static void main(String[] args) {
		Application.launch(DesktopViewMain.class, args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		new DesktopViewLauncher().run();
	}
}
