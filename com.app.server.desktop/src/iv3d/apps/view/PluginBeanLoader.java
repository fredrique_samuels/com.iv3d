package iv3d.apps.view;

import iv3d.apps.view.plugin.PluginBean;

/**
 * Created by fred on 2016/07/17.
 */
public interface PluginBeanLoader {
    <T> T load(PluginBean pluginBean, Class<T> type);
}
