package iv3d.apps.view;

import com.iv3d.common.mvc.control.config.ViewConfig;
import iv3d.apps.view.plugin.PluginViewOld;

import java.util.List;
import java.util.Map;

public interface PluginViewLoader {
    List<PluginViewOld> load(ViewConfig viewConfig, Map<String, String> params);
}
