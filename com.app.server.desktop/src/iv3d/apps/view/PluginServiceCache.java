package iv3d.apps.view;

import iv3d.apps.view.plugin.PluginServiceOld;

public interface PluginServiceCache {
    PluginServiceOld getService(String pluginId);
}
