package iv3d.apps.view.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import iv3d.apps.view.PluginCache;
import iv3d.apps.view.PluginManager;
import iv3d.apps.view.PluginServiceCache;
import iv3d.apps.view.plugin.Plugin;
import iv3d.apps.view.plugin.PluginServiceOld;

@Singleton
public class PluginManagerImpl implements PluginManager {

    private final PluginCache pluginCache;
    private final PluginServiceCache pluginServiceCache;

    @Inject
    public PluginManagerImpl(PluginCache pluginCache, PluginServiceCache pluginServiceCache) {
        this.pluginCache = pluginCache;
        this.pluginServiceCache = pluginServiceCache;
    }

    public Plugin getPlugin(String pluginId) {
        return pluginCache.getPlugin(pluginId);
    }
    public PluginServiceOld getPluginService(String pluginId) {
        return pluginServiceCache.getService(pluginId);
    }

}
