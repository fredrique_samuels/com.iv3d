package iv3d.apps.view.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.iv3d.common.utils.ExceptionUtils;
import iv3d.apps.view.*;
import iv3d.apps.view.plugin.Plugin;
import iv3d.apps.view.plugin.PluginServiceOld;
import iv3d.apps.view.plugin.PluginServiceSettings;
import iv3d.apps.view.plugin.bizimpl.AbstractPluginService;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

@Singleton
public class PluginServiceCacheImpl implements PluginServiceCache {
    private final Logger logger = org.apache.log4j.Logger.getLogger(PluginServiceCacheImpl.class);

    private final PluginCache pluginCache;
    private final PluginPathManager pluginPathManager;
    private final PluginBeanLoader pluginBeanLoader;
    private final Provider<PluginServiceManager> appServiceProvider;

    private final Map<String, PluginServiceOld> services;

    @Inject
    public PluginServiceCacheImpl(PluginCache pluginCache,
                                  PluginPathManager pluginPathManager,
                                  PluginBeanLoader pluginBeanLoader,
                                  Provider<PluginServiceManager> appServiceProvider) {
        this.pluginCache = pluginCache;
        this.pluginPathManager = pluginPathManager;
        this.pluginBeanLoader = pluginBeanLoader;
        this.appServiceProvider = appServiceProvider;

        this.services = load();
    }

    @Override
    public PluginServiceOld getService(String pluginId) {
        return services.get(pluginId);
    }

    private Map<String, PluginServiceOld> load() {

        Map<String, PluginServiceOld> services = new HashMap<>();
        for(Plugin plugin : pluginCache) {
            try {
                PluginServiceSettings serviceSettings = plugin.getServiceSettings();
                if (serviceSettings == null) continue;
                loadPlugin(services, plugin, serviceSettings);
            }catch (Exception e) {
                logger.error(ExceptionUtils.getStackTrace(e));
            }
        }

        return services;
    }

    private void loadPlugin(Map<String, PluginServiceOld> services, Plugin plugin, PluginServiceSettings serviceSettings) {
        logger.info("Loading service for plugin " + plugin.getId());
        AbstractPluginService service = pluginBeanLoader.load(serviceSettings.getBean(), AbstractPluginService.class);
        service.setAppServiceProvider(appServiceProvider);
        service.setPlugin(plugin);
        services.put(plugin.getId(), service);
    }

}
