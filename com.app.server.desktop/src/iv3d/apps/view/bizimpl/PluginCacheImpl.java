package iv3d.apps.view.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.utils.JsonUtils;
import com.iv3d.common.utils.XmlUtils;
import iv3d.apps.view.PluginCache;
import iv3d.apps.view.PluginPathManager;
import iv3d.apps.view.plugin.Plugin;
import iv3d.apps.view.plugin.PluginServiceSettings;
import iv3d.apps.view.plugin.PluginConfig;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Singleton
public class PluginCacheImpl implements PluginCache {
    private static Logger logger = Logger.getLogger(PluginCacheImpl.class);
    private final Map<String, Plugin> plugins;

    @Inject
    public PluginCacheImpl(PluginPathManager pluginPathManager) {
        this.plugins = loadPlugins(pluginPathManager);
    }

    private final Map<String, Plugin> loadPlugins(PluginPathManager pluginPathManager) {
        List<File> allPluginFiles = pluginPathManager.getAllPluginFiles();

        Map<String, Plugin> plugins = new HashMap<>();
        for (File pluginFile : allPluginFiles) {
            Plugin p = loadPluginFromFile(pluginPathManager, pluginFile);
            plugins.put(p.getId(), p);
        }

        return plugins;
    }

    private Plugin loadPluginFromFile(PluginPathManager pluginPathManager, File pluginFile) {
        logger.info("Loading plugin " + pluginFile.getPath());
        PluginConfig ps;
        try {
            ps = XmlUtils.read(pluginFile, PluginConfig.class);
        } catch ( Exception e) {
            try {
                ps = JsonUtils.readFromFile(pluginFile, PluginConfig.class);
            } catch (Exception ex2) {
                throw new RuntimeException(ex2);
            }
        }
        return new PluginImpl(ps.getId(), pluginPathManager, ps);
    }

    @Override
    public Plugin getPlugin(String pluginId) {
        return plugins.get(pluginId);
    }

    @Override
    public Iterator<Plugin> iterator() {
        return plugins.values().iterator();
    }


    private static class PluginImpl implements Plugin {
        private final String id;
        private final PluginPathManager pluginPathManager;
        private final PluginConfig ps;

        public PluginImpl(String id, PluginPathManager pluginPathManager, PluginConfig ps) {
            this.id = id;
            this.pluginPathManager = pluginPathManager;
            this.ps = ps;
        }

        public String getId() {return id;}
        public File getResource(String file) {
            return pluginPathManager.getResource(id, file);
        }
        public PluginConfig getConfig() {
            return ps;
        }
        public PluginServiceSettings getServiceSettings() {
            return ps==null?null:ps.getService();
        }
    }

}
