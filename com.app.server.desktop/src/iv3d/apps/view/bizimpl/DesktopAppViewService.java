/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.view.bizimpl;

import com.app.server.base.server.web.AppFlow;
import com.app.server.base.server.web.ApplicationDataParam;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.mvc.control.FlowHints;
import com.iv3d.common.mvc.control.config.FlowConfig;
import com.iv3d.common.mvc.control.config.FlowConfigBuilder;
import com.iv3d.common.mvc.control.config.ViewConfig;
import com.iv3d.common.mvc.control.config.ViewConfigBuilder;
import com.iv3d.common.rest.RestClient;
import com.iv3d.common.rest.RestRequest;
import com.iv3d.common.utils.JsonUtils;
import iv3d.apps.view.PluginManager;
import iv3d.apps.view.PluginServiceManager;
import iv3d.apps.view.PluginViewLoader;
import iv3d.apps.view.plugin.PluginServiceOld;
import iv3d.apps.view.plugin.PluginViewOld;
import iv3d.apps.view.plugin.PluginViewLifeCycle;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Singleton
public final class DesktopAppViewService implements PluginServiceManager {

	private final PluginManager pluginManager;
    private PluginViewLoader viewLoader;
    private RestClient restClient;
    Logger logger = Logger.getLogger(DesktopAppViewService.class);
    private Environment environment = Environment.DEV;

    @Inject
	public DesktopAppViewService(PluginManager pluginManager,
                                 PluginViewLoader viewLoader,
								 RestClient restClient) {
		this.pluginManager = pluginManager;
        this.viewLoader = viewLoader;
        this.restClient = restClient;
    }

    @Override
    public String getAppHost(String packageId) {
        return "http://localhost:58080";
    }

    @Override
    public ApplicationDataParam executeServiceCommand(String pluginId, String command, Map<String, String> params) {
        PluginServiceOld pluginServiceOld = pluginManager.getPluginService(pluginId);
        if(pluginServiceOld !=null)
            return pluginServiceOld.executeCommand(command, params);
        return new ApplicationDataParam();
    }

    @Override
    public List<PluginViewOld> getViews(RestRequest request, Map<String, String> params) {
        ResponseEntity<String> response = restClient.execute(request, String.class);
        ApplicationDataParam gr = JsonUtils.readFromString(response.getBody(), ApplicationDataParam.class);
        ViewConfig viewConfig = gr.getView();
        return viewLoader.load(viewConfig, params);
    }

    @Override
    public void showView(PluginViewLifeCycle viewLifeCycle, Map<String, String> params) {
        Parent parent = viewLifeCycle.getParent();
        FlowConfig config = viewLifeCycle.getFlowConfig();

        Stage stage = new Stage();
        stage.setTitle(config.getName());
        stage.setScene(new Scene(parent, 600, 275));
        stage.show();
        stage.setOnCloseRequest((e)->viewLifeCycle.dispose());
    }

    @Override
    public void showView(PluginViewOld view) {
        showView(PluginViewLifeCycle.create(view), Collections.EMPTY_MAP);
    }

    @Override
    public void showView(PluginViewOld view, Map<String, String> params) {
        showView(PluginViewLifeCycle.create(view), params);
    }

    @Override
    public PluginServiceOld getPluginService(String id) {
        return pluginManager.getPluginService(id);
    }

    @Override
    public void logPluginServiceNoAvailableWarning(String id, String serviceId) {
        String msg = String.format("Plugin %s could not locate dependency %s", id, serviceId);
        logger.warn(msg);
    }

    @Override
    public boolean isProduction() {
        return environment==Environment.PRODUCTION;
    }

    @Override
    public boolean isDev() {
        return environment==Environment.DEV;
    }

    @Override
    public List<PluginViewOld> getViews(String id, Map<String, String> params) {
        ViewConfigBuilder view = new ViewConfigBuilder();
        FlowConfigBuilder flow = view.createFlow();
        flow.setHint(FlowHints.JAVAFX);
        flow.addUserProperty(AppFlow.JFX_VIEW_NAME, id);

        return viewLoader.load(view.build(), params);
    }

}
