package iv3d.apps.view.bizimpl;


import com.app.server.base.server.web.AppFlow;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.mvc.control.FlowHints;
import com.iv3d.common.mvc.control.config.FlowConfig;
import com.iv3d.common.mvc.control.config.ViewConfig;
import com.jfx.profile.JavaFxUiProfile;
import com.jfx.profile.JavaFxUiProfileBuilder;
import iv3d.apps.view.PluginBeanLoader;
import iv3d.apps.view.PluginCache;
import iv3d.apps.view.PluginPathManager;
import iv3d.apps.view.PluginViewLoader;
import iv3d.apps.view.domain.ExecutorServiceProvider;
import iv3d.apps.view.jfx.AbstractJfxController;
import iv3d.apps.view.jfx.FxmlView;
import iv3d.apps.view.plugin.Plugin;
import iv3d.apps.view.plugin.PluginBean;
import iv3d.apps.view.plugin.PluginConfig;
import iv3d.apps.view.plugin.PluginViewOld;
import javafx.scene.Parent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Singleton
public class PluginViewLoaderImpl implements PluginViewLoader {

    private final PluginPathManager pluginPathManager;
    private final PluginBeanLoader pluginBeanLoader;
    private final PluginCache pluginCache;
    private final ExecutorServiceProvider executorServiceProvider;

    @Inject
    public PluginViewLoaderImpl(PluginPathManager pluginPathManager,
                                PluginBeanLoader pluginBeanLoader,
                                PluginCache pluginCache,
                                ExecutorServiceProvider executorServiceProvider) {
        this.pluginPathManager = pluginPathManager;
        this.pluginBeanLoader = pluginBeanLoader;
        this.pluginCache = pluginCache;
        this.executorServiceProvider = executorServiceProvider;
    }

    @Override
    public List<PluginViewOld> load(ViewConfig viewConfig, Map<String, String> params) {
        if(viewConfig==null)
            return Collections.emptyList();
        List<FlowConfig> flows = viewConfig.getFlows();

        List<PluginViewOld> widgets = new ArrayList<>();
        for(FlowConfig f:flows) {
            PluginViewOld view = loadFlowView(f, params);
            if(view!=null)widgets.add(view);
        }

        return widgets;
    }

    private PluginViewOld loadFlowView(FlowConfig flow, Map<String, String> params) {
        String hint = flow.getHint();
        boolean isJavaFxFlow = FlowHints.JAVAFX.equals(hint);
        if(isJavaFxFlow) return loadFxView(flow, params);
        return null;
    }

    private PluginViewOld loadFxView(FlowConfig flow, Map<String, String> params) {
        Map<String, String> userProperties = flow.getUserProperties();
        String pluginId = userProperties.get(AppFlow.JFX_VIEW_NAME);
        Plugin plugin = pluginCache.getPlugin(pluginId);
        PluginConfig settings = plugin.getConfig();

        Parent fxParent = getFxParent(settings.getFxmlView(), params);
        return new PluginViewImpl(fxParent, flow);
    }

    private Parent getFxParent(FxmlView fxmlView, Map<String, String> params) {
        Parent parent;
        if((parent=getFxmlParent(fxmlView, params))!=null) {return parent;}
        return null;
    }

    private Parent getFxmlParent(FxmlView ps, Map<String, String> params) {
        String fxml = ps.getFxml();
        if(fxml==null) {
            return null;
        }
        JavaFxUiProfile fxProfile = createFxProfile(ps, params);
        return fxProfile.getParent();
    }

    private JavaFxUiProfile createFxProfile(FxmlView ps, Map<String, String> params) {
        File fxmlFile = pluginPathManager.getFxmlFile(ps.getFxml());
        PluginBean controller = ps.getController();

        AbstractJfxController javaFxController = loadPluginBean(controller, AbstractJfxController.class);

        if(javaFxController!=null) {
            javaFxController.setPluginPathManager(pluginPathManager);
        }
        
        try {
            String toExternalForm = fxmlFile.getPath();

            String jsFile = ps.getJsFile();
            return new JavaFxUiProfileBuilder(executorServiceProvider.get(), toExternalForm)
                    .setController(javaFxController)
                    .setJsFile(jsFile)
                    .build();
        } catch (Exception e) {
            throw new JfxViewCreationError(e);
        }

//        return new FxProfileBuilder(executorServiceProvider.get())
//                .setFxmlFile(fxmlFile.getAbsolutePath())
//                .setJsFile(controller!=null?controller.getBean():null)
//                .setTaskProvider(provider)
//                .setModelPropertyInitializer(modelInitializer)
//                .setPropertyChangeListener(null)
//                .setViewInitializer(viewInitializer)
//                .setParams(params)
//                .build();
    }

    private <T> T loadPluginBean(PluginBean pluginBean, Class<T> c) {
        return pluginBeanLoader.load(pluginBean, c);
    }


    private static class PluginViewImpl implements PluginViewOld {
        private final Parent node;
        private final FlowConfig config;

        private PluginViewImpl(Parent node, FlowConfig config) {
            this.node = node;
            this.config = config;
        }

        @Override
        public Parent getParent() {
            return node;
        }

        @Override
        public FlowConfig getConfig() {
            return config;
        }
    }

    private class JfxViewCreationError extends RuntimeException {
        public JfxViewCreationError(Exception e) {
            super(e);
        }
    }
}
