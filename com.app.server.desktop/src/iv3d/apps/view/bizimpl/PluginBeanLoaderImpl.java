package iv3d.apps.view.bizimpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.beans.BeanConfig;
import com.iv3d.common.beans.BeanLoader;
import iv3d.apps.view.PluginBeanLoader;
import iv3d.apps.view.PluginPathManager;
import iv3d.apps.view.plugin.PluginBean;
import iv3d.apps.view.plugin.bizimpl.PluginBeanToBeanConfTransformer;

@Singleton
public class PluginBeanLoaderImpl implements PluginBeanLoader {

    private final PluginPathManager pluginPathManager;
    private final BeanLoader beanLoader;

    @Inject
    public PluginBeanLoaderImpl(PluginPathManager pluginPathManager,
                                BeanLoader beanLoader) {
        this.pluginPathManager = pluginPathManager;
        this.beanLoader = beanLoader;
    }

    @Override
    public <T> T load(PluginBean pluginBean, Class<T> type) {
        String rootPath = pluginPathManager.getRootPath();
        PluginBeanToBeanConfTransformer transformer = new PluginBeanToBeanConfTransformer(rootPath);
        BeanConfig beanConfig = transformer.transform(pluginBean);
        return beanLoader.load(beanConfig, type);
    }
}
