package iv3d.apps.view.bizimpl;

import com.google.inject.Singleton;
import com.iv3d.common.jython.JythonUtils;
import iv3d.apps.view.PluginPathManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Singleton
public class PluginPathManagerImpl implements PluginPathManager {
    private static final String JYTHON_APP_MODULE = "./cache/modules/jython.app";
    private static final String PLUGIN_ROOT = "./cache/plugins";
    private static final String PLUGIN_EXT_XML = ".plugin.xml";
    private static final String PLUGIN_EXT_JSON = ".plugin.json";

    private final File root;

    public PluginPathManagerImpl() {
        JythonUtils.addLibPath(JYTHON_APP_MODULE);
        this.root = new File(PLUGIN_ROOT);
    }

    public final List<File> getAllPluginFiles() {
        File[] a = root.listFiles((f, name) -> name.endsWith(PLUGIN_EXT_XML));
        File[] b = root.listFiles((f, name) -> name.endsWith(PLUGIN_EXT_JSON));

        ArrayList<File> files = new ArrayList<>();
        files.addAll(Arrays.asList(a));
        files.addAll(Arrays.asList(b));
        return files;
    }

    @Override
    public String getResource(String file) {
        return new File(PLUGIN_ROOT, file).getPath();
    }

    public File getResource(String pluginId, String resource) {
        return new File(getFileInRoot(pluginId), resource);
    }

    @Override
    public String getRootPath() {
        return root.getAbsolutePath();
    }

    private File getFileInRoot(String p) {
        return new File(root, p);
    }

    public File getFxmlFile(String fxmlFile) {
        return getFileInRoot(fxmlFile);
    }

//    public File getPluginRoot() {
//        return new File(PLUGIN_ROOT);
//    }
//
//    public File getPluginConfig(String pluginId) {
//        return getFileInRoot(pluginId + PLUGIN_EXT_XML);
//    }
//


}
