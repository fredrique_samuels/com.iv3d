package iv3d.apps.view.jfx;

import com.google.inject.Provider;
import com.jfx.profile.JavaFxController;
import iv3d.apps.view.PluginPathManager;
import iv3d.apps.view.PluginServiceManager;
import iv3d.apps.view.plugin.PluginServiceOld;
import iv3d.apps.view.plugin.PluginViewOld;
import iv3d.apps.view.plugin.bizimpl.PluginController;

import java.util.List;
import java.util.Map;

/**
 * Created by fred on 2016/09/08.
 */
public class AbstractJfxController extends JavaFxController implements PluginController {

    private Provider<PluginServiceManager> appServiceProvider;
    private PluginPathManager pluginPathManager;

    @Override
    public void setPluginServiceManagerProvider(Provider<PluginServiceManager> appServiceProvider) {
        this.appServiceProvider = appServiceProvider;
    }

    public void setPluginPathManager(PluginPathManager pluginPathManager) {
        this.pluginPathManager = pluginPathManager;
    }

    @Override
    public PluginServiceOld getPluginService(String id) {
        return appServiceProvider.get().getPluginService(id);
    }

    @Override
    public final List<PluginViewOld> create(String pluginId, Map<String, String> params) {
        return appServiceProvider.get().getViews(pluginId, params);
    }

    public final boolean isProduction() {
        return appServiceProvider.get().isProduction();
    }

    public final boolean isDev() {
        return appServiceProvider.get().isDev();
    }

    public final String getResource(String file) {
        return pluginPathManager.getResource(file);
    }
}
