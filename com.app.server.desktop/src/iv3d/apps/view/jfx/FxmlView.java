package iv3d.apps.view.jfx;

import iv3d.apps.view.plugin.PluginBean;

/**
 * Created by fred on 2016/09/08.
 */
public class FxmlView {
    private String fxml;
    private String jsFile;
    private PluginBean controller;

    public String getFxml() {
        return fxml;
    }

    public void setFxml(String fxml) {
        this.fxml = fxml;
    }

    public String getJsFile() {
        return jsFile;
    }

    public void setJsFile(String jsFile) {
        this.jsFile = jsFile;
    }

    public PluginBean getController() {return controller;}
    public void setController(PluginBean controllerSourceParam) {this.controller = controllerSourceParam;}
}
