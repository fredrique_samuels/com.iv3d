/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.view.jfx;

import com.jfx.control.ViewInitializer;
import javafx.collections.ObservableMap;
import javafx.scene.Parent;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractViewInitializer implements ViewInitializer {

	private Parent rootNode;
	private ObservableMap<String, Object> nodeLookup;
	private Map<String, String> parameters;
	
	@Override
	public void setup(Parent root, ObservableMap<String, Object> namespace, Map<String, String> parameters) {
		this.rootNode = root;
		this.nodeLookup = namespace;
		this.parameters = Collections.unmodifiableMap(parameters);
		run();
	}
	
	public final Parent getRootNode() {return rootNode;}
	public final ObservableMap<String, Object> getNodeLookup() {return nodeLookup;}
	public final Map<String, String> getParameters() {return parameters;}
	public final Object getNode(String id){return nodeLookup.get(id);}
	public final String getParameter(String key){return parameters==null?null:parameters.get(key);};

	public abstract void run();

}
