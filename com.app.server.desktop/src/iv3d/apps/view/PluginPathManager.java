package iv3d.apps.view;

import java.io.File;
import java.util.List;

public interface PluginPathManager {
    String getRootPath();
    File getResource(String pluginId, String file);
    File getFxmlFile(String fxml);
    List<File> getAllPluginFiles();
    String getResource(String file);
}
