package iv3d.apps.view.domain;

import javafx.beans.InvalidationListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Created by fred on 2016/07/15.
 */
public class EmptyObservableMap implements ObservableMap<String, Object> {
    @Override
    public void addListener(MapChangeListener<? super String, ? super Object> listener) {

    }

    @Override
    public void removeListener(MapChangeListener<? super String, ? super Object> listener) {

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public Object get(Object key) {
        return null;
    }

    @Override
    public Object put(String key, Object value) {
        return null;
    }

    @Override
    public Object remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<String> keySet() {
        return Collections.EMPTY_SET;
    }

    @Override
    public Collection<Object> values() {
        return Collections.EMPTY_SET;
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return Collections.EMPTY_SET;
    }

    @Override
    public void addListener(InvalidationListener listener) {

    }

    @Override
    public void removeListener(InvalidationListener listener) {

    }
}
