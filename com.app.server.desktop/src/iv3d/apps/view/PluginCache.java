package iv3d.apps.view;

import iv3d.apps.view.plugin.Plugin;

/**
 * Created by fred on 2016/07/16.
 */
public interface PluginCache extends Iterable<Plugin>{
    Plugin getPlugin(String pluginId);
}
