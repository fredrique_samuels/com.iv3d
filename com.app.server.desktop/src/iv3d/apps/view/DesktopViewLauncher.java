/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.view;

import com.app.server.base.common.inject.CommonInjectorModule;
import com.app.server.base.common.inject.InjectorProvider;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.iv3d.common.rest.RestRequest;
import iv3d.apps.view.bizimpl.DesktopAppViewService;
import iv3d.apps.view.inject.ViewInjectorModule;
import iv3d.apps.view.plugin.PluginViewOld;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

public final class DesktopViewLauncher {

	public final void run() {
		Injector injector = setupInjections();
		PluginServiceManager appService = injector.getInstance(DesktopAppViewService.class);
//        testauthDialog(appService);
		appService.executeServiceCommand("iv3d.dashboard.service", "main", Collections.emptyMap());
	}

    private void testauthDialog(PluginServiceManager appService) {
        RestRequest request = new RestRequest() {
            @Override
            public URI getUrl() {
                try {
                    return new URI("http://localhost:58080/iv3d.authentication/view/login");
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public HttpMethod getMethod() {
                return HttpMethod.GET;
            }

            @Override
            public HttpHeaders getHeaders() {
                return new HttpHeaders();
            }

            @Override
            public String getPayload() {
                return null;
            }
        };

        List<PluginViewOld> views = appService.getViews(request, Collections.emptyMap());
        for(PluginViewOld v: views ) {
            appService.showView(v);
        }
        appService.executeServiceCommand("iv3d.authentication.dialog", "main", Collections.emptyMap());
    }

    private Injector setupInjections() {
		InjectorProvider provider = new InjectorProvider(); 
		Injector injector = Guice.createInjector(new CommonInjectorModule(),
				new ViewInjectorModule(provider));
		provider.setInjector(injector);
		return injector;
	}
	
}
