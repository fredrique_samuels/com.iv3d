/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.view.plugin;

import iv3d.apps.view.jfx.FxmlView;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="plugin")
@XmlAccessorType(XmlAccessType.FIELD)
public class PluginConfig {
	private String id;
	private PluginServiceSettings service;
    private FxmlView fxmlView;
	
	public String getId() {return id;}
	public void setId(String id) {this.id = id;}

    public FxmlView getFxmlView() {
        return fxmlView;
    }

    public void setFxmlView(FxmlView fxmlView) {
        this.fxmlView = fxmlView;
    }

    public PluginServiceSettings getService() {return service;}
	public void setService(PluginServiceSettings service) {this.service = service;}
}
