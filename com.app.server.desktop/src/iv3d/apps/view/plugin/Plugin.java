package iv3d.apps.view.plugin;

import java.io.File;

public interface Plugin {
    String getId();
    File getResource(String file);
    PluginConfig getConfig();
    PluginServiceSettings getServiceSettings();
}
