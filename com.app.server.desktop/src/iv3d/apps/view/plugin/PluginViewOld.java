package iv3d.apps.view.plugin;

import com.iv3d.common.mvc.control.config.DefaultFlowConfigBuilder;
import com.iv3d.common.mvc.control.config.FlowConfig;
import javafx.scene.Parent;

public interface PluginViewOld {
    Parent getParent();
    FlowConfig getConfig();


    public static PluginViewOld createForParent(Parent parent) {
        return new PluginViewOld() {
            @Override
            public Parent getParent() {
                return parent;
            }

            @Override
            public FlowConfig getConfig() {
                return new DefaultFlowConfigBuilder().setName("").build();
            }
        };
    }
}
