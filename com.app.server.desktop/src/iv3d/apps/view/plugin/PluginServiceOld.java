package iv3d.apps.view.plugin;

import com.app.server.base.server.web.ApplicationDataParam;

import java.util.Map;

public interface PluginServiceOld {
    ApplicationDataParam executeCommand(String command, Map<String, String> params);
}
