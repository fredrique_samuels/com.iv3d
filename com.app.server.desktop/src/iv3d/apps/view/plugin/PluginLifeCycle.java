package iv3d.apps.view.plugin;

/**
 * Created by fred on 2016/07/17.
 */
public interface PluginLifeCycle {
    void dispose();
}
