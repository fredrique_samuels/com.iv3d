package iv3d.apps.view.plugin.bizimpl;

import com.app.server.base.server.web.ApplicationDataParam;
import com.google.inject.Provider;
import iv3d.apps.view.PluginServiceManager;
import iv3d.apps.view.plugin.Plugin;
import iv3d.apps.view.plugin.PluginServiceOld;
import iv3d.apps.view.plugin.PluginViewOld;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractPluginService implements PluginServiceOld {
    private static Logger logger = Logger.getLogger(AbstractPluginService.class);
    private Provider<PluginServiceManager> appServiceProvider;
    private Plugin plugin;

    /* Setup setters */
    public final void setAppServiceProvider(Provider<PluginServiceManager> appServiceProvider) {this.appServiceProvider = appServiceProvider;}
    public final void setPlugin(Plugin plugin) {this.plugin = plugin;}

    /* Jython property getters */
    public final PluginServiceOld getPluginService(String id) {
        PluginServiceManager pluginServiceManager = getPluginServiceManager();
        if(pluginServiceManager==null) {
            return null;
        }
        return pluginServiceManager.getPluginService(id);
    }

    public final List<PluginViewOld> createPluginViews(String pluginId, Map<String, String> params) {
        return getPluginServiceManager().getViews(pluginId, params);
    }

    public final List<PluginViewOld> createPluginViews(String pluginId) {
        return getPluginServiceManager().getViews(pluginId, new HashMap<>());
    }

    /* Misc */
    public final void logServiceAccessError(String packageId, Exception e) {
        logger.warn("Unable to reach remote service "+packageId, e);
    }

    public final void logPluginServiceNoAvailableWarning(String serviceId) {
        PluginServiceManager pluginServiceManager = getPluginServiceManager();
        pluginServiceManager.logPluginServiceNoAvailableWarning(plugin.getId(), serviceId);
    }

    public final void showView(PluginViewOld view) {
        PluginServiceManager pluginServiceManager = getPluginServiceManager();
        pluginServiceManager.showView(view);
    }

    /* Response help methods */
    public final ApplicationDataParam returnOk(){
        return new ApplicationDataParam();
    }

    private final PluginServiceManager getPluginServiceManager() {return appServiceProvider.get();}
}
