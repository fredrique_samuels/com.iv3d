package iv3d.apps.view.plugin.bizimpl;

import com.google.inject.Provider;
import iv3d.apps.view.PluginServiceManager;
import iv3d.apps.view.plugin.PluginServiceOld;
import iv3d.apps.view.plugin.PluginViewOld;

import java.util.List;
import java.util.Map;

/**
 * Created by fred on 2016/09/08.
 */
public interface PluginController {
    void setPluginServiceManagerProvider(Provider<PluginServiceManager> appServiceProvider);
    PluginServiceOld getPluginService(String id);
    List<PluginViewOld> create(String pluginId, Map<String, String> params);

}
