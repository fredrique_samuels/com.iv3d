package iv3d.apps.view.plugin;

import com.iv3d.common.mvc.control.config.FlowConfig;
import javafx.scene.Parent;

public abstract class PluginViewLifeCycle implements PluginLifeCycle {

    public abstract Parent getParent();
    public abstract FlowConfig getFlowConfig();

    public static
    PluginViewLifeCycle createForModel(PluginViewOld pluginView, PluginViewModel model) {
        return new SingleModelPluginView(pluginView, model);
    }

    public static PluginViewLifeCycle create(PluginViewOld view) {
        return createForModel(view, null);
    }

    private static class SingleModelPluginView extends PluginViewLifeCycle {

        private final PluginViewOld pluginView;
        private final PluginViewModel model;

        public SingleModelPluginView(PluginViewOld pluginView, PluginViewModel model) {
            this.pluginView = pluginView;
            this.model = model;
        }

        @Override
        public void dispose() {
            if(model!=null) {model.dispose();}
        }

        @Override
        public Parent getParent() {
            return pluginView.getParent();
        }

        @Override
        public FlowConfig getFlowConfig() {
            return pluginView.getConfig();
        }
    }
}
