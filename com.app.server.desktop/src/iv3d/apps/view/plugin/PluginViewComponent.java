package iv3d.apps.view.plugin;


public interface PluginViewComponent {
    void setText(String text);
    String getText();

    void select(String text);
    void deselect(String text);

    void disable(String text);
    void enable(String text);

    void hide();
    void show();

    void add(PluginViewComponent component);
    void remove(PluginViewComponent component);
}
