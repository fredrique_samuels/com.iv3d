package iv3d.apps.view.plugin;

/**
 * Created by fred on 2016/07/15.
 */
public class PluginServiceSettings {
    private PluginBean bean;

    public PluginBean getBean() {
        return bean;
    }

    public void setBean(PluginBean bean) {
        this.bean = bean;
    }
}
