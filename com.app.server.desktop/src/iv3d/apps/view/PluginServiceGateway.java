package iv3d.apps.view;

import iv3d.apps.view.plugin.PluginViewOld;

import java.util.List;
import java.util.Map;

/**
 * Created by fred on 2016/07/15.
 */
public interface PluginServiceGateway {
    List<PluginViewOld> getViews(String pluginId, String action, Map<String, String> params);
}
