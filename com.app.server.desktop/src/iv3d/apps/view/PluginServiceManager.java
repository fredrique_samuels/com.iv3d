/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.apps.view;

import com.app.server.base.server.web.ApplicationDataParam;
import com.iv3d.common.rest.RestRequest;
import iv3d.apps.view.plugin.PluginServiceOld;
import iv3d.apps.view.plugin.PluginViewOld;
import iv3d.apps.view.plugin.PluginViewLifeCycle;

import java.util.List;
import java.util.Map;

public interface PluginServiceManager {
    String getAppHost(String packageIds);
    PluginServiceOld getPluginService(String id);
    ApplicationDataParam executeServiceCommand(String pluginId, String command, Map<String, String> params);

    List<PluginViewOld> getViews(RestRequest request, Map<String, String> params);
    List<PluginViewOld> getViews(String id, Map<String, String> params);

    void showView(PluginViewOld view);
    void showView(PluginViewOld view, Map<String, String> params);
    void showView(PluginViewLifeCycle viewLifeCycle, Map<String, String> params);

    void logPluginServiceNoAvailableWarning(String id, String serviceId);

    boolean isProduction();
    boolean isDev();
}
