from iv3d.apps.desktop.platform.plugin import PluginService
from iv3d.apps.desktop.platform.plugin import PluginServiceCommandHandler

class DefaultHandler(PluginServiceCommandHandler):

    def __init__(self):
        PluginService.__init__(self)

    def execute(self, plugin_service, params):
        print 'Running Handler', plugin_service.getPluginId(), params, self


class MyWorldMainService(PluginService):

    def __init__(self):
        PluginService.__init__(self)
        self.addCommandHandler('main', DefaultHandler() )
