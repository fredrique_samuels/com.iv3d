import java.util.HashMap as HashMap

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import iv3d.apps.view.plugin.PluginView;

import jython_app_common
from jython_app_common import JythonPluginService
from jython_app_common import JythonPluginServiceCommand

import com.iv3d.common.ResourceLoader as ResourceLoader


class MainCommandHandler(JythonPluginServiceCommand):
    def execute(self, parentService, params):
        coreFormService = parentService.getPluginService(jython_app_common.CORE_FORM_SERVICE)
        if(coreFormService):
            coreFormService.executeCommand("main");
            print 'coreFormService works'
        else:
            print 'Unable to load service  coreFormService'

        views = parentService.createPluginViews("iv3d.authentication.dialog", HashMap());
        print views

        btn = javafx.scene.control.Button();
        btn.setText("Say 'Hello World'");

        root = javafx.scene.layout.StackPane()
        root.getChildren().add(btn);


        # String image = JavaFXApplication9.class.getResource("splash.jpg").toExternalForm();
        # root.setStyle("-fx-background-image: url('" + image + "'); " +
        #               "-fx-background-position: center center; " +
        #               "-fx-background-repeat: stretch;");
        # parentService.showView(iv3d.apps.view.plugin.PluginView.createForParent(root));

        # worldMapService = self.getPluginService("iv3d.worldmap")
        # if not worldMapService:
        #     return None
        #
        # views = worldMapService.execute("view/fullmap", HashMap());

        if views:
            for v in views:
                parentService.showView(v);
        return parentService.returnOk()


class Iv3dDashboardService(JythonPluginService):

    def __init__(self):
        JythonPluginService.__init__(self)
        self.set_command_handler("main", MainCommandHandler())
