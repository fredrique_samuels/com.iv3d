# -*- coding: ascii -*-

import json
from javafx.event import EventHandler
from javafx.scene.control import Button
from javafx.scene.input import MouseEvent
from javafx.scene.shape import SVGPath
from jython_app_common import *
from jython_view_common import *

from com.iv3d.common.map import SphereMercatorCalculator
from com.iv3d.common import LatLng

class WorldEventsController(JythonController):
    def __init__(self):
        JythonController.__init__(self)
        self.svgContainer = JythonSvgPane()
    def addMapSvgs(self):
        map_pane = self.findNodeById("map_container")
        map_pane.getChildren().add(self.svgContainer)
        svgs = self.load_svgs()
        for svg in svgs :
            path = SVGPath()
            path.setContent(svg['svgPath'])
            path.setStyle("-fx-stroke-width:1;-fx-fill:null;-fx-stroke:black")
            self.svgContainer.add(path);
    def load_svgs(self):
        resource = self.getResource("iv3d.worldmap.events.widget/data/worldmapSvg.json")
        fh = open(resource, 'r')
        strRes = fh.read();
        fh.close();
        svgs = json.loads(strRes)['body']
        return svgs
    def load_meta_data(self):
        resource = self.getResource("iv3d.worldmap.events.widget/data/worldmapBB.json")
        fh = open(resource, 'r')
        strRes = fh.read();
        fh.close();
        svgs = json.loads(strRes)
        return svgs
    def initNode(self, *args):
        root = self.getNode()
        print "Init WM " + str(root) + " " + str(args)
        self.addMapSvgs()
        self.mapMetaData = self.load_meta_data()

        class onMouseEnter (EventHandler):
            def __init__(self):
                EventHandler.__init__(self)
            def handle(self, event):
                print event

        notification_container = self.findNodeById("notification_container")

        button = Button("Hello")
        notification_container.getChildren().add(button);

        def callback(r):
            point = get_map_point_from_latlng(LatLng(0,0), r.getWidth(), r.getHeight())
            button.setLayoutX(r.getX() + point.getX());
            button.setLayoutY(r.getY() + point.getY());

        def get_map_point_from_latlng(latLng, w, h):
            t = self.mapMetaData['topLatitude']
            l = self.mapMetaData['leftLongitude']
            b = self.mapMetaData['bottomLatitude']
            r = self.mapMetaData['rightLongitude']
            smc = SphereMercatorCalculator(LatLng(t, l), LatLng(b, r), w, h)
            return smc.latLngToPoint(latLng)

        self.svgContainer.setCallback(callback);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED,
                                   onMouseEnter())

        categoryPanel = self.findNodeById("categoryPanel")

        # def on_event(*args, **kargs):
        #     print args
        # testButton = self.findNodeById("test_button")
        # testButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
        #                            onMouseEnter())




