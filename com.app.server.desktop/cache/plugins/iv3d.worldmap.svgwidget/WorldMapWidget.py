from jython_app_common import JythonPluginService
from jython_app_common import JythonViewInit
from jython_view_common import *


class WorldMapService (JythonPluginService):
    pass

class WorldEventsController(JythonController):
    def __init__(self):
        JythonViewInit.__init__(self)
    def initNode(self, *args):
        print "Init WM " + str(self.getNode()) + " " + args

class WorldMapWidget(JythonSvgPane):
    def __init__(self):
        JythonSvgPane.__init__(self)
