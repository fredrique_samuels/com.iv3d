import java.util.HashMap as HashMap
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import iv3d.apps.view.plugin.PluginView;

from jython_app_common import JythonPluginService
from jython_app_common import JythonPluginServiceCommand


class Action(javafx.event.EventHandler) :
    def handle(self, event):
        print "Hello World!"

class MainCommandHandler(JythonPluginServiceCommand):
    def execute(self, parentService, params):
        print self

        btn = javafx.scene.control.Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(Action())

        root = javafx.scene.layout.StackPane()
        root.getChildren().add(btn);

        parentService.showView(iv3d.apps.view.plugin.PluginView.createForParent(root));

        return parentService.returnOk()

class CoreFormService(JythonPluginService):

    def __init__(self):
        JythonPluginService.__init__(self)
        self.set_command_handler("main", MainCommandHandler())
