
//=================================
// Tasks
var __loginTask = null

// UI Model
var __viewModel = {
		"heading":"Authentication Demo",
		"user_label":"User",
		"password_label":"Password",
		"cancel_label":"Cancel",
		"submit_label":"Submit",
		"cancel_help_text":"Cancel",
		"submit_help_text":"Submit",
		"info_text":"some text",
		"submit_progress_text":"Authenticating User ...",
		"cancel_progress_text":"Canceling Authenticating ...",
		"can_cancel":true
	}

var __processing = false
var __canceling = false
var __error_state = false

function setProcessing(b) {
	__processing=b;
	__canceling=false;
	__error_state=false;
	__update_view__();
}
function setCanceling() {
	__canceling=true;
	__update_view__();
}
function setDisplayingError(message) {
	__error_state=true;
	__viewModel.info_text=message;
	__update_view__();
}
function setNoError(message) {
	__error_state=false;
	__viewModel.info_text=message;
	__update_view__();
}

//==================================
// Framework functions

function __get_view__() {
	return __viewModel;
}

function __set_view__(view) {
	__viewModel = view
	
	// set static text values
	heading.setText(view.heading)
	userLabel.setText(view.user_label)
	passwordLabel.setText(view.password_label)
	cancelAction.setText(view.cancel_label)
	submitAction.setText(view.submit_label)
	
	// set info text style
	if(__error_state)
		resultText.setId("result_text_error")
	else 
		resultText.setId("result_text")

	// set info text  uhmmm text
	if(__processing && !__canceling)
		resultText.setText(view.submit_progress_text)
	else if(__canceling) 
		resultText.setText(view.cancel_progress_text)
	else
		resultText.setText(view.info_text)
	
	// disable inputs while processing 
	userIdInput.setDisable(__processing);
	passwordInput.setDisable(__processing);
	submitAction.setDisable(__processing);
	cancelAction.setDisable(!__canceling);
}	

function __update_view__() {
	__set_view__(__get_view__())
}

function __init__() {
	setProcessing(false);
}

//==================================
// Logic functions

function doSubmitAction(event) {
	__loginTask = controller.buildTask("login")
		.setDoneFunction(onSubmitResult)
		.setTimeout(10000)
		.setParam('username', userIdInput.getText())
		.setParam('password', passwordInput.getText())
		.execute();
	setProcessing(true);
}

function onSubmitResult(result) {
	// setProcessing(false);
	// if(result.hasError()) {
	// 	setDisplayingError(result.getMessage());
	// } else {
	// 	setNoError(result.getMessage());
	// }
}

function doCancelAction(event) {
	// if(__processing) {
	// 	setCanceling();
	//     __loginTask.cancel();
	// } else if (__viewModel.can_cancel){
	// 	cancelOpeation();
	// }
}

function cancelOpeation() {
	controller.buildTask("cancel")
		.execute(); 
}