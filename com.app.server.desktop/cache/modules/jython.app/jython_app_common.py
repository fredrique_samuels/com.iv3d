import java.util.HashMap as HashMap
from com.iv3d.common import Percentage
from com.iv3d.common.rest import HttpRestClient
from com.iv3d.common.ui import UiTaskResultBuilder
from com.iv3d.common.ui import UiTaskStatusBuilder
from iv3d.apps.view.jfx import AbstractJfxController
from iv3d.apps.view.plugin import PluginTask
from iv3d.apps.view.plugin.bizimpl import AbstractPluginService
from java.lang import Exception

CORE_FORM_SERVICE = "iv3d.core.widgets.form"
CORE_DASHBOARD_SERVICE = "iv3d.dashboard.service"

class JythonController (AbstractJfxController):
    def __init__(self, *args):
        AbstractJfxController.__init__(self, *args)

class JythonPluginServiceCommand:
    def execute(self, parentService, params):
        raise Exception();

class JythonPluginService(AbstractPluginService):
    def __init__(self):
        AbstractPluginService.__init__(self)
        self.__commandHandlers = {}

    def set_command_handler(self, command, handler):
        self.__commandHandlers[command] = handler

    def executeCommand(self, command, params=HashMap()):
        if self.__commandHandlers.has_key(command):
            self.__commandHandlers[command].execute(self, params)


class TaskResultBuilder:
    def result(self, message, value=None, error=None):
        b = UiTaskResultBuilder()
        b.setMessage(message)
        if value:
            b.setValue(value)
        if error:
            b.setException(error)
        return b.build()
    def result_error(self, message):
        b = UiTaskResultBuilder()
        b.configureAsError(message)
        return b.build()

class JythonPluginTask(PluginTask, TaskResultBuilder):
    def __init__(self):
        TaskResultBuilder.__init__(self)
    def get_node_by_id(self, id):
        if self.nodeLookup:
            return self.nodeLookup.lookup(id)
    def get_param(self, name):
        return self.getParameter(name);
    def is_canceled(self):
        return self.context.isCanceled()
    def post_update(self, message, percentage=None):
        b = UiTaskStatusBuilder()
        if(percentage):
            b.setPercentage(Percentage(percentage))
        b.setMessage(message)
        self.context.postUpdate(b.build())

class WebRequest:
    def get(self, url):
        return HttpRestClient().buildGet(url);

    def post(self, url):
        return HttpRestClient().buildPost(url);



