package com.iv3d.schema;

/**
 * Created by fred on 2017/02/13.
 */
public class SchemaConfig {
    SchemaConnectionConfig connection;
    String packageName;

    public SchemaConnectionConfig getConnection() {
        return connection;
    }

    public void setConnection(SchemaConnectionConfig connection) {
        this.connection = connection;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
