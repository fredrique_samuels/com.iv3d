package etc.config.sql.schema.iv3d_test;

import com.eroi.migrate.Migration;

import static com.eroi.migrate.Define.DataTypes.INTEGER;
import static com.eroi.migrate.Define.DataTypes.VARCHAR;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

public class Migration_1 implements Migration {

  public void up() {
    createTable(
      table("simple_table",
        column("id", INTEGER, primarykey(), notnull()),
        column("desc", VARCHAR, length(50), defaultValue("NA"))));
  }

  public void down() {
    dropTable("simple_table");
  }
} 