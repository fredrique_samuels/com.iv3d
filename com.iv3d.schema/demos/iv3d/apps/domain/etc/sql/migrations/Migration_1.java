package iv3d.apps.domain.etc.sql.migrations;

import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Define.DataTypes.*;
import static com.eroi.migrate.Execute.*;
import com.eroi.migrate.Migration; 

public class Migration_1 implements Migration {

  public void up() {
    createTable(
      table("simple_table",
        column("id", INTEGER, primarykey(), notnull()),
        column("desc", VARCHAR, length(50), defaultValue("NA"))));
  }

  public void down() {
    dropTable("simple_table");
  }
} 