package iv3d.apps.domain.etc.sql.migrations;

import com.eroi.migrate.Migration;

import static com.eroi.migrate.Define.DataTypes.INTEGER;
import static com.eroi.migrate.Define.DataTypes.VARCHAR;
import static com.eroi.migrate.Define.*;
import static com.eroi.migrate.Execute.createTable;
import static com.eroi.migrate.Execute.dropTable;

public class Migration_2 implements Migration {

  public void up() {
    createTable(
      table("simple_table2",
        column("id", INTEGER, primarykey(), notnull()),
        column("desc", VARCHAR, length(50), defaultValue("NA"))));
  }

  public void down() {
    dropTable("simple_table2");
  }
} 