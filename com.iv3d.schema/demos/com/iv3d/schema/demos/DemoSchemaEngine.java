package com.iv3d.schema.demos;


import com.iv3d.common.storage.migrate.SchemaMigration;
import com.iv3d.common.storage.migrate.SchemaMigrationRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by fred on 2017/02/13.
 */
public class DemoSchemaEngine {

    public static void main(String[] args) throws IOException {

//        Connection c = null;
//        Statement stmt = null;
//        try {
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:test.db");
//            System.out.println("Opened database successfully");
//
//            stmt = c.createStatement();
//            String sql = "CREATE TABLE COMPANY " +
//                    "(ID INT PRIMARY KEY     NOT NULL," +
//                    " NAME           TEXT    NOT NULL, " +
//                    " AGE            INT     NOT NULL, " +
//                    " ADDRESS        CHAR(50), " +
//                    " SALARY         REAL)";
//            stmt.executeUpdate(sql);
//            stmt.close();
//            c.close();
//        } catch ( Exception e ) {
//            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//            System.exit(0);
//        }
//        System.out.println("Table created successfully");

        String[] schemaFiles = {"iv3d_test3.schema.properties"};
        SchemaMigrationRunner schemaMigration = new SchemaMigrationRunner(schemaFiles);
        schemaMigration.updateAll();
    }
}
