/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.projman.projfile.domain;

import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.projfile.ProjectFileDao;
import com.iv3d.projman.projfile.ProjectFileFactory;
import com.iv3d.projman.projfile.ProjectFileManager;

public class DefaultProjectFileManager implements ProjectFileManager {

	private ProjectFileFactory projectFactory;
	private ProjectFileDao dao;

	public DefaultProjectFileManager(ProjectFileFactory projectFactory, ProjectFileDao dao) {
		this.projectFactory = projectFactory;
		this.dao = dao;
	}

	@Override
	public ProjectFileParam createProject(String name, String description) {
		ProjectFileParam param = projectFactory.create(name, description);
		upsert(param);
		return param;
	}

	@Override
	public void upsert(ProjectFileParam project) {
		dao.upsert(project);
	}

	@Override
	public ProjectFileParam[] get(String ...ids) {
		return dao.get(ids);
	}

	@Override
	public String[] getIdsList() {
		return dao.getIdsList();
	}

}
