/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.projman.projfile.domain;

import com.iv3d.common.IdGenerator;
import com.iv3d.common.date.UtcClock;
import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.projfile.ProjectFileFactory;

public class DefaultProjectFileFactory implements ProjectFileFactory {

	private UtcClock calender;
	private IdGenerator idGenerator;

	public DefaultProjectFileFactory(UtcClock calender, IdGenerator idGen) {
		this.calender = calender;
		this.idGenerator = idGen;
	}

	@Override
	public ProjectFileParam create(String name, String details) {
		ProjectFileParam projectFile = new ProjectFileParam();
		projectFile.setId(idGenerator.createId());
		projectFile.setDateCreated(calender.now().toIso8601String());
		projectFile.setName(name);
		projectFile.setDetail(details);
		return projectFile;
	}
	
}
