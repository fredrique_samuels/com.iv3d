/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.projman.media.domain;

import java.io.File;
import java.io.InputStream;

import com.iv3d.common.IdGenerator;
import com.iv3d.projman.dao.FileSystemDao;
import com.iv3d.projman.entries.LogEntryParam;
import com.iv3d.projman.media.MediaDao;

public class DefaultMediaDao extends FileSystemDao<LogEntryParam> implements MediaDao {

	private IdGenerator idGenerator;

	public DefaultMediaDao(File dataRoot, IdGenerator idGenerator) {
		super(dataRoot, "media", LogEntryParam.class);
		this.idGenerator = idGenerator;
	}

	@Override
	public String save(InputStream media) {
		String id = idGenerator.createId();
		upsertStream(id, media);
		return id;
	}

	@Override
	public File getFileById(String id) {
		return getStreamFileById(id);
	}

}
