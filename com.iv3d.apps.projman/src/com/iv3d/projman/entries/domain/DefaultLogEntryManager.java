/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.projman.entries.domain;

import java.io.File;
import java.io.InputStream;

import com.iv3d.projman.entries.LogEntryDao;
import com.iv3d.projman.entries.LogEntryFactory;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.entries.LogEntryParam;
import com.iv3d.projman.media.MediaDao;

public class DefaultLogEntryManager implements LogEntryManager {

	private LogEntryFactory factory;
	private LogEntryDao entryDao;
	private MediaDao mediaDao;

	public DefaultLogEntryManager(LogEntryFactory factory, LogEntryDao dao, MediaDao mediaDao) {
		this.factory = factory;
		this.entryDao = dao;
		this.mediaDao = mediaDao;
	}


	public final class LogEntryAlreadyExistsError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public LogEntryAlreadyExistsError(String id) {
			super(String.format("A log entry with the id='%s'  already exists.", id));
		}
	}

	@Override
	public LogEntryParam save(String projectId, String[] tagIds, String mediaType, InputStream media) {
		LogEntryParam param = factory.create(projectId, tagIds);
		
		if(media==null) {
			param.setMediaType("no media");
		} else  {
			String mediaId = mediaDao.save(media);
			param.setMediaType(mediaType);
			param.setMediaId(mediaId);
		}
		entryDao.upsert(param);
		return param;
	}

	@Override
	public LogEntryParam[] getLogs(String projectId) {
		return entryDao.getLogs(projectId);
	}

	@Override
	public File getMedia(String mediaId) {
		return mediaDao.getFileById(mediaId);
	}

}