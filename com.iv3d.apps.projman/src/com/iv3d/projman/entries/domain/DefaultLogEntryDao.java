package com.iv3d.projman.entries.domain;

import java.io.File;

import com.iv3d.common.Filter;
import com.iv3d.projman.dao.FileSystemDao;
import com.iv3d.projman.entries.LogEntryDao;
import com.iv3d.projman.entries.LogEntryParam;

public class DefaultLogEntryDao extends FileSystemDao<LogEntryParam> implements LogEntryDao {

	public DefaultLogEntryDao(File dataRoot) {
		super(dataRoot, "entries", LogEntryParam.class);
	}

	@Override
	public LogEntryParam[] getLogs(final String projId) {
		Filter<LogEntryParam> filter = new Filter<LogEntryParam>() {
			
			@Override
			public boolean accept(LogEntryParam readFromFile) {
				return projId.equals(readFromFile.getProjectId());
			}
		}; 
		return get(filter, getIdsList());
	}
	
}
