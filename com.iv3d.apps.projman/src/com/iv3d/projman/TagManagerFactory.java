package com.iv3d.projman;

import java.io.File;

import com.iv3d.common.IdGenerator;
import com.iv3d.common.date.SystemMilliSecSource;
import com.iv3d.common.date.UtcClock;
import com.iv3d.common.date.UtcSystemClock;
import com.iv3d.projman.dao.PersistedIdGenerator;
import com.iv3d.projman.tags.TagDao;
import com.iv3d.projman.tags.TagManager;
import com.iv3d.projman.tags.domain.DefaultTagDao;
import com.iv3d.projman.tags.domain.DefaultTagFactory;
import com.iv3d.projman.tags.domain.DefaultTagManager;

public class TagManagerFactory {

	public TagManager create(File dataRoot) {
		UtcClock clock = new UtcSystemClock(new SystemMilliSecSource());
		IdGenerator idGenerator = new PersistedIdGenerator("tags", dataRoot);
		DefaultTagFactory factory = new DefaultTagFactory(clock, idGenerator);
		TagDao dao = new DefaultTagDao(dataRoot);
		return new DefaultTagManager(factory, dao);
	}

}
