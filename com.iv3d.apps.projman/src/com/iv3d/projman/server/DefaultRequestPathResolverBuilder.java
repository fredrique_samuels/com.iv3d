package com.iv3d.projman.server;

import org.eclipse.jetty.server.Handler;

import com.google.inject.Inject;
import com.iv3d.common.server.RequestPathResolver;
import com.iv3d.common.server.inject.RequestPathResolverBuilder;
import com.iv3d.projman.server.usecases.EntryController;
import com.iv3d.projman.server.usecases.ProjectFileController;
import com.iv3d.projman.server.usecases.TagController;

public class DefaultRequestPathResolverBuilder implements RequestPathResolverBuilder {

	private final ProjectFileController projectFileController;
	private final TagController tagController;
	private final EntryController entryController;
	private final RequestPathResolver resolver;

	@Inject
	public DefaultRequestPathResolverBuilder(RequestPathResolver resolver, ProjectFileController projectFileController, TagController tagController,
			EntryController entryController) {
		super();
		this.resolver = resolver;
		this.projectFileController = projectFileController;
		this.tagController = tagController;
		this.entryController = entryController;
	}

	public Handler build() {
		resolver.addController(projectFileController);
		resolver.addController(tagController);
		resolver.addController(entryController);
		return resolver;
	}

}
