package com.iv3d.projman.server;


import com.google.inject.Guice;
import com.google.inject.Injector;

public final class ProjectManagerServerMain {
	private ProjectManagerService server;

	public ProjectManagerServerMain() {
		Injector injector = Guice.createInjector(new ServerSettingsInjector());
		server = injector.getInstance(ProjectManagerService.class);
	}
	
	public final void run() {
		server.run();
	}
	
	public final void stop() {
		server.stop();
	}
	
	public static void main(String[] args) throws Exception {
		//docs http://www.baeldung.com/httpclient-post-http-request
		new ProjectManagerServerMain().run();
	}
}
