package com.iv3d.projman.server.injection;

import java.io.File;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.server.inject.settings.DataDirProvider;
import com.iv3d.projman.TagManagerFactory;
import com.iv3d.projman.tags.TagManager;

@Singleton
public class TagManagerBuilder {
	private TagManager manager;
	private final DataDirProvider dataRoot;
	private final TagManagerFactory factory;
	
	@Inject
	public TagManagerBuilder(DataDirProvider dataRoot,
			TagManagerFactory factory) {
				this.dataRoot = dataRoot;
				this.factory = factory;
	}
	
	public TagManager build() {
		if(manager==null) {
			manager = factory.create(new File(dataRoot.get()));
		}
		return manager;
	}

}
