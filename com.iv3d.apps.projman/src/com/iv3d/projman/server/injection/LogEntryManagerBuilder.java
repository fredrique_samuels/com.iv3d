package com.iv3d.projman.server.injection;

import java.io.File;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.server.inject.settings.DataDirProvider;
import com.iv3d.projman.LogEntryManagerFactory;
import com.iv3d.projman.entries.LogEntryManager;

@Singleton
public class LogEntryManagerBuilder {
	private LogEntryManager manager;
	private final DataDirProvider dataRoot;
	private final LogEntryManagerFactory factory;
	
	@Inject
	public LogEntryManagerBuilder(DataDirProvider dataRoot,
			LogEntryManagerFactory factory) {
				this.dataRoot = dataRoot;
				this.factory = factory;
	}
	
	public LogEntryManager build() {
		if(manager==null) {
			manager = factory.create(new File(dataRoot.get()));
		}
		return manager;
	}

}
