package com.iv3d.projman.server;

import org.eclipse.jetty.server.handler.ResourceHandler;

import com.iv3d.common.server.inject.ResourceHandlerBuilder;

public class DefaultResourceHandlerBuilder implements ResourceHandlerBuilder {

	@Override
	public ResourceHandler build() {
		ResourceHandler resource_handler = new ResourceHandler();
	    resource_handler.setDirectoriesListed(true);
	    resource_handler.setWelcomeFiles(new String[]{ "index.html", "upload_entry.html" });
	    resource_handler.setResourceBase("cache");
 
	    return resource_handler;
	}

}
