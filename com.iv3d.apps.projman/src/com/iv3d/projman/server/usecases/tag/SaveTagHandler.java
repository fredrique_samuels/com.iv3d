package com.iv3d.projman.server.usecases.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.server.injection.TagManagerBuilder;
import com.iv3d.projman.tags.TagManager;
import com.iv3d.projman.tags.TagParam;

public class SaveTagHandler extends AbstractPathHandler {

	private TagManager manager;

	@Inject
	public SaveTagHandler(TagManagerBuilder manager) {
		this.manager = manager.build();
	}
	
	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		TagParam param = getJsonBody(request, TagParam.class);
		TagParam[] pf = manager.get(param.getId());
		if(pf.length==0) {
			String message = "Tag with id='%s' does not exists.";
			returnFailure(response, String.format(message, param.getId()));
		} else {
			manager.upsert(param);
			returnOkResponse(response);
		}
	}

	@Override
	public String getMethod() {
		return GET;
	}

}
