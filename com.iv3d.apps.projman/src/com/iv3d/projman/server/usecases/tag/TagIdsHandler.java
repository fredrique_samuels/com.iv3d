package com.iv3d.projman.server.usecases.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.server.injection.TagManagerBuilder;
import com.iv3d.projman.tags.TagManager;
import com.iv3d.projman.tags.TagParam;

public class TagIdsHandler extends AbstractPathHandler {
	private TagManager manager;

	@Inject
	public TagIdsHandler(TagManagerBuilder manager) {
		this.manager = manager.build();
	}
	
	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		String[] idsList = manager.getIdsList();
		TagParam[] params = manager.get(idsList);
		returnJsonParam(response, params);
	}

	@Override
	public String getMethod() {
		return GET;
	}

}
