package com.iv3d.projman.server.usecases.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.server.injection.TagManagerBuilder;
import com.iv3d.projman.tags.TagManager;
import com.iv3d.projman.tags.TagParam;

public class CreateTagHandler extends AbstractPathHandler {

	private TagManager manager;

	@Inject
	public CreateTagHandler(TagManagerBuilder manager) {
		this.manager = manager.build();
	}
	
	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		String value = getParameter(request, "value");
		TagParam param = manager.createTag(value);
		returnJsonParam(response, param);
	}

	@Override
	public String getMethod() {
		return GET;
	}

}
