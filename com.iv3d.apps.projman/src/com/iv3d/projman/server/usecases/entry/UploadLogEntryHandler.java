package com.iv3d.projman.server.usecases.entry;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.common.server.MultiPartRequest;
import com.iv3d.common.server.RequestMultiPartParser;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.entries.LogEntryParam;
import com.iv3d.projman.server.injection.LogEntryManagerBuilder;

public class UploadLogEntryHandler extends AbstractPathHandler {

	private final LogEntryManager logEntryManager;
	private final RequestMultiPartParser multiPartParser;

	@Inject
	public UploadLogEntryHandler(LogEntryManagerBuilder logEntryManager,
			RequestMultiPartParser multiPartParser) {
		this.multiPartParser = multiPartParser;
		this.logEntryManager = logEntryManager.build();
	}

	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		
		try {
			MultiPartRequest multiPartRequest = multiPartParser.parse(request);
			InputStream media = multiPartRequest.getInputStream();
			
			String projectId = getParameter(multiPartRequest, "projectId");
			String[] tagIds = getListParameter(multiPartRequest, "tagId");
			String ignoreResponse = getOptionalParameter(multiPartRequest, "ignoreResponse", "false");
			String type = getParameter(multiPartRequest, "mediaType");
			
			LogEntryParam param = logEntryManager.save(projectId, tagIds, type, media);
			
			if(ignoreResponse.equals("true")) {
				returnNoContent(response);
			} else {
				returnJsonParam(response, param);
			}
		} catch (IOException e) {
			returnFailure(response, e.getMessage());
		} catch (FileUploadException e) {
			returnFailure(response, e.getMessage());
		}
	}

	@Override
	public String getMethod() {
		return POST;
	}

}
