package com.iv3d.projman.server.usecases.entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.entries.LogEntryParam;
import com.iv3d.projman.server.injection.LogEntryManagerBuilder;

public class RetreiveLogEntriesHandler extends AbstractPathHandler  {
	private final LogEntryManager logEntryManager;

	@Inject
	public RetreiveLogEntriesHandler(LogEntryManagerBuilder logEntryManager) {
		this.logEntryManager = logEntryManager.build();
	}
	
	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		String projectId = getParameter(request, "projectId");
		LogEntryParam[] params = logEntryManager.getLogs(projectId);
		returnJsonParam(response, params);
	}

	@Override
	public String getMethod() {
		return GET;
	}

}
