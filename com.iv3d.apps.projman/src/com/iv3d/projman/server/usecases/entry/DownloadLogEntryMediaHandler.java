package com.iv3d.projman.server.usecases.entry;

import java.io.File;
import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.server.injection.LogEntryManagerBuilder;

public class DownloadLogEntryMediaHandler extends AbstractPathHandler {
	private final LogEntryManager manager;

	@Inject
	public DownloadLogEntryMediaHandler(LogEntryManagerBuilder logEntryManager) {
		this.manager = logEntryManager.build();
	}
	
	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {

		String mediaId = getParameter(request, "mediaId");
		File media = manager.getMedia(mediaId);
		if(media==null) {
			returnFailure(response, "media with id='%s' does not exist");
			return;
		}
		
        try {
			downloadFile(request, response, media);
		} catch (FileNotFoundException e) {
			returnFailure(response, "media with id='%s' does not exist");
		}         
	}

	

	@Override
	public String getMethod() {
		return GET;
	}

}
