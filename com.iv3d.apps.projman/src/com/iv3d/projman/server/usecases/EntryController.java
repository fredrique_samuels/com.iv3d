package com.iv3d.projman.server.usecases;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractController;
import com.iv3d.projman.server.usecases.entry.DownloadLogEntryMediaHandler;
import com.iv3d.projman.server.usecases.entry.RetreiveLogEntriesHandler;
import com.iv3d.projman.server.usecases.entry.UploadLogEntryHandler;

public class EntryController extends AbstractController {
	
	private UploadLogEntryHandler uploadLogEntryHandler;
	private RetreiveLogEntriesHandler retreiveLogEntriesHandler;
	private DownloadLogEntryMediaHandler downlaodLogEntryMediaHandler;
	
	@Inject
	public EntryController(UploadLogEntryHandler uploadLogEntryHandler,
			RetreiveLogEntriesHandler retreiveLogEntriesHandler,
			DownloadLogEntryMediaHandler downlaodLogEntryMediaHandler) {
		super();
		this.uploadLogEntryHandler = uploadLogEntryHandler;
		this.retreiveLogEntriesHandler = retreiveLogEntriesHandler;
		this.downlaodLogEntryMediaHandler = downlaodLogEntryMediaHandler;
	}

	@Override
	public void configure() {
		set("/com.iv3d.projman/entry/upload", uploadLogEntryHandler);
		set("/com.iv3d.projman/entry/media/download", downlaodLogEntryMediaHandler);
		set("/com.iv3d.projman/entry/all", retreiveLogEntriesHandler);
	}
}
