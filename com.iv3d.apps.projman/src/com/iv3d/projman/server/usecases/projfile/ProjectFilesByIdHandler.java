package com.iv3d.projman.server.usecases.projfile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.projfile.ProjectFileManager;
import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.server.injection.ProjectFileManagerBuilder;

public class ProjectFilesByIdHandler extends AbstractPathHandler {

	private final ProjectFileManager manager;

	@Inject
	public ProjectFilesByIdHandler(ProjectFileManagerBuilder manager) {
		this.manager = manager.build();
	}

	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		ProjectFileParam[] param = getParam(request);
		returnJsonParam(response, param);
	}

	private ProjectFileParam[] getParam(HttpServletRequest request) {
		String[] ids = getListParameter(request, "id");
		return manager.get(ids);
	}
	
	@Override
	public String getMethod() {
		return GET;
	}

}
