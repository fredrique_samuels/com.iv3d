/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.projman.entries.domain;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.iv3d.projman.MockAwareTestCase;
import com.iv3d.projman.entries.LogEntryDao;
import com.iv3d.projman.entries.LogEntryFactory;
import com.iv3d.projman.entries.LogEntryManager;
import com.iv3d.projman.entries.LogEntryParam;
import com.iv3d.projman.media.MediaDao;

import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.VerificationsInOrder;

public class DefaultEntryManagerTest extends MockAwareTestCase {
	@Mocked
	LogEntryFactory factory;
	@Mocked
	LogEntryParam entry;
	@Mocked
	LogEntryDao entryDao;
	@Mocked
	MediaDao mediaDao;
	@Mocked
	InputStream stream;
	
	LogEntryManager manager;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mockUtcDateIso8601("2016-01-02T13:00:00Z");
		mockIdGenerator("LOG_ID_1");
		manager = new DefaultLogEntryManager(factory, entryDao, mediaDao);
	}
	
	public void testCreateWithNoMedia() {
		final String projectId = "PROJ_1";
		final InputStream stream=null;
		String mediaType = "";
		final String[] tags = new String[]{"TagId1"};

		new NonStrictExpectations() {
			{
				factory.create(projectId, tags);
				result = entry;
			}
		};
		
		final LogEntryParam actual = manager.save(projectId, tags, mediaType, stream);
		assertNotNull(actual);
		
		new VerificationsInOrder() {
			
			{
				factory.create(projectId, tags);
				times=1;
				
				entry.setMediaType("no media");
				times=1;
				
				entryDao.upsert(entry);
				times=1;
			}
		};
	}
	
	public void testSaveWithMedia() {
		final String mediaId = "MEDIA_1";
		final String mediaType = "binary";
		final String projectId = "PROJ_1";
		final InputStream stream=new ByteArrayInputStream(new byte[]{});
		final String[] tags = new String[]{"TagId1"};

		new NonStrictExpectations() {
			{
				factory.create(projectId, tags);
				result = entry;
				
				mediaDao.save(stream);
				result = mediaId;
				
				entryDao.upsert(entry);
				times=1;
			}
		};
		
		final LogEntryParam actual = manager.save(projectId, tags, mediaType, stream);
		assertNotNull(actual);
		
		new VerificationsInOrder() {
			
			{
				factory.create(projectId, tags);
				times=1;
				
				entry.setMediaType(mediaType);
				times=1;
				
				entry.setMediaId(mediaId);
				times=1;
			}
		};
	}
	
	public void testGetLogsByProject(){
		final String projectId = "ProjId";
		manager.getLogs(projectId);
		
		new VerificationsInOrder() {
			
			{
				entryDao.getLogs(projectId);
				times=1;
			}
		};
		
	}
	
	public void testGetMediaFile(){
		final String mediaId = "MediaId";
		manager.getMedia(mediaId);
		
		new VerificationsInOrder() {
			
			{
				mediaDao.getFileById(mediaId);
				times=1;
			}
		};
	}
		
}
