package com.iv3d.projman;

import com.iv3d.common.IdGenerator;
import com.iv3d.common.date.UtcClock;
import com.iv3d.common.date.UtcDate;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

public class MockAwareTestCase extends TestCase {
	@Mocked
	protected UtcClock clock;
	@Mocked
	protected UtcDate date;
	@Mocked
	protected IdGenerator idGenerator;
	
	protected final void mockUtcDateIso8601(final String time) {
		new NonStrictExpectations() {
			
			{
				clock.now();
				result = date;
				
				date.toIso8601String();
				result = time;
			}
		};
	}
	
	protected final void mockIdGenerator(final String id) {
		mockIdGenerator(idGenerator, id);
	}
	
	protected final void mockIdGenerator(final IdGenerator idGenerator, final String id) {
		new NonStrictExpectations() {
			
			{
				idGenerator.createId();
				result = id;
			}
		};
	}
	
	public void testNothing(){}
}
