package com.iv3d.projman.tags.domain;

import com.iv3d.projman.tags.TagParam;
import com.iv3d.projman.MockAwareTestCase;
import com.iv3d.projman.tags.TagFactory;
import com.iv3d.projman.tags.domain.DefaultTagFactory;

public class DefaultTagFactoryTest extends MockAwareTestCase {
	private TagFactory instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		mockUtcDateIso8601("2016-01-02T13:00:00Z");
		mockIdGenerator("TAG_1");
		instance = new DefaultTagFactory(clock, idGenerator);
	}
	
	public void testCreate() {
		TagParam tag = instance.create("value");
		
		assertNotNull(tag);
		assertEquals("TAG_1", tag.getId());
		assertEquals("2016-01-02T13:00:00Z", tag.getDateCreated());
		assertEquals("value", tag.getValue());
	}	
	
}
