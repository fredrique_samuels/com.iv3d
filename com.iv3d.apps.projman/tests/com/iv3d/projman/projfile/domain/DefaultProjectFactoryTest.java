/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.projman.projfile.domain;

import com.iv3d.projman.MockAwareTestCase;
import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.projfile.domain.DefaultProjectFileFactory;

public class DefaultProjectFactoryTest extends MockAwareTestCase {
	
	private DefaultProjectFileFactory instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mockUtcDateIso8601("2016-01-02T13:00:00Z");
		mockIdGenerator("PROJ_1");
		instance = new DefaultProjectFileFactory(clock, idGenerator);
	}
	
	public void testCreate() {
		String name = "Project1";
		String details = "Details";
		ProjectFileParam proj = instance.create(name, details);
		
		assertNotNull(proj);
		assertEquals("PROJ_1", proj.getId());
		assertEquals("2016-01-02T13:00:00Z", proj.getDateCreated());
		assertEquals("Project1", proj.getName());
		assertEquals("Details", proj.getDetail());
	}	
	
}
