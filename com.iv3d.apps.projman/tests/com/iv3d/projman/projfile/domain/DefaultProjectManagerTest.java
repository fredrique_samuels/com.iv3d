/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.projman.projfile.domain;

import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.MockAwareTestCase;
import com.iv3d.projman.projfile.ProjectFileDao;
import com.iv3d.projman.projfile.ProjectFileFactory;
import com.iv3d.projman.projfile.ProjectFileManager;
import com.iv3d.projman.projfile.domain.DefaultProjectFileManager;

import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

public class DefaultProjectManagerTest extends MockAwareTestCase {
	
	private ProjectFileManager instance;
	@Mocked
	private ProjectFileFactory factory;
	@Mocked
	private ProjectFileDao dao;
	@Mocked
	ProjectFileParam projectFile;
	
	public void setUp() throws Exception {
		super.setUp();
		
		new NonStrictExpectations() {
			{
				factory.create(anyString, anyString);
				result = projectFile;
			}
		};
		
		instance = new DefaultProjectFileManager(factory, dao);
	}
	

	public void testCreateProject() {
		
		ProjectFileParam project = instance.createProject(null, null);
		assertNotNull(project);
		
		new Verifications() {
			{
				factory.create(anyString, anyString);
				times=1;
				
				dao.upsert(projectFile);
				times = 1;
			}
		};
	}

	public void testUpsert() {
		instance.upsert(projectFile);
		
		new Verifications() {
			
			{
				dao.upsert(projectFile);
				times = 1;
			}
		};
	}
	
	public void testGet() {
		
		new NonStrictExpectations() {
			{
				dao.get("id");
				result = new ProjectFileParam[]{projectFile};
			}
		};
		
		ProjectFileParam[] pf = instance.get("id");
		assertEquals(1, pf.length);
		
		new Verifications() {
			{
				dao.get("id");
				times=1;
			}
		};
	}
	
	public void testGetIdList() {
		
		new NonStrictExpectations() {
			{
				dao.getIdsList();
				result = new String[]{};
			}
		};
		
		String[] pfs = instance.getIdsList();
		assertNotNull(pfs);
		
		new Verifications() {
			{
				dao.getIdsList();
				times=1;
			}
		};
	}
	
}